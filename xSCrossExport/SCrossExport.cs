﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace SCrossExport
{
    public class SCrossExport : LC360Data.Exporter
    {
        #region Properties

        /// <summary>
        /// Company name - Set in AppSettings (i.e. Sutton)
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Customer name - Set in AppSettings (i.e. USAA)
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Customers to export from LC360 - Set in AppSettings (i.e. 0000, 1234, LOOKUPID)
        /// It is a comma separated string within the App.config.
        /// </summary>
        public string CustomersToExport { get; set; }

        /// <summary>
        /// Log directory - Set in AppSettings (i.e. C:\Accounts\Sutton\USAA\Export\Logs\)
        /// </summary>
        public string LogsDir { get; set; }

        /// <summary>
        /// Return form data on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. true)
        /// </summary>
        public string ReturnFormData { get; set; }

        /// <summary>
        /// Return PDF on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. true)
        /// </summary>
        public string ReturnPDF { get; set; }

        /// <summary>
        /// Return raw images on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. false)
        /// </summary>
        public string ReturnRawImages { get; set; }

        /// <summary>
        /// Send emails to specified email address on export of cases in DEBUG mode - Set in AppSettings (i.e. test@inpections.com)
        /// </summary>
        public string EmailSendToDebug { get; set; }

        /// <summary>
        /// Specify the From email address on export of cases in DEBUG mode - Set in AppSettings (i.e. test@inpections.com)
        /// </summary>
        public string EmailFromDebug { get; set; }

        /// <summary>
        /// Send emails to specified email address on export of cases in LIVE/RELEASE mode - Set in AppSettings (i.e. inspections@sibfla.com)
        /// </summary>
        public string EmailSendToLive { get; set; }

        /// <summary>
        /// Specify the From email address on export of cases in LIVE/RELEASE mode - Set in AppSettings (i.e. GA_EDS@usaa.com)
        /// </summary>
        public string EmailFromLive { get; set; }

        /// <summary>
        /// Export filter for completed cases by start date
        /// </summary>
        public string CompletedMinDate { get; set; }

        /// <summary>
        /// Dir for exported PDF's
        /// </summary>
        public string PDFDir { get; set; }

        /// <summary>
        /// Number of cases exported
        /// </summary>
        public int ExportCount { get; set; }

        /// <summary>
        /// 360 SQL conn str
        /// </summary>
        public string cfg_360UtilConnStr { get; set; }

        /// <summary>
        ///JHA FTP Dir
        /// </summary>
        public string FTPDir { get; set; }
        #endregion


        #region IExporter Members

        /// <summary>
        /// Here we set up the filter which tells the control what cases we want back.
        /// Note: We only pass cases that have not been previously exported.
        /// </summary>
        /// <returns></returns>
        public override LC360Data.CasesSearchCriteria GetFilter()
        {
            // Message displayed for logging.
            Log(new LC360Data.Log() { Decription = "Getting Search Criteria", LogCode = LC360Data.LogCode.JustForProgrammer });

            // This would normally be filtered by a company or case type to get back desired cases.  
            // Most likely with a Date filter as well.
            LC360Data.CasesSearchCriteria criteria = new LC360Data.CasesSearchCriteria();

            // Customer selection criteria - split the comma separated string "CustomersToExport" from App.config into a string array of customer lookup IDs
            criteria.CustomerLookupIDs = CustomersToExport.Split(',');

            string date = CompletedMinDate;
            //string sEnddate = "05/10/2013";
            DateTime startDate = DateTime.Parse(date);
            //DateTime enddate = DateTime.Parse(sEnddate);
            //Setup date filter
            criteria.DateFilters = new LC360Data.DateFilters();
            criteria.DateFilters.CompletedMin = startDate;
            //criteria.DateFilters.CompletedMax = enddate;

            /// Case Statuses desired
            criteria.CaseStatuses = new int[] { (int)LC360Data.CaseStatuses.Reviewed, (int)LC360Data.CaseStatuses.Complete };

            return criteria;
        }

        /// <summary>
        /// Name of the exporter
        /// </summary>
        public override string Name
        {
            get
            { return (CompanyName + " " + CustomerName + " " + "Exporter"); }
        }

        #endregion

        /// <summary>
        /// Called by controller once the cases have been retrieved according to the Filter set in GetFilter().
        /// </summary>
        /// <param name="cases"></param>
        /// <returns></returns>
        public override LC360Data.ExportResult ExportCases(List<LC360Data.ExportCaseHolder> cases)
        {
            StringBuilder sbConfEmail = new StringBuilder();
            EmailBuilder eb = new EmailBuilder();
            List<CaseTracker> ct = new List<CaseTracker>();
            string emailSubject = "";
            int iSuccessCount = 0;
            string sIRFileNum = "";
            string sPDFFileWork = "";
            string sPDFFileName = "";

            // IR Static Values - 
            string msIRDrawer = "SCTP";
            string ms7062IRUID = "CHDAVIS";
            string ms7213IRUID = "lblair";
            string msIRUID = "CHDAVIS";
            string msIRFolderType = "Policy Information 0";
            string msIRDocType = "INSP";
            string msIRPageDesc = "Insp Rcvd";
            string msIRFlowID = "612";
            string msIRStepID = "614";
            string msIRPriority = "5";
            string msIRTaskDesc = "Inspection Received";
            string msIRConvFlag = "C";
            string msIRFileType = "SCTP";



            /// Logging
            Log(new LC360Data.Log() { Decription = "Starting " + CustomerName + " Export", LogCode = LC360Data.LogCode.JustForProgrammer });

            /// Logging
            Log(new LC360Data.Log() { Decription = "Received " + cases.Count + " for export", LogCode = LC360Data.LogCode.GeneralMessage });

            ExportCount = cases.Count;

            // Initialize confirmation email
            sbConfEmail.Append("Sutton Inspection Bureau, Inc." + System.Environment.NewLine);
            sbConfEmail.Append("The following completed inspections have been transmitted: " + System.Environment.NewLine + System.Environment.NewLine);

            /// Create List for successfully exported case IDs
            List<Guid> exportedCaseIDs = new List<Guid>();

            foreach(LC360Data.ExportCaseHolder holder in cases)
            {

                ///Keep track of Exports success or failure.
                Func<String, bool, CaseTracker> TrackCaseInfo = (description,successful) =>
                    new CaseTracker
                    {
                        Description = description,
                        Successful = successful,
                        PolicyNumber = holder.Case.PolicyNumber ?? "",
                        PolicyHolder = holder.Case.InsuredName ?? "",
                        Street1 = holder.Case.LocationAddress ?? "",
                        City = holder.Case.LocationCity ?? "",
                        StateOrProvince = holder.Case.LocationState ?? "",
                        ZipCode = holder.Case.LocationPostalCode ?? "",
                        InspectionType = holder.Case.CaseType ?? ""
                    };

                Log(new LC360Data.Log() { Decription = "Starting export of Case: " + holder.Case.CaseNumber, LogCode = LC360Data.LogCode.JustForProgrammer });
                try
                {

                    // Does case have IR File number
                    sIRFileNum = GetIRFileNum(holder.Case.CaseID, cfg_360UtilConnStr);

                    // If nothing returned - report error
                    if (sIRFileNum.Length == 0)
                    {
                        sIRFileNum = holder.Case.CaseNumber.ToString();
                        Log(new LC360Data.Log() { Decription = "No IRFileNum for case: " + holder.Case.CaseNumber, LogCode = LC360Data.LogCode.GeneralMessage });

                    }

                    //sOutputFileName = sIRDrawer + "_" & sIRFile & "_" & msIRFolderType & "_" & msIRDocType & "__" & "_______" & msIRPageDesc & "_" & msIRFlowID & "_" & msIRStepID & "_" & sIRUID & "_" & msIRPriority & "_" & msIRTaskDesc & "__" & msIRConvFlag & "_" & msIRFileType & "_" & iUnique.ToString & ".pdf"
                    sPDFFileWork = msIRDrawer + "_" + sIRFileNum + "_" + msIRFolderType + "_" + msIRDocType + "__" + "_______" + msIRPageDesc + "_" + msIRFlowID + "_" + msIRStepID + "_" + msIRUID + "_" + msIRPriority + "_" + msIRTaskDesc + "__" + msIRConvFlag + "_" + msIRFileType + "_";

                    // Insure file name is unique in FTP folder
                    // If it exists - add digit to end
                    sPDFFileName = sPDFFileWork + "1.pdf";
                    if (File.Exists(PDFDir + sPDFFileName))
                    {

                        int i = 2;
                        while (true)
                        {
                            sPDFFileName = sPDFFileWork + i.ToString() + ".pdf";
                            if (File.Exists(PDFDir + sPDFFileName))
                            {
                                i++;
                                if (i > 10)
                                {
                                    Log(new LC360Data.Log() { Decription = "**** PDF of case " + holder.Case.CaseNumber + " could not be saved (EXISTS). ****", LogCode = LC360Data.LogCode.Error });
                                    ct.Add(TrackCaseInfo("Error exporting case for Policy #: " + holder.Case.PolicyNumber + " Case #: " + holder.Case.CaseNumber + ".", false));
                                }

                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    // Gets PDF into a byte array
                    byte[] file = holder.PDFFile;

                    // Write to file
                    File.WriteAllBytes(PDFDir + sPDFFileName, file);

                    // Copy To FTP
                    FileInfo fi = new FileInfo(PDFDir + sPDFFileName);
                    fi.CopyTo(FTPDir + sPDFFileName);
                    fi = null;


                    // Log that email was successfully created
                    Log(new LC360Data.Log() { Decription = "File created for case " + holder.Case.CaseNumber + " - " + holder.Case.CaseNumber.ToString() + ".pdf", LogCode = LC360Data.LogCode.GeneralMessage });

                    /// Add this caseID to list that will be passed back to calling program
                    exportedCaseIDs.Add(holder.Case.CaseID);

                    ct.Add(TrackCaseInfo("Success exporting case: " + holder.Case.CaseNumber + " - " + emailSubject, true));

                    // Add case to confirmation email
                    sbConfEmail.Append(holder.Case.CaseNumber + "   " + holder.Case.PolicyNumber + "   " + holder.Case.InsuredName + System.Environment.NewLine);

                }
                catch (Exception ex)
                {
                    Log(new LC360Data.Log() { Decription = "Error exporting case: " + holder.Case.CaseID + ex.Message, LogCode = LC360Data.LogCode.IssueWithPlugin });
                    ct.Add(TrackCaseInfo("Error exporting case for: " + holder.Case.CaseNumber, false));
                }

            //End of ForEach holder in cases
            }

            sendConfEmail(sbConfEmail.ToString(), EmailSendToLive);

            ///Create and send success/failure email
            eb.CreateEmail(ct, cases);

            /// Arrange to pass back 'Export Complete' message
            Log(new LC360Data.Log() { Decription = "Export Complete", LogCode = LC360Data.LogCode.Successful });


            /// Set up results to be returned to the calling program
            LC360Data.ExportResult result = new LC360Data.ExportResult();

             
            //*****************************************************************
            /// Do only if live - because with test you may need to reexport
            //result.ExportedCaseIDs = exportedCaseIDs;
            //*****************************************************************

            // return successfully exported cases
            ExportCount = iSuccessCount;

            return result;
        }


        /// <summary>
        /// Send a given message using the current default SMTP Client 
        /// </summary>
        /// <param name="message"></param>
        public static void SendEmailMessage(MailMessage message)
        {
            SmtpClient client = new SmtpClient();
            client.Send(message);
        }

        public static void sendConfEmail(string bodytext, string sConfEmail)
        {
            // Send confirmation email

            //Instantiate and create Email message
            using (MailMessage msg = new MailMessage())
            {
                msg.From = new MailAddress("inspections@sibfla.com","Sutton Inspection Bureau");
                msg.To.Add(new MailAddress(sConfEmail));
                msg.Bcc.Add(new MailAddress("jeff@sibfla.com"));
                msg.Subject = "Completed Cases - " + DateTime.Now.ToShortDateString();
                SmtpClient client = new SmtpClient();
                client.Send(msg);
            }

        }

        /// <summary>
        /// Called by controller to get export options for the retrieval of case information
        /// These options are set using AppSettings located in App.config
        /// </summary>
        /// <returns></returns>
        public override LC360Data.ExportOptions GetExportOptions()
        {
            LC360Data.ExportOptions options = new LC360Data.ExportOptions();
            options.ReturnFormData = Convert.ToBoolean(ReturnFormData);
            options.ReturnPDF = Convert.ToBoolean(ReturnPDF);
            options.ReturnRawImages = Convert.ToBoolean(ReturnRawImages);
            return options;
        }

        /// <summary>
        /// Guid ID for this particular application.
        /// </summary>
        public override Guid ApplicationID
        {
            get { return new Guid("D2BC0C1E-0550-40DE-814D-1B306DA5D73B"); }
        }

        private static string GetIRFileNum(Guid guCaseID, string cfg_360UtilConnStr)
        {

            string sRetVal = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                sqlCmd1.Parameters.AddWithValue("@fieldname", "ImageRight File Number");

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        if (!sqlReader.IsDBNull(0))
                        {
                            sRetVal = sqlReader.GetSqlString(0).ToString();
                        }

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }

            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();


            }

            return sRetVal;
        }
    }

}
