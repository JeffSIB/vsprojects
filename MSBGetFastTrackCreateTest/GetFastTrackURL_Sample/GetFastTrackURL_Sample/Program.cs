﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.ServiceModel;
using GetFastTrackURL_Sample.RCT_ValuationService;

namespace GetFastTrackURL_Sample
{
    class Program
    {
        static void Main(string[] args)
        {
            // Send the data and process response
            try
            {
                ChannelFactory<ValuationService> channelFactory = new ChannelFactory<ValuationService>("ValuationServiceEndpoint");
                channelFactory.Credentials.UserName.UserName = ConfigurationManager.AppSettings["UserName"];
                channelFactory.Credentials.UserName.Password = ConfigurationManager.AppSettings["Password"];
                ValuationService valuationService = channelFactory.CreateChannel();

                // Establish FastTrackRequest
                FastTrackRequest getFastTrackRequest = new FastTrackRequest();
                getFastTrackRequest.UserLogin = "MLSutton";  // Specify UserLogin of user to be logged into RCT Express
                getFastTrackRequest.ValuationIdentifier = new ValuationIdentifier();
                getFastTrackRequest.ValuationIdentifier.PolicyNumber = "UID5"; //Specify the valuation's unique identifier
                //getFastTrackRequest.FirstName = "New";   //optional
                //getFastTrackRequest.LastName = "User";  //optional
                //getFastTrackRequest.AgencyName = "NewTestAgency";   //only required when creating a new user or agency
                getFastTrackRequest.Operation = FastTrackOperationCode.Edit;
                getFastTrackRequest.Role = "Agent";     //only required when creating a new user             
                getFastTrackRequest.RedirectURL = "http://www.msn.com";

                //Create a valuation with the valid 5 inputs
                ValuationIn valuationIn = new ValuationIn();
                valuationIn.ValuationIdentifier = new ValuationIdentifier();
                valuationIn.ValuationIdentifier.PolicyNumber = "TestPolicy5"; //Specify the valuation's unique identifier
                valuationIn.ValuationDetails = new ValuationDetails();
                valuationIn.ValuationDetails.OwnerUser = "MLSutton";
                valuationIn.ValuationDetails.UpdateUser = "MLSutton";
				valuationIn.ValuationDetails.Policy = new Policy();
				valuationIn.ValuationDetails.Policy.CurrentCoverage = 50000; //Coverage A
                valuationIn.ValuationDetails.PropertyAddress = new PropertyAddress();
                valuationIn.ValuationDetails.PropertyAddress.Address1 = "4810 W Melrose Ave";
                valuationIn.ValuationDetails.PropertyAddress.City = "Tampa";
                valuationIn.ValuationDetails.PropertyAddress.StateProvince = "FL";
                valuationIn.ValuationDetails.PropertyAddress.ZipPostalCode = "33629";
                valuationIn.ValuationDetails.PolicyHolder = new PolicyHolder();
                valuationIn.ValuationDetails.PolicyHolder.InsuredFullName = "Test Insured Name";

                //valuationIn.ValuationDetails.Building = new Building();
                //valuationIn.ValuationDetails.Building.HomeStyle = "Style.Story2";
                //valuationIn.ValuationDetails.Building.NumberOfFamilies = 1;
                //valuationIn.ValuationDetails.Building.MainHome = new Section();
                //valuationIn.ValuationDetails.Building.MainHome.YearBuilt = 1980;
                //valuationIn.ValuationDetails.Building.MainHome.NoOfStories = 2;
                //valuationIn.ValuationDetails.Building.MainHome.LivingArea = 4697;
                //valuationIn.ValuationDetails.Building.MainHome.FinishedLivingArea = 4697;

                // Enables the CoreLogic Knowledge Table Assumptions
                SupplementalDataRequest supplementalDataRequest = new SupplementalDataRequest();
                supplementalDataRequest.Assumptions = true;

                // ExpressLync4 ValutionService web service call to web method GetFastTrackURL:
                FastTrackResult getFastTrackResult = valuationService.GetFastTrackURL(getFastTrackRequest, valuationIn, supplementalDataRequest);

                // Display URL on writeline
                Console.WriteLine("FastTrackURL: {0}", getFastTrackResult.FastTrackURL);

                // Open url in default browser
                System.Diagnostics.Process.Start(getFastTrackResult.FastTrackURL);
            }
            catch (FaultException<ExpressLyncFault> faultException)
            {
                // ExpressLyncFault contains a collection of errors
                foreach (ExpressLyncError error in faultException.Detail.Errors)
                {
                    // Each RctError contains error code and description
                    Console.WriteLine("{0} - {1}", error.ErrorCode, error.ErrorDescription);
                }
            }
            catch (Exception ex)
            {
                // Any other exceptions
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("Press Enter to exit.");
            Console.ReadLine();
        }
    }
}
