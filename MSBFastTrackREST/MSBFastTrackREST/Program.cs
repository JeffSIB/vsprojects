﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MSBFastTrackREST
{

    public class DataObject
    {
        public string Name { get; set; }
    }

    public class GetQuickLink
    {

        public string Logon { get; set; }
        public string CompanyID { get; set; }
        public string Password { get; set; }
        public QuickLinkInfo QuickLinkInfo { get; set; }
    }

    public class QuickLinkInfo
    {
        public string UserLogon { get; set; }
        public long ValuationId { get; set; }
        public string Role { get; set; }
        public DateTime LogonExpires { get; set; }
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RecordAccessAction { get; set; }
        public string AfterRecordAccessAction { get; set; }
        public string RedirectUrl { get; set; }
        public string PolicyNumber { get; set; }

    }

    public class Class1
    {

        //static string sJSON =
        //@"{""Password": "AdminUserPassword",   "QuickLinkInfo": {     "UserLogon": "YourUIUser",     "ValuationId": 2,     "Role": "UW",     "LogonExpires": "2015-05-05T12:02:23.1452738-05:00",     "AuthenticationTimeout": 1.1,     "GroupName": "YourCompany Root",     "GroupDescription": "",     "FirstName": "UI",     "LastName": "User",     "RecordAccessAction": "",     "AfterRecordAccessAction": "",     "RedirectUrl": "",     "PolicyNumber": ""   },   "Logon": "AdminUser",   "CompanyID": "22222222-2222-2222-2222-222222222222",   "SecurityToken": "713b06db-ad75-43ce-88fb-a9fc01f54f00", }";

 

        static void Main(string[] args)
        {

            DateTime dLoginExpires = new DateTime();
            dLoginExpires = DateTime.Now.AddDays(7);


            QuickLinkInfo qli = new QuickLinkInfo
            {
                UserLogon = "SIBDemo",
                ValuationId = 1000,
                Role = "AD",
                LogonExpires = dLoginExpires,
                GroupName = "SuttonInspectionParent",
                GroupDescription = "",
                FirstName = "Sutton",
                LastName = "Inspection",
                RecordAccessAction = "",
                AfterRecordAccessAction = "",
                RedirectUrl = "",
                PolicyNumber = ""
            };

            GetQuickLink ql = new GetQuickLink
            {
                Password = "b25e059b1396Q",
                QuickLinkInfo = qli,
                Logon = "SIBDemo",
                CompanyID = "a9b7adb7-a081-4b96-b259-55e969e059b5"
        };
            

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://celad.msbcommercialtest.com");
            client.DefaultRequestHeaders.Accept.Clear();

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));


            // List data response.
            HttpResponseMessage response = client.PostAsJsonAsync("/Admin1.3.API.Host/api/Security/GetQuickLink", ql).Result;  // Blocking call!

            response.EnsureSuccessStatusCode();

            // return URI of the created resource.
            //return response.Headers.Location;


            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var dataObjects = response.Content.ReadAsAsync<IEnumerable<DataObject>>().Result;
                foreach (var d in dataObjects)
                {
                    Console.WriteLine("{0}", d.Name);
                }
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }
        }
    }

}
