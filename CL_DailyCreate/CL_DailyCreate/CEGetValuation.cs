﻿using CL_DailyCreate.IValuationService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CL_DailyCreate
{
    class CEGetValuation
    {
        private string _contextUserName = ConfigurationManager.AppSettings["ContextUserName"];
        private string _contextUserPassword = ConfigurationManager.AppSettings["ContextUserPassword"];
        private string _contextAlias = ConfigurationManager.AppSettings["ContextAlias"];
        private Guid _contextCompanyId = new Guid(ConfigurationManager.AppSettings["CompanyId"]);
        private string _contextLogonUser = ConfigurationManager.AppSettings["LogonUser"];

        private ChannelFactory<IValuation> _channelFactory = new ChannelFactory<IValuation>("ClearUsernameBinding_IValuation");
        ResponseGetValuation _response = null;
        public CEGetValuation()
        {
            _channelFactory.Credentials.UserName.UserName = _contextUserName + "@" + _contextAlias;
            _channelFactory.Credentials.UserName.Password = _contextUserPassword;
        }

        public string CEGetValuationbyId(long valuationid)
        {
            // gets the policy number auto created by the prior step
            // returns internal valuation number - 0 on failure
            // throws exception to calling proc
            string policynumber = "";
            string sErr = "";
               
            IValuation ivaluation = _channelFactory.CreateChannel();

             GetValuationRequest request = new GetValuationRequest();

            request.CompanyID = _contextCompanyId;
            request.Logon = _contextLogonUser;
            request.ValuationID = valuationid;

            try
            {
                _response = ivaluation.GetValuation(request);

                if ((_response.Errors.Length > 0))
                {
                    foreach (ErrorDetail error in _response.Errors)
                    {
                        sErr += error.Description + System.Environment.NewLine;
                    }
                }
                else
                {
                    if (_response.Status == ResponseStatusType.Success)
                    {
                        policynumber = _response.Valuation.ValuationNumber;
                    }
                }

                if (sErr.Length > 0)
                {
                    throw new Exception(sErr);
                }
            }
            catch (Exception ex)
            {
                // pass error back to calling procedure
                throw ex;
            }

            return policynumber; //0 = unsuccessful response
        }

        public ResponseGetValuation ReturnGetValuationResponse()
        {
            return _response;
        }
    }
}


