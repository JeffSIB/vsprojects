﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace CL_DailyCreate
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static int cfg_TimeZoneOffset;
        static DateTime dBegDate;
        static DateTime dEndDate;
        static DateTime dBegDate360;
        static DateTime dEndDate360;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        //static string msErrMsg;
        static string sEmailSubject = "";
        static StringBuilder sbEmail = new StringBuilder();
        static string sMode;
        static int iCount;


        static void Main(string[] args)
        {

            mbErr = false;
            //msErrMsg = "";
            sMode = "";
            sbEmail.Append("");
            iCount = 0;

            try
            {

                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //  AM = run at 6:01am looking for cases created 6pm previous day to 6am current day
                //  PM = run at 6:01pm looking for cases created 6am day to 6pm current day
                //  C = custom date/time dange

                if (args.Length < 1)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                if (args[0].ToUpper() == "AM")
                {
                    // run at 6:01am looking for cases created 6pm previous day to 6am current day
                    sMode = "AM";

                    DateTime dt = new DateTime();
                    dt = DateTime.Today;

                    dBegDate = dt.AddDays(-1);
                    dEndDate = dt;
                    dBegDate = ChangeTime(dBegDate, 18, 0, 0, 0);   // 6pm previous day
                    dEndDate = ChangeTime(dEndDate, 6, 0, 0, 0);    // 6am today
                }

                else if (args[0].ToUpper() == "PM")
                {

                    //  PM = run at 6:01pm looking for cases created 6am day to 6pm current day
                    sMode = "PM";

                    DateTime dt = new DateTime();
                    dt = DateTime.Today;

                    dBegDate = dt;
                    dEndDate = dt;
                    dBegDate = ChangeTime(dBegDate, 6, 1, 0, 0);   // 6:01am current day
                    dEndDate = ChangeTime(dEndDate, 18, 0, 0, 0);    // 6pm today
                }
                else if (args[0].ToUpper() == "C")
                {
                    // Custom dates/time
                    try
                    {
                        sMode = "C";
                        dBegDate = Convert.ToDateTime(args[1]);
                        dEndDate = Convert.ToDateTime(args[2]);
                        dBegDate = ChangeTime(dBegDate, 6, 1, 0, 0);   // 6:01am current day
                        dEndDate = ChangeTime(dEndDate, 18, 0, 0, 0);    // 6pm today
                    }
                    catch
                    {
                        throw new SystemException("Invalid period arguments supplied");
                    }
                }
                else
                {
                    throw new SystemException("Invalid period arguments supplied");
                }

                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                //cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));
                cfg_TimeZoneOffset = getTimeZoneOffset();

                string sDateDisp = dBegDate.ToString() + "-" + dEndDate.ToString();

                // adjust time to GMT
                dBegDate360 = dBegDate.AddHours(cfg_TimeZoneOffset);
                dEndDate360 = dEndDate.AddHours(cfg_TimeZoneOffset);

                sEmailSubject = "Commercial valuations created for cases ordered on " + sDateDisp;

                // Customers with no generic field
                List<int> lCustAcnts = new List<int>();
                lCustAcnts.Add(7317);   // Heritage
                lCustAcnts.Add(5570);   // Sutton Valuation (PP)
                lCustAcnts.Add(7316);   // AmRisc 7316 - Houston Texas    
                lCustAcnts.Add(7170);   // TCC Associates Inc.
                lCustAcnts.Add(7450);   // CrossCover - 7450


                // Customers where we have to check generic field
                //Type Yes to include a Building Valuation (ITV, TIV, MSB) Additional Fee Applies
                List<int> lCheckGF = new List<int>();
                lCheckGF.Add(7260);   // AmWINS-7260- Orlando
                lCheckGF.Add(7101);   // ABCO 7101
                lCheckGF.Add(6012);   // Absolute Underwriting Managers
                lCheckGF.Add(7320);   // AES Risk Services, LLC
                lCheckGF.Add(7217);   // AmCap Insurance 
                lCheckGF.Add(7332);   // American Intermediaries 
                lCheckGF.Add(7288);   // AmWins-7288-Brokerage of Florida
                lCheckGF.Add(7337);   // AmWINS-7337-West Palm Beach-Commercial
                lCheckGF.Add(7346);   // AmWINS-7346- AL
                lCheckGF.Add(7358);   // AmWins-7358- Jacksonville-Commercial
                lCheckGF.Add(7399);   // AmWins-7399 - Stockbridge
                lCheckGF.Add(7402);   // AmWins-7402- Escondido
                lCheckGF.Add(7403);   // AmWins-7403- Fort Washington
                lCheckGF.Add(7404);   // AmWins-7404 - Franklin
                lCheckGF.Add(7405);   // AmWins-7405- Hackettstown
                lCheckGF.Add(7406);   // AmWins-7406- Hopedale
                lCheckGF.Add(7407);   // AmWins-7407- Melville
                lCheckGF.Add(7408);   // AmWins-7408- Morehead City
                lCheckGF.Add(7409);   // AmWins-7409- Morgan Hill
                lCheckGF.Add(7410);   // AmWins-7410- Redondo Beach
                lCheckGF.Add(7411);   // AmWins-7411- Reno
                lCheckGF.Add(7412);   // AmWins-7412-Springfield
                lCheckGF.Add(7413);   // AmWins-7413-Woodland Hills
                lCheckGF.Add(7414);   // AmWins-7414-Downers Grove
                lCheckGF.Add(7415);   // AmWins-7415-Denver
                lCheckGF.Add(7301);   // AmWINS-7301-Automated-Access
                lCheckGF.Add(7222);   // Atlantic Risk Specialists, Inc.
                lCheckGF.Add(7161);   // Bass-Commercial-Atlanta
                lCheckGF.Add(7248);   // Brown & Riding 
                lCheckGF.Add(7286);   // Capacity Insurance Company - 7286
                lCheckGF.Add(7270);   // Elite Underwriters
                lCheckGF.Add(8064);   // Florida Intracoastal Underwriters, Ltd. 8064
                lCheckGF.Add(7025);   // Hull & Co.-7025- Commercial - Brandon
                lCheckGF.Add(7070);   // Hull & Co.-7070-Commercial & Personal - Jax
                lCheckGF.Add(7150);   // Hull & Co.-7150- Commercial - FL 
                lCheckGF.Add(7164);   // Hull & Co.-7164-Commercial & Personal - Margate
                lCheckGF.Add(7180);   // Hull & Co.-7180- Commercial - Tampa Bay
                lCheckGF.Add(7290);   // Hull & Co.-7290- Commercial - NC
                lCheckGF.Add(7385);   // Hull & Co.-7385 - Commercial - GA
                lCheckGF.Add(8068);   // IB Green & Associates
                lCheckGF.Add(7086);   // Jimcor Agencies -  South East
                lCheckGF.Add(7203);   // LIG Marine Managers
                lCheckGF.Add(7305);   // London Underwriters
                lCheckGF.Add(7185);   // MacDuff Underwriters, Inc.
                lCheckGF.Add(7100);   // MacNeill-7100-Group, Inc. - Commercial
                lCheckGF.Add(7354);   // MJ Kelly-7354 - Seminole
                lCheckGF.Add(7326);   // Peachtree Special Risk Brokers
                lCheckGF.Add(7124);   // Regency Insurance Brokerage
                lCheckGF.Add(7401);   // Risksmith Insurance Services
                lCheckGF.Add(7355);   // Riviera Underwriters
                lCheckGF.Add(7217);   // AmCap Insurance 
                lCheckGF.Add(7332);   // American Intermediaries 
                lCheckGF.Add(7237);   // RT Specialty-7237, LLC - FL
                lCheckGF.Add(7321);   // RT Specialty-7321, LLC - CT 
                lCheckGF.Add(7327);   // RT Specialty-7327, LLC - CA 
                lCheckGF.Add(7343);   // RT Specialty-7343, LLC - MO 
                lCheckGF.Add(7344);   // RT Specialty-7344, LLC - TN 
                lCheckGF.Add(7351);   // RT Specialty-7351, LLC - WA
                lCheckGF.Add(7352);   // RT Specialty-7352, LLC - LA
                lCheckGF.Add(7353);   // RT Specialty-7353, LLC - NV
                lCheckGF.Add(7375);   // RT Specialty-7375, LLC - NY 
                lCheckGF.Add(7386);   // RT Specialty-7386, LLC - MA 
                lCheckGF.Add(7389);   // RT Specialty-7389, LLC - FL Winter Springs 
                lCheckGF.Add(7417);   // RT Specialty-7417, LLC - KY
                lCheckGF.Add(7416);   // Scottish American Insurance
                lCheckGF.Add(7004);   // SCU-7004- Commercial - Orlando
                lCheckGF.Add(7062);   // SCU-7062- Commercial - Tampa
                lCheckGF.Add(7194);   // SCU-7194- Commercial - Boca Raton
                lCheckGF.Add(7213);   // SCU-7213-Garage - Tampa  
                lCheckGF.Add(7261);   // SCU-7261- Commercial - Miami
                lCheckGF.Add(7338);   // SCU-7338- Transportation - Orlando 
                lCheckGF.Add(7435);   // CRC-7435 - New Orleans
                lCheckGF.Add(7029);   // SeaCoast Underwriters-7029 - Lake Mary Office
                lCheckGF.Add(7074);   // Shelly, Middlebrooks & O'Leary
                lCheckGF.Add(7245);   // SIU-7245- Southern Ins. Underwriters-FL-Commercial 
                lCheckGF.Add(7246);   // SIU-7246- Southern Ins. Underwriters-Garage-GA
                lCheckGF.Add(7333);   // SIU-7333- Commercial P&C - Penn America - GA 
                lCheckGF.Add(7334);   // SIU-7334- Standard Underwriters Network - GA
                lCheckGF.Add(7357);   // SIU-7357- Commercial P&C - GA 
                lCheckGF.Add(7314);   // Sycamore 
                lCheckGF.Add(7275);   // TAPCO 
                lCheckGF.Add(7340);   // Trigon
                lCheckGF.Add(7313);   // White Pine Insurance Co.
                lCheckGF.Add(7286);   // Capacity Insurance Company - 7286

                //lCheckGF.Add(7032);   // RPS-7032-FL Risk Placement Services, Inc.
                //lCheckGF.Add(7328);   // RPS-7328-GA Risk Placement Services, Inc.
                //lCheckGF.Add(7348);   // RPS-7348-KY Risk Placement Services, Inc.- P&C  
                //lCheckGF.Add(7349);   // RPS-7349-KY Risk Placement Services, Inc.- Garage 
                //lCheckGF.Add(7350);   // RPS-7350-KY Risk Placement Services, Inc.- Brokerage 
                //lCheckGF.Add(7356);   // RPS-7356-GA Risk Placement Services, Inc.- Brokerage 
                //lCheckGF.Add(7360);   // RPS-7360-AL-NAI-Risk Placement Services - Commercial
                //lCheckGF.Add(7361);   // RPS-7361-CA Risk Placement Services, Inc.
                //lCheckGF.Add(7373);   // RPS-7373-TX Risk Placement Services, Inc.
                //lCheckGF.Add(7374);   // RPS-7374-OK Risk Placement Services, Inc.
                //lCheckGF.Add(7);   // 
                //lCheckGF.Add(7);   // 
                //lCheckGF.Add(7);   // 
                //lCheckGF.Add(7);   // 
                //lCheckGF.Add(7);   //
                //lCheckGF.Add(7);   // 
                //lCheckGF.Add(7);   // 
                //lCheckGF.Add(7);   // 


                //lCheckGF.Add();   // 

                // Customers where we have to check generic field (Additional fee add ons)
                // **** Does not look at case types
                List<int> lCheckGFNew = new List<int>();
                lCheckGFNew.Add(7366);   // 7366 - XS Brokers
                lCheckGFNew.Add(7329);   // Worldwide Facilities LLC -7329- Tampa
                lCheckGFNew.Add(7318);   // Worldwide Facilities, LLC -7318 - Orlando
                lCheckGFNew.Add(7437);   // AmWins-7437-Commercial
                lCheckGFNew.Add(7441);   // RT Specialty-7441-All Offices
                lCheckGFNew.Add(7439);   // All Risks Ltd-7439
                lCheckGFNew.Add(7445);   // River Valley Underwriters - 7445
                lCheckGFNew.Add(7452);   // Citizens Property Insurance Corporation - 7452 - Commercial
                //lCheckGFNew.Add(7138);   // Jencap Insurance Services-7138 - Tampa
                //lCheckGFNew.Add(7354);   // Jencap Insurance Services-7354 - Seminole
                //lCheckGFNew.Add(7442);   // Jencap Insurance Services-7442 - Arkansas


                // Case Types
                List<Guid> lCaseTypes = new List<Guid>();
                lCaseTypes.Add(Guid.Parse("839f0d5e-d218-4150-aa67-9f6c953549a5"));   //7317 - Property - Heritage 9.30.17
                lCaseTypes.Add(Guid.Parse("13741c10-15b5-425a-83ae-5dc044fdab1e"));   //5570 - Property Valuation (PP) Sutton
                lCaseTypes.Add(Guid.Parse("d4ddb0dd-525c-4733-87e1-d45fbeee2ea4"));   //5570 - Property Valuation (PP-Flood) Sutton 
                lCaseTypes.Add(Guid.Parse("722df7ce-e191-4898-896b-fd8b99d4c976"));   //7316 - AmRisc 
                lCaseTypes.Add(Guid.Parse("1d06d90c-ca5b-43af-8061-0cb1edb419a7"));   //7260 - Mobile Home Park Package- AmWins-Orlando
                lCaseTypes.Add(Guid.Parse("86b06e35-73d4-4eb6-9da0-c46dcf6947cd"));   //7260 - Package - AmWins-Orlando 
                lCaseTypes.Add(Guid.Parse("77e137d5-c232-4fe9-bad4-73cfb983cecd"));   //7260 - Property - AmWins-Orlando
                lCaseTypes.Add(Guid.Parse("ffd41dd5-0ca7-4102-a76e-51a3984f3801"));   //7101 - Property - ABCO 
                lCaseTypes.Add(Guid.Parse("7ab4d661-e650-4149-bac9-597221d027c4"));   //7101 - Property Short  - ABCO
                lCaseTypes.Add(Guid.Parse("6ce1ef1a-dd57-4dac-9f12-6b577236bb6e"));   //6012 - Package - Absolute UW
                lCaseTypes.Add(Guid.Parse("325c3b15-e9ab-40de-99cd-31b365085973"));   //6012 - Package Short - Absolute UW 
                lCaseTypes.Add(Guid.Parse("57bbcda7-eabc-4fd7-9172-8faecd0fd090"));   //6012 - Property - Absolute UW
                lCaseTypes.Add(Guid.Parse("a2206b45-9d60-4699-bc34-6f5bff846802"));   //6012 - Package - AES Risk Services
                lCaseTypes.Add(Guid.Parse("9f71ff8d-1394-47d9-a4d6-185c627f7278"));   //7320 - Property Short - Absolute UW
                lCaseTypes.Add(Guid.Parse("aab7768a-a573-4335-bd13-9f66ca11bc90"));   //7320 - Package Short - AES Risk Services
                lCaseTypes.Add(Guid.Parse("fde2cf64-bc1f-494c-aed7-272de8bfd926"));   //7320 - Property - AES Risk Services
                lCaseTypes.Add(Guid.Parse("6c5e928a-1b3d-4dcf-ab24-5538fe2d8753"));   //7320 - Property Short - AES Risk Services
                lCaseTypes.Add(Guid.Parse("c4ff298b-a10e-43c3-8421-ff1f75ce31f1"));   //7288 - Property
                lCaseTypes.Add(Guid.Parse("b50db61e-91e9-42e5-858f-ba6603ad8120"));   //7288 - Property w/roof
                lCaseTypes.Add(Guid.Parse("5955d86e-fbe9-414c-bab4-a60f937b6c12"));   //7337 - Mobile Home Park Package- AmWins-West Palm
                lCaseTypes.Add(Guid.Parse("2b6e14f1-919d-44db-b1cc-2741cd10809f"));   //7337 - Package - AmWins-West Palm
                lCaseTypes.Add(Guid.Parse("a90b8e6f-af14-40e4-9e22-fa7f48ab66dd"));   //7337 - Package Short - AmWins-West Palm
                lCaseTypes.Add(Guid.Parse("30ea3da5-1dfb-4cdf-8053-52717aa4dba9"));   //7337 - Property - AmWins-West Palm
                lCaseTypes.Add(Guid.Parse("3eb6d5f2-c692-4e22-a492-97320b6285fa"));   //7337 - Property Short - AmWins-West Palm
                lCaseTypes.Add(Guid.Parse("9e8fcdca-9139-4ed7-b1eb-129c0a05af0d"));   //7346 - Package - AmWins-AL
                lCaseTypes.Add(Guid.Parse("19ffcb56-ad08-40f3-bae8-32b81a28f8fc"));   //7346 - Package Short - AmWins-AL
                lCaseTypes.Add(Guid.Parse("cfe71ad7-0dad-4dea-9813-4416cc147b09"));   //7346 - Property - AmWins-AL
                lCaseTypes.Add(Guid.Parse("88476cfd-9e19-41d0-a670-b091458f42d7"));   //7346 - Property Short - AmWins-AL
                lCaseTypes.Add(Guid.Parse("d1673efc-9454-400a-9917-8ada63d6a8d3"));   //7358 - Mobile Home Park Package- AmWins-Jacksonville
                lCaseTypes.Add(Guid.Parse("6434ffb5-fc47-4e04-8228-c82325b067e8"));   //7358 - Package - AmWins-Jacksonville 
                lCaseTypes.Add(Guid.Parse("2b56e2fa-c4b7-4f96-867f-fcbaecec94d4"));   //7358 - Package Short - AmWins-Jacksonville
                lCaseTypes.Add(Guid.Parse("bfc4179e-3763-41d0-89f0-7993e3d23343"));   //7358 - Property - AmWins-Jacksonville
                lCaseTypes.Add(Guid.Parse("5da6635d-2d07-4a60-bba7-e185361bd569"));   //7358 - Property Short - AmWins-Jacksonville
                lCaseTypes.Add(Guid.Parse("3c9790a3-cccf-409d-975a-d60bbd4345ae"));   //7222 - Package - Atlantic Risk Specialists
                lCaseTypes.Add(Guid.Parse("5a853efc-b126-422c-8e49-96b7dee1ff0c"));   //7222 - Package Short - Atlantic Risk Specialists
                lCaseTypes.Add(Guid.Parse("979b8f5d-ea00-4922-bbe8-f6080de2e94c"));   //7222 - Property - Atlantic Risk Specialists 
                lCaseTypes.Add(Guid.Parse("1bfa6d9d-f182-4b6b-ab5e-3ca8e869ba00"));   //7222 - Property Short - Atlantic Risk Specialists
                lCaseTypes.Add(Guid.Parse("9e96c36a-e7a8-4408-b5db-be12f33a2f83"));   //7401 - Property
                lCaseTypes.Add(Guid.Parse("987571ad-ec19-4cdd-87ed-1ee979ef7772"));   //7401 - Property Short
                lCaseTypes.Add(Guid.Parse("fbee103a-f80c-4286-b52c-e18946b6cb98"));   //Package - AmWins-Stockbridge
                lCaseTypes.Add(Guid.Parse("1d695f7e-e0e2-46a0-be05-924252e0715e"));   //Package Short - AmWins-Stockbridge
                lCaseTypes.Add(Guid.Parse("c047d74c-f713-492a-9514-640605e424b7"));   //Property - AmWins-Stockbridge
                lCaseTypes.Add(Guid.Parse("ddee4030-d44e-482f-923a-8ad728c581cb"));   //Property Short - AmWins-Stockbridge
                lCaseTypes.Add(Guid.Parse("1105b856-296c-45b9-b8aa-c8732c7a6e5f"));   //Package Short - AmWins-Escondido
                lCaseTypes.Add(Guid.Parse("fb6a2870-8572-466d-9325-a5b5731a7211"));   //Property - AmWins-Escondido
                lCaseTypes.Add(Guid.Parse("307ddfe5-621f-4b42-b653-af98ed1a1fcf"));   //Package - AmWins-Escondido
                lCaseTypes.Add(Guid.Parse("a6fad684-8b9f-4fc2-9942-98e0acbf5ce5"));   //Property Short - AmWins-Escondido
                lCaseTypes.Add(Guid.Parse("db13c7e7-121d-4873-b5bd-d88b74792a21"));   //Package - AmWins-Fort Washington
                lCaseTypes.Add(Guid.Parse("4c836cf1-1ce0-42a8-b1f3-22e4f155f7ce"));   //Package Short - AmWins-Fort Washington
                lCaseTypes.Add(Guid.Parse("80cd661c-ffe5-490b-b8a4-4ad5df7b6939"));   //Property - AmWins-Fort Washington
                lCaseTypes.Add(Guid.Parse("4a641c31-59ab-4cb7-933a-b57f67dd69a6"));   //Property Short - AmWins-Fort Washington
                lCaseTypes.Add(Guid.Parse("4d7d58db-5e94-41e4-88af-e2971ed37e9c"));   //Package - AmWins-Franklin
                lCaseTypes.Add(Guid.Parse("486f07c7-34f0-407b-b1fb-be351e1a71a8"));   //Package Short - AmWins-Franklin
                lCaseTypes.Add(Guid.Parse("18bd2078-7e44-4cb0-900f-00f3ba620d54"));   //Property - AmWins-Franklin
                lCaseTypes.Add(Guid.Parse("c1476c4f-1288-4f64-a9cd-8d295719b716"));   //Property Short - AmWins-Franklin
                lCaseTypes.Add(Guid.Parse("a3371187-9073-4663-8797-724e5d67017a"));   //Package - AmWins-Hackettstown
                lCaseTypes.Add(Guid.Parse("a3371187-9073-4663-8797-724e5d67017a"));   //Package Short - AmWins-Hackettstown
                lCaseTypes.Add(Guid.Parse("7ee1213d-9bda-4c54-af11-74b70e697849"));   //Property - AmWins-Hackettstown
                lCaseTypes.Add(Guid.Parse("9bccc2f9-fa79-4646-9b56-ef01025cc353"));   //Property Short - AmWins-Hackettstown
                lCaseTypes.Add(Guid.Parse("1a0ff160-16d2-4419-baa2-ec33d830e8b1"));   //Package - AmWins-Hopedale
                lCaseTypes.Add(Guid.Parse("44aa662e-bb13-4e33-a758-261be0413ef8"));   //Package Short - AmWins-Hopedale
                lCaseTypes.Add(Guid.Parse("db0a5fd8-81c1-416e-995f-0f7695eee53c"));   //Property - AmWins-Hopedale
                lCaseTypes.Add(Guid.Parse("a2ff0f78-0efe-473b-81a4-f9c2843d3ab3"));   //Property Short - AmWins-Hopedale
                lCaseTypes.Add(Guid.Parse("fb46a34e-08e7-4f87-8ad7-7b77330bd2cc"));   //Package - AmWins-Melville
                lCaseTypes.Add(Guid.Parse("cbee37b9-d3d0-445f-a9d4-558967ae4c5b"));   //Package Short - AmWins-Melville
                lCaseTypes.Add(Guid.Parse("314eab84-1828-4b21-b083-b8583b9044dd"));   //Property - AmWins-Melville
                lCaseTypes.Add(Guid.Parse("65643d68-def8-41ab-ab28-d4470d43d9da"));   //Property Short - AmWins-Melville
                lCaseTypes.Add(Guid.Parse("28eb1952-63b5-4ed0-9124-5f2c20ec9dfb"));   //Package - AmWins-Morehead City
                lCaseTypes.Add(Guid.Parse("c411bd03-edd9-4a6f-965d-9d54d2485476"));   //Package Short - AmWins-Morehead City
                lCaseTypes.Add(Guid.Parse("0e506123-97a2-462c-81de-351cad61e6dc"));   //Property - AmWins-Morehead City
                lCaseTypes.Add(Guid.Parse("d5445661-0140-40a2-bb42-80acbc869a44"));   //Property Short - AmWins-Morehead City
                lCaseTypes.Add(Guid.Parse("1987b04c-8c71-4bab-911d-a6671c62b461"));   //Package - AmWins-Morgan Hill
                lCaseTypes.Add(Guid.Parse("15d94dc1-ce68-4298-af83-a30f51a810d6"));   //Package Short - AmWins-Morgan Hill
                lCaseTypes.Add(Guid.Parse("f2d795a9-da79-4f1b-bc20-51c758cbeeb3"));   //Property - AmWins-Morgan Hill
                lCaseTypes.Add(Guid.Parse("5c3eb30f-790b-4f12-a92f-f8ff18dd6a49"));   //Property Short - AmWins-Morgan Hill
                lCaseTypes.Add(Guid.Parse("baa60719-63ee-4dd5-9b6f-a303c902e208"));   //Package - AmWins-Redondo Beach
                lCaseTypes.Add(Guid.Parse("8e760e58-8f98-4106-8fea-4347451310af"));   //Package Short - AmWins-Redondo Beach
                lCaseTypes.Add(Guid.Parse("666c3f3c-ac94-4f86-82b7-9c36ddc86269"));   //Property - AmWins-Redondo Beach
                lCaseTypes.Add(Guid.Parse("f786f581-d46d-4496-8b42-557473a6c718"));   //Property Short - AmWins-Redondo Beach
                lCaseTypes.Add(Guid.Parse("f1d1cfe7-4ca7-41d9-bc96-bae846cdcc9e"));   //Package - AmWins-Reno
                lCaseTypes.Add(Guid.Parse("5b7c4f4b-417a-44ed-9410-df410896537b"));   //Package Short - AmWins-Reno
                lCaseTypes.Add(Guid.Parse("6206eae4-fba2-4c6e-97b9-f333fbaa78b5"));   //Property - AmWins-Reno
                lCaseTypes.Add(Guid.Parse("71225cf4-5e2e-45f1-bf4e-eb76b513b41f"));   //Property Short - AmWins-Reno
                lCaseTypes.Add(Guid.Parse("3d060b35-ecf2-4a51-ad6c-9405ac26337c"));   //Package - AmWins-Woodland Hills
                lCaseTypes.Add(Guid.Parse("c83c339c-5eeb-4ec5-a363-973b3f71b08e"));   //Package Short - AmWins-Woodland Hills
                lCaseTypes.Add(Guid.Parse("b789c66a-b7f3-471e-a7b1-04c17733a80b"));   //Property - AmWins-Woodland Hills
                lCaseTypes.Add(Guid.Parse("5f8d3bb6-11f8-443e-8c72-2e62a5ec3e62"));   //Property Short - AmWins-Woodland Hills
                lCaseTypes.Add(Guid.Parse("920d5780-91d3-4b35-9bb0-4d3d0571f81a"));   //Package - AmWins-Downers Grove
                lCaseTypes.Add(Guid.Parse("b0067d67-4b8e-429f-8fc6-6f88ec58364d"));   //Package Short - AmWins-Downers Grove
                lCaseTypes.Add(Guid.Parse("94d3eee1-fba9-4fee-9050-0084768193e5"));   //Property - AmWins-Downers Grove
                lCaseTypes.Add(Guid.Parse("65bf45c2-3bfb-418b-a57f-0d8ba6dceeb2"));   //Property Short - AmWins-Downers Grove
                lCaseTypes.Add(Guid.Parse("11e084c4-2fbd-4d2e-8d06-17f934c06263"));   //Package - AmWins-Denver
                lCaseTypes.Add(Guid.Parse("4b448271-6f4d-4641-8d14-0dc8da42a2f7"));   //Package Short - AmWins-Denver
                lCaseTypes.Add(Guid.Parse("1078cf00-6997-4a29-b711-d246a46670e9"));   //Property - AmWins-Denver
                lCaseTypes.Add(Guid.Parse("6c6777be-9613-49c3-be1e-3faa7d34911c"));   //Property Short - AmWins-Denver
                lCaseTypes.Add(Guid.Parse("e651ae6c-dc56-4a76-b933-71452e21eb3b"));   //Package - Bass - Atlanta
                lCaseTypes.Add(Guid.Parse("78b012af-1ec9-410d-85bf-84a851035fd8"));   //Package - Brown & Riding 
                lCaseTypes.Add(Guid.Parse("714b305c-0e92-45a5-952c-892e75d6527a"));   //Package Short - Brown & Riding
                lCaseTypes.Add(Guid.Parse("06019cf2-dba9-455a-a5d9-cd4ee606d789"));   //Property - Brown & Riding 
                lCaseTypes.Add(Guid.Parse("88f135ec-985c-4ff4-a4d1-df0fa95b8ef7"));   //Property Short - Brown & Riding 
                lCaseTypes.Add(Guid.Parse("9ccd874d-c1de-4508-8677-738b2ad5e646"));   //Package - Elite Underwriters 
                lCaseTypes.Add(Guid.Parse("9ab0220a-9c7b-4281-b883-57fc291a3bd3"));   //Package Short - Elite Underwriters
                lCaseTypes.Add(Guid.Parse("0e098e26-356c-4d39-bdad-f55551c5e3a0"));   //Property - Elite Underwriters
                lCaseTypes.Add(Guid.Parse("0341ccfd-5515-49cc-9a00-37c65eab38d6"));   //Property Short - Elite Underwriters
                lCaseTypes.Add(Guid.Parse("7744e08c-9ad9-412b-9ec4-b30d2fba4632"));   //Package - IB Green
                lCaseTypes.Add(Guid.Parse("da63cf4a-5c81-4fdb-8582-d88d3acabb30"));   //Package Short - IB Green
                lCaseTypes.Add(Guid.Parse("57790e91-fc2d-45d4-ade7-9f6ef141a7b9"));   //Property - IB Green
                lCaseTypes.Add(Guid.Parse("968c766d-d75f-4454-a687-ccd2b0636b18"));   //Property Short - IB Green
                lCaseTypes.Add(Guid.Parse("0498375e-e050-4991-8b77-83e7227a98ee"));   //Package - Jimcor-GA
                lCaseTypes.Add(Guid.Parse("8c527c78-7930-47d5-8509-f43decdea318"));   //Package Short - Jimcor-GA
                lCaseTypes.Add(Guid.Parse("2047fd51-ba70-4e8d-9a7c-0cd4f926f263"));   //Property - Jimcor-GA
                lCaseTypes.Add(Guid.Parse("ead2f2ee-63b2-4c03-90ad-3f2625b02a88"));   //Property Short - Jimcor-GA
                lCaseTypes.Add(Guid.Parse("fc31df5f-79e4-4c87-9c9f-6cdca90dacc3"));   //Package - LIG
                lCaseTypes.Add(Guid.Parse("37ef5f8f-53ed-45b3-8f3b-95e849fdd411"));   //Property - LIG
                lCaseTypes.Add(Guid.Parse("d6812f91-6fcb-4864-90a9-b5d831ade57c"));   //Package - MJ Kelly-Seminole
                lCaseTypes.Add(Guid.Parse("11533add-aff5-4cb4-ae12-57770e2716b6"));   //Package Short - MJ Kelly-Seminole
                lCaseTypes.Add(Guid.Parse("cc80c478-a838-47d4-8d88-6e15e52a4333"));   //Property - MJ Kelly-Seminole
                lCaseTypes.Add(Guid.Parse("d3b3f744-db78-486c-81a9-a05377da9bc1"));   //Property Short - MJ Kelly-Seminole
                lCaseTypes.Add(Guid.Parse("d4b5ca3e-c5f0-4ba9-adc0-525fcb61ef70"));   //Property - Riviera Underwriters
                lCaseTypes.Add(Guid.Parse("8380af97-19aa-4307-9210-d7074d69e1ab"));   //Property Short -  Riviera Underwriters
                lCaseTypes.Add(Guid.Parse("a3bc2342-ffbb-4be1-b347-797b9fabdce7"));   //Package - SCU-Orlando
                lCaseTypes.Add(Guid.Parse("d2278ef6-8d5f-4c7b-b00e-a60f8d8ac957"));   //Package Short - SCU-Orlando
                lCaseTypes.Add(Guid.Parse("c318b3ee-dc13-4e00-9e55-780bd2340c37"));   //Property - SCU-Orlando
                lCaseTypes.Add(Guid.Parse("0182c5dc-f3ff-4450-9ee4-cbfd614e7fdb"));   //Property Short - SCU-Orlando
                lCaseTypes.Add(Guid.Parse("860054a4-1828-4565-9099-fc974698da3a"));   //Package - Shelly, Middlebrooks & O'Leary
                lCaseTypes.Add(Guid.Parse("c1f47c0c-b2ab-436c-8bed-6a367ebfdf40"));   //Package Short - Shelly, Middlebrooks & O'Leary
                lCaseTypes.Add(Guid.Parse("f5d47187-e99b-4659-a9e8-e1160ba85c14"));   //Property - Shelly, Middlebrooks & O'Leary
                lCaseTypes.Add(Guid.Parse("78e457eb-a5f3-4ad1-9bd2-2f16eaf9b46b"));   //Property Short - Shelly, Middlebrooks & O'Leary

                lCaseTypes.Add(Guid.Parse("1d06d90c-ca5b-43af-8061-0cb1edb419a7"));   //Mobile Home Park Package- AmWins-Orlando
                lCaseTypes.Add(Guid.Parse("5955d86e-fbe9-414c-bab4-a60f937b6c12"));   //Mobile Home Park Package- AmWins-West Palm
                lCaseTypes.Add(Guid.Parse("d1673efc-9454-400a-9917-8ada63d6a8d3"));   //Mobile Home Park Package- AmWins-Jacksonville
                lCaseTypes.Add(Guid.Parse("e9567a55-e27b-45b3-91ac-1a00ab6331ab"));   //Mobile Home Park Package- AmWins-Stockbridge
                lCaseTypes.Add(Guid.Parse("d53b9646-a6fe-48a6-97f0-11aaae700655"));   //Mobile Home Park Package- AmWins-Escondido
                lCaseTypes.Add(Guid.Parse("d0295459-04ec-4ab8-a0f5-bfa073e88865"));   //Mobile Home Park Package- AmWins-Fort Washington
                lCaseTypes.Add(Guid.Parse("a00b5fc3-7252-44f0-bc96-5a02a6aeb18a"));   //Mobile Home Park Package- AmWins-Franklin
                lCaseTypes.Add(Guid.Parse("2844f1ba-c889-4a93-bec9-55c23e7df6f5"));   //Mobile Home Park Package- AmWins-Hackettstown
                lCaseTypes.Add(Guid.Parse("30a4f6d3-dd9d-4ea7-a516-562ffe9a8696"));   //Mobile Home Park Package- AmWins-Hopedale
                lCaseTypes.Add(Guid.Parse("56b97bb3-2b2d-483e-a4e3-07108b16b770"));   //Mobile Home Park Package- AmWins-Melville
                lCaseTypes.Add(Guid.Parse("3dd06800-a904-4e5f-af4b-6ae855b36260"));   //Mobile Home Park Package- AmWins-Morehead City
                lCaseTypes.Add(Guid.Parse("d5c7f21c-9dc4-46b3-a5c7-019227729b7f"));   //Mobile Home Park Package- AmWins-Morgan Hill
                lCaseTypes.Add(Guid.Parse("6cf0d49c-d7eb-4b06-b94a-dc005c663ecb"));   //Mobile Home Park Package- AmWins-Redondo Beach
                lCaseTypes.Add(Guid.Parse("147122cb-3fe4-4b61-a036-21d4baad1572"));   //Mobile Home Park Package- AmWins-Reno
                lCaseTypes.Add(Guid.Parse("59179714-602e-4318-95f3-06df890089ec"));   //Mobile Home Park Package- AmWins-Springfield
                lCaseTypes.Add(Guid.Parse("7a2e8b69-89b0-48f1-8338-b2235d42a041"));   //Mobile Home Park Package- AmWins-Woodland Hills
                lCaseTypes.Add(Guid.Parse("6e9a45a4-7755-4a8e-bcf5-0156eb69c166"));   //Mobile Home Park Package- AmWins-Downers Grove
                lCaseTypes.Add(Guid.Parse("d7dbed22-9808-43b7-a9b1-dfa2188e9f29"));   //Mobile Home Park Package- AmWins-Denver
                lCaseTypes.Add(Guid.Parse("95a29b4c-5662-4a7e-8cb2-5f1c358b2a7a"));   //Garage Package - Brown & Riding 
                lCaseTypes.Add(Guid.Parse("7557bf6f-2700-445b-bdd7-45804b918961"));   //Garage Package - IB Green
                lCaseTypes.Add(Guid.Parse("965ed7a4-8e09-49ab-aabe-dd09c69d8eaa"));   //Garage Package - Jimcor-GA
                lCaseTypes.Add(Guid.Parse("a1fdf651-3f50-4e5a-be5b-5ce38f4cd91f"));   //Mobile Home Park Package - Jimcor-GA
                lCaseTypes.Add(Guid.Parse("b542f4ad-e2ef-46ca-85cf-ba30add3154c"));   //Garage Package - Commercial-Johnson & Johnson
                lCaseTypes.Add(Guid.Parse("5491a622-6570-42cf-9378-e1ac6cc2bf67"));   //Mobile Home Park Package  - Commercial-Johnson & Johnson
                lCaseTypes.Add(Guid.Parse("6077cf52-d034-4e40-a40e-e2064747fe7b"));   //Garage Package - MJ Kelly-Seminole
                lCaseTypes.Add(Guid.Parse("0912c0e1-cd72-4d78-aa89-195095b1b9d2"));   //Mobile Home Park Package - MJ Kelly- Seminole  
                lCaseTypes.Add(Guid.Parse("0b8031f8-c7cf-4e0b-9c7b-92f485de70a4"));   //Garage Package - SCU-Orlando
                lCaseTypes.Add(Guid.Parse("137e0b96-77dd-458e-bcea-8a1e3abc08d3"));   //Garage Package Short - SCU-Orlando
                lCaseTypes.Add(Guid.Parse("535f25bd-3b57-4c59-9957-415de8783775"));   //Mobile Home Park Package - Shelly, Middlebrooks & O'Leary

                lCaseTypes.Add(Guid.Parse("12145f33-f7a0-4911-8dc5-81c3a47f9f19"));   //Package - XS Brokers
                lCaseTypes.Add(Guid.Parse("3c705c28-14c0-45ef-9c14-17d4066c334d"));   //Package Short - XS Brokers
                lCaseTypes.Add(Guid.Parse("871b17ed-3994-431d-9748-0d0f9af74f22"));   //Package - Worldwide - Tampa
                lCaseTypes.Add(Guid.Parse("767465ef-8137-4c62-9626-700e8246ec7b"));   //Package Short - Worldwide - Tampa
                lCaseTypes.Add(Guid.Parse("5fdf2f54-3da9-4e53-bc48-d4ea17b54c3d"));   //Package - Worldwide - Orlando
                lCaseTypes.Add(Guid.Parse("286ed3d1-b1de-4472-aec7-452bd3f8fb06"));   //Package & Buildfax - Worldwide - Orlando
                lCaseTypes.Add(Guid.Parse("645f0971-e1fb-441e-b5fc-b25c55b04f13"));   //Package Short - Worldwide - Orlando
                lCaseTypes.Add(Guid.Parse("8979df33-caa4-4114-b04c-2acdd6fbb093"));   //Package Short & Buildfax - Worldwide - Orlando

                lCaseTypes.Add(Guid.Parse("ec3add60-5134-4330-a4bc-7bacb865494e"));   //Package - Scottish American
                lCaseTypes.Add(Guid.Parse("a9a94d89-b40c-4adb-b9a8-79c1b6f8d68b"));   //Package Short - Scottish American
                lCaseTypes.Add(Guid.Parse("2e14e5c0-243f-4f7e-931a-7b726ba9c44f"));   //Property - Scottish American
                lCaseTypes.Add(Guid.Parse("c7fabf07-413e-4771-a7f4-7814cafcd376"));   //Property Short - Scottish American
                lCaseTypes.Add(Guid.Parse("b1a56d60-77bf-435d-a1db-8fae06851115"));   //Mobile Home Park Package - Scottish American
                lCaseTypes.Add(Guid.Parse("2204b1d3-681d-42f9-82b0-4c93de8204ce"));   //Garage Package - Scottish American
                lCaseTypes.Add(Guid.Parse("53e9474c-bc78-443a-a24f-aaa795d84720"));   //Garage Package Short - Scottish American
                lCaseTypes.Add(Guid.Parse("76094f8c-5f23-4517-a360-bf92a0e893fa"));   //Property - TCC
                lCaseTypes.Add(Guid.Parse("8bc2b3bc-e530-4e6c-b84a-42922c373adc"));   //Property Valuation (PP) - TCC
                lCaseTypes.Add(Guid.Parse("d570b2d1-5a18-4dbd-853f-d077301f560d"));   //Property Valuation (PP-Flood) - TCC
                lCaseTypes.Add(Guid.Parse("144c70d3-9cd7-4887-bb25-e7268f82dd7a"));   //Package - TAPCO 
                lCaseTypes.Add(Guid.Parse("52347a8f-80ac-4550-9f5a-e6e3ec66d301"));   //Package I - TAPCO
                lCaseTypes.Add(Guid.Parse("3be985ca-abd8-43ab-b123-1eede0a66151"));   //Property - TAPCO
                lCaseTypes.Add(Guid.Parse("8abe18e6-a14a-4be3-a45c-92b00652dfa1"));   //Property I - TAPCO 
                lCaseTypes.Add(Guid.Parse("7ade859e-cc08-4fcf-b8dd-f8627d113460"));   //Garage Package - TAPCO 
                lCaseTypes.Add(Guid.Parse("f7820476-c9e8-4aca-80b7-a0bb612578ad"));   //Garage Package I - TAPCO 
                lCaseTypes.Add(Guid.Parse("b22b681e-2338-4646-8e71-427099797c22"));   //Mobile Home Park Package - TAPCO
                lCaseTypes.Add(Guid.Parse("560f5338-8d09-4576-acdf-e73a14b1f842"));   //Package - White Pine 
                lCaseTypes.Add(Guid.Parse("8a49c669-b673-40cd-a983-6f8e7c4da15e"));   //Package Short - White Pine 
                lCaseTypes.Add(Guid.Parse("aff2f5aa-8501-4c68-ae45-681fda916e73"));   //Property - White Pine 
                lCaseTypes.Add(Guid.Parse("91c74d58-ed6f-4835-883a-97edcce5db35"));   //Property Short - White Pine 
                lCaseTypes.Add(Guid.Parse("99af1dac-aaa2-41ed-a407-c04037fef055"));   //Garage Package - White Pine 
                lCaseTypes.Add(Guid.Parse("2c7aa424-a896-472c-8774-9c3f6e522f44"));   //Package - Sycamore
                lCaseTypes.Add(Guid.Parse("a4800cc4-86d6-4883-8187-9dd4160e7aae"));   //Package Short - Sycamore
                lCaseTypes.Add(Guid.Parse("e7c8574b-ed78-4d38-b777-66f7e928887c"));   //Property - Sycamore
                lCaseTypes.Add(Guid.Parse("e311684f-d007-421e-949b-f20b9467dab7"));   //Property Short - Sycamore
                lCaseTypes.Add(Guid.Parse("6f026864-c57c-46dc-9879-e87e7de0da6f"));   //Package - SCU - Tampa
                lCaseTypes.Add(Guid.Parse("7564a2ef-0ed4-43c0-98c8-1a0312d96655"));   //Package Short - SCU - Tampa 
                lCaseTypes.Add(Guid.Parse("1e219c7d-5ab6-486f-a6f2-cb2968cf71de"));   //Property - SCU - Tampa
                lCaseTypes.Add(Guid.Parse("5ed6ca8f-368a-45df-bd01-af1502749232"));   //Mobile Home Park Package - SCU - Tampa
                lCaseTypes.Add(Guid.Parse("cc710847-bfd2-4fa0-8922-393dc912c53d"));   //Package - SCU - Boca Raton
                lCaseTypes.Add(Guid.Parse("c34e8969-3f88-4de4-82b9-6efdc49d5b38"));   //Package Short - SCU - Boca Raton
                lCaseTypes.Add(Guid.Parse("c319de90-45de-47c9-94cc-91f87fe39be4"));   //Property - SCU - Boca Raton
                lCaseTypes.Add(Guid.Parse("295e0787-96fd-4f63-bb33-31868c81b94b"));   //Property Short - SCU - Boca Raton
                lCaseTypes.Add(Guid.Parse("5ed6ca8f-368a-45df-bd01-af1502749232"));   //Mobile Home Park Package - SCU - Tampa
                lCaseTypes.Add(Guid.Parse("3b942908-cb64-4f5c-9c7b-f526f175ae34"));   //Garage Package - SCU - Tampa-Garage
                lCaseTypes.Add(Guid.Parse("6cbabeef-e0bd-45a7-9801-60711061a8d0"));   //Package Short - SCU - Tampa-Garage
                lCaseTypes.Add(Guid.Parse("32a723ae-166c-4820-b8a3-6dc4ecf64c2c"));   //Package Short - SCU - Miami
                lCaseTypes.Add(Guid.Parse("b97ef6f2-8e5f-451b-b76e-3208c362c9ec"));   //Property Short - SCU - Miami
                lCaseTypes.Add(Guid.Parse("ac4af927-e1dc-42a4-9383-e80388bd3439"));   //Garage Package - SCU-Orlando
                lCaseTypes.Add(Guid.Parse("1c1ac836-f656-49ca-95e3-a52b92841c29"));   //Package Short-SeaCoast-Commercial
                lCaseTypes.Add(Guid.Parse("99d1b5f2-e8fd-428a-84bb-33dc13aa8faf"));   //Package-SeaCoast-Commercial
                lCaseTypes.Add(Guid.Parse("28a18ae0-6489-42be-a76f-c71882788b41"));   //Property Short-SeaCoast-Commercial
                lCaseTypes.Add(Guid.Parse("1605dae3-e5ec-4778-8148-1fafd3ed8a40"));   //Property-SeaCoast-Commercial
                lCaseTypes.Add(Guid.Parse("e63a5d35-4ac0-4239-aaf8-767fdc9b24c4"));   //Package - SIU-Comm-FL
                lCaseTypes.Add(Guid.Parse("bb7d8027-cd73-44fd-acb7-18d2ac2e66b9"));   //Package I - SIU-Comm-FL
                lCaseTypes.Add(Guid.Parse("f3d3b7f5-cd5c-4cca-bad3-d6ef0bf051c9"));   //Property - SIU-Comm-FL
                lCaseTypes.Add(Guid.Parse("273d9930-65ab-466e-8102-3e0dc3d67a70"));   //Property I - SIU-Comm-FL
                lCaseTypes.Add(Guid.Parse("50208f45-c3a5-416b-8da9-b1f5c80db88a"));   //Garage Package - SIU-GA
                lCaseTypes.Add(Guid.Parse("b21b4120-9820-4ead-bfa2-d1c370389f3d"));   //Garage Package Short - SIU-GA
                lCaseTypes.Add(Guid.Parse("ffaa22ef-8dcd-4816-b38d-14c9a8ee7808"));   //Package - SIU-Commercial P&C-Penn America
                lCaseTypes.Add(Guid.Parse("0b7e045e-16d5-485d-a14e-95bdc4fd616b"));   //Package I - SIU-Commercial P&C-Penn America-GA
                lCaseTypes.Add(Guid.Parse("81fa831e-55f3-45fe-8408-13dafd57dc89"));   //Property - SIU-Commercial P&C-Penn America-GA
                lCaseTypes.Add(Guid.Parse("65068df2-ba30-4f27-96d3-d12a09d1ceba"));   //Property I - SIU-Commercial P&C-Penn America-GA
                lCaseTypes.Add(Guid.Parse("f1614c6e-a679-4741-9f78-4603bfb4b152"));   //Package - SIU-Standard Underwriters Network-GA
                lCaseTypes.Add(Guid.Parse("7cf23851-fac2-4510-b4b9-5e754e6954e1"));   //Package I - SIU-Standard Underwriters Network-GA
                lCaseTypes.Add(Guid.Parse("120670c4-b27f-4955-82f2-08aa57ffa3b8"));   //Property - SIU-Standard Underwriters Network-GA
                lCaseTypes.Add(Guid.Parse("d06ede7b-a78f-4714-9a74-d7af47816cd4"));   //Property I - SIU-Standard Underwriters Network-GA
                lCaseTypes.Add(Guid.Parse("b1c2ede0-ea3e-4848-b8fb-2a5ccf5eff71"));   //Package - SIU-Commercial P&C-GA
                lCaseTypes.Add(Guid.Parse("2a50a33a-9206-4990-994f-143cc3f2a71a"));   //Package I - SIU-Commercial P&C-GA
                lCaseTypes.Add(Guid.Parse("d5fa9e75-ed1e-4654-92aa-2e528372106e"));   //Property - SIU-Commercial P&C-GA
                lCaseTypes.Add(Guid.Parse("fafe1f35-10b5-4ebd-84fe-ac316ef7faa7"));   //Property I - SIU-Commercial P&C-GA
                lCaseTypes.Add(Guid.Parse("c87a5de6-1a47-4035-b380-5503ef5963f2"));   //Property - AmCap
                lCaseTypes.Add(Guid.Parse("6898d80e-7356-4a23-8822-16f1e9fc2fad"));   //Property - American Intermediaries 
                lCaseTypes.Add(Guid.Parse("7df97946-c32b-474c-b475-effa04f89991"));   //Property Short - American Intermediaries
                lCaseTypes.Add(Guid.Parse("37f67e5c-9423-45f6-aee4-5c61a0d88ab3"));   //Package - Trigon
                lCaseTypes.Add(Guid.Parse("99636f8d-13e6-4f4e-ad18-69a71b630031"));   //Package Short - Trigon
                lCaseTypes.Add(Guid.Parse("1cc2174e-a660-4656-89d3-620a4eb6a0e0"));   //Property - Trigon
                lCaseTypes.Add(Guid.Parse("9261e28b-1e5b-40ce-9908-900f32661bb9"));   //Property Short - Trigon
                lCaseTypes.Add(Guid.Parse("e06fcf5e-4f97-45b2-b375-ee4fd2decbb8"));   //Garage Package - RT Speciality-FL
                lCaseTypes.Add(Guid.Parse("5643a4c1-13aa-4390-8b4a-37a4309894a1"));   //Garage Package I - RT Speciality-FL
                lCaseTypes.Add(Guid.Parse("312057f0-1177-43b8-b0da-c452c943de76"));   //Mobile Home Park Package - RT Specialty-FL
                lCaseTypes.Add(Guid.Parse("a78b6c0c-d82d-4b74-99d6-45b2e45e5291"));   //Package - RT Specialty-FL 
                lCaseTypes.Add(Guid.Parse("1a5f6fe2-c3ce-4e52-aea6-f6a0d9943811"));   //Package I - RT Specialty-FL 
                lCaseTypes.Add(Guid.Parse("80fb87ec-7e32-4b53-a8e7-b06fe5b17fda"));   //Package w/Roof - RT Specialty-FL 
                lCaseTypes.Add(Guid.Parse("14cea13e-7712-4d0a-8944-a6f50c64c0b8"));   //Package w/Roof I - RT Specialty-FL 
                lCaseTypes.Add(Guid.Parse("e31a921d-528f-4020-ba5c-e72ced972e45"));   //Property - RT Specialty-FL 
                lCaseTypes.Add(Guid.Parse("0bfceaae-3b58-488b-97c3-6122e94bfb48"));   //Property I - RT Specialty-FL
                lCaseTypes.Add(Guid.Parse("7f2c7d81-3b2f-4341-b39c-4621e4ee48b9"));   //Property w/Roof - RT Specialty-FL 
                lCaseTypes.Add(Guid.Parse("9257aeb0-ebe2-465b-8587-95389e7f1afc"));   //Property w/Roof I - RT Specialty-FL 
                lCaseTypes.Add(Guid.Parse("6398f26b-c650-4d38-8fbb-f5c25ba4f516"));   //Garage Package - RT Speciality-CT
                lCaseTypes.Add(Guid.Parse("dabb8947-8ee0-4511-b12b-4eb7edb9e0e1"));   //Garage Package I - RT Speciality-CT
                lCaseTypes.Add(Guid.Parse("f21ac08c-93e7-4142-b518-f33b79438e4a"));   //Package - RT Specialty-CT
                lCaseTypes.Add(Guid.Parse("3809f5b9-543b-42cf-a7ea-a8aa383babf2"));   //Package I - RT Specialty-CT 
                lCaseTypes.Add(Guid.Parse("373621b7-cc7b-4db3-82cb-17828a446037"));   //Property - RT Specialty-CT 
                lCaseTypes.Add(Guid.Parse("f8247c74-5478-47af-b1b7-55e13fa389cb"));   //Property I - RT Specialty-CT
                lCaseTypes.Add(Guid.Parse("f0c6103b-ed92-4f2b-893f-74acba3619a6"));   //Garage Package - RT Speciality-CA
                lCaseTypes.Add(Guid.Parse("9dd44c3e-5bf2-4e86-b915-7ec2d8001143"));   //Garage Package I - RT Speciality-CA
                lCaseTypes.Add(Guid.Parse("c77f0510-3ab6-4192-aace-d60d54d85e84"));   //Package - RT Specialty-CA 
                lCaseTypes.Add(Guid.Parse("341dbffb-2733-4d0c-b325-cdaba1356806"));   //Package I - RT Specialty-CA 
                lCaseTypes.Add(Guid.Parse("b3acdcd3-d67e-4b88-95c5-8e52436e1fe7"));   //Property - RT Specialty-CA 
                lCaseTypes.Add(Guid.Parse("6a476ddd-ba81-4cf0-b131-a250a7e8bab2"));   //Property I - RT Specialty-CA
                lCaseTypes.Add(Guid.Parse("01378244-6434-47fa-b1a1-45e93fbd5db0"));   //Garage Package - RT Speciality-MO
                lCaseTypes.Add(Guid.Parse("a56afed3-a76f-4928-aaf7-0c452e6bbe83"));   //Garage Package I - RT Speciality-MO
                lCaseTypes.Add(Guid.Parse("61ec459d-a905-48d8-bbdd-35e86a0351c3"));   //Package - RT Specialty-MO
                lCaseTypes.Add(Guid.Parse("67fbb90c-b907-40a5-8682-d0b32e000bd8"));   //Property - RT Specialty-MO
                lCaseTypes.Add(Guid.Parse("65a7baf4-d61f-4986-9ef7-53f7617317f3"));   //Property I - RT Specialty-MO
                lCaseTypes.Add(Guid.Parse("d236aa11-6176-4f78-83ad-f19c6e04cd7a"));   //Package I - RT Specialty-MO
                lCaseTypes.Add(Guid.Parse("cb669b67-fa85-4edf-8cc4-987df6ccdcc3"));   //Residential Exterior w/MSB & update - AmWins-Access Ins. Services 7301
                lCaseTypes.Add(Guid.Parse("6bf21d04-9aa9-41d2-9cbe-6ad87e7d1856"));   //7450-Property - CrossCover 
                //lCaseTypes.Add(Guid.Parse(""));   //
                //lCaseTypes.Add(Guid.Parse(""));   //
                //lCaseTypes.Add(Guid.Parse(""));   //
                //lCaseTypes.Add(Guid.Parse(""));   //
                //lCaseTypes.Add(Guid.Parse(""));   //
                //lCaseTypes.Add(Guid.Parse(""));   //
                //lCaseTypes.Add(Guid.Parse(""));   //
                //lCaseTypes.Add(Guid.Parse(""));   //





                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                int iCaseNum;
                int iCustNum;
                Guid guCaseType;
                Guid guCaseTypeRet;
                Guid guCaseID;
                string sCustNum = "";
                string sInsured;
                string sPolicyNum;
                string sStreetAdd;
                string sCity;
                string sState;
                string sZip;
                decimal dCoverageA;
                int iCoverageA;
                string curPolNum = "";
                bool bCreateVal = false;

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");
                oLU.WritetoLog(sDateDisp);

                // Set up SQL 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetRCTOrderedByDate"; // works for RCT and Commercial
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();

                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate360);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate360);
                sqlConn1.Open();

                // loop through cases
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through cases
                    do
                    {

                        //c.CaseNumber, cu.LookupID, c.OrderedType, c.PolicyNumber, c.PolicyHolderName, a.Line1, a.City,p.StateAbbreviation, p.PostalCodeValue, c.CaseID

                        // case #
                        iCaseNum = (int)sqlReader.GetSqlInt32(0);

                        oLU.WritetoLog("Processing: " + iCaseNum.ToString());

                        // cust #
                        sCustNum = sqlReader.GetSqlString(1).ToString();
                        if (sCustNum.Length < 1)
                        {
                            iCustNum = 0;
                        }
                        else
                        {
                            iCustNum = (int)sqlReader.GetSqlString(1).ToSqlInt32();
                        }

                        // case type 
                        guCaseType = (Guid)sqlReader.GetSqlGuid(2);

                        // Policy
                        sPolicyNum = sqlReader.GetSqlString(3).ToString();

                        // Insured
                        sInsured = sqlReader.GetSqlString(4).ToString();
                        if (sInsured.Length > 49)   // 50 char max insured name
                        {
                            sInsured = sInsured.Substring(0, 49);
                        }

                        // Address
                        sStreetAdd = sqlReader.GetSqlString(5).ToString();

                        // City
                        sCity = sqlReader.GetSqlString(6).ToString();

                        // State
                        sState = sqlReader.GetSqlString(7).ToString();

                        // Zip
                        sZip = sqlReader.GetSqlString(8).ToString();

                        // case id 
                        guCaseID = (Guid)sqlReader.GetSqlGuid(9);

                        // Coverage A
                        dCoverageA = (decimal)sqlReader.GetSqlMoney(10);
                        iCoverageA = Convert.ToInt32(dCoverageA);

                        // case type (returned)
                        guCaseTypeRet = (Guid)sqlReader.GetSqlGuid(11);

                        bCreateVal = false;

                        // Handle missing customer number
                        if (iCustNum > 0)
                        {
                            // if customer is in list 1 - (no generic field)
                            if (lCustAcnts.IndexOf(iCustNum) != -1)
                            {

                                // if case type is in list
                                if (lCaseTypes.IndexOf(guCaseType) != -1 || lCaseTypes.IndexOf(guCaseTypeRet) != -1)
                                {
                                    bCreateVal = true;
                                }

                            }
                            // if case type is in list 2 - (uses old generic field)
                            //Type Yes to include a Building Valuation (ITV, TIV, MSB) Additional Fee Applies
                            if (lCheckGF.IndexOf(iCustNum) != -1)
                            {
                                // if case type is in list
                                //if (lCaseTypes.IndexOf(guCaseType) != -1 || lCaseTypes.IndexOf(guCaseTypeRet) != -1)
                                //{
                                // check generic field for ITV
                                if (GetITV(iCaseNum))
                                    bCreateVal = true;
                                //}
                            }

                            // if case type is in list 3 - (uses new generic field)
                            //Additional fee add ons: 
                            //"Check this box for a valuation" - AMWins 7437 only
                            if (lCheckGFNew.IndexOf(iCustNum) != -1)
                            {

                                // removed 9/28/2020
                                //// if case type is in list
                                //if (lCaseTypes.IndexOf(guCaseType) != -1 || lCaseTypes.IndexOf(guCaseTypeRet) != -1)
                                //{
                                // check generic field for Valuation

                                // AMWins uses "Check this box for a valuation"
                                if (iCustNum == 7437)
                                {
                                    if (GetValuation7437(iCaseNum))
                                        bCreateVal = true;
                                }
                                else
                                {
                                    if (GetValuation(iCaseNum))
                                        bCreateVal = true;
                                }
                            }
                        }   // iCustNum < 0

                        if (bCreateVal)
                        {
                            sbEmail.Append("Creating valuation for Case# " + iCaseNum.ToString() + "\r\n\r\n");
                            oLU.WritetoLog("Creating valuation for Case# " + iCaseNum.ToString());

                            try
                            {
                                // create minimal valuation
                                // returns 0 on failure - throws exception if error occurred
                                CEAddValuationMin nmAddValuationMin = new CEAddValuationMin();
                                long newValuationId = nmAddValuationMin.CEAddValuationMinimum(sInsured, sStreetAdd, sCity, sState, sZip);

                                if (newValuationId != 0)
                                {
                                    // get the valuation just created
                                    CEGetValuation nmGetValuation = new CEGetValuation();
                                    curPolNum = nmGetValuation.CEGetValuationbyId(Convert.ToInt32(newValuationId));

                                    // Update valuation to set the policy # to the case # 
                                    CEUpdateValuation nmUpdateValuation = new CEUpdateValuation();
                                    nmUpdateValuation.UpdateValuation(iCaseNum.ToString(), nmGetValuation.ReturnGetValuationResponse());
                                }
                                else
                                {
                                    throw new Exception("Create valuation returned 0");
                                }

                                oLU.WritetoLog("Valuation created for: " + iCaseNum.ToString());

                            }
                            catch (Exception ex)
                            {
                                //record exception 
                                mbErr = true;
                                string sErr = "Error creating valuation: " + iCaseNum.ToString() + System.Environment.NewLine + ex.Message;
                                sbEmail.Append(sErr);
                                oLU.WritetoLog(sErr);
                            }

                            iCount++;
                        }


                    } while (sqlReader.Read());     // cases ordered

                }   // has rows

                if (iCount == 0)
                {
                    sbEmail.Append("No cases to process.\r\n\r\n");
                }

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;
            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                sbEmail.Append(System.Environment.NewLine + ex.Message + System.Environment.NewLine);
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(sbEmail.ToString());
                }
                else
                {
                    sendEmail(sbEmail.ToString());
                }

            }
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;
        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {
            DateTime dRet = dDate;
            dRet = dRet.AddDays(-(dRet.Day - 1));
            return dRet;
        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {
            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com; kim@sibfla.com";
            oMail.MsgSubject = sEmailSubject;
            oMail.MsgBody = sbEmail.ToString();
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                sbEmail.Append(System.Environment.NewLine + "Error in module SendMail: " + sRet + System.Environment.NewLine);
                mbErr = true;
            }
        }

        static void sendErrEmail(string sMsgBody)
        {
            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by CL_DailyCreate Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;
        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

        static bool GetITV(int iCaseNum)
        {

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            string sITV = "";
            bool bRetVal = false;

            try
            {

                // Get ITV generic field
                // Type Yes to include a Building Valuation (ITV, TIV, MSB) Additional Fee Applies
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.CommandText = "sp_GetCaseITV";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", iCaseNum);
                sqlConn1.Open();

                object oRetVal = sqlCmd1.ExecuteScalar();

                if (oRetVal != null)
                {
                    sITV = (string)oRetVal;
                }

                sITV = sITV.ToUpper();
                if (sITV.Contains("YES"))
                {
                    bRetVal = true;
                }

            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

            finally
            {

                // close objects
                if (sqlConn1 != null)
                    sqlConn1.Close();
            }

            return bRetVal;
        }

        static bool GetValuation(int iCaseNum)
        {

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            string sITV = "";
            bool bRetVal = false;

            try
            {

                // Check generic field "Additional fee add ons: " for "Valuation"
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.CommandText = "sp_GetCaseValuation";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", iCaseNum);
                sqlConn1.Open();

                object oRetVal = sqlCmd1.ExecuteScalar();

                if (oRetVal != null)
                {
                    sITV = (string)oRetVal;
                }

                sITV = sITV.ToUpper();
                if (sITV.Contains("VALUATION"))
                {
                    bRetVal = true;
                }

            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

            finally
            {

                // close objects
                if (sqlConn1 != null)
                    sqlConn1.Close();
            }

            return bRetVal;
        }

        static bool GetValuation7437(int iCaseNum)
        {

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            string sITV = "";
            bool bRetVal = false;

            try
            {

                // Check generic field "Valuation" for "Check this box for a valuation"
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.CommandText = "sp_GetCaseITVChkBox";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", iCaseNum);
                sqlConn1.Open();

                object oRetVal = sqlCmd1.ExecuteScalar();

                if (oRetVal != null)
                {
                    sITV = (string)oRetVal;
                }

                sITV = sITV.ToUpper();
                if (sITV.Contains("VALUATION"))
                {
                    bRetVal = true;
                }

            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

            finally
            {

                // close objects
                if (sqlConn1 != null)
                    sqlConn1.Close();
            }

            return bRetVal;
        }

        static int getTimeZoneOffset()
        {
            SqlConnection sqlConn = null;
            SqlCommand sqlCmd = null;
            int iRetVal = 0;

            try
            {
                //string sQuery = @"SELECT dbo.TimeZoneOffset AS TZOffset;";
                sqlConn = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd = new SqlCommand("SELECT dbo.TimeZoneOffset()" , sqlConn);
                sqlConn.Open();
                Int32 functionResult = (Int32)sqlCmd.ExecuteScalar();

                iRetVal =  functionResult;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }
            finally
            {

                // close objects
                if (sqlConn != null)
                    sqlConn.Close();
            }

            return iRetVal;
        }
    }
}
