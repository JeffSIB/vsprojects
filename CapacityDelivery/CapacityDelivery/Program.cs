﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Word = Microsoft.Office.Interop.Word;
using WinSCP;


namespace CapacityDelivery
{
    class Program
    {

        static void Main(string[] args)
        {
            Setup setup = new Setup();
        }


        /// <summary>
        /// Instantiate properties from the AppSettings located in the App.config.
        /// </summary>
        public class Setup
        {

            private string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
            private string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
            private string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            private string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
            private string cfg_recroot = ConfigurationManager.AppSettings["RecRoot"];
            private string cfg_FTPRoot = ConfigurationManager.AppSettings["FTPRoot"];
            private string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
            private string cfg_exportapp = ConfigurationManager.AppSettings["ExportApp"];
            private string cfg_recapp = ConfigurationManager.AppSettings["RecApp"];
            private string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];

            private LogUtils.LogUtils oLU;
            private bool bErr = false;
            private string sPolicy = "";
            private string sCaseNum = "";
            private Guid guCaseID;
            private string sRefNum = "";
            private string sCaseType = "";
            private string sDeliveryDate = "";
            private string sBatchNum = "";

            //Used for email message body.  
            private StringBuilder sbEmail = new StringBuilder();

            //Used for transmittal message body.  
            private StringBuilder sbmsgBody = new StringBuilder();

            public Setup()
            {
                int iCasesExported = 0;

                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                bool bExport = true;
                bool bFTPFiles = true;

                DirectoryInfo diPDF;
                //DirectoryInfo diCaseFiles;

                FileInfo[] fiPDFFiles;
                oLU.WritetoLog("Begin Processing  " + DateTime.Now.ToString());
                sbEmail.Append("Begin Processing  " + DateTime.Now.ToString() + "\r\n\r\n");

                //***********************************************************
                // Export PDF's from 360
                if (bExport)
                {
                    try
                    {

                        // Export PDF's from 360
                        oLU.WritetoLog(" Begin exporting completed cases from 360");
                        sbEmail.Append("---- Begin exporting completed cases from 360\r\n\r\n");
                        oLU.closeLog();

                        oLU.OpenLog();
                        oLU.WritetoLog(cfg_exportapp);

                        var proc = Process.Start(cfg_exportapp);
                        proc.WaitForExit();
                        var exitCode = proc.ExitCode;
                        sbEmail.Append("Exporter returned: " + exitCode.ToString() + "\r\n");

                        // exit code should be the number of cases exported
                        // 9/21 JK - exporter does not appear to be returning number of cases exported
                        iCasesExported = exitCode;

                        Console.WriteLine(exitCode.ToString());

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR IN EXPORT:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                        oLU.closeLog();

                        oLU.OpenLog();

                    }

                    diPDF = new DirectoryInfo(cfg_pdfroot);
                    fiPDFFiles = diPDF.GetFiles("*.pdf");

                    //if (fiPDFFiles.Count() != iCasesExported)
                    //{
                    //    sbEmail.Append("**** PDF count does not match number exported ****\r\n");
                    //    sbEmail.Append("Exported: " + iCasesExported.ToString() + " - In folder: " + fiPDFFiles.Count().ToString() + "\r\n\r\n");
                    //}

                }

                //***********************************************************
                // Rename PDF's 
                // FTP contents of FTP folder
                if (bFTPFiles && !bErr)
                {

                    //*******************
                    // Rename files in PDF folder, move to FTP folder
                    // File name = OrderReferenceNumber.PDF
                    // 
                    // OrderReferenceNumber is generic field 

                    string sOutputFileName = "";

                    try
                    {

                        // Delete all files in FTP folder.
                        string[] sFiles = Directory.GetFiles(cfg_FTPRoot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);

                        // get list of files in PDF folder
                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        // for each PDF 
                        foreach (FileInfo pdfFile in fiPDFFiles)
                        {

                            // Get case # from file name
                            sCaseNum = pdfFile.Name.Substring(0, pdfFile.Name.IndexOf("."));

                            // Get Reference Number
                            if (!GetCaseInfo(sCaseNum))
                            {
                                sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("GetInfo failed for case: " + sCaseNum);
                            }

                            if (sRefNum == "")
                            {
                                sbEmail.Append("No Order Reference Number for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("No Order Reference Number for case: " + sCaseNum);
                            }

                            // Build Report file name
                            sOutputFileName = sRefNum + ".pdf";

                            sbEmail.Append("Filename for for case: " + sCaseNum + "  " + sRefNum + "\r\n\r\n");

                            //// replace any illegal characters with "-"
                            //foreach (char c in sInvalidChars)
                            //{
                            //    sOutputFileName = sOutputFileName.Replace(c, '-');
                            //}

                            // Copy PDF to FTP folder under new name
                            pdfFile.CopyTo(cfg_FTPRoot + sOutputFileName);

                        }   // Rename PDF's
                    }
                    catch (Exception ex)
                    {
                        bErr = true;
                        sbEmail.Append("**** Error renaming files" + "\r\n" + ex.Message + "\r\n\r\n");
                    }   // Rename PDF's

                    //////////////////////////////
                    // FTP all files in FTP folder
                    //////////////////////////////
                    if (!bErr)
                    {
                        int iNumUploaded = 0;
                        int iNumToUpload = 0;

                        diPDF = new DirectoryInfo(cfg_FTPRoot);
                        fiPDFFiles = diPDF.GetFiles("*.*");

                        iNumToUpload = fiPDFFiles.Count();
                        if (iNumToUpload == 0)
                        {
                            sbEmail.Append("\r\n\r\n---- No Files to FTP\r\n");
                        }
                        else
                        {
                            sbEmail.Append("\r\n\r\n---- Begin FTP transfer\r\n");
                            sbEmail.Append("\r\n\r\n" + iNumToUpload.ToString() + " files to upload\r\n");
                            try
                            {
                                // Setup session options
                                SessionOptions sessionOptions = new SessionOptions
                                {
                                    Protocol = Protocol.Ftp,
                                    HostName = "transfer.macneillgroup.com",
                                    UserName = "sib",
                                    Password = "S8Nq_h"
                                };

                                using (Session session = new Session())
                                {

                                    session.SessionLogPath = @"c:\automationlogs\Capacity\Export\FTPLogs.txt";

                                    // Connect
                                    session.Open(sessionOptions);

                                    // Upload files
                                    TransferOptions transferOptions = new TransferOptions();
                                    transferOptions.TransferMode = TransferMode.Binary;
                                    TransferOperationResult transferResult;
                                    transferResult = session.PutFiles(cfg_FTPRoot + "*", "Capacity_Report/", true, transferOptions);

                                    // Throw on any error
                                    transferResult.Check();

                                    // Print results
                                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                                    {
                                        oLU.WritetoLog(transfer.FileName + " uploaded");
                                        iNumUploaded++;
                                    }

                                }

                                sbEmail.Append("\r\n" + iNumUploaded.ToString() + " files uploaded\r\n\r\n---- End FTP transfer\r\n");
                            }
                            catch (Exception ex)
                            {
                                bErr = true;
                                sbEmail.Append("**** Error in FTP Transfer\r\n\r\n" + ex.Message + "\r\n\r\n");
                            }

                        }
                    }

                    // Done - write to log, send email  and clean up
                    sbEmail.Append("\r\nEnd Processing  " + DateTime.Now.ToString() + "\r\n\r\n");
                    oLU.WritetoLog(sbEmail.ToString());
                    oLU.closeLog();

                    if (bErr)
                    {
                        sendLogEmail("**** ERROR RECORDED - Capacity Delivery Processing ****", sbEmail.ToString());
                    }
                    else
                    {
                        sendLogEmail("Capacity Delivery Processing", sbEmail.ToString());
                    }
                }

            }   //if FTP



            private void sendLogEmail(string sSubject, string sBodyText)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "noreply@sibfla.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;
                }
            }


            private bool GetCaseInfo(string sCaseNum)
            {

                bool bRetVal = false;

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseInfo_CaseNum";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {
                            guCaseID = sqlReader.GetGuid(1);

                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }

                    // IR FileNum
                    sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                    sqlCmd1.Parameters.AddWithValue("@fieldname", "Order Reference Number");

                    sRefNum = "";
                    object oRetVal = sqlCmd1.ExecuteScalar();

                    if (oRetVal != null)
                    {
                        sRefNum = (string)oRetVal;
                    }

                    if (sRefNum == "")
                    {
                        sbEmail.Append("\r\n" + "No Reference Num for " + sCaseNum + " \r\n");
                        bErr = true;
                    }

                    bRetVal = true;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();

                }

                return bRetVal;
            }

        }
    }
}
