﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RCT4Utils.RCT_ValuationService;
using System.ServiceModel;

namespace RCT4Utils
{
    public class RCT4
    {
        public string APIUserName { get; set; }
        public string APIPassword { get; set; }
        public string UserLogin { get; set; }
        public string Policy { get; set; }
        public string ValuationUID { get; set; }
        public string InsuredName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public int CoverageAIn { get; set; }


        public string CreateValuation()
        {
            string sRetVal = "";

            try
            {
                ChannelFactory<ValuationService> channelFactory = new ChannelFactory<ValuationService>("ValuationServiceEndpoint");
                channelFactory.Credentials.UserName.UserName = APIUserName;
                channelFactory.Credentials.UserName.Password = APIPassword;
                ValuationService valuationService = channelFactory.CreateChannel();

                // Establish FastTrackRequest
                FastTrackRequest getFastTrackRequest = new FastTrackRequest();
                getFastTrackRequest.UserLogin = UserLogin;  // Specify UserLogin of user to be logged into RCT Express
                //getFastTrackRequest.ValuationIdentifier = new ValuationIdentifier();
                //getFastTrackRequest.ValuationIdentifier.PolicyNumber = "UID5"; //Specify the valuation's unique identifier
                //getFastTrackRequest.FirstName = "New";   //optional
                //getFastTrackRequest.LastName = "User";  //optional
                //getFastTrackRequest.AgencyName = "NewTestAgency";   //only required when creating a new user or agency
                getFastTrackRequest.Operation = FastTrackOperationCode.Edit;
                getFastTrackRequest.Role = "Agent";     //only required when creating a new user             
                getFastTrackRequest.RedirectURL = "http://www.sibfla.com";

                //Create a valuation with the valid 5 inputs
                ValuationIn valuationIn = new ValuationIn();
                valuationIn.ValuationIdentifier = new ValuationIdentifier();
                valuationIn.ValuationIdentifier.PolicyNumber = ValuationUID; //Specify the valuation's unique identifier
                valuationIn.ValuationDetails = new ValuationDetails();
                valuationIn.ValuationDetails.OwnerUser = UserLogin;
                valuationIn.ValuationDetails.UpdateUser = UserLogin;
                valuationIn.ValuationDetails.Policy = new Policy();
                valuationIn.ValuationDetails.Policy.CurrentCoverage = CoverageAIn; //Coverage A
                valuationIn.ValuationDetails.PropertyAddress = new PropertyAddress();
                valuationIn.ValuationDetails.PropertyAddress.Address1 = StreetAddress;
                valuationIn.ValuationDetails.PropertyAddress.City = City;
                valuationIn.ValuationDetails.PropertyAddress.StateProvince = State;
                valuationIn.ValuationDetails.PropertyAddress.ZipPostalCode = PostalCode;
                valuationIn.ValuationDetails.PolicyHolder = new PolicyHolder();
                valuationIn.ValuationDetails.PolicyHolder.InsuredFullName = InsuredName;

                //valuationIn.ValuationDetails.Building = new Building();
                //valuationIn.ValuationDetails.Building.HomeStyle = "Style.Story2";
                //valuationIn.ValuationDetails.Building.NumberOfFamilies = 1;
                //valuationIn.ValuationDetails.Building.MainHome = new Section();
                //valuationIn.ValuationDetails.Building.MainHome.YearBuilt = 1980;
                //valuationIn.ValuationDetails.Building.MainHome.NoOfStories = 2;
                //valuationIn.ValuationDetails.Building.MainHome.LivingArea = 4697;
                //valuationIn.ValuationDetails.Building.MainHome.FinishedLivingArea = 4697;

                // Enables the CoreLogic Knowledge Table Assumptions
                SupplementalDataRequest supplementalDataRequest = new SupplementalDataRequest();
                supplementalDataRequest.Assumptions = true;

                // ExpressLync4 ValutionService web service call to web method GetFastTrackURL:
                FastTrackResult getFastTrackResult = valuationService.GetFastTrackURL(getFastTrackRequest, valuationIn, supplementalDataRequest);

                // Display URL on writeline
                //Console.WriteLine("FastTrackURL: {0}", getFastTrackResult.FastTrackURL);

                // Open url in default browser
                //System.Diagnostics.Process.Start(getFastTrackResult.FastTrackURL);

                return getFastTrackResult.FastTrackURL;
            }
            catch (FaultException<ExpressLyncFault> faultException)
            {
                // ExpressLyncFault contains a collection of errors
                sRetVal = "**ERROR** ";

                foreach (ExpressLyncError error in faultException.Detail.Errors)
                {
                    // Each RctError contains error code and description
                    sRetVal += error.ErrorCode + " " + error.ErrorDescription;
                }
            }
            catch (Exception ex)
            {
                // Any other exceptions
                sRetVal = "**ERROR** " + ex.Message;
            }

            return sRetVal;
        }


    }
}
