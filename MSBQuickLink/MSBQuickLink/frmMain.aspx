﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmMain.aspx.cs" Inherits="MSBQuickLink.frmMain" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Sutton Inspection Bureau</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript">

        function cancelForm() {
            window.opener = top;
            redirectTimerId = window.setTimeout('redirect()', 2000);
            window.close();
        }

        function SeamlessLoginToRCT() {

            var polnum = frmMain.hiMode.value;

            frmMain.method = "POST";

            frmMain.action = "https://rct3.msbexpress.net/inspectorstd/autologin.asp?policyNum=" + polnum + "&operation=edit";
            //frmMain.action = "https://rct3.msbexpress.net/inspectorstd/autologin.asp?policyNum=" + HFL00178100 + "&operation=view";
            frmMain.submit();
        }

    </script>

</head>
<body>
    <form id="frmMain" runat="server">
        <div>
            <div id="div1" runat="server" style="left: 10px; width: 700px; position: absolute">
                <table style="width: 700px; border-bottom: gray thin solid">
                    <tr>
                        <td style="width: 600px">
                            <asp:Label ID="lblTitle" runat="server" CssClass="StdTextGreySmallCap">
								&nbsp;Sutton RCT Express FastTrack
                            </asp:Label>
                        </td>
                        <td style="width: 100px; text-align: center;">
                            <input class="StdTextSmall" onclick="cancelForm()" tabindex="500" type="button" value="Cancel"
                                name="bCancel" />
                        </td>
                    </tr>
                </table>
                <table id="Table1" style="width: 700px; border-right: #c0c0c0 thin solid; border-top: #c0c0c0 thin solid; border-left: #c0c0c0 thin solid; border-bottom: #c0c0c0 thin solid; border: 0; border-spacing: 0; padding: 2px"
                    runat="server">
                    <tr style="height: 25px; background-color: #e4ffe4">
                        <td style="width: 700px" class="StdTextSmall">
                            <asp:Label ID="lblTitle2" runat="server" CssClass="StdTextSmall">
								&nbsp;Sutton RCT Express Login
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 700px; border: 0; border-spacing: 0; padding: 2px">
                                <tr>
                                    <td style="width: 50px"></td>
                                    <td style="width: 650px"></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left" colspan="2">
                                        <asp:Label ID="Label2" runat="server" CssClass="StdTextSmallGrey">
				                        <p>This process will log you into the MSB Valuation for the current inspection on the RCT Express web site.</p>
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                    </td>
                                    <td style="text-align: left; vertical-align: middle" class="StdTextSmall">
                                        <input class="StdTextSmall" onclick="SeamlessLoginToRCT()" tabindex="500" type="button" value="Continue to RCT Express"
                                            name="bRCT" />
                                        <asp:Button ID="bSubmit" runat="server" Text="Launch Valuation" OnClick="bSubmit_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div>


        </div>

       <input id="hiMode" type="hidden" runat="server" name="hiMode" />
    </form>
</body>
</html>
