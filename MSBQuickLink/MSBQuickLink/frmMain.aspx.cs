﻿using System;
using System.Web.UI;
using System.Configuration;
using System.ServiceModel;
using MSBQuickLink.CE_SecurityService;

namespace MSBQuickLink
{
    public partial class frmMain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string sMode = Request.QueryString["val"];
                
                //hiMode.Value = sMode;

            }
        }

        protected void bSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //setup the SOAP Security header
                ChannelFactory<ISecurity> channelFactory = new ChannelFactory<ISecurity>("ClearUsernameBinding_ISecurity");
                string contextUserName = ConfigurationManager.AppSettings["UserName"];
                string contextCompanyAlias = ConfigurationManager.AppSettings["CompanyAlias"];
                string contextUserPassword = ConfigurationManager.AppSettings["Password"];
                Guid companyID = new Guid(ConfigurationManager.AppSettings["CompanyID"]);
                string contextUserID = string.Format("{0}@{1}", contextUserName, contextCompanyAlias);
                channelFactory.Credentials.UserName.UserName = contextUserID;
                channelFactory.Credentials.UserName.Password = contextUserPassword;

                //create the GetQuickLinkRequest object
                GetQuickLinkRequest getQuickLinkRequest = new GetQuickLinkRequest();
                getQuickLinkRequest.CompanyID = companyID;
                getQuickLinkRequest.Logon = "SIBDemo";

                //create the QuickLinkInfo object
                getQuickLinkRequest.QuickLinkInfo = new QuickLinkInfo();
                getQuickLinkRequest.QuickLinkInfo.AfterRecordAccessAction = "Logout";
                getQuickLinkRequest.QuickLinkInfo.GroupName = "SuttonInspectionParent";
                getQuickLinkRequest.QuickLinkInfo.LogonExpires = new System.DateTime(2020, 10, 26, 21, 32, 52);
                getQuickLinkRequest.QuickLinkInfo.Role = "AD";
                getQuickLinkRequest.QuickLinkInfo.UserLogon = "SIBDemo";
                getQuickLinkRequest.QuickLinkInfo.PolicyNumber = "1024";
                getQuickLinkRequest.QuickLinkInfo.FirstName = "Sutton";
                getQuickLinkRequest.QuickLinkInfo.LastName = "Inspection";

                //serialize the XML request for troubleshooting
                System.IO.FileStream file = System.IO.File.Create(@"c:\temp\GetQuickLink_Request.xml");
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(getQuickLinkRequest.GetType());
                x.Serialize(file, getQuickLinkRequest);

                //create the connection to Commercial Express and send the request.
                ISecurity iSecurity = channelFactory.CreateChannel();
                ResponseGetQuickLink responseGetQuickLink = iSecurity.GetQuickLink(getQuickLinkRequest);

                //serilaize the XML response for troubleshooting
                System.IO.FileStream file1 = System.IO.File.Create(@"c:\temp\GetQuickLink_Response.xml");
                System.Xml.Serialization.XmlSerializer y = new System.Xml.Serialization.XmlSerializer(responseGetQuickLink.GetType());
                y.Serialize(file1, responseGetQuickLink);

                //extract the single use token URL from the response
                string launchURL = responseGetQuickLink.Url;

                //launch the URL in a browser
                System.Diagnostics.Process.Start(responseGetQuickLink.Url);
            }

            catch (Exception ex)
            {
                string sErr = ex.Message;
                //oLU.closeLog();
                //sendErrEmail("Error initializing importAmRisc\r\n\r\n" + ex.Message);                
                return;
            }
        }
    }
}