﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmQLaunch.aspx.cs" Inherits="MSBQuickLink.frmQLaunch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sutton Inspection Bureau</title>
    <link href="Site.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript">

        function cancelForm1() {
            alert("Close");
            //window.opener = top;
            //redirectTimerId = window.setTimeout('redirect()', 2000);
            window.close();
        }
         

    </script>
</head>
<body>
    <form id="frmMain" runat="server">
            <div class="center">
                <div id="div1" runat="server" style="width: 100%;">
                    <table style="width: 100%; border-bottom: gray thin solid">
                        <tr>
                            <td style="width: 80%">
                                <asp:Label ID="lblTitle" runat="server" CssClass="StdTextGreySmallCap">
								&nbsp;Sutton Inspection Bureau Valuation Portal
                                </asp:Label>
                            </td>
                            <td style="width: 20%; text-align: center;">
                                &nbsp;<asp:Button ID="bClose" runat="server" OnClick="Button1_Click" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                    <table id="tbl1" style="width: 100%; border-right: #c0c0c0 thin solid; border-top: #c0c0c0 thin solid; border-left: #c0c0c0 thin solid; border-bottom: #c0c0c0 thin solid; border: 0; border-spacing: 0; padding: 2px"
                        runat="server">
                        <tr style="height: 25px; background-color: #e4ffe4">
                            <td style="width: 700px" class="StdText">
                                <asp:Label ID="lblTitle2" runat="server" CssClass="StdText">
								&nbsp;Sutton MSB Express Login
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width: 100%; border: 0; border-spacing: 0; padding: 2px">
                                    <tr>
                                        <td style="width: 10%"></td>
                                        <td style="width: 90%"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" colspan="2">
                                            <asp:Label ID="Label2" runat="server" CssClass="StdTextSmallGrey">
				                        <p>This process will log you into the MSB Valuation for the current inspection.</p>
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="lblStatus" runat="server" CssClass="StdText">Please wait...
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        <input id="hiMode" type="hidden" runat="server" name="hiMode" />
    </form>

</body>
</html>
