﻿using System;
using System.Web.UI;
using System.Configuration;
using System.ServiceModel;
using System.Net;
using System.Data.SqlClient;
using System.Data;
using MSBQuickLink.CE_SecurityService;

namespace MSBQuickLink
{
    public partial class frmQLaunch : System.Web.UI.Page
    {
        static string sVal = "";
        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;

        private int iCaseNum;
        private string sInspNum;
        private string sInspFName;
        private string sInspLName;

        protected void Page_Load(object sender, EventArgs e)
        {

            string sVal = "";
            string sCaseInfo = "";

            try
            {
                if (!Page.IsPostBack)
                {
                    sVal = Request.QueryString["val"];

                    // load configuration values from app.config
                    System.Collections.Specialized.NameValueCollection colNameVal;
                    colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                    cfg_smtpserver = colNameVal.Get("smtpserver");
                    cfg_logfilename = colNameVal.Get("logfilename");
                    cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");

                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    WriteToLog("Preparing link for case#: " + sVal);

                    if (sVal.Length < 1)
                    {
                        lblStatus.Text = "Unable to load valuation - please contact Sutton Support.";
                        WriteToLog("**** NO VALUATION PASSED ****");
                        throw new ApplicationException("**** NO VALUATION PASSED ****");
                    }

                    bool bResult = int.TryParse(sVal, out iCaseNum);
                    if (!bResult || !(iCaseNum > 92000 && iCaseNum < 11000000))
                    {
                        lblStatus.Text = "Unable to load valuation - please contact Sutton Support.";
                        WriteToLog("**** INVALID CASE # PASSED ****");
                        throw new ApplicationException("**** INVALID CASE # PASSED ****");
                    }

                    // Get inspector info
                    sCaseInfo = getCaseInfo(iCaseNum);
                    if (sCaseInfo.Length > 0)
                    {
                        throw new ApplicationException(sCaseInfo);
                    }

                    //setup the SOAP Security header
                    ChannelFactory<ISecurity> channelFactory = new ChannelFactory<ISecurity>("ClearUsernameBinding_ISecurity");
                    string contextUserName = ConfigurationManager.AppSettings["UserName"];
                    string contextCompanyAlias = ConfigurationManager.AppSettings["CompanyAlias"];
                    string contextUserPassword = ConfigurationManager.AppSettings["Password"];
                    Guid companyID = new Guid(ConfigurationManager.AppSettings["CompanyID"]);
                    string contextUserID = string.Format("{0}@{1}", contextUserName, contextCompanyAlias);
                    channelFactory.Credentials.UserName.UserName = contextUserID;
                    channelFactory.Credentials.UserName.Password = contextUserPassword;

                    //create the GetQuickLinkRequest object
                    GetQuickLinkRequest getQuickLinkRequest = new GetQuickLinkRequest();
                    getQuickLinkRequest.CompanyID = companyID;


                    //****************************
                    getQuickLinkRequest.Logon = contextUserName;
                    //****************************


                    DateTime dLoginExp = new DateTime();
                    dLoginExp = DateTime.Today.AddDays(21);

                    //create the QuickLinkInfo object

                    // **** PRODUCTION 9/10/2020 ****
                    getQuickLinkRequest.QuickLinkInfo = new QuickLinkInfo();
                    getQuickLinkRequest.QuickLinkInfo.AfterRecordAccessAction = "Logout";
                    getQuickLinkRequest.QuickLinkInfo.GroupName = "Sutton Inspection Bureau";
                    getQuickLinkRequest.QuickLinkInfo.LogonExpires = dLoginExp;
                    getQuickLinkRequest.QuickLinkInfo.Role = "AG";
                    getQuickLinkRequest.QuickLinkInfo.UserLogon = sInspNum;
                    getQuickLinkRequest.QuickLinkInfo.PolicyNumber = sVal;
                    getQuickLinkRequest.QuickLinkInfo.FirstName = sInspFName;
                    getQuickLinkRequest.QuickLinkInfo.LastName = sInspLName;


                    // **** OLD PRODUCTION ****
                    //getQuickLinkRequest.QuickLinkInfo = new QuickLinkInfo();
                    //getQuickLinkRequest.QuickLinkInfo.AfterRecordAccessAction = "Logout";
                    //getQuickLinkRequest.QuickLinkInfo.GroupName = "Sutton Inspection Bureau";
                    //getQuickLinkRequest.QuickLinkInfo.LogonExpires = dLoginExp;
                    //getQuickLinkRequest.QuickLinkInfo.Role = "AG";
                    //getQuickLinkRequest.QuickLinkInfo.UserLogon = "SuttonAuto";
                    //getQuickLinkRequest.QuickLinkInfo.PolicyNumber = sVal;
                    //getQuickLinkRequest.QuickLinkInfo.FirstName = "Sutton ";
                    //getQuickLinkRequest.QuickLinkInfo.LastName = "Automation";

                    //****For testing
                    //****serialize the XML request for troubleshooting
                    //System.IO.FileStream file = System.IO.File.Create(@"c:\temp\GetQuickLink_Request.xml");
                    //System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(getQuickLinkRequest.GetType());
                    //x.Serialize(file, getQuickLinkRequest);
                    //****For testing

                    //create the connection to Commercial Express and send the request.
                    ISecurity iSecurity = channelFactory.CreateChannel();
                    ResponseGetQuickLink responseGetQuickLink = iSecurity.GetQuickLink(getQuickLinkRequest);

                    //****For testing
                    //****serilaize the XML response for troubleshooting
                    //System.IO.FileStream file1 = System.IO.File.Create(@"c:\temp\GetQuickLink_Response.xml");
                    //System.Xml.Serialization.XmlSerializer y = new System.Xml.Serialization.XmlSerializer(responseGetQuickLink.GetType());
                    //y.Serialize(file1, responseGetQuickLink);
                    //****For testing


                    //extract the single use token URL from the response
                    string launchURL = responseGetQuickLink.Url;

                    //launch the URL in a browser
                    //System.Diagnostics.Process.Start(responseGetQuickLink.Url);
                    Response.Redirect(responseGetQuickLink.Url, false);

                    WriteToLog("Valuation launched. " + responseGetQuickLink.Url);
                    lblStatus.ForeColor = System.Drawing.Color.DarkGreen;
                    lblStatus.Text = "Valuation launched.";
                }
            }
            catch (Exception ex)
            {

                if (sCaseInfo.Length > 0)
                {
                    WriteToLog("Error creating valuation for: " + iCaseNum.ToString() + System.Environment.NewLine + sCaseInfo);
                    lblStatus.Text = "&nbsp; &nbsp;" + sCaseInfo + "&nbsp;&nbsp;";
                }
                else
                {
                    WriteToLog("Error creating valuation for: " + iCaseNum.ToString() + System.Environment.NewLine + ex.Message);
                    lblStatus.Text = "&nbsp;&nbsp;An unexpected error has occurred - please contact Sutton support.&nbsp;&nbsp;";
                }

                lblStatus.ForeColor = System.Drawing.Color.Maroon;

                sendErrEmail("Error creating valuation for: " + iCaseNum.ToString() + "\r\n\r\n" + ex.Message + "\r\n\r\n" + sVal);

            }

            finally
            {
                // close log
                if (oLU != null)
                    oLU.closeLog();
            }

        }

        protected void launchQL()
        {
            try
            {
                //setup the SOAP Security header
                ChannelFactory<ISecurity> channelFactory = new ChannelFactory<ISecurity>("ClearUsernameBinding_ISecurity");
                string contextUserName = ConfigurationManager.AppSettings["UserName"];
                string contextCompanyAlias = ConfigurationManager.AppSettings["CompanyAlias"];
                string contextUserPassword = ConfigurationManager.AppSettings["Password"];
                Guid companyID = new Guid(ConfigurationManager.AppSettings["CompanyID"]);
                string contextUserID = string.Format("{0}@{1}", contextUserName, contextCompanyAlias);
                channelFactory.Credentials.UserName.UserName = contextUserID;
                channelFactory.Credentials.UserName.Password = contextUserPassword;

                //create the GetQuickLinkRequest object
                GetQuickLinkRequest getQuickLinkRequest = new GetQuickLinkRequest();
                getQuickLinkRequest.CompanyID = companyID;


                //****************************
                getQuickLinkRequest.Logon = contextUserName;
                //****************************


                DateTime dLoginExp = new DateTime();
                dLoginExp = DateTime.Today.AddDays(21);

                //create the QuickLinkInfo object
                // **** DEMO ****
                //getQuickLinkRequest.QuickLinkInfo = new QuickLinkInfo();
                //getQuickLinkRequest.QuickLinkInfo.AfterRecordAccessAction = "Logout";
                //getQuickLinkRequest.QuickLinkInfo.GroupName = "SuttonInspectionParent";
                //getQuickLinkRequest.QuickLinkInfo.LogonExpires = dLoginExp;
                //getQuickLinkRequest.QuickLinkInfo.Role = "AD";
                //getQuickLinkRequest.QuickLinkInfo.UserLogon = "SIBDemo";
                //getQuickLinkRequest.QuickLinkInfo.PolicyNumber = sVal;
                //getQuickLinkRequest.QuickLinkInfo.FirstName = "Sutton";
                //getQuickLinkRequest.QuickLinkInfo.LastName = "Inspection";


                // **** PRODUCTION ****
                getQuickLinkRequest.QuickLinkInfo = new QuickLinkInfo();
                getQuickLinkRequest.QuickLinkInfo.AfterRecordAccessAction = "Logout";
                getQuickLinkRequest.QuickLinkInfo.GroupName = "Sutton Inspection Bureau";
                getQuickLinkRequest.QuickLinkInfo.LogonExpires = dLoginExp;
                getQuickLinkRequest.QuickLinkInfo.Role = "AD";
                getQuickLinkRequest.QuickLinkInfo.UserLogon = "QLUser";
                getQuickLinkRequest.QuickLinkInfo.PolicyNumber = sVal;
                getQuickLinkRequest.QuickLinkInfo.FirstName = "GetQuickLink";
                getQuickLinkRequest.QuickLinkInfo.LastName = "Sutton";

                //serialize the XML request for troubleshooting
                //System.IO.FileStream file = System.IO.File.Create(@"c:\temp\GetQuickLink_Request.xml");
                //System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(getQuickLinkRequest.GetType());
                //x.Serialize(file, getQuickLinkRequest);

                //create the connection to Commercial Express and send the request.
                ISecurity iSecurity = channelFactory.CreateChannel();
                ResponseGetQuickLink responseGetQuickLink = iSecurity.GetQuickLink(getQuickLinkRequest);

                //serilaize the XML response for troubleshooting
                //System.IO.FileStream file1 = System.IO.File.Create(@"c:\temp\GetQuickLink_Response.xml");
                //System.Xml.Serialization.XmlSerializer y = new System.Xml.Serialization.XmlSerializer(responseGetQuickLink.GetType());
                //y.Serialize(file1, responseGetQuickLink);

                //extract the single use token URL from the response
                string launchURL = responseGetQuickLink.Url;

                //launch the URL in a browser
                //System.Diagnostics.Process.Start(responseGetQuickLink.Url);
                Response.Redirect(responseGetQuickLink.Url, false);
            }

            catch (Exception ex)
            {

                string sErr = ex.Message;
                sendErrEmail("Error initializing \r\n\r\n" + ex.Message);
                return;
            }
        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            string smtpserver = colNameVal.Get("smtpserver");

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com;kim@sibfla.com";
            oMail.MsgSubject = "** Errors logged by MSBQuickLink **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        private string getCaseInfo(int iCaseNum)
        {
            int iStatus = 0;
            string sRetVal = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetCaseInfoValuation";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", iCaseNum);

                sqlConn1.Open();

                // Get case info
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through results
                    do
                    {
                        // Case status
                        iStatus = (int)sqlReader.GetSqlInt32(8);

                        //// Insp Number
                        sInspNum = sqlReader.GetSqlString(9).ToString();

                        //// Insp first name
                        sInspFName = sqlReader.GetSqlString(10).ToString();

                        ////Insp last name
                        sInspLName = sqlReader.GetSqlString(11).ToString();


                    } while (sqlReader.Read());

                    sqlReader.Close();
                    sqlConn1.Close();

                }   // has rows
                else
                {
                    sRetVal = "Unable to access valuation: the inspection number is not valid.";
                    throw new SystemException("No rows returned for: " + iCaseNum.ToString());
                }

                // break if case has not been assigned
                if (iStatus == 1)
                {
                    sRetVal = "Unable to access valuation: the inspection has not been assigned.";
                    throw new SystemException("Inspection completed: " + iCaseNum.ToString());
                }

                // break if case has been completed
                if (iStatus == 6 || iStatus == 7)
                {
                    sRetVal = "Unable to access valuation: this inspection has already been completed.";
                    throw new SystemException("Inspection completed: " + iCaseNum.ToString());
                }

                // break if inspector information incomplete
                if (sInspNum.Length < 1 || sInspFName.Length < 1 || sInspLName.Length < 1)
                {
                    sRetVal = "Unable to access valuation: the inspector information is not complete.";
                    throw new SystemException("Incomplete insp info: " + iCaseNum.ToString() + " Insp#: " + sInspNum + " FName: " + sInspFName + " LName: " + sInspLName);
                }
            }

            catch (Exception ex)
            {

                if (sRetVal.Length > 0)
                {
                    WriteToLog("Error attempting to access inspection: " + iCaseNum.ToString() + System.Environment.NewLine + ex.Message);
                    sRetVal = "&nbsp; &nbsp;" + sRetVal + "&nbsp;&nbsp;";
                }
                else
                {
                    WriteToLog("Error attempting to access inspection: " + iCaseNum.ToString() + System.Environment.NewLine + ex.Message);
                    sRetVal = "&nbsp;&nbsp;An unexpected error has occurred - please contact Sutton support.&nbsp;&nbsp;";
                }
            }

            finally
            {

                if (sqlReader != null)
                    sqlReader.Close();
                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

            return sRetVal;
        }

        private void WriteToLog(string sMessage)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();
            oLU.WritetoLog(sMessage);
            oLU.closeLog();
            oLU = null;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmQL.aspx");
        }
    }
}