﻿namespace _360Utils
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteCases = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDeleteCases
            // 
            this.btnDeleteCases.Location = new System.Drawing.Point(50, 31);
            this.btnDeleteCases.Name = "btnDeleteCases";
            this.btnDeleteCases.Size = new System.Drawing.Size(383, 41);
            this.btnDeleteCases.TabIndex = 1;
            this.btnDeleteCases.Text = "Remove cases from 360";
            this.btnDeleteCases.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDeleteCases.UseVisualStyleBackColor = true;
            this.btnDeleteCases.Click += new System.EventHandler(this.btnDeleteCases_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(482, 356);
            this.Controls.Add(this.btnDeleteCases);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frmMenu";
            this.Text = "360 Utils";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDeleteCases;
    }
}

