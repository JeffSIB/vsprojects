﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace _360Utils
{
    public partial class frmDeleteCases : Form
    {

        static LogUtils.LogUtils oLU;
        static string cfg_logfilename;
        static string cfg_360ConnStr;
        static string cfg_360UtilConnStr;

        public frmDeleteCases()
        {
            InitializeComponent();

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_logfilename = colNameVal.Get("logfilename");
            cfg_360ConnStr = colNameVal.Get("360ConnStr");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");


            tbCaseID.Select();
        }
         


        private void bDeleteCase_Click(object sender, EventArgs e)
        {
            if (tbCaseID.Text.Length < 5)
            {
                MessageBox.Show("Please enter a valid case id.","Invalid Case ID",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            SqlDataReader sqlReader = null;

            int iCaseNum = Convert.ToInt32(tbCaseID.Text);
            string sCaseID = "";
            string sPolicyNum = "";
            string sInsuredName = "";

            try
            {
                

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlConn2 = new SqlConnection(cfg_360ConnStr);
                sqlCmd2 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_Case_Lookup";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", iCaseNum);

                sqlConn1.Open();

                // Get case info
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through results
                    do
                    {

                        //case number
                        if (sqlReader.IsDBNull(0))
                        {
                            throw new SystemException("SQL returned empty Case#");
                        }
                        else
                        {
                            iCaseNum = (int)sqlReader.GetSqlInt32(0);
                        }

                        //case ID
                        sCaseID = sqlReader.GetSqlGuid(1).ToString();

                        // name
                        sInsuredName = sqlReader.GetSqlString(2).ToString();

                        // policy #
                        sPolicyNum = sqlReader.GetSqlString(3).ToString();

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }   // has rows
                else
                {
                    MessageBox.Show("Unable to locate case number: " + iCaseNum.ToString() + "\r\n\r\nPlease verify the number you entered is correct.",  "Invalid case number", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    sCaseID = "";
                }
                
               

            }

            catch (Exception ex)
            {

                string sErr = "Error attempting to access case: " + tbCaseID.Text + System.Environment.NewLine;
                sErr += "User id: " + System.Windows.Forms.SystemInformation.UserName + System.Environment.NewLine;

                //record exception  
                WriteToLog(sErr + ex.Message);

                MessageBox.Show("An error occurred while attempting to access this case.\r\nThe case cannot be deleted at this time.\r\n\r\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                sCaseID = "";
            }

            finally
            {

                if (sqlReader != null)
                    sqlReader.Close();
                
            }

            /// Confirm delete
            if (sCaseID.Length > 0)
            {

                if (MessageBox.Show("Permenantly delete the following case:\r\n\r\nCase number: " + iCaseNum.ToString() + "\r\n\r\nInsured name: " + sInsuredName + "\r\n\r\nPolicy number: " + sPolicyNum, "Delete Inspection", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Cancel)
                {
                    MessageBox.Show("Delete cancelled", "Cancel Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    tbCaseID.Text = "";
                    tbCaseID.Select();
                    return;                
                }

                try
                {

                    sqlCmd2.CommandType = CommandType.StoredProcedure;
                    sqlCmd2.Connection = sqlConn2;

                    sqlCmd2.CommandText = "DeleteCase";
                    sqlCmd2.Parameters.Clear();
                    sqlCmd2.CommandTimeout = 180;
                    sqlCmd2.Parameters.AddWithValue("@pCaseID", sCaseID);
                    sqlConn2.Open();
                    object oRet = sqlCmd2.ExecuteNonQuery();

                    MessageBox.Show("Case number " + iCaseNum.ToString() + " deleted.", "Case Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    //record delete
                    WriteToLog("Case: " + tbCaseID.Text + "deleted by user id: " + System.Windows.Forms.SystemInformation.UserName + System.Environment.NewLine);

                    //tbCaseID.Text = "";
                    tbCaseID.Select();

                }
                catch (SqlException ex)
                {

                    string sErr = "Error attempting to delete case: " + tbCaseID.Text + System.Environment.NewLine;
                    sErr += "User id: " + System.Windows.Forms.SystemInformation.UserName + System.Environment.NewLine;

                    //record exception  
                    WriteToLog(sErr + ex.Message);

                    MessageBox.Show("An error occurred while attempting to delete this case.\r\nThe case cannot be deleted at this time.\r\n\r\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    sCaseID = "";
                }

                finally
                {

                    if (sqlConn1 != null)
                        sqlConn1.Close();
                    if (sqlConn2 != null)
                        sqlConn2.Close();
                
                }
            }

        }

        private void WriteToLog(string sMessage)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();
            oLU.WritetoLog(sMessage);
            oLU.closeLog();
            oLU = null;


        }



     }
}
