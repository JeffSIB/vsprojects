﻿namespace _360Utils
{
    partial class frmDeleteCases
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tbCaseID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bDeleteCase = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(482, 60);
            this.textBox1.TabIndex = 99;
            this.textBox1.Text = "---- WARNING ----\r\nThis utility will PERMENANTLY DELETE cases from 360.\r\nOnly tes" +
                "t cases should be removed.";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbCaseID
            // 
            this.tbCaseID.Location = new System.Drawing.Point(229, 76);
            this.tbCaseID.Name = "tbCaseID";
            this.tbCaseID.Size = new System.Drawing.Size(100, 20);
            this.tbCaseID.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(154, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 99;
            this.label1.Text = "Case number";
            // 
            // bDeleteCase
            // 
            this.bDeleteCase.Location = new System.Drawing.Point(191, 115);
            this.bDeleteCase.Name = "bDeleteCase";
            this.bDeleteCase.Size = new System.Drawing.Size(100, 23);
            this.bDeleteCase.TabIndex = 1;
            this.bDeleteCase.Text = "DeleteCase";
            this.bDeleteCase.UseVisualStyleBackColor = true;
            this.bDeleteCase.Click += new System.EventHandler(this.bDeleteCase_Click);
            // 
            // frmDeleteCases
            // 
            this.AcceptButton = this.bDeleteCase;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(482, 169);
            this.Controls.Add(this.bDeleteCase);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbCaseID);
            this.Controls.Add(this.textBox1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "frmDeleteCases";
            this.Text = "Remove cases from 360";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox tbCaseID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bDeleteCase;
    }
}