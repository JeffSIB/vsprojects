﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace RCTDailyCreate
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static int cfg_TimeZoneOffset;
        static DateTime dBegDate;
        static DateTime dEndDate;
        static DateTime dBegDate360;
        static DateTime dEndDate360;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string sEmailSubject = "";
        static StringBuilder sbEmail = new StringBuilder();
        static string sMode;
        static int iCount;


        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            sMode = "";
            sbEmail.Append("");
            iCount = 0;

            try
            {

                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //  AM = run at 6:01am looking for cases created 6pm previous day to 6am current day
                //  PM = run at 6:01pm looking for cases created 6am day to 6pm current day
                //  C = custom date/time dange

                if (args.Length < 1)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                //if (args[0].ToUpper() == "PD")
                //{
                //    // create valuations for cases created the previous day 
                //    sMode = "PD";

                //    DateTime dt = new DateTime();
                //    dt = DateTime.Today;

                //    dBegDate = dt.AddDays(-1);
                //    dEndDate = dt;
                //    //dBegDate = ChangeTime(dBegDate, 0, 0, 0, 0);   // 12:00 am previous day
                //    //dEndDate = ChangeTime(dEndDate, 11, 59, 0, 0);    // 11:59 pm previous day
                //}
                if (args[0].ToUpper() == "AM")
                {
                    // run at 6:01am looking for cases created 6pm previous day to 6am current day
                    sMode = "AM";

                    DateTime dt = new DateTime();
                    dt = DateTime.Today;

                    dBegDate = dt.AddDays(-1);
                    dEndDate = dt;
                    dBegDate = ChangeTime(dBegDate, 18, 0, 0, 0);   // 6pm previous day
                    dEndDate = ChangeTime(dEndDate, 6, 0, 0, 0);    // 6am today
                }

                else if (args[0].ToUpper() == "PM")
                {

                    //  PM = run at 6:01pm looking for cases created 6am day to 6pm current day
                    sMode = "PM";

                    DateTime dt = new DateTime();
                    dt = DateTime.Today;

                    dBegDate = dt;
                    dEndDate = dt;
                    dBegDate = ChangeTime(dBegDate, 6, 1, 0, 0);   // 6:01am current day
                    dEndDate = ChangeTime(dEndDate, 18, 0, 0, 0);    // 6pm today
                }
                else if (args[0].ToUpper() == "C")
                {
                    // Custom dates/time
                    try
                    {
                        sMode = "C";
                        dBegDate = Convert.ToDateTime(args[1]);
                        dEndDate = Convert.ToDateTime(args[2]);
                        dBegDate = ChangeTime(dBegDate, 6, 1, 0, 0);   // 6:01am current day
                        dEndDate = ChangeTime(dEndDate, 18, 0, 0, 0);    // 6pm today
                    }
                    catch
                    {
                        throw new SystemException("Invalid period arguments supplied");
                    }
                }
                else
                {
                    throw new SystemException("Invalid period arguments supplied");
                }

                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                //cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));
                cfg_TimeZoneOffset = getTimeZoneOffset();

                string sDateDisp = dBegDate.ToString() + "-" + dEndDate.ToString();

                // adjust time to GMT
                dBegDate360 = dBegDate.AddHours(cfg_TimeZoneOffset);
                dEndDate360 = dEndDate.AddHours(cfg_TimeZoneOffset);

                sEmailSubject = "RCT4 valuations created for cases ordered on " + sDateDisp;

                // Customers to process
                List<int> lCustAcnts = new List<int>();
                lCustAcnts.Add(7300);
                lCustAcnts.Add(7032);
                lCustAcnts.Add(7033);   //Hull & Co.-7033- Personal - Tampa Bay
                lCustAcnts.Add(7064);
                lCustAcnts.Add(7305);   //London Underwriters
                lCustAcnts.Add(7307);   //Johnson & Johnson
                lCustAcnts.Add(7328);   //RPS-7328-GA Risk Placement Services, Inc. 
                lCustAcnts.Add(7247);   //SIU-7247- Southern Ins. Underwriters-FL-Personal
                lCustAcnts.Add(7026);   //SCU-7026- Personal - Orlando 
                lCustAcnts.Add(7301);   //AmWINS-7301-Automated-Access Insurance Services-PSL
                lCustAcnts.Add(7298);   //MacNeill-7298-Group, Inc - Personal 
                lCustAcnts.Add(7011);   //SeaCoast Underwriters-7011 - Coral Gables Office
                lCustAcnts.Add(7101);   //ABCO Insurance Underwriters
                lCustAcnts.Add(7295);   //National Risk Solutions - NRS
                lCustAcnts.Add(7205);   //Quaker Special Risk-Personal
                lCustAcnts.Add(7074);   //Shelly, Middlebrooks & O'Leary
                lCustAcnts.Add(7365);   //RMAG - Risk Management Advisory Group LLC - Personal Lines 
                lCustAcnts.Add(7292);   //Marketscout - Personal
                lCustAcnts.Add(7270);   //Elite Underwriters
                lCustAcnts.Add(7185);   //MacDuff Underwriters, Inc.
                lCustAcnts.Add(7070);   //Hull & Co.-7070-Commercial & Personal - Jax
                lCustAcnts.Add(7366);   //XS Brokers-7366

                lCustAcnts.Add(7289);   //Hull & Co.-7289- Personal - NC
                lCustAcnts.Add(7164);   //Hull & Co.-7164-Commercial & Personal - Margate
                lCustAcnts.Add(7192);   //Broward-7192- Insurance Consulting / Bass-Personal-SP 
                lCustAcnts.Add(7030);   //Broward-7030 - Insurance Consulting / Bass-Personal-Ft. Lauderdale 
                lCustAcnts.Add(7341);   //Broward-7341- Insurance Consulting / Bass-Personal-Orlando
                lCustAcnts.Add(7342);   //Broward-7342- Insurance Consulting / Bass-Personal-Gainesville
                lCustAcnts.Add(7370);   //Broward-7370 - Insurance Consulting / Bass-Personal-Atlanta 
                lCustAcnts.Add(7371);   //Broward-7371 - Insurance Consulting / Bass-Personal-Houston
                lCustAcnts.Add(7372);   //Broward-7372 - Insurance Consulting / Bass-Personal-Gulf Coast
                lCustAcnts.Add(7376);   //Broward-7376 - Insurance Consulting / Bass-Personal-New Orleans 
                lCustAcnts.Add(7377);   //Broward-7377 - Insurance Consulting / Bass-Personal-Las Vegas 
                lCustAcnts.Add(7383);   //Broward-7383- Insurance Consulting / Bass-Personal-SC 
                lCustAcnts.Add(7390);   //Halcyon Underwriters
                lCustAcnts.Add(7420);   //Cabrillo Coastal General Insurance Agency, LLC
                lCustAcnts.Add(7431);   //CRC Group - Bayport - 7431
                lCustAcnts.Add(7429);   //RPS-7429-LA Risk Placement Services
                lCustAcnts.Add(7438);   //RPS-7438-WA Risk Placement Services 
                lCustAcnts.Add(7432);   //Worldwide Facilities, LLC-7432 - NC 
                lCustAcnts.Add(7441);   //RT Specialty-7441-All Offices
                lCustAcnts.Add(7170);   //TCC Associates Inc.
                lCustAcnts.Add(7454);   //KGU, LLC-7454



                // Case Types
                List<Guid> lCaseTypes = new List<Guid>();
                lCaseTypes.Add(Guid.Parse("a451bb7b-6c55-4e27-8aae-30950732d8fc"));   //7300 - Residential Exterior w/MSB - AmWinsAccess Ins. Services
                lCaseTypes.Add(Guid.Parse("a451bb7b-6c55-4e27-8aae-30950732d8fc"));   //7300 - Residential Exterior w/MSB - AmWinsAccess Ins. Services 4.1.20z
                lCaseTypes.Add(Guid.Parse("35f28916-5059-4180-9903-56470a3d4580"));   //7300 - Residential Exterior w/MSB & update - AmWins-Access Ins. Services 
                lCaseTypes.Add(Guid.Parse("d7d8b0fb-91b0-4968-b6f6-686db00faa6f"));   //7300 - Residential Exterior w/MSB & update - AmWins-Access Ins. Services 4.1.20z
                lCaseTypes.Add(Guid.Parse("5d2bfe4a-b6fa-4256-b21a-37984cb8adaa"));   //7300 - Residential Interior & Exterior w/MSB - AmWins-Access Ins. Services 
                lCaseTypes.Add(Guid.Parse("a3bbdda9-e07d-42e9-b544-05512a1a6e97"));   //7300 - Residential Interior & Exterior w/MSB & update - AmWins-Access Ins. Services 

                lCaseTypes.Add(Guid.Parse("5837c5d0-ca3a-4420-9346-11956e768c6e"));   //7032 - Residential Exterior w/MSB - RPS-FL
                lCaseTypes.Add(Guid.Parse("bb0f8496-609d-407f-ba2b-4f29a953889e"));   //7032 - Residential Exterior w/MSB & update - RPS-FL
                lCaseTypes.Add(Guid.Parse("29d64198-cc15-4ff5-956c-16911b13aa6b"));   //7032 - Residential Interior & Exterior w/MSB - RPS-FL 
                lCaseTypes.Add(Guid.Parse("63b1d2fd-3168-4c70-9a92-a7fb74ec59d3"));   //7032 - Residential Interior & Exterior w/MSB & update - RPS-FL

                //7064 updated 2/22/21
                lCaseTypes.Add(Guid.Parse("8ef1a057-deb0-4bfd-b888-68aafc25ca9a"));   //7064 - Residential Exterior w/MSB & update - Hull-FL
                lCaseTypes.Add(Guid.Parse("1a3e66ef-73b6-42a2-957a-00a25528cf8b"));   //7064 - Residential Exterior w/MSB- Hull-FL 
                lCaseTypes.Add(Guid.Parse("b42c8795-e964-4f78-9b37-ab83465a3c47"));   //7064 - Residential Interior & Exterior w/MSB - Hull-FL 
                lCaseTypes.Add(Guid.Parse("2590969f-9d6a-4e7d-94fa-e625828eabbe"));   //7064 - Residential Interior & Exterior w/MSB & update - Hull-FL

                lCaseTypes.Add(Guid.Parse("8d2391c2-b8a7-4786-9ca0-9dec0e1d76d6"));   //7305 - Double Tap - Residential Exterior w/MSB & update - London Underwriters 
                lCaseTypes.Add(Guid.Parse("7fc7eb56-c40e-453a-b800-5797d71943db"));   //7305 - Double Tap - Residential Interior & Exterior w/MSB & Update- London Underwriters 
                lCaseTypes.Add(Guid.Parse("ff76cc65-2969-4f47-8707-bc4e2a79f0b8"));   //7305 - Residential Exterior w/MSB - London Underwriters  
                lCaseTypes.Add(Guid.Parse("d2a94b2b-f6ed-49eb-8141-0032527e9d47"));   //7305 - Residential Exterior w/MSB & update - London Underwriters 
                lCaseTypes.Add(Guid.Parse("510352f2-de95-4609-9d5e-4872a5b4a3a9"));   //7305 - Residential Interior & Exterior w/MSB - London Underwriters 
                lCaseTypes.Add(Guid.Parse("35602daa-cda6-490d-8222-2d2ed163eca0"));   //7305 - Residential Interior & Exterior w/MSB & Update- London Underwriters 

                lCaseTypes.Add(Guid.Parse("e9f018bf-2a80-44b8-b86f-51d7eb06fcba"));   //7307 - Residential Exterior w/MSB - Personal-Johnson & Johnson
                lCaseTypes.Add(Guid.Parse("11979a73-8f68-41ec-8a50-24383a09f142"));   //7307 - Residential Exterior w/MSB & update - Personal-Johnson & Johnson  
                lCaseTypes.Add(Guid.Parse("932edf9c-2cac-41aa-82f0-bce77c9edeb9"));   //7307 - Residential Interior & Exterior w/MSB - Personal-Johnson & Johnson 
                lCaseTypes.Add(Guid.Parse("35a061ea-3fb6-4d6f-bf3e-906075945b5f"));   //7307 - Residential Interior & Exterior w/MSB & Roof - Personal-Johnson & Johnson  
                lCaseTypes.Add(Guid.Parse("65c69191-aea5-4c77-95d7-12bd969bf91c"));   //7307 - Residential Interior & Exterior w/MSB & Update - Personal-Johnson & Johnson  
                lCaseTypes.Add(Guid.Parse("8567dbac-01bd-4f94-b580-018eccbf8642"));   //7307 - Residential Interior & Exterior w/MSB & Update & Roof - Personal-Johnson & Johnson   
                lCaseTypes.Add(Guid.Parse("8567dbac-01bd-4f94-b580-018eccbf8642"));   //7307 - Residential Exterior w/MSB - Personal-Johnson & Johnson-2.14.20

                lCaseTypes.Add(Guid.Parse("60290d66-bf56-4cb8-829c-c6c47fad29ac"));   //7328 - Residential Exterior w/MSB - RPS-GA
                lCaseTypes.Add(Guid.Parse("92dfa16c-3731-485e-a858-4e1314f57883"));   //7328 - Residential Exterior w/MSB & update - RPS-GA
                lCaseTypes.Add(Guid.Parse("9aac52f5-f862-4b45-a9fd-52915f483f24"));   //7328 - Residential Interior & Exterior w/MSB - RPS-GA 
                lCaseTypes.Add(Guid.Parse("f6cf9a50-a4b0-4b07-b0e0-ff8b5f11a8b7"));   //7328 - Residential Interior & Exterior w/MSB & update - RPS-GA

                lCaseTypes.Add(Guid.Parse("6f61e40d-7cae-4bc1-a90a-9816e2de300f"));   //7247 - Residential Exterior w/MSB - SIU-FL
                lCaseTypes.Add(Guid.Parse("d1660b65-d6a4-4dfd-b846-10cf19c052c6"));   //7247 - Residential Exterior w/MSB & update - SIU-FL
                lCaseTypes.Add(Guid.Parse("2ee1fc01-3265-42c2-8667-1c71c4024ac2"));   //7247 - Residential Interior & Exterior w/MSB - SIU-FL
                lCaseTypes.Add(Guid.Parse("6a14e156-54b2-4b0c-bcc6-5a14ff770a79"));   //7247 - Residential Interior & Exterior w/MSB & update - SIU-FL

                lCaseTypes.Add(Guid.Parse("9e71df8e-7333-4687-88a3-6836a79e681b"));   //7026 - Residential Exterior w/MSB - CRC-Personal-Orlando  
                lCaseTypes.Add(Guid.Parse("5e339d05-dd39-48df-afbb-261ed5eba72c"));   //7026 - Residential Exterior w/MSB & update - CRC-Personal-Orlando
                lCaseTypes.Add(Guid.Parse("3cf6680a-6dc8-4d69-80ce-29fbe65df21c"));   //7026 - Residential Interior & Exterior w/MSB - CRC-Personal-Orlando
                lCaseTypes.Add(Guid.Parse("47b5fc48-85f5-4872-8db3-fcbb6b21abe9"));   //7026 - Residential Interior & Exterior w/MSB & update - CRC-Personal-Orlando

                lCaseTypes.Add(Guid.Parse("cb669b67-fa85-4edf-8cc4-987df6ccdcc3"));   //7301 - Residential Exterior w/MSB & update - AmWins-Access Ins. Services  
                lCaseTypes.Add(Guid.Parse("cef48f1e-f6b4-4cfb-9d00-813398b2cf3a"));   //7301 - Residential Interior & Exterior High-Value  w/MSB & update - AmWins-Access Ins. Services  
                lCaseTypes.Add(Guid.Parse("0c6e2ac8-97b6-4ecf-b18e-682f90d8ccad"));   //7301 - Residential Interior & Exterior Mid-Value  w/MSB & update - AmWins-Access Ins. Services 

                // 7033 updated 2/22/21
                lCaseTypes.Add(Guid.Parse("05ade286-e520-44ec-a72f-dd6a6eb63459"));   //7033 - Residential Exterior w/MSB - Hull-TB   
                lCaseTypes.Add(Guid.Parse("595f354d-64db-400c-9449-24e511fbd82f"));   //7033 - Residential Exterior w/MSB & update - Hull-TB 
                lCaseTypes.Add(Guid.Parse("e6e6d1b3-60c0-4bdf-986b-c1375535a9ae"));   //7033 - Residential Interior & Exterior w/MSB - Hull TB 
                lCaseTypes.Add(Guid.Parse("93132005-fe16-4b4e-8974-cf51c8210a70"));   //7036 - Residential Interior & Exterior w/MSB & update - Hull-TB

                lCaseTypes.Add(Guid.Parse("9d952284-2574-47ca-b52a-02c7d51bee1b"));   //7298 - Residential Exterior w/MSB - MacNeill 
                lCaseTypes.Add(Guid.Parse("709251d8-c2d8-4cc0-8ad0-52d48120ab0d"));   //7298 - Residential Exterior w/MSB - MacNeill 
                lCaseTypes.Add(Guid.Parse("5e572911-bdcb-4a21-a31a-de839772afb8"));   //7298 - Residential Interior & Exterior w/MSB - MacNeill 

                lCaseTypes.Add(Guid.Parse("5598a656-fcad-41fb-9d01-0c2aff194803"));   //7011 - Residential Exterior w/MSB & update-SeaCoast-Personal
                lCaseTypes.Add(Guid.Parse("04490aef-c7c8-4498-97e5-00803492f615"));   //7011 - Residential Exterior w/MSB-SeaCoast-Personal
                lCaseTypes.Add(Guid.Parse("01e5579d-91a1-4766-8934-c1730d04e2d2"));   //7011 - Residential Interior & Exterior w/MSB & update-SeaCoast-Personal
                lCaseTypes.Add(Guid.Parse("397e52d7-346f-4c75-9847-8f8f775835a0"));   //7011 - Residential Interior & Exterior w/MSB-SeaCoast-Personal 

                lCaseTypes.Add(Guid.Parse("f459a456-bd3d-4b97-be9f-cba4cdf13375"));   //7101 - Residential Exterior w/MSB - ABCO
                lCaseTypes.Add(Guid.Parse("32264c0d-8727-4d80-8fb7-11303b913683"));   //7101 - Residential Exterior w/MSB - ABCO 4.14.20z
                lCaseTypes.Add(Guid.Parse("5855bf54-70e8-495d-9b0a-ae25235870b0"));   //7101 - Residential Exterior w/MSB & update - ABCO

                lCaseTypes.Add(Guid.Parse("7846a80e-93ad-4001-b3bd-f95e42c458be"));   //7295 - Residential Exterior w/MSB - National Risk Solutions   
                lCaseTypes.Add(Guid.Parse("533f02e0-6bed-4d46-bfe6-a46a30b06601"));   //7295 - Residential Exterior w/MSB & update - National Risk Solutions 
                lCaseTypes.Add(Guid.Parse("7a84ce2e-7aa3-41ba-ba90-670b22efbaf3"));   //7295 - Residential Interior & Exterior w/MSB - National Risk Solutions 
                lCaseTypes.Add(Guid.Parse("df872e74-8c50-4dee-a6f1-2ae9ab60b4fa"));   //7295 - Residential Interior & Exterior w/MSB & update - National Risk Solutions 

                lCaseTypes.Add(Guid.Parse("a06b688f-fc0a-4feb-965d-393472f2ac8c"));   //7205 - Residential Exterior w/MSB - Quaker 
                lCaseTypes.Add(Guid.Parse("1d37d67b-b289-4712-9fc7-982adfd63c27"));   //7205 - Residential Exterior w/MSB & update - Quaker 
                lCaseTypes.Add(Guid.Parse("88cc3405-0e74-4103-b2b5-26c4b509c51c"));   //7205 - Residential Interior & Exterior w/MSB - Quaker 
                lCaseTypes.Add(Guid.Parse("8351d481-9b1c-4d09-b8e3-286c685d0706"));   //7205 - Residential Interior & Exterior w/MSB & update - Quaker 

                lCaseTypes.Add(Guid.Parse("b403d7a8-debc-44b8-b7bd-b053ee597de2"));   //7074 - Residential Exterior w/MSB - Shelly, Middlebrooks & O'Leary
                lCaseTypes.Add(Guid.Parse("862864ec-2004-446f-bc6e-e91cf2c65c72"));   //7074 - Residential Exterior w/MSB - Shelly, Middlebrooks & O'Leary z3.13.20
                lCaseTypes.Add(Guid.Parse("bf8a1dea-bbc5-4bae-9def-d15ce3096223"));   //7074 - Residential Exterior w/MSB & update- Shelly, Middlebrooks & O'Leary 
                lCaseTypes.Add(Guid.Parse("08a1813c-b181-45d0-998f-91485db1c39f"));   //7074 - Residential Interior & Exterior w/MSB - Shelly, Middlebrooks & O'Leary 
                lCaseTypes.Add(Guid.Parse("6c7bcc96-1406-4a95-9067-0d5a12d3e3a2"));   //7074 - Residential Interior & Exterior w/MSB & Update - Shelly, Middlebrooks & O'Leary

                lCaseTypes.Add(Guid.Parse("336c0b64-b4a3-42a9-ada6-758fc4e85567"));   //7365 - Residential Exterior w/MSB - Risk Management Advisory Group
                lCaseTypes.Add(Guid.Parse("51da61a3-0311-4699-a123-103d3a66146b"));   //7365 - Residential Exterior w/MSB & update - Risk Management Advisory Group
                lCaseTypes.Add(Guid.Parse("7fbea757-d7a2-4e92-9517-cc0195af0735"));   //7365 - Residential Interior & Exterior w/MSB - Risk Management Advisory Group 05/28
                lCaseTypes.Add(Guid.Parse("a85370aa-47ee-4919-8a6d-cb54a5a63815"));   //7365 - Residential Interior & Exterior w/MSB & update - Risk Management Advisory Group 05/28

                lCaseTypes.Add(Guid.Parse("6df11f0f-92a8-4993-b923-da5c3666fa27"));   //7292 - Residential Exterior w/MSB - Marketscout
                lCaseTypes.Add(Guid.Parse("cb1cb532-3811-4c09-b0eb-9ca702456470"));   //7292 - Residential Exterior w/MSB & update - Marketscout
                lCaseTypes.Add(Guid.Parse("1a432cff-2bc4-4636-b63b-d67e3509e22a"));   //7292 - Residential Interior & Exterior w/MSB - Marketscout 
                lCaseTypes.Add(Guid.Parse("987ac670-10e5-492f-b55a-3f7735b1cebf"));   //7292 - Residential Interior & Exterior w/MSB & update - Marketscout

                lCaseTypes.Add(Guid.Parse("1c560f82-bb2e-4d6f-bb33-c871a547cb3e"));   //7292 - Residential Exterior w/MSB - Elite Underwriters  
                lCaseTypes.Add(Guid.Parse("b1e4ed27-e134-4187-a714-c188a67a2fe2"));   //7292 - Residential Exterior w/MSB & update - Elite Underwriters  

                lCaseTypes.Add(Guid.Parse("c2d184f5-1178-4f95-8f54-f17513f9bae2"));   //7185 - Residential Exterior w/MSB -  MacDuff
                lCaseTypes.Add(Guid.Parse("0336e15c-2dfd-4331-8ec6-26c0436f690e"));   //7185 - Residential Exterior w/MSB & update - MacDuff
                lCaseTypes.Add(Guid.Parse("d04e70f5-e2f4-4ba8-bc96-5044f4451f91"));   //7185 - Residential Interior & Exterior w/MSB - MacDuff 
                lCaseTypes.Add(Guid.Parse("d197b2cf-8173-433c-897f-378e8469b16f"));   //7185 - Residential Interior & Exterior w/MSB & update - MacDuff

                // 7070 updated 2/22/21
                lCaseTypes.Add(Guid.Parse("32e938cb-2159-4fc0-8a20-f5cfd8c0c410"));   //7070 - Residential Exterior w/MSB - Hull Jax   
                lCaseTypes.Add(Guid.Parse("8ff75df6-c8ae-4fec-8c1d-b77cbbbb4c50"));   //7070 - Residential Exterior w/MSB & update - Hull Jax
                lCaseTypes.Add(Guid.Parse("3f1d6312-e81f-42b6-8e2c-7218ea04bf4b"));   //7070 - Residential Interior & Exterior w/MSB - Hull Jax 
                lCaseTypes.Add(Guid.Parse("4a91771b-3725-41e5-9f04-88e18b08163f"));   //7070 - Residential Interior & Exterior w/MSB & update - Hull Jax
                lCaseTypes.Add(Guid.Parse("41c3e29f-6d6b-473d-a1c9-5c0a8d803e0b"));   //7070 - Residential Exterior w/MSB - Hull Jax   
                lCaseTypes.Add(Guid.Parse("ba0e7a39-94b6-4f6f-9117-19ae437816d2"));   //7070 - Residential Exterior w/MSB & update - Hull Jax
                
                lCaseTypes.Add(Guid.Parse("78f1fbf0-a5ec-42d6-b531-8485a6ece1b0"));   //7366 - Residential Exterior w/MSB - XS Brokers
                lCaseTypes.Add(Guid.Parse("3dbbe425-1d45-4667-a20d-f8873c845aae"));   //7366 - Residential Exterior w/MSB & update - XS Brokers
                lCaseTypes.Add(Guid.Parse("31db723c-b8e0-42d9-b3e1-bef191f14e1a"));   //7366 - Residential Interior & Exterior w/MSB - XS Brokers
                lCaseTypes.Add(Guid.Parse("71bc53f1-7300-411e-a59a-e349e9cb82bd"));   //7366 - Residential Interior & Exterior w/MSB & update - XS Brokers

                lCaseTypes.Add(Guid.Parse("6ccfb81d-cea0-4dc1-9aab-97eadbae7ef3"));   //7289 - Residential Exterior w/MSB & update - Hull-NC
                lCaseTypes.Add(Guid.Parse("21cac6c5-af42-414f-82e2-a8645b486419"));   //7289 - Residential Exterior w/MSB- Hull-NC 
                lCaseTypes.Add(Guid.Parse("f3d7d70d-5bc2-4c88-a0bf-0288334a8120"));   //7289 - Residential Interior & Exterior w/MSB - Hull-NC 
                lCaseTypes.Add(Guid.Parse("69f76015-9191-4b7e-adec-44cf4fd41fcc"));   //7289 - Residential Interior & Exterior w/MSB & update - Hull-NC
                lCaseTypes.Add(Guid.Parse("f76b28c6-fec7-46c9-8f1f-08f4de11fcdb"));   //7289 - Residential Exterior w/MSB & update - Hull-NC
                lCaseTypes.Add(Guid.Parse("3be6bc1f-ff12-4951-8ecb-5c4bf4b2bbba"));   //7289 - Residential Exterior w/MSB- Hull-NC 

                lCaseTypes.Add(Guid.Parse("d19123ee-f8f7-422e-ae2b-da1b7f65872f"));   //7192 - Residential Exterior w/MSB & update-Bass-St. Pete
                lCaseTypes.Add(Guid.Parse("7c9d2b68-4f4a-4a08-8851-9849729f4e8f"));   //7192 - Residential Exterior w/MSB-Bass-St. Pete
                lCaseTypes.Add(Guid.Parse("3d7ea8e2-a016-41e9-b837-d917affc93f5"));   //7192 - Residential Interior & Exterior w/MSB & update-Bass-St. Pete
                lCaseTypes.Add(Guid.Parse("6415d9d0-2eb3-480a-adcb-f4ab57acd2c4"));   //7192 - Residential Interior & Exterior w/MSB-Bass-St. Pete

                lCaseTypes.Add(Guid.Parse("4b6d37e7-6f32-40a1-95a8-b7b218edca87"));   //7030 - Residential Exterior w/MSB & update-Bass-Ft. Lauderdale
                lCaseTypes.Add(Guid.Parse("0f0930a6-de6d-4580-affc-7d307588dd39"));   //7030 - Residential Exterior w/MSB-Bass-Ft. Lauderdale
                lCaseTypes.Add(Guid.Parse("645f70c8-3891-43a5-802f-12970c6705b2"));   //7030 - Residential Interior & Exterior w/MSB & update-Bass-Ft. Lauderdale
                lCaseTypes.Add(Guid.Parse("e8f56f0f-ba4b-4b31-a9ac-ce4d0d2427a3"));   //7030 - Residential Interior & Exterior w/MSB-Bass-Ft. Lauderdale

                lCaseTypes.Add(Guid.Parse("66bdec9c-bd3e-4d51-b699-855a9cd80c2f"));   //7341 - Residential Exterior w/MSB & update-Bass-Orlando
                lCaseTypes.Add(Guid.Parse("89fffa0e-10df-4fa1-88b4-9d586e9541af"));   //7341 - Residential Exterior w/MSB-Bass-Orlando
                lCaseTypes.Add(Guid.Parse("e6da0759-ec14-47ed-9fbf-3384c63b36b4"));   //7341 - Residential Interior & Exterior w/MSB & update-Bass-Orlando
                lCaseTypes.Add(Guid.Parse("6dfc32fd-c9f7-4f95-b4dd-d818513380b0"));   //7341 - Residential Interior & Exterior w/MSB-Bass-Orlando

                lCaseTypes.Add(Guid.Parse("7fc18047-b880-4c56-9247-2df238bced30"));   //7342 - Residential Exterior w/MSB & update-Bass-Gainesville
                lCaseTypes.Add(Guid.Parse("f6c68725-d3b9-43a6-9cef-4b21697b23dc"));   //7342 - Residential Exterior w/MSB-Bass-Gainesville
                lCaseTypes.Add(Guid.Parse("7e244a0d-f5d0-452d-8afd-78ed718d1650"));   //7342 - Residential Interior & Exterior w/MSB & update-Bass-Gainesville
                lCaseTypes.Add(Guid.Parse("52041203-b177-4050-ba91-b6828cf9cf9c"));   //7342 - Residential Interior & Exterior w/MSB-Bass-Gainesville

                lCaseTypes.Add(Guid.Parse("4d3f0558-09eb-4bd1-94f7-a61ea7f7aabe"));   //7370 - Residential Exterior w/MSB & update-Bass-Atlanta
                lCaseTypes.Add(Guid.Parse("f07ba9f7-1a8a-427f-a943-2c26609a40ce"));   //7370 - Residential Exterior w/MSB-Bass-Atlanta
                lCaseTypes.Add(Guid.Parse("a54c828d-e831-489d-963f-1b419bc5f1ca"));   //7370 - Residential Interior & Exterior w/MSB & update-Bass-Atlanta
                lCaseTypes.Add(Guid.Parse("1916d99c-e22f-4c06-b552-fe4ea9aa756c"));   //7370 - Residential Interior & Exterior w/MSB-Bass-Atlanta

                lCaseTypes.Add(Guid.Parse("48641900-e591-48d6-b71c-f329c9f1570b"));   //7371 - Residential Exterior w/MSB & update-Bass-Houston
                lCaseTypes.Add(Guid.Parse("92cbd0f1-12e0-4e48-9134-88e2789ed28a"));   //7371 - Residential Exterior w/MSB-Bass-Houston
                lCaseTypes.Add(Guid.Parse("14933732-5e7b-4be8-bb2d-d1a409694749"));   //7371 - Residential Interior & Exterior w/MSB & update-Bass-Houston
                lCaseTypes.Add(Guid.Parse("d7273beb-a6d7-4ba0-991d-6759f7339a80"));   //7371 - Residential Interior & Exterior w/MSB-Bass-Houston

                lCaseTypes.Add(Guid.Parse("4aa9dc8c-138c-435f-882e-80c721a433cc"));   //7372 - Residential Exterior w/MSB & update-Bass-Gulf Coast
                lCaseTypes.Add(Guid.Parse("d2b34c0f-085e-4031-a921-fb0dbdea6e3c"));   //7372 - Residential Exterior w/MSB-Bass-Gulf Coast
                lCaseTypes.Add(Guid.Parse("382c7317-2f1b-44ce-bdad-e612c99e5c22"));   //7372 - Residential Interior & Exterior w/MSB & update-Bass-Gulf Coast
                lCaseTypes.Add(Guid.Parse("c1b9a9c6-5566-4a6a-8a4e-8ed507fecaeb"));   //7372 - Residential Interior & Exterior w/MSB-Bass-Gulf Coast

                lCaseTypes.Add(Guid.Parse("a8d5a7c7-4707-4ad6-abf9-b2ee1d05759b"));   //7376 - Residential Exterior w/MSB & update-Bass-New Orleans 
                lCaseTypes.Add(Guid.Parse("0aafe8a4-d111-4eec-802c-cb2e14014eee"));   //7376 - Residential Exterior w/MSB-Bass-New Orleans 
                lCaseTypes.Add(Guid.Parse("c051c3ab-9a9a-4d7c-985d-189b4a4eda3f"));   //7376 - Residential Interior & Exterior w/MSB & update-Bass-New Orleans 
                lCaseTypes.Add(Guid.Parse("2cb7c57d-5e27-4ecd-b501-0245dc1f53bb"));   //7376 - Residential Interior & Exterior w/MSB-Bass-New Orleans

                lCaseTypes.Add(Guid.Parse("992bfc30-5e6e-4ad8-82e6-60460b735b7a"));   //7377 - Residential Exterior w/MSB & update-Bass-Las Vegas 
                lCaseTypes.Add(Guid.Parse("d9e7fb42-e6da-4c2f-847d-340dcb6dfebe"));   //7377 - Residential Exterior w/MSB-Bass-Las Vegas
                lCaseTypes.Add(Guid.Parse("329e6505-75f7-4e55-a82e-51cd02e21eaf"));   //7377 - Residential Interior & Exterior w/MSB & update-Bass-Las Vegas  
                lCaseTypes.Add(Guid.Parse("aa02bdf4-2b5c-4c2e-a27b-b61929a468d5"));   //7377 - Residential Interior & Exterior w/MSB-Bass-Las Vegas 

                lCaseTypes.Add(Guid.Parse("ee72b7af-8d96-44f0-987c-1a93b27506fb"));   //7383 - Residential Exterior w/MSB & update-Bass-SC
                lCaseTypes.Add(Guid.Parse("046a3afc-333e-4160-afe5-874703e39520"));   //7383 - Residential Exterior w/MSB-Bass-SC
                lCaseTypes.Add(Guid.Parse("c68d8aa0-ed99-4443-b839-f897ee515f5e"));   //7383 - Residential Interior & Exterior w/MSB & update-Bass-SC
                lCaseTypes.Add(Guid.Parse("9b711cfa-0771-4996-8c51-fab277f23d76"));   //7383 - Residential Interior & Exterior w/MSB-Bass-SC

                lCaseTypes.Add(Guid.Parse("cff59468-039c-42ba-bf4a-5c3e6fed68ca"));   //7390 - Residential Exterior w/MSB - Halcyon  
                lCaseTypes.Add(Guid.Parse("f946117d-713a-4764-a27c-6f06e1c12f39"));   //7390 - Residential Exterior w/MSB & update - Halcyon 
                lCaseTypes.Add(Guid.Parse("68d3cd86-1cd3-471c-8354-dd54f67694a1"));   //7390 - Residential Interior & Exterior w/MSB - Halcyon 
                lCaseTypes.Add(Guid.Parse("53f89362-8007-4bc6-8d00-96fe818448cd"));   //7390 - Residential Interior & Exterior w/MSB & update - Halcyon 


                lCaseTypes.Add(Guid.Parse("13393b66-b7ce-4592-ba03-a991e677ec91"));   //7420 - Residential Exterior w/MSB - Cabrillo
                lCaseTypes.Add(Guid.Parse("a66333dd-89ec-444f-a34b-d61bdedd3731"));   //7420 - Residential Interior & Exterior w/MSB - Cabrillo
                lCaseTypes.Add(Guid.Parse("0952309d-67a1-45a1-821f-fb890b6102f4"));   //7420 - Residential Interior & Exterior w/MSB & update - Cabrillo

                lCaseTypes.Add(Guid.Parse("507a9686-5abc-45cb-8dc1-b4c5a3c8c42c"));   //7431 - Residential Exterior w/MSB - CRC - Bayport
                lCaseTypes.Add(Guid.Parse("6cd8b1f3-a34a-4e7b-ad65-9ebe0e74711b"));   //7431 - Residential Exterior w/MSB & update - CRC - Bayport
                lCaseTypes.Add(Guid.Parse("09cf8ecb-1cc7-46fa-a571-fcfdfec6a39a"));   //7431 - Residential Interior & Exterior w/MSB - CRC - Bayport
                lCaseTypes.Add(Guid.Parse("f87c2200-78e4-4a68-9ceb-bedb5fd7e13c"));   //7431 - Residential Interior & Exterior w/MSB & update - CRC - Bayport

                lCaseTypes.Add(Guid.Parse("b7070206-7963-4ac3-a57e-fe59de2aa68a"));   //7164 - Residential Exterior w/MSB - Hull-Margate 
                lCaseTypes.Add(Guid.Parse("810e1123-c907-4893-9dc8-7093f0f970f9"));   //7164 - Residential Exterior w/MSB & update - Hull-Margate
                lCaseTypes.Add(Guid.Parse("936abd1c-5b36-4272-a53f-feda2528a8d4"));   //7164 - Residential Interior & Exterior w/MSB - Hull-Margate 
                lCaseTypes.Add(Guid.Parse("7bf1cc8e-a372-453d-9eae-62bf18d391c3"));   //7164 - Residential Interior & Exterior w/MSB & update - Hull-Margate

                lCaseTypes.Add(Guid.Parse("d153f8c7-c099-46ae-b7df-5914c25d0f94"));   //7429 - Residential Exterior w/MSB - RPS-LA
                lCaseTypes.Add(Guid.Parse("449f2219-5a10-477b-8fc4-2b8f32d699eb"));   //7429 - Residential Exterior w/MSB & update - RPS-LA
                lCaseTypes.Add(Guid.Parse("301f890b-aa32-470e-9446-b07918f4a6e7"));   //7429 - Residential Interior & Exterior w/MSB - RPS-LA 
                lCaseTypes.Add(Guid.Parse("71477081-2aa3-48ce-9ba5-b4f1c7337d8f"));   //7429 - Residential Interior & Exterior w/MSB & update - RPS-LA

                lCaseTypes.Add(Guid.Parse("19a5c531-1c16-4d5c-8c36-7d3969832a00"));   //7438 - Residential Exterior w/MSB - RPS-WA
                lCaseTypes.Add(Guid.Parse("49525ee4-9657-481a-b9c3-8b46e6419cab"));   //7438 - Residential Exterior w/MSB & update - RPS-WA
                lCaseTypes.Add(Guid.Parse("05505a51-f490-44a8-a8e2-74e475bb5862"));   //7438 - Residential Interior & Exterior w/MSB - RPS-WA 
                lCaseTypes.Add(Guid.Parse("b771afb5-a328-467a-b175-69af18042fe2"));   //7438 - Residential Interior & Exterior w/MSB & update - RPS-WA

                lCaseTypes.Add(Guid.Parse("de93c623-8251-40f9-aa60-338166f682dc"));   //7432 - Residential Exterior w/MSB - NC
                lCaseTypes.Add(Guid.Parse("b2beafd7-f22e-45b3-882d-717fa247eb5a"));   //7432 - Residential Exterior w/MSB & update - NC
                lCaseTypes.Add(Guid.Parse("038f9a48-cedd-49c8-8359-5f6332a042a2"));   //7432 - Residential Interior & Exterior w/MSB - NC
                lCaseTypes.Add(Guid.Parse("545cc918-3769-433c-8fb4-8cad372601a2"));   //7432 - Residential Interior & Exterior w/MSB & update - NC

                lCaseTypes.Add(Guid.Parse("d50977d6-b068-41e5-a437-fc76ee205983"));   //7441 - Exterior Inspection-PL - RT Specialty
                lCaseTypes.Add(Guid.Parse("32d0d0ce-16b6-4bb5-9bee-248019284ba5"));   //7441 - Interior & Exterior Inspection-PL - RT Specialty 

                lCaseTypes.Add(Guid.Parse("2bbf5350-dfa7-4004-aa1e-89334b731e98"));   //7170 - Residential Interior & Exterior w/MSB
                lCaseTypes.Add(Guid.Parse("c15f1683-84be-45fc-968d-aa7624b11af8"));   //7170 - Residential Interior & Exterior w/MSB & update

                lCaseTypes.Add(Guid.Parse("b3729ebd-666b-4ae9-a708-a91fea005092"));   //7454 - Residential Exterior w/MSB - KGU, LLC
                lCaseTypes.Add(Guid.Parse("cba4936a-b3fd-417d-a130-d9d133d3ea78"));   //7454 - Residential Exterior w/MSB & update - KGU, LLC
                lCaseTypes.Add(Guid.Parse("935c8767-cd79-45a3-bd90-689debb9cc1e"));   //7454 - Residential Interior & Exterior w/MSB - KGU, LLC
                lCaseTypes.Add(Guid.Parse("5bbc622e-d9c5-4d4c-b74e-97c235a43a38"));   //7454 - Residential Interior & Exterior w/MSB & update - KGU, LLC



                //lCaseTypes.Add(Guid.Parse(""));   //

                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                int iCaseNum;
                int iCustNum;
                Guid guCaseType;
                Guid guCaseTypeRet;
                Guid guCaseID;
                string sCustNum;
                string sInsured;
                string sPolicyNum;
                string sStreetAdd;
                string sCity;
                string sState;
                string sZip;
                decimal dCoverageA;
                int iCoverageA;

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                // Set up SQL 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetRCTOrderedByDate";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();

                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate360);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate360);
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through cases
                    do
                    {

                        //c.CaseNumber, cu.LookupID, c.OrderedType, c.PolicyNumber, c.PolicyHolderName, a.Line1, a.City,p.StateAbbreviation, p.PostalCodeValue, c.CaseID

                        // case #
                        iCaseNum = (int)sqlReader.GetSqlInt32(0);

                        // cust #
                        sCustNum = sqlReader.GetSqlString(1).ToString();
                        if (sCustNum.Length < 1)
                        {
                            iCustNum = 0;
                        }
                        else
                        {
                            iCustNum = (int)sqlReader.GetSqlString(1).ToSqlInt32();
                        }

                        // case type 
                        guCaseType = (Guid)sqlReader.GetSqlGuid(2);

                        // Policy
                        sPolicyNum = sqlReader.GetSqlString(3).ToString();

                        // Insured
                        sInsured = sqlReader.GetSqlString(4).ToString();
                        if (sInsured.Length > 49)   // 50 char max insured name
                        {
                            sInsured = sInsured.Substring(0, 49);
                        }

                        // Address
                        sStreetAdd = sqlReader.GetSqlString(5).ToString();

                        // City
                        sCity = sqlReader.GetSqlString(6).ToString();

                        // State
                        sState = sqlReader.GetSqlString(7).ToString();

                        // Zip
                        sZip = sqlReader.GetSqlString(8).ToString();

                        // case id 
                        guCaseID = (Guid)sqlReader.GetSqlGuid(9);

                        // Coverage A
                        dCoverageA = (decimal)sqlReader.GetSqlMoney(10);
                        iCoverageA = Convert.ToInt32(dCoverageA);

                        // case type (returned)
                        guCaseTypeRet = (Guid)sqlReader.GetSqlGuid(11);

                        // if customer is in the list 
                        if (lCustAcnts.IndexOf(iCustNum) != -1)
                        {
                            oLU.WritetoLog("Checking case type for case# " + iCaseNum.ToString() + "\r\n\r\n");

                            // if case type is in list
                            //if (lCaseTypes.IndexOf(guCaseType) != -1 && iCoverageA > 0)
                            if (lCaseTypes.IndexOf(guCaseType) != -1 || lCaseTypes.IndexOf(guCaseTypeRet) != -1)
                            {

                                oLU.WritetoLog("Creating valuation for Case# " + iCaseNum.ToString());

                                RCT4Utils.RCT4 oRCT4 = new RCT4Utils.RCT4();

                                oRCT4.APIUserName = "Sutton_GFT";
                                oRCT4.APIPassword = "Welcome18";
                                oRCT4.UserLogin = "MLSutton";
                                oRCT4.InsuredName = sInsured;
                                oRCT4.Policy = sPolicyNum;
                                oRCT4.StreetAddress = sStreetAdd;
                                oRCT4.City = sCity;
                                oRCT4.State = sState;
                                oRCT4.PostalCode = sZip;
                                oRCT4.ValuationUID = iCaseNum.ToString();
                                oRCT4.CoverageAIn = iCoverageA;

                                string sRetVal = oRCT4.CreateValuation();

                                sbEmail.Append("Creating valuation for Case# " + iCaseNum.ToString() + "\r\n\r\n");
                                oLU.WritetoLog("Valuation created for Case# " + iCaseNum.ToString());
                                iCount++;
                            }

                        }   // skip case

                    } while (sqlReader.Read());     // cases ordered

                }   // has rows

                if (iCount == 0)
                {
                    sbEmail.Append("No cases to process.\r\n\r\n");
                }

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;
            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(sbEmail.ToString());
                }

            }
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;
        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {
            DateTime dRet = dDate;
            dRet = dRet.AddDays(-(dRet.Day - 1));
            return dRet;
        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {
            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = sEmailSubject;
            oMail.MsgBody = sbEmail.ToString();
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;
            }
        }

        static void sendErrEmail(string sMsgBody)
        {
            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by RCTDailyCreate Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;
        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

        static int getTimeZoneOffset()
        {
            SqlConnection sqlConn = null;
            SqlCommand sqlCmd = null;
            int iRetVal = 0;

            try
            {
                //string sQuery = @"SELECT dbo.TimeZoneOffset AS TZOffset;";
                sqlConn = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd = new SqlCommand("SELECT dbo.TimeZoneOffset()", sqlConn);
                sqlConn.Open();
                Int32 functionResult = (Int32)sqlCmd.ExecuteScalar();

                iRetVal = functionResult;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }
            finally
            {

                // close objects
                if (sqlConn != null)
                    sqlConn.Close();
            }

            return iRetVal;
        }
    }
}
