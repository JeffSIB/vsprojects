﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace CapacityExport
{
    public class Export : LC360Data.Exporter
    {
        #region Properties

        /// <summary>
        /// Company name - Set in AppSettings (i.e. Sutton)
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Customer name - Set in AppSettings (i.e. USAA)
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Customers to export from LC360 - Set in AppSettings (i.e. 0000, 1234, LOOKUPID)
        /// It is a comma separated string within the App.config.
        /// </summary>
        public string CustomersToExport { get; set; }

        /// <summary>
        /// Log directory - Set in AppSettings (i.e. C:\Accounts\Sutton\USAA\Export\Logs\)
        /// </summary>
        public string LogsDir { get; set; }

        /// <summary>
        /// Return form data on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. true)
        /// </summary>
        public string ReturnFormData { get; set; }

        /// <summary>
        /// Return PDF on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. true)
        /// </summary>
        public string ReturnPDF { get; set; }

        /// <summary>
        /// Return raw images on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. false)
        /// </summary>
        public string ReturnRawImages { get; set; }

        /// <summary>
        /// Send emails to specified email address on export of cases in DEBUG mode - Set in AppSettings (i.e. test@inpections.com)
        /// </summary>
        public string EmailSendToDebug { get; set; }

        /// <summary>
        /// Specify the From email address on export of cases in DEBUG mode - Set in AppSettings (i.e. test@inpections.com)
        /// </summary>
        public string EmailFromDebug { get; set; }

        /// <summary>
        /// Send emails to specified email address on export of cases in LIVE/RELEASE mode - Set in AppSettings (i.e. inspections@sibfla.com)
        /// </summary>
        public string EmailSendToLive { get; set; }

        /// <summary>
        /// Specify the From email address on export of cases in LIVE/RELEASE mode - Set in AppSettings (i.e. GA_EDS@usaa.com)
        /// </summary>
        public string EmailFromLive { get; set; }

        /// <summary>
        /// Export filter for completed cases by start date
        /// </summary>
        public string CompletedMinDate { get; set; }

        /// <summary>
        /// PDF directory - Set in AppSettings (i.e. C:\Accounts\Sutton\USAA\Export\Logs\)
        /// </summary>
        public string PDFExportFolder { get; set; }

         
        #endregion


        #region IExporter Members

        /// <summary>
        /// Here we set up the filter which tells the control what cases we want back.
        /// Note: We only pass cases that have not been previously exported.
        /// </summary>
        /// <returns></returns>
        public override LC360Data.CasesSearchCriteria GetFilter()
        {
            // Message displayed for logging.
            Log(new LC360Data.Log() { Decription = "Getting Search Criteria", LogCode = LC360Data.LogCode.JustForProgrammer });

            // This would normally be filtered by a company or case type to get back desired cases.  
            // Most likely with a Date filter as well.
            LC360Data.CasesSearchCriteria criteria = new LC360Data.CasesSearchCriteria();

            // Customer selection criteria - split the comma separated string "CustomersToExport" from App.config into a string array of customer lookup IDs
            criteria.CustomerLookupIDs = CustomersToExport.Split(',');

            string date = CompletedMinDate;
            DateTime startDate = DateTime.Parse(date);
            string sEnddate = "11/14/2021";
            DateTime enddate = DateTime.Parse(sEnddate);

            //Setup date filter
            criteria.DateFilters = new LC360Data.DateFilters();
            criteria.DateFilters.CompletedMin = startDate;
            //criteria.DateFilters.CompletedMax = enddate;

            /// Case Statuses desired
            criteria.CaseStatuses = new int[] { (int)LC360Data.CaseStatuses.Complete };

            return criteria;
        }

        /// <summary>
        /// Name of the exporter
        /// </summary>
        public override string Name
        {
            get
            { return (CompanyName + " " + CustomerName + " " + "Exporter"); }
        }

        #endregion

        /// <summary>
        /// Called by controller once the cases have been retrieved according to the Filter set in GetFilter().
        /// </summary>
        /// <param name="cases"></param>
        /// <returns></returns>
        public override LC360Data.ExportResult ExportCases(List<LC360Data.ExportCaseHolder> cases)
        {
            StringBuilder email = new StringBuilder();
            EmailBuilder eb = new EmailBuilder();
            List<CaseTracker> ct = new List<CaseTracker>();
            //string emailSubject = "";

            string sPDFFileName = "";

            /// Logging
            Log(new LC360Data.Log() { Decription = "Starting " + CustomerName + " Export", LogCode = LC360Data.LogCode.JustForProgrammer });

            /// Logging
            Log(new LC360Data.Log() { Decription = "Received " + cases.Count + " for export", LogCode = LC360Data.LogCode.GeneralMessage });

            
            /// Create List for successfully exported case IDs
            List<Guid> exportedCaseIDs = new List<Guid>();

            foreach(LC360Data.ExportCaseHolder holder in cases)
            {
                //emailSubject = "";

                ///Keep track of Exports success or failure.
                Func<String, bool, CaseTracker> TrackCaseInfo = (description,successful) =>
                    new CaseTracker
                    {
                        Description = description,
                        Successful = successful,
                        PolicyNumber = holder.Case.PolicyNumber ?? "",
                        PolicyHolder = holder.Case.InsuredName ?? "",
                        Street1 = holder.Case.LocationAddress ?? "",
                        City = holder.Case.LocationCity ?? "",
                        StateOrProvince = holder.Case.LocationState ?? "",
                        ZipCode = holder.Case.LocationPostalCode ?? "",
                        InspectionType = holder.Case.CaseType ?? ""
                    };

                Log(new LC360Data.Log() { Decription = "Starting export of Case: " + holder.Case.CaseNumber, LogCode = LC360Data.LogCode.JustForProgrammer });
                try
                {

                    // Create PDF file name
                    sPDFFileName = PDFExportFolder + holder.Case.CaseNumber + ".PDF";

                    // Save PDF
                    byte[] file = holder.PDFFile;
                    if (ByteArrayToFile(sPDFFileName, file))
                    {
                        Log(new LC360Data.Log() { Decription = "PDF of case #: " + holder.Case.CaseNumber + " successfully saved.", LogCode = LC360Data.LogCode.GeneralMessage });
                    }
                    else
                    {
                        Log(new LC360Data.Log() { Decription = "PDF of case #: " + holder.Case.CaseNumber + " could not be saved. ****", LogCode = LC360Data.LogCode.Error });
                        ct.Add(TrackCaseInfo("Error exporting case for Policy #: " + holder.Case.CaseNumber + "  " + holder.Case.InsuredName + ".", false));
                    }

                       
                    /// Add this caseID to list that will be passed back to calling program
                    exportedCaseIDs.Add(holder.Case.CaseID);

                    ct.Add(TrackCaseInfo("Success exporting case #: " + holder.Case.CaseNumber + ".", true));
                    
                }
                catch(Exception ex)
                {
                    Log(new LC360Data.Log() { Decription = "**** Error exporting case: " + holder.Case.CaseNumber + ex.Message, LogCode = LC360Data.LogCode.IssueWithPlugin });
                    ct.Add(TrackCaseInfo("Error exporting case #: " + holder.Case.CaseNumber + ".", false));
                }

            /// End of ForEach holder in cases
            }

            ///Create and send success/failure email
            eb.CreateEmail(ct, cases);

            /// Arrange to pass back 'Export Complete' message
            Log(new LC360Data.Log() { Decription = "Export Complete", LogCode = LC360Data.LogCode.Successful });
            
            /// Set up results to be returned to the calling program
            LC360Data.ExportResult result = new LC360Data.ExportResult();
             
            /// Mark cases as exported
            result.ExportedCaseIDs = exportedCaseIDs;

            return result;
        }


        /// <summary>
        /// Function to save byte array to a file
        /// </summary>
        /// <param name="sFileName">File name to save byte array</param>
        /// <param name="_ByteArray">Byte array to save to external file</param>
        /// <returns>Return true if byte array save successfully, if not return false</returns>
        public bool ByteArrayToFile(string sFileName, byte[] _ByteArray)
        {
            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream = new System.IO.FileStream(sFileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                // Writes a block of bytes to this stream using data from a byte array.
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);

                // close file stream
                _FileStream.Close();

                return true;
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());

            }

            // error occured, return false
            return false;
        }



        /// <summary>
        /// Send a given message using the current default SMTP Client 
        /// </summary>
        /// <param name="message"></param>
        public static void SendEmailMessage(MailMessage message)
        {
            SmtpClient client = new SmtpClient();
            client.Send(message);
        }


        /// <summary>
        /// Called by controller to get export options for the retrieval of case information
        /// These options are set using AppSettings located in App.config
        /// </summary>
        /// <returns></returns>
        public override LC360Data.ExportOptions GetExportOptions()
        {
            LC360Data.ExportOptions options = new LC360Data.ExportOptions();
            options.ReturnFormData = Convert.ToBoolean(ReturnFormData);
            options.ReturnPDF = Convert.ToBoolean(ReturnPDF);
            options.ReturnRawImages = Convert.ToBoolean(ReturnRawImages);
            return options;
        }

        /// <summary>
        /// Guid ID for this particular application.
        /// </summary>
        public override Guid ApplicationID
        {
            get { return new Guid("AEC8247D-0EA5-4CDD-AF0F-3654D6BADC15"); }
        }
   
    }

}
