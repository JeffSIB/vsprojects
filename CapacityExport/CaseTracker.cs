﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapacityExport
{
    /// <summary>
    /// Stores the success/error info gathered while validating import as well as converted errors from the Web Service
    /// </summary>
    public class CaseTracker
    {
        /// <summary>
        /// Track case information for email builder.
        /// </summary>
        public string Description { get; set; }

        public bool Successful { get; set; }

        /// <summary>
        /// The policy number of the case
        /// </summary>
        public string PolicyNumber { get; set; }
        public string PolicyHolder { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string StateOrProvince { get; set; }
        public string ZipCode { get; set; }
        public string InsuredPhone { get; set; }
        public string InspectionType { get; set; }
        public string FileName { get; set; }

    }
}
