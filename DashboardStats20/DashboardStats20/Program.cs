﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;

namespace DashboardStats20
{
    class Program
    {

        static string cfg_360UtilConnStr;

        static LogUtils.LogUtils oLU;
        static string cfg_logfilename;
        

        static void Main(string[] args)
        {

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;

            cfg_logfilename = colNameVal.Get("logfilename");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");

            //WeeklyNewCompChart();
            SalesHistChart();

        }

        static void WeeklyNewCompChart()
        {

            SqlConnection sqlConn360 = null;
            SqlCommand sqlCmd360 = null;
            SqlDataReader sqlDR360 = null;

            try
            {

                // Date range
                DateTime dBegDate = new DateTime();
                dBegDate = DateTime.Today;
                dBegDate = FirstDayofWeek(dBegDate);
                DateTime dEndDate = dBegDate.AddDays(-7);
                dBegDate = dBegDate.AddDays(-361);
                dBegDate = FirstDayofWeek(dBegDate);

                int iRow = 0;
                int iNew = 0;
                int iComp = 0;
                DateTime dWeek;

                int[] aiNew = new int[52];
                int[] aiComp = new int[52];
                DateTime[] adtWeek = new DateTime[52];

                // 360
                var lNewCases = new List<int>();
                var lCompCases = new List<int>();
                var lWeek = new List<DateTime>();

                sqlConn360 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd360 = new SqlCommand();

                sqlCmd360.CommandType = CommandType.Text;
                sqlCmd360.CommandText = "SELECT newcases, completed, statusperiod FROM Dashboard_Stats WHERE statusperiod BETWEEN '" + dBegDate + "' AND '" + dEndDate + "' AND stattype = 'W' ORDER BY statusperiod";
                sqlCmd360.Connection = sqlConn360;
                sqlConn360.Open();

                sqlDR360 = sqlCmd360.ExecuteReader();

                // anythign returned?
                if (sqlDR360.HasRows)
                {
                    iRow = 0;
                    sqlDR360.Read();

                    do
                    {

                        iNew = (int)sqlDR360.GetInt32(0);
                        iComp = (int)sqlDR360.GetInt32(1);
                        dWeek = sqlDR360.GetDateTime(2);

                        lNewCases.Add(iNew);
                        lCompCases.Add(iComp);
                        lWeek.Add(dWeek);
                        iRow++;

                    } while (sqlDR360.Read());
                }

                sqlConn360.Close();

                // create chart
                var chart = new Chart();
                chart.Size = new Size(900, 220);

                var chartArea = new ChartArea();
                //chartArea.AxisX.LabelStyle.Format = "dd/MMM\nhh:mm";
                chartArea.AxisX.LabelStyle.Format = "dd/MMM";
                //chartArea.AxisX.LabelStyle.Format = "hh:mm";
                chartArea.AxisX.LabelStyle.Interval = 2;
                chartArea.AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Weeks;
                chartArea.AxisX.Title = "Week";
                chartArea.AxisY.Title = "Cases";

                chartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
                chartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
                chartArea.AxisX.LabelStyle.Font = new Font("Consolas", 8);
                chartArea.AxisY.LabelStyle.Font = new Font("Consolas", 8);
                chart.ChartAreas.Add(chartArea);

                var series = new Series();
                series.Name = "New";
                series.Color = System.Drawing.Color.DarkOrange;
                series.ChartType = SeriesChartType.Line;
                series.XValueType = ChartValueType.DateTime;
                series.MarkerStyle = MarkerStyle.Circle;
                series.MarkerColor = Color.DarkOrange;
                series.BorderWidth = 2;
                chart.Series.Add(series);

                var series2 = new Series();
                series2.Name = "Comp";
                series2.Color = System.Drawing.Color.DarkBlue;
                series2.ChartType = SeriesChartType.Line;
                series2.XValueType = ChartValueType.DateTime;
                series2.MarkerStyle = MarkerStyle.Circle;
                series2.MarkerColor = Color.DarkBlue;
                series2.BorderWidth = 2;
                chart.Series.Add(series2);

                // bind the datapoints
                chart.Series["New"].Points.DataBindXY(lWeek, lNewCases);
                chart.Series["Comp"].Points.DataBindXY(lWeek, lCompCases);

                //chart.Legends.Add("NewComp");
                //chart.Legends["NewComp"].Position.Auto = true;
                //chart.Legends["NewComp"].Position = new ElementPosition(30, 5, 100, 20);

                //Title chartTitle = new Title();
                //chartTitle.Text = "Weekly New and Completed Cases";
                //chartTitle.Alignment = ContentAlignment.TopCenter;
                //chartTitle.Font = new Font("Aerial", 11);
                //chart.Titles.Add(chartTitle);
                
                // draw
                chart.Invalidate();

                // write out a file
                string cfg_chartdir = @"c:\temp\";
                chart.SaveImage(cfg_chartdir + "Weekgraph.png", ChartImageFormat.Png);

                chart = null;
            }

            catch (Exception ex)
            {

                //logError(ex.Message);


            }


        }

        static void SalesHistChart()
        {

            SqlConnection sqlConn360 = null;
            SqlCommand sqlCmd360 = null;
            SqlDataReader sqlDR360 = null;

            int miCurYear = DateTime.Now.Year;
            int miCurYear1 = miCurYear - 1;
            int miCurYear2 = miCurYear - 2;
            int miCurYear3 = miCurYear - 3;
            int miCurYear4 = miCurYear - 4;
            int miCurYear5 = miCurYear - 5;
            int miCurYear6 = miCurYear - 6;

            DateTime dtCurYear = DateTime.Now;
            DateTime dtCurYear1 = dtCurYear.AddYears(-1);
            DateTime dtCurYear2 = dtCurYear.AddYears(-2);
            DateTime dtCurYear3 = dtCurYear.AddYears(-3);
            DateTime dtCurYear4 = dtCurYear.AddYears(-4);
            DateTime dtCurYear5 = dtCurYear.AddYears(-5);
            DateTime dtCurYear6 = dtCurYear.AddYears(-6);

            int miCurYearCount = 0;
            int miCurYear1Count = 0;
            int miCurYear2Count = 0;
            int miCurYear3Count = 0;
            int miCurYear4Count = 0;
            int miCurYear5Count = 0;
            int miCurYear6Count = 0;

            decimal mdecCurYearAmount = 0;
            decimal mdecCurYear1Amount = 0;
            decimal mdecCurYear2Amount = 0;
            decimal mdecCurYear3Amount = 0;
            decimal mdecCurYear4Amount = 0;
            decimal mdecCurYear5Amount = 0;
            decimal mdecCurYear6Amount = 0;

            DateTime dPeriodBeg;
            DateTime dPeriodEnd;
            DateTime dUpdated;

            try
            {

       
                // 360
                sqlConn360 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd360 = new SqlCommand();

                sqlCmd360.CommandType = CommandType.Text;
                sqlCmd360.CommandText = "SELECT * FROM Dashboard_Sales_Hist";
                sqlCmd360.Connection = sqlConn360;
                sqlConn360.Open();

                sqlDR360 = sqlCmd360.ExecuteReader();

                // anythign returned?
                if (sqlDR360.HasRows)
                {

                    sqlDR360.Read();

                    miCurYearCount = (int)sqlDR360.GetInt32(0);
                    mdecCurYearAmount =  Math.Round(((decimal)sqlDR360.GetDecimal(1) * (decimal).001), 2);
                    miCurYear1Count = (int)sqlDR360.GetInt32(2);
                    mdecCurYear1Amount = mdecCurYear1Amount + Math.Round(((decimal)sqlDR360.GetDecimal(3) * (decimal).001), 2);
                    miCurYear2Count = miCurYear2Count + (int)sqlDR360.GetInt32(4);
                    mdecCurYear2Amount = mdecCurYear2Amount + Math.Round(((decimal)sqlDR360.GetDecimal(5) * (decimal).001), 2);
                    miCurYear3Count = miCurYear3Count + (int)sqlDR360.GetInt32(6);
                    mdecCurYear3Amount = mdecCurYear3Amount + Math.Round(((decimal)sqlDR360.GetDecimal(7) * (decimal).001), 2);
                    miCurYear4Count = miCurYear4Count + (int)sqlDR360.GetInt32(8);
                    mdecCurYear4Amount = mdecCurYear4Amount + Math.Round(((decimal)sqlDR360.GetDecimal(9) * (decimal).001), 2);
                    miCurYear5Count = miCurYear5Count + (int)sqlDR360.GetInt32(10);
                    mdecCurYear5Amount = mdecCurYear5Amount + Math.Round(((decimal)sqlDR360.GetDecimal(11) * (decimal).001), 2);
                    miCurYear6Count = miCurYear6Count + (int)sqlDR360.GetInt32(12);
                    mdecCurYear6Amount = mdecCurYear6Amount + Math.Round(((decimal)sqlDR360.GetDecimal(13) * (decimal).001), 2);
                    dPeriodBeg = (DateTime)sqlDR360.GetSqlDateTime(15);
                    dPeriodEnd = (DateTime)sqlDR360.GetSqlDateTime(16);
                }

                sqlConn360.Close();
                sqlDR360 = null;

                var lCases = new List<int>();
                var lYear = new List<DateTime>();

                lCases.Add(miCurYear6Count);
                lYear.Add(dtCurYear6);
                lCases.Add(miCurYear5Count);
                lYear.Add(dtCurYear5);
                lCases.Add(miCurYear4Count);
                lYear.Add(dtCurYear4);
                lCases.Add(miCurYear3Count);
                lYear.Add(dtCurYear3);
                lCases.Add(miCurYear2Count);
                lYear.Add(dtCurYear2);
                lCases.Add(miCurYear1Count);
                lYear.Add(dtCurYear1);
                lCases.Add(miCurYearCount);
                lYear.Add(dtCurYear);

                // create chart
                var chart = new Chart();
                chart.Size = new Size(600, 220);

                var chartArea = new ChartArea();
                chartArea.AxisX.LabelStyle.Format = "yyyy";
                //chartArea.AxisX.LabelStyle.Interval = 1;
                chartArea.AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Years;
                chartArea.AxisX.Title = "Year";
                chartArea.AxisY.Title = "New Cases";
                chartArea.AxisX.Interval = 1;

                chartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
                chartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
                chartArea.AxisX.LabelStyle.Font = new Font("Consolas", 8);
                chartArea.AxisY.LabelStyle.Font = new Font("Consolas", 8);
                chart.ChartAreas.Add(chartArea);

                var series = new Series();
                series.Name = "Years";
                //series.Color = System.Drawing.Color.DarkOrange;
                series.ChartType = SeriesChartType.Column;
                series.XValueType = ChartValueType.Int32;
                //series.MarkerColor = Color.DarkOrange;
                series.BorderWidth = 2;
                series.BorderColor = System.Drawing.Color.Transparent;
                series.IsValueShownAsLabel = true;
                series.IsXValueIndexed = true;
                series.LabelForeColor = Color.Black;
                //series.LabelFormat = "{#}%";
                chart.Series.Add(series);

                // bind the datapoints
                chart.Series["Years"].Points.DataBindXY(lYear, lCases);
                //series.Points[1].Color = System.Drawing.ColorTranslator.FromHtml("#006cc5");
                //series.Points[1].Color = System.Drawing.ColorTranslator.FromHtml("#81bd01");

                //int axisLblPos = 1;
                //Axis axisX = chartArea.AxisX;
                //axisX.CustomLabels.Add(axisLblPos,axisLblPos+1, "2015");

                //chart.Legends.Add("NewComp");
                //chart.Legends["NewComp"].Position.Auto = true;
                //chart.Legends["NewComp"].Position = new ElementPosition(30, 5, 100, 20);

                //Title chartTitle = new Title();
                //chartTitle.Text = "Weekly New and Completed Cases";
                //chartTitle.Alignment = ContentAlignment.TopCenter;
                //chartTitle.Font = new Font("Aerial", 11);
                //chart.Titles.Add(chartTitle);

                // draw
                chart.Invalidate();

                // write out a file
                string cfg_chartdir = @"c:\temp\";
                chart.SaveImage(cfg_chartdir + "RequestYear.png", ChartImageFormat.Png);
            }

            catch (Exception ex)
            {

                logError(ex.Message);

            }

        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static void logError(string sErrMsg)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();

            //record exception  
            oLU.WritetoLog("Default.aspx - " + sErrMsg);

            // close objects
            oLU.closeLog();

        }

    }
}
