﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace SurveyCountAuto
{
    class Program
    {
        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static string cfg_deliverto;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Range oRange = null;
        static string sReportDate;
        static string sReportTitle;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;
        static DataTable dt360;
        static DataTable dtFinal;
        static DataSet dsReportData;
        static DateTime dBegDate360;
        static DateTime dEndDate360;
        static int cfg_TimeZoneOffset;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_deliverto = colNameVal.Get("deliverto");
                //cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));
                cfg_TimeZoneOffset = getTimeZoneOffset();


                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                DateTime dt = new DateTime();


                dt = DateTime.Today.AddDays(-1);

                //Prior year
                //dt = dt.AddYears(-1);

                sReportDate = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();


                // set dates to beg/end of current month
                dBegDate360 = FirstDayOfMonth(dt);
                dEndDate360 = LastDayOfMonth(dt);

                string sDateDisp = dt.Month.ToString() + "/" + dt.Day.ToString() + "/" + dt.Year.ToString();
                sReportTitle = "Survey Count for " + dt.ToString("MMMM") + " " + dt.Year.ToString();
                string sEmailSubject = "Survey Count for " + dt.ToString("MMMM") + " " + dt.Day.ToString() + ", " + dt.Year.ToString();
                string sExcelFileNameNoEx = cfg_outputdir + "SurveyCount_" + dt.Month.ToString("") + dt.Day.ToString() + dt.Year.ToString();
                string sExcelFileName = sExcelFileNameNoEx + ".xlsx";
                int iRow = 0;
                int iRowTot = 0;
                int iGrandTot = 0;

                dt360 = new DataTable("360");
                dt360.Columns.Add("acnt", typeof(int));
                //dt360.Columns.Add("custname", typeof(string));
                dt360.Columns.Add("datereq", typeof(DateTime));
                dt360.Columns.Add("numreq", typeof(int));

                dtFinal = new DataTable("Final");

                // Create a DataSet.
                dsReportData = new DataSet("reportdata");
                dsReportData.Tables.Add(dt360);

                try
                {

                    int iCurCol = 1;

                    dtFinal.Columns.Add("acnt", typeof(int));
                    dtFinal.Columns.Add("custname", typeof(string));

                    while (iCurCol < 32)
                    {
                        dtFinal.Columns.Add(iCurCol.ToString(), typeof(int));
                        iCurCol++;
                    }

                    dtFinal.Columns.Add("rowtotal", typeof(int));

                    //DataView dvFinal = new DataView(dtFinal);
                    //dvFinal.Sort = "acnt ASC";


                    if (!proc360())
                    {
                        throw new Exception("Error loading data from 360");
                    }

                    if (!buildFinalTable())
                    {
                        throw new Exception("Error building final table.");
                    }

                    // sort table
                    dtFinal.DefaultView.Sort = "acnt ASC";
                    dtFinal = dtFinal.DefaultView.ToTable();


                    if (dtFinal.Rows.Count > 0)
                    {

                        oExcel = new Excel.Application();
                        oExcel.Visible = true;
                        oWorkbook = oExcel.Workbooks.Add(1);
                        o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                        o360Worksheet.Name = "360";
                        createHeader(o360Worksheet);

                        iRow = 4;

                        // loop through rows
                        foreach (DataRow row in dtFinal.Rows)
                        {
                            addData(o360Worksheet, iRow, 1, row[0].ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 2, row[1].ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                            addData(o360Worksheet, iRow, 3, row[2].ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 4, row[3].ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 5, row[4].ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 6, row[5].ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 7, row[6].ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 8, row[7].ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 9, row[8].ToString(), "I" + iRow.ToString(), "I" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 10, row[9].ToString(), "J" + iRow.ToString(), "J" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 11, row[10].ToString(), "K" + iRow.ToString(), "K" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 12, row[11].ToString(), "L" + iRow.ToString(), "L" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 13, row[12].ToString(), "M" + iRow.ToString(), "M" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 14, row[13].ToString(), "N" + iRow.ToString(), "N" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 15, row[14].ToString(), "O" + iRow.ToString(), "O" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 16, row[15].ToString(), "P" + iRow.ToString(), "P" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 17, row[16].ToString(), "Q" + iRow.ToString(), "Q" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 18, row[17].ToString(), "R" + iRow.ToString(), "R" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 19, row[18].ToString(), "S" + iRow.ToString(), "S" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 20, row[19].ToString(), "T" + iRow.ToString(), "T" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 21, row[20].ToString(), "U" + iRow.ToString(), "U" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 22, row[21].ToString(), "V" + iRow.ToString(), "V" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 23, row[22].ToString(), "W" + iRow.ToString(), "W" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 24, row[23].ToString(), "X" + iRow.ToString(), "X" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 25, row[24].ToString(), "Y" + iRow.ToString(), "Y" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 26, row[25].ToString(), "Z" + iRow.ToString(), "Z" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 27, row[26].ToString(), "AA" + iRow.ToString(), "AA" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 28, row[27].ToString(), "AB" + iRow.ToString(), "AB" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 29, row[28].ToString(), "AC" + iRow.ToString(), "AC" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 30, row[29].ToString(), "AD" + iRow.ToString(), "AD" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 31, row[30].ToString(), "AE" + iRow.ToString(), "AE" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 32, row[31].ToString(), "AF" + iRow.ToString(), "AF" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 33, row[32].ToString(), "AG" + iRow.ToString(), "AG" + iRow.ToString(), "", "C");

                            for (int i = 2; i < 32; i++)
                            {
                                iRowTot = iRowTot + Convert.ToInt32(row[i]);
                            }
                            addData(o360Worksheet, iRow, 34, iRowTot.ToString(), "AH" + iRow.ToString(), "AH" + iRow.ToString(), "#,##0", "C");

                            iRow++;
                            iRowTot = 0;

                        } // foreach

                        // total columns
                        int[] iaColTot = new int[34];
                        foreach (DataRow row in dtFinal.Rows)
                        {
                            for (int i = 2; i < 34; i++)
                            {
                                iaColTot[i] = iaColTot[i] + Convert.ToInt32(row[i]);
                            }
                        }

                        // grand total
                        for (int i = 2; i < 34; i++)
                        {
                            iGrandTot = iGrandTot + iaColTot[i];
                        }


                        addData(o360Worksheet, iRow, 3, iaColTot[2].ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 4, iaColTot[3].ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 5, iaColTot[4].ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 6, iaColTot[5].ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 7, iaColTot[6].ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 8, iaColTot[7].ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 9, iaColTot[8].ToString(), "I" + iRow.ToString(), "I" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 10, iaColTot[9].ToString(), "J" + iRow.ToString(), "J" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 11, iaColTot[10].ToString(), "K" + iRow.ToString(), "K" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 12, iaColTot[11].ToString(), "L" + iRow.ToString(), "L" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 13, iaColTot[12].ToString(), "M" + iRow.ToString(), "M" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 14, iaColTot[13].ToString(), "N" + iRow.ToString(), "N" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 15, iaColTot[14].ToString(), "O" + iRow.ToString(), "O" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 16, iaColTot[15].ToString(), "P" + iRow.ToString(), "P" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 17, iaColTot[16].ToString(), "Q" + iRow.ToString(), "Q" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 18, iaColTot[17].ToString(), "R" + iRow.ToString(), "R" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 19, iaColTot[18].ToString(), "S" + iRow.ToString(), "S" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 20, iaColTot[19].ToString(), "T" + iRow.ToString(), "T" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 21, iaColTot[20].ToString(), "U" + iRow.ToString(), "U" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 22, iaColTot[21].ToString(), "V" + iRow.ToString(), "V" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 23, iaColTot[22].ToString(), "W" + iRow.ToString(), "W" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 24, iaColTot[23].ToString(), "X" + iRow.ToString(), "X" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 25, iaColTot[24].ToString(), "Y" + iRow.ToString(), "Y" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 26, iaColTot[25].ToString(), "Z" + iRow.ToString(), "Z" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 27, iaColTot[26].ToString(), "AA" + iRow.ToString(), "AA" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 28, iaColTot[27].ToString(), "AB" + iRow.ToString(), "AB" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 29, iaColTot[28].ToString(), "AC" + iRow.ToString(), "AC" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 30, iaColTot[29].ToString(), "AD" + iRow.ToString(), "AD" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 31, iaColTot[30].ToString(), "AE" + iRow.ToString(), "AE" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 32, iaColTot[31].ToString(), "AF" + iRow.ToString(), "AF" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 33, iaColTot[32].ToString(), "AG" + iRow.ToString(), "AG" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 34, iGrandTot.ToString(), "AH" + iRow.ToString(), "AH" + iRow.ToString(), "#,##0", "C");


                        oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value,false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                        oWorkbook.Close(true, oMissing, oMissing);
                        oExcel.Quit();
                        msMsgBody += "Processing completed for 360" + System.Environment.NewLine + cfg_outputdir + sExcelFileNameNoEx + System.Environment.NewLine + System.Environment.NewLine;

                        releaseObject(oExcel);
                        releaseObject(oWorkbook);
                        releaseObject(o360Worksheet);
                        //sendExcelFile(sEmailSubject, sExcelFileName);
                        msMsgBody += "File sent " + sExcelFileName;

                    }   //(dtFinal.Rows.Count > 0)

                }   // try

                catch (Exception ex)
                {
                    //record exception   
                    oLU.WritetoLog(ex.Message);
                    mbErr = true;
                    msErrMsg = ex.Message;
                }

            }

            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(msMsgBody);
                }

            }
        }


        static bool proc360()
        {

            bool bRet = false;
            int iAcnt;
            string sAcnt;
            DateTime dDateReq;
            int iNumReq;


            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // init SQL connection for read query
                SqlDataReader sqlDR;
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;


                // fix beg / end date for 360 GMT
                dBegDate360 = dBegDate360.AddHours(cfg_TimeZoneOffset);
                dEndDate360 = dEndDate360.AddHours(cfg_TimeZoneOffset);

                // get items requested for the period
                sqlCmd1.CommandText = "sp_GetOrderedByCust";
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate360);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate360);
                sqlConn1.Open();
                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        dDateReq = sqlDR.GetDateTime(0);

                        if (sqlDR.IsDBNull(1))
                        {
                            sAcnt = "0";
                        }
                        else
                        {
                            sAcnt = sqlDR.GetSqlString(1).ToString();
                            if (sAcnt.Length == 0)
                                sAcnt = "0";
                        }
                        iAcnt = Convert.ToInt32(sAcnt);
                        iNumReq = (int)sqlDR.GetSqlInt32(2);

                        dt360.Rows.Add(iAcnt, dDateReq, iNumReq);

                    }	// while


                }	// has rows

                bRet = true;

            }
            catch (Exception ex)
            {

                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {
                if (sqlConn1 != null)

                    sqlConn1.Close();
            }

            return bRet;
        }

        static bool buildFinalTable()
        {

            bool bRet = false;
            DateTime dCurDate = dBegDate360;
            DateTime dLastDate = dEndDate360.AddDays(1);
            string sCurDate = "";
            int iAcnt = 0;
            int iNumReq = 0;
            int iWork = 0;
            string sCustName = "";

            try
            {
 
                foreach (System.Data.DataRow row in dt360.Rows)
                {

                    // get data from current row
                    dCurDate = (DateTime)row["datereq"];
                    iAcnt = (int)row["acnt"];
                    iNumReq = (int)row["numreq"];

                    // if row for acnt does not exist in final table - add it
                    System.Data.DataRow[] CombRow = dtFinal.Select("acnt = " + iAcnt);

                    if (CombRow.Length == 0)
                    {

                        sCustName = GetCustName(iAcnt.ToString());
                        dtFinal.Rows.Add(iAcnt,sCustName,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                    }

                    // add count to appropriate date column
                    sCurDate = dCurDate.Day.ToString();
                    CombRow = dtFinal.Select("acnt = " + iAcnt);
                    iWork = (int)CombRow[0][sCurDate];
                    CombRow[0][sCurDate] = iWork + iNumReq;

                }


                bRet = true;
            }
            catch (Exception ex)
            {

                //MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


            return bRet;

        }

        static string GetCustName(string sCustNum)
        {

            string sCustName = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetCustomerName";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@lookupid", sCustNum);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();


                object oRetVal = sqlCmd1.ExecuteScalar();

                if (oRetVal != null)
                {
                    sCustName = (string)oRetVal;
                }

            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

            finally
            {

                // close objects
                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

            return sCustName;
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeader(Excel._Worksheet oWorkSheet)
        {

            Excel.Range oRange;


            oWorkSheet.get_Range("A1", "AH1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "AH1");
            oRange.FormulaR1C1 = sReportTitle;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oRange = oWorkSheet.get_Range("A2", "A2");
            oRange.FormulaR1C1 = "Report date: " + sReportDate;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 9;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGray);

            oRange = oWorkSheet.get_Range("C2", "AH2");
            oRange.FormulaR1C1 = " ";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 9;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGray);


            oWorkSheet.Cells[3, 1] = "Account";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Name";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 45;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "1";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "2";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "3";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 6] = "4";
            oRange = oWorkSheet.get_Range("F3", "F3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 7] = "5";
            oRange = oWorkSheet.get_Range("G3", "G3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 8] = "6";
            oRange = oWorkSheet.get_Range("H3", "H3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 9] = "7";
            oRange = oWorkSheet.get_Range("I3", "I3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 10] = "8";
            oRange = oWorkSheet.get_Range("J3", "J3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 11] = "9";
            oRange = oWorkSheet.get_Range("K3", "K3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 12] = "10";
            oRange = oWorkSheet.get_Range("L3", "L3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 13] = "11";
            oRange = oWorkSheet.get_Range("M3", "M3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 14] = "12";
            oRange = oWorkSheet.get_Range("N3", "N3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 15] = "13";
            oRange = oWorkSheet.get_Range("O3", "O3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 16] = "14";
            oRange = oWorkSheet.get_Range("P3", "P3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 17] = "15";
            oRange = oWorkSheet.get_Range("Q3", "Q3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 18] = "16";
            oRange = oWorkSheet.get_Range("R3", "R3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 19] = "17";
            oRange = oWorkSheet.get_Range("S3", "S3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 20] = "18";
            oRange = oWorkSheet.get_Range("T3", "T3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 21] = "19";
            oRange = oWorkSheet.get_Range("U3", "U3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 22] = "20";
            oRange = oWorkSheet.get_Range("V3", "V3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 23] = "21";
            oRange = oWorkSheet.get_Range("W3", "W3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 24] = "22";
            oRange = oWorkSheet.get_Range("X3", "X3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 25] = "23";
            oRange = oWorkSheet.get_Range("Y3", "Y3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 26] = "24";
            oRange = oWorkSheet.get_Range("Z3", "Z3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 27] = "25";
            oRange = oWorkSheet.get_Range("AA3", "AA3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 28] = "26";
            oRange = oWorkSheet.get_Range("AB3", "AB3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 29] = "27";
            oRange = oWorkSheet.get_Range("AC3", "AC3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 30] = "28";
            oRange = oWorkSheet.get_Range("AD3", "AD3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 31] = "29";
            oRange = oWorkSheet.get_Range("AE3", "AE3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 32] = "30";
            oRange = oWorkSheet.get_Range("AF3", "AF3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 33] = "31";
            oRange = oWorkSheet.get_Range("AG3", "AG3");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 34] = "Total";
            oRange = oWorkSheet.get_Range("AH3", "AH3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        }




        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2, string format, string sHorizAlign)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }

        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "Survey Count";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Survey Count Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = cfg_deliverto;

                //oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }
        }

        static int getTimeZoneOffset()
        {
            SqlConnection sqlConn = null;
            SqlCommand sqlCmd = null;
            int iRetVal = 0;

            try
            {
                //string sQuery = @"SELECT dbo.TimeZoneOffset AS TZOffset;";
                sqlConn = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd = new SqlCommand("SELECT dbo.TimeZoneOffset()", sqlConn);
                sqlConn.Open();
                Int32 functionResult = (Int32)sqlCmd.ExecuteScalar();

                iRetVal = functionResult;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }
            finally
            {

                // close objects
                if (sqlConn != null)
                    sqlConn.Close();
            }

            return iRetVal;
        }
    }
}

