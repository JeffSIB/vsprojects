﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace QAStats
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static int cfg_TimeZoneOffset;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        //static Excel.Worksheet oSIBWorksheet = null;
        static Excel.Worksheet o360Worksheet = null;
        //static Excel.Worksheet o360MWorksheet = null;
        //static Excel.Worksheet o360IEWorksheet = null;
        static Excel.Range oRange = null;
        static DateTime dBegDate;
        static DateTime dEndDate;
        static DateTime dBegDate360;
        static DateTime dEndDate360;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;
        static string sMode;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                //cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));
                cfg_TimeZoneOffset = getTimeZoneOffset();


                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //  N = daily - after 4am for previous day
                //  T = current day - Tiana
                //  M = monthly
                //  C = custom
                sMode = "";
                if (args.Length < 1)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                DateTime dt = new DateTime();

                if (args[0].ToUpper() == "W")
                {
                    // ** Weekly **
                    // Assumes that it is being run on Monday
                    // Results are for previous Sun-Sat

                    sMode = "W";
                    dt = DateTime.Today;

                    // Set beg date to previous Sunday - Sat
                    dBegDate = dt.AddDays(-8);
                    dEndDate = dt.AddDays(-2);
               }
                else if (args[0].ToUpper() == "M")
                {

                    ///////////////////////////////////////////
                    // **Monthly**
                    // Runs for previous month

                    sMode = "M";                 
                    dt = DateTime.Today;
                    dt = dt.AddMonths(-1);

                    // Set beg date to first/last day of previous month
                    dBegDate = FirstDayOfMonth(dt);
                    dEndDate = LastDayOfMonth(dt);
                }
                else if (args[0].ToUpper() == "C")
                {

                    ///////////////////////////////////////////
                    // **Custom Date Range**
                    sMode = "C";
                    dBegDate = Convert.ToDateTime(args[1]);
                    dEndDate = Convert.ToDateTime(args[2]);
                }
                else if (args[0].ToUpper() == "T")
                {

                    ///////////////////////////////////////////
                    // **Current day** for Tiana
                    sMode = "T";
                    dt = DateTime.Today;
                    dEndDate = dt;
                    dBegDate = dt;
                }
                else if (args[0].ToUpper() == "N")
                {
                    ///////////////////////////////////////////
                    // **Run after 4am for previous day**
                    sMode = "N";
                    dt = DateTime.Today;
                    dEndDate = dt.AddDays(-1);
                    dBegDate = dt.AddDays(-1);
                }

                else
                {
                    throw new SystemException("Invalid period arguments supplied");
                }

                string sDateDisp = dBegDate.Month + "-" + dBegDate.Day + "-" + dBegDate.Year.ToString();
                string sEmailSubject = "QA stats - " + dBegDate.Month + "/" + dBegDate.Day + "/" + dBegDate.Year.ToString();
                string sExcelFileNameNoEx = cfg_outputdir + "QAStats_" + sDateDisp;

                if (sMode == "T")
                {
                    sExcelFileNameNoEx += DateTime.Now.Hour.ToString();
                
                }
                string sExcelFileName = sExcelFileNameNoEx + ".xlsx";

                ///////////////////////////////////////////////////////////////               
                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                int iCompleted = 0;
                decimal dInvoiced = 0;
                decimal dPaid = 0;
                decimal dMargin = 0;
                string sUser = "";
                int iCaseNum = 0;
                DateTime dDateComp;
                string sFieldRep = "";
                int iRow = 0;
                bool bDataFound = false;

                oLU.WritetoLog("Mode: " + sMode);

                ///////////////////////////////////////////////////
                // Process
                ///////////////////////////////////////////////////
                oLU.WritetoLog("Processing");

                // set time to 12:00am
                // adjust for 360 GMT
                dBegDate = ChangeTime(dBegDate, 0, 0, 0, 0);
                dBegDate360 = dBegDate.AddHours(cfg_TimeZoneOffset);

                // set time to midnight
                dEndDate = ChangeTime(dEndDate, 23, 59, 59, 0);
                dEndDate360 = dEndDate.AddHours(cfg_TimeZoneOffset);

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_QAStats";
                sqlCmd1.CommandTimeout = 360;
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate360);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate360);
                sqlConn1.Open();

                oLU.WritetoLog("SQL call");
                sqlReader = sqlCmd1.ExecuteReader();

                oLU.WritetoLog("Open Excel");
                oExcel = new Excel.Application();
                oExcel.Visible = true;
                oWorkbook = oExcel.Workbooks.Add(1);
                o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];

                o360Worksheet.Name = "QA Stats";
                createHeader(o360Worksheet, "Main");

                iRow = 4;

                if (sqlReader.HasRows)
                {

                    bDataFound = true;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //completed
                        if (sqlReader.IsDBNull(0))
                        {
                            iCompleted = 0;
                        }
                        else
                        {
                            iCompleted = (int)sqlReader.GetSqlInt32(0);
                        }
                        addData(o360Worksheet, iRow, 1, iCompleted.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "#,##0");


                        //invoiced
                        if (sqlReader.IsDBNull(1))
                        {
                            dInvoiced = 0;
                        }
                        else
                        {
                            dInvoiced = (decimal)sqlReader.GetSqlDecimal(1);
                        }
                        addData(o360Worksheet, iRow, 2, dInvoiced.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0.00");


                        //paid
                        if (sqlReader.IsDBNull(2))
                        {
                            dPaid = 0;
                        }
                        else
                        {
                            dPaid = (decimal)sqlReader.GetSqlDecimal(2);
                        }
                        addData(o360Worksheet, iRow, 3, dPaid.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0.00");


                        //margin
                        if (sqlReader.IsDBNull(3))
                        {
                            dMargin = 0;
                        }
                        else
                        {
                            dMargin = (decimal)sqlReader.GetSqlDecimal(3);
                        }
                        addData(o360Worksheet, iRow, 4, dMargin.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "##0.00%");


                        //user
                        if (sqlReader.IsDBNull(4))
                        {
                            sUser = "";
                        }
                        else
                        {
                            sUser = (string)sqlReader.GetSqlString(4);
                        }
                        addData(o360Worksheet, iRow, 5, sUser.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "");

                        iRow++;

                    } while (sqlReader.Read());     

                }   // has rows

                else
                {
                    addDataNoFormat(o360Worksheet, iRow, 1, "No items found", "A" + iRow.ToString(), "A" + iRow.ToString());
                }


                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;
                oLU.WritetoLog("SQL done");

                oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                    oWorkbook.Close(true, oMissing, oMissing);
                    oExcel.Quit();
                    msMsgBody += "Processing completed" + System.Environment.NewLine + cfg_outputdir + "ReviewerStatsSIB_" + sDateDisp + System.Environment.NewLine + System.Environment.NewLine;
                oLU.WritetoLog("File saved");

                releaseObject(oExcel);
                releaseObject(oWorkbook);
                //releaseObject(o360MWorksheet);
                releaseObject(o360Worksheet);
                //releaseObject(o360IEWorksheet);

                oLU.WritetoLog("Sending Excel");
                sendExcelFile(sEmailSubject, sExcelFileName);
                msMsgBody += "File sent " + sExcelFileName;

                oLU.WritetoLog("Done");

            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(msMsgBody);                
                }            
            }
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeader(Excel._Worksheet oWorkSheet, string sMode)
        {

            Excel.Range oRange;

            oWorkSheet.get_Range("A1", "E1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "E1");
            oRange.FormulaR1C1 = "QA Stats " + dBegDate.ToShortDateString();

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Completed";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Invoiced";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "Paid";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Margin";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Comp By";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);     
                
        }
              

        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data, 
			string cell1, string cell2,string format)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }


        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "QA Stats";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by QA Stats Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = "tiana@sibfla.com; axel@sibfla.com";
                //oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

        static int getTimeZoneOffset()
        {
            SqlConnection sqlConn = null;
            SqlCommand sqlCmd = null;
            int iRetVal = 0;

            try
            {
                //string sQuery = @"SELECT dbo.TimeZoneOffset AS TZOffset;";
                sqlConn = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd = new SqlCommand("SELECT dbo.TimeZoneOffset()", sqlConn);
                sqlConn.Open();
                Int32 functionResult = (Int32)sqlCmd.ExecuteScalar();

                iRetVal = functionResult;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }
            finally
            {

                // close objects
                if (sqlConn != null)
                    sqlConn.Close();
            }

            return iRetVal;
        }



    }
}
