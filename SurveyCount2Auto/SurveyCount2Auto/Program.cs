﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace SurveyCount2Auto
{
    class Program
    {
        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static string cfg_deliverto;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Range oRange = null;
        static string sReportDate;
        static string sReportTitle;
        static string sPrevMonthColHdr;
        static string sCurMonthColHdr;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;
        static DataTable dt360;
        static DataSet dsReportData;
        static DateTime dBegDate360;
        static DateTime dEndDate360;
        static int cfg_TimeZoneOffset;
        static string sMode;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_deliverto = colNameVal.Get("deliverto");
                //cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));
                cfg_TimeZoneOffset = getTimeZoneOffset();


                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //  W = weekly - Monday afer midnight for prev week - skip if 1st falls on Monday
                //  M = monthly - 1st day of month for prev month 
                //  C = custom
                sMode = "";
                if (args.Length < 1)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                // set report period based on parammeter passed
                DateTime dt = new DateTime();

                if (args[0].ToUpper() == "W")
                {
                    // ** Weekly **
                    // Assumes that it is being run on Monday am
                    // Current period is current month to run date
                    // Skip if run date is 1st of the month and day of week = Monday

                    sMode = "W";
                    dt = DateTime.Today;

                    // Skip if run date is 1st of the month and day of week = Monday
                    if (dt.Day == 1 && dt.DayOfWeek == DayOfWeek.Monday)
                        throw new SystemException("Report does not run when 1st day of the month is Monday");

                    // set dates to beg/end of current month
                    dBegDate360 = FirstDayOfMonth(dt);
                    dEndDate360 = dt.AddDays(-1);
                }
                else if (args[0].ToUpper() == "M")
                {

                    ///////////////////////////////////////////
                    // **Monthly**
                    // Runs for previous month

                    sMode = "M";
                    dt = DateTime.Today;
                    dt = dt.AddMonths(-1);

                    // Set beg date to first/last day of previous month
                    dBegDate360 = FirstDayOfMonth(dt);
                    dEndDate360 = LastDayOfMonth(dt);
                }
                else if (args[0].ToUpper() == "C")
                {

                    ///////////////////////////////////////////
                    // **Custom Date Range**
                    sMode = "C";
                    //dBegDate = Convert.ToDateTime(args[1]);
                    //dEndDate = Convert.ToDateTime(args[2]);
                }
                else
                {
                    throw new SystemException("Invalid period argument supplied");
                }

                // set report run date
                sReportDate = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                // Set report title
                string sDateDisp = dt.Month.ToString() + "/" + dt.Day.ToString() + "/" + dt.Year.ToString();
                sReportTitle = "Survey Count for " + dt.ToString("MMMM") + " " + dt.Year.ToString();
                string sEmailSubject = sReportTitle;
                string sExcelFileNameNoEx = cfg_outputdir + "SurveyCount2_" + dt.Month.ToString() + dt.Day.ToString() + dt.Year.ToString();
                string sExcelFileName = sExcelFileNameNoEx + ".xlsx";
                int iRow = 0;
                int iPrevYearTot = 0;
                int iCurYearTot = 0;

                sPrevMonthColHdr = dt.AddYears(-1).ToString("MMM") + " " + dt.AddYears(-1).Year.ToString();
                sCurMonthColHdr = dt.ToString("MMM") + " " + dt.Year.ToString();

                // Create a DataSet.
                dsReportData = new DataSet("reportdata");
                dt360 = new DataTable("360");
                dsReportData.Tables.Add(dt360);

                try
                {
                    dt360.Columns.Add("acnt", typeof(int));
                    dt360.Columns.Add("custname", typeof(string));
                    dt360.Columns.Add("prevyear", typeof(int));
                    dt360.Columns.Add("curyear", typeof(int));
                    dt360.Columns.Add("pctchange", typeof(double));

                    if (!proc360())
                    {
                        throw new Exception("Error loading data from 360");
                    }

                    calcPctChange();

                    // sort table
                    dt360.DefaultView.Sort = "acnt ASC";
                    dt360 = dt360.DefaultView.ToTable();


                    if (dt360.Rows.Count > 0)
                    {

                        oExcel = new Excel.Application();
                        oExcel.Visible = true;
                        oWorkbook = oExcel.Workbooks.Add(1);
                        o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                        o360Worksheet.Name = "360";
                        createHeader(o360Worksheet, sPrevMonthColHdr, sCurMonthColHdr);

                        iRow = 4;
                        int iBGColor = 0;
                        double dPct = 0;

                        // loop through rows
                        foreach (DataRow row in dt360.Rows)
                        {
                            addData(o360Worksheet, iRow, 1, row[0].ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 2, row[1].ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                            addData(o360Worksheet, iRow, 3, row[2].ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                            addData(o360Worksheet, iRow, 4, row[3].ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                            dPct = (double)row[4];
                            if (dPct == -1)
                            {
                                dPct = 100;
                                iBGColor = 2;
                            }
                            else if (dPct == 1)
                            {
                                dPct = 100;
                                iBGColor = 1;
                            }
                            else if (dPct <= -5)
                                iBGColor = 2;
                            else if (dPct >= 5)
                                iBGColor = 1;
                            else
                                iBGColor = 3;

                            addDataWColor(o360Worksheet, iRow, 5, dPct.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "##0.0", "C",iBGColor);

                            iPrevYearTot += Convert.ToInt32(row[2]);
                            iCurYearTot += Convert.ToInt32(row[3]);

                            iRow++;

                        } // foreach


                        // Totals
                        addData(o360Worksheet, iRow, 1, "", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                        addData(o360Worksheet, iRow, 2, "Period Totals", "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                        addData(o360Worksheet, iRow, 3, iPrevYearTot.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                        addData(o360Worksheet, iRow, 4, iCurYearTot.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");

                        // Calc pct
                        double dPctChange = 0;
                        int iDiff = 0;
                        // prev year = 0, cur year has value, pct change = 100
                        if (iPrevYearTot == 0)
                        {
                            dPctChange = 1;
                        }
                        // cur year = 0, prev year has value, pct change = -100
                        else if (iCurYearTot == 0)
                        {
                            dPctChange = -1;
                        }
                        // diff between years
                        else
                        {
                            iDiff = iCurYearTot - iPrevYearTot;

                            // pct change
                            if (iDiff == 0)
                            {
                                dPctChange = 0;
                            }
                            else
                            {
                                dPctChange = (Convert.ToDouble(iDiff) / Convert.ToDouble(iPrevYearTot)) * 100;
                            }
                        }


                        dPct = dPctChange;
                        if (dPct == -1)
                        {
                            dPct = 100;
                            iBGColor = 2;
                        }
                        else if (dPct == 1)
                        {
                            dPct = 100;
                            iBGColor = 1;
                        }
                        else if (dPct <= -5)
                            iBGColor = 2;
                        else if (dPct >= 5)
                            iBGColor = 1;
                        else
                            iBGColor = 3;

                        addDataWColor(o360Worksheet, iRow, 5, dPct.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "##0.0", "C", iBGColor);


                        oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                        oWorkbook.Close(true, oMissing, oMissing);
                        oExcel.Quit();
                        msMsgBody += "Processing completed for 360" + System.Environment.NewLine + cfg_outputdir + sExcelFileNameNoEx + System.Environment.NewLine + System.Environment.NewLine;

                        releaseObject(oExcel);
                        releaseObject(oWorkbook);
                        releaseObject(o360Worksheet);
                        sendExcelFile(sEmailSubject, sExcelFileName);
                        msMsgBody += "File sent " + sExcelFileName;

                    }   //(dtFinal.Rows.Count > 0)

                }   // try

                catch (Exception ex)
                {
                    //record exception   
                    oLU.WritetoLog(ex.Message);
                    mbErr = true;
                    msErrMsg = ex.Message;
                }

            }

            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(msMsgBody);
                }

            }
        }


        static bool proc360()
        {

            bool bRet = false;
            int iAcnt;
            string sAcnt;
            int iNumReq;
            string sCustName = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // init SQL connection for read query
                SqlDataReader sqlDR;
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;

                // fix beg / end date for 360 GMT
                dBegDate360 = dBegDate360.AddHours(cfg_TimeZoneOffset);
                dEndDate360 = dEndDate360.AddHours(24+cfg_TimeZoneOffset);

                // get items requested for the current month to date
                sqlCmd1.CommandText = "sp_GetOrderedByCustPeriod";
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate360);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate360);
                sqlConn1.Open();
                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        if (sqlDR.IsDBNull(0))
                        {
                            sAcnt = "0";
                        }
                        else
                        {
                            sAcnt = sqlDR.GetSqlString(0).ToString();
                            if (sAcnt.Length == 0)
                                sAcnt = "0";
                        }
                        sCustName = GetCustName(sAcnt);
                        iAcnt = Convert.ToInt32(sAcnt);
                        iNumReq = (int)sqlDR.GetSqlInt32(1);

                        dt360.Rows.Add(iAcnt, sCustName, 0, iNumReq, 0);

                    }	// while


                }   // has rows
                sqlDR.Close();

                // get items requested for the prior year month 
                dBegDate360 = dBegDate360.AddYears(-1);
                dEndDate360 = dEndDate360.AddYears(-1);

                sqlCmd1.CommandText = "sp_GetOrderedByCustPeriod";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate360);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate360);
                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        if (sqlDR.IsDBNull(0))
                        {
                            sAcnt = "0";
                        }
                        else
                        {
                            sAcnt = sqlDR.GetSqlString(0).ToString();
                            if (sAcnt.Length == 0)
                                sAcnt = "0";
                        }
                        iAcnt = Convert.ToInt32(sAcnt);
                        iNumReq = (int)sqlDR.GetSqlInt32(1);

                        // if row for acnt does not exist in table - add it
                        System.Data.DataRow[] newRow = dt360.Select("acnt = " + iAcnt);

                        if (newRow.Length == 0)
                        {

                            sCustName = GetCustName(iAcnt.ToString());
                            dt360.Rows.Add(iAcnt, sCustName, iNumReq, 0, 0);
                        }

                        // add prev year count
                        newRow = dt360.Select("acnt = " + iAcnt);
                        newRow[0][2] = iNumReq;

                    }	// while


                }   // has rows
                sqlDR.Close();

                bRet = true;

            }
            catch (Exception ex)
            {

                oLU.WritetoLog(ex.Message);

            }

            finally
            {
                if (sqlConn1 != null)

                    sqlConn1.Close();
            }

            return bRet;
        }

        static bool calcPctChange()
        {

            bool bRet = false;
            int iAcnt = 0;
            int iPrevYear = 0;
            int iCurYear = 0;
            int iDiff = 0;
            double dPctChange = 0;

            try
            {
 
                foreach (System.Data.DataRow row in dt360.Rows)
                {

                    // get data from current row
                    iAcnt = (int)row["acnt"];
                    iPrevYear = (int)row["prevyear"];
                    iCurYear = (int)row["curyear"];

                    // prev year = 0, cur year has value, pct change = 100
                    if (iPrevYear == 0)
                    {
                        dPctChange = 1;
                    }
                    // cur year = 0, prev year has value, pct change = -100
                    else if (iCurYear == 0)
                    {
                        dPctChange = -1;
                    }
                    // diff between years
                    else
                    {
                        iDiff = iCurYear - iPrevYear;

                        // pct change
                        if (iDiff == 0)
                        {
                            dPctChange = 0;
                        }
                        else
                        {
                            dPctChange = (Convert.ToDouble(iDiff) / Convert.ToDouble(iPrevYear)) * 100;
                        }
                    }

                    // 
                    System.Data.DataRow[] drRow = dt360.Select("acnt = " + iAcnt);
                    drRow[0]["pctchange"] = dPctChange ;

                }


                bRet = true;
            }
            catch (Exception ex)
            {

                oLU.WritetoLog(ex.Message);

            }


            return bRet;

        }

        static string GetCustName(string sCustNum)
        {

            string sCustName = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetCustomerName";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@lookupid", sCustNum);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();


                object oRetVal = sqlCmd1.ExecuteScalar();

                if (oRetVal != null)
                {
                    sCustName = (string)oRetVal;
                }

            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

            finally
            {

                // close objects
                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

            return sCustName;
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeader(Excel._Worksheet oWorkSheet, string sPrevMonthColHdr, string sCurMonthColHdr)
        {

            Excel.Range oRange;
            
            oWorkSheet.get_Range("A1", "AE1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "AE1");
            oRange.FormulaR1C1 = sReportTitle;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oRange = oWorkSheet.get_Range("A2", "A2");
            oRange.FormulaR1C1 = "Report date: " + sReportDate;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 9;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGray);

            oRange = oWorkSheet.get_Range("C2", "E2");
            oRange.FormulaR1C1 = " ";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 9;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGray);


            oWorkSheet.Cells[3, 1] = "Account";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Name";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 45;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = sPrevMonthColHdr;
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = sCurMonthColHdr;
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Pct Change";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

       
        }




        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2, string format, string sHorizAlign)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
        }

        static void addDataWColor(Excel._Worksheet oWorkSheet, int row, int col, string data,
           string cell1, string cell2, string format, string sHorizAlign, int ibgColor)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }

            if (ibgColor == 1)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.PaleGreen);
            }
            else if (ibgColor == 2)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightPink);
            }
            else if (ibgColor == 3)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            }

        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }

        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "Survey Count Weekly";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Survey Count2 Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = cfg_deliverto;

                //oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }

        static int getTimeZoneOffset()
        {
            SqlConnection sqlConn = null;
            SqlCommand sqlCmd = null;
            int iRetVal = 0;

            try
            {
                //string sQuery = @"SELECT dbo.TimeZoneOffset AS TZOffset;";
                sqlConn = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd = new SqlCommand("SELECT dbo.TimeZoneOffset()", sqlConn);
                sqlConn.Open();
                Int32 functionResult = (Int32)sqlCmd.ExecuteScalar();

                iRetVal = functionResult;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }
            finally
            {

                // close objects
                if (sqlConn != null)
                    sqlConn.Close();
            }

            return iRetVal;
        }
    }
}
