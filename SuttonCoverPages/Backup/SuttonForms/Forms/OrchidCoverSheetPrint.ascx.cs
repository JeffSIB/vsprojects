﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text.RegularExpressions;
using FSMData;
using LC360Web;

namespace Utilant.Forms.Forms
{
    public partial class OrchidCoverSheetPrint : Utilant.Web.UI.WebControls.CaseForm
    {
        public int TotalNumberOfHazards { get; set; }
        public int TotalNumberOfRecommendations { get; set; }
        public int? TotalNumRecs { get; set; }

        public class GenericField
        {
            public string FieldName { get; set; }
            public string Value { get; set; }
        }

        private List<GenericField> GenericFields;
        private bool bUpdate;

        private DataRow ResExteriorRow = null;
        private DataRow ResExteriorE2VRow = null;
        private DataRow ResExteriorE2VUpdtRow = null;
        private DataRow ResIntExtRow = null;
        private DataRow ResIntExtUpdtRow = null;
        private DataRow ResIntExtUpdtNoE2VRow = null;


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindGeneralCaseInformation();
                BindPhotos();
                BindHazards();
                BindFormData();
                TotalNumRecs = (int)GetRecommendations(TotalNumberOfRecommendations);

                //if (ResIntExtUpdtNoE2VRow != null || ResIntExtUpdtRow != null || ResExteriorE2VUpdtRow != null)
                //{
                //    bUpdate = true;
                //    divUpdates.Visible = true;
                //    litUpdates.Text = "UPDATES=TRUE";
                //}
                //else
                //{
                //    bUpdate = false;
                //    divUpdates.Visible = false;
                //    litUpdates.Text = "UPDATES=FALSE";
                //}

                if (bUpdate)
                {
                    divUpdates.Visible = true;
                }
                else
                {
                    divUpdates.Visible = false;
                }

            }
            catch
            {

            }
        }

        private void BindHazards()
        {
            var chs = Case.CaseHazards.Where(ch => ch.CaseForm.Deleted == false).OrderByDescending(ch => ch.HazardScore);
            foreach (var ch in chs)
            {
                this.litHazards.Text += ch.Description + "<br/>";
            }
        }
        
        private List<GenericField> GetGenericFields()
        {
            if (Case == null || Case.CaseGenericFields == null) return new List<GenericField>();

            var genericFields = Case.CaseGenericFields.Select(gf => new GenericField
            {
                FieldName = gf.CaseTypeGenericField == null ? "" : gf.CaseTypeGenericField.Name,
                Value = gf.Value ?? ""
            }).ToList();

            return genericFields;
        }

        public GenericField GetGenericFieldByFieldName(string fieldName)
        {
            if (fieldName == null) return null;
            return GenericFields.FirstOrDefault(gf => gf.FieldName.ToLower() == fieldName.ToLower());
        }

        private void BindGeneralCaseInformation()
        {
            /* Company Header Information */
            try
            {
                this.litCompanyAddress.Text = "5656 Central Avenue";
                this.litCompanyCityStateZip.Text = "St. Petersburg, FL 33707-1718";
                this.litCompanyCompletedForName.Text = Case.Customer.Name;
                this.litPhone.Text = Constants.CompanyPhone;
                this.litFax.Text = Constants.CompanyFax;
            }
            catch (Exception e)
            {
                this.litCompanyAddress.Text = "Error loading address";
                this.litCompanyCompletedForName.Text = "";
                this.litPhone.Text = "727-555-1234";
                this.litFax.Text = "727-555-1234";                
            }

            if (Case.CaseType.Name.ToUpper().Contains("UPDATE"))
            {
                bUpdate = true;
            }

            /* Left-hand columns */
            this.litInsuredName.Text = Case.PolicyHolderName;
            //this.litInsuredAddress.Text = Case.Address1.Line1;
            //this.litCityStateZip.Text = Case.Address1.PostalCode.City + ", " + Case.Address1.PostalCode.StateName + ", " + Case.Address1.PostalCode.PostalCodeValue;
            this.litLocationAddress.Text = Case.Address.Line1 + " " + Case.Address.City + ", " + Case.Address.PostalCode.StateName + ", " + Case.Address.PostalCode.PostalCodeValue;
            //this.litLocationCityStateZip.Text = Case.Address.City + ", " + Case.Address.PostalCode.StateName + ", " + Case.Address.PostalCode.PostalCodeValue;

            /* Right-hand columns */
            this.litPolicyNumber.Text = Case.PolicyNumber;

            Regex removeTime = new Regex(" 12:00:00 AM");
            this.litInspectionDate.Text = removeTime.Replace(GetValueOrDefault("DateInspected"), "");
            if (Case.Producer != null)
            {
                this.litAgent.Text = Case.Producer.Name;
                this.litAgentPhone.Text = Case.Producer.ContactPhone;
            }
            if (Case.Underwriter != null)
                this.litUnderwriter.Text = Case.Underwriter.FullName;
        }

        //TODO: Remove hacky regexs and add in some type safety maybe.
        private void BindFormData()
        {
            /* Occupancy / Construction */
            //this.litOccupied.Text = GetValueOrDefault("Occupancy", null, null, null, null);
            //this.litConstruction.Text = GetValueOrDefault("ExtWallSupportsPercent", "ExtWallSupportsPercent", "ExtWallConstruction", null, "ExtWallConst");
            
            
            


            /* Fire Protection / Breed of Dog */
            //this.litProtectionClass.Text = GetValueOrDefault("ProtectionClass", false);
            //this.litBreed.Text = GetValueOrDefault("DogBredd", "DogBredd", "DogBredd", "IfDogs", null);
            
            /* Water Exposure */

            Regex removeDecimal = new Regex("\\.0+$");
            //this.litGulf.Text = removeDecimal.Replace(GetValueOrDefault("GulfMiles", "GulfMiles", "GulfMiles", "GulfMiles", "GlufMiles"), " miles");
            //this.litAtlOcean.Text = removeDecimal.Replace(GetValueOrDefault("AtlanticMiles", "AtlanticMiles", "AtlanticMiles", "AtlanticMiles", "AtlanticMlies"), " miles");

            /* Replacement Cost Analysis */
            //decimal? coverageAOut = GetCoverageA();
            //this.litBuildingCoverage.Text = Case.CaseField.CoverageAIn.HasValue ? Math.Ceiling(Case.CaseField.CoverageAIn.Value).ToString("C0") : "N/A";
            //this.litReplacmenetCost.Text = coverageAOut.HasValue ? Math.Ceiling(coverageAOut.Value).ToString("C0") : "N/A";
            //this.litITV.Text = GetITV(Case.CaseField.CoverageAIn, coverageAOut);// Coverage A In / Coverage A Out

            //List<GenericField> GenericFields = GetGenericFields();


            try
            {

                // Get values from Orchid stored in Generic fields
                var genericFields = Case.CaseGenericFields.Select(gf => new { FieldName = gf.CaseTypeGenericField.Name, gf.Value }).ToList();
                CaseGenericField gfUsage = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "Usage");
                CaseGenericField gfOccupancy = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "Occupancy");
                CaseGenericField gfDwellingType = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "DwellingType");
                CaseGenericField gfYearBuilt = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "YearBuiltOrchid");
                CaseGenericField gfStories = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "Stories");
                CaseGenericField gfExtWallType = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "ExteriorWallType");
                CaseGenericField gfExtExtCoating = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "ExteriorCoating");
                CaseGenericField gfRoofGeo = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "RoofGeometry");
                CaseGenericField gfRoofType = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "RoofType");
                CaseGenericField gfRoofYear = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "RoofYear");
                CaseGenericField gfRoofCond = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "RoofCondition");
                CaseGenericField gfBurgAlarm = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "CentralStationBurglarAlarmPresent");
                CaseGenericField gfFireAlarm = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "CentralStationFireAlarmPresent");
                CaseGenericField gfCentralAlarm = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "CentralAlarmPresent");
                CaseGenericField gfPoolPresent = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "PoolPresent");
                CaseGenericField gfPoolAboveGround = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "PoolAboveGround");
                CaseGenericField gfPoolScreenEnclosurePresent = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "PoolScreenEnclosurePresent");
                CaseGenericField gfPoolFencePresent = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "PoolFencePresent");
                CaseGenericField gfPoolSlidePresent = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "PoolSlidePresent");
                CaseGenericField gfDivingBoard = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "DivingBoardOrPlatformPresent");
                CaseGenericField gfProtectionClass = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "ProtectionClass");
                CaseGenericField gfDistanceCoast = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "DistanceCoast");
                CaseGenericField gfDistanceCoast2 = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "DistanceCoast ");
                CaseGenericField gfPets = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "Dog");
                CaseGenericField gfWindStorm = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "WindstormProtectiveDevices");
                CaseGenericField gfPlumbUpdates = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "PlumbingUpdatesOrchid");
                CaseGenericField gfPlumbFP = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "PlumbingFull/PartialUpdateOrchid");
                CaseGenericField gfPlumbYear = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "PlumbingUpdateYearOrchid");
                CaseGenericField gfHVACUpdates = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "HvacUpdatesOrchid");
                CaseGenericField gfHVACFP = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "HvacFull/PartialUpdateOrchid");
                CaseGenericField gfHVACYear = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "HvacUpdateYearOrchid");
                CaseGenericField gfWiringUpdates = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "ElectricalUpdatesOrchid");
                CaseGenericField gfWiringFP = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "ElectricalFull/PartialUpdateOrchid");
                CaseGenericField gfWiringYear = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "ElectricalUpdateYearOrchid");



                //GenericField gfUsage = GetGenericFieldByFieldName("UsageOrchid");
                //GenericField gfDwellingType = GetGenericFieldByFieldName("DwellingType");
                //GenericField gfYearBuilt = GetGenericFieldByFieldName("YearBuiltOrchid");
                //GenericField gfStories = GetGenericFieldByFieldName("Stories");

                /* Y/N Questions */

                // Occupancy
                // Res Ext -> UsageCorrect
                this.litOccupancy.Text = gfOccupancy.Value;
                string sOccupancyYN = GetValueOrDefault("UsageCorrect");
                this.rbOccupancyY.Checked = sOccupancyYN == "True";
                this.rbOccupancyN.Checked = sOccupancyYN == "False";

                // Usage
                this.litUsage.Text = gfUsage.Value;
                string sUsageYN = GetValueOrDefault("Usage");
                this.rbUsageY.Checked = sUsageYN == "True";
                this.rbUsageN.Checked = sUsageYN == "False";

                // Dwelling type
                this.litDwellingType.Text = gfDwellingType.Value;
                string sDwellingType = GetValueOrDefault("DwellingTypeCorrect");
                this.rbDwellingTypeY.Checked = sDwellingType == "True";
                this.rbDwellingTypeN.Checked = sDwellingType == "False";

                // Year built
                this.litYearBuilt.Text = gfYearBuilt.Value;
                string sYearBuiltYN = GetValueOrDefault("YearBuiltYN");
                this.rbYearBuiltY.Checked = sYearBuiltYN == "True";
                this.rbYearBuiltN.Checked = sYearBuiltYN == "False";

                // # Stories
                this.litStories.Text = gfStories.Value;
                string sStories = GetValueOrDefault("NumberStoriesCorrect");
                this.rbStoriesY.Checked = sStories == "True";
                this.rbStoriesN.Checked = sStories == "False";

                // # ExtWallType
                this.litExtWallType.Text = gfExtWallType.Value;
                string sExtWallType = GetValueOrDefault("IsExteriorWallTypeCorrect");
                this.rbExtWallTypeY.Checked = sExtWallType == "True";
                this.rbExtWallTypeN.Checked = sExtWallType == "False";

                // # ExtCoating
                //this.litExtCoating.Text = gfExtExtCoating.Value;
                //string sExtCoating = GetValueOrDefault("IsExteriorCoatingCorrect", null, null, null, null);
                //this.rbExtCoatingY.Checked = sExtCoating == "True";
                //this.rbExtCoatingN.Checked = sExtCoating == "False";

                // # RoofGeo
                this.litRoofGeo.Text = gfRoofGeo.Value;
                string sRoofGeo = GetValueOrDefault("RoofGeometryCorrect");
                this.rbRoofGeoY.Checked = sRoofGeo == "True";
                this.rbRoofGeoN.Checked = sRoofGeo == "False";

                // # RoofType
                this.litRoofType.Text = gfRoofType.Value;
                string sRoofType = GetValueOrDefault("RoofTypeCorrect");
                this.rbRoofTypeY.Checked = sRoofType == "True";
                this.rbRoofTypeN.Checked = sRoofType == "False";

                // # RoofYear
                this.litRoofYear.Text = gfRoofYear.Value;
                string sRoofYear = GetValueOrDefault("RoofYearCorrect");
                this.rbRoofYearY.Checked = sRoofYear == "True";
                this.rbRoofYearN.Checked = sRoofYear == "False";

                // # RoofCond
                //this.litRoofCond.Text = gfRoofCond.Value;
                //string sRoofCond = GetValueOrDefault("RoofConditionCorrect");
                //this.rbRoofCondY.Checked = sRoofCond == "True";
                //this.rbRoofCondN.Checked = sRoofCond == "False";

                //// Burg Alarm
                //this.litBurgAlarm.Text = gfBurgAlarm.Value;
                //string sBurgAlarm = GetValueOrDefault("BurglarAlarmCentralCorrect");
                //this.rbBurgAlarmY.Checked = sBurgAlarm == "True";
                //this.rbBurgAlarmN.Checked = sBurgAlarm == "False";

                //// Fire alarm
                //this.litFireAlarm.Text = gfFireAlarm.Value;
                //string sFireAlarm = GetValueOrDefault("FireAlarmCentralCorrect");
                //this.rbFireAlarmY.Checked = sFireAlarm == "True";
                //this.rbFireAlarmN.Checked = sFireAlarm == "False";

                // Central alarm
                this.litCentralAlarm.Text = gfCentralAlarm.Value;
                string sCentralAlarm = GetValueOrDefault("CentralAlarmCorrectYN", "CentralAlarmCorrect", "CentralAlarmCorrect", "CentralAlarmCorrec", "CentralAlarmCorrect", "CentralAlarmCorrect");
                this.rbCentralAlarmY.Checked = sCentralAlarm == "True";
                this.rbCentralAlarmN.Checked = sCentralAlarm == "False";

                // # PoolPresent
                this.litPoolPresent.Text = gfPoolPresent.Value;
                string sPoolPresent = GetValueOrDefault("PoolCorrect");
                this.rbPoolPresentY.Checked = sPoolPresent == "True";
                this.rbPoolPresentN.Checked = sPoolPresent == "False";

                // # PoolAbove
                this.litPoolAbove.Text = gfPoolAboveGround.Value;
                string sPoolAbove = GetValueOrDefault("PoolAboveGrountCorrect");
                this.rbPoolAboveY.Checked = sPoolAbove == "True";
                this.rbPoolAboveN.Checked = sPoolAbove == "False";

                // # PoolEncl
                this.litPoolEncl.Text = gfPoolScreenEnclosurePresent.Value;
                string sPoolEncl = GetValueOrDefault("PoolScreenCorrect");
                this.rbPoolEnclY.Checked = sPoolEncl == "True";
                this.rbPoolEnclN.Checked = sPoolEncl == "False";

                // # PoolFence
                //this.litPoolFence.Text = gfPoolFencePresent.Value;
                //string sPoolFence = GetValueOrDefault("PoolFencePresentCorrect");
                //this.rbPoolFenceY.Checked = sPoolFence == "True";
                //this.rbPoolFenceN.Checked = sPoolFence == "False";

                // # PoolSlide
                this.litPoolSlide.Text = gfPoolSlidePresent.Value;
                string sPoolSlide = GetValueOrDefault("SlidePresentCorrect");
                this.rbPoolSlideY.Checked = sPoolSlide == "True";
                this.rbPoolSlideN.Checked = sPoolSlide == "False";

                // # DivingBoard
                //this.litDivingBoard.Text = gfDivingBoard.Value;
                //string sDivingBoard = GetValueOrDefault("DivingBoard");
                //this.rbDivingBoardY.Checked = sDivingBoard == "True";
                //this.rbDivingBoardN.Checked = sDivingBoard == "False";

                // # Windstorm
                this.litWindStorm.Text = gfWindStorm.Value;
                string sWindStorm = GetValueOrDefault("WindStormDevicesCorrect");
                this.rbWindStormY.Checked = sWindStorm == "True";
                this.rbWindStormN.Checked = sWindStorm == "False";

                // # ProtClass
                this.litProtClass.Text = gfProtectionClass.Value;
                string sProtClass = GetValueOrDefault("ProtectionClassCorrect");
                this.rbProtClassY.Checked = sProtClass == "True";
                this.rbProtClassN.Checked = sProtClass == "False";

                // # Pets
                this.litPets.Text = gfPets.Value;
                string sPets = GetValueOrDefault("DogsCorrect");
                this.rbPetsY.Checked = sPets == "True";
                this.rbPetsN.Checked = sPets == "False";

                // # Distance to coast
                if (gfDistanceCoast != null)
                {
                    this.litCoast.Text = gfDistanceCoast.Value;
                }
                if (gfDistanceCoast2 != null)
                {
                    this.litCoast.Text = gfDistanceCoast2.Value;
                }
                string sCoast = GetValueOrDefault("DistanceCoastCorrect");
                this.rbCoastY.Checked = sCoast == "True";
                this.rbCoastN.Checked = sCoast == "False";

                string sInsp = GetInspectorName();
                this.litInspector.Text = sInsp;

                if (bUpdate)
                {
                    // Plumbing Updates
                    this.litPlumbUpdt.Text = gfPlumbUpdates.Value;
                    string sPlumbUpdates = GetValueOrDefault("PlumbingUpdatesOrchidCorrect");
                    this.rbPlumbUpdtY.Checked = sPlumbUpdates == "True";
                    this.rbPlumbUpdtN.Checked = sPlumbUpdates == "False";

                    // Plumbing FP
                    this.litPlumbFP.Text = gfPlumbFP.Value;
                    string sPlumbFP = GetValueOrDefault("PlumbingFullPartial");
                    this.rbPlumbFPY.Checked = sPlumbFP == "True";
                    this.rbPlumbFPN.Checked = sPlumbFP == "False";

                    // Plumbing year
                    this.litPlumbYear.Text = gfPlumbYear.Value;
                    string sPlumbYear = GetValueOrDefault("PlumbingYearCorrect");
                    this.rbPlumbYearY.Checked = sPlumbYear == "True";
                    this.rbPlumbYearN.Checked = sPlumbYear == "False";

                    // HVAC Updates
                    this.litHVAC.Text = gfHVACUpdates.Value;
                    string sHVACUpdates = GetValueOrDefault("HvacUpdatesOrchidCorrect");
                    this.rbHVACY.Checked = sHVACUpdates == "True";
                    this.rbHVACN.Checked = sHVACUpdates == "False";

                    // HVAC FP
                    this.litHVACFP.Text = gfHVACFP.Value;
                    string sHVACFP = GetValueOrDefault("HvacCorrect");
                    this.rbHVACFPY.Checked = sHVACFP == "True";
                    this.rbHVACFPN.Checked = sHVACFP == "False";

                    // HVAC year
                    this.litHVACYear.Text = gfHVACYear.Value;
                    string sHVACYear = GetValueOrDefault("HvacYearUpdateCorrect");
                    this.rbHVACYearY.Checked = sHVACYear == "True";
                    this.rbHVACYearN.Checked = sHVACYear == "False";

                    // Wiring Updates
                    this.litWiring.Text = gfWiringUpdates.Value;
                    string sWiringUpdates = GetValueOrDefault("ElectricalUpdatesCorrectOrchid");
                    this.rbWiringY.Checked = sWiringUpdates == "True";
                    this.rbWiringN.Checked = sWiringUpdates == "False";

                    // Wiring FP
                    this.litWiringFP.Text = gfWiringFP.Value;
                    string sWiringFP = GetValueOrDefault(null, null, "ElectrialFullPartialCorrect", null, "ElectricalFullPartialCorrect", "ElectricalFullPartialCorrect");
                    this.rbWiringFPY.Checked = sWiringFP == "True";
                    this.rbWiringFPN.Checked = sWiringFP == "False";

                    // Wiring year
                    this.litWiringYear.Text = gfWiringYear.Value;
                    string sWiringYear = GetValueOrDefault("ElectricalYearUpdateCorrect");
                    this.rbWiringYearY.Checked = sWiringYear == "True";
                    this.rbWiringYearN.Checked = sWiringYear == "False";
                }


            }

            catch (Exception ex)
            {

                //this.litError.Text = ex.Message;
            }
        }

        private decimal? GetCoverageA()
        {
            var dt = SQL.ExecuteSql("SELECT CoverageAOut FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if(dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageAOut"] != null)
            {
                decimal covA;
                if(Decimal.TryParse(tr["CoverageAOut"].ToString(), out covA))
                    return covA;
            }

            return null;
        }

        private string GetITV(decimal? coverageAIn, decimal? coverageAOut)
        {
            var cAOut = coverageAOut ?? 0;
            var cAIn = coverageAIn ?? 0;
            if(cAOut != 0)
                return Math.Round((cAIn / cAOut) * 100, 0) + " %";
            return "N/A";
        }

        private string GetValueOrDefault(string ColName, bool liabilityHas=true)
        {
            if (liabilityHas)
                return GetValueOrDefault(ColName, ColName, ColName, ColName, ColName, ColName);
            else
                return GetValueOrDefault(ColName, ColName, ColName, null, ColName, ColName);
        }

        private List<Guid> NoFormDataFound = new List<Guid>();

        private DataRow GetFormData(Guid FormID)
        {
            if (NoFormDataFound.Contains(FormID)) //so we don't have to hit the db on every request if we don't find a form on the Case the first time just skip.
                return null;

            DataRow dr = null;
            var caseForm = Case.CaseForms.Where(cf => cf.FormID == FormID && cf.Deleted == false).FirstOrDefault();
            if (caseForm != null)
            {
                string table = caseForm.Form.formTableName;
                var sql = "SELECT * FROM Form." + table + " WHERE FormID = @formInstanceID";
                    //f0e982e2-8495-4ac7-8dfd-dd54f129c8d3

                var ds = SQL.ExecuteSql(sql, "@formInstanceID", caseForm.FormInstanceID);
                if (ds.Tables.Count == 0)
                {
                    NoFormDataFound.Add(FormID);
                    return null;
                }
                var t = ds.Tables[0];
                if (t.Rows.Count > 0)
                    dr = t.Rows[0];
                else
                    NoFormDataFound.Add(FormID);
            }
            else
            {
                NoFormDataFound.Add(FormID);
            }

            return dr;
        }

        private string commaDelimitIfList(string list)
        {
            if (list.IsNotNullOrEmpty() && list.Contains('|'))//probably a list
                return string.Join(", ", list.Split('|').ToArray()).Trim(',',' ');
            return list;
        }

        private string GetColumnValue(DataRow row, string column)
        {
            if (row != null
                && row[column] != null
                && row[column].ToString().IsNotNullOrEmpty())
                return commaDelimitIfList(row[column].ToString());
            return null;
        }


        //Residential Exterior Orchid Import                                11BCD0D5-258B-4537-91A0-16A0E658BFE9
        //Residential Exterior Orchid  w/e2value Import                     FC412AD7-0171-465E-81AD-ED0FA83D2F81
        //Residential Exterior Orchid w/update & e2value Import             15B999B3-98C2-4587-9824-6467BD7CA91D
        //Residential Interior & Exterior Orchid Import                     011A26BA-3FBF-439E-8308-85464366624A
        //Residential Interior & Exterior Orchid w/update Import            EFC58069-BC2E-4CB4-B109-B7810A7F57A9
        //Residential Interior & Exterior Orchid w/update w/No e2value Import 5DB95DCE-1EBA-4F32-A730-AC32EC035B07



        //                                      Form Table Name (Forms table)
        private string GetValueOrDefault(string ResExterior, string ResExteriorE2V, string ResExtE2VUpdt, string ResIntExt, string ResIntExtUpdt, string ResIntExtUpdtNoE2V)
        {
            // Residential Exterior Orchid Import
            if (ResExterior != null)
            {
                if (ResExteriorRow == null)
                    //                                                         Form ID (Forms table)
                    ResExteriorRow = GetFormData(new Guid("11BCD0D5-258B-4537-91A0-16A0E658BFE9"));

                var ResExteriorOrchidVal = GetColumnValue(ResExteriorRow, ResExterior);
                if (ResExteriorOrchidVal != null)
                    return ResExteriorOrchidVal;
            }

            //Residential Exterior Orchid  w/e2value Import   
            if (ResExteriorE2V != null)
            {
                if (ResExteriorE2VRow == null)
                    ResExteriorE2VRow = GetFormData(new Guid("FC412AD7-0171-465E-81AD-ED0FA83D2F81"));

                var ResExteriorE2VVal = GetColumnValue(ResExteriorE2VRow, ResExteriorE2V);

                if (ResExteriorE2VVal != null)
                    return ResExteriorE2VVal;
            }

            //Residential Exterior Orchid w/update & e2value Import
            if (ResExtE2VUpdt != null)
            {
                if (ResExteriorE2VUpdtRow == null)              
                    ResExteriorE2VUpdtRow = GetFormData(new Guid("15B999B3-98C2-4587-9824-6467BD7CA91D"));

                var ResExtE2VUpdtVal = GetColumnValue(ResExteriorE2VUpdtRow, ResExtE2VUpdt);

                if (ResExtE2VUpdtVal != null)
                    return ResExtE2VUpdtVal;
            }

            //Residential Interior & Exterior Orchid Import
            if (ResIntExt != null)
            {
                if (ResIntExtRow == null)
                    ResIntExtRow = GetFormData(new Guid("011A26BA-3FBF-439E-8308-85464366624A"));

                var ResIntExtVal = GetColumnValue(ResIntExtRow, ResIntExt);

                if (ResIntExtVal != null)
                    return ResIntExtVal;
            }

            //Residential Interior & Exterior Orchid w/update Import
            if (ResIntExtUpdt != null)
            {
                if (ResIntExtUpdtRow == null)
                    ResIntExtUpdtRow = GetFormData(new Guid("EFC58069-BC2E-4CB4-B109-B7810A7F57A9"));

                var ResIntExtUpdtVal = GetColumnValue(ResIntExtUpdtRow, ResIntExtUpdt);

                if (ResIntExtUpdtVal != null)
                    return ResIntExtUpdtVal;
            }

            //Residential Interior & Exterior Orchid w/update w/No e2value Import
            if (ResIntExtUpdtNoE2V != null)
            {
                if (ResIntExtUpdtNoE2VRow == null)
                    ResIntExtUpdtNoE2VRow = GetFormData(new Guid("5DB95DCE-1EBA-4F32-A730-AC32EC035B07"));

                var ResIntExtUpdtNoE2VVal = GetColumnValue(ResIntExtUpdtNoE2VRow, ResIntExtUpdtNoE2V);

                if (ResIntExtUpdtNoE2VVal != null)
                    return ResIntExtUpdtNoE2VVal;
            }




            return "N/A";
        }


        private void BindPhotos()
        {
            if (Case.Photos.Count == 0)
                this.image.ImageUrl = "~/images/custom/housenoimageavailable.jpg";
            else
            {
                var photoID = Case.Photos.OrderBy(p => p.DisplayOrder).Take(1).Select(p => p.PhotoID.ToString()).FirstOrDefault();
                this.image.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";
            }
        }

        public override void LoadData()
        {

        }

        public override void ClearFormData(FSMData.Form form)
        {

        }

        private object GetRecommendations(object ds)
        {
            if (ds is int)
            {
                try
                {

                    ds = SQL.GetInteger(@"Select  
												count(*) as TotalNumberOfRecs 
											from 
												caseformrecs cfr
												inner join CaseForms CF on CF.CaseFormID=cfr.CaseFormID and cfr.visible = 1
												inner join Cases C on C.CaseID=CF.CaseID
												left outer join RecommendationTypes RT on RT.RecommendationTypeID=cfr.RecommendationTypeID
											where 
												cf.deleted=0 and c.CaseID = @CaseId", "@CaseId", this.Case.CaseID);

                    return ds;
                }
                catch
                {
                    return 0;
                }
            }
            if (ds is List<Recommendations>)
            {
                try
                {
                    ds = SQL.ExecuteSql(@"Select 
												ISNULL(RT.Name,'Unknown Category') as Name, 
												count(*) as Count 
											from 
												caseformrecs cfr
												inner join CaseForms CF on CF.CaseFormID=cfr.CaseFormID and cfr.visible = 1
												inner join Cases C on C.CaseID=CF.CaseID
												left outer join RecommendationTypes RT on RT.RecommendationTypeID=cfr.RecommendationTypeID
											where 
												cf.deleted=0 and c.CaseID = @caseID
											Group by 
												ISNULL(RT.Name,'Unknown Category')", "@caseID", this.Case.CaseID).Tables[0].CastToList<Recommendations>();

                    if (ds == null)
                        return null;

                    return ds;
                }
                catch
                {
                    return null;
                }
            }

            return null;
        }

        private string GetInspectorName()
        {

            string sInspName = SQL.GetString(@"SELECT fr.FieldRepNumber + ' ' + ui.LastName 
                                                FROM Sutton.dbo.FieldReps fr inner join Sutton.dbo.UserInfos ui on ui.UserID = fr.LoginID inner join Sutton.dbo.Cases c on c.AssignedTo = fr.FieldRepID
                                                WHERE c.CaseID = @caseID", "@caseID", this.Case.CaseID);

            return sInspName;
        
        }
    }
}