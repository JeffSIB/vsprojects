﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using LC360Web;

namespace Utilant.Forms.Forms
{
    public partial class SuttonCoverSheetPrint : Utilant.Web.UI.WebControls.CaseForm
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindGeneralCaseInformation();
                BindFormData();
                BindPhotos();
            }
            catch
            {

            }
        }

        private void BindGeneralCaseInformation()
        {
            /* Company Header Information */
            this.litCompanyAddress.Text = Constants.CompanyAddress;
            this.litCompanyName.Text = Constants.CompanyName;
            this.litPhone.Text = Constants.CompanyPhone;
            this.litFax.Text = Constants.CompanyFax;

            /* Left-hand columns */
            this.litInsuredName.Text = Case.PolicyHolderName;
            this.litInsuredAddress.Text = Case.Address1.Line1;
            this.litCityStateZip.Text = Case.Address1.PostalCode.City + ", " + Case.Address1.PostalCode.StateName + ", " + Case.Address1.PostalCode.PostalCodeValue;
            this.litLocationAddress.Text = Case.Address.Line1;
            this.litLocationCityStateZip.Text = Case.Address.City + ", " + Case.Address.PostalCode.StateName + ", " + Case.Address.PostalCode.PostalCodeValue;

            /* Right-hand columns */
            this.litPolicyNumber.Text = Case.PolicyNumber;
            this.litInspectionDate.Text = Case.CaseDate.InspectionEnd.ToShortDateStringOrEmpty();
            this.litAgent.Text = Case.Producer.ContactName;
            this.litAgentPhone.Text = Case.Producer.ContactPhone;
            this.litUnderwriter.Text = Case.Underwriter.FullName;
        }

        private void BindFormData()
        {
            /* Occupancy / Construction */
            this.litOccupied.Text = GetValueOrDefault("Occupancy", "Occupancy", "Occupied");
            this.litConstruction.Text = GetValueOrDefault("ExtWallSupportsPercent", "ExtWallSupportsPercent", "ExtWallConstruction");
            this.litYearBuilt.Text = GetValueOrDefault("YearBuilt");

            /* Fire Protection / Breed of Dog */
            this.litProtectionClass.Text = GetValueOrDefault("Protection");
            this.litBreed.Text = GetValueOrDefault("DogBredd");
            
            /* Water Exposure */
            this.litGulf.Text = GetValueOrDefault("GulfMiles");
            this.litAtlOcean.Text = GetValueOrDefault("AtlanticMiles");

            /* Replacement Cost Analysis */
            this.litBuildingCoverage.Text = Case.PromotedField.CoverageAOut.HasValue ? Case.PromotedField.CoverageAOut.Value.ToString() : "N/A";
            this.litReplacmenetCost.Text = "GET REPLACEMENT COST";
            this.litITV.Text = "GET ITV %";// Coverage A In / Coverage A Out
        }

        private string GetValueOrDefault(string ColName)
        {
            return GetValueOrDefault(ColName, ColName, ColName);
        }

        private DataRow BriefExteriorWOE2VAL515Row = null;
        private DataRow BriefExteriorWOE2VAL562Row = null;
        private DataRow BriefExteriorWOE2VAL11117Row = null;

        private List<Guid> NoFormDataFound = new List<Guid>();

        private DataRow GetFormData(Guid FormID)
        {
            if (NoFormDataFound.Contains(FormID)) //so we don't have to hit the db on every request if we don't find a form on the Case the first time just skip.
                return null;

            DataRow dr = null;
            var caseForm = Case.CaseForms.Where(cf => cf.FormID == FormID && cf.Deleted == false).FirstOrDefault();
            if (caseForm != null)
            {
                string table = caseForm.Form.formTableName;
                var ds = SQL.ExecuteSql("SELECT * FROM " + table + " WHERE FormInstanceID = @formInstanceID", caseForm.FormInstanceID);
                var t = ds.Tables[table];
                if (t.Rows.Count > 0)
                    dr = t.Rows[0];
                else
                    NoFormDataFound.Add(FormID);
            }
            else
            {
                NoFormDataFound.Add(FormID);
            }

            return dr;
        }

        private string GetValueOrDefault(string BriefExteriorWOE2VAL515Col, string BriefExteriorWOE2VAL562Col, string BriefExteriorWOE2VAL11117Col)
        {

            if (BriefExteriorWOE2VAL515Row == null)
                BriefExteriorWOE2VAL515Row = GetFormData(new Guid("1445eb76-443b-4956-834e-7ee2f07cffe9"));//Residential Exterior Crump

            if(BriefExteriorWOE2VAL515Row != null
                && BriefExteriorWOE2VAL515Row[BriefExteriorWOE2VAL515Col] != null 
                && BriefExteriorWOE2VAL515Row[BriefExteriorWOE2VAL515Col].ToString().IsNotNullOrEmpty())
                return BriefExteriorWOE2VAL515Row[BriefExteriorWOE2VAL515Col].ToString();

            if (BriefExteriorWOE2VAL562Row == null)
                BriefExteriorWOE2VAL562Row = GetFormData(new Guid("51f4fec6-4ae8-4574-b91f-9bc7f4ce1000"));


            if (BriefExteriorWOE2VAL562Row != null
                && BriefExteriorWOE2VAL562Row[BriefExteriorWOE2VAL562Col] != null
                && BriefExteriorWOE2VAL562Row[BriefExteriorWOE2VAL562Col].ToString().IsNotNullOrEmpty())
                return BriefExteriorWOE2VAL562Row[BriefExteriorWOE2VAL562Col].ToString();

            if (BriefExteriorWOE2VAL11117Row == null)
                BriefExteriorWOE2VAL11117Row = GetFormData(new Guid("e9bf2db1-d805-4b43-a7c2-c73cda952455"));

            if (BriefExteriorWOE2VAL11117Row != null
                && BriefExteriorWOE2VAL11117Row[BriefExteriorWOE2VAL11117Col] != null
                && BriefExteriorWOE2VAL11117Row[BriefExteriorWOE2VAL11117Col].ToString().IsNotNullOrEmpty())
                return BriefExteriorWOE2VAL11117Row[BriefExteriorWOE2VAL11117Col].ToString();

            return "N/A";
        }


        private void BindPhotos()
        {
            if (Case.Photos.Count == 0)
                this.image.ImageUrl = "~/images/custom/housenoimageavailable.jpg";
            else
            {
                var photoID = Case.Photos.Take(1).Select(p => p.PhotoID.ToString()).FirstOrDefault();
                this.image.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";
            }
        }

        public override void LoadData()
        {

        }

        public override void ClearFormData(FSMData.Form form)
        {

        }
    }
}