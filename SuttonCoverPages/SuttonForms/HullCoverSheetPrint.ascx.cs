﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using LC360Web;
using System.Text.RegularExpressions;

namespace Utilant.Forms.Forms
{
    public partial class HullCoverSheetPrint : Utilant.Web.UI.WebControls.CaseForm
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindGeneralCaseInformation();
                BindPhotos();
                BindHazards();
                BindFormData();
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
            }
        }

        private void BindHazards()
        {
            var chs = Case.CaseHazards.Where(ch => ch.CaseForm.Deleted == false).OrderByDescending(ch => ch.HazardScore);
            foreach (var ch in chs)
            {
                this.litHazards.Text += ch.Description + "<br/>";
            }
        }

        private void BindGeneralCaseInformation()
        {
            /* Company Header Information */
            try
            {
                this.litCompanyAddress.Text = Constants.CompanyAddress;
                this.litCompanyCompletedForName.Text = Case.Customer.Name;
                this.litPhone.Text = Constants.CompanyPhone;
                this.litFax.Text = Constants.CompanyFax;
            }
            catch (Exception e)
            {
                this.litCompanyAddress.Text = "5656 Central Avenue<br />St. Petersburg, FL 33707";
                this.litCompanyCompletedForName.Text = "Hull & Co.-7180- Commercial - Tampa Bay";
                this.litPhone.Text = "727-384-5454";
                this.litFax.Text = "727-384-6565";
            }

            /* Left-hand columns */
            this.litInsuredName.Text = Case.PolicyHolderName;
            this.litInsuredAddress.Text = Case.Address1.Line1;
            this.litCityStateZip.Text = Case.Address1.City + ", " + Case.Address1.PostalCode.StateName + ", " + Case.Address1.PostalCode.PostalCodeValue;
            this.litLocationAddress.Text = Case.Address.Line1;
            this.litLocationCityStateZip.Text = Case.Address.City + ", " + Case.Address.PostalCode.StateName + ", " + Case.Address.PostalCode.PostalCodeValue;

            /* Right-hand columns */
            this.litPolicyNumber.Text = Case.PolicyNumber;

            Regex removeTime = new Regex(" 12:00:00 AM");
            this.litInspectionDate.Text = removeTime.Replace(GetValueOrDefault("DateInspected"), "");
            if (Case.Producer != null)
            {
                this.litAgent.Text = Case.Producer.Name;
                this.litAgentPhone.Text = Case.Producer.ContactPhone;
            }
            if (Case.Underwriter != null)
                this.litUnderwriter.Text = Case.Underwriter.FullName;
        }

        //TODO: Remove hacky regexs and add in some type safety maybe.
        private void BindFormData()
        {

            string sWork = "";

            /* Occupancy / Construction */
            this.litOccupied.Text = GetValueOrDefault("BusinessTypeOccupancy");
            this.litConstruction.Text = GetValueOrDefault("ConstructionType");
            this.litYearBuilt.Text = GetValueOrDefault("Year");
            this.litSqFt.Text = GetValueOrDefault("buildingAreaInt");

            sWork = GetValueOrDefault("ProtectionClass");
            if (sWork != "N/A")
            {
                this.litLblProtClass.Text = "Protection class:";
                this.litProtectionClass.Text = sWork;
            }

            sWork = GetValueOrDefault("ProtectionTen");
            this.litPC10.Text = (sWork == "N/A") ? "" : sWork;

            this.litSmokeDet.Text = GetValueOrDefault("SmokeDetectorsPresent");
            this.litBurgAlarm.Text = GetValueOrDefault("BurglarAlarm");

            sWork = GetValueOrDefault(null,null, null, null,null, null, "AutoExt", "AutoExt", "AutoExt");
            if (sWork != "N/A")
            {
                this.litLblAES.Text = "AES System";
                this.litAES.Text = sWork;
            }

            //sWork = GetValueOrDefault(null, null, null, null, null, "AutoExt");
            //if (sWork != "N/A")
            //{
            //    this.litLblAES.Text = "AES System";
            //    this.litAES.Text = sWork;
            //}


            // Updates
            this.litRoofUpdate.Text = GetValueOrDefault("RoofUpdateYear");

            // ** DEBUG **
            //string xxxxx = "";
        }

        private decimal? GetCoverageA()
        {
            var dt = SQL.ExecuteSql("SELECT CoverageAOut FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if(dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageAOut"] != null)
            {
                decimal covA;
                if(Decimal.TryParse(tr["CoverageAOut"].ToString(), out covA))
                    return covA;
            }

            return null;
        }

        private string GetITV(decimal? coverageAIn, decimal? coverageAOut)
        {
            var cAOut = coverageAOut ?? 0;
            var cAIn = coverageAIn ?? 0;
            if(cAOut != 0)
                return Math.Round((cAIn / cAOut) * 100, 0) + " %";
            return "N/A";
        }

        private string GetValueOrDefault(string ColName, bool liabilityHas=true)
        {
            if (liabilityHas)
                return GetValueOrDefault(ColName, ColName, ColName, ColName, ColName, ColName, null, null, null);
            else
                return GetValueOrDefault(ColName, ColName, ColName, ColName, ColName, ColName, null, null, null);
        }

        private List<Guid> NoFormDataFound = new List<Guid>();

        private DataRow GetFormData(Guid FormID)
        {
            if (NoFormDataFound.Contains(FormID)) //so we don't have to hit the db on every request if we don't find a form on the Case the first time just skip.
                return null;

            DataRow dr = null;
            var caseForm = Case.CaseForms.Where(cf => cf.FormID == FormID && cf.Deleted == false).FirstOrDefault();
            if (caseForm != null)
            {
                string table = caseForm.Form.formTableName;
                var sql = "SELECT * FROM Form." + table + " WHERE FormID = @formInstanceID";
                    //f0e982e2-8495-4ac7-8dfd-dd54f129c8d3

                var ds = SQL.ExecuteSql(sql, "@formInstanceID", caseForm.FormInstanceID);
                if (ds.Tables.Count == 0)
                {
                    NoFormDataFound.Add(FormID);
                    return null;
                }
                var t = ds.Tables[0];
                if (t.Rows.Count > 0)
                    dr = t.Rows[0];
                else
                    NoFormDataFound.Add(FormID);
            }
            else
            {
                NoFormDataFound.Add(FormID);
            }

            return dr;
        }

        private string commaDelimitIfList(string list)
        {
            if (list.IsNotNullOrEmpty() && list.Contains('|'))//probably a list
                return string.Join(", ", list.Split('|').ToArray()).Trim(',',' ');
            return list;
        }

        private string GetColumnValue(DataRow row, string column)
        {
            try
            {
                if (row != null
                    && row[column] != null
                    && row[column].ToString().IsNotNullOrEmpty())
                    return commaDelimitIfList(row[column].ToString());
                return null;
            }
            catch
            {
                return null;
            }
        }
        
        private DataRow PackageShortRow = null;
        private DataRow PackageLongRow = null;
        private DataRow LiabShortRow = null;
        private DataRow LiabLongRow = null;
        private DataRow PropShortRow = null;
        private DataRow PropLongRow = null;
        private DataRow CommCookSupRow = null;
        private DataRow RestLiabSupRow = null;

        private string GetValueOrDefault(string PackageShortCol, string LiabShortCol, string PropShortCol, string PackageLongCol, string PropLongCol, string LiabLongCol, string CommCookSup, string RestLiabSup, string RestAddOn)
        {
            // Pckage Short
            if (PackageShortCol != null)
            {
                if (PackageShortRow == null)
                    PackageShortRow = GetFormData(new Guid("8320df18-8949-4e35-87bd-3563c680ec7b")); // Hull - Tampa Bay Package Short v1.31.18

                var PackageShortVal = GetColumnValue(PackageShortRow, PackageShortCol);
                if (PackageShortVal != null)
                {
                    return PackageShortVal;
                }
                else
                {
                    PackageShortRow = GetFormData(new Guid("2eb2ca35-0b9b-4c07-8641-2949fba0813d")); // 7150 Hull - Package Short v9.7.18
                    PackageShortVal = GetColumnValue(PackageShortRow, PackageShortCol);
                    if (PackageShortVal != null)
                    {
                        return PackageShortVal;
                    }
                }
            }

            // Package Long
            if (PackageLongCol != null)
            {
                if (PackageLongRow == null)
                    PackageLongRow = GetFormData(new Guid("e012e773-1e8b-4071-bc7e-b87102255981")); // Hull - Tampa Bay Package Long v1.31.18
                
                var PackageLongVal = GetColumnValue(PackageLongRow, PackageLongCol);
                if (PackageLongVal != null)
                {
                    return PackageLongVal;
                }
                else
                {
                    PackageLongRow = GetFormData(new Guid("fdd2f60e-1e68-4386-9f0b-8c1276aecfba")); // Not Active-7150 Hull - Package Long v9.7.18

                    PackageLongVal = GetColumnValue(PackageLongRow, PackageLongCol);
                    if (PackageLongVal != null)
                    {
                        return PackageLongVal;
                    }
                }
            }

            // Liability Long
            if (LiabLongCol != null)
            {
                if (LiabLongRow == null)
                    LiabLongRow = GetFormData(new Guid("7738117e-1c6d-4ea0-97a7-cf1930b51eca")); // Hull - Tampa Bay Liability Long v1.31.18

                var LiabLongVal = GetColumnValue(LiabLongRow, LiabLongCol);

                if (LiabLongVal != null)
                {
                    return LiabLongVal;
                }
                else
                {
                    if (LiabLongRow == null)
                        LiabLongRow = GetFormData(new Guid("d31c0e69-ed26-4ec9-9c20-f02e44073ab7")); // 7150 Hull - Liability Long v9.7.18

                    LiabLongVal = GetColumnValue(LiabLongRow, LiabLongCol);

                    if (LiabLongVal != null)
                    {
                        return LiabLongVal;
                    }
                }
            }

            // Liability short
            if (LiabShortCol != null)
            {
                if (LiabShortRow == null)
                    LiabShortRow = GetFormData(new Guid("292c77a7-5d66-4a25-90a4-197cf04a8b3d")); //Hull - Tampa Bay Liability Short v1.31.18

                var LiabShortVal = GetColumnValue(LiabShortRow, LiabShortCol);

                if (LiabShortVal != null)
                {
                    return LiabShortVal;
                }
                else
                {
                    LiabShortRow = GetFormData(new Guid("ce7f64b2-d346-45a1-bee0-6a0adb970e40")); //7150 Hull - Liability Short v9.7.18
                    LiabShortVal = GetColumnValue(LiabShortRow, LiabShortCol);

                    if (LiabShortVal != null)
                    {
                        return LiabShortVal;
                    }
                }
            }

            // Property short
            if (PropShortCol != null)
            {
                if (PropShortRow == null)
                    PropShortRow = GetFormData(new Guid("714597b2-29e1-4046-a2c2-a22105d52ef7"));  //Hull - Tampa Bay Property Short v1.31.18 

                var PropShortVal = GetColumnValue(PropShortRow, PropShortCol);

                if (PropShortVal != null)
                {
                    return PropShortVal;
                }
                else
                {
                    PropShortRow = GetFormData(new Guid("b1eb78e7-7768-4447-ab0d-8fb2e458335b"));  //7150 Hull - Property Short v9.7.18

                    PropShortVal = GetColumnValue(PropShortRow, PropShortCol);

                    if (PropShortVal != null)
                    {
                        return PropShortVal;
                    }
                }
            }

            // Property long
            if (PropLongCol != null)
            {
                if (PropLongRow == null)
                    PropLongRow = GetFormData(new Guid("bd00fe74-6340-4808-9d1d-901fb3e2c38a"));  //Hull - Tampa Bay Property Long v1.31.18 

                var PropLongVal = GetColumnValue(PropLongRow, PropLongCol);

                if (PropLongVal != null)
                {
                    return PropLongVal;
                }
                else
                {
                    PropLongRow = GetFormData(new Guid("532ee0da-e40c-49d9-9f05-1f74a28de462"));  //7150 Hull - Property Long v9.7.18

                    PropLongVal = GetColumnValue(PropLongRow, PropLongCol);

                    if (PropLongVal != null)
                    {
                        return PropLongVal;
                    }
                }
            }

            // Hull Commercial Cooking v1.31.18
            if (CommCookSup != null)
            {
                if (CommCookSup == "ProtectionClass") return "N/A";
                if (CommCookSupRow == null)
                    CommCookSupRow = GetFormData(new Guid("c7b8aaa3-9f7a-4a1f-86b5-79e5a90af3ac"));

                var CommCookVal = GetColumnValue(CommCookSupRow, CommCookSup);

                if (CommCookVal != null)
                {
                    return CommCookVal;
                }
                else
                {
                    CommCookSupRow = GetFormData(new Guid("c7b8aaa3-9f7a-4a1f-86b5-79e5a90af3ac"));

                    CommCookVal = GetColumnValue(CommCookSupRow, CommCookSup);

                    if (CommCookVal != null)
                    {
                        return CommCookVal;
                    }
                }
            }

            // Hull Restaurant Liability Add On v3.2.18
            if (RestLiabSup != null)
            {
                if (RestLiabSup == "ProtectionClass") return "N/A";
                if (RestLiabSupRow == null)
                    RestLiabSupRow = GetFormData(new Guid("4c396ea1-b786-43d5-840a-7dcbdc990cb8")); 

                var RestLiabSupVal = GetColumnValue(RestLiabSupRow, RestLiabSup);

                if (RestLiabSupVal != null)
                {
                    return RestLiabSupVal;
                }
            }

            // Hull Restaurant Add On v3.2.18
            if (RestAddOn != null)
            {
                if (RestAddOn == "ProtectionClass") return "N/A";
                if (RestLiabSupRow == null)
                    RestLiabSupRow = GetFormData(new Guid("45444702-2ba3-4d66-b3c1-d66188b3fdef")); 

                var RestLiabSupVal = GetColumnValue(RestLiabSupRow, RestLiabSup);

                if (RestLiabSupVal != null)
                {
                    return RestLiabSupVal;
                }
            }

            return "N/A";
        }


        private void BindPhotos()
        {
            if (Case.Photos.Count == 0)
                this.image.ImageUrl = "~/images/custom/housenoimageavailable.jpg";
            else
            {
                var photoID = Case.Photos.OrderBy(p => p.DisplayOrder).Take(1).Select(p => p.PhotoID.ToString()).FirstOrDefault();
                this.image.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

                // ** DEBUG **
                //this.image.ImageUrl = "https://ecommerce3.sibfla.com/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

            }
        }

        public override void LoadData()
        {

        }

        public override void ClearFormData(FSMData.Form form)
        {

        }
    }
}