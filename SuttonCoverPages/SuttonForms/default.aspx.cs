﻿using System;
using System.Configuration;
using System.Linq;

namespace Utilant.Forms
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //this line will allow you to append ?pdf=true to the url in the browser and it will then render as a pdf document.
			    if (Query.ToBool("pdf", false))
				Response.BinaryWrite(PDFGenerator.PDF.FromUrl(Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "") + "/default.aspx"));

            GetCaseInformation();
        }

        private void GetCaseInformation()
        {
            //case guid, found in the query string section of a case when clicked on from the grid
            //e4b38203 - 0ad7 - 499b - b504 - 2417f14e90db

            //PackageV2 - 
            //var caseID = Query.ToGuid("caseID", new Guid("511362ed-85a8-46e2-b439-d566b5922bdc"));   //9976699 ;

            //PropertyV2 - 
            //var caseID = Query.ToGuid("caseID", new Guid("58553ed6-83e4-466c-84b5-51f3b7c2c7f6"));   //9988596  ;

            //Liability V2 - 
            //var caseID = Query.ToGuid("caseID", new Guid("fc8b9542-0330-452a-8f77-9460e8d3548e"));   //9988594  ;
            //var caseID = Query.ToGuid("caseID", new Guid("1e72c2ae-5a58-403f-aa31-fe120b65f384"));   //9991108  ;
            //var caseID = Query.ToGuid("caseID", new Guid("72a158c3-e3b2-4260-b76a-ba1004bec907"));   //9991116 ;

            //AMWins
            //var caseID = Query.ToGuid("caseID", new Guid("5aa224b7-bd23-4219-8a34-6924c65088c9"));   //9996428 -Liab;
            //var caseID = Query.ToGuid("caseID", new Guid("6d7f35c0-c449-46d2-a0fa-d74d60a9b831"));   //9996199 - Package;
            //var caseID = Query.ToGuid("caseID", new Guid("ad190c78-067c-4cb8-8f4f-bb573d185017"));   //9996423 - Prop;
            //var caseID = Query.ToGuid("caseID", new Guid("b65034f5-a24e-4d86-86c6-5c97a05856e7"));   //9995870 - Package;
            //var caseID = Query.ToGuid("caseID", new Guid("2b8f2d85-1164-4388-8ea9-e6fea815b9f3"));   //9997785 -Liab - LIVE;
            //var caseID = Query.ToGuid("caseID", new Guid("b5c79f55-3933-4f84-8308-0af28a45cc39"));    //9997206 -Liab - LIVE;
            //var caseID = Query.ToGuid("caseID", new Guid("645aec83-f30f-444f-aadd-5e08fbbdb28a"));    //9997214 -Prop - LIVE;
            //var caseID = Query.ToGuid("caseID", new Guid("3746b30a-3832-4f80-97da-2db115a553bf"));    //9997218 -Prop - LIVE;
            var caseID = Query.ToGuid("caseID", new Guid("3bdc80c0-2c6f-4246-868c-4a013811a23b"));    //9998424   -Prop - LIVE;



            //Orchid 
            //var caseID = Query.ToGuid("caseID", new Guid("5d280704-7116-4656-9c56-51b2f6ee1886"));   //9978305 ;

            //J&J
            //var caseID = Query.ToGuid("caseID", new Guid("a9ce7fa1-e943-4afa-a861-a74f9b6f1bbc"));   //9987411 ;


            //this will be the second guid in the query string when a form is being viewed for a particular case. 
            //this can be commented out when testing a cover page that is not directly tied to a form in the form setup
            //var caseFormID = Query.ToGuid("caseFormID", new Guid("e4abdbc3-ce04-49bd-82d3-d761b774b857")); // Residential Exterior Orchid Connect w/e2value v5.1.17
            //var caseFormID = Query.ToGuid("caseFormID", new Guid("8e46a35f-b59a-4362-8c18-b8501c18c84b")); 


            //this is the connection string used in the web config that should point to your database, so that you can pull the form information down
            var db = new FSMData.FSMDataContext(ConfigurationManager.ConnectionStrings["FSM_ConnectionString"].ConnectionString);

            //var caseForm = db.CaseForms.FirstOrDefault(c => c.CaseFormID == caseFormID);
            //Also comment this next line out if testing a cover page that is not tied to a form. 
            //uc.FormInstanceID = caseForm.FormInstanceID;

            //this can be assigned directly for a cover page. The guid would be found in Admin -> forms -> form manager . Click in the cover sheet in question. grab the form id from the query string in the browser. 
            //uc.FormID = db.Forms.FirstOrDefault(f => f.formID == caseForm.FormID).formID;
            uc.Case = db.Cases.FirstOrDefault(c => c.CaseID == caseID);
            uc.Case.DataContext = db;

        }

    }
}