﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Utilant.Forms._default" %>
<!-- This is where you will put in the form that you'd like to test , replacing YourformNamehere with the name of the form in question--> 
<%--<%@ Register Src="~/Forms/LiabV2CoverSheetPrint.ascx" TagName="userControl" TagPrefix="uc" %>--%>
<%--<%@ Register Src="~/Forms/PV2CoverSheetPrint.ascx" TagName="userControl" TagPrefix="uc" %>--%>
<%@ Register Src="~/Forms/AW2CoverSheetPrint.ascx" TagName="userControl" TagPrefix="uc" %>
<%--<%@ Register Src="~/Forms/JJCoverSheetPrint.ascx" TagName="userControl" TagPrefix="uc" %>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <base href="https://ecommerce.sibfla.com" />
    <link href="/App_Themes/Print/Custom.css" type="text/css"
        rel="stylesheet" />
    <link href="/App_Themes/Print/LineReport.css" type="text/css"
        rel="stylesheet" />
    <title></title>
    <link href="/App_Themes/Print/Custom.css" type="text/css"
        rel="stylesheet" />
    <link href="/App_Themes/Print/LineReport.css" type="text/css"
        rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    <uc:userControl ID="uc" runat="server" />
    </form>
</body>
</html>
