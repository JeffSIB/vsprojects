﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using LC360Web;
using System.Text.RegularExpressions;

namespace Utilant.Forms.Forms
{
    public partial class PersonalCoverSheetPrint : Utilant.Web.UI.WebControls.CaseForm
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindGeneralCaseInformation();
                BindPhotos();
                BindHazards();
                BindFormData();
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
            }
        }

        private void BindHazards()
        {
            var chs = Case.CaseHazards.Where(ch => ch.CaseForm.Deleted == false).OrderByDescending(ch => ch.HazardScore);
            foreach (var ch in chs)
            {
                this.litHazards.Text += ch.Description + "<br/>";
            }
        }

        private void BindGeneralCaseInformation()
        {
            /* Company Header Information */
            try
            {
                this.litCompanyAddress.Text = Constants.CompanyAddress;
                this.litCompanyCompletedForName.Text = Case.Customer.Name;
                this.litPhone.Text = Constants.CompanyPhone;
                this.litFax.Text = Constants.CompanyFax;
            }
            catch (Exception e)
            {
                this.litCompanyAddress.Text = "5656 Central Avenue<br />St. Petersburg, FL 33707";
                this.litCompanyCompletedForName.Text = "Hull & Co.-7064- Personal - FL";
                this.litPhone.Text = "727-384-5454";
                this.litFax.Text = "727-384-6565";
            }

            /* Left-hand columns */
            this.litInsuredName.Text = Case.PolicyHolderName;
            this.litInsuredAddress.Text = Case.Address1.Line1;
            this.litCityStateZip.Text = Case.Address1.PostalCode.City + ", " + Case.Address1.PostalCode.StateName + ", " + Case.Address1.PostalCode.PostalCodeValue;
            this.litLocationAddress.Text = Case.Address.Line1;
            this.litLocationCityStateZip.Text = Case.Address.City + ", " + Case.Address.PostalCode.StateName + ", " + Case.Address.PostalCode.PostalCodeValue;

            /* Right-hand columns */
            this.litPolicyNumber.Text = Case.PolicyNumber;

            Regex removeTime = new Regex(" 12:00:00 AM");
            this.litInspectionDate.Text = removeTime.Replace(GetValueOrDefault("DateInspected"), "");
            if (Case.Producer != null)
            {
                this.litAgent.Text = Case.Producer.Name;
                this.litAgentPhone.Text = Case.Producer.ContactPhone;
            }
            if (Case.Underwriter != null)
                this.litUnderwriter.Text = Case.Underwriter.FullName;
        }

        //TODO: Remove hacky regexs and add in some type safety maybe.
        private void BindFormData()
        {
            try
            {
                /* Occupancy / Construction */
                this.litOccupied.Text = GetValueOrDefault("Occupied", "Occupied", "Occupancy", null);
                this.litConstruction.Text = GetValueOrDefault("ExtWallConstruction", "ExtWallConstruction", "ExtWallSupportsPercent", null);
                this.litYearBuilt.Text = GetValueOrDefault("YearBuilt", "YearBuilt", "YearBuilt", null);
                litSqFt.Text = GetValueOrDefault("TotalLivingArea", "TotalLivingArea", "TotalLivingArea", null);

                //Stories
                string sStories = "";
                if (GetValueOrDefault("TypeOfDwelling").ToUpper().Contains("CONDO"))
                {
                    sStories = GetValueOrDefault("CondoStories", "CondoStories", "CondoStories", null);
                }
                else
                {
                    sStories = GetValueOrDefault("StoriesFromGrade", "StoriesFromGrade", "StoriesFromGrade", null);
                }
                if (sStories != "N/A")
                {
                    if (sStories.Contains("."))
                    {
                        decimal value;
                        if (Decimal.TryParse(sStories, out value))
                        {
                            sStories = String.Format("{0:0.0}", value);
                        }
                    }
                }
                litStories.Text = sStories;

                /* Fire Protection / Breed of Dog */
                this.litProtectionClass.Text = GetValueOrDefault("ProtectionClass", "ProtectionClass", "ProtectionClass", null);
                this.litBreed.Text = GetValueOrDefault("DogBredd", "DogBredd", "DogBredd", null);
                string sFireAlarm = GetValueOrDefault("FireAlarmSystem", "FireAlarm", "FireAlarm", null);
                if (sFireAlarm == "True" || sFireAlarm == "False")
                    litFireAlarm.Text = convertToYN(sFireAlarm);
                else
                    litFireAlarm.Text = sFireAlarm;

                this.litBurgAlarm.Text = GetValueOrDefault("BurglarAlarm", "BurglarAlarms", "BurglarAlarms", null);

                /* Water Exposure */
                Regex removeDecimal = new Regex("\\.0+$");
                this.litGulf.Text = removeDecimal.Replace(GetValueOrDefault("GulfMiles", "GulfMiles", "GulfMiles", null), " miles");
                this.litAtlOcean.Text = removeDecimal.Replace(GetValueOrDefault("AtlanticMiles", "AtlanticMiles", "AtlanticMiles", null), " miles");


                // Get CoverageA In/Out and variance
                try
                {
                    decimal? dCoverageAOut = null;

                    // Check for coverage A (E2V)
                    dCoverageAOut = GetCoverageAOut();

                    // if null check for MSB
                    if (dCoverageAOut == null)
                        dCoverageAOut = GetCoverageOutA();

                    // Coverage A In
                    if (Case.CaseField.CoverageAIn.HasValue)
                    {
                        // populate field on cover sheet
                        litBuildingCoverage.Text = Case.CaseField.CoverageAIn.Value.ToString("C");
                    }

                    if (Case.CaseField.CoverageAIn.HasValue && dCoverageAOut != null)
                    {
                        decimal dCoverageAOutVal = Convert.ToDecimal(dCoverageAOut.Value);

                        var coveragePercent = Math.Abs((((decimal)Case.CaseField.CoverageAIn.Value) / dCoverageAOutVal)).ToString("P");
                        litReplacmenetCost.Text = dCoverageAOutVal.ToString("C");
                        litITV.Text = coveragePercent;
                        //litCoverageA.Text = "<div class='coveragePercentage'>Coverage In: " + Case.CaseField.CoverageAIn.Value.ToString("C") + " Coverage Out: " + dCoverageAOutVal.ToString("C") + " Coverage is " + coveragePercent + " of final replacement cost</div>";
                    }
                }
                catch (Exception ex)
                {
                    //string stemperr = ex.Message;
                    litITV.Text = "*Error*";
                }


                ///* Replacement Cost Analysis */
                //decimal? coverageAOut = GetCoverageA();
                //this.litBuildingCoverage.Text = Case.CaseField.CoverageAIn.HasValue ? Math.Ceiling(Case.CaseField.CoverageAIn.Value).ToString("C0") : "N/A";
                //this.litReplacmenetCost.Text = coverageAOut.HasValue ? Math.Ceiling(coverageAOut.Value).ToString("C0") : "N/A";
                //this.litITV.Text = GetITV(Case.CaseField.CoverageAIn, coverageAOut);// Coverage A In / Coverage A Out

                // Updates
                litRoofAge.Text = GetValueOrDefault(null, null, null, "RoofAge");
                litElectUpd.Text = GetValueOrDefault(null, null, null, "DateWiringUpdate");
                litPlumbUpd.Text = GetValueOrDefault(null, null, null, "DatePlumbingUpdated");
                litHVACUpdt.Text = GetValueOrDefault(null, null, null, "DateHVACUpdated");
            }
            catch (Exception ex)
            {
                //string stemperr = ex.Message;
                litITV.Text = "*Error*";
            }

        }

        //private decimal? GetCoverageA()
        //{
        //    var dt = SQL.ExecuteSql("SELECT CoverageAOut FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
        //    if (dt.Tables.Count == 0)
        //        return null;
        //    if(dt.Tables[0].Rows.Count == 0)
        //        return null;
        //    var tr = dt.Tables[0].Rows[0];
        //    if (tr["CoverageAOut"] != null)
        //    {
        //        decimal covA;
        //        if(Decimal.TryParse(tr["CoverageAOut"].ToString(), out covA))
        //            return covA;
        //    }

        //    return null;
        //}

        private decimal? GetCoverageAOut()
        {
            // NOTE: CoverageAOut and CoverageOutA
            var dt = SQL.ExecuteSql("SELECT CoverageAOut FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if (dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageAOut"] != null)
            {
                string sTemp = RemoveChars(tr["CoverageAOut"].ToString());
                decimal covA;
                if (Decimal.TryParse(sTemp, out covA))
                    return covA;
            }

            return null;
        }

        private decimal? GetCoverageOutA()
        {
            // NOTE: CoverageAOut and CoverageOutA
            //CoverageOutA - MSB?
            var dt = SQL.ExecuteSql("SELECT CoverageOutA FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if (dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageOutA"] != null)
            {
                string sTemp = RemoveChars(tr["CoverageOutA"].ToString());
                decimal covA;
                if (Decimal.TryParse(sTemp, out covA))
                    return covA;
            }

            return null;
        }

        private static string RemoveChars(string sInput)
        {
            return sInput.Replace("$", "");
        }

        private string GetITV(decimal? coverageAIn, decimal? coverageAOut)
        {
            var cAOut = coverageAOut ?? 0;
            var cAIn = coverageAIn ?? 0;
            if (cAOut != 0)
                return Math.Round((cAIn / cAOut) * 100, 0) + " %";
            return "N/A";
        }

        private string GetValueOrDefault(string ColName)
        {
            return GetValueOrDefault(ColName, ColName, ColName, ColName);
        }

        private List<Guid> NoFormDataFound = new List<Guid>();

        private DataRow GetFormData(Guid FormID)
        {
            if (NoFormDataFound.Contains(FormID)) //so we don't have to hit the db on every request if we don't find a form on the Case the first time just skip.
                return null;

            DataRow dr = null;
            var caseForm = Case.CaseForms.Where(cf => cf.FormID == FormID && cf.Deleted == false).FirstOrDefault();
            if (caseForm != null)
            {
                string table = caseForm.Form.formTableName;
                var sql = "SELECT * FROM Form." + table + " WHERE FormID = @formInstanceID";
                //f0e982e2-8495-4ac7-8dfd-dd54f129c8d3

                var ds = SQL.ExecuteSql(sql, "@formInstanceID", caseForm.FormInstanceID);
                if (ds.Tables.Count == 0)
                {
                    NoFormDataFound.Add(FormID);
                    return null;
                }
                var t = ds.Tables[0];
                if (t.Rows.Count > 0)
                    dr = t.Rows[0];
                else
                    NoFormDataFound.Add(FormID);
            }
            else
            {
                NoFormDataFound.Add(FormID);
            }

            return dr;
        }

        private string commaDelimitIfList(string list)
        {
            if (list.IsNotNullOrEmpty() && list.Contains('|'))//probably a list
                return string.Join(", ", list.Split('|').ToArray()).Trim(',', ' ');
            return list;
        }

        private string GetColumnValue(DataRow row, string column)
        {
            try
            {
                if (row != null
                    && row[column] != null
                    && row[column].ToString().IsNotNullOrEmpty())
                    return commaDelimitIfList(row[column].ToString());
                return null;
            }
            catch
            {
                return null;
            }
        }


        private DataRow ResIntExtRow = null;
        private DataRow ResExtRow = null;
        private DataRow ResExtValRow = null;
        private DataRow UpdateRow = null;

        private string GetValueOrDefault(string ResIntExtCol, string ResExtCol, string ResExtValCol, string UpdateCol)
        {
            //Hull FL - Residential Interior & Exterior v7.12.17 "b126e638-571e-41d8-96e8-84a23130a3df"
            //Hull FL - Residential Interior & Exterior  v7.11.18  "38696c4e-faaf-47f0-ba26-8b360873fd4d"
            //Hull FL - Residential Interior & Exterior  v8.6.18  "41b4455c-2ba7-4cab-bdb1-0e454f6fd675"
            //Hull FL - Residential Interior & Exterior  v10.3.19   "6421e767-f200-4345-8158-ca692bc9c88d"

            if (ResIntExtCol != null)
            {
                if (ResIntExtRow == null)
                    ResIntExtRow = GetFormData(new Guid("6421e767-f200-4345-8158-ca692bc9c88d"));   //v10.3.19

                var ResIntExtVal = GetColumnValue(ResIntExtRow, ResIntExtCol);
                if (ResIntExtVal != null)
                {
                    return ResIntExtVal;
                }

                ResIntExtRow = GetFormData(new Guid("41b4455c-2ba7-4cab-bdb1-0e454f6fd675"));   //v8.6.18

                ResIntExtVal = GetColumnValue(ResIntExtRow, ResIntExtCol);
                if (ResIntExtVal != null)
                {
                    return ResIntExtVal;
                }

                ResIntExtRow = GetFormData(new Guid("b126e638-571e-41d8-96e8-84a23130a3df"));   //v7.12.17
                ResIntExtVal = GetColumnValue(ResIntExtRow, ResIntExtCol);
                if (ResIntExtVal != null)
                {
                    return ResIntExtVal;
                }

                ResIntExtRow = GetFormData(new Guid("38696c4e-faaf-47f0-ba26-8b360873fd4d"));   //v7.11.18
                ResIntExtVal = GetColumnValue(ResIntExtRow, ResIntExtCol);
                if (ResIntExtVal != null)
                {
                    return ResIntExtVal;
                }
            }
            //if (ResIntExtCol != null)
            //{
            //    if (ResIntExtRow == null)
            //        ResIntExtRow = GetFormData(new Guid("41b4455c-2ba7-4cab-bdb1-0e454f6fd675"));   //v8.6.18

            //    var ResIntExtVal = GetColumnValue(ResIntExtRow, ResIntExtCol);
            //    if (ResIntExtVal != null)
            //    {
            //        return ResIntExtVal;
            //    }
            //    else
            //    {
            //        ResIntExtRow = GetFormData(new Guid("b126e638-571e-41d8-96e8-84a23130a3df"));   //v7.12.17
            //        ResIntExtVal = GetColumnValue(ResIntExtRow, ResIntExtCol);
            //        if (ResIntExtVal != null)
            //        {
            //            return ResIntExtVal;
            //        }
            //        else
            //        {
            //            ResIntExtRow = GetFormData(new Guid("38696c4e-faaf-47f0-ba26-8b360873fd4d"));   //v7.11.18
            //            ResIntExtVal = GetColumnValue(ResIntExtRow, ResIntExtCol);
            //            if (ResIntExtVal != null)
            //            {
            //                return ResIntExtVal;
            //            }
            //        }
            //    }
            //}


            //Hull FL - Residential Exterior v7.11.18  "cfe8d638-ab4d-4aa2-90b4-ae1b96b67927"
            //Hull FL - Residential Exterior v7.12.17 "4b4495a5-cfd1-4226-bc60-195b63eb2730"
            //Hull FL - Residential Exterior v10.3.19   "42eb56b3-5152-4d8f-8b4e-3e6dcc1f099f"

            if (ResExtCol != null)
            {
                if (ResExtRow == null)
                    ResExtRow = GetFormData(new Guid("42eb56b3-5152-4d8f-8b4e-3e6dcc1f099f")); //v10.3.19

                var ResExtVal = GetColumnValue(ResExtRow, ResExtCol);
                if (ResExtVal != null)
                {
                    return ResExtVal;
                }

                ResExtRow = GetFormData(new Guid("cfe8d638-ab4d-4aa2-90b4-ae1b96b67927"));  //v7.11.18
                ResExtVal = GetColumnValue(ResExtRow, ResExtCol);
                if (ResExtVal != null)
                {
                    return ResExtVal;
                }

                ResExtRow = GetFormData(new Guid("4b4495a5-cfd1-4226-bc60-195b63eb2730"));  //v7.12.17
                ResExtVal = GetColumnValue(ResExtRow, ResExtCol);
                if (ResExtVal != null)
                {
                    return ResExtVal;
                }
            }

            //if (ResExtCol != null)
            //{
            //    if (ResExtRow == null)
            //        ResExtRow = GetFormData(new Guid("cfe8d638-ab4d-4aa2-90b4-ae1b96b67927"));  //v7.11.18

            //    var ResExtVal = GetColumnValue(ResExtRow, ResExtCol);
            //    if (ResExtVal != null)
            //    {
            //        return ResExtVal;
            //    }
            //    else
            //    {
            //        ResExtRow = GetFormData(new Guid("4b4495a5-cfd1-4226-bc60-195b63eb2730"));  //v7.12.17
            //        ResExtVal = GetColumnValue(ResExtRow, ResExtCol);
            //        if (ResExtVal != null)
            //        {
            //            return ResExtVal;
            //        }
            //    }
            //}

            //Hull FL - Residential Exterior w/valuation v7.11.18   "f1b3b04d-2a46-4a5f-ba72-e0b6c447de9c"
            //Hull FL - Residential Exterior w/valuation v7.12.17   "21035030-0de9-4673-abff-1b822881853e"
            //Hull FL - Residential Exterior w/valuation v8.6.18    "91be7377-a226-4a90-8068-6bb25144e387"
            //Hull FL - Residential Exterior w/valuation v10.3.19   "07aa8f30-4e4c-42ab-867e-8ae05ccd0b80" 

            if (ResExtValCol != null)
            {
                if (ResExtValRow == null)
                    ResExtValRow = GetFormData(new Guid("07aa8f30-4e4c-42ab-867e-8ae05ccd0b80"));   //v10.3.19

                var ResExtValVal = GetColumnValue(ResExtValRow, ResExtValCol);
                if (ResExtValVal != null)
                {
                    return ResExtValVal;
                }

                ResExtValRow = GetFormData(new Guid("f1b3b04d-2a46-4a5f-ba72-e0b6c447de9c"));   //v7.11.18
                ResExtValVal = GetColumnValue(ResExtValRow, ResExtValCol);
                if (ResExtValVal != null)
                {
                    return ResExtValVal;
                }

                ResExtValRow = GetFormData(new Guid("21035030-0de9-4673-abff-1b822881853e"));   //v7.12.17
                ResExtValVal = GetColumnValue(ResExtValRow, ResExtValCol);
                if (ResExtValVal != null)
                {
                    return ResExtValVal;
                }

                ResExtValRow = GetFormData(new Guid("91be7377-a226-4a90-8068-6bb25144e387"));   //v8.6.18
                ResExtValVal = GetColumnValue(ResExtValRow, ResExtValCol);
                if (ResExtValVal != null)
                {
                    return ResExtValVal;
                }
            }


            //if (ResExtValCol != null)
            //{
            //    if (ResExtValRow == null)
            //        ResExtValRow = GetFormData(new Guid("f1b3b04d-2a46-4a5f-ba72-e0b6c447de9c"));   //v7.11.18

            //    var ResExtValVal = GetColumnValue(ResExtValRow, ResExtValCol);
            //    if (ResExtValVal != null)
            //    {
            //        return ResExtValVal;
            //    }
            //    else
            //    {
            //        ResExtValRow = GetFormData(new Guid("21035030-0de9-4673-abff-1b822881853e"));   //v7.12.17

            //        ResExtValVal = GetColumnValue(ResExtValRow, ResExtValCol);
            //        if (ResExtValVal != null)
            //        {
            //            return ResExtValVal;
            //        }
            //        else
            //        {
            //            ResExtValRow = GetFormData(new Guid("91be7377-a226-4a90-8068-6bb25144e387"));   //v8.6.18

            //            ResExtValVal = GetColumnValue(ResExtValRow, ResExtValCol);
            //            if (ResExtValVal != null)
            //            {
            //                return ResExtValVal;
            //            }
            //        }
            //    }
            //}

            //Hull FL - Update v7.11.18 
            //Hull FL - Update v3.28.17
            if (UpdateCol != null)
            {
                if (UpdateRow == null)
                    UpdateRow = GetFormData(new Guid("e5e096d8-7137-4e4b-81db-de736f81e967"));  //v7.11.18

                var UpdateVal = GetColumnValue(UpdateRow, UpdateCol);
                if (UpdateVal != null)
                {
                    return UpdateVal;
                }
                else
                {
                    UpdateRow = GetFormData(new Guid("fe93d745-289b-4ba9-bd20-a11d1a8f48f5"));  //v3.28.17
                    UpdateVal = GetColumnValue(UpdateRow, UpdateCol);
                    if (UpdateVal != null)
                    {
                        return UpdateVal;
                    }
                }
            }

            return "N/A";
        }


        private void BindPhotos()
        {
            if (Case.Photos.Count == 0)
                this.image.ImageUrl = "~/images/custom/housenoimageavailable.jpg";
            else
            {
                var photoID = Case.Photos.OrderBy(p => p.DisplayOrder).Take(1).Select(p => p.PhotoID.ToString()).FirstOrDefault();
                this.image.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";
                // ** DEBUG **
                //this.image.ImageUrl = "https://ecommerce3.sibfla.com/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

            }
        }

        private string convertToYN(string sValue)
        {
            string sRetStr = " ";

            if (sValue == "N/A")
                return "N/A";

            if (sValue == "True")
                return "Yes";

            if (sValue == "False")
                return "No";

            return sRetStr;

        }

        public override void LoadData()
        {

        }

        public override void ClearFormData(FSMData.Form form)
        {

        }
    }
}