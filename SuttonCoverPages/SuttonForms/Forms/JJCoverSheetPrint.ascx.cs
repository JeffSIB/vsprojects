﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using LC360Web;
using System.Text.RegularExpressions;
using FSMData;


namespace Utilant.Forms.Forms
{
    public partial class JJCoverSheetPrint : Utilant.Web.UI.WebControls.CaseForm
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindGeneralCaseInformation();
                BindPhotos();
                BindHazards();
                BindFormData();
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
            }
        }

        private void BindHazards()
        {
            var chs = Case.CaseHazards.Where(ch => ch.CaseForm.Deleted == false).OrderByDescending(ch => ch.HazardScore);
            foreach (var ch in chs)
            {
                this.litHazards.Text += ch.Description + "<br/>";
            }
        }

        private void BindGeneralCaseInformation()
        {
            /* Company Header Information */
            try
            {
                this.litCompanyAddress.Text = Constants.CompanyAddress;
                this.litCompanyCompletedForName.Text = Case.Customer.Name;
                this.litPhone.Text = Constants.CompanyPhone;
                this.litFax.Text = Constants.CompanyFax;
            }
            catch (Exception ex)
            {
                this.litCompanyAddress.Text = "5656 Central Avenue<br />St. Petersburg, FL 33707";
                this.litCompanyCompletedForName.Text = "Johnson & Johnson-7307- Personal";
                this.litPhone.Text = "727-384-5454";
                this.litFax.Text = "727-384-6565";
            }

            /* Left-hand columns */
            this.litInsuredName.Text = Case.PolicyHolderName;
            litInsured2.Text = Case.PolicyHolderName;
            this.litInsuredAddress.Text = Case.Address1.Line1;
            this.litCityStateZip.Text = Case.Address1.City + ", " + Case.Address1.PostalCode.StateName + ", " + Case.Address1.PostalCode.PostalCodeValue;
            this.litLocationAddress.Text = Case.Address.Line1;
            this.litLocationCityStateZip.Text = Case.Address.City + ", " + Case.Address.PostalCode.StateName + ", " + Case.Address.PostalCode.PostalCodeValue;

            /* Right-hand columns */
            litPolicyNumber.Text = Case.PolicyNumber;
            litPolicyNum2.Text = Case.PolicyNumber;

            Regex removeTime = new Regex(" 12:00:00 AM");
            litInspectionDate.Text = removeTime.Replace(GetValueOrDefault("DateInspected"), "");
            litEffDate.Text = removeTime.Replace(Case.CaseDate.Effective.ToString(), "");

            if (Case.Producer != null)
            {
                this.litAgent.Text = Case.Producer.Name;
                this.litAgentPhone.Text = Case.Producer.ContactPhone;
            }
            if (Case.Underwriter != null)
            {
                this.litUnderwriter.Text = Case.Underwriter.FullName;
                litUW2.Text = Case.Underwriter.FullName;
            }

            // Consistency check values //
            // Occupancy

            try
            {

                // Get generic field values
                var genericFields = Case.CaseGenericFields.Select(gf => new { FieldName = gf.CaseTypeGenericField.Name, gf.Value }).ToList();
                //CaseGenericField gfZone = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "Zone");
                //CaseGenericField gfMilesFromCoast = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "MilesFromCoast");

                // Stories
                // Single Family - Number of stories from grade = StoriesFromGrade
                // Condo - Number of stories - CondoStories
                string sStories = GetValueOrDefault("StoriesFromGrade");
                if (sStories == "N/A")
                {
                    sStories = GetValueOrDefault("CondoStories");
                }
                if (sStories.Contains("."))
                {
                    sStories = Convert.ToDecimal(sStories).ToString("0.0");
                }


                // Liab hazards
                string sLiabHaz = "";
                if (GetValueOrDefault("BusinessOnPremises") == "True")
                    sLiabHaz = "Business on premises  ";

                if (GetValueOrDefault("Farming") == "True")
                    sLiabHaz += "Farming  ";

                if (GetValueOrDefault("WaterExposure") == "True")
                    sLiabHaz += "Water exposure  ";


                litInspType.Text = Case.CaseType.Name;


                litPerInterview.Text = convertToYN(GetValueOrDefault("Interviewed", "Interviewed", "Inerviewed", "Inerviewed", ""));
                litPerInterviewIs.Text = GetValueOrDefault("PersonInterviewedIs", "PersonInterviewedIs", "OmterviewedIs", "OmterviewedIs", "");

                litYearBlt.Text = GetValueOrDefault("YearBuilt");
                litOccupancy.Text = GetValueOrDefault("TypeOfDwelling");
                litConst.Text = GetValueOrDefault("ExtWallSupportsPercent", "ExtWallSupportsPercent", "ExtWallConstruction", "ExtWallConstruction", "");
                litSqFt.Text = GetValueOrDefault("TotalSqFt");
                litWallFin.Text = GetValueOrDefault("ExteriorWallFinish", "ExteriorWallFinish", "ExtWallFinish", "ExtWallFinish", "");
                litFoundation.Text = GetValueOrDefault("Foundation");
                litStories.Text = sStories;
                litRoofShape.Text = GetValueOrDefault("RoofType");
                litRoofMat.Text = GetValueOrDefault("RoofCovering");
                litProtClass.Text = GetValueOrDefault("ProtectionClass");
                litPropAccess.Text = GetValueOrDefault("AccessToRisk");
                litFireDept.Text = GetValueOrDefault("FireDeptType");
                litFireDistance.Text = GetValueOrDefault("DistanceFireDept");
                litWaterSource.Text = GetValueOrDefault("AdditionalWaterSources");
                litStormProt.Text = GetValueOrDefault("StormShutterType", "StormShutterType", "HurricaneShutters", "HurricaneShutters", "");
                litHurrStrap.Text = GetValueOrDefault("RoofAttachments");
                litGated.Text = convertToYN(GetValueOrDefault("GatedCommunity"));
                litFireAlarm.Text = GetValueOrDefault("Firealarmsystems", "Firealarmsystems", "Firealarmsystems", "Firealarmsystems","");
                litBurgAlarm.Text = convertToYN(GetValueOrDefault("BurglarAlam", "BurglarAlam", "BurglarAlam", "BurglarAlam", ""));
                //litPool.Text = GetValueOrDefault("TypeOfPoolHotTub", "TypeOfPoolHotTub", "PoolHotTubType", "TypeOfPoolHotTub","");
                //litDiving.Text = convertToYN(GetValueOrDefault("DivingBoard"));
                //litSlide.Text = convertToYN(GetValueOrDefault("Slide"));
                //litPoolFence.Text = GetValueOrDefault("PoolProtection");
                litTramp.Text = GetValueOrDefault("TrampolinSafety");
                litDogs.Text = GetValueOrDefault("Dogs");
                litDogBreed.Text = GetValueOrDefault("DogBredd");
                litLiabHaz.Text = sLiabHaz;

                litRoofYear.Text = GetValueOrDefault("RoofYear");
                litPlumbYear.Text = GetValueOrDefault("YearPlumbing");
                litElectYear.Text = GetValueOrDefault("YearElectrical");
                litHVACYear.Text = GetValueOrDefault("YearHVAC");

                // Get CoverageA In/Out and variance
                try
                {
                    decimal? dCoverageAOut = null;

                    // Check for coverage A (E2V)
                    dCoverageAOut = GetCoverageAOut();

                    // if null check for MSB
                    if (dCoverageAOut == null)
                        dCoverageAOut = GetCoverageOutA();

                    // Coverage A In
                    if (Case.CaseField.CoverageAIn.HasValue)
                    {
                        // populate field on cover sheet
                        litCovAIn.Text = Case.CaseField.CoverageAIn.Value.ToString("C");
                    }

                    if (Case.CaseField.CoverageAIn.HasValue && dCoverageAOut != null)
                    {
                        decimal dCoverageAOutVal = Convert.ToDecimal(dCoverageAOut.Value);

                        var coveragePercent = Math.Abs((((decimal)Case.CaseField.CoverageAIn.Value) / dCoverageAOutVal)).ToString("P");
                        litCovAOut.Text = dCoverageAOutVal.ToString("C");
                        litCovAVar.Text = coveragePercent;
                        //litCoverageA.Text = "<div class='coveragePercentage'>Coverage In: " + Case.CaseField.CoverageAIn.Value.ToString("C") + " Coverage Out: " + dCoverageAOutVal.ToString("C") + " Coverage is " + coveragePercent + " of final replacement cost</div>";
                    }
                }
                catch {
                    litCovAVar.Text = "*Error*";
                }





                // Pool related
                // Handle possible show hide pool questions

                string sPoolHT = GetValueOrDefault("PoolHotTub", "PoolHotTub","PoolHotTub", "PoolHotTub","");
                if (sPoolHT == "True")
                {
                    string sPoolType = GetValueOrDefault("TypeOfPoolHotTub", "TypeOfPoolHotTub", "PoolHotTubType", "PoolHotTubType", "");
                    litPool.Text = sPoolType;

                    // Pool only
                    if (sPoolType == "Pool")
                    {
                        litDiving.Text = convertToYN(GetValueOrDefault("DivingBoard"));
                        litSlide.Text = convertToYN(GetValueOrDefault("Slide"));
                        litPoolFence.Text = convertToYN(GetValueOrDefault("IsPoolProtected", "PoolProtection", "PoolProtected", "PoolProtection", ""));
                    }
                    else if (sPoolType.Contains("built"))
                    {
                        litDiving.Text = convertToYN(GetValueOrDefault("DivingBoardPH"));
                        litSlide.Text = convertToYN(GetValueOrDefault("SlidePH"));
                        litPoolFence.Text = convertToYN(GetValueOrDefault("PoolWithHotTubProtected", "PoolWithHotTubProtected", "PoolWithHotTubProtected", "PoolWithHotTubProtected", ""));
                    }

                    else if (sPoolType.Contains("tub only"))
                    {
                        litDiving.Text = "N/A";
                        litSlide.Text = "N/A";
                        litPoolFence.Text = convertToYN(GetValueOrDefault("HotTubProtected", "HotTubProtected", "HotTubProtected", "HotTubProtected", ""));
                    }
                }
                else
                {
                    litPool.Text = "No";
                    litDiving.Text = "N/A";
                    litSlide.Text = "N/A";
                    litPoolFence.Text = "N/A";
                }

            }
            catch (Exception ex)
            {

                //this.litError.Text = ex.Message;
            }


        }

        private string convertToYN(string sValue)
        {
            string sRetStr = " ";

            if (sValue == "N/A")
                return "N/A";

            if (sValue == "True")
                return "Yes";

            if (sValue == "False")
                return "No";

            return sRetStr;

        }

        private void BindFormData()
        {

            //string sWork = "";

            /* Occupancy / Construction */
            //this.litOccupied.Text = GetValueOrDefault("BusinessTypeOccupancy");
            //this.litConstruction.Text = GetValueOrDefault("ConstructionType", null, "ConstructionType", null, null, null);
            //this.litYearBuilt.Text = GetValueOrDefault("Year", null, "Year", null, null, null);
            //this.litSqFt.Text = GetValueOrDefault("buildingAreaInt");

            /* Fire Protection / Security */

            //sWork = GetValueOrDefault("ProtectionClass");
            //this.litProtectionClass.Text = (sWork == "N/A") ? "" : sWork;
            //sWork = GetValueOrDefault("ProtectionTen");
            //this.litPC10.Text = (sWork == "N/A") ? "" : sWork;
            //this.litSmokeDet.Text = GetValueOrDefault("SmokeDetectorsPresent");
            //this.litBurgAlarm.Text = GetValueOrDefault("BurglarAlarm");

            //sWork = GetValueOrDefault(null,null, null, null,"AutoExt", null);
            //if (sWork != "N/A")
            //{
            //    this.litLblAES.Text = "AES System";
            //    this.litAES.Text = sWork;
            //}

            //sWork = GetValueOrDefault(null, null, null, null, null, "AutoExt");
            //if (sWork != "N/A")
            //{
            //    this.litLblAES.Text = "AES System";
            //    this.litAES.Text = sWork;
            //}


            //// Updates
            //this.litRoofUpdate.Text = GetValueOrDefault("RoofUpdateYear");

            // ** DEBUG **
            //string xxxxx = "";
        }

        private decimal? GetCoverageAOut() 
        {
            // NOTE: CoverageAOut and CoverageOutA
            //CoverageOutA - E2V?
            var dt = SQL.ExecuteSql("SELECT CoverageAOut FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if(dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageAOut"] != null)
            {
                string sTemp = RemoveChars(tr["CoverageAOut"].ToString());
                decimal covA;
                if (Decimal.TryParse(sTemp, out covA))
                    return covA;
            }

            return null;
        }

        private decimal? GetCoverageOutA()
        {
            // NOTE: CoverageAOut and CoverageOutA
            //CoverageOutA - MSB?
            var dt = SQL.ExecuteSql("SELECT CoverageOutA FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if (dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageOutA"] != null)
            {
                string sTemp = RemoveChars(tr["CoverageOutA"].ToString());
                decimal covA;
                if (Decimal.TryParse(sTemp, out covA))
                    return covA;
            }

            return null;
        }

        private static string RemoveChars(string sInput)
        {
            return sInput.Replace("$", "");
        }

        private string GetITV(decimal? coverageAIn, decimal? coverageAOut)
        {
            var cAOut = coverageAOut ?? 0;
            var cAIn = coverageAIn ?? 0;
            if(cAOut != 0)
                return Math.Round((cAIn / cAOut) * 100, 0) + " %";
            return "N/A";
        }

        private string GetValueOrDefault(string ColName, bool liabilityHas=true)
        {
            if (liabilityHas)
                return GetValueOrDefault(ColName, ColName, ColName, ColName, ColName);
            else
                return GetValueOrDefault(ColName, ColName, ColName, ColName, ColName);
        }

        private List<Guid> NoFormDataFound = new List<Guid>();

        private DataRow GetFormData(Guid FormID)
        {
            if (NoFormDataFound.Contains(FormID)) //so we don't have to hit the db on every request if we don't find a form on the Case the first time just skip.
                return null;

            DataRow dr = null;
            var caseForm = Case.CaseForms.Where(cf => cf.FormID == FormID && cf.Deleted == false).FirstOrDefault();
            if (caseForm != null)
            {
                string table = caseForm.Form.formTableName;
                var sql = "SELECT * FROM [Form].[" + table + "] WHERE FormID = @formInstanceID";
                    //f0e982e2-8495-4ac7-8dfd-dd54f129c8d3

                var ds = SQL.ExecuteSql(sql, "@formInstanceID", caseForm.FormInstanceID);
                if (ds.Tables.Count == 0)
                {
                    NoFormDataFound.Add(FormID);
                    return null;
                }
                var t = ds.Tables[0];
                if (t.Rows.Count > 0)
                    dr = t.Rows[0];
                else
                    NoFormDataFound.Add(FormID);
            }
            else
            {
                NoFormDataFound.Add(FormID);
            }

            return dr;
        }

        private string commaDelimitIfList(string list)
        {
            if (list.IsNotNullOrEmpty() && list.Contains('|'))//probably a list
                return string.Join(", ", list.Split('|').ToArray()).Trim(',',' ');
            return list;
        }

        private string GetColumnValue(DataRow row, string column)
        {
            try
            {
                if (row != null
                    && row[column] != null
                    && row[column].ToString().IsNotNullOrEmpty())
                    return commaDelimitIfList(row[column].ToString());
                return null;
            }
            catch (Exception ex)
            {
                string sEx = ex.Message;
                return null;
            }
        }


        private DataRow ResExtRow = null;
        private DataRow ResExtwValRow = null;
        private DataRow ResIntExtRow = null;
        private DataRow ResIntExtwRoofRow = null;
        private DataRow ResUpdtRow = null;

        //J & J Residential Exterior v.4.1.18                   773774c9-0771-4fc4-959c-9757b2b0d19c
        //J & J Residential Exterior w/valuation v.4.1.18       f9fbd198-9169-410d-b6bc-7af7e10312c9
        //J & J Residential Exterior w/valuation v3.19.20       6f2c981c-e34c-4b3f-8de6-de9de56bb1d4
        //J & J Residential Interior & Exterior v4.1.18         c1fe09e8-aeef-4ff6-a6eb-4637e44bac2f
        //J & J Residential Interior & Exterior & Roof v4.1.18  aa65dd07-c341-41e8-9b5c-916ea872a21d
        //J & J Residential Update v5.25.18                     612167fe-4394-4ace-9c50-99fdd1fe3073

        private string GetValueOrDefault(string ResExtCol, string ResExtwValCol, string ResIntExtCol, string ResIntExtwRoofCol, string ResUpdtCol)
        {
            //J & J Residential Exterior v.4.1.18
            if (ResExtCol != null)
            {
                if (ResExtRow == null)
                    ResExtRow = GetFormData(new Guid("773774c9-0771-4fc4-959c-9757b2b0d19c")); // Form GUID

                var ResExtVal = GetColumnValue(ResExtRow, ResExtCol);
                if (ResExtVal != null)
                    return ResExtVal;
            }

            //J & J Residential Exterior w/valuation v.4.1.18
            if (ResExtwValCol != null)
            {
                if (this.ResExtwValRow == null)
                    ResExtwValRow = GetFormData(new Guid("f9fbd198-9169-410d-b6bc-7af7e10312c9"));

                var ResExtwValVal = GetColumnValue(this.ResExtwValRow, ResExtwValCol);

                if (ResExtwValVal != null)
                {
                    return ResExtwValVal;
                }
                else
                {
                    //J & J Residential Exterior w/valuation v3.19.20
                    ResExtwValRow = GetFormData(new Guid("6f2c981c-e34c-4b3f-8de6-de9de56bb1d4"));

                    ResExtwValVal = GetColumnValue(this.ResExtwValRow, ResExtwValCol);

                    if (ResExtwValVal != null)
                    {
                        return ResExtwValVal;
                    }

                }

            }

            //J & J Residential Interior & Exterior v4.1.18 
            if (ResIntExtCol != null)
            {
                if (this.ResIntExtRow == null)
                    this.ResIntExtRow = GetFormData(new Guid("c1fe09e8-aeef-4ff6-a6eb-4637e44bac2f"));

                var ResIntExt = GetColumnValue(this.ResIntExtRow, ResIntExtCol);

                if (ResIntExt != null)
                    return ResIntExt;
            }

            //J & J Residential Interior & Exterior & Roof v4.1.18
            if (ResIntExtwRoofCol != null)
            {
                if (this.ResIntExtwRoofRow == null)
                    this.ResIntExtwRoofRow = GetFormData(new Guid("aa65dd07-c341-41e8-9b5c-916ea872a21d"));

                var ResIntExtwRoofVal = GetColumnValue(this.ResIntExtwRoofRow, ResIntExtwRoofCol);

                if (ResIntExtwRoofVal != null)
                    return ResIntExtwRoofVal;
            }


            //J & J Residential Update v5.25.18 
            if (ResUpdtCol != null)
            {
                //if (ResUpdt == "ProtectionClass") return "N/A";
                if (ResUpdtRow == null)
                    ResUpdtRow = GetFormData(new Guid("612167fe-4394-4ace-9c50-99fdd1fe3073"));

                var ResUpdtVal = GetColumnValue(ResUpdtRow, ResUpdtCol);

                if (ResUpdtVal != null)
                    return ResUpdtVal;
            }

            return "N/A";
        }


        private void BindPhotos()
        {
            if (Case.Photos.Count == 0)
                this.image.ImageUrl = "~/images/custom/housenoimageavailable.jpg";
            else
            {
                var photoID = Case.Photos.OrderBy(p => p.DisplayOrder).Take(1).Select(p => p.PhotoID.ToString()).FirstOrDefault();
                this.image.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

                // ** DEBUG **
                //this.image.ImageUrl = "https://ecommerce3.sibfla.com/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

            }
        }

        public override void LoadData()
        {

        }

        public override void ClearFormData(FSMData.Form form)
        {

        }

        private string stripDecimal(string sInput)
        {
            try
            {
                int iPos = sInput.IndexOf(".");

                if (iPos == 0)
                {
                    return sInput;
                }
                else
                {
                    return sInput.Left(iPos);
                }
            }

            catch
            {
                return sInput;
            }
        }

        private string formatCurrency(string sInput)
        {
            try
            {
                int iVal = 0;
                if (int.TryParse(sInput, out iVal))
                {

                    return iVal.ToString("C0");
                }
                else
                {
                    return sInput;
                }

            }

            catch
            {
                return sInput;
            }
        }
    }
}