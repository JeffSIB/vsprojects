﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AMWinsCoverSheetPrint.ascx.cs"
    Inherits="Utilant.Forms.Forms.AMWinsCoverSheetPrint" %>


<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }

    .main-page {
        width: 7.5in;
        min-height: 10.3in;
        margin: auto;
        padding: 10px 10px 10px 10px;
        font-family: Arial;
        font-size: 14px;
        padding-left: 60px;
        padding-right: 60px;
    }

    .headerRow {
        background-color: #81bd01;
        color: White;
        font-weight: bold;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }

    .tableHeaderRow {
        background-color: #888888;
        color: White;
        font-weight: bold;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }

    .ccTable {
        border: 2px solid #808080;
        padding-top: 15px;
    }

    .ccTitle {
        border-bottom: 3px solid #000000;
        font-family: Calibri;
        color: black;
        font-weight: normal;
        font-size: 28pt;
    }

    .ccTableHeaderRow {
        background-color: #81bd01;
        border: 2px solid #808080;
        color: black;
        font-weight: bold;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
        text-align: center;
    }

    .ccTableDataRow {
        background-color: white;
        border: 2px solid #808080;
        width: 90px;
        text-align: center;
        color: black;
        font-weight: normal;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }

    .ccTableDataRowRight {
        background-color: white;
        border: 2px solid #808080;
        width: 90px;
        text-align: right;
        color: black;
        font-weight: normal;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }

    .ccTableDataRowLeft {
        background-color: white;
        border: 2px solid #808080;
        width: 90px;
        text-align: left;
        color: black;
        font-weight: normal;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }


    .logo {
        float: left;
    }

    .clear {
        clear: both;
    }

    .companyInfo {
        float: right;
        text-align: right;
        width: 30%;
        color: #006cc5;
        font-weight: bold;
    }

    .label {
        font-weight: bold;
    }

    .lastTableRow td {
        padding-bottom: 15px;
    }
</style>

<%--START OF PAGE--%>
<div class="main-page">
    <%--PAGE HEADER AND CASE INFORMATION--%>
    <div class="page-header">
        <div>
            <img id="imgHeader" class="logo" alt="[Company Logo]" src='<%= Page.ResolveClientUrl("~/images/Custom/mainlogo.png") %>' />

            <%--            // ** DEBUG **--%>
<%--                        <img id="imgHeader" class="logo" alt="[Company Logo]" src='https://ecommerce3.sibfla.com/App_Themes/images/mainLogoSmall.png' />--%>

            <div class="companyInfo">
                <table>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyAddress" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyCityStateZip" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Phone:
                            <asp:Literal ID="litPhone" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Fax:
                            <asp:Literal ID="litFax" runat="server" /></td>
                    </tr>
                </table>

            </div>
            <div class="clear" />
        </div>
        <div>
            <table style="border-spacing: 0;">
                <tr class="lastTableRow">
                    <td class="label" colspan="2">Completed Inspection Report For:</td>
                    <td colspan="2">
                        <asp:Literal ID="litCompanyCompletedForName" runat="server" /></td>
                </tr>
                <tr>
                    <td class="label">Insured Name:
                    </td>
                    <td>
                        <asp:Literal ID="litInsuredName" runat="server" />
                    </td>
                    <td class="label">Policy Number:
                    </td>
                    <td>
                        <asp:Literal ID="litPolicyNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Insured Address:
                    </td>
                    <td>
                        <asp:Literal ID="litInsuredAddress" runat="server" />
                    </td>
                    <td class="label">Inspection Date:
                    </td>
                    <td>
                        <asp:Literal ID="litInspectionDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">City,State,Zip:
                    </td>
                    <td>
                        <asp:Literal ID="litCityStateZip" runat="server" />
                    </td>
                    <td class="label">Agent:
                    </td>
                    <td>
                        <asp:Literal ID="litAgent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Location Address:
                    </td>
                    <td>
                        <asp:Literal ID="litLocationAddress" runat="server" />
                    </td>
                    <td class="label">Agent Phone:
                    </td>
                    <td>
                        <asp:Literal ID="litAgentPhone" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">City, State, Zip
                    </td>
                    <td>
                        <asp:Literal ID="litLocationCityStateZip" runat="server" />
                    </td>
                    <td class="label">Underwriter:
                    </td>
                    <td>
                        <asp:Literal ID="litUnderwriter" runat="server" />
                    </td>
                </tr>
            </table>

            <%--              <tr class="headerRow">
                    <td colspan="2" class="label">Occupancy
                    </td>
                    <td colspan="2" class="label">Construction & Year Built
                    </td>
                </tr>
                <tr>
                    <td class="label">Insured Property is:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litOccupied" runat="server" />
                    </td>
                    <td class="label">Construction:
                    </td>
                    <td>
                        <asp:Literal ID="litConstruction" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">Square footage:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litSqFt" runat="server" />
                    </td>
                    <td class="label">Year Built:
                    </td>
                    <td>
                        <asp:Literal ID="litYearBuilt" runat="server" />
                    </td>

                </tr>
                <tr class="headerRow">
                    <td colspan="2" class="label">Fire Protection / Security
                    </td>
                    <td colspan="2" class="label">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="label">Protection Class:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litProtectionClass" runat="server" />&nbsp;&nbsp;<asp:Literal ID="litPC10" runat="server" />

                    </td>
                    <td class="label">Smoke Detectors:
                    </td>
                    <td>
                        <asp:Literal ID="litSmokeDet" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">Burglar alarm:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litBurgAlarm" runat="server" />
                    </td>
                    <td class="label">
                        <asp:Literal ID="litLblAES" runat="server" />

                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litAES" runat="server" />
                    </td>
                </tr>
                <tr class="headerRow">
                    <td colspan="2" class="label">Updates
                    </td>
                    <td colspan="2" class="label">&nbsp;
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">Year roof was updated:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litRoofUpdate" runat="server" />
                    </td>
                    <td class="label">&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>--%>
            <div>
                <div style="width: 45%; float: left">
                    <table>
                        <tr class="headerRow">
                            <td class="label">Hazards / Exposures Noted
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Literal ID="litHazards" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="float: left; width: 45%; margin-left: 15px;">
                    <asp:Image ID="image" Width="380px" runat="server" />
                </div>
                <div class="clear" />
            </div>
        </div>
        <p style="page-break-after: always;">&nbsp;</p>
        <p>
            <asp:Literal ID="litError" runat="server" />
        </p>
        <p>
            <table style="width: 100%">
                <tr>
                    <td class="ccTitle">Consistency Check</td>
                </tr>
            </table>
        </p>
        <table style="width: 100%" class="ccTable">

            <tr>

                <td class="ccTableHeaderRow" style="width: 28%;">Field for Comparison
                </td>
                <td class="ccTableHeaderRow" style="width: 25%;">Applicant Provided
                </td>
                <td class="ccTableHeaderRow" style="width: 12%;">Confirmed
                </td>
                <td class="ccTableHeaderRow" style="width: 35%;">Discrepancy
                </td>

            </tr>

            <tr>
                <td class="ccTableDataRow">Zone Selection for Syndicates
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litZoneSelProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litZoneSelConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litZoneSelDisc" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Miles from Coast
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litMilesCoastProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litMilesCoastConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litMilesCoastDisc" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Occupancy
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litOccupProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litOccupConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litOccupDisc" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Construction Type
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litConstProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litConstConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litConstDisc" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Original Year Built
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litYearBltProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litYearBltConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litYearBltDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Effective Year Built
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litEffYearBltProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litEffYearBltConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litEffYearBltDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Protection Class
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPProtClassProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPProtClassConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPProtClassDisc" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Roof Type
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofTypeProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofTypeConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofTypeDisc" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Roof Age
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofAgeProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofAgeConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofAgeDisc" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Roof Material
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofMatProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofMatConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofMatDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Roof Shape
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofShapeProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofShapeConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofShapeDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Number of Stories
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litStoriesProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litStoriesConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litStoriesDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Square Feet
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litSqFtProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litSqFtConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litSqFtDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Coverage A
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCovAProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCovAConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCovADisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Coverage B
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCovBProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCovBConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCovBDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Automatic Water Shut-Off System
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litAutoWaterProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litAutoWaterConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litAutoWaterDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">
                    <asp:Label runat="server">Brush / Wildfire Zone</asp:Label>
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litBrushFireProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litBrushFireConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litBrushFireDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Fire Alarm
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCenFireProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCenFireConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCenFireDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Burglar Alarm
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCenBurgProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCenBurgConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litCenBurgDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Hurricane Protection Roof to Latest Code
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litHurrRoofProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litHurrRoofConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litHurrRoofDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Hurricane Protection Approved Opening Protection - All
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litHurrProtAllProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litHurrProtAllConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litHurrProtAllDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Farms?
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litFarmsProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litFarmsConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litFarmsDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Mobile Homes?
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litMHProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litMHConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litMHDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Homes with pools and/or diving boards that do not meet local code requirements
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPoolDiveProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPoolDiveConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPoolDiveDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Property with existing damage
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litDamageProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litDamageConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litDamageDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Dwellings not insured to 100% of RCV value
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="lit100RCVProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="lit100RCVConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="lit100RCVDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Dwellings with knob and tube wiring
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litKnobTubeProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litKnobTubeConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litKnobTubeDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Dwellings on the National Registry
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litNatRegProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litNatRegConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litNatRegDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Dwellings with EIFS siding older than 1998
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litEIFSProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litEIFSConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litEIFSDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Dwellings with wood stoves as primary heat
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litWoodStoveProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litWoodStoveConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litWoodStoveDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Dwellings with daycare or assisted living operations
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litDayCareProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litDayCareConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litDayCareDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Risks with acreage over 10
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="lit10AcresProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="lit10AcresConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="lit10AcresDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Risks in foreclosure proceedings
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litForecloseProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litForecloseConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litForecloseDisc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Developers speculation homes
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litSpecHomeProv" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litSpecHomeConf" runat="server" />
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litSpecHomeDisc" runat="server" />
                </td>
            </tr>

        </table>
        <p>&nbsp;</p>
        <table style="width: 100%" class="ccTable">
            <tr>
                <td class="ccTableHeaderRow" style="width: 100%;">Rating Information - Updates
                </td>
            </tr>
        </table>
        <table style="width: 100%" class="ccTable">
            <tr>
                <td class="ccTableDataRowLeft" style="width: 33%;">
                    <asp:Literal ID="litElecUpdt" runat="server" />
                </td>
                <td class="ccTableDataRowLeft" style="width: 34%;">
                    <asp:Literal ID="litPlumUpdt" runat="server" />
                </td>
                <td class="ccTableDataRowLeft" style="width: 33%;">
                    <asp:Literal ID="litHVACUpdt" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRowLeft">
                    <asp:Literal ID="litElecScope" runat="server" />
                </td>
                <td class="ccTableDataRowLeft">
                    <asp:Literal ID="litPlumScope" runat="server" />
                </td>
                <td class="ccTableDataRowLeft">
                    <asp:Literal ID="litHVACScope" runat="server" />
                </td>
            </tr>
        </table>
        <p>&nbsp;</p>
        <table style="width: 100%" class="ccTable">
            <tr>
                <td class="ccTableHeaderRow" style="width: 100%;">Rating Information - Protective Measures
                </td>
            </tr>
        </table>
        <table style="width: 100%" class="ccTable">
            <tr>
                <td colspan="2" class="ccTableDataRowLeft">Exposures
                </td>
            </tr>
            <tr>
                <td style="width: 20%;" class="ccTableDataRowRight">North:&nbsp;
                </td>
                <td style="width: 80%;" class="ccTableDataRowLeft">
                    <asp:Literal ID="litExpNorth" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 20%;" class="ccTableDataRowRight">South:&nbsp;
                </td>
                <td style="width: 80%;" class="ccTableDataRowLeft">
                    <asp:Literal ID="litExpSouth" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 20%;" class="ccTableDataRowRight">East:&nbsp;
                </td>
                <td style="width: 80%;" class="ccTableDataRowLeft">
                    <asp:Literal ID="litExpEast" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 20%;" class="ccTableDataRowRight">West:&nbsp;
                </td>
                <td style="width: 80%;" class="ccTableDataRowLeft">
                    <asp:Literal ID="litExpWest" runat="server" />
                </td>
            </tr>
        </table>
        <table style="width: 100%" class="ccTable">
            <tr>
                <td style="width: 50%;" class="ccTableDataRowLeft">
                    <asp:Literal ID="litRIBurg" runat="server" />

                </td>
                <td style="width: 50%;" class="ccTableDataRowLeft">
                    <asp:Literal ID="litRIBurgMonitor" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 50%;" class="ccTableDataRowLeft">
                    <asp:Literal ID="litRIFire" runat="server" />
                </td>
                <td style="width: 50%;" class="ccTableDataRowLeft">
                    <asp:Literal ID="litRIFireMonitor" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 50%;" class="ccTableDataRowLeft">
                    <asp:Literal ID="litRIProtClass" runat="server" />
                </td>
                <td style="width: 50%;" class="ccTableDataRowLeft">
                    <asp:Literal ID="litRIFireDept" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="ccTableDataRowLeft">
                    <asp:Literal ID="litRIFireDistance" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="ccTableDataRowLeft">
                    <asp:Literal ID="litRIComments" runat="server" />
                </td>
            </tr>
        </table>
        <p>&nbsp;</p>
        <table style="width: 100%" class="ccTable">
            <tr>
                <td class="ccTableHeaderRow" style="width: 100%;">Rural/Remote Risk Fire Rating Information
                </td>
            </tr>
        </table>
        <table style="width: 100%" class="ccTable">
            <tr>
                <td class="ccTableDataRowLeft" style="width: 50%;">Risk is within 1000' of an adequate water source
                </td>
                <td class="ccTableDataRowLeft" style="width: 34%;">
                    <asp:Literal ID="litWater1K" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRowLeft" style="width: 50%;">Fire department within 10 minutes of property
                </td>
                <td class="ccTableDataRowLeft" style="width: 34%;">
                    <asp:Literal ID="litFD10min" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRowLeft" style="width: 50%;">The risk is accessible by road year-round
                </td>
                <td class="ccTableDataRowLeft" style="width: 34%;">
                    <asp:Literal ID="litPropAcc" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRowLeft" style="width: 50%;">The risk is occupied or is checked daily
                </td>
                <td class="ccTableDataRowLeft" style="width: 34%;">
                    <asp:Literal ID="litDailyCheck" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRowLeft" style="width: 50%;">The property is visible to the neighbors
                </td>
                <td class="ccTableDataRowLeft" style="width: 34%;">
                    <asp:Literal ID="litPropVis" runat="server" />
                </td>
            </tr>

        </table>

    </div>
</div>
