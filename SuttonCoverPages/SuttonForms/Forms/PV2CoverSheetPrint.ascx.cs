﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using LC360Web;
using System.Text.RegularExpressions;

namespace Utilant.Forms.Forms
{
    public partial class PV2CoverSheetPrint : Utilant.Web.UI.WebControls.CaseForm
    {

        public int? iTotalNumRecs { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindGeneralCaseInformation();
                BindPhotos();
                BindHazards();
                BindFormData();
                iTotalNumRecs = GetRecommendations();
                litNumRecs.Text = iTotalNumRecs.ToString();

            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
            }
        }

        private void BindHazards()
        {
            var chs = Case.CaseHazards.Where(ch => ch.CaseForm.Deleted == false).OrderByDescending(ch => ch.HazardScore);
            foreach (var ch in chs)
            {
                this.litHazards.Text += ch.Description + "<br/>";
            }
        }

        private void BindGeneralCaseInformation()
        {
            /* Company Header Information */
            try
            {
                this.litCompanyAddress.Text = Constants.CompanyAddress;
                this.litCompanyCompletedForName.Text = Case.Customer.Name;
                this.litPhone.Text = Constants.CompanyPhone;
                this.litFax.Text = Constants.CompanyFax;
            }
            catch (Exception e)
            {
                this.litCompanyAddress.Text = "5656 Central Avenue<br />St. Petersburg, FL 33707";
                this.litCompanyCompletedForName.Text = "Customer Name";
                this.litPhone.Text = "727-384-5454";
                this.litFax.Text = "727-384-6565";
            }

            /* Left-hand columns */
            this.litInsuredName.Text = Case.PolicyHolderName;
            this.litLocationAddress.Text = Case.Address.Line1;
            this.litLocationCityStateZip.Text = Case.Address.City + ", " + Case.Address.PostalCode.StateName + ", " + Case.Address.PostalCode.PostalCodeValue;

            /* Right-hand columns */
            this.litPolicyNumber.Text = Case.PolicyNumber;

            Regex removeTime = new Regex(" 12:00:00 AM");
            this.litInspectionDate.Text = removeTime.Replace(GetValueOrDefault("DateInsp"), "");
            if (Case.Producer != null)
            {
                this.litAgent.Text = Case.Producer.Name;
                this.litAgentPhone.Text = Case.Producer.ContactPhone;
            }
            if (Case.Underwriter != null)
                this.litUnderwriter.Text = Case.Underwriter.FullName;
        }

        //TODO: Remove hacky regexs and add in some type safety maybe.
        private void BindFormData()
        {

            string sWork = "";
            string sBusOcc = "";

            // Occupancy
            litOccupied.Text = "N/A";
            sWork = GetValueOrDefault("BusTypeOccu");
            if (sWork != "N/A")
            {
                sBusOcc = GetBusOccupancy(sWork);
                litOccupied.Text = sBusOcc;
            }

            litConstruction.Text = GetValueOrDefault("Construction");
            litYearBuilt.Text = GetValueOrDefault("YearBuilt");

            // Square footage //
            sWork = GetValueOrDefault("RatingBasArea");
            if (sWork != "N/A")
            {
                litSqFtLbl1.Text = "Total Building SqFt:";
                litSqFtData1.Text = sWork;
            }

            sWork = GetValueOrDefault("RatingBasAreaUnit");
            if (sWork != "N/A")
            {
                litSqFtLbl1.Text = "Total Unit SqFt:";
                litSqFtData1.Text = sWork;
            }

            sWork = GetValueOrDefault("RatingBasAreaTenant");
            if (sWork != "N/A")
            {
                litSqFtLbl1.Text = "Total Tenant SqFt:";
                litSqFtData1.Text = sWork;
            }

            sWork = GetValueOrDefault("RatingBasAreaOther");
            if (sWork != "N/A")
            {
                litSqFtLbl1.Text = "Total Bldg SqFt:";
                litSqFtData1.Text = sWork;
            }

            sWork = GetValueOrDefault("ClubhouseBuildingArea");
            if (sWork != "N/A")
            {
                litSqFtLbl1.Text = "Total Clubhouse SqFt:";
                litSqFtData1.Text = sWork;
            }

            // Contents
            sWork = GetValueOrDefault("ValueOfOwnerContents");
            if (sWork != "N/A")
            {
                litContentsVal.Text = sWork;
            }

            sWork = GetValueOrDefault("ValueOfContentsTenant");
            if (sWork != "N/A")
            {
                litContentsVal.Text = sWork;
            }

            sWork = GetValueOrDefault("ValueOfClubhouseContents");
            if (sWork != "N/A")
            {
                litContentsVal.Text = sWork;
            }

            // Protection class
            sWork = GetValueOrDefault("ProtectionClass");
            if (sWork != "N/A")
            {
                litProtectionClass.Text = sWork;
            }
            sWork = GetValueOrDefault("PCIs");
            if (sWork != "N/A")
            {
                litProtectionClass.Text = sWork;
            }
            sWork = GetValueOrDefault("SplitPC");
            if (sWork != "N/A")
            {
                litProtectionClass.Text = sWork;
            }
            sWork = GetValueOrDefault("NotSplitPC");
            if (sWork != "N/A")
            {
                litProtectionClass.Text = sWork;
            }

            // Burglar alarm
            litBurgAlarm.Text = "N/A";
            sWork = GetValueOrDefault("Security");
            if (sWork != "N/A")
            {
                if (sWork.Contains("Local"))
                    litBurgAlarm.Text = "Local";
                else if (sWork.Contains("Central"))
                    litBurgAlarm.Text = "Central";
            }

            sWork = GetValueOrDefault("SecurityShort");
            if (sWork != "N/A")
            {
                if (sWork.Contains("Local"))
                    litBurgAlarm.Text = "Local";
                else if (sWork.Contains("Central"))
                    litBurgAlarm.Text = "Central";
            }

            sWork = GetValueOrDefault("SecurityOrderLong");
            if (sWork != "N/A")
            {
                if (sWork.Contains("Local"))
                    litBurgAlarm.Text = "Local";
                else if (sWork.Contains("Central"))
                    litBurgAlarm.Text = "Central";
            }


            // Updates
            litRoofUpdate.Text = GetValueOrDefault("RoofUpdYear");
            litHVACUpdate.Text = GetValueOrDefault("HVACUpd");
            litElecUpdate.Text = GetValueOrDefault("WiringUpd");
            litPlumbUpdate.Text = GetValueOrDefault("PlumbingUpd");


            // ** DEBUG **
            //string xxxxx = "";
        }

        private string GetBusOccupancy(string sValue)
        {

            var dt = SQL.ExecuteSql("SELECT Answer FROM QuestionPossibleAnswers WHERE questionID = (SELECT questionID FROM Questions where formid = '1DE1516E-95C1-44AF-B874-1723C97D922B' and dbColName = 'BusTypeOccu') AND [value] = '" + sValue + "'");
            if (dt.Tables.Count == 0)
                return null;
            if (dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["Answer"] != null)
            {
                return tr["Answer"].ToString();
            }

            return "N/A";
        }

        private decimal? GetCoverageA()
        {
            var dt = SQL.ExecuteSql("SELECT CoverageAOut FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if(dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageAOut"] != null)
            {
                decimal covA;
                if(Decimal.TryParse(tr["CoverageAOut"].ToString(), out covA))
                    return covA;
            }

            return null;
        }

        private string GetITV(decimal? coverageAIn, decimal? coverageAOut)
        {
            var cAOut = coverageAOut ?? 0;
            var cAIn = coverageAIn ?? 0;
            if(cAOut != 0)
                return Math.Round((cAIn / cAOut) * 100, 0) + " %";
            return "N/A";
        }

        private string GetValueOrDefault(string ColName, bool liabilityHas=true)
        {
            if (liabilityHas)
                return GetValueOrDefault(ColName, ColName, ColName, ColName, ColName, ColName, null, null, null);
            else
                return GetValueOrDefault(ColName, ColName, ColName, ColName, ColName, ColName, null, null, null);
        }

        private List<Guid> NoFormDataFound = new List<Guid>();

        private DataRow GetFormData(Guid FormID)
        {
            if (NoFormDataFound.Contains(FormID)) //so we don't have to hit the db on every request if we don't find a form on the Case the first time just skip.
                return null;

            DataRow dr = null;
            var caseForm = Case.CaseForms.Where(cf => cf.FormID == FormID && cf.Deleted == false).FirstOrDefault();
            if (caseForm != null)
            {
                string table = caseForm.Form.formTableName;
                var sql = "SELECT * FROM Form." + table + " WHERE FormID = @formInstanceID";
                    //f0e982e2-8495-4ac7-8dfd-dd54f129c8d3

                var ds = SQL.ExecuteSql(sql, "@formInstanceID", caseForm.FormInstanceID);
                if (ds.Tables.Count == 0)
                {
                    NoFormDataFound.Add(FormID);
                    return null;
                }
                var t = ds.Tables[0];
                if (t.Rows.Count > 0)
                    dr = t.Rows[0];
                else
                    NoFormDataFound.Add(FormID);
            }
            else
            {
                NoFormDataFound.Add(FormID);
            }

            return dr;
        }

        private string commaDelimitIfList(string list)
        {
            if (list.IsNotNullOrEmpty() && list.Contains('|'))//probably a list
                return string.Join(", ", list.Split('|').ToArray()).Trim(',',' ');
            return list;
        }

        private string GetColumnValue(DataRow row, string column)
        {
            try
            {
                if (row != null
                    && row[column] != null
                    && row[column].ToString().IsNotNullOrEmpty())
                    return commaDelimitIfList(row[column].ToString());
                return null;
            }
            catch
            {
                return null;
            }
        }
        
        private DataRow PackageV2Row = null;
        private DataRow PropV2Row = null;

        private string GetValueOrDefault(string PackageV2Col, string PropV2Col, string PropShortCol, string PackageLongCol, string PropLongCol, string LiabLongCol, string CommCookSup, string RestLiabSup, string RestAddOn)
        {
            // Package v2
            if (PackageV2Col != null)
            {
                if (this.PackageV2Row == null)
                    this.PackageV2Row = GetFormData(new Guid("1de1516e-95c1-44af-b874-1723c97d922b")); // Package v2

                var PackageShortVal = GetColumnValue(this.PackageV2Row, PackageV2Col);
                if (PackageShortVal != null)
                {
                    return PackageShortVal;
                }
            }

            // Property V2
            if (PackageLongCol != null)
            {
                if (this.PropV2Row == null)
                    this.PropV2Row = GetFormData(new Guid("5384deab-13cc-484c-b0bd-4bfb08cb8aa9")); // Property V2

                var PropV2Val = GetColumnValue(this.PropV2Row, PropV2Col);
                if (PropV2Val != null)
                {
                    return PropV2Val;
                }
            }

            return "N/A";
        }


        private void BindPhotos()
        {
            if (Case.Photos.Count == 0)
                this.image.ImageUrl = "~/images/custom/housenoimageavailable.jpg";
            else
            {
                var photoID = Case.Photos.OrderBy(p => p.DisplayOrder).Take(1).Select(p => p.PhotoID.ToString()).FirstOrDefault();
                image.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

                // ** DEBUG **
                //image.ImageUrl = "https://ecommerce3.sibfla.com/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

            }
        }

        private int GetRecommendations()
        {
            int iRetVal = 0;

                try
                {

                    iRetVal = SQL.GetInteger(@"Select  
												count(*) as TotalNumberOfRecs 
											from 
												caseformrecs cfr
												inner join CaseForms CF on CF.CaseFormID=cfr.CaseFormID and cfr.visible = 1
												inner join Cases C on C.CaseID=CF.CaseID
												left outer join RecommendationTypes RT on RT.RecommendationTypeID=cfr.RecommendationTypeID
											where 
												cf.deleted=0 and c.CaseID = @CaseId", "@CaseId", Case.CaseID);

                }
                catch
                {
                    iRetVal = 999;
            }

            return iRetVal;
        }
        public override void LoadData()
        {

        }

        public override void ClearFormData(FSMData.Form form)
        {

        }
    }
}