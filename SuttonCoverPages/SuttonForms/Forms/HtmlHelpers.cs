﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utilant.Forms.Forms
{
    public static class HtmlHelpers
    {
        public static void x(this System.Web.UI.WebControls.Literal literal, object value = null, string text = null)
        {
            if(value == null)
                return;

            ///Handle Integer
            if(value is int?)
            {
                int? nullInt = value as int?;

                if(nullInt != null && nullInt != 0)
                {
                    if(text.IsNotNullOrEmpty())
                    {
                        literal.Text = nullInt.Value.ToString() + text;
                    }
                    else
                        literal.Text = nullInt.Value.ToString();
                }
                else
                {
                    if(text.IsNotNullOrEmpty() && text == "%" || text == "sq ft" || text == " sqft" || text == "sqft" || text == " sq ft" || text == "$")
                    {
                        literal.Text = "0" + text;
                    }
                    else
                        literal.Text = "N/A";
                }
            }

            ///Handle String
            else if(value is string)
            {
                if(value == null || !value.ToString().IsNotNullOrWhiteSpace())
                    return;

                if(text.IsNotNullOrEmpty())
                {
                    if(text == "$")
                    {
                        literal.Text = (value.ToString().Length > 0) ? value.ToString() : "";
                    }
                    if(value.ToString().IsNotNullOrEmpty() && value.ToString().Contains("|"))
                    {
                        string tmp = value.ToString().Trim('|').Replace("|", ", ").Trim();
                        literal.Text = (tmp.Length > 0) ? tmp + text : "";
                    }
                    else
                    {
                        literal.Text = (value.ToString().Length > 0) ? (value.ToString().Trim() + text ?? "") : "";
                    }
                }
                else if(value.ToString().Contains("|"))
                {
                    string tmp = value.ToString().Trim('|').Replace("|", ", ").Trim();
                    literal.Text = (tmp.Length > 0) ? tmp : "";
                }
                else
                    literal.Text = (value.ToString().Length > 0) ? (value.ToString().Trim() ?? "") : "";

            }

            ///Handle Decimal
            else if(value is decimal)
            {
                decimal? nullDecimal = value as decimal?;

                if(nullDecimal != null && nullDecimal != 0)
                {
                    literal.Text = nullDecimal.Value.ToString();
                }
                else
                {
                    literal.Text = "N/A";
                }
            }

            ///Handle DateTime
            else if(value is DateTime?)
            {
                DateTime? nullDateTime = value as DateTime?;

                if(nullDateTime != null && nullDateTime.HasValue)
                {
                    if(text.IsNotNullOrEmpty())
                    {
                        literal.Text = nullDateTime.Value.ToShortDateString() + text;
                    }
                    else
                        literal.Text = nullDateTime.Value.ToShortDateString();
                }
                else
                {
                    literal.Text = "N/A";
                }
            }
        }

        public static void ToCommaSeparated(this System.Web.UI.WebControls.Literal literal, string value = null, string separator = null, string otherComment = null)
        {
            try
            {
                if(value.IsNullOrEmpty())
                    return;

                if(value.Contains("Other") && otherComment.IsNotNullOrEmpty())
                {
                    if(separator != null)
                    {
                        literal.Text = value + "   Test:" + value.Replace("Other", otherComment); // value.ToUpper().Replace("OTHER", otherComment).Trim().Replace(separator, ", ").Trim().TrimEnd(',');
                    }
                }
                else if(separator != null)
                {
                    literal.Text = value.Replace("|", ", ");
                }

                else
                    return;

            }
            catch(Exception ex)
            {
                throw new Exception("Could not parse separated list." + Environment.NewLine + ex.Message);
            }
        }

        //public static void ToCurrency(this string currency, object value, string type = "{0:C}", string label = null)
        //{
        //    if(value == null)
        //        return;

        //    if(value is string)
        //    {
        //        string moneyValue = value.ToString();
        //        if(moneyValue.IsNotNullOrEmpty() && label.IsNotNullOrEmpty())
        //        {
        //            currency = String.Format(label + type, moneyValue);
        //        }
        //        else
        //        {
        //            currency = String.Format(type, moneyValue);
        //        }
        //    }

        //    if(value is decimal)
        //    {
        //        decimal moneyValue = Convert.ToDecimal(value);
        //        if(label.IsNotNullOrEmpty())
        //        {
        //            currency = String.Format(label + type, moneyValue);
        //        }
        //        else
        //        {
        //            currency = String.Format(type, moneyValue);
        //        }
        //    }
        //}
    }
}