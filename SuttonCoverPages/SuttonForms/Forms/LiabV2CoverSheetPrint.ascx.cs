﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using LC360Web;
using System.Text.RegularExpressions;

namespace Utilant.Forms.Forms
{
    public partial class LiabV2CoverSheetPrint : Utilant.Web.UI.WebControls.CaseForm
    {

        public int? iTotalNumRecs { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindGeneralCaseInformation();
                BindPhotos();
                BindHazards();
                BindFormData();
                iTotalNumRecs = GetRecommendations();
                litNumRecs.Text = iTotalNumRecs.ToString();

            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
            }
        }

        private void BindHazards()
        {
            var chs = Case.CaseHazards.Where(ch => ch.CaseForm.Deleted == false).OrderByDescending(ch => ch.HazardScore);
            foreach (var ch in chs)
            {
                this.litHazards.Text += ch.Description + "<br/>";
            }
        }

        private void BindGeneralCaseInformation()
        {
            /* Company Header Information */
            try
            {
                this.litCompanyAddress.Text = Constants.CompanyAddress;
                this.litCompanyCompletedForName.Text = Case.Customer.Name;
                this.litPhone.Text = Constants.CompanyPhone;
                this.litFax.Text = Constants.CompanyFax;
            }
            catch (Exception e)
            {
                this.litCompanyAddress.Text = "5656 Central Avenue<br />St. Petersburg, FL 33707";
                this.litCompanyCompletedForName.Text = "Customer Name";
                this.litPhone.Text = "727-384-5454";
                this.litFax.Text = "727-384-6565";
            }

            /* Left-hand columns */
            this.litInsuredName.Text = Case.PolicyHolderName;
            this.litLocationAddress.Text = Case.Address.Line1;
            this.litLocationCityStateZip.Text = Case.Address.City + ", " + Case.Address.PostalCode.StateName + ", " + Case.Address.PostalCode.PostalCodeValue;

            /* Right-hand columns */
            this.litPolicyNumber.Text = Case.PolicyNumber;

            Regex removeTime = new Regex(" 12:00:00 AM");
            this.litInspectionDate.Text = removeTime.Replace(GetValueOrDefault("DateInsp"), "");
            if (Case.Producer != null)
            {
                this.litAgent.Text = Case.Producer.Name;
                this.litAgentPhone.Text = Case.Producer.ContactPhone;
            }
            if (Case.Underwriter != null)
                this.litUnderwriter.Text = Case.Underwriter.FullName;
        }

        private void BindFormData()
        {

            string sWork = "";
            string sBusOcc = "";

            // Occupancy
            litOccupied.Text = "N/A";
            sWork = GetValueOrDefault("BusTypeOccu");
            if (sWork != "N/A")
            {
                sBusOcc = GetBusOccupancy(sWork);
                litOccupied.Text = sBusOcc;
            }

            // SqFt // Only 1 will have a value
            string sSqFtLabel = "";
            string sSqFtData = "";
            sWork = GetValueOrDefault("RatingBasArea");
            if (sWork != "N/A")
            {
                sSqFtLabel = "Total Building SqFt:";
                sSqFtData = sWork;
            }

            sWork = GetValueOrDefault("RatingBasAreaUnit");
            if (sWork != "N/A")
            {
                sSqFtLabel = "Total Unit SqFt:";
                sSqFtData = sWork;            }


            sWork = GetValueOrDefault("RatingBasAreaTenant");
            if (sWork != "N/A")
            {
                sSqFtLabel = "Total Tenant SqFt:";
                sSqFtData = sWork;
            }

            sWork = GetValueOrDefault("RatingBasAreaOther");
            if (sWork != "N/A")
            {
                sSqFtLabel = "Total Building SqFt:";
                sSqFtData = sWork;
            }

            // Display only rating basis items that have values
            // Hide empty rows

            int iCells = 0; // number of cells with values

            string[] asLabels = new string[5];  //array of cell labels
            string[] asData = new string[5];    // array of cell data

            // SqFt 
            if (sSqFtLabel.Length > 0)
            {
                asLabels[0] = sSqFtLabel;
                asData[0] = sSqFtData;
                iCells = 1;
            }

            sWork = GetValueOrDefault("AnnualReceipts");
            if (sWork != "N/A")
            {
                asLabels[iCells] = "Annual reciepts:";
                asData[iCells] = sWork;
                iCells++;
            }
            
            sWork = GetValueOrDefault("Payroll");
            if (sWork != "N/A")
            {
                asLabels[iCells] = "Payroll:";
                asData[iCells] = sWork;
                iCells++;
            }

            sWork = GetValueOrDefault("CostOfSubcontractors");
            if (sWork != "N/A")
            {
                asLabels[iCells] = "Subcontractor cost:";
                asData[iCells] = sWork;
                iCells++;
            }

            sWork = GetValueOrDefault("RatingBasUnits");
            if (sWork != "N/A")
            {
                asLabels[iCells] = "Number of units:";
                asData[iCells] = sWork;
                iCells++;
            }
            else
            {
                sWork = GetValueOrDefault("RatingBasUnitsApart");
                if (sWork != "N/A")
                {
                    asLabels[iCells] = "Number of units:";
                    asData[iCells] = sWork;
                    iCells++;
                }
            }

            // show hide tables rows based on cell data
            trRB1.Visible = false;
            trRB2.Visible = false;
            trRB3.Visible = false;

            if (iCells > 0)
            {
                litLbl1.Text = asLabels[0];
                litData1.Text = asData[0];
                trRB1.Visible = true;

                if (iCells > 1)
                {
                    litLbl2.Text = asLabels[1];
                    litData2.Text = asData[1];
                
                    if (iCells >= 3)
                    {
                        litLbl3.Text = asLabels[2];
                        litData3.Text = asData[2];
                        trRB2.Visible = true;

                        if (iCells > 3)
                        {
                            litLbl4.Text = asLabels[3];
                            litData4.Text = asData[3];

                            if (iCells > 4)
                            {
                                litLbl5.Text = asLabels[4];
                                litData5.Text = asData[4];
                                trRB3.Visible = true;
                            }
                        }
                    }
                }
            }

            // Protection class
            sWork = GetValueOrDefault("ProtectionClass");
            if (sWork != "N/A")
            {
                litProtectionClass.Text = sWork;
            }
            sWork = GetValueOrDefault("PCIs");
            if (sWork != "N/A")
            {
                litProtectionClass.Text = sWork;
            }
            sWork = GetValueOrDefault("SplitPC");
            if (sWork != "N/A")
            {
                litProtectionClass.Text = sWork;
            }
            sWork = GetValueOrDefault("NotSplitPC");
            if (sWork != "N/A")
            {
                litProtectionClass.Text = sWork;
            }

        }

        private string GetBusOccupancy(string sValue)
        {

            var dt = SQL.ExecuteSql("SELECT Answer FROM QuestionPossibleAnswers WHERE questionID = (SELECT questionID FROM Questions where formid = '1DE1516E-95C1-44AF-B874-1723C97D922B' and dbColName = 'BusTypeOccu') AND [value] = '" + sValue + "'");
            if (dt.Tables.Count == 0)
                return null;
            if (dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["Answer"] != null)
            {
                return tr["Answer"].ToString();
            }

            return "N/A";
        }

        private decimal? GetCoverageA()
        {
            var dt = SQL.ExecuteSql("SELECT CoverageAOut FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if(dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageAOut"] != null)
            {
                decimal covA;
                if(Decimal.TryParse(tr["CoverageAOut"].ToString(), out covA))
                    return covA;
            }

            return null;
        }

        private string GetITV(decimal? coverageAIn, decimal? coverageAOut)
        {
            var cAOut = coverageAOut ?? 0;
            var cAIn = coverageAIn ?? 0;
            if(cAOut != 0)
                return Math.Round((cAIn / cAOut) * 100, 0) + " %";
            return "N/A";
        }

        //private string GetValueOrDefault(string ColName, bool liabilityHas=true)
        //{
        //    if (liabilityHas)
        //        return GetValueOrDefault(ColName, ColName, ColName, ColName, ColName, ColName, null, null, null);
        //    else
        //        return GetValueOrDefault(ColName, ColName, ColName, ColName, ColName, ColName, null, null, null);
        //}

        private List<Guid> NoFormDataFound = new List<Guid>();

        private DataRow GetFormData(Guid FormID)
        {
            if (NoFormDataFound.Contains(FormID)) //so we don't have to hit the db on every request if we don't find a form on the Case the first time just skip.
                return null;

            DataRow dr = null;
            var caseForm = Case.CaseForms.Where(cf => cf.FormID == FormID && cf.Deleted == false).FirstOrDefault();
            if (caseForm != null)
            {
                string table = caseForm.Form.formTableName;
                var sql = "SELECT * FROM Form." + table + " WHERE FormID = @formInstanceID";
                    //f0e982e2-8495-4ac7-8dfd-dd54f129c8d3

                var ds = SQL.ExecuteSql(sql, "@formInstanceID", caseForm.FormInstanceID);
                if (ds.Tables.Count == 0)
                {
                    NoFormDataFound.Add(FormID);
                    return null;
                }
                var t = ds.Tables[0];
                if (t.Rows.Count > 0)
                    dr = t.Rows[0];
                else
                    NoFormDataFound.Add(FormID);
            }
            else
            {
                NoFormDataFound.Add(FormID);
            }

            return dr;
        }

        private string commaDelimitIfList(string list)
        {
            if (list.IsNotNullOrEmpty() && list.Contains('|'))//probably a list
                return string.Join(", ", list.Split('|').ToArray()).Trim(',',' ');
            return list;
        }

        private string GetColumnValue(DataRow row, string column)
        {
            try
            {
                if (row != null
                    && row[column] != null
                    && row[column].ToString().IsNotNullOrEmpty())
                    return commaDelimitIfList(row[column].ToString());
                return null;
            }
            catch
            {
                return null;
            }
        }
        
        private DataRow LiabV2Row = null;

        private string GetValueOrDefault(string LiabV2Col)
        {
            // Liab v2
            if (LiabV2Col != null)
            {
                if (this.LiabV2Row == null)
                    this.LiabV2Row = GetFormData(new Guid("d553bdc7-0c82-477a-af67-74b8a9bcfea3")); // Liability v2

                var LiabV2Val = GetColumnValue(this.LiabV2Row, LiabV2Col);
                if (LiabV2Val != null)
                {
                    return LiabV2Val;
                }
            }
            
            return "N/A";
        }


        private void BindPhotos()
        {
            if (Case.Photos.Count == 0)
                this.image.ImageUrl = "~/images/custom/housenoimageavailable.jpg";
            else
            {
                var photoID = Case.Photos.OrderBy(p => p.DisplayOrder).Take(1).Select(p => p.PhotoID.ToString()).FirstOrDefault();
                image.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

                // ** DEBUG **
                //image.ImageUrl = "https://ecommerce3.sibfla.com/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

            }
        }

        private int GetRecommendations()
        {
            int iRetVal = 0;

                try
                {

                    iRetVal = SQL.GetInteger(@"Select  
												count(*) as TotalNumberOfRecs 
											from 
												caseformrecs cfr
												inner join CaseForms CF on CF.CaseFormID=cfr.CaseFormID and cfr.visible = 1
												inner join Cases C on C.CaseID=CF.CaseID
												left outer join RecommendationTypes RT on RT.RecommendationTypeID=cfr.RecommendationTypeID
											where 
												cf.deleted=0 and c.CaseID = @CaseId", "@CaseId", Case.CaseID);

                }
                catch
                {
                    iRetVal = 999;
            }

            return iRetVal;
        }
        public override void LoadData()
        {

        }

        public override void ClearFormData(FSMData.Form form)
        {

        }
    }
}