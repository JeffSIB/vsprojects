﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;

namespace Utilant.Forms.Forms
{
//    public partial class FirstCommercialCoverPrint : Utilant.Web.UI.WebControls.CaseForm
//    {
//        public int TotalNumberOfHazards { get; set; }
//        public int TotalNumberOfRecommendations { get; set; }
//        public int? TotalNumRecs { get; set; }
//        public List<PromotedFields> ListOfPromotedFields { get; set; }
//        public List<Hazards> ListOfHazards { get; set; }

//        public override void LoadData()
//        {

//        }
//        public override void ClearFormData(FSMData.Form form)
//        {

//        }

//        protected void Page_Load(object sender, EventArgs e)
//        {
//            //LoadTestData();
//            #region Instantiate Form Data

//            TotalNumberOfHazards = 0;
//            TotalNumberOfRecommendations = 0;

//            //FormData db = null;

//            //string formTableName = "CommercialPackagePart2";
//            //Guid FormId = new Guid("c7b94cf9-dae4-436c-9ce6-e9ea89dd394f");

//            AssignNullValues();

//            var Forms = Case.GetViewableCaseForms("Customer", false);

//            //var formInstanceID = (from cf in Case.CaseForms
//            //                      where cf.FormID == FormId && cf.Deleted == false
//            //                      select cf.FormInstanceID).FirstOrDefault();

//            //var formData = GetFormData(formTableName, formInstanceID);

//            #endregion

//            #region CoverPage

//            //DefaultPageOutputNullable(this.Page);

//            FSMData.Photo Photo = Case.Photos.OrderBy(p => p.DisplayOrder).FirstOrDefault();
//            string Role = Query.ToString("Role", "").ToLower();

//            if(!IsPostBack)
//            {
//                #region Photos and Images Section

//                if(Photo != null)
//                {
//                    imgMain.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + Photo.PhotoID + "-500.jpg";

//                    imgMain.Height = new Unit(Photo.GetHeightThatWillFitInBounds(400, 700) + " px");
//                    litPhotoTitle.Text = Photo.Description.Length > 0 ? "<br/><strong>" + Photo.Description + "</strong>" : "";
//                }
//                else
//                {
//                    imgMain.Visible = false;
//                }

//                #endregion

//                string locationAddress = this.Case.Address.HtmlMultiLine.IsNotNullOrEmpty() ? this.Case.Address.HtmlMultiLine : "N/A";
//                _InsuredAddress.x(locationAddress);

//                _PolicyHolderName.x(this.Case.PolicyHolderName);
//                _PolicyNumber.x(this.Case.PolicyNumber);
//                _OrderDate.x(this.Case.CaseDate.Ordered);
//                _SurveyDate.x("MM/DD/YYYY");
//                _EffectiveDate.x(this.Case.CaseDate.Effective);
//                if(this.Case.Producer != null)
//                {
//                    _AgentName.x(this.Case.Producer.Name ?? "");
//                }
//                if(this.Case.Underwriter != null)
//                {
//                    _Underwriter.x(this.Case.Underwriter.FullName);
//                }

//                #region Promoted Field Data, Coverage Data, etc

//                var pms = FSMData.PromotedField.GetNonNullPromotedFields(Case.CaseID).OrderBy(pm => pm.Field).ToList();

//                var coverageOut = pms.SingleOrDefault(pf => pf.Field == "Replacement Cost");

//                // They want these not shown on cover, just used for billing.
//                pms = pms.Where(pm => !pm.Field.ContainedIn("Second trip required", "Appointment Attempt", "APT", "Close out (PL)", "Insufficient Address", "Replacement Cost")).ToList();

//                if(Case.CaseField.CoverageAIn.HasValue && coverageOut != null && !string.IsNullOrEmpty(coverageOut.Value))
//                {
//                    decimal coverageOutVal = Convert.ToDecimal(coverageOut.Value);

//                    _CoverageAOut.Text = coverageOutVal.ToString().IsNotNullOrEmpty() ? coverageOutVal.ToString("d2") : "";

//                    var coveragePercent = Math.Abs((((decimal)Case.CaseField.CoverageAIn.Value) / coverageOutVal)).ToString("P0");

//                    pms.Add(new FSMData.PromotedFieldWrapper
//                    {
//                        Field = "Coverage A",
//                        Value = Case.CaseField.CoverageAIn.Value.ToString("C0")
//                    });
//                    pms.Add(new FSMData.PromotedFieldWrapper
//                    {
//                        Field = "Replacement Cost",
//                        Value = coverageOutVal.ToString("C0")
//                    });
//                    pms.Add(new FSMData.PromotedFieldWrapper
//                    {
//                        Field = "Percent to Value",
//                        Value = coveragePercent
//                    });
//                }
//                else if(coverageOut != null && !string.IsNullOrEmpty(coverageOut.Value))
//                {
//                    decimal coverageOutVal = Convert.ToDecimal(coverageOut.Value);

//                    pms.Add(new FSMData.PromotedFieldWrapper
//                    {
//                        Field = "Replacement Cost",
//                        Value = coverageOutVal.ToString("C0")
//                    });
//                }

//                #endregion

//                TotalNumRecs = (int)GetRecommendations(TotalNumberOfRecommendations);
//                //_TotalNumRecs.Text = (GetRecommendations(TotalNumberOfRecommendations)).ToString();
//                //_TotalHazards.Text = BindHazards();

//            #endregion

//                #region Inspections Page(s)

//                _PolicyNumber2.x(this.Case.PolicyNumber);
//                _PolicyHolderName2.x(this.Case.PolicyHolderName);
//                _PrintSmallLogo.ImageUrl = "/images/custom/mainLogoSmall.png";

//                GetAllForms();

//                #endregion
//            }
//            else
//            {
//                AssignNullValues();
//            }
//        }


//        private void LoadTestData()
//        {
//            ListOfHazards = new List<Hazards>();

//            var h = new Hazards();
//            h.Description = "Hazard 1";
//            h.Image = "/images/warning.png";
//            ListOfHazards.Add(h);

//            h = new Hazards();
//            h.Description = "Hazard 2";
//            h.Image = "/images/warning.png";
//            ListOfHazards.Add(h);

//            h = new Hazards();
//            h.Description = "Hazard 3";
//            h.Image = "/images/error.png";
//            ListOfHazards.Add(h);

//            h = new Hazards();
//            h.Description = "Hazard 4";
//            h.Image = "/images/error.png";
//            ListOfHazards.Add(h);
//        }

//        private List<PromotedFields> GetPromotedFields()
//        {
//            try
//            {
//                var data = new List<PromotedFields>();

//                var dataSet = SQL.ExecuteSql(@"select
//														cf.caseFormID,
//														pfd.Description,
//														pfv.value
//												from
//														CaseForms cF
//														inner join forms f on f.formID = cf.formID
//														inner join questions q on q.formID = f.formID
//														inner join promotedFieldDefinitions pfd on pfd.ColumnName = q.PromotableField
//														inner join dbo.PromotedFieldsVertical pfv on pfv.PromotedFieldID = pfd.PromotedFieldDefinitionID and pfv.caseID = cf.caseID
//												where
//														cf.Deleted = 0
//														and cf.caseID = @CaseId
//														and q.PromotableField is not null
//														and pfv.value is not null
//												order by
//														cf.caseFormID,
//														Description", "@CaseId", this.Case.CaseID);

//                if(dataSet == null || dataSet.Tables == null || dataSet.Tables.Count < 1)
//                {
//                    return null;
//                }

//                DataTable table = dataSet.Tables[0];
//                if(table != null && table.Rows != null && table.Rows.Count > 0)
//                {
//                    data = table.CastToList<PromotedFields>();

//                    if(data == null || data.Count < 1)
//                    {
//                        return null;
//                    }

//                    return data;
//                }
//                else
//                {
//                    return null;
//                }

//                #region TestData
//                //var lstPF = new List<PromotedFields>();
//                //var guid1 = new Guid("E79D5DB0-33A6-4026-8C47-9D812B00CEF8");
//                //var guid2 = new Guid("92952E0B-DA11-404F-A774-C024260E1F22");
//                //var guid3 = new Guid("B6E79E7E-B2F8-480D-9CB2-4198135B684D");
//                //var guid4 = new Guid("F9AAC5A5-AB98-497E-98ED-14E6EBD4023B");
//                //var guid5 = new Guid("AA0A6B91-59EB-43F2-9F32-6C3F82EF783A");
//                //var guid6 = new Guid("30536788-FC71-416C-88F7-82962BD83D12");
//                //var guid7 = new Guid("CBD9BA14-5348-422C-82A4-70536A473636");
//                //var guid8 = new Guid("9BE10D42-7334-4EEE-823E-6835AC14CDFD");
//                //var guid9 = new Guid("23D5BC55-A3A4-4A92-94FA-8F8BC5D73D41");
//                //var guid10 = new Guid("1BB0D178-F534-46B6-864B-48FD7FC07858");
//                //var guid11 = new Guid("4589C711-11EB-448E-8AB6-FAB2FD0DE414");
//                //var guid12 = new Guid("033630D9-ACEF-4EAA-9C28-C1BBA96DC131");

//                //var pf = new PromotedFields();

//                //pf.CaseFormId = guid1; 
//                //pf.Description = "First";
//                //pf.Value = "Value 1";
//                //lstPF.Add(pf);

//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid1; 
//                //pf.Description = "Second"; 
//                //pf.Value = "Value 2";
//                //lstPF.Add(pf);

//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid2; pf.Description = "Third"; pf.Value = "Value 3";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid4; pf.Description = "Fourth"; pf.Value = "Value 4";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid5; pf.Description = "Fifth"; pf.Value = "Value 5";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid6; pf.Description = "Sixth"; pf.Value = "Value 6";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid7; pf.Description = "Seventh"; pf.Value = "Value 7";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid8; pf.Description = "Eighth"; pf.Value = "Value 8";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid9; pf.Description = "Nineth"; pf.Value = "Value 9";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid10; pf.Description = "Tenth"; pf.Value = "Value 10";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid11; pf.Description = "Eleventh"; pf.Value = "Value 11";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid12; pf.Description = "Twelfth"; pf.Value = "Value 12";
//                //lstPF.Add(pf);

//                //pf.CaseFormId = guid1;
//                //pf.Description = "First";
//                //pf.Value = "Value 1";
//                //lstPF.Add(pf);

//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid1;
//                //pf.Description = "Second";
//                //pf.Value = "Value 2";
//                //lstPF.Add(pf);

//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid3; pf.Description = "Third"; pf.Value = "Value 3";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid4; pf.Description = "Fourth"; pf.Value = "Value 4";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid5; pf.Description = "Fifth"; pf.Value = "Value 5";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid6; pf.Description = "Sixth"; pf.Value = "Value 6";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid7; pf.Description = "Seventh"; pf.Value = "Value 7";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid8; pf.Description = "Eighth"; pf.Value = "Value 8";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid9; pf.Description = "Nineth"; pf.Value = "Value 9";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid10; pf.Description = "Tenth"; pf.Value = "Value 10";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid11; pf.Description = "Eleventh"; pf.Value = "Value 11";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid12; pf.Description = "Twelfth"; pf.Value = "Value 12";
//                //lstPF.Add(pf);

//                //pf.CaseFormId = guid1;
//                //pf.Description = "First";
//                //pf.Value = "Value 1";
//                //lstPF.Add(pf);

//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid1;
//                //pf.Description = "Second";
//                //pf.Value = "Value 2";
//                //lstPF.Add(pf);

//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid3; pf.Description = "Third"; pf.Value = "Value 3";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid4; pf.Description = "Fourth"; pf.Value = "Value 4";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid5; pf.Description = "Fifth"; pf.Value = "Value 5";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid6; pf.Description = "Sixth"; pf.Value = "Value 6";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid7; pf.Description = "Seventh"; pf.Value = "Value 7";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid8; pf.Description = "Eighth"; pf.Value = "Value 8";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid9; pf.Description = "Nineth"; pf.Value = "Value 9";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid10; pf.Description = "Tenth"; pf.Value = "Value 10";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid11; pf.Description = "Eleventh"; pf.Value = "Value 11";
//                //lstPF.Add(pf);
//                //pf = new PromotedFields();
//                //pf.CaseFormId = guid12; pf.Description = "Twelfth"; pf.Value = "Value 12";
//                //lstPF.Add(pf);

//                //data = lstPF;
//                //return data;
//                #endregion
//            }
//            catch
//            {
//                return null;
//            }
//        }

//        private object GetRecommendations(object ds)
//        {
//            if(ds is int)
//            {
//                try
//                {

//                    ds = SQL.GetInteger(@"Select  
//												count(*) as TotalNumberOfRecs 
//											from 
//												caseformrecs cfr
//												inner join CaseForms CF on CF.CaseFormID=cfr.CaseFormID and cfr.visible = 1
//												inner join Cases C on C.CaseID=CF.CaseID
//												left outer join RecommendationTypes RT on RT.RecommendationTypeID=cfr.RecommendationTypeID
//											where 
//												cf.deleted=0 and c.CaseID = @CaseId", "@CaseId", this.Case.CaseID);

//                    return ds;
//                }
//                catch
//                {
//                    return 0;
//                }
//            }
//            if(ds is List<Recommendations>)
//            {
//                try
//                {
//                    ds = SQL.ExecuteSql(@"Select 
//												ISNULL(RT.Name,'Unknown Category') as Name, 
//												count(*) as Count 
//											from 
//												caseformrecs cfr
//												inner join CaseForms CF on CF.CaseFormID=cfr.CaseFormID and cfr.visible = 1
//												inner join Cases C on C.CaseID=CF.CaseID
//												left outer join RecommendationTypes RT on RT.RecommendationTypeID=cfr.RecommendationTypeID
//											where 
//												cf.deleted=0 and c.CaseID = @caseID
//											Group by 
//												ISNULL(RT.Name,'Unknown Category')", "@caseID", this.Case.CaseID).Tables[0].CastToList<Recommendations>();

//                    if(ds == null)
//                        return null;

//                    return ds;
//                }
//                catch
//                {
//                    return null;
//                }
//            }

//            return null;
//        }

//        private int GetRecommendationsPerForm(Guid CaseFormId)
//        {
//            try
//            {

//                var ds = SQL.GetInteger(@"Select  
//												count(*) as TotalNumberOfRecs 
//											from 
//												caseformrecs cfr
//												inner join CaseForms CF on CF.CaseFormID=cfr.CaseFormID and cfr.visible = 1
//												inner join Cases C on C.CaseID=CF.CaseID
//												left outer join RecommendationTypes RT on RT.RecommendationTypeID=cfr.RecommendationTypeID
//											where 
//												cf.deleted=0 and c.CaseID = @CaseId and cf.CaseFormId = @CaseFormId", "@CaseId", this.Case.CaseID, "@CaseFormId", CaseFormId);

//                return ds;
//            }
//            catch
//            {
//                return 0;
//            }
//        }

//        //        private List<Recommendations> GetRecommendations()
//        //        {
//        //            try
//        //            {
//        //                List<Recommendations> listRecs = new List<Recommendations>();

//        //                var ds = SQL.ExecuteSql(@"Select ISNULL(RT.Name,'Unknown Category') as Name, count(*) as Count from caseformrecs cfr
//        //                                          inner join CaseForms CF on CF.CaseFormID=cfr.CaseFormID and cfr.visible = 1
//        //                                          inner join Cases C on C.CaseID=CF.CaseID
//        //                                          left outer join RecommendationTypes RT on RT.RecommendationTypeID=cfr.RecommendationTypeID
//        //                                          where cf.deleted=0 and c.CaseID = @caseID
//        //                                          Group by ISNULL(RT.Name,'Unknown Category')", "@caseID", this.Case.CaseID);

//        //                if(ds == null || ds.Tables == null || ds.Tables.Count < 1)
//        //                {
//        //                    //throw new Exception("Could not map data to class");
//        //                }
//        //                else
//        //                {
//        //                    var table = ds.Tables[0];
//        //                    if(table != null && table.Rows != null && table.Rows.Count > 0)
//        //                    {
//        //                        listRecs = table.CastToList<Recommendations>();

//        //                        if(listRecs == null || listRecs.Count < 1)
//        //                        {
//        //                            //throw new Exception("Could not map data to class");
//        //                        }
//        //                    }
//        //                }

//        //                return listRecs;
//        //            }
//        //            catch(Exception ex)
//        //            {
//        //                throw new Exception("Could not gather Recommendations. " + ex.Message);
//        //            }
//        //        }

//        //private void PrintRecommendations(List<Recommendations> recs)
//        //{
//        //    int totalRecs = 0;
//        //    var items = (from r in recs
//        //                 select new
//        //                 {
//        //                     Name = r.Name,
//        //                     Count = r.Count,
//        //                 }).ToList();

//        //    if(items.Count > 0)
//        //    {
//        //        totalRecs = items.Sum(s => s.Count);
//        //    }

//        //    _TotalNumRecs.Text = totalRecs.ToString();
//        //    //_rptrRecs.DataSource = items;
//        //    //_rptrRecs.DataBind();
//        //}

//        private string BindHazards()
//        {
//            var hazards = (from h in Case.CaseHazards
//                           where h.CaseForm.Deleted == false
//                           orderby h.HazardScore descending
//                           select new
//                           {
//                               h.Description,
//                               Score = h.HazardScore.ToString() + " pts",
//                               Image = h.CancelsPolicy ? "/images/error.png" : "/images/warning.png"
//                           }).ToList();

//            if(hazards == null || hazards.Count < 1)
//                return "0";
//            //TestNumberOfRecommendations = 4;
//            //return TestNumberOfRecommendations.ToString();
//            return hazards.Count.ToString();
//        }

//        private List<Hazards> BindHazards(Guid CaseFormId)
//        {
//            try
//            {
//                var hazards = new List<Hazards>();

//                var tmp = (from h in Case.CaseHazards
//                           where h.CaseForm.Deleted == false
//                           where h.CaseFormID == CaseFormId
//                           orderby h.HazardScore descending
//                           select new
//                           {
//                               h.Description,
//                               //Score = h.HazardScore.ToString() + " pts",
//                               Image = h.CancelsPolicy ? "/images/error.png" : "/images/warning.png"
//                           }).ToList();

//                if(tmp != null && tmp.Count >= 1)
//                {
//                    foreach(var item in tmp)
//                    {
//                        var haz = new Hazards();
//                        haz.Description = item.Description;
//                        haz.Image = item.Image;

//                        hazards.Add(haz);
//                    }
//                }

//                return hazards;
//            }
//            catch
//            {
//                return null;
//            }

//        }

//        private void AssignNullValues()
//        {
//            _AgentName.Text = "";
//            _CoverageAIn.Text = "";
//            _CoverageAOut.Text = "";
//            _CoverageAPercentValue.Text = "";
//            //_CoverPrintLogo.AlternateText = "";
//            _EffectiveDate.Text = "";
//            _InsuredAddress.Text = "";
//            //_TotalNumRecs.Text = "";
//            _OrderDate.Text = "";
//            _PolicyHolderName.Text = "";
//            _PolicyNumber.Text = "";
//            _PrintSmallLogo.AlternateText = "";
//            _SurveyDate.Text = "";
//            //_TotalHazards.Text = "";
//            _Underwriter.Text = "";
//        }

//        private void DefaultPageOutputNullable(Control Page)
//        {
//            foreach(Control ctrl in Page.Controls)
//            {
//                if(ctrl is Literal)
//                {
//                    ((Literal)(ctrl)).Text = "Test";
//                }
//                else
//                {
//                    //if(ctrl.Controls.Count > 0)
//                    //{
//                    //    SetTextBoxBackColor(ctrl, clr);
//                    //}
//                }
//            }
//        }

//        public HtmlTable CreateTable(Guid CaseFormId, List<PromotedFields> data, HtmlGenericControl container)
//        {
//            var table = new HtmlTable();

//            //table.Attributes["style"] = "border: 1px solid #000; width: 50%;";

            

//            table = FillTable(CaseFormId, table, data);
//            table.Attributes["class"] = "FormsTables";
//            //table.Align = "left";

//            return table;
//        }

//        public HtmlTable CreateTable(List<Hazards> data, HtmlGenericControl container)
//        {
//            var table = new HtmlTable();

//           // table.Attributes["style"] = "border: 1px solid #000; width: 50%;";
            

//            table = FillTable(table, data);
//            table.Attributes["class"] = "FormsTables";
//            return table;
//        }

//        public HtmlTable FillTable(Guid CaseFormId, HtmlTable table, List<PromotedFields> data = null)
//        {
//            if(data != null)
//            {
//                int cols = 2;
//                int counter = 0;
//                int recCount = 0;
//                string headerValue = "";
//                string cellValue = "";

//                foreach(var item in data)
//                {
//                    headerValue = item.Description ?? "&nbsp;";
//                    cellValue = item.Value ?? "&nbsp;";

//                    var tr = BuildTableCells(data, cols, counter, recCount, headerValue, cellValue);

//                    table.Rows.Add(tr);
//                    counter++;
//                }

//                var trL = new HtmlTableRow();

//                var thL = new HtmlTableCell();
//                thL.Attributes["style"] = "width: 20%; text-align: right; font-weight: Bold;";
//                thL.InnerHtml = "Recommendations:";
//                trL.Cells.Add(thL);
//                int RecCount = GetRecommendationsPerForm(CaseFormId);
//                var tdL = new HtmlTableCell();
//                tdL.Attributes["style"] = "width: 80%; text-align: left; font-weight: Bold;";
//                tdL.InnerHtml = (RecCount > 0) ? ("&nbsp;&nbsp;<font color='Red'>" + RecCount + "</font>") : ("&nbsp;&nbsp;" + RecCount);
//                trL.Cells.Add(tdL);

//                table.Rows.Add(trL);
//            }
//            else
//            {
//                table = CreateNullTable(table, "Promoted Fields");
//            }

//            return table;
//        }

//        private static HtmlTableRow BuildTableCells(List<PromotedFields> data, int cols, int counter, int recCount, string headerValue, string cellValue)
//        {
//            var tr = new HtmlTableRow();
//            for(int i = 0; i <= cols - 1; i++)
//            {
//                if(counter == data.Count) //Put Number of Recs at bottom
//                {
//                    if(i == 0)//Header Column
//                    {
//                        var th = new HtmlTableCell();
//                        th.Attributes["style"] = "width: 20%; text-align: right; font-weight: Bold;";
//                        th.InnerHtml = "Recommendations: ";
//                        tr.Cells.Add(th);
//                    }
//                    else
//                    {
//                        var td = new HtmlTableCell();
//                        td.Attributes["style"] = "width: 80%; text-align: left;";
//                        td.InnerHtml = (recCount > 0) ? ("&nbsp;&nbsp;<font-color='Red'> " + recCount + "</font>") : ("&nbsp;&nbsp;" + recCount);
//                        tr.Cells.Add(td);
//                    }
//                }
//                else
//                {
//                    if(i == 0)//Header Column
//                    {
//                        var th = new HtmlTableCell();
//                        th.Attributes["style"] = "width: 20%; text-align: right; font-weight: Bold;";
//                        th.InnerHtml = headerValue + ":";
//                        tr.Cells.Add(th);
//                    }
//                    else
//                    {
//                        var td = new HtmlTableCell();
//                        td.Attributes["style"] = "width: 80%; text-align: left;";
//                        td.InnerHtml = "&nbsp;&nbsp;" + cellValue;
//                        tr.Cells.Add(td);
//                    }
//                }
//            }
//            return tr;
//        }

//        public HtmlTable FillTable(HtmlTable table, List<Hazards> data = null)
//        {
//            if(data != null)
//            {
//                int cols = 2;

//                var tr = new HtmlTableRow();

//                var th = new HtmlTableCell();
//                th.Attributes["style"] = "width: 80%; text-align: right; font-weight: Bold;";
//                th.InnerHtml = (data.Count > 0) ? ("Hazards:&nbsp;&nbsp;&nbsp;<font color='Red'>" + data.Count() + "</font>") : ("Hazards:&nbsp;&nbsp;&nbsp;<font color='Black'>" + data.Count() + "</font>");
//                tr.Cells.Add(th);
//                var td = new HtmlTableCell();
//                td.Attributes["style"] = "width: 20%; text-align: left; color: Silver;";
//                td.InnerHtml = "&nbsp;";
//                tr.Cells.Add(td);

//                table.Rows.Add(tr);

//                foreach(var item in data)
//                {
//                    tr = new HtmlTableRow();

//                    for(int i = 0; i <= cols - 1; i++)
//                    {
//                        if(i == 0)
//                        {
//                            td = new HtmlTableCell();
//                            td.Attributes["style"] = "width: 80%; text-align: right;";
//                            td.InnerHtml = item.Description + ":";
//                            tr.Cells.Add(td);
//                        }
//                        else
//                        {
//                            td = new HtmlTableCell();
//                            td.Attributes["style"] = "width: 20%; text-align: left;";
//                            td.InnerHtml = "<img src=\"" + item.Image + "\">";
//                            tr.Cells.Add(td);
//                        }
//                    }

//                    table.Rows.Add(tr);
//                }
//            }
//            else
//            {
//                table = CreateNullTable(table);
//            }

//            return table;
//        }

//        private HtmlTable CreateNullTable(HtmlTable table, string typeOfTable = null)
//        {
//            int cols = 2;
//            int rows = 1;

//            var tr = new HtmlTableRow();

//            var th = new HtmlTableCell();
//            th.Attributes["style"] = "width: 20%; text-align: right; font-weight: Bold;";
//            th.InnerHtml = typeOfTable.IsNotNullOrEmpty() ? (typeOfTable + ":&nbsp;&nbsp;&nbsp;0") : "No Data Available.";
//            tr.Cells.Add(th);
//            var td = new HtmlTableCell();
//            td.Attributes["style"] = "width: 80%; text-align: left;";
//            td.InnerHtml = "&nbsp;";
//            tr.Cells.Add(td);

//            table.Rows.Add(tr);

//            for(int i = 0; i <= rows; i++)
//            {
//                tr = new HtmlTableRow();

//                for(int j = 0; j <= cols - 1; j++)
//                {
//                    if(i == 0)
//                    {
//                        td = new HtmlTableCell();
//                        td.Attributes["style"] = "width: 20%; text-align: right;";
//                        td.InnerHtml = "&nbsp;";
//                        tr.Cells.Add(td);
//                    }
//                    else
//                    {
//                        td = new HtmlTableCell();
//                        td.Attributes["style"] = "width: 80%; text-align: left;";
//                        td.InnerHtml = "&nbsp;";
//                        tr.Cells.Add(td);
//                    }
//                }

//                table.Rows.Add(tr);
//            }
//            return table;
//        }

//        private HtmlTable CreateNullTableWithRecs(HtmlTable table, Guid CaseFormId, string typeOfTable = null)
//        {
//            int cols = 2;
//            int rows = 1;
//            int? RecCount = GetRecommendationsPerForm(CaseFormId);

//            var tr = new HtmlTableRow();

//            var th = new HtmlTableCell();
//            th.Attributes["style"] = "width: 80%; text-align: right; font-weight: Bold;";
//            th.InnerHtml = typeOfTable.IsNotNullOrEmpty() ? (typeOfTable + ":&nbsp;&nbsp;&nbsp;") : "Recommendations:&nbsp;&nbsp;&nbsp;";
//            th.InnerHtml += (RecCount > 0) ? ("&nbsp;&nbsp;<font color='Red'>" + RecCount + "</font>") : ("&nbsp;&nbsp;" + RecCount);
//            tr.Cells.Add(th);
//            var td = new HtmlTableCell();
//            td.Attributes["style"] = "width: 20%; text-align: left;";
//            td.InnerHtml = "&nbsp;";
//            tr.Cells.Add(td);

//            table.Rows.Add(tr);

//            for(int i = 0; i <= rows; i++)
//            {
//                tr = new HtmlTableRow();

//                for(int j = 0; j <= cols - 1; j++)
//                {
//                    if(i == 0)
//                    {
//                        td = new HtmlTableCell();
//                        td.Attributes["style"] = "width: 80%; text-align: right;";
//                        td.InnerHtml = "&nbsp;";
//                        tr.Cells.Add(td);
//                    }
//                    else
//                    {
//                        td = new HtmlTableCell();
//                        td.Attributes["style"] = "width: 20%; text-align: left;";
//                        td.InnerHtml = "&nbsp;";
//                        tr.Cells.Add(td);
//                    }
//                }

//                table.Rows.Add(tr);
//            }
//            return table;
//        }

//        public void GetAllForms()
//        {
//            var formDetails = GetFormsData();
//            var promotedFields = GetPromotedFields();

//            if(formDetails != null)
//            {
//                foreach(var forms in formDetails)
//                {
//                    var container = new HtmlGenericControl("div");
//                    var div = new HtmlGenericControl("div");
//                    var hr = new HtmlGenericControl("hr");
//                    var divFormPromotedFields = new HtmlTable();
//                    divFormPromotedFields.Attributes["class"] = "FormsTables";
//                    var divHazardsContainer = new HtmlTable();
//                    divHazardsContainer.Attributes["class"] = "FormsTables";
//                    var clear = new HtmlGenericControl("div");
//                    clear.Attributes["style"] = "clear: both; width: 100%; margin-top: 25px;";
//                    clear.InnerHtml = "<center>&nbsp;</center>";
//                    div.InnerHtml = "<h3><font-color='Silver'><b><i>" + forms.FormName + "</i></b></font></h3>"; //Blue: #1F497D
//                    div.Attributes["style"] = "width: 100%;";
//                    container.Controls.Add(div);
//                    container.Controls.Add(hr);

//                    #region PromotedFields

//                    if(forms != null && promotedFields != null && promotedFields.Count >= 1)
//                    {
//                        var pf = (from x in promotedFields
//                                  where x.CaseFormId == forms.CaseFormId
//                                  select x).ToList();
//                        divFormPromotedFields = CreateTable(forms.CaseFormId, pf, container);
//                    }
//                    else
//                    {
//                        divFormPromotedFields = CreateNullTableWithRecs(divFormPromotedFields, forms.CaseFormId, "Recommendations");  
//                    }
//                    divFormPromotedFields.Attributes["class"] = "FormsTables";
//                    divFormPromotedFields.Align = "left";
//                    container.Controls.Add(divFormPromotedFields);

//                    #endregion
					
//                    #region Hazards

//                    var hazards = BindHazards(forms.CaseFormId);
//                    if(forms != null && hazards != null && hazards.Count >=1)
//                    {
//                        divHazardsContainer = CreateTable(hazards, container);
//                    }
//                    else
//                    {
//                        divHazardsContainer = CreateNullTable(divHazardsContainer, "Hazards");
//                    }
//                    divHazardsContainer.Attributes["class"] = "FormsTables";
//                    container.Controls.Add(divHazardsContainer);

//                    #endregion

//                    container.Controls.Add(clear);
//                    ph.Controls.Add(container);
//                }
//            }
//            else
//            {

//            }
//        }

//        private List<FormDetails> GetFormsData()
//        {
//            var formDetails = SQL.ExecuteSql(@"select 
//													f.FormId, cf.CaseFormId, ISNULL(f.FormName, f.FormTableName) as FormName, f.FormTableName
//											   from 
//													cases c
//													inner join CaseForms cf on cf.CaseID = c.CaseID
//													inner join Forms f on f.formID = cf.FormID
//											   where cf.Deleted = 0
//													and cf.CaseID = @CaseId", "@CaseId", this.Case.CaseID).Tables[0].CastToList<FormDetails>();
//            return formDetails;
//        }
//    }

//    //public class AllForms
//    //{
//    //    public List<FormDetails> FormDetails { get; set; }
//    //    public List<PromotedFields> PromotedFields { get; set; }
//    //}

//    public class PromotedFields
//    {
//        public Guid CaseFormId { get; set; }
//        public string Description { get; set; }
//        public string Value { get; set; }
//    }

//    public class FormDetails
//    {
//        public Guid FormId { get; set; }
//        public Guid CaseFormId { get; set; }
//        public string FormName { get; set; }
//        public string FormTableName { get; set; }
//    }

//    public class Recommendations
//    {
//        public string Name { get; set; }
//        public int Count { get; set; }
//    }

//    public class Hazards
//    {
//        public string Description { get; set; }
//        public string Image { get; set; }
//    }
}