﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommercialCoverSummaryPrint.ascx.cs"
    Inherits="Utilant.Forms.Forms.CommercialCoverSummaryPrint" %>
<style type="text/css">
    div.pagewrapper
    {
        width: 7.5in;
        padding: 0px, 0px, 0px, 0px;
        border: 1px solid #FFF;
        margin: auto;
        font-family: Calibri;
        color: #404040;
    }
    
    div.pagewrapper2
    {
        width: 7.5in;
        padding: 0px, 0px, 0px, 0px;
        border: 1px solid #FFF;
        margin: auto;
        font-family: Calibri;
        color: #404040;
        page-break-before: always;
    }
    
    table.div.pagewrapper
    {
        table-layout: fixed;
        vertical-align: text-top;
    }
    
    /*<%--span.Title
    {
        margin-left: auto;
        margin-right: auto;
        font-family: Candara;
        font-size: x-large;
        color: #333;
    }--%>*/
    
    span.ReportName
    {
        margin-left: auto;
        margin-right: auto;
        font-family: Calibri;
        font-size: x-large;
    }
    
    span.ReportSummary
    {
        font-family: Calibri;
        font-size: large;
        font-weight: bold;
        text-align: center;
    }
    
    .FormsTables
    {
        padding-left: 5px;
        border: 0px solid #000;
        width: 50%;
    }
    
    th, td.FormsTables
    {
        padding-left: 5px;
        vertical-align: text-top;
        border: 0px solid #000;
        width: 50%;
    }
    
    .header
    {
        text-align: center;
    }
    
    div.SecondHeader
    {
        font-size: large;
    }
    
    #imgMain
    {
        border: 1px solid #ccc;
    }
    
    table.PolicyInformation
    {
        font-size: 1em;
    }
    
    .Bold
    {
        font-weight: bold;
    }
    
    .Red
    {
        color: Red;
    }
    
    .Logo
    {
        float: right;
        margin: 10px 15px 3px 0px;
    }
    
    #litPhotoTitle
    {
        font-style: italic;
        font-size: small;
    }
    
    div.pagebreak
    {
        page-break-after: always;
    }
    
    @media print
    {
        pagewrapper
        {
            page-break-after: always;
        }
    }
</style>
<div class="pagewrapper">
    <table class="header" style="width: 100%;">
        <tr>
            <%= LC360Web.Constants.GetCustomerHeader(Case, null)%>
        </tr>
        <tr>
            <td>
                <br />
                <br />
                <center>
                    <span class="ReportName"></span>
                </center>
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <%--<hr />--%>
            </td>
        </tr>
        <tr>
            <td>
                <div style="text-align: center; font-size: small;">
                    <asp:Image ID="imgMain" runat="server" /><asp:Literal ID="litPhotoTitle" runat="server"></asp:Literal>
                </div>
            </td>
        </tr>
        <tr>
            <td class="PolicyInformation">
                <br />
                <center>
                    <b>
                        <asp:Literal ID="_PolicyHolderName" runat="server"></asp:Literal></b></center>
            </td>
        </tr>
        <tr>
            <td>
                <center>
                    <asp:Literal ID="_InsuredAddress" runat="server"></asp:Literal></center>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <table style="width: 100%;" align="center" class="PolicyInformation">
        <tr>
            <td style="width: 25%; text-align: right;">
                <span class="Bold">Policy Number:</span>
            </td>
            <td style="width: 25%;">
                &nbsp;<asp:Literal ID="_PolicyNumber" runat="server"></asp:Literal>
            </td>
            <td style="width: 25%; text-align: right;">
                <span class="Bold">Total Hazards Found:&nbsp;</span>
            </td>
            <td style="width: 25%;">
                <span style='color: Red;'><asp:Literal  ID="_TotalHazards" runat="server"></asp:Literal></span>
<%--                <%= (Case.HazardScoreTotal ?? 0) > 0 ? "<span style='color: Red;'>&nbsp;" + Case.HazardScoreTotal.Value + "</span>" : "0" %>--%>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <span class="Bold">Order Date:</span>
            </td>
            <td>
                &nbsp;<asp:Literal ID="_OrderDate" runat="server"></asp:Literal>
            </td>
            <td style="text-align: right;">
                <span class="Bold"># of Recommendations:</span>
            </td>
            <td>
                <%--&nbsp;<span><asp:Literal  ID="_TotalNumRecs" runat="server" Visible="false"></asp:Literal></span>--%>
                <%= (TotalNumRecs ?? 0) > 0 ? "<span style='color: Red;'>&nbsp;" + TotalNumRecs + "</span>" : "0"%>
            </td>
        </tr>
        <%--       <tr>
            <td style="text-align: right;">
                <span class="Bold">Survey Date:</span>
            </td>
            <td>
                &nbsp;<asp:Literal ID="_SurveyDate" runat="server"></asp:Literal>
            </td>
            <td style="text-align: right;">
                <span class="Bold">Coverage A In:</span>
            </td>
            <td>
                &nbsp;<span><asp:Literal  ID="_CoverageAIn" runat="server"></asp:Literal></span>
            </td>
        </tr>--%>
        <tr>
            <td style="text-align: right;">
                <span class="Bold">Effective Date:</span>
            </td>
            <td>
                &nbsp;<asp:Literal ID="_EffectiveDate" runat="server"></asp:Literal>
            </td>
            <td style="text-align: right;">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <%--       <tr>
            <td style="text-align: right;">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td style="text-align: right;">
                <span class="Bold">Coverage A % Value:</span>
            </td>
            <td>
                &nbsp;<span><asp:Literal  ID="_CoverageAPercentValue" runat="server"></asp:Literal></span>
            </td>
        </tr>--%>
        <tr>
            <td style="text-align: right;">
                <span class="Bold">Agent Name:</span>
            </td>
            <td>
                &nbsp;<asp:Literal ID="_AgentName" runat="server"></asp:Literal>
            </td>
            <td style="text-align: right;">
                <span class="Bold">Underwriter:</span>
            </td>
            <td>
                &nbsp;<asp:Literal ID="_Underwriter" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</div>
<%--Page 1 Container--%>
<div class="pagewrapper2">
    <table class="header" style="width: 100%;">
        <tr>
            <%= LC360Web.Constants.GetCustomerHeader(Case, null)%>
        </tr>
    </table>
    <br />
    <center>
        <span class="ReportName">Inspection Summary</span>
    </center>
    <hr />
    <br />
    <br />
    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
</div>
<%--Page 2 Container--%>
