﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using LC360Web;
using System.Text.RegularExpressions;

namespace Utilant.Forms.Forms
{
    public partial class AW2CoverSheetPrint : Utilant.Web.UI.WebControls.CaseForm
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindGeneralCaseInformation();
                BindPhotos();
                BindHazards();
                BindFormData();
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
            }
        }

        private void BindHazards()
        {
            var chs = Case.CaseHazards.Where(ch => ch.CaseForm.Deleted == false).OrderByDescending(ch => ch.HazardScore);
            foreach (var ch in chs)
            {
                this.litHazards.Text += ch.Description + "<br/>";
            }
        }

        private void BindGeneralCaseInformation()
        {
            /* Company Header Information */
            try
            {
                this.litCompanyAddress.Text = Constants.CompanyAddress;
                this.litCompanyCompletedForName.Text = Case.Customer.Name;
                this.litPhone.Text = Constants.CompanyPhone;
                this.litFax.Text = Constants.CompanyFax;
            }
            catch (Exception e)
            {
                this.litCompanyAddress.Text = "5656 Central Avenue<br />St. Petersburg, FL 33707";
                this.litCompanyCompletedForName.Text = "Customer Name";
                this.litPhone.Text = "727-384-5454";
                this.litFax.Text = "727-384-6565";
            }

            /* Left-hand columns */
            this.litInsuredName.Text = Case.PolicyHolderName;
            this.litLocationAddress.Text = Case.Address.Line1;
            this.litLocationCityStateZip.Text = Case.Address.City + ", " + Case.Address.PostalCode.StateName + ", " + Case.Address.PostalCode.PostalCodeValue;

            /* Right-hand columns */
            this.litPolicyNumber.Text = Case.PolicyNumber;

            Regex removeTime = new Regex(" 12:00:00 AM");
            this.litInspectionDate.Text = removeTime.Replace(GetValueOrDefault("InterviewDate"), "");
            if (Case.Producer != null)
            {
                this.litAgent.Text = Case.Producer.Name;
                this.litAgentPhone.Text = Case.Producer.ContactPhone;
            }
            if (Case.Underwriter != null)
                this.litUnderwriter.Text = Case.Underwriter.FullName;
            
        }

        private void BindFormData()
        {

            string sWork = "";

            // is this a Liability case type?
            bool bIsLiab = false;
            if (Case.CaseType.CaseTypeID == new Guid("aa52775c-e59e-4441-903a-a4a9901c8547"))
            {
                bIsLiab = true;
            }

            // Occupancy
            litOccupied.Text = "N/A";
            sWork = GetValueOrDefault("BusinessTypeOccupancy");
            litOccupied.Text = sWork;

            //Construction 
            if (bIsLiab)
            {
                litConstructionHdr.Text = "";
                litYearBuiltLbl.Text = "";
                litConstructionLbl.Text = "";
                litConstruction.Text = "";
            }
            else
            {
                litConstructionHdr.Text = "Construction & Year Built";
                litConstructionLbl.Text = "Construction:";
                litConstruction.Text = GetValueOrDefault("Construct");
                litYearBuiltLbl.Text = "Year built:";
                litYearBuilt.Text = GetValueOrDefault("YearBuilt");

            }

            sWork = GetValueOrDefault("TotalBuildingSquareFootage");
            if (sWork == "N/A")
                this.litSqFt.Text = "N/A";
            else
            {
                if (sWork.IndexOf(".") > 0)
                this.litSqFt.Text = sWork.Substring(0,sWork.IndexOf("."));
            }

            sWork = GetValueOrDefault("ProtectionClass");
            if (sWork != "N/A")
            {
                this.litLblProtClass.Text = "Protection class:";
                this.litProtectionClass.Text = sWork;
            }

            //sWork = GetValueOrDefault("ProtectionTen");
            //this.litPC10.Text = (sWork == "N/A") ? "" : sWork;

            this.litSmokeDet.Text = GetValueOrDefault("ActivelyManagedSmokeDetectors");

            sWork = GetValueOrDefault("CentralStationOrLocal");
            if (sWork == "N/A")            
                this.litBurgAlarm.Text = "None";
                else
                this.litBurgAlarm.Text = sWork;


            // Updates
            tblUpdates.Visible = false;
            if (!bIsLiab)
            {
                tblUpdates.Visible = true;

                // Updates
                this.litRoofUpdate.Text = GetValueOrDefault("RoofAge");

                this.litWiringUpdate.Text = GetValueOrDefault("WireUpd");
                sWork = GetValueOrDefault("WireYrUpd");
                if (sWork == "N/A")
                    this.litWiringYr.Text = "";
                else
                    this.litWiringYr.Text = sWork;

                this.litPlumbingUpdate.Text = GetValueOrDefault("PlumbUpd");
                sWork = GetValueOrDefault("PlumbYrUpd");
                if (sWork == "N/A")
                    this.litPlumbingYr.Text = "";
                else
                    this.litPlumbingYr.Text = sWork;

                this.litHVACUpdate.Text = GetValueOrDefault("HVACUpdate");
                sWork = GetValueOrDefault("HVACYrUpd");
                if (sWork == "N/A")
                    litHVACYr.Text = "";
                else
                    litHVACYr.Text = sWork;
            }

        }

        private decimal? GetCoverageA()
        {
            var dt = SQL.ExecuteSql("SELECT CoverageAOut FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if(dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageAOut"] != null)
            {
                decimal covA;
                if(Decimal.TryParse(tr["CoverageAOut"].ToString(), out covA))
                    return covA;
            }

            return null;
        }

        private string GetITV(decimal? coverageAIn, decimal? coverageAOut)
        {
            var cAOut = coverageAOut ?? 0;
            var cAIn = coverageAIn ?? 0;
            if(cAOut != 0)
                return Math.Round((cAIn / cAOut) * 100, 0) + " %";
            return "N/A";
        }

        private string GetValueOrDefault(string ColName, bool liabilityHas=true)
        {
            if (liabilityHas)
                return GetValueOrDefault(ColName, ColName, ColName);
            else
                return GetValueOrDefault(ColName, ColName, ColName);
        }

        private List<Guid> NoFormDataFound = new List<Guid>();

        private DataRow GetFormData(Guid FormID)
        {
            if (NoFormDataFound.Contains(FormID)) //so we don't have to hit the db on every request if we don't find a form on the Case the first time just skip.
                return null;

            DataRow dr = null;
            var caseForm = Case.CaseForms.Where(cf => cf.FormID == FormID && cf.Deleted == false).FirstOrDefault();
            if (caseForm != null)
            {
                string table = caseForm.Form.formTableName;
                var sql = "SELECT * FROM Form." + table + " WHERE FormID = @formInstanceID";

                var ds = SQL.ExecuteSql(sql, "@formInstanceID", caseForm.FormInstanceID);
                if (ds.Tables.Count == 0)
                {
                    NoFormDataFound.Add(FormID);
                    return null;
                }
                var t = ds.Tables[0];
                if (t.Rows.Count > 0)
                    dr = t.Rows[0];
                else
                    NoFormDataFound.Add(FormID);
            }
            else
            {
                NoFormDataFound.Add(FormID);
            }

            return dr;
        }

        private string commaDelimitIfList(string list)
        {
            if (list.IsNotNullOrEmpty() && list.Contains('|'))//probably a list
                return string.Join(", ", list.Split('|').ToArray()).Trim(',',' ');
            return list;
        }

        private string GetColumnValue(DataRow row, string column)
        {
            try
            {
                if (row != null
                    && row[column] != null
                    && row[column].ToString().IsNotNullOrEmpty())
                    return commaDelimitIfList(row[column].ToString());
                return null;
            }
            catch
            {
                return null;
            }
        }
        
        private DataRow PackageRow = null;
        private DataRow PropertyRow = null;
        private DataRow LiabilityRow = null;

        private string GetValueOrDefault(string sPackage, string sProperty, string sLiab)
        {
            //  AmWins Package
            if (sPackage != null)
            {
                if (PackageRow == null)
                    PackageRow = GetFormData(new Guid("cf074592-572f-4488-baca-75c73530603f")); 

                var PackageVal = GetColumnValue(PackageRow, sPackage);
                if (PackageVal != null)
                {
                    return PackageVal;
                }
            }

            //  AmWins Property
            if (sProperty != null)
            {
                if (PropertyRow == null)
                    PropertyRow = GetFormData(new Guid("9e6bec85-8903-4ed9-9f34-10950e916b78"));

                var PropertyVal = GetColumnValue(PropertyRow, sProperty);
                if (PropertyVal != null)
                {
                    return PropertyVal;
                }
            }

            //  AmWins Liability
            if (sLiab != null)
            {
                if (LiabilityRow == null)
                    LiabilityRow = GetFormData(new Guid("f55a8ea0-3fd5-4749-b9a2-8677a52430a7"));

                var LiabVal = GetColumnValue(LiabilityRow, sLiab);
                if (LiabVal != null)
                {
                    return LiabVal;
                }
            }



            return "N/A";
        }


        private void BindPhotos()
        {
            if (Case.Photos.Count == 0)
                this.image.ImageUrl = "~/images/custom/housenoimageavailable.jpg";
            else
            {
                var photoID = Case.Photos.OrderBy(p => p.DisplayOrder).Take(1).Select(p => p.PhotoID.ToString()).FirstOrDefault();
                this.image.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

                // ** DEBUG **
                //this.image.ImageUrl = "https://ecommerce3.sibfla.com/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

            }
        }
        private string GetBusOccupancy(string sValue)
        {

            var dt = SQL.ExecuteSql("SELECT Answer FROM QuestionPossibleAnswers WHERE questionID = (SELECT questionID FROM Questions where formid = '1DE1516E-95C1-44AF-B874-1723C97D922B' and dbColName = 'BusTypeOccu') AND [value] = '" + sValue + "'");
            if (dt.Tables.Count == 0)
                return null;
            if (dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["Answer"] != null)
            {
                return tr["Answer"].ToString();
            }

            return "N/A";
        }
        public override void LoadData()
        {

        }

        public override void ClearFormData(FSMData.Form form)
        {

        }
    }
}