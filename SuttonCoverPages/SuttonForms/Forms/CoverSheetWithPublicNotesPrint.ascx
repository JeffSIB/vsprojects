﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CoverSheetWithPublicNotesPrint.ascx.cs"
	Inherits="Utilant.Forms.Forms.CoverSheetPrintWithPublicNotes" %>
<style>
	.head { font-weight: bold; }
	.info, .fieldsAndHazards { width: 100%; }
	.info td { font-size: 12px; padding: 5px; vertical-align: top; }
	.fieldsAndHazards td { padding: 2px; vertical-align: top; }
	.fieldsAndHazards th { font-size: 12px; padding: 10px; vertical-align: top; border-bottom: 1px solid black; text-align: left; }
	.coveragePercentage { background-color: #f5f5f5; color: black; border: 1px solid #ccc; padding: 3px; margin: 2px 0px; text-align: center; }
	
	
	.headerRow_Print { font-weight: bold; background-color: <%= mainCatBack %>; color: <%= mainCatFore %>; width: 100%; padding-top: 3px; padding-bottom: 3px; padding-left: 3px; }
	
	.noteHeader { background-color: <%= sub1CatBack %>; color: <%= sub1CatFore %>; padding-top: 3px; padding-bottom: 3px; padding-left: 3px; }
	.noteBody { margin-left: 23px; padding-top: 5px; padding-bottom: 5px; }
	.noteDate { font-style: italic; }
	.caseNotesExplanation { padding-top: 3px; padding-bottom: 3px; padding-left: 3px; padding-bottom: 10px; }
</style>
<div style="width: 7.5in; border: 0px solid #FFF; margin: auto;">
	<%= LC360Web.Constants.GetCustomerHeader(Case, null)%>
	<asp:Literal ID="litCoveragePercent" runat="server"></asp:Literal><br />
	<br />
	<asp:Literal ID="litPreUnderwriterOptions" runat="server"></asp:Literal>
	<br />
	<div style="text-align: center;">
		<asp:Image ID="imgMain" Style="border: 1px solid #ccc;" runat="server" /><asp:Literal
			ID="litPhotoTitle" runat="server"></asp:Literal>
	</div>
	<br />
	<br />
	<table class="fieldsAndHazards">
		<tr>
			<th>
				<div class="coverSectionHeader <%= (Case.HazardScoreTotal ?? 0) > 0? "redText" : "" %>">
					<%= (Case.HazardScoreTotal ?? 0) > 0 ? "<span style='float:right'>Total Score:&nbsp;" + Case.HazardScoreTotal.Value + "&nbsp;</span>" : "" %>
					Adverse Conditions</div>
			</th>
			<td>
			</td>
			<th>
				Important Fields
			</th>
		</tr>
		<tr>
			<td style="width: 45%">
				<asp:GridView ID="gvHazards" runat="server" AutoGenerateColumns="false" ShowHeader="false"
					Width="100%" GridLines="None" EnableTheming="false">
					<EmptyDataTemplate>
						No conditions to report
					</EmptyDataTemplate>
					<Columns>
						<asp:BoundField DataField="Description" />
						<asp:BoundField DataField="Score" />
						<asp:ImageField DataImageUrlField="Image" />
					</Columns>
				</asp:GridView>
			</td>
			<td>
			</td>
			<td style="width: 45%">
				<asp:GridView ID="gvImportantFields" GridLines="None" EnableTheming="false" ShowHeader="false"
					runat="server">
					<EmptyDataTemplate>
						No fields to display
					</EmptyDataTemplate>
				</asp:GridView>
			</td>
		</tr>
	</table>
	<asp:Panel ID="pnlPublicNotes" runat="server" Visible="false" Width="100%">
		<table style="width: 100%; page-break-before: always; page-break-after: always;">
			<tr>
				<td>
					<%= LC360Web.Constants.GetCustomerHeader(Case, null)%>
					<br />
					<br />
					<br />
				</td>
			</tr>
			<tr>
				<td class="headerRow_Print">
					<%= LC360Web.Constants.GetResource("Case") %>
					Notes
				</td>
			</tr>
			<tr>
				<td class="caseNotesExplanation">
					<%= LC360Web.Constants.GetResource("CaseNotesPDfDescription") %>
				</td>
			</tr>
			<asp:Repeater ID="repCaseNotes" runat="server">
				<ItemTemplate>
					<tr>
						<td>
							<div class="noteHeader">
								<%# Eval("Role")%>
								Note - <span class="noteDate">
									<%# Eval("Date")%>
								</span>
							</div>
							<div class="noteBody">
								<%# Eval("Notes")%>
							</div>
						</td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</asp:Panel>
	<asp:PlaceHolder ID="phDisclaimer" runat="server"></asp:PlaceHolder>
</div>
