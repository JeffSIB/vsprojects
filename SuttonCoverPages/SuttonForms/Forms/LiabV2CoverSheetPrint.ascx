﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LiabV2CoverSheetPrint.ascx.cs"
    Inherits="Utilant.Forms.Forms.LiabV2CoverSheetPrint" %>


<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }

    .main-page {
        width: 7.5in;
        min-height: 10.3in;
        margin: auto;
        padding: 10px 10px 10px 10px;
        font-family: Arial;
        font-size: 14px;
        padding-left: 60px;
        padding-right: 60px;
    }

    .headerRow {
        background-color: #81bd01;
        color: White;
        font-weight: bold;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }

    .logo {
        float: left;
    }

    .clear {
        clear: both;
    }

    .companyInfo {
        float: right;
        text-align: right;
        width: 30%;
        color: #006cc5;
        font-weight: bold;
    }

    .label {
        font-weight: bold;
    }

    .firstTableRow td {
        padding-top: 15px;
    }

    .lastTableRow td {
        padding-bottom: 15px;
    }
</style>

<%--START OF PAGE--%>
<div class="main-page">
    <%--PAGE HEADER AND CASE INFORMATION--%>
    <div class="page-header">
        <div>
            <img id="imgHeader" class="logo" alt="[Company Logo]" src='<%= Page.ResolveClientUrl("~/images/Custom/mainlogo.png") %>' />

            <%--            // ** DEBUG **--%>
            <%--            <img id="imgHeader" class="logo" alt="[Company Logo]" src='https://ecommerce3.sibfla.com/App_Themes/images/mainLogoSmall.png' />--%>

            <div class="companyInfo">
                <table>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyAddress" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyCityStateZip" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Phone:
                            <asp:Literal ID="litPhone" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Fax:
                            <asp:Literal ID="litFax" runat="server" /></td>
                    </tr>
                </table>

            </div>
            <div class="clear" />
        </div>
        <div>
            <table style="border-spacing: 0">
                <tr class="lastTableRow">
                    <td class="label" colspan="2">Completed Inspection Report For:</td>
                    <td colspan="2">
                        <asp:Literal ID="litCompanyCompletedForName" runat="server" /></td>
                </tr>
            </table>
            <table style="border-spacing: 0">
                <tr>
                    <td class="label">Insured Name:
                    </td>
                    <td>
                        <asp:Literal ID="litInsuredName" runat="server" />
                    </td>
                    <td class="label">Policy Number:
                    </td>
                    <td>
                        <asp:Literal ID="litPolicyNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label" style="width: 20%">Location Address:
                    </td>
                    <td style="width: 30%">
                        <asp:Literal ID="litLocationAddress" runat="server" />
                    </td>
                    <td class="label" style="width: 20%">Inspection Date:
                    </td>
                    <td style="width: 30%">
                        <asp:Literal ID="litInspectionDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">City,State,Zip:
                    </td>
                    <td>
                        <asp:Literal ID="litLocationCityStateZip" runat="server" />
                    </td>
                    <td class="label">Agent:
                    </td>
                    <td>
                        <asp:Literal ID="litAgent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">&nbsp;
                    </td>

                    <td>&nbsp;</td>
                    <td class="label">Agent Phone:
                    </td>
                    <td>
                        <asp:Literal ID="litAgentPhone" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td class="label">Underwriter:
                    </td>
                    <td>
                        <asp:Literal ID="litUnderwriter" runat="server" />
                    </td>
                </tr>
            </table>
            <table style="border-spacing: 0">
                <tr class="headerRow">
                    <td colspan="4" class="label">Occupancy
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td style="width: 25%" class="label">Insured Property is:
                    </td>
                    <td style="width: 25%">
                        <asp:Literal ID="litOccupied" runat="server" />
                    </td>
                    <td style="width: 25%">&nbsp;</td>
                    <td style="width: 25%">&nbsp;</td>
                </tr>
                <tr class="headerRow">
                    <td colspan="4" class="label">Rating Basis
                    </td>
                </tr>
                <tr id="trRB1" runat="server">
                    <td class="label">
                        <asp:Literal ID="litLbl1" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="litData1" runat="server" />
                    </td>
                    <td class="label">
                        <asp:Literal ID="litLbl2" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="litData2" runat="server" />
                    </td>
                </tr>
                <tr id="trRB2" runat="server">
                    <td class="label">
                        <asp:Literal ID="litLbl3" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="litData3" runat="server" />
                    </td>
                    <td class="label">
                        <asp:Literal ID="litLbl4" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="litData4" runat="server" />
                    </td>
                </tr>
                <tr id="trRB3" runat="server" class="lastTableRow">
                    <td class="label">
                        <asp:Literal ID="litLbl5" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="litData5" runat="server" />
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="headerRow">
                    <td colspan="2" class="label">Fire Protection / Security
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">Protection class:
                    </td>
                    <td>
                        <asp:Literal ID="litProtectionClass" runat="server" />

                    </td>
                </tr>
            </table>
            <div>
                <div style="width: 45%; float: left">
                    <table>
                        <tr class="headerRow">
                            <td class="label">Recommendations
                            </td>
                        </tr>
                        <tr class="lastTableRow">
                            <td class="label">Number of Recommendations: &nbsp;&nbsp;
                                <asp:Literal ID="litNumRecs" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr class="headerRow">
                            <td class="label">Hazards / Exposures Noted
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Literal ID="litHazards" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="float: left; width: 45%; margin-left: 15px;">
                    <asp:Image ID="image" Width="380px" runat="server" />
                </div>
                <div class="clear" />
            </div>
        </div>
    </div>
</div>
