﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using LC360Web;
using System.Text.RegularExpressions;

namespace Utilant.Forms.Forms
{
    public partial class CrumpCoverSheetPrint : Utilant.Web.UI.WebControls.CaseForm
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindGeneralCaseInformation();
                BindPhotos();
                BindHazards();
                BindFormData();
            }
            catch
            {

            }
        }

        private void BindHazards()
        {
            var chs = Case.CaseHazards.Where(ch => ch.CaseForm.Deleted == false).OrderByDescending(ch => ch.HazardScore);
            foreach (var ch in chs)
            {
                this.litHazards.Text += ch.Description + "<br/>";
            }
        }

        private void BindGeneralCaseInformation()
        {
            /* Company Header Information */
            try
            {
                this.litCompanyAddress.Text = Constants.CompanyAddress;
                this.litCompanyCompletedForName.Text = Case.Customer.Name;
                this.litPhone.Text = Constants.CompanyPhone;
                this.litFax.Text = Constants.CompanyFax;
            }
            catch (Exception e)
            {
                this.litCompanyAddress.Text = "Error loading address";
                this.litCompanyCompletedForName.Text = "";
                this.litPhone.Text = "716-555-1234";
                this.litFax.Text = "716-555-1234";
            }

            /* Left-hand columns */
            this.litInsuredName.Text = Case.PolicyHolderName;
            this.litInsuredAddress.Text = Case.Address1.Line1;
            this.litCityStateZip.Text = Case.Address1.PostalCode.City + ", " + Case.Address1.PostalCode.StateName + ", " + Case.Address1.PostalCode.PostalCodeValue;
            this.litLocationAddress.Text = Case.Address.Line1;
            this.litLocationCityStateZip.Text = Case.Address.City + ", " + Case.Address.PostalCode.StateName + ", " + Case.Address.PostalCode.PostalCodeValue;

            /* Right-hand columns */
            this.litPolicyNumber.Text = Case.PolicyNumber;

            Regex removeTime = new Regex(" 12:00:00 AM");
            this.litInspectionDate.Text = removeTime.Replace(GetValueOrDefault("DateInspected"), "");
            if (Case.Producer != null)
            {
                this.litAgent.Text = Case.Producer.Name;
                this.litAgentPhone.Text = Case.Producer.ContactPhone;
            }
            if (Case.Underwriter != null)
                this.litUnderwriter.Text = Case.Underwriter.FullName;
        }

        //TODO: Remove hacky regexs and add in some type safety maybe.
        private void BindFormData()
        {
            /* Occupancy / Construction */
            this.litOccupied.Text = GetValueOrDefault("Occupancy", "Occupancy", "Occupied", "Occupancy", "Structure");
            this.litConstruction.Text = GetValueOrDefault("ExtWallSupportsPercent", "ExtWallSupportsPercent", "ExtWallConstruction", null, "ExtWallConst");
            this.litYearBuilt.Text = GetValueOrDefault("YearBuilt", false);

            /* Fire Protection / Breed of Dog */
            this.litProtectionClass.Text = GetValueOrDefault("ProtectionClass", false);
            this.litBreed.Text = GetValueOrDefault("DogBredd", "DogBredd", "DogBredd", "IfDogs", null);
            
            /* Water Exposure */

            Regex removeDecimal = new Regex("\\.0+$");
            this.litGulf.Text = removeDecimal.Replace(GetValueOrDefault("GulfMiles", "GulfMiles", "GulfMiles", "GulfMiles", "GlufMiles"), " miles");
            this.litAtlOcean.Text = removeDecimal.Replace(GetValueOrDefault("AtlanticMiles", "AtlanticMiles", "AtlanticMiles", "AtlanticMiles", "AtlanticMlies"), " miles");

            // Get CoverageA In/Out and variance
            try
            {
                decimal? dCoverageAOut = null;

                // Check for coverage A (E2V)
                dCoverageAOut = GetCoverageAOut();

                // if null check for MSB
                if (dCoverageAOut == null)
                    dCoverageAOut = GetCoverageOutA();

                // Coverage A In
                if (Case.CaseField.CoverageAIn.HasValue)
                {
                    // populate field on cover sheet
                    litBuildingCoverage.Text = Case.CaseField.CoverageAIn.Value.ToString("C");
                }

                if (Case.CaseField.CoverageAIn.HasValue && dCoverageAOut != null)
                {
                    decimal dCoverageAOutVal = Convert.ToDecimal(dCoverageAOut.Value);

                    var coveragePercent = Math.Abs((((decimal)Case.CaseField.CoverageAIn.Value) / dCoverageAOutVal)).ToString("P");
                    litReplacmenetCost.Text = dCoverageAOutVal.ToString("C");
                    litITV.Text = coveragePercent;
                    //litCoverageA.Text = "<div class='coveragePercentage'>Coverage In: " + Case.CaseField.CoverageAIn.Value.ToString("C") + " Coverage Out: " + dCoverageAOutVal.ToString("C") + " Coverage is " + coveragePercent + " of final replacement cost</div>";
                }
            }
            catch (Exception ex)
            {
                //string stemperr = ex.Message;
                litITV.Text = "*Error*";
            }
            

            /* Replacement Cost Analysis */
            //decimal? coverageAOut = GetCoverageA();
            //this.litBuildingCoverage.Text = Case.CaseField.CoverageAIn.HasValue ? Math.Ceiling(Case.CaseField.CoverageAIn.Value).ToString("C0") : "N/A";
            //this.litReplacmenetCost.Text = coverageAOut.HasValue ? Math.Ceiling(coverageAOut.Value).ToString("C0") : "N/A";
            //this.litITV.Text = GetITV(Case.CaseField.CoverageAIn, coverageAOut);// Coverage A In / Coverage A Out
        }


        private decimal? GetCoverageAOut()
        {
            // NOTE: CoverageAOut and CoverageOutA
            var dt = SQL.ExecuteSql("SELECT CoverageAOut FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if (dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageAOut"] != null)
            {
                string sTemp = RemoveChars(tr["CoverageAOut"].ToString());
                decimal covA;
                if (Decimal.TryParse(sTemp, out covA))
                    return covA;
            }

            return null;
        }

        private decimal? GetCoverageOutA()
        {
            // NOTE: CoverageAOut and CoverageOutA
            //CoverageOutA - MSB?
            var dt = SQL.ExecuteSql("SELECT CoverageOutA FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if (dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageOutA"] != null)
            {
                string sTemp = RemoveChars(tr["CoverageOutA"].ToString());
                decimal covA;
                if (Decimal.TryParse(sTemp, out covA))
                    return covA;
            }

            return null;
        }

        private static string RemoveChars(string sInput)
        {
            return sInput.Replace("$", "");
        }
        //private decimal? GetCoverageA()
        //{
        //    var dt = SQL.ExecuteSql("SELECT CoverageAOut FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
        //    if (dt.Tables.Count == 0)
        //        return null;
        //    if(dt.Tables[0].Rows.Count == 0)
        //        return null;
        //    var tr = dt.Tables[0].Rows[0];
        //    if (tr["CoverageAOut"] != null)
        //    {
        //        decimal covA;
        //        if(Decimal.TryParse(tr["CoverageAOut"].ToString(), out covA))
        //            return covA;
        //    }

        //    return null;
        //}

        private string GetITV(decimal? coverageAIn, decimal? coverageAOut)
        {
            var cAOut = coverageAOut ?? 0;
            var cAIn = coverageAIn ?? 0;
            if(cAOut != 0)
                return Math.Round((cAIn / cAOut) * 100, 0) + " %";
            return "N/A";
        }

        private string GetValueOrDefault(string ColName, bool liabilityHas=true)
        {
            if (liabilityHas)
                return GetValueOrDefault(ColName, ColName, ColName, ColName, ColName);
            else
                return GetValueOrDefault(ColName, ColName, ColName, null, ColName);
        }

        private List<Guid> NoFormDataFound = new List<Guid>();

        private DataRow GetFormData(Guid FormID)
        {
            if (NoFormDataFound.Contains(FormID)) //so we don't have to hit the db on every request if we don't find a form on the Case the first time just skip.
                return null;

            DataRow dr = null;
            var caseForm = Case.CaseForms.Where(cf => cf.FormID == FormID && cf.Deleted == false).FirstOrDefault();
            if (caseForm != null)
            {
                string table = caseForm.Form.formTableName;
                var sql = "SELECT * FROM Form." + table + " WHERE FormID = @formInstanceID";
                    //f0e982e2-8495-4ac7-8dfd-dd54f129c8d3

                var ds = SQL.ExecuteSql(sql, "@formInstanceID", caseForm.FormInstanceID);
                if (ds.Tables.Count == 0)
                {
                    NoFormDataFound.Add(FormID);
                    return null;
                }
                var t = ds.Tables[0];
                if (t.Rows.Count > 0)
                    dr = t.Rows[0];
                else
                    NoFormDataFound.Add(FormID);
            }
            else
            {
                NoFormDataFound.Add(FormID);
            }

            return dr;
        }

        private string commaDelimitIfList(string list)
        {
            if (list.IsNotNullOrEmpty() && list.Contains('|'))//probably a list
                return string.Join(", ", list.Split('|').ToArray()).Trim(',',' ');
            return list;
        }

        private string GetColumnValue(DataRow row, string column)
        {
            if (row != null
                && row[column] != null
                && row[column].ToString().IsNotNullOrEmpty())
                return commaDelimitIfList(row[column].ToString());
            return null;
        }


        private DataRow BriefExteriorWOE2VAL515Row = null;
        private DataRow BriefExteriorWOE2VAL562Row = null;
        private DataRow BriefExteriorWOE2VAL11117Row = null;
        private DataRow BriefLiabilityWOE2Val12Row = null;
        private DataRow FloodRow = null;

        private string GetValueOrDefault(string BriefExteriorWOE2VAL515Col, string BriefExteriorWOE2VAL562Col, string BriefExteriorWOE2VAL11117Col, string BriefLiabilityWOE2Val12Col, string floodCol)
        {

            //Residential Exterior
            if (BriefExteriorWOE2VAL515Col != null)
            {
                var WOE2VAL515Val = "";

                //SCU-Orlando Residential Exterior v11.13.18 
                if (BriefExteriorWOE2VAL515Row == null)
                    BriefExteriorWOE2VAL515Row = GetFormData(new Guid("0f3669db-da6c-4a17-80b1-f64100c685c2"));

                WOE2VAL515Val = GetColumnValue(BriefExteriorWOE2VAL515Row, BriefExteriorWOE2VAL515Col);
                if (WOE2VAL515Val != null)
                    return WOE2VAL515Val;

                //SCU-Orlando Residential Exterior v8.20.14
                if (BriefExteriorWOE2VAL515Row == null)
                    BriefExteriorWOE2VAL515Row = GetFormData(new Guid("93a2df4e-8a23-4759-a2f2-26f1f314d6fd"));

                WOE2VAL515Val = GetColumnValue(BriefExteriorWOE2VAL515Row, BriefExteriorWOE2VAL515Col);
                if (WOE2VAL515Val != null)
                    return WOE2VAL515Val;
            }

            //Residential Exterior w/Valuation
            if (BriefExteriorWOE2VAL562Col != null)
            {
                var WOE2VAL562Val = "";

                //SCU-Orlando Residential Exterior w/Valuation v11.13.18
                if (BriefExteriorWOE2VAL562Row == null)
                    BriefExteriorWOE2VAL562Row = GetFormData(new Guid("db6ffb35-0366-4999-92bb-9ad4f7af43cf"));

                WOE2VAL562Val = GetColumnValue(BriefExteriorWOE2VAL562Row, BriefExteriorWOE2VAL562Col);

                if (WOE2VAL562Val != null)
                    return WOE2VAL562Val;

                //SCU-Orlando Residential Exterior w/Valuation 8.20.14
                if (BriefExteriorWOE2VAL562Row == null)
                    BriefExteriorWOE2VAL562Row = GetFormData(new Guid("01a1f7fd-752c-45dc-9643-644556282ffe"));

                WOE2VAL562Val = GetColumnValue(BriefExteriorWOE2VAL562Row, BriefExteriorWOE2VAL562Col);

                if (WOE2VAL562Val != null)
                    return WOE2VAL562Val;

            }

            //Residential Interior & Exterior
            if (BriefExteriorWOE2VAL11117Col != null)
            {
                //SCU-Orlando Residential Interior & Exterior v8.20.14
                if (BriefExteriorWOE2VAL11117Row == null)
                    BriefExteriorWOE2VAL11117Row = GetFormData(new Guid("cd4378b2-3572-48bb-8e7c-00d61c768f9d"));


                var WOE2VAL11117Val = GetColumnValue(BriefExteriorWOE2VAL11117Row, BriefExteriorWOE2VAL11117Col);

                if (WOE2VAL11117Val != null)
                    return WOE2VAL11117Val;
            }

            //Residential Liability
            if (BriefLiabilityWOE2Val12Col != null)
            {
                var BriefLiabilityWOE2Val12Val = "";

                // SCU-Orlando Residential Liability v11.13.18
                if (BriefLiabilityWOE2Val12Row == null)
                    BriefLiabilityWOE2Val12Row = GetFormData(new Guid("a5139d87-8e07-4bed-bd60-6f0fa81e4faf"));

                BriefLiabilityWOE2Val12Val = GetColumnValue(BriefLiabilityWOE2Val12Row, BriefLiabilityWOE2Val12Col);

                if (BriefLiabilityWOE2Val12Val != null)
                    return BriefLiabilityWOE2Val12Val;

                // SCU - Orlando Residential Liability
                if (BriefLiabilityWOE2Val12Row == null)
                    BriefLiabilityWOE2Val12Row = GetFormData(new Guid("c9b36bbd-46f3-4aa0-af79-5e527c3abd91"));

                BriefLiabilityWOE2Val12Val = GetColumnValue(BriefLiabilityWOE2Val12Row, BriefLiabilityWOE2Val12Col);

                if (BriefLiabilityWOE2Val12Val != null)
                    return BriefLiabilityWOE2Val12Val;

            }

            //Flood
            if (floodCol != null)
            {
                if (floodCol == "ProtectionClass") return "N/A";

                if (FloodRow == null)
                    FloodRow = GetFormData(new Guid("5273bb3b-28eb-4504-96d9-587ba9e33e20"));

                var FloodVal = GetColumnValue(FloodRow, floodCol);

                if (FloodVal != null)
                    return FloodVal;
            }

            return "N/A";
        }


        private void BindPhotos()
        {
            if (Case.Photos.Count == 0)
                this.image.ImageUrl = "~/images/custom/housenoimageavailable.jpg";
            else
            {
                var photoID = Case.Photos.OrderBy(p => p.DisplayOrder).Take(1).Select(p => p.PhotoID.ToString()).FirstOrDefault();
                this.image.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

                //Debug
                //this.image.ImageUrl = "https://ecommerce3.sibfla.com/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

            }
        }

        public override void LoadData()
        {

        }

        public override void ClearFormData(FSMData.Form form)
        {

        }
    }
}