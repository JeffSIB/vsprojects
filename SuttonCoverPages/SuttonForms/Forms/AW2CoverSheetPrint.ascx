﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AW2CoverSheetPrint.ascx.cs"
    Inherits="Utilant.Forms.Forms.AW2CoverSheetPrint" %>


<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }

    .main-page {
        width: 7.5in;
        min-height: 10.3in;
        margin: auto;
        padding: 10px 10px 10px 10px;
        font-family: Arial;
        font-size: 14px;
        padding-left: 60px;
        padding-right: 60px;
    }

    .headerRow {
        background-color: #81bd01;
        color: White;
        font-weight: bold;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }

    .logo {
        float: left;
    }

    .clear {
        clear: both;
    }

    .companyInfo {
        float: right;
        text-align: right;
        width: 30%;
        color: #006cc5;
        font-weight: bold;
    }

    .label {
        font-weight: bold;
    }

    .lastTableRow td {
        padding-bottom: 15px;
    }
</style>

<%--START OF PAGE--%>
<div class="main-page">
    <%--PAGE HEADER AND CASE INFORMATION--%>
    <div class="page-header">
        <div>
            <img id="imgHeader" class="logo" alt="[Company Logo]" src='<%= Page.ResolveClientUrl("~/images/Custom/mainlogo.png") %>' />

            <%--            // ** DEBUG **--%>
            <%--                        <img id="imgHeader" class="logo" alt="[Company Logo]" src='https://ecommerce3.sibfla.com/App_Themes/images/mainLogoSmall.png' />--%>

            <div class="companyInfo">
                <table>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyAddress" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyCityStateZip" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Phone:
                            <asp:Literal ID="litPhone" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Fax:
                            <asp:Literal ID="litFax" runat="server" /></td>
                    </tr>
                </table>

            </div>
            <div class="clear" />
        </div>
        <div>
            <table style="border-spacing: 0">
                <tr class="lastTableRow">
                    <td class="label" colspan="2">Completed Inspection Report For:</td>
                    <td colspan="2">
                        <asp:Literal ID="litCompanyCompletedForName" runat="server" /></td>
                </tr>
                <tr>
                    <td class="label">Insured Name:
                    </td>
                    <td>
                        <asp:Literal ID="litInsuredName" runat="server" />
                    </td>
                    <td class="label">Policy Number:
                    </td>
                    <td>
                        <asp:Literal ID="litPolicyNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Insured Address:
                    </td>
                    <td>
                        <asp:Literal ID="litLocationAddress" runat="server" />
                    </td>
                    <td class="label">Inspection Date:
                    </td>
                    <td>
                        <asp:Literal ID="litInspectionDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">City,State,Zip:
                    </td>
                    <td>
                        <asp:Literal ID="litLocationCityStateZip" runat="server" />
                    </td>
                    <td class="label">Agent:
                    </td>
                    <td>
                        <asp:Literal ID="litAgent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td class="label">Agent Phone:
                    </td>
                    <td>
                        <asp:Literal ID="litAgentPhone" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                    <td class="label">Underwriter:
                    </td>
                    <td>
                        <asp:Literal ID="litUnderwriter" runat="server" />
                    </td>
                </tr>
                <tr class="headerRow">
                    <td colspan="2" class="label">Occupancy
                    </td>
                    <td colspan="2" class="label">
                        <asp:Literal ID="litConstructionHdr" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Insured Property is:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litOccupied" runat="server" />
                    </td>
                    <td class="label">
                        <asp:Literal ID="litConstructionLbl" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="litConstruction" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">Square footage:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litSqFt" runat="server" />
                    </td>
                    <td class="label">
                        <asp:Literal ID="litYearBuiltLbl" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="litYearBuilt" runat="server" />
                    </td>

                </tr>
                <tr class="headerRow">
                    <td colspan="2" class="label">Fire Protection / Security
                    </td>
                    <td colspan="2" class="label">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Literal ID="litLblProtClass" runat="server" />
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litProtectionClass" runat="server" />&nbsp;&nbsp;<asp:Literal ID="litPC10" runat="server" />

                    </td>
                    <td class="label">Smoke Detectors:
                    </td>
                    <td>
                        <asp:Literal ID="litSmokeDet" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">Burglar alarm:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litBurgAlarm" runat="server" />
                    </td>
                    <td class="label">
                        <asp:Literal ID="litLblAES" runat="server" />

                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litAES" runat="server" />
                    </td>
                </tr>
            </table>
            <table id="tblUpdates" style="border-spacing: 0" runat="server">
                <tr class="headerRow">
                    <td style="width:20%" class="label">Updates
                    </td>
                    <td style="width:80%" class="label">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="label">Roof age:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litRoofUpdate" runat="server" />
                    </td>
                    <td class="label">&nbsp;</td>
                    <td class="label">&nbsp;</td>
                </tr>
                <tr>
                    <td class="label">Wiring updated:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litWiringUpdate" runat="server" />&nbsp;&nbsp;
                        <asp:Literal ID="litWiringYr" runat="server" />
                    </td>
                    <td colspan="2" class="label">&nbsp;</td>
                </tr>
                <tr>
                    <td class="label">Plumbing updated:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litPlumbingUpdate" runat="server" />&nbsp;&nbsp;
                        <asp:Literal ID="litPlumbingYr" runat="server" />
                    </td>
                    <td colspan="2" class="label">&nbsp;</td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">HVAC updated:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litHVACUpdate" runat="server" />&nbsp;&nbsp;
                        <asp:Literal ID="litHVACYr" runat="server" />
                    </td>
                    <td colspan="2" class="label">&nbsp;</td>
                </tr>

            </table>
            <div>
                <div style="width: 45%; float: left">
                    <table>
                        <tr class="headerRow">
                            <td class="label">Hazards / Exposures Noted
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Literal ID="litHazards" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="float: left; width: 45%; margin-left: 15px;">
                    <asp:Image ID="image" Width="380px" runat="server" />
                </div>
                <div class="clear" />
            </div>
        </div>
    </div>
</div>
