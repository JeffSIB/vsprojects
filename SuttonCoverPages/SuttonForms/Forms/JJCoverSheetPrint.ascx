﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JJCoverSheetPrint.ascx.cs"
    Inherits="Utilant.Forms.Forms.JJCoverSheetPrint" %>


<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }

    .main-page {
        width: 7.5in;
        min-height: 10.3in;
        margin: auto;
        padding: 10px 10px 10px 10px;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        padding-left: 60px;
        padding-right: 60px;
    }

    .headerRow {
        background-color: #81bd01;
        color: White;
        font-weight: bold;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }

    .tableHeaderRow {
        background-color: #888888;
        color: White;
        font-weight: bold;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }

    .ccTable {
        border: 2px solid #808080;
        padding-top: 15px;
    }

    .ccTitle {
        border-bottom: 3px solid #000000;
        font-family: Calibri;
        color: black;
        font-weight: normal;
        font-size: 28pt;
    }

    .ccTableHeaderRow {
        background-color: #81bd01;
        border: 2px solid #808080;
        color: black;
        font-weight: bold;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
        text-align: center;
    }

    .ccTableDataRow {
        background-color: white;
        border: 2px solid #808080;
        width: 90px;
        text-align: center;
        color: black;
        font-weight: normal;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }

    .ccTableDataRowRight {
        background-color: white;
        border: 2px solid #808080;
        width: 90px;
        text-align: right;
        color: black;
        font-weight: normal;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }

    .ccTableDataRowLeft {
        background-color: white;
        border: 2px solid #808080;
        width: 90px;
        text-align: left;
        color: black;
        font-weight: normal;
        font-size: 10pt;
        padding: 6px 2px 6px 6px;
    }


    .logo {
        float: left;
    }

    .clear {
        clear: both;
    }

    .companyInfo {
        float: right;
        text-align: right;
        width: 30%;
        color: #006cc5;
        font-weight: bold;
    }

    .label {
        font-weight: bold;
    }

    .lastTableRow td {
        padding-bottom: 15px;
    }
</style>

<%--START OF PAGE--%>
<div class="main-page">
    <%--PAGE HEADER AND CASE INFORMATION--%>
    <div class="page-header">
        <div>
                        <img id="imgHeader" class="logo" alt="[Company Logo]" src='<%= Page.ResolveClientUrl("~/images/Custom/mainlogo.png") %>' />

            <%--            // ** DEBUG **--%>
<%--            <img id="imgHeader" class="logo" alt="[Company Logo]" src='https://ecommerce3.sibfla.com/App_Themes/images/mainLogoSmall.png' />--%>

            <div class="companyInfo">
                <table>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyAddress" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyCityStateZip" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Phone:
                            <asp:Literal ID="litPhone" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Fax:
                            <asp:Literal ID="litFax" runat="server" /></td>
                    </tr>
                </table>

            </div>
            <div class="clear" />
        </div>
        <div>
            <table style="border-spacing: 0;">
                <tr class="lastTableRow">
                    <td class="label" colspan="2">Completed Inspection Report For:</td>
                    <td colspan="2">
                        <asp:Literal ID="litCompanyCompletedForName" runat="server" /></td>
                </tr>
                <tr>
                    <td class="label">Insured Name:
                    </td>
                    <td>
                        <asp:Literal ID="litInsuredName" runat="server" />
                    </td>
                    <td class="label">Policy Number:
                    </td>
                    <td>
                        <asp:Literal ID="litPolicyNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Insured Address:
                    </td>
                    <td>
                        <asp:Literal ID="litInsuredAddress" runat="server" />
                    </td>
                    <td class="label">Inspection Date:
                    </td>
                    <td>
                        <asp:Literal ID="litInspectionDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">City,State,Zip:
                    </td>
                    <td>
                        <asp:Literal ID="litCityStateZip" runat="server" />
                    </td>
                    <td class="label">Agent:
                    </td>
                    <td>
                        <asp:Literal ID="litAgent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Location Address:
                    </td>
                    <td>
                        <asp:Literal ID="litLocationAddress" runat="server" />
                    </td>
                    <td class="label">Agent Phone:
                    </td>
                    <td>
                        <asp:Literal ID="litAgentPhone" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">City, State, Zip
                    </td>
                    <td>
                        <asp:Literal ID="litLocationCityStateZip" runat="server" />
                    </td>
                    <td class="label">Underwriter:
                    </td>
                    <td>
                        <asp:Literal ID="litUnderwriter" runat="server" />
                    </td>
                </tr>
            </table>

            <div>
                <div style="width: 45%; float: left">
                    <table>
                        <tr class="headerRow">
                            <td class="label">Hazards / Exposures Noted
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Literal ID="litHazards" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="float: left; width: 45%; margin-left: 15px;">
                    <asp:Image ID="image" Width="380px" runat="server" />
                </div>
                <div class="clear" />
            </div>
        </div>
        <p style="page-break-after: always;">&nbsp;</p>
<%--        <p>
            <asp:Literal ID="litError" runat="server" />
        </p>--%>
        <table style="width: 100%">
            <tr>
                <td class="ccTitle">Inspection Checklist</td>
            </tr>
        </table>
        <br />
        <table style="width: 100%" class="ccTable">
            <tr>
                <td class="ccTableDataRowLeft" style="width: 50%;">Policy Number:&nbsp;                       
                    <asp:Literal ID="litPolicyNum2" runat="server" />
                </td>
                <td class="ccTableDataRowLeft" style="width: 50%;">Policy Eff Date:&nbsp;
                    <asp:Literal ID="litEffDate" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRowLeft">Insured Name:&nbsp;                       
                    <asp:Literal ID="litInsured2" runat="server" />
                </td>
                <td class="ccTableDataRowLeft">Underwriter/Assistant:&nbsp;   
                    <asp:Literal ID="litUW2" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="ccTableDataRowLeft" >Type of Inspection Completed:&nbsp;                       
                    <asp:Literal ID="litInspType" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <table style="width: 100%" class="ccTable">

            <tr>

                <td class="ccTableHeaderRow" style="width: 30%;">Question
                </td>
                <td class="ccTableHeaderRow" style="width: 70%;">Observation
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Person interviewed:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPerInterview" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Person interviewed is:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPerInterviewIs" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Year Built:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litYearBlt" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Occupancy Type:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litOccupancy" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Construction Type:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litConst" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Square Footage:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litSqFt" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Wall Finish Type:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litWallFin" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Foundation Type:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litFoundation" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Number of Stories:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litStories" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <table style="width: 100%" class="ccTable">

            <tr>
                <td class="ccTableDataRow" style="width: 30%;">Roof Shape:
                </td>
                <td class="ccTableDataRow" style="width: 70%;">
                    <asp:Literal ID="litRoofShape" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="ccTableDataRow">Roof Material:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofMat" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Roof Updated:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litRoofYear" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Plumbing Updated:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPlumbYear" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Electrical Updated:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litElectYear" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">HVAC Updated:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litHVACYear" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <table style="width: 100%" class="ccTable">
            <tr>
                <td class="ccTableDataRow" style="width: 30%;">Protection class:
                </td>
                <td class="ccTableDataRow" style="width: 70%;">
                    <asp:Literal ID="litProtClass" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Water source:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litWaterSource" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Property Accessibility:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPropAccess" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Responding Fire Dept:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litFireDept" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Distance To Fire Dept:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litFireDistance" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <table style="width: 100%" class="ccTable">
            <tr>
                <td class="ccTableDataRow" style="width: 30%;">Storm Shutters:
                </td>
                <td class="ccTableDataRow" style="width: 70%;">
                    <asp:Literal ID="litStormProt" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Hurricane Straps:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litHurrStrap" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Gated Community:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litGated" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Burglar Alarm System:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litBurgAlarm" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Fire Alarm System:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litFireAlarm" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Pool:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPool" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Diving Board:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litDiving" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Slide:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litSlide" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Approved Fence:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litPoolFence" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Trampoline:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litTramp" runat="server" />
                </td>
            </tr>
        </table>
<%--        <p style="page-break-after: always;">&nbsp;</p>--%>
        <table style="width: 100%" class="ccTable">
            <tr>
                <td class="ccTableDataRow" style="width: 30%;">Dogs Present:
                </td>
                <td class="ccTableDataRow" style="width: 70%;">
                    <asp:Literal ID="litDogs" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow">Breed:
                </td>
                <td class="ccTableDataRow">
                    <asp:Literal ID="litDogBreed" runat="server" />
                </td>
            </tr>
        </table>
       <br />
        <table style="width: 100%" class="ccTable">
            <tr>
                <td class="ccTableDataRow" style="width: 30%;">Other Liability Hazards:
                </td>
                <td class="ccTableDataRow" style="width: 70%;">
                    <asp:Literal ID="litLiabHaz" runat="server" />
                </td>
            </tr>
        </table>
       <br />
        <table style="width: 100%" class="ccTable">
            <tr>
                <td class="ccTableDataRow" style="width: 34%;">Coverage A In
                </td>
                <td class="ccTableDataRow" style="width: 33%;">Coverage A Out
                </td>
                <td class="ccTableDataRow" style="width: 33%;">Variance
                </td>
            </tr>
            <tr>
                <td class="ccTableDataRow" style="width: 34%;">
                    <asp:Literal ID="litCovAIn" runat="server" />
                </td>
                <td class="ccTableDataRow" style="width: 33%;">
                    <asp:Literal ID="litCovAOut" runat="server" />
                </td>
                <td class="ccTableDataRow" style="width: 33%;">
                    <asp:Literal ID="litCovAVar" runat="server" />
                </td>
            </tr>
        </table>

    </div>
</div>
