﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CoverSheetRecsPrint.ascx.cs"
    Inherits="Utilant.Forms.Forms.CoverSheetRecsPrint" %>
<style>
    .head {
        font-weight: bold;
    }

    .info, .fieldsAndHazards {
        width: 100%;
    }

        .info td {
            font-size: 12px;
            padding: 5px;
            vertical-align: top;
        }

        .fieldsAndHazards td {
            padding: 2px;
            vertical-align: top;
        }

        .fieldsAndHazards th {
            font-size: 12px;
            padding: 10px;
            vertical-align: top;
            border-bottom: 1px solid black;
            text-align: left;
        }

    .coveragePercentage {
        background-color: #f5f5f5;
        color: black;
        border: 1px solid #ccc;
        padding: 3px;
        margin: 2px 0px;
        text-align: center;
    }
</style>
<div style="width: 7.5in; border: 0px solid #FFF; margin: auto;">
    <%= LC360Web.Constants.GetCustomerHeader(Case, null)%>
    <asp:Literal ID="litCoveragePercent" runat="server"></asp:Literal><br />
    <br />
    ---
	<asp:Literal ID="litPreUnderwriterOptions" runat="server"></asp:Literal>
    <br />
    <div style="text-align: center;">
        <asp:Image ID="imgMain" Style="border: 1px solid #ccc;" runat="server" /><asp:Literal
            ID="litPhotoTitle" runat="server"></asp:Literal>
    </div>
    <br />
    <br />
    <table class="fieldsAndHazards">
        <tr>
            <th>
                <div class="coverSectionHeader <%= (Case.HazardScoreTotal ?? 0) > 0? "redText" : "" %>">
                    <%= (Case.HazardScoreTotal ?? 0) > 0 ? "<span style='float:right'>Total Score:&nbsp;" + Case.HazardScoreTotal.Value + "&nbsp;</span>" : "" %>
                    Adverse Conditions
                </div>
            </th>
            <td></td>
            <th>Important Fields
            </th>
        </tr>
        <tr>
            <td style="width: 45%">
                <asp:GridView ID="gvHazards" runat="server" AutoGenerateColumns="false" ShowHeader="false"
                    Width="100%" GridLines="None" EnableTheming="false">
                    <EmptyDataTemplate>
                        No conditions to report
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="Description" />
                        <asp:BoundField DataField="Score" />
                        <asp:ImageField DataImageUrlField="Image" />
                    </Columns>
                </asp:GridView>
            </td>
            <td></td>
            <td style="width: 45%">
                <asp:GridView ID="gvImportantFields" GridLines="None" EnableTheming="false" ShowHeader="false"
                    runat="server">
                    <EmptyDataTemplate>
                        No fields to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <th>Recommendations
            </th>
            <td></td>
        </tr>

        <tr>
            <td style="width: 45%">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 90%">General</td>
                        <td style="width: 10%"><%= (TotalNumGenRecs ?? 0) > 0 ? "<span style='color: Black;'>&nbsp;" + TotalNumGenRecs + "</span>" : "0"%></td>
                    </tr>
                    <tr>
                        <td>Suggested</td>
                        <td><%= (TotalNumSugRecs ?? 0) > 0 ? "<span style='color: Black;'>&nbsp;" + TotalNumSugRecs + "</span>" : "0"%></td>
                    </tr>

                </table>
            </td>
            <td></td>
            <td style="width: 45%">&nbsp;</td>
        </tr>

    </table>

    <asp:PlaceHolder ID="phDisclaimer" runat="server"></asp:PlaceHolder>
</div>
