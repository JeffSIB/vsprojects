﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrchidConnectCoverSheetPrint.ascx.cs"
    Inherits="Utilant.Forms.Forms.OrchidConnectCoverSheetPrint" %>
<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }

    .main-page {
        width: 7.5in;
        min-height: 10.3in;
        margin: auto;
        padding: 10px 10px 10px 10px;
        font-size: 14px;
        padding-left: 60px;
        padding-right: 60px;
    }

    .headerRow {
        background-color: #13684E;
        color: White;
        font-weight: bold;
        font-size: 10pt;
        padding: 2px 2px 2px 6px;
    }

    .logo {
        float: left;
    }

    .clear {
        clear: both;
    }

    .companyInfo {
        float: right;
        text-align: right;
        width: 30%;
        color: #13684E;
        font-weight: bold;
    }

    .label {
        font-weight: bold;
        width: 20%;
    }

    .value {
        font-weight: normal;
        width: 30%;
    }


    .lastTableRow td {
        padding-bottom: 15px;
    }
</style>
<%--START OF PAGE--%>
<div class="main-page">
    <%--PAGE HEADER AND CASE INFORMATION--%>
    <div class="page-header">
        <div>
            <img id="imgHeader" class="logo" alt="[Company Logo]" src='<%= Page.ResolveClientUrl("~/images/Custom/mainlogo.png") %>' />
            <div class="companyInfo">
                <table>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyAddress" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyCityStateZip" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>Phone:
                            <asp:Literal ID="litPhone" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>Fax:
                            <asp:Literal ID="litFax" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clear" />
        </div>
        <div>
            <table style="border-spacing: 0">
                <tr>
                    <td class="label">&nbsp;
                    </td>
                    <td class="value">&nbsp;
                    </td>
                    <td class="label">&nbsp;
                    </td>
                    <td class="value">&nbsp;
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label" colspan="2">Completed Inspection Report For:
                    </td>
                    <td colspan="2">
                        <asp:Literal ID="litCompanyCompletedForName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Insured Name:
                    </td>
                    <td>
                        <asp:Literal ID="litInsuredName" runat="server" />
                    </td>
                    <td class="label">Policy Number:
                    </td>
                    <td>
                        <asp:Literal ID="litPolicyNumber" runat="server" />
                    </td>
                </tr>
                <tr style="padding-bottom: 5px;">
                    <td class="label">Location Address:
                    </td>
                    <td colspan="3">
                        <asp:Literal ID="litLocationAddress" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Underwriter:
                    </td>
                    <td>
                        <asp:Literal ID="litUnderwriter" runat="server" />
                    </td>
                    <td class="label">Inspection Date:
                    </td>
                    <td>
                        <asp:Literal ID="litInspectionDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Agent:
                    </td>
                    <td>
                        <asp:Literal ID="litAgent" runat="server" />
                    </td>
                    <td class="label">Agent Phone:
                    </td>
                    <td>
                        <asp:Literal ID="litAgentPhone" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">Inspector:
                    </td>
                    <td>
                        <asp:Literal ID="litInspector" runat="server" />
                    </td>
                    <td class="label">&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>

                <tr class="headerRow">
                    <td colspan="4" class="label">Validation of information provided
                    </td>
                </tr>
            </table>
            <div>
                <div>
                    <table style="border-spacing: 0">
                        <tr>
                            <td align="left" style="width: 15%;">&nbsp;
                            </td>
                            <td align="left" style="width: 23%;">&nbsp;
                            </td>
                            <td class="label" align="center" style="width: 5%;">Y
                            </td>
                            <td class="label" align="center" style="width: 5%;">N
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">&nbsp;
                            </td>
                            <td align="left" style="width: 23%;">&nbsp;
                            </td>
                            <td class="label" align="center" style="width: 5%;">Y
                            </td>
                            <td class="label" align="center" style="width: 5%;">N
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Occupancy
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litOccupancy" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbOccupancyY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbOccupancyN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;"># of stories
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litStories" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbStoriesY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbStoriesN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Dwelling Type
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litDwellingType" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbDwellingTypeY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbDwellingTypeN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">Burglar alarm
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litBurgAlarm" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbBurgAlarmY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbBurgAlarmN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Year Built
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litYearBuilt" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbYearBuiltY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbYearBuiltN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">Fire alarm
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litFireAlarm" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbFireAlarmY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbFireAlarmN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Roof type
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litRoofType" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbRoofTypeY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbRoofTypeN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">Roof geometry
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litRoofGeo" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbRoofGeoY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbRoofGeoN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Roof year
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litRoofYear" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbRoofYearY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbRoofYearN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">Wood shake
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litWoodShake" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbWoodShakeY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbWoodShakeN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Dry Vit
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litDryVit" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbDryVitY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbDryVitN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">EIFS
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litEIFS" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbEIFSY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbEIFSN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Masonite
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litMasonite" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbMasoniteY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbMasoniteN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">Other siding
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litOtherSide" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbOtherSideY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbOtherSideN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Pool fenced
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litPoolFence" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPoolFenceY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPoolFenceN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">Pool diving bd
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litDivingBoard" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbDivingBoardY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbDivingBoardN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Pool present
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litPoolPresent" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPoolPresentY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPoolPresentN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">Pool above grnd
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litPoolAbove" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPoolAboveY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPoolAboveN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Pool slide
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litPoolSlide" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPoolSlideY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPoolSlideN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">Pool screen encl
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litPoolEncl" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPoolEnclY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPoolEnclN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Windstorm prot
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litWindStorm" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbWindStormY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbWindStormN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">Protection class
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litProtClass" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbProtClassY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbProtClassN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Dogs
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litPets" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPetsY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbPetsN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">Other pets
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litOtherPets" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbOtherPetsY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbOtherPetsN" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 15%;">Distance to Coast
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litCoast" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbCoastY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbCoastN" runat="server" />
                            </td>
                            <td align="left" style="width: 4%;">&nbsp;
                            </td>
                            <td align="left" style="width: 15%;">Exterior wall type
                            </td>
                            <td align="left" style="width: 23%;">
                                <asp:Literal ID="litExtWallType" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbExtWallTypeY" runat="server" />
                            </td>
                            <td class="label" align="center" style="width: 5%;">
                                <asp:RadioButton ID="rbExtWallTypeN" runat="server" />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="9">&nbsp;
                            </td>
                        </tr>
                    </table>
                    <div id="divUpdates" runat="server">
                        <table style="border-spacing: 0">
                            <tr class="headerRow">
                                <td class="label">Updates
                                </td>
                            </tr>
                        </table>
                        <table style="border-spacing: 0">

                            <tr>
                                <td align="left" style="width: 15%;">&nbsp;
                                </td>
                                <td align="left" style="width: 23%;">&nbsp;
                                </td>
                                <td class="label" align="center" style="width: 5%;">Y
                                </td>
                                <td class="label" align="center" style="width: 5%;">N
                                </td>
                                <td align="left" style="width: 4%;">&nbsp;
                                </td>
                                <td align="left" style="width: 15%;">&nbsp;
                                </td>
                                <td align="left" style="width: 23%;">&nbsp;
                                </td>
                                <td class="label" align="center" style="width: 5%;">Y
                                </td>
                                <td class="label" align="center" style="width: 5%;">N
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 15%;">Updates
                                </td>
                                <td align="left" style="width: 23%;">
                                    <asp:Literal ID="litUpdates" runat="server" />
                                </td>
                                <td class="label" align="center" style="width: 5%;">
                                    <asp:RadioButton ID="rbUpdatesY" runat="server" />
                                </td>
                                <td class="label" align="center" style="width: 5%;">
                                    <asp:RadioButton ID="rbUpdatesN" runat="server" />
                                </td>
                                <td align="left" style="width: 4%;">&nbsp;
                                </td>
                                <td align="left" style="width: 15%;">Plumbing year
                                </td>
                                <td align="left" style="width: 23%;">
                                    <asp:Literal ID="litPlumbYear" runat="server" />
                                </td>
                                <td class="label" align="center" style="width: 5%;">
                                    <asp:RadioButton ID="rbPlumbYearY" runat="server" />
                                </td>
                                <td class="label" align="center" style="width: 5%;">
                                    <asp:RadioButton ID="rbPlumbYearN" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 15%;">HVAC year
                                </td>
                                <td align="left" style="width: 23%;">
                                    <asp:Literal ID="litHVACYear" runat="server" />
                                </td>
                                <td class="label" align="center" style="width: 5%;">
                                    <asp:RadioButton ID="rbHVACYearY" runat="server" />
                                </td>
                                <td class="label" align="center" style="width: 5%;">
                                    <asp:RadioButton ID="rbHVACYearN" runat="server" />
                                </td>
                                <td align="left" style="width: 4%;">&nbsp;
                                </td>
                                <td align="left" style="width: 15%;">Wiring year
                                </td>
                                <td align="left" style="width: 23%;">
                                    <asp:Literal ID="litWiringYear" runat="server" />
                                </td>
                                <td class="label" align="center" style="width: 5%;">
                                    <asp:RadioButton ID="rbWiringYearY" runat="server" />
                                </td>
                                <td class="label" align="center" style="width: 5%;">
                                    <asp:RadioButton ID="rbWiringYearN" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9">&nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <asp:Literal ID="litError" runat="server" />
                        <div style="width: 45%; float: left">
                            <table>
                                <tr class="headerRow">
                                    <td class="label">Hazards / Exposures Noted
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal ID="litHazards" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                                <tr class="headerRow">
                                    <td class="label">Recommendations
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="Bold"># of Recommendations: </span>
                                        <%= (TotalNumRecs ?? 0) > 0 ? "<span style='color: Red;'>&nbsp;" + TotalNumRecs + "</span>" : "0"%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="float: left; width: 45%; margin-left: 15px;">
                            <asp:Image ID="image" Width="380px" runat="server" />
                        </div>
                        <div class="clear" />
                    </div>
                </div>
            </div>
        </div>
    </div>
