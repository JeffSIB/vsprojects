﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CrumpCoverSheetPrint.ascx.cs"
    Inherits="Utilant.Forms.Forms.CrumpCoverSheetPrint" %>


<style type="text/css">
    table
    {
        width: 100%;
        border-collapse: collapse;
    }

    .main-page
    {
        width: 7.5in;
        min-height: 10.3in;
        margin: auto;
        padding: 10px 10px 10px 10px;
        font-size: 14px;
        padding-left:60px;
        padding-right:60px;
    }

    .headerRow
    {
        background-color: #13684E;
        color: White;
        font-weight: bold;
        font-size: 10pt;
        padding: 2px 2px 2px 6px;
    }

    .logo
    {
        float: left;
    }

    .clear
    {
        clear: both;
    }

    .companyInfo
    {
        float: right;
        text-align: right;
        width: 30%;
        color: #13684E;
        font-weight:bold;
    }

    .label
    {
        font-weight: bold;
    }

    .lastTableRow td
    {
        padding-bottom: 15px;
    }
</style>

<%--START OF PAGE--%>
<div class="main-page">
    <%--PAGE HEADER AND CASE INFORMATION--%>
    <div class="page-header">
        <div>
            <img id="imgHeader" class="logo" alt="[Company Logo]" src='<%= Page.ResolveClientUrl("~/images/Custom/mainlogo.png") %>' />

<%--            <img id="imgHeader" class="logo" alt="[Company Logo]" src='https://ecommerce3.sibfla.com/App_Themes/images/mainLogo.png' />--%>

            <div class="companyInfo">
                <table>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyAddress" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="litCompanyCityStateZip" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Phone:
                            <asp:Literal ID="litPhone" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Fax:
                            <asp:Literal ID="litFax" runat="server" /></td>
                    </tr>
                </table>

            </div>
            <div class="clear" />
        </div>
        <div>
            <table style="border-spacing: 0">
                <tr class="lastTableRow">
                    <td class="label" colspan="2">Completed Inspection Report For:</td>
                    <td colspan="2">
                        <asp:Literal ID="litCompanyCompletedForName" runat="server" /></td>
                </tr>
                <tr>
                    <td class="label">Insured Name:
                    </td>
                    <td>
                        <asp:Literal ID="litInsuredName" runat="server" />
                    </td>
                    <td class="label">Policy Number:
                    </td>
                    <td>
                        <asp:Literal ID="litPolicyNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Insured Address:
                    </td>
                    <td>
                        <asp:Literal ID="litInsuredAddress" runat="server" />
                    </td>
                    <td class="label">Inspection Date:
                    </td>
                    <td>
                        <asp:Literal ID="litInspectionDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">City,State,Zip:
                    </td>
                    <td>
                        <asp:Literal ID="litCityStateZip" runat="server" />
                    </td>
                    <td class="label">Agent:
                    </td>
                    <td>
                        <asp:Literal ID="litAgent" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Location Address:
                    </td>
                    <td>
                        <asp:Literal ID="litLocationAddress" runat="server" />
                    </td>
                    <td class="label">Agent Phone:
                    </td>
                    <td>
                        <asp:Literal ID="litAgentPhone" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">
                        City, State, Zip
                    </td>
                    <td>
                        <asp:Literal ID="litLocationCityStateZip" runat="server" />
                    </td>
                    <td class="label">
                        Underwriter:
                    </td>
                    <td>
                        <asp:Literal ID="litUnderwriter" runat="server" />
                    </td>
                </tr>
                <tr class="headerRow">
                    <td colspan="2" class="label">Occupancy
                    </td>
                    <td colspan="2" class="label">Construction & Year Dwelling Built
                    </td>
                </tr>
                <tr>
                    <td class="label">Insured Property is:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litOccupied" runat="server" />
                    </td>
                    <td class="label">Construction:
                    </td>
                    <td>
                        <asp:Literal ID="litConstruction" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td colspan="2"></td>
                    <td class="label">
                        Year Built:
                    </td>
                    <td>
                        <asp:Literal ID="litYearBuilt" runat="server" />
                    </td>

                </tr>
                <tr class="headerRow">
                    <td colspan="2" class="label">Fire Protection
                    </td>
                    <td colspan="2" class="label">Breed of Dog
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label">Protection Class:
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litProtectionClass" runat="server" />
                    </td>
                    <td class="label">Breed:
                    </td>
                    <td>
                        <asp:Literal ID="litBreed" runat="server" />
                    </td>
                </tr>
                <tr class="headerRow">
                    <td colspan="2" class="label">Water Exposure
                    </td>
                    <td colspan="2" class="label">Replacement Cost Analysis
                    </td>
                </tr>
                <tr>
                    <td class="label">Gulf of Mexico
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litGulf" runat="server" />
                    </td>
                    <td class="label">Building Coverage:
                    </td>
                    <td>
                        <asp:Literal ID="litBuildingCoverage" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="label">Atlantic Ocean
                    </td>
                    <td class="auto-style1">
                        <asp:Literal ID="litAtlOcean" runat="server" />
                    </td>
                    <td class="label">Replacement Cost:
                    </td>
                    <td>
                        <asp:Literal ID="litReplacmenetCost" runat="server" />
                    </td>
                </tr>
                <tr class="lastTableRow">
                    <td class="label"></td>
                    <td class="auto-style1"></td>
                    <td class="label">ITV%
                    </td>
                    <td>
                        <asp:Literal ID="litITV" runat="server" />
                    </td>
                </tr>
            </table>
            <div>
                <div style="width: 45%; float: left">
                    <table>
                        <tr class="headerRow">
                            <td class="label">
                                Hazards / Exposures Noted
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Literal ID="litHazards" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="float:left; width:45%; margin-left:15px;">
                    <asp:Image ID="image" Width="380px" runat="server" />
                </div>
                <div class="clear" />
            </div>
        </div>
    </div>
</div>
