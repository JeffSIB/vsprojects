﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using LC360Web;
using System.Text.RegularExpressions;
using FSMData;


namespace Utilant.Forms.Forms
{
    public partial class AMWinsCoverSheetPrint : Utilant.Web.UI.WebControls.CaseForm
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindGeneralCaseInformation();
                BindPhotos();
                BindHazards();
                BindFormData();
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
            }
        }

        private void BindHazards()
        {
            var chs = Case.CaseHazards.Where(ch => ch.CaseForm.Deleted == false).OrderByDescending(ch => ch.HazardScore);
            foreach (var ch in chs)
            {
                this.litHazards.Text += ch.Description + "<br/>";
            }
        }

        private void BindGeneralCaseInformation()
        {
            /* Company Header Information */
            try
            {
                this.litCompanyAddress.Text = Constants.CompanyAddress;
                this.litCompanyCompletedForName.Text = Case.Customer.Name;
                this.litPhone.Text = Constants.CompanyPhone;
                this.litFax.Text = Constants.CompanyFax;
            }
            catch (Exception e)
            {
                this.litCompanyAddress.Text = "5656 Central Avenue<br />St. Petersburg, FL 33707";
                this.litCompanyCompletedForName.Text = "AmWINS-7300-Access Insurance Services-PSL";
                this.litPhone.Text = "727-384-5454";
                this.litFax.Text = "727-384-6565";
            }

            /* Left-hand columns */
            this.litInsuredName.Text = Case.PolicyHolderName;
            this.litInsuredAddress.Text = Case.Address1.Line1;
            this.litCityStateZip.Text = Case.Address1.City + ", " + Case.Address1.PostalCode.StateName + ", " + Case.Address1.PostalCode.PostalCodeValue;
            this.litLocationAddress.Text = Case.Address.Line1;
            this.litLocationCityStateZip.Text = Case.Address.City + ", " + Case.Address.PostalCode.StateName + ", " + Case.Address.PostalCode.PostalCodeValue;

            /* Right-hand columns */
            this.litPolicyNumber.Text = Case.PolicyNumber;

            Regex removeTime = new Regex(" 12:00:00 AM");
            this.litInspectionDate.Text = removeTime.Replace(GetValueOrDefault("DateInspected"), "");
            if (Case.Producer != null)
            {
                this.litAgent.Text = Case.Producer.Name;
                this.litAgentPhone.Text = Case.Producer.ContactPhone;
            }
            if (Case.Underwriter != null)
                this.litUnderwriter.Text = Case.Underwriter.FullName;

            // Consistency check values //
            // Occupancy

            try
            {

                // Get generic field values
                var genericFields = Case.CaseGenericFields.Select(gf => new { FieldName = gf.CaseTypeGenericField.Name, gf.Value }).ToList();
                CaseGenericField gfZone = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "Zone");
                CaseGenericField gfMilesFromCoast = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "MilesFromCoast");
                CaseGenericField gfOccupancy = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "Occupancy");
                CaseGenericField gfConstruction = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "Construction");
                CaseGenericField gfYearBuilt = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "YearBuilt");
                CaseGenericField gfEffYearBuilt = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "EffectiveYearBuilt");
                CaseGenericField gfProtClass = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "ProtectionClass");
                CaseGenericField gfRoofType = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "RoofType");
                CaseGenericField gfRoofAge = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "RoofAge");
                CaseGenericField gfRoofMat = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "RoofMaterial");
                CaseGenericField gfRoofShape = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "RoofShape");
                CaseGenericField gfStories = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "NumberOfStories");
                CaseGenericField gfSqFt = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "SquareFeet");
                CaseGenericField gfCoverageA = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "CoverageA");
                CaseGenericField gfCoverageB = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "CoverageB");
                CaseGenericField gfAutoWaterShutoff = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "AutomaticWaterShutOff");
                CaseGenericField gfBrushWildFire = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "BrushWildfireZone");
                CaseGenericField gfFireAlarm = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "CentralStationFireAlarm");
                CaseGenericField gfBurgAlarm = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "BurglarAlarm");
                CaseGenericField gfHurrProtRoof = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "HurricaneRoofProtectionToCode");
                CaseGenericField gfHurrProtAll = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "HurricaneProtection");
                CaseGenericField gfFarms = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "Farm");
                CaseGenericField gfMobileHomes = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "MobileHomes");
                CaseGenericField gfPoolDB = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "DivingBoardNotToCode");
                CaseGenericField gfDamage = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "ExistingDamage");
                CaseGenericField gfRCV = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "DwellingsNotInsured100PctOfRCV");
                CaseGenericField gfKnob = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "KnobTubeWiring");
                CaseGenericField gfNatReg = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "NationalRegistry");
                CaseGenericField gfEIFS = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "EIFSOlderThan1998");
                CaseGenericField gfWoodStove = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "WoodStovePrimaryHeat");
                CaseGenericField gfDaycare = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "DaycareOrAssistedLiving");
                CaseGenericField gfAcerage = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "Over10Acres");
                CaseGenericField gfForeclose = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "RisksInForeclosureProceedings");
                CaseGenericField gfSpecHome = Case.CaseGenericFields.FirstOrDefault(gf => gf.CaseTypeGenericField.Name == "DeveloperSpecHomes");



                this.litZoneSelProv.Text = gfZone.Value;

                this.litMilesCoastProv.Text = gfMilesFromCoast.Value;
                this.litMilesCoastConf.Text = convertToYN(GetValueOrDefault("DistanceCoastCorrrect"));  // MISSPELLED
                this.litMilesCoastDisc.Text = GetValueOrDefault("WaterExposureRemarks");

                this.litOccupProv.Text = gfOccupancy.Value;
                this.litOccupConf.Text = convertToYN(GetValueOrDefault("UsageCorrect"));
                this.litOccupDisc.Text = GetValueOrDefault("Occupied");

                this.litConstProv.Text = gfConstruction.Value;
                this.litConstConf.Text = convertToYN(GetValueOrDefault("ExteriorWallTypeCorrect"));
                this.litConstDisc.Text = GetValueOrDefault("ExtWallConstruction");

                this.litYearBltProv.Text = gfYearBuilt.Value;
                this.litYearBltConf.Text = convertToYN(GetValueOrDefault("YearBuiltYN"));
                this.litYearBltDisc.Text = GetValueOrDefault("YearBuiltExplain");

                this.litEffYearBltProv.Text = gfEffYearBuilt.Value;
                this.litEffYearBltConf.Text = GetValueOrDefault("EffectiveYearBuilt");
                this.litEffYearBltDisc.Text = GetValueOrDefault("CorrectEffectiveDate");

                this.litPProtClassProv.Text = gfProtClass.Value;
                this.litPProtClassConf.Text = convertToYN(GetValueOrDefault("ProtectionClassCorrect"));
                this.litPProtClassDisc.Text = GetValueOrDefault("ProtectionClasses");

                this.litRoofTypeProv.Text = gfRoofType.Value;
                this.litRoofTypeConf.Text = convertToYN(GetValueOrDefault("RoofTypeCorrect"));
                this.litRoofTypeDisc.Text = GetValueOrDefault("RoofCovering");

                this.litRoofAgeProv.Text = gfRoofAge.Value;
                this.litRoofAgeConf.Text = convertToYN(GetValueOrDefault("RoofAgeCorrect"));
                this.litRoofAgeDisc.Text = GetValueOrDefault("AgeOfRoof");

                this.litRoofMatProv.Text = gfRoofMat.Value;
                this.litRoofMatConf.Text = GetValueOrDefault("RoofMaterialProvided");
                this.litRoofMatDisc.Text = GetValueOrDefault("CorrectRoofMaterial");

                this.litRoofShapeProv.Text = gfRoofShape.Value;
                this.litRoofShapeConf.Text = convertToYN(GetValueOrDefault("RoofGeometryCorrect"));
                this.litRoofShapeDisc.Text = GetValueOrDefault("RoofType");

                this.litStoriesProv.Text = gfStories.Value;
                this.litStoriesConf.Text = convertToYN(GetValueOrDefault("NumberStoriesCorrect"));
                this.litStoriesDisc.Text = stripDecimal(GetValueOrDefault("CondoStories"));

                this.litSqFtProv.Text = gfSqFt.Value;
                this.litSqFtConf.Text = convertToYN(GetValueOrDefault("SqFTCorrect"));
                this.litSqFtDisc.Text = GetValueOrDefault("SqFtAmwins");

                this.litCovAProv.Text = gfCoverageA.Value;
                this.litCovAConf.Text = convertToYN(GetValueOrDefault("CoverageACorrect"));
                this.litCovADisc.Text = formatCurrency(GetValueOrDefault("CoverageA"));

                this.litCovBProv.Text = gfCoverageB.Value;
                this.litCovBConf.Text = convertToYN(GetValueOrDefault("CoverageBCorrect"));
                this.litCovBDisc.Text = formatCurrency(GetValueOrDefault("CoverageB"));

                this.litAutoWaterProv.Text = gfAutoWaterShutoff.Value;
                this.litAutoWaterConf.Text = GetValueOrDefault("AutomaticWaterCorrect");
                this.litAutoWaterDisc.Text = GetValueOrDefault("AutomaticShutOffCorrectExplain");

                this.litBrushFireProv.Text = gfBrushWildFire.Value;
                this.litBrushFireConf.Text = convertToYN(GetValueOrDefault("AnyBrushFireExposure"));
                this.litBrushFireDisc.Text = GetValueOrDefault("BrushWildfireNo");

                this.litCenFireProv.Text = gfFireAlarm.Value;
                this.litCenFireConf.Text = convertToYN(GetValueOrDefault("FireAlarmSystem"));
                this.litCenFireDisc.Text = GetValueOrDefault("FireAlarmAmWins");

                this.litCenBurgProv.Text = gfBurgAlarm.Value;
                this.litCenBurgConf.Text = convertToYN(GetValueOrDefault("BurglarAlarmSystem"));
                this.litCenBurgDisc.Text = GetValueOrDefault("BurglarAlarmAmWins");

                // INT-EXT
                //this.litHurrRoofProv.Text = gfHurrProtRoof.Value;
                //this.litHurrRoofConf.Text = convertToYN(GetValueOrDefault("HurricaneProtectionRoof"));  
                //this.litHurrRoofDisc.Text = GetValueOrDefault("HurricaneProtectionRoofExplain");
                
                //EXT
                this.litHurrRoofProv.Text = gfHurrProtRoof.Value;
                this.litHurrRoofConf.Text = GetValueOrDefault("HurricaneProtectionRoofRadioList");
                this.litHurrRoofDisc.Text = GetValueOrDefault("HurricaneProtectionRoofExplain");

                this.litHurrProtAllProv.Text = gfHurrProtAll.Value;
                this.litHurrProtAllConf.Text = convertToYN(GetValueOrDefault("WimdstormDevicesCorrect")); // MISSPELLED
                this.litHurrProtAllDisc.Text = GetValueOrDefault("WindstormDevicesNotCorrect"); 

                this.litFarmsProv.Text = gfFarms.Value;
                this.litFarmsConf.Text = convertToYN(GetValueOrDefault("Farming"));
                this.litFarmsDisc.Text = GetValueOrDefault("FarmingNoExplain");

                this.litMHProv.Text = gfMobileHomes.Value;
                this.litMHConf.Text = convertToYN(GetValueOrDefault("MobileHomeCorrect"));
                this.litMHDisc.Text = GetValueOrDefault("MobileHomeCorrectExplain");

                this.litPoolDiveProv.Text = gfPoolDB.Value;
                this.litPoolDiveConf.Text = convertToYN(GetValueOrDefault("PoolDivingCorrect"));
                this.litPoolDiveDisc.Text = GetValueOrDefault("PoolDivingNoExplain");

                this.litDamageProv.Text = gfDamage.Value;
                this.litDamageConf.Text = convertToYN(GetValueOrDefault("PropertyExistingDamageCorrect"));
                this.litDamageDisc.Text = GetValueOrDefault("ExistingPropertyDamageRemarks");

                this.lit100RCVProv.Text = gfRCV.Value;
                this.lit100RCVConf.Text = convertToYN(GetValueOrDefault("DwellingsNotIns"));
                this.lit100RCVDisc.Text = GetValueOrDefault("DwellingsNotInsExplain");

                this.litKnobTubeProv.Text = gfKnob.Value;
                this.litKnobTubeConf.Text = GetValueOrDefault("KnobTubeCorrect");
                this.litKnobTubeDisc.Text = GetValueOrDefault("KnobTubeCorrectExplain");

                this.litNatRegProv.Text = gfNatReg.Value;
                this.litNatRegConf.Text = convertToYN(GetValueOrDefault("NationalRegistryCorrect"));
                this.litNatRegDisc.Text = GetValueOrDefault("NationalRegistryCorrectExplain");

                this.litEIFSProv.Text = gfEIFS.Value;
                this.litEIFSConf.Text = convertToYN(GetValueOrDefault("DwellingEifsCorrect"));
                this.litEIFSDisc.Text = GetValueOrDefault("DwellingsEifsExplain");

                this.litWoodStoveProv.Text = gfWoodStove.Value;
                this.litWoodStoveConf.Text = GetValueOrDefault("WoodStoveCorrect");
                this.litWoodStoveDisc.Text = GetValueOrDefault("WoodStoveExplain");

                this.litDayCareProv.Text = gfDaycare.Value;
                this.litDayCareConf.Text = convertToYN(GetValueOrDefault("DaycareCorrect"));
                this.litDayCareDisc.Text = GetValueOrDefault("DaycareAssistedNoExplain");

                this.lit10AcresProv.Text = gfAcerage.Value;
                this.lit10AcresConf.Text = convertToYN(GetValueOrDefault("OverTenAcresCorrect"));
                this.lit10AcresDisc.Text = GetValueOrDefault("OverTenAcresExplain");

                this.litForecloseProv.Text = gfForeclose.Value;
                this.litForecloseConf.Text = GetValueOrDefault("FloeclosureCorrectRadioList");
                this.litForecloseDisc.Text = GetValueOrDefault("ForeclosureCorrectExplain");

                this.litSpecHomeProv.Text = gfSpecHome.Value;
                this.litSpecHomeConf.Text = convertToYN(GetValueOrDefault("DevelopersSpeculationCorrect"));
                this.litSpecHomeDisc.Text = GetValueOrDefault("DevelopersSpeculationCorrectExp");

                //Updates
                this.litElecUpdt.Text = "Electrical: " + GetValueOrDefault("ElectricalUpgrades");
                this.litElecScope.Text = GetValueOrDefault("ElectricalUpdate");

                this.litPlumUpdt.Text = "Plumbing: " + GetValueOrDefault("PlumbingUpgrades");
                this.litPlumScope.Text = GetValueOrDefault("PlumingUpgrades");  //**** MISSPELLED

                this.litHVACUpdt.Text = "HVAC: " + GetValueOrDefault("HvacUpdates");
                this.litHVACScope.Text = GetValueOrDefault("HvacUpdate");

                this.litExpNorth.Text = GetValueOrDefault("NorthExposurer"); //**** MISSPELLED
                this.litExpSouth.Text = GetValueOrDefault("SouthExposure");
                this.litExpEast.Text = GetValueOrDefault("EastExposure");
                this.litExpWest.Text = GetValueOrDefault("WestExposure");

                this.litRIBurg.Text = "Burglar alarm: " + GetValueOrDefault("BurglarAlarm");
                this.litRIBurgMonitor.Text = "Monitoring: " + GetValueOrDefault("BurglarAlarmMonitoring");

                this.litRIFire.Text = "Fire alarm: " + GetValueOrDefault("FireAlarmLocCent");
                this.litRIFireMonitor.Text = "Monitoring: " + GetValueOrDefault("FireAlarmMonitoring");

                this.litRIProtClass.Text = "Protection class: " + GetValueOrDefault("ProtectionClass");
                this.litRIFireDept.Text = "Fire Department: " + GetValueOrDefault("NameFireDept");
                this.litRIFireDistance.Text = "Distance to fire department: " + GetValueOrDefault("DistanceFireDept");
                this.litRIComments.Text = "Comments: " + GetValueOrDefault("RemarksFireProtection"); ;

                // Rural/Remote Risk
                this.litWater1K.Text = GetValueOrDefault("RiskWithinAdequateWater");
                this.litFD10min.Text = GetValueOrDefault("FireDeptWithin");
                this.litPropAcc.Text = GetValueOrDefault("RiskAccessible");
                this.litDailyCheck.Text = GetValueOrDefault("RiskOccupied");
                this.litPropVis.Text = GetValueOrDefault("PropertyVisible");


            }
            catch (Exception ex)
            {

                this.litError.Text = ex.Message;
            }


        }

        private string convertToYN(string sValue)
        {
            string sRetStr = " ";

            if (sValue == "N/A")
                return "N/A";

            if (sValue == "True")
                return "Yes";

            if (sValue == "False")
                return "No";

            return sRetStr;

        }

        private void BindFormData()
        {

            //string sWork = "";

            /* Occupancy / Construction */
            //this.litOccupied.Text = GetValueOrDefault("BusinessTypeOccupancy");
            //this.litConstruction.Text = GetValueOrDefault("ConstructionType", null, "ConstructionType", null, null, null);
            //this.litYearBuilt.Text = GetValueOrDefault("Year", null, "Year", null, null, null);
            //this.litSqFt.Text = GetValueOrDefault("buildingAreaInt");

            /* Fire Protection / Security */

            //sWork = GetValueOrDefault("ProtectionClass");
            //this.litProtectionClass.Text = (sWork == "N/A") ? "" : sWork;
            //sWork = GetValueOrDefault("ProtectionTen");
            //this.litPC10.Text = (sWork == "N/A") ? "" : sWork;
            //this.litSmokeDet.Text = GetValueOrDefault("SmokeDetectorsPresent");
            //this.litBurgAlarm.Text = GetValueOrDefault("BurglarAlarm");

            //sWork = GetValueOrDefault(null,null, null, null,"AutoExt", null);
            //if (sWork != "N/A")
            //{
            //    this.litLblAES.Text = "AES System";
            //    this.litAES.Text = sWork;
            //}

            //sWork = GetValueOrDefault(null, null, null, null, null, "AutoExt");
            //if (sWork != "N/A")
            //{
            //    this.litLblAES.Text = "AES System";
            //    this.litAES.Text = sWork;
            //}


            //// Updates
            //this.litRoofUpdate.Text = GetValueOrDefault("RoofUpdateYear");

            // ** DEBUG **
            //string xxxxx = "";
        }

        private decimal? GetCoverageA()
        {
            var dt = SQL.ExecuteSql("SELECT CoverageAOut FROM PromotedFields WHERE CaseID = @CaseID", "@CaseID", Case.CaseID);
            if (dt.Tables.Count == 0)
                return null;
            if(dt.Tables[0].Rows.Count == 0)
                return null;
            var tr = dt.Tables[0].Rows[0];
            if (tr["CoverageAOut"] != null)
            {
                decimal covA;
                if(Decimal.TryParse(tr["CoverageAOut"].ToString(), out covA))
                    return covA;
            }

            return null;
        }

        private string GetITV(decimal? coverageAIn, decimal? coverageAOut)
        {
            var cAOut = coverageAOut ?? 0;
            var cAIn = coverageAIn ?? 0;
            if(cAOut != 0)
                return Math.Round((cAIn / cAOut) * 100, 0) + " %";
            return "N/A";
        }

        private string GetValueOrDefault(string ColName, bool liabilityHas=true)
        {
            if (liabilityHas)
                return GetValueOrDefault(ColName, ColName, ColName, ColName, ColName, null);
            else
                return GetValueOrDefault(ColName, ColName, ColName, null, ColName, null);
        }

        private List<Guid> NoFormDataFound = new List<Guid>();

        private DataRow GetFormData(Guid FormID)
        {
            if (NoFormDataFound.Contains(FormID)) //so we don't have to hit the db on every request if we don't find a form on the Case the first time just skip.
                return null;

            DataRow dr = null;
            var caseForm = Case.CaseForms.Where(cf => cf.FormID == FormID && cf.Deleted == false).FirstOrDefault();
            if (caseForm != null)
            {
                string table = caseForm.Form.formTableName;
                var sql = "SELECT * FROM Form." + table + " WHERE FormID = @formInstanceID";
                    //f0e982e2-8495-4ac7-8dfd-dd54f129c8d3

                var ds = SQL.ExecuteSql(sql, "@formInstanceID", caseForm.FormInstanceID);
                if (ds.Tables.Count == 0)
                {
                    NoFormDataFound.Add(FormID);
                    return null;
                }
                var t = ds.Tables[0];
                if (t.Rows.Count > 0)
                    dr = t.Rows[0];
                else
                    NoFormDataFound.Add(FormID);
            }
            else
            {
                NoFormDataFound.Add(FormID);
            }

            return dr;
        }

        private string commaDelimitIfList(string list)
        {
            if (list.IsNotNullOrEmpty() && list.Contains('|'))//probably a list
                return string.Join(", ", list.Split('|').ToArray()).Trim(',',' ');
            return list;
        }

        private string GetColumnValue(DataRow row, string column)
        {
            try
            {
                if (row != null
                    && row[column] != null
                    && row[column].ToString().IsNotNullOrEmpty())
                    return commaDelimitIfList(row[column].ToString());
                return null;
            }
            catch
            {
                return null;
            }
        }


        private DataRow ResIntExtRoofRow = null;
        private DataRow LiabShortRow = null;
        private DataRow PropShortRow = null;
        private DataRow BriefLiabilityWOE2Val12Row = null;
        private DataRow CommCookSupRow = null;
        private DataRow RestLiabSupRow = null;

        private string GetValueOrDefault(string ResIntExtRoofCol, string LiabShortCol, string PropShortCol, string BriefLiabilityWOE2Val12Col, string CommCookSup, string RestLiabSup)
        {
            //AmWins Residential Interior & Exterior Roof 
            if (ResIntExtRoofCol != null)
            {
                if (ResIntExtRoofRow == null)
                    ResIntExtRoofRow = GetFormData(new Guid("e06b5442-323e-498e-9eef-c74141f4b41a")); // Form GUID


                var ResIntExtRoofVal = GetColumnValue(ResIntExtRoofRow, ResIntExtRoofCol);
                if (ResIntExtRoofVal != null)
                    return ResIntExtRoofVal;
            }

            //AmWins Residential Exterior Roof 
            if (LiabShortCol != null)
            {
                if (LiabShortRow == null)
                    LiabShortRow = GetFormData(new Guid("ccdf7a46-91f6-4c49-ba16-c17f6a33b83a"));
                //BriefExteriorWOE2VAL562Row = GetFormData(new Guid("51f4fec6-4ae8-4574-b91f-9bc7f4ce1000"));

                var LiabShortVal = GetColumnValue(LiabShortRow, LiabShortCol);

                if (LiabShortVal != null)
                    return LiabShortVal;
            }


            //Hull - Tampa Bay Property Short v1.31.18 
            if (PropShortCol != null)
            {
                if (PropShortRow == null)
                    PropShortRow = GetFormData(new Guid("714597b2-29e1-4046-a2c2-a22105d52ef7"));


                var PropShortVal = GetColumnValue(PropShortRow, PropShortCol);

                if (PropShortVal != null)
                    return PropShortVal;
            }


            //Commercial cooking supp
            if (CommCookSup != null)
            {
                if (CommCookSup == "ProtectionClass") return "N/A";
                if (CommCookSupRow == null)
                    CommCookSupRow = GetFormData(new Guid("c7b8aaa3-9f7a-4a1f-86b5-79e5a90af3ac"));

                var CommCookVal = GetColumnValue(CommCookSupRow, CommCookSup);

                if (CommCookVal != null)
                    return CommCookVal;
            }

            // Hull Restaurant Liability Add On v1.31.18
            if (RestLiabSup != null)
            {
                if (RestLiabSup == "ProtectionClass") return "N/A";
                if (RestLiabSupRow == null)
                    RestLiabSupRow = GetFormData(new Guid("4c396ea1-b786-43d5-840a-7dcbdc990cb8")); // Hull Restaurant Liability Add On v1.31.18

                var RestLiabSupVal = GetColumnValue(RestLiabSupRow, RestLiabSup);

                if (RestLiabSupVal != null)
                    return RestLiabSupVal;
            }

            return "N/A";
        }


        private void BindPhotos()
        {
            if (Case.Photos.Count == 0)
                this.image.ImageUrl = "~/images/custom/housenoimageavailable.jpg";
            else
            {
                var photoID = Case.Photos.OrderBy(p => p.DisplayOrder).Take(1).Select(p => p.PhotoID.ToString()).FirstOrDefault();
                this.image.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

                // ** DEBUG **
                //this.image.ImageUrl = "https://ecommerce3.sibfla.com/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + photoID + "-500.jpg";

            }
        }

        public override void LoadData()
        {

        }

        public override void ClearFormData(FSMData.Form form)
        {

        }

        private string stripDecimal(string sInput)
        {
            try
            {
                int iPos = sInput.IndexOf(".");

                if (iPos == 0)
                {
                    return sInput;
                }
                else
                {
                    return sInput.Left(iPos);
                }
            }

            catch
            {
                return sInput;
            }
        }

        private string formatCurrency(string sInput)
        {
            try
            {
                int iVal = 0;
                if (int.TryParse(sInput, out iVal))
                {

                    return iVal.ToString("C0");
                }
                else
                {
                    return sInput;
                }

            }

            catch
            {
                return sInput;
            }
        }
    }
}