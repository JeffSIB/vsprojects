﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FSMWeb;


namespace Utilant.Forms.Forms
{
	public partial class CoverSheetRecsPrint : Utilant.Web.UI.WebControls.CaseForm
	{

        public int TotalNumberOfGenRecommendations { get; set; }
        public int? TotalNumGenRecs { get; set; }

        public int TotalNumberOfSugRecommendations { get; set; }
        public int? TotalNumSugRecs { get; set; }



        protected void Page_Load(object sender, EventArgs e)
		{
			//caseForm = db.CaseForms.SingleOrDefault(cf => cf.FormInstanceID == FormInstanceID);
			//Case = caseForm.Case;

			FSMData.Photo Photo = Case.Photos.Where(p => p.IncludeInReport == true).OrderBy(p => p.DisplayOrder).FirstOrDefault();
			string Role = Query.ToString("Role", "").ToLower();

			if (!IsPostBack)
			{

				if (Photo != null)
				{
					imgMain.ImageUrl = "~/images/photoHandler.ashx?caseID=" + Case.CaseID + "&ImageID=" + Photo.PhotoID + "-500.jpg";

					imgMain.Height = new Unit(Photo.GetHeightThatWillFitInBounds(400, 700) + " px");
					litPhotoTitle.Text = Photo.Description.Length > 0 ? "<br/><strong>" + Photo.Description + "</strong>" : "";
				}
				else
				{
					imgMain.Visible = false;
				}

				if( Case.CasePreUnderwritingOptions.Any() )
				{
					litPreUnderwriterOptions.Text = "<div style='text-align:center'><strong>Underwriting Code(s):</strong> " + Case.CasePreUnderwritingOptions.Select(pu => pu.Text).ToDelimitedString(", ") + "</div>";
				}

				var pms = FSMData.PromotedField.GetNonNullPromotedFields(Case.CaseID,null,false);
				gvImportantFields.DataSource = from pm in pms
											   orderby pm.Field
											   select new
											   {
												   pm.Field,
												   Value = pm.Value.Replace("|", ", ").Trim().Trim(',')
											   };
				gvImportantFields.DataBind();

				BindHazards();
                TotalNumGenRecs = (int)GetGenRecommendations(TotalNumberOfGenRecommendations);
                TotalNumSugRecs = (int)GetSugRecommendations(TotalNumberOfSugRecommendations);

                //PromotedFields fields = promoted.Tables[0].CastToList<PromotedFields>()[0];



                //litCoverageOut.Text = fields.CoverageAOut.HasValue ? fields.CoverageAOut.Value.ToString("C") : "unknown";

                var coverageOut = pms.SingleOrDefault(pf => pf.Field == "Coverage Out");



				if (Case.CaseField.CoverageAIn.HasValue && coverageOut != null && !string.IsNullOrEmpty(coverageOut.Value))
				{
					decimal coverageOutVal = Convert.ToDecimal(coverageOut.Value);

					var coveragePercent = Math.Abs((((decimal)Case.CaseField.CoverageAIn.Value) / coverageOutVal)).ToString("P");
					litCoveragePercent.Text = "<div class='coveragePercentage'>Coverage In: " + Case.CaseField.CoverageAIn.Value.ToString("C") + " Coverage Out: " + coverageOutVal.ToString("C") + " Coverage is " + coveragePercent + " of final replacement cost</div>";
				}

			}
		}


		private void BindHazards()
		{
			gvHazards.DataSource = (from h in Case.CaseHazards
									where h.CaseForm.Deleted == false
									orderby h.HazardScore descending
									select new
									{
										h.Description,
										Score = h.HazardScore.ToString() + " pts",
										Image = h.CancelsPolicy ? "~/images/error.png" : "~/images/warning.png"
									}).ToList();
			gvHazards.DataBind();
		}


	  

		public override void LoadData()
		{
 
		}

		public override void ClearFormData(FSMData.Form form)
		{
			
		}

        private object GetSugRecommendations(object ds)
        {

            if (ds is int)
            {
                try
                {

                    ds = SQL.GetInteger(@"Select  
            	count(*) as TotalNumberOfRecs 
            from 
            	caseformrecs cfr
            	inner join CaseForms CF on CF.CaseFormID=cfr.CaseFormID and cfr.visible = 1
            	inner join Cases C on C.CaseID=CF.CaseID
            	left outer join RecommendationTypes RT on RT.RecommendationTypeID=cfr.RecommendationTypeID
            where 
            	cf.deleted=0 and and RT.name = 'Suggested'  c.CaseID = @CaseId", "@CaseId", this.Case.CaseID);

                    return ds;
                }
                catch 
                {
                    return 0;
                }
            }


            //if (ds is List<Recommendations>)
            //{
            //    try
            //    {
            //        ds = SQL.ExecuteSql(@"Select 
            //	ISNULL(RT.Name,'Unknown Category') as Name, 
            //	count(*) as Count 
            //from 
            //	caseformrecs cfr
            //	inner join CaseForms CF on CF.CaseFormID=cfr.CaseFormID and cfr.visible = 1
            //	inner join Cases C on C.CaseID=CF.CaseID
            //	left outer join RecommendationTypes RT on RT.RecommendationTypeID=cfr.RecommendationTypeID
            //where 
            //	cf.deleted=0 and c.CaseID = @caseID
            //Group by 
            //	ISNULL(RT.Name,'Unknown Category')", "@caseID", this.Case.CaseID).Tables[0].CastToList<Recommendations>();

            //        if (ds == null)
            //            return null;

            //        return ds;
            //    }
            //    catch
            //    {
            //        return null;
            //    }
            //}

            return 0;
        }




        private object GetGenRecommendations(object ds)
        {

            if (ds is int)
            {
                try
                {

                    ds = SQL.GetInteger(@"Select  
            	count(*) as TotalNumberOfRecs 
            from 
            	caseformrecs cfr
            	inner join CaseForms CF on CF.CaseFormID=cfr.CaseFormID and cfr.visible = 1
            	inner join Cases C on C.CaseID=CF.CaseID
            	left outer join RecommendationTypes RT on RT.RecommendationTypeID=cfr.RecommendationTypeID
            where 
            	cf.deleted=0 and RT.name = 'General' and c.CaseID = @CaseId", "@CaseId", this.Case.CaseID);

                    return ds;
                }
                catch
                {
                    return 0;
                }
            }

            return 0;
        }
    }
}