﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Linq;
using System.Net;


namespace importAmRisc
{

    /// <summary>
    /// </summary>
    /// 

    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyNumber { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string UnderwriterEmail { get; set; }
            public string UnderwriterPhone { get; set; }
            public string UWAssistantName { get; set; }
            public string UWAssistantEmail { get; set; }
            public string UWAssistantPhone { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string BrokerName { get; set; }
            public string BrokerPhone { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            //public string BusinessOperations { get; set; }
            public string LocationFullAddress { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string CoverageB { get; set; }
            public string CoverageC { get; set; }
            public string CoverageD { get; set; }
            public string CoverageE { get; set; }
            public string CoverageF { get; set; }
            public string ISOClass { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }

            public string InspectionLink { get; set; }
            public string LocationNumber { get; set; }
            public string LocationName { get; set; }
            public string SpecialInstructions { get; set; }


            public string Construction { get; set; }
            public string YearBuilt { get; set; }
            public string DwellingType { get; set; }
            public string NumberOfFloors { get; set; }
            public string Occupancy { get; set; }
            public string SquareFootage { get; set; }
            public string DistanceToCoast { get; set; }
            public string RoofYear { get; set; }
            public string RoofMaterials { get; set; }
            public string RoofGeometry { get; set; }
            public string Usage { get; set; }
            public string WindMitigation { get; set; }
            public string PCClass { get; set; }
            public string RoofCladding { get; set; }
            public string RoofCondition { get; set; }
            public string BuildingCladding { get; set; }
            public string CompanyPolicyForm { get; set; }
            public string CentralFireAlarm { get; set; }
            public string CentralBurglarAlarm { get; set; }
            public string CentralAlarm { get; set; }
            public string PolicyStatus { get; set; }
            public string PoolFenced { get; set; }
            public string Heating { get; set; }
            public string HeatingFullPartial { get; set; }
            public string HeatingUpdate { get; set; }
            public string HeatingYear { get; set; }
            public string PoolAboveGround { get; set; }
            public string Farming { get; set; }
            public string NumberOfFamilies { get; set; }
            public string Pets { get; set; }
            public string Plumbing { get; set; }
            public string PlumbingFullPartial { get; set; }
            public string PlumbingUpdate { get; set; }
            public string PlumbingYear { get; set; }
            public string PoolSlide { get; set; }
            public string PoolDivingBoard { get; set; }
            public string PoolScreenEncl { get; set; }
            public string Pool { get; set; }
            public string Updates { get; set; }
            public string Wiring { get; set; }
            public string WiringFullPartial { get; set; }
            public string WiringUpdate { get; set; }
            public string WiringYear { get; set; }



        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        //static string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
        static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        //static string cfg_SQLMainSIBIConnStr = ConfigurationManager.AppSettings["SIBIConnStr"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];
        static string cfg_NewRequestsURI = ConfigurationManager.AppSettings["NewRequestsURI"];


        static LogUtils.LogUtils oLU;
        static bool bErr = false;
        static int iNodes = 0;

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {

            string sXMLFileName = "";
            DateTime dToday = new DateTime();
            dToday = DateTime.Now;

            bool bImportSuccess = false;

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                // Look for file passed via command line - manual import
                if (args.Length > 0)
                {
                    //get excel file name from command line
                    sXMLFileName = args[0];

                    oLU.WritetoLog("Manual import of: " + sXMLFileName);
                }
                else
                {

                    try
                    {

                        // load XML from AmRisc website
                        oLU.WritetoLog("Requesting XML from website");
                        HttpWebRequest request = WebRequest.Create(cfg_NewRequestsURI) as HttpWebRequest;
                        request.ContentType = "application/xml";
                        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(response.GetResponseStream());

                        //build file name
                        sXMLFileName = "AmRisc" + dToday.Year.ToString() + dToday.Month.ToString() + dToday.Day.ToString() + dToday.Hour.ToString() + dToday.Minute.ToString() + dToday.Second.ToString() + ".xml";

                        //save file to tmp folder
                        xmlDoc.Save(cfg_sourcedir + sXMLFileName);
                        xmlDoc = null;

                        oLU.WritetoLog("Saved XML as: " + sXMLFileName);
                    }

                    catch (WebException ex)
                    {
                        if (ex.Response != null)
                        {
                            var response = ex.Response;
                            var dataStream = response.GetResponseStream();
                            var reader = new StreamReader(dataStream);
                            var details = reader.ReadToEnd();
                        }
                    }

                }

                // does file exist?
                FileInfo fi = new FileInfo(cfg_sourcedir + sXMLFileName);
                if (!fi.Exists)
                {
                    // send email
                    throw new ApplicationException("XML file does not exist: " + sXMLFileName);
                }

            }


            catch (Exception ex)
            {
                try
                {
                    oLU.WritetoLog("Error initializing importAmRisc\r\n\r\n" + ex.Message);
                    oLU.closeLog();
                }
                catch
                {
                    // ignore error
                }
                sendErrEmail("Error initializing importAmRisc\r\n\r\n" + ex.Message);
                return;
            }

            iNodes = 0;

            try
            {


                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(cfg_sourcedir + sXMLFileName);
                XmlNodeList requests = xmlDoc.GetElementsByTagName("InspectionRequestData");

                // process each inspection in XML file
                foreach (XmlNode node in requests)
                {

                    ImportRecord IR = new ImportRecord();

                    XmlElement reqElem = (XmlElement)node;

                    iNodes++;

                    IR.AgencyAgentName = reqElem.GetElementsByTagName("BrokerCo")[0].InnerText;
                    IR.Producer = reqElem.GetElementsByTagName("Broker")[0].InnerText;
                    IR.AgencyAgentPhone = reqElem.GetElementsByTagName("BrokerPhone")[0].InnerText;
                    IR.Comments = reqElem.GetElementsByTagName("InspComments")[0].InnerText;
                    IR.InspectionLink = reqElem.GetElementsByTagName("InspectionFormURL")[0].InnerText;
                    IR.LocationNumber = reqElem.GetElementsByTagName("LocNum")[0].InnerText;
                    IR.InsuredName = reqElem.GetElementsByTagName("InsuredName")[0].InnerText;
                    IR.LocationName = reqElem.GetElementsByTagName("LocationName")[0].InnerText;
                    if (IR.LocationName.Length == 0)
                        IR.LocationName = "";

                    IR.PolicyNumber = reqElem.GetElementsByTagName("PolicyNumber")[0].InnerText;
                    IR.ContactName = reqElem.GetElementsByTagName("SiteContact")[0].InnerText;
                    IR.ContactPhoneWork = reqElem.GetElementsByTagName("SiteContactPhone")[0].InnerText;
                    IR.SpecialInstructions = reqElem.GetElementsByTagName("SpecInstruct")[0].InnerText;
                    IR.LocationAddress1 = reqElem.GetElementsByTagName("StreetName")[0].InnerText;
                    IR.LocationAddress2 = "";
                    IR.LocationCity = reqElem.GetElementsByTagName("City")[0].InnerText;
                    IR.LocationState = reqElem.GetElementsByTagName("StateCode")[0].InnerText;
                    IR.LocationZip = reqElem.GetElementsByTagName("Zip")[0].InnerText;
                    IR.Underwriter = reqElem.GetElementsByTagName("UWName")[0].InnerText;
                    IR.UnderwriterEmail = reqElem.GetElementsByTagName("UWEmail")[0].InnerText;
                    IR.UnderwriterPhone = reqElem.GetElementsByTagName("UWPhone")[0].InnerText;
                    IR.BrokerName = reqElem.GetElementsByTagName("Broker")[0].InnerText;
                    IR.BrokerPhone = reqElem.GetElementsByTagName("BrokerPhone")[0].InnerText;
                    IR.UWAssistantName = reqElem.GetElementsByTagName("UWTAName")[0].InnerText;
                    IR.UWAssistantEmail = reqElem.GetElementsByTagName("UWTAEmail")[0].InnerText;
                    IR.UWAssistantPhone = reqElem.GetElementsByTagName("UWTAPhone")[0].InnerText;

                    ImportRequests oAPI = new ImportRequests();
                    oAPI.CustomerUserName = "APIProd";
                    oAPI.CustomerPassword = "Sutton2012";
                    oAPI.CustomerAccount = "7316";

                    // If policy number is 0 or empty - use insured
                    if (IR.PolicyNumber == "0" || IR.PolicyNumber.Length == 0)
                    {
                        IR.PolicyNumber = IR.InsuredName;
                        if (IR.PolicyNumber.Length > 20)
                            IR.PolicyNumber = IR.PolicyNumber.Substring(0, 20);
                    }
                    // standard values provided
                    oAPI.InsuredName = IR.InsuredName;
                    oAPI.LocationContactName = IR.ContactName;
                    oAPI.ContactPhoneWork = IR.ContactPhoneWork;
                    oAPI.ContactPhoneCell = "";
                    oAPI.ContactPhoneHome = "";
                    oAPI.PolicyNumber = IR.PolicyNumber;
                    //oAPI.EffectiveDate = IR.EffectiveDate;
                    oAPI.CoverageA = "";
                    oAPI.LocationAddress1 = IR.LocationAddress1;
                    oAPI.LocationAddress2 = "";
                    oAPI.LocationCity = IR.LocationCity;
                    oAPI.LocationState = IR.LocationState;
                    oAPI.LocationZip = IR.LocationZip;
                    oAPI.LocationName = IR.LocationName;
                    oAPI.LocationNumber = IR.LocationNumber;
                    oAPI.MailAddress1 = IR.LocationAddress1;
                    oAPI.MailAddress2 = "";
                    oAPI.MailCity = IR.LocationCity;
                    oAPI.MailState = IR.LocationState;
                    oAPI.MailZip = IR.LocationZip;
                    oAPI.InsuranceCompany = "";

                    oAPI.AgencyAgentName = IR.AgencyAgentName;
                    oAPI.AgencyAgentPhone = IR.AgencyAgentPhone;
                    oAPI.AgentCode = "";
                    oAPI.AgentFax = "";
                    oAPI.AgencyAgentContact = IR.Producer;
                    oAPI.AgentAddress1 = "5656 Central Ave";
                    oAPI.AgentAddress2 = "";
                    oAPI.AgentCity = "St. Petersburg";
                    oAPI.AgentState = "FL";
                    oAPI.AgentZip = "33707";

                    oAPI.Producer = IR.Producer;
                    oAPI.Underwriter = IR.Underwriter;
                    oAPI.UnderwriterFirstName = "";
                    oAPI.UnderwriterLastName = "";
                    oAPI.UnderwriterPhone = IR.UnderwriterPhone;
                    oAPI.UnderwriterCorrEmail = IR.UnderwriterEmail;
                    oAPI.InspectionURI = IR.InspectionLink;
                    oAPI.BrokerName = IR.BrokerName;
                    oAPI.BrokerPhone = IR.BrokerPhone;
                    oAPI.UWAssistantName = IR.UWAssistantName;
                    oAPI.UWAssistantPhone = IR.UWAssistantPhone;
                    oAPI.UWAssistantEmail = IR.UWAssistantEmail;




                    // standard values not provided
                    oAPI.EmailConfirmation = "";
                    oAPI.RushHandling = "Y";
                    oAPI.ContactName = "";

                    // Convert CoverageA to numeric
                    //double dCovA = Convert.ToDouble(row.CoverageA);

                    // inspection type
                    string sInspType = "7316-IT";

                    oAPI.InspectionType = sInspType;

                    // comments
                    oAPI.Comments = IR.Comments + "\r\n\r\n" + IR.SpecialInstructions +
                        "\r\nLocation name: " + IR.LocationName +
                        "\r\nLocation number: " + IR.LocationNumber +
                        "\r\nBroker name: " + IR.BrokerName +
                        "\r\nBroker phone: " + IR.BrokerPhone +
                        "\r\nUW Assistant name: " + IR.UWAssistantName +
                        "\r\nUW Assistant phone: " + IR.UWAssistantPhone +
                        "\r\nUW Assistant email: " + IR.UWAssistantEmail;


                    //**********************************
                    // UNCOMMENT FOR TEST 
                    //oAPI.CustomerUserName = "APITest";
                    //oAPI.CustomerAccount = "9998";
                    //oAPI.InspectionType = "9998RE";
                    //oAPI.EffectiveDate = "";

                    //**********************************

                    oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + IR.PolicyNumber);
                    sbEmail.Append("Importing Policy# " + IR.PolicyNumber + System.Environment.NewLine);

                    string sRet = oAPI.ImportAmRisc();

                    oLU.WritetoLog("oAPI.Import return for for Policy# " + IR.PolicyNumber + "\r\n\r\n" + sRet);

                    var importResults = sRet.FromJSON<List<ImportResult>>();

                    foreach (var importResult in importResults)
                    {

                        if (importResult.Successful)
                        {
                            oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                            sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + System.Environment.NewLine);
                        }
                        else
                        {
                            oLU.WritetoLog("**** Import failed **** "); if (importResult.Errors != null)
                                sbEmail.Append("**** Import failed ****" + System.Environment.NewLine);

                            foreach (var error in importResult.Errors)
                            {
                                oLU.WritetoLog("Error: " + error.ErrorText);
                                sbEmail.Append("Error: " + error.ErrorText + System.Environment.NewLine);
                            }

                            throw new ApplicationException("Import Failed: " + sRet);
                        }

                        if ((bool)importResult.Duplicate)
                        {
                            oLU.WritetoLog("Duplicate case");
                            sbEmail.Append("Duplicate case" + System.Environment.NewLine);
                        }
                    }

                }   // foreach node

                bImportSuccess = true;


            }   //try

            catch (Exception ex)
            {
                bImportSuccess = false;
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail("Exception logged" + System.Environment.NewLine + ex.Message);
            }

            finally
            {


                // Copy source file to holding area and delete temp
                string sSourceName = sXMLFileName;
                string sDestName = "";
                string sDestFolder = "";
                int iUnique = 1;

                //if successful - copy to sibidata\AmRisc\Processed
                //if failed - copy to sibidata\AmRisc\fail
                if (bImportSuccess)
                {
                    sDestFolder = cfg_compdir;
                }
                else
                {
                    sDestFolder = cfg_faildir;
                }

                // only copy if data present
                if (iNodes == 0)
                {
                    oLU.WritetoLog("Nothing to import");
                }
                else
                {


                    if (File.Exists(sDestFolder + sSourceName))
                    {

                        iUnique = 2;
                        while (true)
                        {

                            if (File.Exists(sDestFolder + iUnique.ToString() + sSourceName))
                            {
                                iUnique++;
                                if (iUnique > 10)
                                {
                                    throw new ApplicationException(sDestFolder + iUnique.ToString() + sSourceName + " could not be saved - exists.");
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    // Copy file to dest folder
                    sDestName = sDestFolder + iUnique.ToString() + sSourceName;
                    File.Copy(cfg_sourcedir + sSourceName, sDestName);

                    // Verify it exists
                    if (!File.Exists(sDestName))
                    {
                        oLU.WritetoLog("Copy failed for: " + sXMLFileName);
                    }
                }

                File.Delete(cfg_sourcedir + sSourceName);

                sendLogEmail(sbEmail.ToString());

                oLU.closeLog();
            }

        }

        /// <summary>
        /// createKey360
        /// </summary>
        /// <returns></returns>
        //public string createKey360()
        //{

        //    string sXMLFile = msXMLFilePath + msXMLFile;
        //    msReturnStr = "";
        //    string sErrText = "";

        //    XmlTextReader reader = null;

        //    try
        //    {

        //        // verify xml file exists
        //        if (!File.Exists(sXMLFile))
        //        {
        //            throw new ApplicationException("File does not exist: " + sXMLFile);
        //        }





        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import AmRisc Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import AmRisc Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static XmlDocument MakeRequest(string requestUrl)
        {
            try
            {

                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                request.ContentType = "application/xml";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(response.GetResponseStream());
                return (xmlDoc);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                Console.Read();
                return null;
            }
        }
    }
}
