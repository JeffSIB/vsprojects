﻿using System;
using System.Collections.Generic;
using System.Text;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;
using System.Text.RegularExpressions;
using WinSCP;
using System.Xml;


namespace importCapacity
{

    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyNumber { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string UnderwriterEmail { get; set; }
            public string AgentFirstName { get; set; }
            public string AgentLastName { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string AgencyAgentEmail { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string InsuredEmail { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            //public string BusinessOperations { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string ISOClass { get; set; }
            public string YearBuilt { get; set; }
            public string Occupancy { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }
            public string Dimensions { get; set; }
            public string SecurityGate { get; set; }
            public string SecurityGuard { get; set; }
            public string Construction { get; set; }
            public string Valuation { get; set; }
            public string OrderReference { get; set; }
        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
        static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string cfg_SQLMainSIBIConnStr = ConfigurationManager.AppSettings["SIBIConnStr"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];

        static LogUtils.LogUtils oLU;
        static int iNodes = 0;

        //Used for email message body  
        static StringBuilder sbEmail = new StringBuilder();
        static StringBuilder sbConfEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {
            string sXMLFileName = "";
            string sFileName = "";
            int iNumDownloaded = 0;
            int iNumRenamed = 0;
            int iNumProcessed = 0;

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                // Look for file passed via command line - manual import
                if (args.Length > 0)
                {
                    // MANUAL IMPORT

                    sXMLFileName = args[0];

                    // does file exist?
                    FileInfo fi = new FileInfo(cfg_sourcedir + sXMLFileName);
                    if (!fi.Exists)
                    {
                        // send email
                        throw new ApplicationException("File does not exist: " + cfg_sourcedir + sXMLFileName);
                    }

                    oLU.WritetoLog("Manual import of: " + sXMLFileName);

                    importFile(fi.Name);
                    iNumDownloaded = 1;
                }
                else
                {
                    // AUTOMATED IMPORT

                    // check for files on SFTP site
                    iNumDownloaded = getSFTP();

                    // if SFTP returns error
                    if (iNumDownloaded < 0)
                    {
                        throw new ApplicationException("SFTP Download return error");
                    }
                    // nothing downloaded
                    else if (iNumDownloaded == 0)
                    {
                        oLU.WritetoLog("Nothing downloaded");
                    }

                    // process files
                    else
                    {
                        oLU.WritetoLog(iNumDownloaded.ToString() + " files downloaded");

                        DirectoryInfo diImportFiles = new DirectoryInfo(cfg_sourcedir);
                        FileInfo[] fiImportFiles = diImportFiles.GetFiles("*.xml");

                        // remove spaces from file name
                        foreach (FileInfo file in fiImportFiles)
                        {
                            // remove spaces from file names
                            sFileName = file.Name.Replace(" ", String.Empty);
                            file.MoveTo(cfg_sourcedir + sFileName);
                            iNumRenamed++;
                        }

                        fiImportFiles = diImportFiles.GetFiles("*.xml");
                        foreach (FileInfo file in fiImportFiles)
                        {
                            // import files
                            importFile(file.Name);
                            iNumProcessed++;
                        }
                    }

                    oLU.WritetoLog(iNumDownloaded.ToString() + " files downloaded");
                    oLU.WritetoLog(iNumRenamed.ToString() + " files renamed");
                    oLU.WritetoLog(iNumProcessed.ToString() + " files processed");

                    // make sure the same number of files downloaded were processed
                    if (iNumRenamed != iNumDownloaded || iNumProcessed != iNumDownloaded)
                    {
                        oLU.WritetoLog("**** Files processed <> files downloaded");
                        throw new ApplicationException("Number of files processed not equal to number downloaded");
                    }

                }   // AUTOMATED IMPORT
                
                oLU.closeLog();

            }

            catch (Exception ex)
            {
                oLU.WritetoLog("Error initializing importCapacity\r\n\r\n" + ex.Message);
                oLU.closeLog();
                sendErrEmail("Error initializing importCapacity\r\n\r\n" + ex.Message);
                return;
            }
        }

        static void importFile(string sXMLFileName)
        {
            bool bImportSuccess = false;

            try
            {

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(cfg_sourcedir + sXMLFileName);

                XmlNodeList requests = xmlDoc.GetElementsByTagName("Order");

                // process each inspection in XML file
                foreach (XmlNode node in requests)
                {

                    ImportRecord IR = new ImportRecord();

                    XmlElement reqElem = (XmlElement)node;

                    iNodes++;

                    //IR.InsuredName = reqElem.GetElementsByTagName("name")[0].InnerText;
                    //IR.ContactName = reqElem.GetElementsByTagName("contact")[0].ChildNodes[0].InnerText;
                    //IR.ContactPhoneWork = reqElem.GetElementsByTagName("contact")[0].ChildNodes[1].InnerText;
                    //IR.RushHandling = reqElem.GetElementsByTagName("priorityLevel")[0].InnerText;

                    XmlNodeList policyInfo = reqElem.GetElementsByTagName("Policy_Info");
                    foreach (XmlNode policy in policyInfo)
                    {
                        IR.PolicyNumber = reqElem.GetElementsByTagName("PolicyNumber")[0].InnerText;
                        IR.InsuredName = reqElem.GetElementsByTagName("InsuredName")[0].InnerText;
                        IR.InsuranceCompany = reqElem.GetElementsByTagName("InsuranceCompany")[0].InnerText;
                        IR.ContactName = testNode(reqElem,("ContactPerson"));
                        IR.ContactPhoneHome = testNode(reqElem,("ContactPhoneNumber"));
                        IR.EffectiveDate = testNode(reqElem,("EffectiveDate"));
                    }

                    XmlNodeList agentInfo = reqElem.GetElementsByTagName("Agency_Info");
                    foreach (XmlNode agent in agentInfo)
                    {
                        IR.AgencyAgentName = testNode(reqElem,("AgencyName"));
                        IR.AgencyAgentPhone = testNode(reqElem,("AgentPhone"));
                        IR.AgencyAgentEmail = testNode(reqElem,("AgentEmail"));
                    }

                    int iBuilding = 1;
                    string sBuildingInfo = "";
                    string sWork = "";

                    //Assume first building has address, other go to comments
                    XmlNodeList buildings = reqElem.GetElementsByTagName("Buildings");
                    foreach (XmlNode building in buildings)
                    {
                        foreach (XmlNode childBuilding in building.ChildNodes)
                        {
                            XmlElement bldgElem = (XmlElement)childBuilding;

                            if (iBuilding == 1)
                            {
                                IR.LocationAddress1 = bldgElem.GetElementsByTagName("PropertyStreetAddress")[0].ChildNodes[0].InnerText;
                                IR.LocationAddress2 = "";
                                IR.LocationCity = bldgElem.GetElementsByTagName("PropertyCity")[0].ChildNodes[0].InnerText;
                                IR.LocationState = bldgElem.GetElementsByTagName("PropertyState")[0].ChildNodes[0].InnerText;
                                IR.LocationZip = bldgElem.GetElementsByTagName("ZipCode")[0].ChildNodes[0].InnerText;
                                IR.Construction = testNode(bldgElem, "Construction");
                                IR.YearBuilt = testNode(bldgElem, "YearBuilt");
                                IR.BuildingCost = testNode(bldgElem, "BuildingLimit");
                                IR.ContentsCost = testNode(bldgElem, "ContentsLimit");
                                IR.Occupancy = testNode(bldgElem, "BuildingDescription");

                                IR.Comments = "Building 1: " + testNode(bldgElem, "SpecialInstruction") + "\r\n";
                                IR.Comments += "Construction: " + IR.Construction;

                            }
                            else
                            {
                                sBuildingInfo = "\r\n\r\n";

                                //if (sBuildingInfo.Length > 1)
                                sBuildingInfo += "Building " + iBuilding.ToString() + ": " + "\r\n"; ;

                                sWork = testNode(bldgElem, "PropertyStreetAddress");
                                if (sWork.Length > 0)
                                    sBuildingInfo += sWork + "\r\n";

                                sWork = testNode(bldgElem, "BuildingLimit");
                                if (sWork.Length > 0)
                                    sBuildingInfo += "Building cost: " + sWork + "\r\n";

                                sWork = testNode(bldgElem, "ContentsLimit");
                                if (sWork.Length > 0)
                                    sBuildingInfo += "Contents cost: " + sWork + "\r\n";

                                sWork = testNode(bldgElem, "BuildingDescription");
                                if (sWork.Length > 0)
                                    sBuildingInfo += "Description: " + sWork + "\r\n";

                                IR.Comments += sBuildingInfo;

                            }   // for each building

                            iBuilding++;

                        }   // for each buildings
                    }

                    XmlNodeList orderInfo = reqElem.GetElementsByTagName("Order_Info");
                    foreach (XmlNode info in orderInfo)
                    {
                        IR.OrderReference = testNode(reqElem, "OrderReferance");
                        IR.InspectionType = reqElem.GetElementsByTagName("InspectionType")[0].ChildNodes[0].InnerText;
                        IR.Valuation = reqElem.GetElementsByTagName("SupplementalFormRequiredBuildingValuationSupplement")[0].ChildNodes[0].InnerText;
                    }

                    ImportRequests oAPI = new ImportRequests();
                    oAPI.CustomerUserName = "APIProd";
                    oAPI.CustomerPassword = "Sutton2012";
                    oAPI.CustomerAccount = "7286";

                    // standard values provided
                    oAPI.InsuredName = IR.InsuredName;
                    oAPI.ContactName = IR.ContactName;
                    oAPI.ContactPhoneWork = "";
                    oAPI.ContactPhoneCell = "";
                    oAPI.ContactPhoneHome = IR.ContactPhoneHome;
                    oAPI.PolicyNumber = IR.PolicyNumber;
                    oAPI.EffectiveDate = IR.EffectiveDate;
                    oAPI.CoverageA = "";
                    oAPI.LocationAddress1 = IR.LocationAddress1;
                    oAPI.LocationAddress2 = IR.LocationAddress2;
                    oAPI.LocationCity = IR.LocationCity;
                    oAPI.LocationState = IR.LocationState;
                    oAPI.LocationZip = IR.LocationZip;
                    oAPI.MailAddress1 = IR.LocationAddress1;
                    oAPI.MailAddress2 = IR.LocationAddress2;
                    oAPI.MailCity = IR.LocationCity;
                    oAPI.MailState = IR.LocationState;
                    oAPI.MailZip = IR.LocationZip;
                    oAPI.InsuranceCompany = IR.InsuranceCompany;
                    oAPI.AgencyAgentName = IR.AgencyAgentName;
                    oAPI.AgencyAgentPhone = IR.AgencyAgentPhone;
                    oAPI.AgencyAgentContact = "";
                    oAPI.AgencyAgentEmail = IR.AgencyAgentEmail;
                    oAPI.AgentAddress1 = "5656 Central Ave";
                    oAPI.AgentAddress2 = "";
                    oAPI.AgentCity = "St. Petersburg";
                    oAPI.AgentState = "FL";
                    oAPI.AgentZip = "33707";

                    oAPI.YearBuilt = IR.YearBuilt;
                    oAPI.Construction = IR.Construction;

                    oAPI.Producer = "";
                    oAPI.Underwriter = "";
                    oAPI.UnderwriterFirstName = "";
                    oAPI.UnderwriterLastName = "";
                    //oAPI.UnderwriterPhone = IR.UnderwriterPhone;
                    oAPI.UnderwriterCorrEmail = "";

                    // standard values not provided
                    oAPI.EmailConfirmation = "";
                    oAPI.RushHandling = "N";

                    // Generic fields
                    oAPI.Occupancy = IR.Occupancy;
                    oAPI.BuildingCost = IR.BuildingCost;
                    oAPI.ContentsCost = IR.ContentsCost;
                    oAPI.InsuranceCompany = IR.InsuranceCompany;
                    oAPI.ImageRightFileNum = IR.OrderReference;

                    oAPI.AdditionalFeeAddOns = "";
                    if (IR.Valuation.ToUpper() == "YES")
                    {
                        oAPI.AdditionalFeeAddOns = "Valuation";
                    }

                    // inspection type
                    string sInspType = IR.InspectionType.ToUpper();
                    if (sInspType.Length < 1)
                    {
                        throw new Exception("No Inspection Type - File name: " + sXMLFileName);
                    }

                    oAPI.InspectionType = "7286-" + sInspType;

                    // comments
                    //oAPI.Comments = "Construction: " + IR.Construction + "\r\n" + IR.Comments;
                    oAPI.Comments = IR.Comments;

                    oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + IR.PolicyNumber);
                    sbEmail.Append("Importing Policy# " + IR.PolicyNumber + System.Environment.NewLine);

                    string sRet = oAPI.ImportCapacity();

                    oLU.WritetoLog("oAPI.Import return for for Policy# " + IR.PolicyNumber + "\r\n\r\n" + sRet);

                    var importResults = sRet.FromJSON<List<ImportResult>>();

                    foreach (var importResult in importResults)
                    {

                        if (importResult.Successful)
                        {
                            oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                            sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + System.Environment.NewLine);
                            bImportSuccess = true;
                        }
                        else
                        {
                            oLU.WritetoLog("**** Import failed **** ");
                            sbEmail.Append("**** Import failed ****" + System.Environment.NewLine);

                            if (importResult.Errors != null)
                            {
                                foreach (var error in importResult.Errors)
                                {
                                    oLU.WritetoLog("Error: " + error.ErrorText);
                                    sbEmail.Append("Error: " + error.ErrorText + System.Environment.NewLine);
                                }
                            }
                        }
                    }

                }   // foreach node



            }   //try

            catch (Exception ex)
            {
                bImportSuccess = false;
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail("Exception logged" + System.Environment.NewLine + ex.Message);
            }

            finally
            {


                // Copy source file to holding area and delete temp
                string sSourceName = cfg_sourcedir + sXMLFileName;
                string sDestName = "";

                //if successful - copy to sibidata\Bass\Processed
                //if failed - copy to sibidata\Bass\fail
                if (bImportSuccess)
                {
                    sDestName = cfg_compdir + sXMLFileName;
                }
                else
                {
                    sDestName = cfg_faildir + sXMLFileName;
                }

                // only copy if data present
                if (iNodes == 0)
                {
                    oLU.WritetoLog("Nothing to import");
                }
                else
                {

                    sendLogEmail(sbEmail.ToString());

                    File.Copy(sSourceName, sDestName);

                    if (!File.Exists(sDestName))
                    {
                        throw new ApplicationException("Copy failed for: " + sXMLFileName);
                    }
                }

                File.Delete(sSourceName);

            }

        }

        static string testNode(XmlElement elem, string sTagName)
        {

            string sRetVal = "";

            //IR.BuildingCost = reqElem.GetElementsByTagName("BuildingLimit")[0].ChildNodes[0].InnerText;

            try
            {
                var testNode = elem.GetElementsByTagName(sTagName)[0].ChildNodes[0];
                if (testNode != null)
                    sRetVal = testNode.InnerText;
            }
            catch (Exception ex)
            {
                // skip missing element
            }

            return sRetVal;
        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import Capacity Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import Capacity Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static bool IsZipCode(string zipCode)
        {
            bool bRet = false;

            string pattern = @"^\d{5}(?:[-\s]\d{4})?$";
            Regex regex = new Regex(pattern);

            bRet = regex.IsMatch(zipCode);

            return bRet;
        }
        static void buildConfHeader()
        {
            // begin header
            sbConfEmail.Append("<html><head><title>Sutton Inspection Bureau, Inc. of Florida</title>" + System.Environment.NewLine);

            // Style
            sbConfEmail.Append("<style type='text/css'>" + System.Environment.NewLine);
            sbConfEmail.Append(".stdText { FONT-SIZE: 8pt; COLOR: black;Font-FAMILY: Verdana,Tahoma,Arial}" + System.Environment.NewLine);

            //end header
            sbConfEmail.Append("</style></head><body>" + System.Environment.NewLine);

            // begin body
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'>" + System.Environment.NewLine);
            sbConfEmail.Append("<tr><td width='10'>&nbsp;</td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='left' width='350'><span class='stdText'>Sutton Inspection Bureau, Inc. of Florida</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='right' width='300'><span class='stdText'>&nbsp;</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("</tr><tr><td>&nbsp;</td>");
            sbConfEmail.Append("<td align='left'><span class='stdText'>Inspection Request Confirmation</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='right'><span class='stdText'>" + DateTime.Now.ToString() + "</span></td></tr></table>" + System.Environment.NewLine);

            // HR
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='0' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650'><hr align='left' width='650'></td></tr></table>" + System.Environment.NewLine);

            // Start table
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'>");

            // Header row
            sbConfEmail.Append("<tr><td align='center' width='100' class='stdText'>Case #</td>");
            sbConfEmail.Append("<td align='left' width='100' class='stdText'>Policy number</td>");
            sbConfEmail.Append("<td align='center' width='460' class='stdText'>Insured / Location</td>");
        }

        static void buildConfLine(string sCaseNum, string sPolicy, string sInsured, string sLocation)
        {
            sbConfEmail.Append("<tr><td align='center' width='100' class='stdText'>" + sCaseNum + "</td>");
            sbConfEmail.Append("<td align='left' width='100' class='stdText'> &nbsp;" + sPolicy + "</td>");
            sbConfEmail.Append("<td align='left' width='460' class='stdText'> &nbsp;" + sInsured + "</td>");
            sbConfEmail.Append("<tr><td class='stdText'> &nbsp;</td><td class='stdText'> &nbsp;</td><td align='left' class='stdText'>" + sLocation + "</td></tr>");
        }

        static void buildConfFooter()
        {
            sbConfEmail.Append("</table></form></body><HTML>");
        }

        static void sendConfEmail(string bodytext, string sConfEmail)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "inspections@sibfla.com";
            oMail.MailTo = sConfEmail;
            oMail.MailBCC = "jeff@sibfla.com";
            oMail.MsgSubject = "Inspection request confirmation";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static int getSFTP()
        {

            // return number of files downloaded or -1 on error
            int iNumDownloaded = 0;

            try
            {
                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Ftp,
                    HostName = "transfer.macneillgroup.com",
                    UserName = "sib",
                    Password = "S8Nq_h",
                };

                using (Session session = new Session())
                {

                    session.SessionLogPath = @"c:\automationlogs\Capacity\Import\FTPLogs.txt";

                    // Connect
                    session.Open(sessionOptions);

                    // Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    transferResult = session.GetFiles("Capacity_Order/*", cfg_sourcedir, true, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        oLU.WritetoLog(transfer.FileName + " downloaded");
                        sbEmail.Append("\r\n" + transfer.FileName + " downloaded");
                        iNumDownloaded++;
                    }

                }

                sbEmail.Append("\r\n" + iNumDownloaded.ToString() + " files downloaded\r\n\r\n---- End FTP transfer\r\n");
            }

            catch (Exception ex)
            {
                sbEmail.Append("**** Error in FTP Transfer\r\n\r\n" + ex.Message + "\r\n\r\n");
                iNumDownloaded = -1;
            }

            return iNumDownloaded;
        }

    }
}
