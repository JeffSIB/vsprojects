﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;

namespace ConiferFTPExport
{
    public class Export : LC360Data.Exporter
    {
        #region Properties

        /// <summary>
        /// Company name - Set in AppSettings (i.e. Sutton)
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Customer name - Set in AppSettings (i.e. USAA)
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Customers to export from LC360 - Set in AppSettings (i.e. 0000, 1234, LOOKUPID)
        /// It is a comma separated string within the App.config.
        /// </summary>
        public string CustomersToExport { get; set; }

        /// <summary>
        /// Log directory - Set in AppSettings (i.e. C:\Accounts\Sutton\USAA\Export\Logs\)
        /// </summary>
        public string LogsDir { get; set; }

        /// <summary>
        /// Return form data on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. true)
        /// </summary>
        public string ReturnFormData { get; set; }

        /// <summary>
        /// Return PDF on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. true)
        /// </summary>
        public string ReturnPDF { get; set; }

        /// <summary>
        /// Return raw images on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. false)
        /// </summary>
        public string ReturnRawImages { get; set; }

        /// <summary>
        /// Send emails to specified email address on export of cases in DEBUG mode - Set in AppSettings (i.e. test@inpections.com)
        /// </summary>
        public string EmailSendToDebug { get; set; }

        /// <summary>
        /// Specify the From email address on export of cases in DEBUG mode - Set in AppSettings (i.e. test@inpections.com)
        /// </summary>
        public string EmailFromDebug { get; set; }

        /// <summary>
        /// Send emails to specified email address on export of cases in LIVE/RELEASE mode - Set in AppSettings (i.e. inspections@sibfla.com)
        /// </summary>
        public string EmailSendToLive { get; set; }

        /// <summary>
        /// Specify the From email address on export of cases in LIVE/RELEASE mode - Set in AppSettings (i.e. GA_EDS@usaa.com)
        /// </summary>
        public string EmailFromLive { get; set; }

        /// <summary>
        /// Export filter for completed cases by start date
        /// </summary>
        public string CompletedMinDate { get; set; }

        /// <summary>
        /// PDF directory - Set in AppSettings (i.e. C:\Accounts\Sutton\USAA\Export\Logs\)
        /// </summary>
        public string PDFExportFolder { get; set; }

        /// <summary>
        /// PDF directory - Set in AppSettings (i.e. C:\Accounts\Sutton\USAA\Export\Logs\)
        /// </summary>
        public string PDFExportFolderSFTP { get; set; }

        /// <summary>
        /// SFTP Log directory - Set in AppSettings (i.e. C:\Accounts\Sutton\USAA\Export\Logs\)
        /// </summary>
        public string SFTPLogDir { get; set; }
        
        #endregion


        #region IExporter Members

        /// <summary>
        /// Here we set up the filter which tells the control what cases we want back.
        /// Note: We only pass cases that have not been previously exported.
        /// </summary>
        /// <returns></returns>
        public override LC360Data.CasesSearchCriteria GetFilter()
        {
            // Message displayed for logging.
            Log(new LC360Data.Log() { Decription = "Getting Search Criteria", LogCode = LC360Data.LogCode.JustForProgrammer });

            // This would normally be filtered by a company or case type to get back desired cases.  
            // Most likely with a Date filter as well.
            LC360Data.CasesSearchCriteria criteria = new LC360Data.CasesSearchCriteria();

            // Customer selection criteria - split the comma separated string "CustomersToExport" from App.config into a string array of customer lookup IDs
            criteria.CustomerLookupIDs = CustomersToExport.Split(',');

            string date = CompletedMinDate;
            DateTime startDate = DateTime.Parse(date);
            string sEnddate = "02/13/2014";
            DateTime enddate = DateTime.Parse(sEnddate);

            //Setup date filter
            criteria.DateFilters = new LC360Data.DateFilters();
            criteria.DateFilters.CompletedMin = startDate;
            //criteria.DateFilters.CompletedMax = enddate;



            /// Case Statuses desired
            criteria.CaseStatuses = new int[] { (int)LC360Data.CaseStatuses.Complete };

            return criteria;
        }

        /// <summary>
        /// Name of the exporter
        /// </summary>
        public override string Name
        {
            get
            { return (CompanyName + " " + CustomerName + " " + "Exporter"); }
        }

        #endregion

        /// <summary>
        /// Called by controller once the cases have been retrieved according to the Filter set in GetFilter().
        /// </summary>
        /// <param name="cases"></param>
        /// <returns></returns>
        public override LC360Data.ExportResult ExportCases(List<LC360Data.ExportCaseHolder> cases)
        {
            StringBuilder email = new StringBuilder();
            EmailBuilder eb = new EmailBuilder();
            List<CaseTracker> ct = new List<CaseTracker>();
            //string emailSubject = "";

            string sPDFFileName = "";

            /// Logging
            Log(new LC360Data.Log() { Decription = "Starting " + CustomerName + " Export", LogCode = LC360Data.LogCode.JustForProgrammer });

            /// Logging
            Log(new LC360Data.Log() { Decription = "Received " + cases.Count + " for export", LogCode = LC360Data.LogCode.GeneralMessage });

            
            /// Create List for successfully exported case IDs
            List<Guid> exportedCaseIDs = new List<Guid>();

            foreach(LC360Data.ExportCaseHolder holder in cases)
            {
                //emailSubject = "";

                ///Keep track of Exports success or failure.
                Func<String, bool, CaseTracker> TrackCaseInfo = (description,successful) =>
                    new CaseTracker
                    {
                        Description = description,
                        Successful = successful,
                        PolicyNumber = holder.Case.PolicyNumber ?? "",
                        PolicyHolder = holder.Case.InsuredName ?? "",
                        Street1 = holder.Case.LocationAddress ?? "",
                        City = holder.Case.LocationCity ?? "",
                        StateOrProvince = holder.Case.LocationState ?? "",
                        ZipCode = holder.Case.LocationPostalCode ?? "",
                        InspectionType = holder.Case.CaseType ?? ""
                    };

                Log(new LC360Data.Log() { Decription = "Starting export of Case: " + holder.Case.CaseID, LogCode = LC360Data.LogCode.JustForProgrammer });
                try
                {

                    // Create PDF file name
                    sPDFFileName = PDFExportFolder + holder.Case.PolicyNumber + ".PDF";

                    // If it exists - add digit to end
                    if (File.Exists(sPDFFileName))
                    {

                        int i = 1;
                        while (true)
                        {
                            sPDFFileName = PDFExportFolder + holder.Case.PolicyNumber + "_" + i.ToString() + ".PDF";
                            if (File.Exists(sPDFFileName))
                            {
                                i++;
                                if (i > 10)
                                {
                                    Log(new LC360Data.Log() { Decription = "**** PDF of case " + holder.Case.PolicyNumber + " could not be saved (EXISTS). ****", LogCode = LC360Data.LogCode.Error });
                                    ct.Add(TrackCaseInfo("Error exporting case for Policy #: " + holder.Case.PolicyNumber + " Case ID: "+ holder.Case.CaseID + ".", false));
                                }
   
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    // Save PDF to hold dir
                    byte[] file = holder.PDFFile;
                    if (ByteArrayToFile(sPDFFileName, file))
                    {
                        Log(new LC360Data.Log() { Decription = "PDF of case Policy #: " + holder.Case.PolicyNumber + " Case ID: " + holder.Case.CaseID + " successfully saved.", LogCode = LC360Data.LogCode.GeneralMessage });
                    }
                    else
                    {
                        Log(new LC360Data.Log() { Decription = "PDF of case Policy #: " + holder.Case.PolicyNumber + " Case ID: " + holder.Case.CaseID + " could not be saved. ****", LogCode = LC360Data.LogCode.Error });
                        ct.Add(TrackCaseInfo("Error exporting case for Policy #: " + holder.Case.PolicyNumber + "  " + holder.Case.InsuredName + ".", false));
                    }

#if DEBUG
                    /// Use these settings if in Debug Mode
                    string FromEmailAddress = EmailFromDebug;
                    string SendToEmail = EmailSendToDebug;
#else
                    /// Use these settings if in Release Mode
                    string FromEmailAddress = EmailFromLive;
                    string SendToEmail = EmailSendToLive;
#endif
                        
                    /// Add this caseID to list that will be passed back to calling program
                    exportedCaseIDs.Add(holder.Case.CaseID);

                    ct.Add(TrackCaseInfo("Success exporting case for: " + holder.Case.InsuredName + ".", true));
                    
                }
                catch(Exception ex)
                {
                    Log(new LC360Data.Log() { Decription = "**** Error exporting case: " + holder.Case.CaseID + ex.Message, LogCode = LC360Data.LogCode.IssueWithPlugin });
                    ct.Add(TrackCaseInfo("Error exporting case for: " + holder.Case.InsuredName + ".", false));
                }

            /// End of ForEach holder in cases
            }

            /// SFTP Files
            Log(new LC360Data.Log() { Decription = "Starting SFTP", LogCode = LC360Data.LogCode.JustForProgrammer });
            
            bool bSFTP = SFTPFiles();
            //bool bSFTP = true;
            if (bSFTP)
            {
                Log(new LC360Data.Log() { Decription = "SFTP upload successful.", LogCode = LC360Data.LogCode.Successful });
            }
            else
            {
                Log(new LC360Data.Log() { Decription = "**** SFTP upload failed. ****", LogCode = LC360Data.LogCode.Error });
            }

            ///Create and send success/failure email
            eb.CreateEmail(ct, cases);

            /// Arrange to pass back 'Export Complete' message
            Log(new LC360Data.Log() { Decription = "Export Complete", LogCode = LC360Data.LogCode.Successful });
            
            /// Set up results to be returned to the calling program
            LC360Data.ExportResult result = new LC360Data.ExportResult();

             
            /// Mark cases as exported
            if (bSFTP)
            {
                result.ExportedCaseIDs = exportedCaseIDs;
            }
            return result;
        }


        /// <summary>
        /// Function to save byte array to a file
        /// </summary>
        /// <param name="sFileName">File name to save byte array</param>
        /// <param name="_ByteArray">Byte array to save to external file</param>
        /// <returns>Return true if byte array save successfully, if not return false</returns>
        public bool ByteArrayToFile(string sFileName, byte[] _ByteArray)
        {
            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream = new System.IO.FileStream(sFileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                // Writes a block of bytes to this stream using data from a byte array.
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);

                // close file stream
                _FileStream.Close();

                return true;
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());

            }

            // error occured, return false
            return false;
        }



        /// <summary>
        /// Send a given message using the current default SMTP Client 
        /// </summary>
        /// <param name="message"></param>
        public static void SendEmailMessage(MailMessage message)
        {
            SmtpClient client = new SmtpClient();
            client.Send(message);
        }


        /// <summary>
        /// Called by controller to get export options for the retrieval of case information
        /// These options are set using AppSettings located in App.config
        /// </summary>
        /// <returns></returns>
        public override LC360Data.ExportOptions GetExportOptions()
        {
            LC360Data.ExportOptions options = new LC360Data.ExportOptions();
            options.ReturnFormData = Convert.ToBoolean(ReturnFormData);
            options.ReturnPDF = Convert.ToBoolean(ReturnPDF);
            options.ReturnRawImages = Convert.ToBoolean(ReturnRawImages);
            return options;
        }

        /// <summary>
        /// Guid ID for this particular application.
        /// </summary>
        public override Guid ApplicationID
        {
            get { return new Guid("A38B93EF-FE6D-43CB-A644-BC69CC8D3235"); }
        }

        /// <summary>
        /// SFTP Files to MacNeill
        /// </summary>
        public bool SFTPFiles()
        {

            bool bRet = true;

            try
            {
                int iNumUploaded = 0;

                string sLogDate = DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Second.ToString();
                //string sLogName = "C:\\temp\\ConiferFTPExport\\SFTPLog_" + sLogDate + ".xml";
                //string sLogName = "J:\\automationlogs\\ConiferFTPExport\\SFTPLogs\\SFTPLog_" + sLogDate + ".xml";
                string sLogName = "\\\\nas\\apps\\automationlogs\\ConiferFTPExport\\SFTPLogs\\SFTPLog_" + sLogDate + ".xml";
                // Run WinSCP process
                Process winscp = new Process();
                winscp.StartInfo.FileName = "winscp.com";
                winscp.StartInfo.Arguments = "/log=\"" + sLogName + "\"";
                winscp.StartInfo.UseShellExecute = false;
                winscp.StartInfo.RedirectStandardInput = true;
                winscp.StartInfo.RedirectStandardOutput = true;
                winscp.StartInfo.CreateNoWindow = true;
                winscp.Start();

                winscp.StandardInput.WriteLine("option batch abort");
                winscp.StandardInput.WriteLine("option confirm off");
                winscp.StandardInput.WriteLine("open MacNeill_FTP");
                winscp.StandardInput.WriteLine("ls");


                // SFTP all PDF files in PDFExportFolder
                DirectoryInfo diSource = new DirectoryInfo(PDFExportFolder);
                FileInfo[] fiSource = diSource.GetFiles("*.PDF");

                Log(new LC360Data.Log() { Decription = "Start SFTP PUT", LogCode = LC360Data.LogCode.JustForProgrammer });

                foreach (FileInfo fiSourceFile in fiSource)
                {
                    winscp.StandardInput.WriteLine("put " + PDFExportFolderSFTP +  fiSourceFile.Name);
                    iNumUploaded++;
                }

                Log(new LC360Data.Log() { Decription = "End SFTP PUT. Files uploaded = " + iNumUploaded.ToString() , LogCode = LC360Data.LogCode.JustForProgrammer });

                diSource = null;
                fiSource = null;
                                
                winscp.StandardInput.Close();
                
                // Collect all output
                string output = winscp.StandardOutput.ReadToEnd();

                // Wait until WinSCP finishes
                winscp.WaitForExit();

                Log(new LC360Data.Log() { Decription = "SFTP Waitfor Reached ", LogCode = LC360Data.LogCode.JustForProgrammer });
                
                // Parse and interpret the XML log
                // (Note that in case of fatal failure the log file may not exist at all)
                XPathDocument log = new XPathDocument(sLogName);
                XmlNamespaceManager ns = new XmlNamespaceManager(new NameTable());
                ns.AddNamespace("w", "http://winscp.net/schema/session/1.0");
                XPathNavigator nav = log.CreateNavigator();

                // Success (0) or error?
                if (winscp.ExitCode != 0)
                {
                    Console.WriteLine("Error in SFTP upload: winscp.ExitCode=" + winscp.ExitCode.ToString());
                    Log(new LC360Data.Log() { Decription = "Error in SFTP upload: winscp.ExitCode=" + winscp.ExitCode.ToString(), LogCode = LC360Data.LogCode.Error });

                    bRet = false;

                    // See if there are any messages associated with the error
                    foreach (XPathNavigator message in nav.Select("//w:message", ns))
                    {
                        Console.WriteLine(message.Value);
                        Log(new LC360Data.Log() { Decription = "SFTP Error: " + message.Value, LogCode = LC360Data.LogCode.JustForProgrammer });

                    }
                }
                else
                {
                    // It can be worth looking for directory listing even in case of
                    // error as possibly only upload may fail

                    XPathNodeIterator files = nav.Select("//w:file", ns);
                    Console.WriteLine(string.Format("There are {0} files and subdirectories:", files.Count));
                    foreach (XPathNavigator file in files)
                    {
                        Console.WriteLine(file.SelectSingleNode("w:filename/@value", ns).Value);
                    }
                }

            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Error in SFTP upload: " + _Exception.Message);
                Log(new LC360Data.Log() { Decription = "Error in SFTP upload: " + _Exception.Message, LogCode = LC360Data.LogCode.Error });
                bRet = false;
            }

            return bRet;
        }
    }

}
