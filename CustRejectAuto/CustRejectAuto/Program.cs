﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace CustRejectAuto
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static string cfg_delivertoW;
        static string cfg_delivertoM;
        static int cfg_TimeZoneOffset;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Range oRange = null;
        static DateTime dBegDate;
        static DateTime dEndDate;
        static DateTime dBegDate360;
        static DateTime dEndDate360;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;
        static string sMode;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_delivertoW = colNameVal.Get("delivertoW");
                cfg_delivertoM = colNameVal.Get("delivertoM");
                cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //  D = daily - Andrea
                //  W = weekly
                //  M = monthly
                if (args.Length < 1)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                if (args[0].ToUpper() == "D")
                {
                    // ** Daily **
                    // Results are for previous Sun-Sat
                    sMode = "D";

                    DateTime dt = new DateTime();
                    dt = DateTime.Today;

                    // Set beg date to previous day
                    dBegDate = dt.AddDays(-1);
                    dEndDate = dt;
                    //dBegDate = new DateTime(2020, 11, 3);
                    //dEndDate = new DateTime(2020, 11, 4);


                }
                else if (args[0].ToUpper() == "W")
                {
                    // ** Weekly **
                    // Assumes that it is being run on Monday
                    // Results are for previous Sun-Sat
                    sMode = "W";

                    DateTime dt = new DateTime();
                    dt = DateTime.Today;

                    // Set beg date to previous Sunday - Sat
                    dBegDate = dt.AddDays(-8);
                    dEndDate = dt.AddDays(-2);

                    //dBegDate = new DateTime(2020, 5, 24);
                    //dEndDate = new DateTime(2020, 5, 30);

                }

                else if (args[0].ToUpper() == "M")
                {

                    ///////////////////////////////////////////
                    // **Monthly**
                    // Runs for previous month
                    sMode = "M";

                    DateTime dt = new DateTime();
                    dt = DateTime.Today;
                    dt = dt.AddMonths(-1);

                    // Set beg date to first/last day of previous month
                    dBegDate = FirstDayOfMonth(dt);
                    dEndDate = LastDayOfMonth(dt);
                    //dBegDate = new DateTime(2022, 1, 1);
                    //dEndDate = new DateTime(2022, 1, 31);

                }
                else
                {
                    throw new SystemException("Invalid period arguments supplied");
                }

                string sDateDisp = dBegDate.Month + "-" + dBegDate.Day + "_" + dEndDate.Month + "-" + dEndDate.Day;
                string sEmailSubject = "Customer Rejects - " + dBegDate.Month + "/" + dBegDate.Day + " - " + dEndDate.Month + "/" + dEndDate.Day + ", " + dBegDate.Year.ToString();
                string sExcelFileNameNoEx = cfg_outputdir + "CustRejects_" + sDateDisp;
                string sExcelFileName = sExcelFileNameNoEx + ".xlsx";

                // set time to 12:00am
                // adjust for 360 GMT
                dBegDate = ChangeTime(dBegDate, 0, 0, 0, 0);
                dBegDate360 = dBegDate.AddHours(cfg_TimeZoneOffset);

                // set time to midnight
                //dEndDate = ChangeTime(dEndDate, 23, 59, 59, 0);
                dEndDate360 = dEndDate.AddHours(cfg_TimeZoneOffset);

                ///////////////////////////////////////////////////////////////


                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                int iCaseNum = 0;
                DateTime dDateRejected;
                DateTime dFirstComp;
                DateTime dLastComp;
                string sFieldRep = "";
                string sReason = "";
                string sCustomer = "";
                string sRejectedBy = "";
                string sCompletedBy = "";
                int iRow = 0;

                ///////////////////////////////////////////////////
                // 360 
                ///////////////////////////////////////////////////

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_CustRejectGet4";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate360);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate360);
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {

                    oExcel = new Excel.Application();
                    oExcel.Visible = true;
                    oWorkbook = oExcel.Workbooks.Add(1);
                    o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    //o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing,oSIBWorksheet,oMissing,oMissing);
                    o360Worksheet.Name = "360";
                    createHeader(o360Worksheet);

                    iRow = 4;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //date rejected
                        dDateRejected = (DateTime)sqlReader.GetSqlDateTime(0);

                        //reason
                        if (sqlReader.IsDBNull(1))
                        {
                            sReason = "";
                        }
                        else
                        {
                            sReason = (string)sqlReader.GetSqlString(1);
                        }

                        //case #
                        if (sqlReader.IsDBNull(2))
                        {
                            iCaseNum = 0;
                        }
                        else
                        {
                            iCaseNum = (int)sqlReader.GetSqlInt32(2);
                        }

                        //inspector
                        if (sqlReader.IsDBNull(3))
                        {
                            sFieldRep = "";
                        }
                        else
                        {
                            sFieldRep = (string)sqlReader.GetSqlString(3);
                        }

                        //customer
                        if (sqlReader.IsDBNull(4))
                        {
                            sCustomer = "";
                        }
                        else
                        {
                            sCustomer = (string)sqlReader.GetSqlString(4);
                        }

                        //rejected by
                        if (sqlReader.IsDBNull(5))
                        {
                            sRejectedBy = "";
                        }
                        else
                        {
                            sRejectedBy = (string)sqlReader.GetSqlString(5);
                        }

                        //first comp
                        dFirstComp = (DateTime)sqlReader.GetSqlDateTime(6);

                        //last comp
                        dLastComp = (DateTime)sqlReader.GetSqlDateTime(7);

                        //Completed by
                        if (sqlReader.IsDBNull(8))
                        {
                            sCompletedBy = "";
                        }
                        else
                        {
                            sCompletedBy = (string)sqlReader.GetSqlString(8);
                        }

                        addData(o360Worksheet, iRow, 1, iCaseNum.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 2, dDateRejected.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "MM/DD/YYYY", "C");
                        addData(o360Worksheet, iRow, 3, sRejectedBy.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 4, sCustomer.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 5, sFieldRep.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 6, dFirstComp.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "MM/DD/YYYY", "C");
                        addData(o360Worksheet, iRow, 7, dLastComp.ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "MM/DD/YYYY", "C");
                        addData(o360Worksheet, iRow, 8, sCompletedBy.ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 9, sReason.ToString(), "I" + iRow.ToString(), "I" + iRow.ToString(), "", "");
                        iRow++;

                    } while (sqlReader.Read());     // 360

                    sqlReader.Close();
                    sqlConn1.Close();
                    sqlConn1 = null;
                    sqlCmd1 = null;

                    //oWorkbook.SaveAs(cfg_outputdir + "ReviewerStats360_" + sDateDisp, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                    oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                    oWorkbook.Close(true, oMissing, oMissing);
                    oExcel.Quit();
                    msMsgBody += "Processing completed for 360" + System.Environment.NewLine + cfg_outputdir + "ReviewerStatsSIB_" + sDateDisp + System.Environment.NewLine + System.Environment.NewLine;

                    releaseObject(oExcel);
                    releaseObject(oWorkbook);
                    releaseObject(o360Worksheet);
                    sendExcelFile(sEmailSubject, sExcelFileName);
                    msMsgBody += "File sent " + sExcelFileName;

                }   // has rows
                else
                {
                    // no data
                    sendNoDataEmail(sEmailSubject);
                    oLU.WritetoLog("No data");
                }

            }   

            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    //sendEmail(msMsgBody);                
                }

            }
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeader(Excel._Worksheet oWorkSheet)
        {

            Excel.Range oRange;


            oWorkSheet.get_Range("A1", "I1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "I1");
            oRange.FormulaR1C1 = "Customer Rejects - 360 " + dBegDate.ToShortDateString() + " - " + dEndDate.ToShortDateString();

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Case #";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Date Rejected";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "Rejected By";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Customer";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Field Rep";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 6] = "First Comp";
            oRange = oWorkSheet.get_Range("F3", "F3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 7] = "Last Comp";
            oRange = oWorkSheet.get_Range("G3", "G3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 8] = "Completed By";
            oRange = oWorkSheet.get_Range("H3", "H3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 9] = "Reason";
            oRange = oWorkSheet.get_Range("I3", "I3");
            oRange.ColumnWidth = 185;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        }




        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2, string format, string sHorizAlign)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }



        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            //oMail.MailTo = "jeff@sibfla.com";

            oMail.MsgSubject = "Customer Rejects";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by CustRejects Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }
        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                if (sMode == "W")   //weekly
                {
                    oMail.MailTo = cfg_delivertoW;
                }
                else if (sMode == "M")  // monthly
                {
                    oMail.MailTo = cfg_delivertoM;
                }
                else  // daily
                {
                    oMail.MailTo = "andrea@sibfla.com;denise@sibfla.com";
                }

                //oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }

        static void sendNoDataEmail(string sSubject)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "No Customer Rejects for the period." + System.Environment.NewLine;

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                if (sMode == "W")   //weekly
                {
                    oMail.MailTo = cfg_delivertoW;
                }
                else if (sMode == "M")  // monthly
                {
                    oMail.MailTo = cfg_delivertoM;
                }
                else  // daily
                {
                    oMail.MailTo = "andrea@sibfla.com;denise@sibfla.com";
                }

                //oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }


    }
}
