﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;
using System.Data.SqlClient;
using System.Data;


namespace importAMWins
{

    /// <summary>
    /// Must reside on H:\VS2010\importAMWins\importAMWins\bin\Release
    /// </summary>
    /// 

    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyNumber { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string AgencyAgentContact { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            //public string BusinessOperations { get; set; }
            public string LocationFullAddress { get; set; }
            public string LocationHouseNumber { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string CoverageB { get; set; }
            public string CoverageC { get; set; }
            public string CoverageD { get; set; }
            public string CoverageE { get; set; }
            public string CoverageF { get; set; }
            public string ISOClass { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }

            public string Construction { get; set; }
            public string YearBuilt { get; set; }
            public string DwellingType { get; set; }
            public string NumberOfFloors { get; set; }
            public string Occupancy { get; set; }
            public string SquareFootage { get; set; }
            public string DistanceToCoast { get; set; }
            public string RoofYear { get; set; }
            public string RoofMaterials { get; set; }
            public string RoofGeometry { get; set; }
            public string RoofType { get; set; }
            public string Usage { get; set; }
            public string WindMitigation { get; set; }
            public string PCClass { get; set; }
            public string RoofCladding { get; set; }
            public string RoofCondition { get; set; }
            public string BuildingCladding { get; set; }
            public string CompanyPolicyForm { get; set; }
            public string CentralFireAlarm { get; set; }
            public string CentralBurglarAlarm { get; set; }
            public string CentralAlarm { get; set; }
            public string PolicyStatus { get; set; }
            public string PoolFenced { get; set; }
            public string Heating { get; set; }
            public string HeatingFullPartial { get; set; }
            public string HeatingUpdate { get; set; }
            public string HeatingYear { get; set; }
            public string PoolAboveGround { get; set; }
            public string Farming { get; set; }
            public string NumberOfFamilies { get; set; }
            public string Pets { get; set; }
            public string Plumbing { get; set; }
            public string PlumbingFullPartial { get; set; }
            public string PlumbingUpdate { get; set; }
            public string PlumbingYear { get; set; }
            public string PoolSlide { get; set; }
            public string PoolDivingBoard { get; set; }
            public string PoolScreenEncl { get; set; }
            public string Pool { get; set; }
            public string Updates { get; set; }
            public string Wiring { get; set; }
            public string WiringFullPartial { get; set; }
            public string WiringUpdate { get; set; }
            public string WiringYear { get; set; }

            public string AgentEmail { get; set; }
            public string AltContactName { get; set; }
            public string AltContactPhone { get; set; }
            public string AltContactMPhone { get; set; }
            public string AltContactEmail { get; set; }

            // AMWins
            public string AutomaticWaterShutOff { get; set; }
            public string BrushWildfireZone { get; set; }
            public string HurricaneRoofProtectionToCode { get; set; }
            public string HurricaneProtection { get; set; }
            public string Farm { get; set; }
            public string MobileHomes { get; set; }
            public string DivingBoardNotToCode { get; set; }
            public string ExistingDamage { get; set; }
            public string DwellingsNotInsured100PctOfRCV { get; set; }
            public string KnobTubeWiring { get; set; }
            public string NationalRegistry { get; set; }
            public string EIFSOlderThan1998 { get; set; }
            public string WoodStovePrimaryHeat { get; set; }
            public string DaycareOrAssistedLiving { get; set; }
            public string Over10Acres { get; set; }
            public string RisksInForeclosureProceedings { get; set; }
            public string DeveloperSpecHomes { get; set; }
            public string FloodZone { get; set; }
            public string EffectiveYearBuilt { get; set; }


        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        //static string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
        static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];


        static LogUtils.LogUtils oLU;
        static bool bErr = false;

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {

            string sExcelFileName = "";
            string sPolNum = "";
            bool bImportSuccess = false;

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                if (args.Length == 0)
                {
                    throw new ApplicationException("No arguments passed");
                }

                // get excel file name from command line
                sExcelFileName = args[0];

                // does file exist?
                FileInfo fi = new FileInfo(cfg_sourcedir + sExcelFileName);
                if (!fi.Exists)
                {
                    // send email
                    throw new ApplicationException("File does not exist: " + sExcelFileName);
                }

                // get policy # from file name
                sPolNum = Path.GetFileNameWithoutExtension(fi.Name);

            }

            catch (Exception ex)
            {
                oLU.closeLog();
                sendErrEmail("Error initializing AMWins\r\n\r\n" + ex.Message);
                return;
            }


            try
            {

                oLU.WritetoLog("Processing: " + sExcelFileName);
                sbEmail.Append(System.Environment.NewLine + "Processing: " + sExcelFileName + System.Environment.NewLine);

                var excel = new ExcelQueryFactory(cfg_sourcedir + sExcelFileName);

                var columnnames = excel.GetColumnNames("Inspection Request");
                //excel.AddMapping<ImportRecord>(x => x.LocationHouseNumber, columnnames[15]);

                excel.AddMapping<ImportRecord>(x => x.InsuredName, "Applicant Name");
                excel.AddMapping<ImportRecord>(x => x.LocationContactName, "Name of Inspection Contact");
                excel.AddMapping<ImportRecord>(x => x.LocationContactPhone, "Phone Number of Inspection Contact");
                //excel.AddMapping<ImportRecord>(x => x.ContactPhoneWork, "Policy # - Customer Phone Business");
                //excel.AddMapping<ImportRecord>(x => x.ContactPhoneHome, "Policy # - Customer Phone Residence");
                //excel.AddMapping<ImportRecord>(x => x.PolicyNumber, "Policy #");

                // confirmation email and underwriter email
                excel.AddMapping<ImportRecord>(x => x.EmailConfirmation, "To Be Reviewed By");

                excel.AddMapping<ImportRecord>(x => x.EffectiveDate, "Effective Date");
                excel.AddMapping<ImportRecord>(x => x.CoverageA, "Dwelling");
                excel.AddMapping<ImportRecord>(x => x.CoverageB, "Other Structures");
                excel.AddMapping<ImportRecord>(x => x.LocationHouseNumber, "Building / House No#");
                excel.AddMapping<ImportRecord>(x => x.LocationAddress1, "Risk Street");
                excel.AddMapping<ImportRecord>(x => x.LocationCity, "Risk City");
                excel.AddMapping<ImportRecord>(x => x.LocationState, "Risk State");
                excel.AddMapping<ImportRecord>(x => x.LocationZip, "Risk Zip Code");
                //excel.AddMapping<ImportRecord>(x => x.InsuranceCompany, "Policy Company");
                excel.AddMapping<ImportRecord>(x => x.Construction, "Construction Type");
                excel.AddMapping<ImportRecord>(x => x.YearBuilt, "Original Year Built");   // Col AG
                excel.AddMapping<ImportRecord>(x => x.EffectiveYearBuilt, "Effective Year Built (renovated \"to studs\")");
                //excel.AddMapping<ImportRecord>(x => x.DwellingType, "Dwelling Type");
                excel.AddMapping<ImportRecord>(x => x.NumberOfFloors, "Number of Stories");
                excel.AddMapping<ImportRecord>(x => x.Occupancy, "Occupancy");
                excel.AddMapping<ImportRecord>(x => x.SquareFootage, "Square Feet");
                excel.AddMapping<ImportRecord>(x => x.InspectionType, "Type of Inspection Ordered");
                excel.AddMapping<ImportRecord>(x => x.Occupancy, "Occupancy");
                excel.AddMapping<ImportRecord>(x => x.FloodZone, "Zone");
                excel.AddMapping<ImportRecord>(x => x.DistanceToCoast, "Distance to Salt Water (miles)");
                excel.AddMapping<ImportRecord>(x => x.RoofYear, "Roof Age / Fully Replaced");
                excel.AddMapping<ImportRecord>(x => x.RoofMaterials, "Roof Material");
                excel.AddMapping<ImportRecord>(x => x.RoofType, "Roof Type");
                excel.AddMapping<ImportRecord>(x => x.RoofGeometry, "Roof Shape");
                excel.AddMapping<ImportRecord>(x => x.PCClass, "Protection Class");

                excel.AddMapping<ImportRecord>(x => x.HurricaneRoofProtectionToCode, "Hurricane Protection - Roof to Latest Code?");
                excel.AddMapping<ImportRecord>(x => x.HurricaneProtection, "Hurricane Protection - Approved Opening Protection - All?");

                excel.AddMapping<ImportRecord>(x => x.AutomaticWaterShutOff, "Automatic Water Shut off system ?");
                excel.AddMapping<ImportRecord>(x => x.BrushWildfireZone, "Brush / Wildfire Zone");
                excel.AddMapping<ImportRecord>(x => x.CentralBurglarAlarm, "Burglar Alarm ?");
                excel.AddMapping<ImportRecord>(x => x.CentralFireAlarm, "Monitored by Central Station ?");
                excel.AddMapping<ImportRecord>(x => x.Farm, "Farms?");
                excel.AddMapping<ImportRecord>(x => x.MobileHomes, "Mobile Homes?");
                excel.AddMapping<ImportRecord>(x => x.DivingBoardNotToCode, "Homes with pools and/or diving boards that do not meet local cod");
                excel.AddMapping<ImportRecord>(x => x.ExistingDamage, "Property with existing damage?");
                excel.AddMapping<ImportRecord>(x => x.DwellingsNotInsured100PctOfRCV, "Dwellings not insured to 100% of RCV value?");
                excel.AddMapping<ImportRecord>(x => x.KnobTubeWiring, "Dwellings with knob and tube wiring?");
                excel.AddMapping<ImportRecord>(x => x.NationalRegistry, "Dwellings on the National Registry?");
                excel.AddMapping<ImportRecord>(x => x.EIFSOlderThan1998, "Dwellings with EIFS siding older than 1998?");
                excel.AddMapping<ImportRecord>(x => x.WoodStovePrimaryHeat, "Dwellings with wood stoves as primary heat?");
                excel.AddMapping<ImportRecord>(x => x.DaycareOrAssistedLiving, "Dwellings with daycare or assisted living operations?");
                excel.AddMapping<ImportRecord>(x => x.Over10Acres, "Risks with acreage over 10?");
                excel.AddMapping<ImportRecord>(x => x.RisksInForeclosureProceedings, "Risks in foreclosure proceedings?");
                excel.AddMapping<ImportRecord>(x => x.DeveloperSpecHomes, "Developers speculation homes?");

                excel.AddMapping<ImportRecord>(x => x.AgencyAgentName, "Agent");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentContact, "Agent Contact Person");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentPhone, "Agent Contact Phone#");
                excel.AddMapping<ImportRecord>(x => x.InsuranceCompany, "Carrier");

                var rows = (from row in excel.Worksheet<ImportRecord>(0) select row).ToList();

                foreach (var row in rows)
                {

                    // skip row if Insured Name is empty
                    if (row.InsuredName.Length > 0)
                    {

                        ImportRequests oAPI = new ImportRequests();
                        oAPI.CustomerUserName = "APIProd";
                        oAPI.CustomerPassword = "Sutton2012";
                        oAPI.CustomerAccount = "7301";

                        // standard values provided
                        oAPI.InsuredName = row.InsuredName;
                        oAPI.LocationContactPhone = row.LocationContactPhone;
                        oAPI.LocationContactName = row.LocationContactName;
                        oAPI.ContactPhoneWork = "";
                        oAPI.ContactPhoneCell = "";
                        oAPI.ContactPhoneHome = "";
                        oAPI.PolicyNumber = sPolNum;
                        oAPI.EffectiveDate = row.EffectiveDate;
                        oAPI.CoverageA = row.CoverageA;
                        oAPI.CoverageB = row.CoverageB;
                        oAPI.LocationAddress1 = row.LocationHouseNumber + " " + row.LocationAddress1;
                        oAPI.LocationAddress2 = "";
                        oAPI.LocationCity = row.LocationCity;
                        oAPI.LocationState = row.LocationState;
                        oAPI.LocationZip = row.LocationZip;
                        oAPI.MailAddress1 = row.LocationHouseNumber + " " + row.LocationAddress1;
                        oAPI.MailAddress2 = "";
                        oAPI.MailCity = row.LocationCity;
                        oAPI.MailState = row.LocationState;
                        oAPI.MailZip = row.LocationZip;
                        oAPI.InsuranceCompany = row.InsuranceCompany ?? "Not Provided";

                        // Agency / Agent
                        oAPI.AgencyAgentName = row.AgencyAgentName ?? "Not Provided";
                        oAPI.AgencyAgentContact = row.AgencyAgentContact ?? "Not Provided";
                        oAPI.AgencyAgentPhone = row.AgencyAgentPhone ?? "Not Provided";
                        oAPI.AgentCode = "";
                        oAPI.AgentFax = "";
                        oAPI.AgentAddress1 = "5656 Central Ave";
                        oAPI.AgentAddress2 = "";
                        oAPI.AgentCity = "St. Petersburg";
                        oAPI.AgentState = "FL";
                        oAPI.AgentZip = "33707";
                        oAPI.AgentEmail = "CustomerServices@sibfla.com";

                        // look up underwriter by email
                        // abort on fail as we must have underwriter hooked up for delivery
                        oAPI.EmailConfirmation = row.EmailConfirmation.Trim();
                        if (oAPI.EmailConfirmation.Length < 1)
                        {
                            throw new Exception("No underwriter email provided");
                        }

                        if (!GetUnderwriter(oAPI, sbEmail))
                            {
                                throw new Exception("Unable to match up underwriter for email: " + oAPI.EmailConfirmation);
                            }

                        oAPI.RushHandling = "N";
                        oAPI.ContactName = "";

                        // Convert CoverageA to numeric
                        double dCovA = 0;
                        if (IsNumeric(row.CoverageA))
                        {
                            dCovA = Convert.ToDouble(row.CoverageA);
                        }

                        // Convert year built to numeric
                        //int iYearBuilt = Convert.ToInt32(row.YearBuilt);
                        //int iYearsOld = 0;
                        //if (iYearBuilt > 0)
                        //{
                        //    iYearsOld = DateTime.Now.Year - iYearBuilt;
                        //}


                        // inspection type
                        string sInspType = "";
                        string sInspTypeDesc = "";
                        //bool bUpdate = false;

                        // Inspection type
                        if (row.InspectionType == null)
                            row.InspectionType = "";
                        else
                            row.InspectionType = row.InspectionType.ToUpper();

                        if (row.InspectionType.Contains("MID-VALUE"))
                        {
                            sInspType = "7301-MV";
                            sInspTypeDesc = " Residential Interior & Exterior Mid-Value w/valuation & update";
                        }
                        if (row.InspectionType.Contains("HIGH VALUE"))
                        {
                            sInspType = "7301-HV";
                            sInspTypeDesc = " Residential Interior & Exterior High-Value w/valuation & update";
                        }
                        if (row.InspectionType.Contains("LVEXTERIOR"))
                        {
                            sInspType = "7301-LV";
                            sInspTypeDesc = " Residential Exterior w/valuation & update";
                        }

                        // If inspection type is blank - throw error
                        if (sInspType.Length == 0)
                        {
                            throw new Exception("No Inspection Type - File name: " + sExcelFileName + " - Policy#: " + row.PolicyNumber);
                        }

                        oAPI.InspectionType = sInspType;

                        // comments
                        oAPI.Comments = "";

                        // Custom/Generic field values
                        oAPI.Occupancy = row.Occupancy ?? "Not Provided";
                        oAPI.Stories = row.NumberOfFloors ?? "Not Provided";
                        oAPI.SquareFootage = row.SquareFootage ?? "Not Provided";
                        oAPI.YearBuilt = row.YearBuilt ?? "Not Provided";
                        oAPI.Construction = row.Construction ?? "Not Provided";
                        oAPI.ProtectionClass = row.PCClass ?? "Not Provided";
                        oAPI.RoofType = row.RoofType ?? "Not Provided";
                        oAPI.RoofYear = row.RoofYear ?? "Not Provided";
                        oAPI.RoofGeometry = row.RoofGeometry ?? "Not Provided";
                        oAPI.RoofMaterial = row.RoofMaterials ?? "Not Provided";
                        oAPI.SquareFootage = row.SquareFootage ?? "Not Provided";
                        oAPI.AutomaticWaterShutOff = row.AutomaticWaterShutOff ?? "Not Provided";
                        oAPI.BrushWildfireZone = row.BrushWildfireZone ?? "Not Provided";
                        oAPI.BurglarlAlarmPresent = row.CentralBurglarAlarm ?? "Not Provided";
                        oAPI.CentralStationFireAlarmPresent = row.CentralFireAlarm ?? "Not Provided";
                        oAPI.HurricaneRoofProtectionToCode = row.HurricaneRoofProtectionToCode ?? "Not Provided";
                        oAPI.HurricaneProtection = row.HurricaneProtection ?? "Not Provided";
                        oAPI.MobileHomes = row.MobileHomes ?? "Not Provided";
                        oAPI.DivingBoardNotToCode = row.DivingBoardNotToCode ?? "Not Provided";
                        oAPI.ExistingDamage = row.ExistingDamage ?? "Not Provided";
                        oAPI.DwellingsNotInsured100PctOfRCV = row.DwellingsNotInsured100PctOfRCV ?? "Not Provided";
                        oAPI.KnobTubeWiring = row.KnobTubeWiring ?? "Not Provided";
                        oAPI.NationalRegistry = row.NationalRegistry ?? "Not Provided";
                        oAPI.EIFSOlderThan1998 = row.EIFSOlderThan1998 ?? "Not Provided";
                        oAPI.WoodStovePrimaryHeat = row.WoodStovePrimaryHeat ?? "Not Provided";
                        oAPI.DaycareOrAssistedLiving = row.DaycareOrAssistedLiving ?? "Not Provided";
                        oAPI.Over10Acres = row.Over10Acres ?? "Not Provided";
                        oAPI.RisksInForeclosureProceedings = row.RisksInForeclosureProceedings ?? "Not Provided";
                        oAPI.DeveloperSpecHomes = row.DeveloperSpecHomes ?? "Not Provided";
                        oAPI.Farm = row.Farm ?? "Not Provided";
                        oAPI.CoverageA = row.CoverageA ?? "Not Provided";
                        oAPI.CoverageB = row.CoverageB ?? "Not Provided";
                        oAPI.FloodZone = row.FloodZone ?? "Not Provided";
                        oAPI.EffectiveYearBuilt = row.EffectiveYearBuilt ?? "Not Provided";
                        oAPI.DistanceCoast = row.DistanceToCoast ?? "Not Provided";



                        //// Update
                        ////if (bUpdate)
                        ////{

                        //    oAPI.ElectricalUpdates = row.WiringUpdate ?? "Not Provided";
                        //    oAPI.ElectricalFullPartial = row.WiringFullPartial ?? "Not Provided";
                        //    oAPI.ElectricalYear = row.WiringYear ?? "Not Provided";
                        //    oAPI.HvacUpdates = row.HeatingUpdate ?? "Not Provided";
                        //    oAPI.HvacFullPartial = row.HeatingFullPartial ?? "Not Provided";
                        //    oAPI.HvacYear = row.HeatingYear ?? "Not Provided";
                        //    oAPI.PlumbingUpdates = row.PlumbingUpdate ?? "Not Provided";
                        //    oAPI.PlumbingFullPartial = row.PlumbingFullPartial ?? "Not Provided";
                        //    oAPI.PlumbingYear = row.PlumbingYear ?? "Not Provided";                        
                        ////}

                        oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + row.PolicyNumber);
                        sbEmail.Append("Importing Policy# " + row.PolicyNumber + System.Environment.NewLine);

                        string sRet = oAPI.ImportAMWins();

                        oLU.WritetoLog("oAPI.Import return for for Policy# " + row.PolicyNumber + "\r\n\r\n" + sRet);

                        var importResults = sRet.FromJSON<List<ImportResult>>();

                        foreach (var importResult in importResults)
                        {

                            if (importResult.Successful)
                            {
                                oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                                sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + System.Environment.NewLine);

                                // Send confirmation
                                if (!sendConfirmation(importResult.CaseNumber.ToString(), oAPI, sInspTypeDesc))
                                {
                                    oLU.WritetoLog("**** Error sending confirmation to: " + oAPI.EmailConfirmation);
                                    sbEmail.Append("**** Error sending confirmation to: " + oAPI.EmailConfirmation + System.Environment.NewLine);
                                }

                            }
                            else
                            {
                                oLU.WritetoLog("**** Import failed **** ");
                                if (importResult.Errors != null)
                                {
                                    foreach (var error in importResult.Errors)
                                    {
                                        oLU.WritetoLog("Error: " + error.ErrorText);
                                        sbEmail.Append("Error: " + error.ErrorText + System.Environment.NewLine);
                                    }
                                }
                            }

                            if ((bool)importResult.Duplicate)
                            {
                                oLU.WritetoLog("Duplicate case");
                                sbEmail.Append("Duplicate case" + System.Environment.NewLine);
                            }
                        }

                    }   // Insured Name not empty

                }   // foreach row in sheet


                bImportSuccess = true;

            }   //try

            catch (Exception ex)
            {
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail("Exception logged" + System.Environment.NewLine + ex.Message);
            }

            finally
            {

                try
                {  

                    // Copy source file to holding area and delete temp
                    string sSourceName = cfg_sourcedir + sExcelFileName;
                    string sDestName = "";
                    DateTime dtNow = DateTime.Now;
                    string sTimeStamp = dtNow.Minute.ToString() + dtNow.Second.ToString();

                    //if successful - copy to sibidata\AMWins\Processed
                    //if failed - copy to sibidata\AMWins\fail
                    if (bImportSuccess)
                    {
                        sDestName = cfg_compdir + sTimeStamp + sExcelFileName;
                    }
                    else
                    {
                        sDestName = cfg_faildir + sTimeStamp + sExcelFileName;
                    }

                    // Copy Excel
                    File.Copy(sSourceName, sDestName);
                    if (File.Exists(sDestName))
                    {
                        File.Delete(sSourceName);
                    }
                    else
                    {
                        throw new ApplicationException("Copy failed for: " + sExcelFileName);
                    }

                }   //try

                catch (Exception ex)
                {
                    oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                    sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                    sendErrEmail("Exception logged" + System.Environment.NewLine + ex.Message);
                }

                sendLogEmail(sbEmail.ToString());
                oLU.closeLog();


            }

        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import AMWins Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import AMWins Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        // IsNumeric Function
        static bool IsNumeric(object Expression)
        {
            bool isNum;

            double retNum;

            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        static bool GetUnderwriter(ImportRequests oAPI, StringBuilder sbEmail)
        {

            bool bRet = false;
            string sFirstName = "";
            string sLastName = "";

            string sEmail = oAPI.EmailConfirmation;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetUnderwriterAmWins";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@underwriteremail", oAPI.EmailConfirmation);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        sFirstName = sqlReader.GetSqlString(0).ToString();
                        sLastName = sqlReader.GetSqlString(1).ToString();

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }
                else
                {
                    throw new Exception("**** No match for underwriter email: " + oAPI.EmailConfirmation);
                }

                // if no first & last name reteurn error
                if (sFirstName.Length < 1 || sLastName.Length < 1)
                {
                    throw new Exception("**** No underwriter name returned for email: " + oAPI.EmailConfirmation);
                }

                oAPI.UnderwriterFirstName = sFirstName;
                oAPI.UnderwriterLastName = sLastName;
                oAPI.UnderwriterCorrEmail = oAPI.EmailConfirmation;
                oAPI.UnderwriterRptEmail = oAPI.EmailConfirmation;
                oAPI.UnderwriterPhone = "";

                bRet = true;
            }
            catch (Exception ex)
            {

                //record exception  
                sbEmail.Append("Exception Logged in GetUnderwriter() " + System.Environment.NewLine + ex.Message + System.Environment.NewLine);

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

            return bRet;
        }

        static bool sendConfirmation(string sCaseNum, ImportRequests oAPI, string sInspType)
        {
            bool bRet = false;

            try
            {


                EmailConfirmation360.EmailConf360 oEM = new EmailConfirmation360.EmailConf360();

                oEM.cfg_360ConnStr = cfg_360UtilConnStr;
                oEM.cfg_smtpserver = cfg_smtpserver;
                oEM.CaseNum = sCaseNum;
                oEM.CustNum = "7301";
                oEM.InsuredName = oAPI.InsuredName;
                oEM.RequestedBy = oAPI.UnderwriterFirstName + " " + oAPI.UnderwriterLastName;
                oEM.Recip = oAPI.EmailConfirmation;
                oEM.BCC = "";
                oEM.DateSubmitted = DateTime.Now.ToString();
                oEM.InsAdd1 = oAPI.MailAddress1;
                //oEM.InsAdd2 = "Insured address line  2";
                oEM.InsCity = oAPI.MailCity;
                oEM.InsState = oAPI.MailState;
                oEM.InsZip = oAPI.MailZip;
                oEM.InsuredContactName = oAPI.ContactName;
                oEM.InsuredContactPhoneHome = oAPI.LocationContactPhone;
                oEM.InsuredContactPhoneWork = "";
                oEM.InsuredContactPhoneCell = "";
                oEM.PolicyNum = oAPI.PolicyNumber;
                oEM.Agent = oAPI.AgencyAgentName + " " + oAPI.AgencyAgentContact;
                oEM.AgentPhone = oAPI.AgencyAgentPhone;
                oEM.InsuranceCo = oAPI.InsuranceCompany;
                oEM.Underwriter = oAPI.UnderwriterFirstName + " " + oAPI.UnderwriterLastName;
                oEM.LocAdd1 = oAPI.LocationAddress1;
                //oEM.LocAdd2 = "Location address line  2";
                oEM.LocCity = oAPI.LocationCity;
                oEM.LocState = oAPI.LocationState;
                oEM.LocZip = oAPI.LocationZip;
                oEM.LocContact = oAPI.LocationContactName;
                oEM.LocContactPhone = oAPI.LocationContactPhone;
                oEM.InspectionType = sInspType;
                oEM.Comments = oAPI.Comments;
                
                string sRet = oEM.sendEmailConf360();

                bRet = true;
            }
            catch (Exception ex)
            {

                //record exception  
                sbEmail.Append("Exception Logged in sendConfirmation() " + System.Environment.NewLine + ex.Message + System.Environment.NewLine);

            }

            return bRet;
        }
    }
}
