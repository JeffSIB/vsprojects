﻿using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Globalization;
using System.Threading;

namespace ZipCodeSkillSetBuild
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360ConnStr;
        static string cfg_360UtilConnStr;
        static string cfg_SQLMainSIBIConnStr;
        static string cfg_SQLMainUTILConnStr;
        static DateTime dBegDate;
        static DateTime dEndDate;

        static void Main(string[] args)
        {
            
            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");
            cfg_360ConnStr = colNameVal.Get("360ConnStr");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");
            cfg_SQLMainUTILConnStr = colNameVal.Get("SQLMainUTILConnStr");

            dBegDate = Convert.ToDateTime(args[0]);
            dEndDate = Convert.ToDateTime(args[1]);

            // Build SIB Work table - clear table first
            BuildSIBZipCodeSkillSet("O", true);
            //BuildSIBZipCodeSkillSet("C", true);

        }

        static void BuildSIBZipCodeSkillSet(string sMode, bool bClearTable)
        {

            // Populate table ZipCodeSkillSetWork

            // sMode - build table based on:
            // O = Ordered
            // C = Completed

            int iRet = 0;
            int iKeynumber = 0;
            string sZipCode = "";
            string sOptions = "";
            string sSkillSet = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin BuildSIBZipCodeSkillSet ++++");

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_SQLMainUTILConnStr);
                sqlCmd1 = new SqlCommand();
                sqlConn2 = new SqlConnection(cfg_SQLMainUTILConnStr);
                sqlCmd2 = new SqlCommand();
                sqlConn1.Open();

                // Second SQL conn for updating table
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                // clear table first time through
                if (bClearTable)
                {
                    sqlCmd2.CommandType = CommandType.Text;

                    if (sMode == "O")
                    {
                        sqlCmd2.CommandText = "DELETE FROM ZipCodeSkillSetWork_O";
                    }
                    else
                    {
                        sqlCmd2.CommandText = "DELETE FROM ZipCodeSkillSetWork_C";
                    }

                    sqlCmd2.Connection = sqlConn2;
                    sqlCmd2.ExecuteNonQuery();
                }

                sqlCmd1.CommandType = CommandType.StoredProcedure;

                if (sMode == "O")
                {
                    sqlCmd1.CommandText = "dbo.sp_BuildZipCodeSkillSet_GetKeys_O";
                }
                else
                {
                    sqlCmd1.CommandText = "dbo.sp_BuildZipCodeSkillSet_GetKeys_C";
                }
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();

                sqlCmd2.CommandType = CommandType.StoredProcedure;
                if (sMode == "O")
                {
                    sqlCmd2.CommandText = "dbo.sp_ZipCodeSkillSetWork_Insert_O";
                    sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                    sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                }
                else
                {
                    sqlCmd2.CommandText = "dbo.sp_ZipCodeSkillSetWork_Insert_C";
                    sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                    sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                }

                // Get all completed cases
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through cases
                    do
                    {


                        iKeynumber = (int)sqlReader.GetSqlInt32(0);

                        //Location zip
                        if (sqlReader.IsDBNull(1))
                        {
                            sZipCode = "";
                        }
                        else
                        {
                            sZipCode = sqlReader.GetSqlString(1).ToString();

                            // if zip is < 5 ignore it
                            if (sZipCode.Length < 5)
                                sZipCode = "";
 
                            // trim to 5 chars max
                            else if (sZipCode.Length > 5)
                                sZipCode = sZipCode.Remove(5);

                            if (sZipCode.Length == 5 && sZipCode.Substring(0,1) != "3")
                                sZipCode = "";

                            if (!IsNumeric(sZipCode))
                                sZipCode = "";
                        }

                        //Options
                        if (sqlReader.IsDBNull(2))
                        {
                            sOptions = "";
                        }
                        else
                        {
                            sOptions = sqlReader.GetSqlString(2).ToString();
                            sOptions = sOptions.ToUpper();
                        }

                        // If zipcode is valid
                        if (sZipCode != "" && sOptions != "")
                        {

                            // set SkillSet based on form in Options
                            if (sOptions.IndexOf("ORM 1010") > 0)
                            {
                                sSkillSet = "1CM";
                            }
                            else if (sOptions.IndexOf("ORM 1016") > 0)
                            {
                                sSkillSet = "1CM";
                            }
                            else if (sOptions.IndexOf("ORM 1028") > 0)
                            {
                                sSkillSet = "1CM";
                            }
                            else if (sOptions.IndexOf("ORM 1080L") > 0)
                            {
                                sSkillSet = "1RES";
                            }
                            else if (sOptions.IndexOf("ORM 1081") > 0)
                            {
                                sSkillSet = "1RES";
                            }
                            else if (sOptions.IndexOf("ORM 1061") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1091") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1007") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1008") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1008P") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1015") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1024") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1039") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1054") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1057") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1091L") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1095") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1097") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1098") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 3005") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 3007") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 3008") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6010") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6020") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6030") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6040") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1052") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 1080AB") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 4000") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 4010") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 1003") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1004") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1005") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1012") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1014") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1099") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1037") > 0)
                            {
                                sSkillSet = "3RES";
                            }
                            else if (sOptions.IndexOf("ORM 1080A") > 0)
                            {
                                sSkillSet = "3RES";
                            }
                            else if (sOptions.IndexOf("ORM 7131") > 0)
                            {
                                sSkillSet = "NP";
                            }
                            else if (sOptions.IndexOf("ORM 1011") > 0)
                            {
                                sSkillSet = "TEL";
                            }
                            else if (sOptions.IndexOf("ORM 1045") > 0)
                            {
                                sSkillSet = "TEL";
                            }
                            else if (sOptions.IndexOf("ORM 1056") > 0)
                            {
                                if (sOptions.IndexOf("ORM 1056C") > 0)
                                {
                                    sSkillSet = "TEL";
                                }
                                else if (sOptions.IndexOf("ORM 1056T") > 0)
                                {
                                    sSkillSet = "TEL";
                                }
                                else if (sOptions.IndexOf("ORM 1056R") > 0)
                                {
                                    sSkillSet = "TEL";
                                }
                                else
                                {
                                sSkillSet = "2CM";
                                }
                            }
                            else if (sOptions.IndexOf("ORM 1023") > 0)
                            {
                                sSkillSet = "WC";
                            }

                            // log no skillset
                            if (sSkillSet == "")
                            {
                                oLU.WritetoLog("Keynumber: " + iKeynumber.ToString() + " - undetermined skill set");
                            }
                            else
                            {
                                //if (sSkillSet == "TEL")
                                //{
                                    // Insert into table
                                    sqlCmd2.Parameters.Clear();

                                    sqlCmd2.Parameters.AddWithValue("@keynumber", iKeynumber);
                                    sqlCmd2.Parameters.AddWithValue("@ZipCode", sZipCode);
                                    sqlCmd2.Parameters.AddWithValue("@SkillSetCode", sSkillSet);

                                    iRet = sqlCmd2.ExecuteNonQuery();

                                    if (iRet == 0)
                                        throw new SystemException("dbo.sp_ZipCodeSkillSetWork_Insert returned 0 for " + iKeynumber.ToString());
                                //}
                            }
                        }
                    } while (sqlReader.Read());     // cases

                    sqlReader.Close();

                }   // has rows

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End BuildSIBZipCodeSkillSet ----");
                oLU.closeLog();

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn1.Close();


            }


        }








        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

        static decimal calcPct(int iCur, int iPrev)
        {
            
            decimal dRetVal = 0;

            if (iCur == 0 && iPrev == 0)
                return dRetVal;

            decimal dCur = Convert.ToDecimal(iCur);
            decimal dPrev = Convert.ToDecimal(iPrev);

            if (dCur == 0)
            {
                return 0 - dPrev;
            }
            if (dPrev == 0)
            {
                return dCur;
            }

            if (dCur - dPrev > 0)
            {
                dRetVal = 1 - (dPrev / dCur);  
            }
            else
            {
                dRetVal = 0 - (1-(dCur / dPrev));            
            }

            return dRetVal;
        
        }

        // IsNumeric Function
        static bool IsNumeric(object Expression)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum;

            // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
            double retNum;

            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }		
    }
}
