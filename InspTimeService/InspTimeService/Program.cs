﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace InspTimeService
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        //static Excel.Worksheet o360Worksheet = null;
        static Excel.Worksheet oTotWorksheet = null;
        static Excel.Range oRange = null;
        static DateTime dBegDate;
        static DateTime dEndDate;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;
        static string sMode;
        static DataTable dtCombined;
        static DataSet dsReportData;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");


                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //  W = weekly
                //  M = monthly
                sMode = "";
                if (args.Length < 1)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                if (args[0].ToUpper() == "W")
                {
                    // ** Weekly **
                    // Assumes that it is being run on Monday
                    // Results are for previous Sun-Sat

                    sMode = "W";
                    DateTime dt = new DateTime();
                    dt = DateTime.Today;

                    // Set beg date to previous Sunday - Sat
                    dBegDate = dt.AddDays(-8);
                    dEndDate = dt.AddDays(-2);

                    //dBegDate = new DateTime(2020, 5, 24);
                    //dEndDate = new DateTime(2020, 5, 30);


                }
                else if (args[0].ToUpper() == "M")
                {

                    ///////////////////////////////////////////
                    // **Monthly**
                    // Runs for previous month

                    sMode = "M";                 
                    DateTime dt = new DateTime();
                    dt = DateTime.Today;
                    dt = dt.AddMonths(-1);

                    // Set beg date to first/last day of previous month
                    dBegDate = FirstDayOfMonth(dt);
                    dEndDate = LastDayOfMonth(dt);
                }
                else if (args[0].ToUpper() == "C")
                {

                    ///////////////////////////////////////////
                    // **Custom Date Range**
                    sMode = "C";
                    dBegDate = Convert.ToDateTime(args[1]);
                    dEndDate = Convert.ToDateTime(args[2]);
                }

                else
                {
                    throw new SystemException("Invalid period arguments supplied");
                }

                string sDateDisp = dBegDate.Month + "-" + dBegDate.Day + "_" + dEndDate.Month + "-" + dEndDate.Day;
                string sEmailSubject = "InspTimeService - " + dBegDate.Month + "/" + dBegDate.Day + " - " + dEndDate.Month + "/" + dEndDate.Day + ", " + dBegDate.Year.ToString();
                string sExcelFileNameNoEx = cfg_outputdir + "InspTimeService_" + sDateDisp;
                string sExcelFileName = sExcelFileNameNoEx + ".xlsx";


                ///////////////////////////////////////////////////////////////

                // Dataset 
                dtCombined = new DataTable("360");
                dtCombined.Columns.Add("insp", typeof(string));
                dtCombined.Columns.Add("completed", typeof(int));
                dtCombined.Columns.Add("Avg360", typeof(int));
                dtCombined.Columns.Add("returns", typeof(int));

                // Create a DataSet.
                dsReportData = new DataSet("reportdata");
                dsReportData.Tables.Add(dtCombined);
                dsReportData.Tables[0].DefaultView.Sort = "insp ASC"; 


                // SIB
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                int iRet = 0;
                int iCompleted = 0;
                int iAvgFirstComp = 0;
                int iAvgLastComp = 0;
                int iReturns = 0;
                decimal dPctReturn = 0;
                string sFieldRep = "";
                int iRow = 0;
                int iWork = 0;
                decimal decWork = 0;

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                // Init Excel
                oExcel = new Excel.Application();
                oExcel.Visible = false;
                oWorkbook = oExcel.Workbooks.Add(1);
                //oSIBWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                //oSIBWorksheet.Name = "SIB";

                //createHeaderSIB(oSIBWorksheet);
                //iRow = 4;

                ////// set up SQL connection (SIBTBL)
                //sqlConn1 = new SqlConnection(cfg_SIBUtilConnStr);
                //sqlCmd1 = new SqlCommand();
                //sqlCmd1.CommandType = CommandType.StoredProcedure;
                //sqlCmd1.CommandTimeout = 90;

                //// build table
                //sqlCmd1.CommandText = "sp_InspTimeService_Build";
                //sqlCmd1.Connection = sqlConn1;
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                //sqlConn1.Open();
                //iRet = sqlCmd1.ExecuteNonQuery();

                //// process table
                //sqlCmd1.CommandText = "sp_InspTimeService";
                //sqlCmd1.Connection = sqlConn1;
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);

                //sqlReader = sqlCmd1.ExecuteReader();

                //if (sqlReader.HasRows)
                //{


                //    sqlReader.Read();

                //    // loop through rows
                //    do
                //    {

                //        //    Field rep
                //        if (sqlReader.IsDBNull(0))
                //        {
                //            sFieldRep = "";
                //        }
                //        else
                //        {
                //            sFieldRep = (string)sqlReader.GetSqlString(0);
                //        }
                //        addData(oSIBWorksheet, iRow, 1, sFieldRep.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "");

                //        //completed
                //        if (sqlReader.IsDBNull(1))
                //        {
                //            iCompleted = 0;
                //        }
                //        else
                //        {
                //            iCompleted = (int)sqlReader.GetSqlInt32(1);
                //        }
                //        addData(oSIBWorksheet, iRow, 2, iCompleted.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0");


                //        //Avg Comp
                //        if (sqlReader.IsDBNull(2))
                //        {
                //            iAvgFirstComp = 0;
                //        }
                //        else
                //        {
                //            iAvgFirstComp = (int)sqlReader.GetSqlInt32(2);
                //        }
                //        addData(oSIBWorksheet, iRow, 3, iAvgFirstComp.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0");


                //        //returns
                //        if (sqlReader.IsDBNull(3))
                //        {
                //            iReturns = 0;
                //        }
                //        else
                //        {
                //            iReturns = (int)sqlReader.GetSqlInt32(3);
                //        }
                //        addData(oSIBWorksheet, iRow, 4, iReturns.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0");


                //        //% returns
                //        dPctReturn = 0;
                //        if (iCompleted > 0 && iReturns > 0)
                //        {
                //            dPctReturn = Convert.ToDecimal(iReturns) / Convert.ToDecimal(iCompleted);
                //        }
                //        addData(oSIBWorksheet, iRow, 5, dPctReturn.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "##0.0%");

                //        iRow++;

                //        // Add row to Totals table
                //        dtCombined.Rows.Add(sFieldRep, iCompleted, iAvgFirstComp, 0, iReturns);


                //    } while (sqlReader.Read());     // SIB

                //}   // has rows

                //sqlReader.Close();
                //sqlConn1.Close();
                //sqlConn1 = null;
                //sqlCmd1 = null;


                //msMsgBody = "Processing completed for SIB" + System.Environment.NewLine + cfg_outputdir + "ReviewerStatsSIB_" + sDateDisp + System.Environment.NewLine + System.Environment.NewLine;

                ///////////////////////////////////////////////////
                // 360 
                ///////////////////////////////////////////////////

                // set time to 12:00am
                dBegDate = ChangeTime(dBegDate, 0, 0, 0, 0);

                // set time to midnight
                dEndDate = ChangeTime(dEndDate, 23, 59, 59, 0);

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandTimeout = 90;
                sqlCmd1.CommandType = CommandType.StoredProcedure;

                // build table
                sqlCmd1.CommandText = "sp_Insp_TimeService_Build";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                sqlConn1.Open();
                iRet = sqlCmd1.ExecuteNonQuery();

                // process table
                sqlCmd1.CommandText = "sp_InspTimeService";
                //sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                //sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {

                    ////oExcel = new Excel.Application();
                    ////oExcel.Visible = false;
                    //oWorkbook = oExcel.Workbooks.Add(1);
                    //oSIBWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    //oSIBWorksheet.Name = "SIB";

                    //o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    //o360Worksheet.Name = "360";
                    //createHeader360(o360Worksheet);

                    //oExcel = new Excel.Application();
                    //oExcel.Visible = false;
                    //oWorkbook = oExcel.Workbooks.Add(1);
                    //o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    //o360Worksheet.Name = "360";
                    //createHeader(o360Worksheet, "360");


                    iRow = 4;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //    Field rep
                        if (sqlReader.IsDBNull(0))
                        {
                            sFieldRep = "";
                        }
                        else
                        {
                            sFieldRep = (string)sqlReader.GetSqlString(0);
                        }
                        //addData(o360Worksheet, iRow, 1, sFieldRep.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "");

                        //completed
                        if (sqlReader.IsDBNull(1))
                        {
                            iCompleted = 0;
                        }
                        else
                        {
                            iCompleted = (int)sqlReader.GetSqlInt32(1);
                        }
                        //addData(o360Worksheet, iRow, 2, iCompleted.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0");


                        //Avg First Comp
                        if (sqlReader.IsDBNull(2))
                        {
                            iAvgFirstComp = 0;
                        }
                        else
                        {
                            iAvgFirstComp = (int)sqlReader.GetSqlInt32(2);
                        }
                        //addData(o360Worksheet, iRow, 3, iAvgFirstComp.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0");


                        //Avg last comp
                        if (sqlReader.IsDBNull(3))
                        {
                            iAvgLastComp = 0;
                        }
                        else
                        {
                            iAvgLastComp = (int)sqlReader.GetSqlInt32(3);
                        }
                        //addData(o360Worksheet, iRow, 4, iAvgLastComp.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0");


                        //returns
                        if (sqlReader.IsDBNull(4))
                        {
                            iReturns = 0;
                        }
                        else
                        {
                            iReturns = (int)sqlReader.GetSqlInt32(4);
                        }
                        //addData(o360Worksheet, iRow, 5, iReturns.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "#,##0");


                        //% returns
                        dPctReturn = 0;
                        if (iCompleted > 0 && iReturns > 0)
                        {
                            dPctReturn = Convert.ToDecimal(iReturns) / Convert.ToDecimal(iCompleted);
                        }
                        //addData(o360Worksheet, iRow, 6, dPctReturn.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "##0.0%");

                        iRow++;

                        // if row for insp does not exist in Combined table - add it
                        System.Data.DataRow[] CombRow = dtCombined.Select("insp = '" + sFieldRep + "'");

                        if (CombRow.Length == 0)
                        {
                            dtCombined.Rows.Add(sFieldRep, 0, iAvgFirstComp, 0);
                        }

                        // update inspector row
                        CombRow = dtCombined.Select("insp = '" + sFieldRep + "'");

                        // # completed
                        iWork = (int)CombRow[0]["completed"];
                        CombRow[0]["completed"] = iWork + iCompleted;

                        // # returned
                        iWork = (int)CombRow[0]["returns"];
                        CombRow[0]["returns"] = iWork + iReturns;

                        // iAvgFirstComp comp
                        CombRow[0]["Avg360"] = iAvgFirstComp;


                    } while (sqlReader.Read());     // 360

                }   // has rows

                sqlReader.Close();


                ///////////////////////////////////////////////////
                // Add any inspectors that did not complete anything 
                // in the period to the combined table 
                ///////////////////////////////////////////////////


                // get all active inspectors
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetAllActiveInsp";
                sqlCmd1.Parameters.Clear();
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //    Field rep
                        if (sqlReader.IsDBNull(1))
                        {
                            sFieldRep = "";
                        }
                        else
                        {
                            sFieldRep = (string)sqlReader.GetSqlString(1);
                        }

                        // if row for insp does not exist in Combined table - add it
                        System.Data.DataRow[] CombRow = dtCombined.Select("insp = '" + sFieldRep + "'");

                        if (CombRow.Length == 0)
                        {
                            dtCombined.Rows.Add(sFieldRep, 0, 0, 0);
                        }

                    } while (sqlReader.Read());     // 360

                }   // has rows

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;

                ///////////////////////////////////////////////////
                // Combined
                ///////////////////////////////////////////////////

                oTotWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                oTotWorksheet.Name = "Time Service";
                createHeaderTot(oTotWorksheet);

                int iTotComp = 0;
                int iTotReturns = 0;

                iRow = 4;

                // load data from dataset
                DataView dv = dtCombined.DefaultView;
                dv.Sort = "insp ASC";
                DataTable sortedDT = dv.ToTable();

                foreach (System.Data.DataRow row in sortedDT.Rows)
                {

                    //    Field rep
                    addData(oTotWorksheet, iRow, 1, row["insp"].ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "");

                    //completed
                    iTotComp = (int)row["completed"];
                    addData(oTotWorksheet, iRow, 2, row["completed"].ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0");

                    //Avg Comp 360
                    addData(oTotWorksheet, iRow, 3, row["Avg360"].ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0");

                    //returns
                    iTotReturns = (int)row["returns"];
                    addData(oTotWorksheet, iRow, 4, row["returns"].ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0");

                    //% returns
                    dPctReturn = 0;
                    if (iTotComp > 0 && iTotReturns > 0)
                    {
                        dPctReturn = Convert.ToDecimal(iTotReturns) / Convert.ToDecimal(iTotComp);
                    }
                    addData(oTotWorksheet, iRow, 5, dPctReturn.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "##0.0%");

                    iRow++;

                }

                //oWorkbook.SaveAs(cfg_outputdir + "ReviewerStats360_" + sDateDisp, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.Close(true, oMissing, oMissing);
                oExcel.Quit();
                msMsgBody += "Processing completed for 360" + System.Environment.NewLine + cfg_outputdir + "InspTimeService_" + sDateDisp + System.Environment.NewLine + System.Environment.NewLine;
                
                releaseObject(oExcel);
                releaseObject(oWorkbook);
                //releaseObject(oSIBWorksheet);
                //releaseObject(oTotWorksheet);
                //releaseObject(o360Worksheet);
                sendExcelFile(sEmailSubject, sExcelFileName);
                msMsgBody += "File sent " + sExcelFileName;
            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(msMsgBody);                
                }
            
            }
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeaderSIB(Excel._Worksheet oWorkSheet)
        {

            Excel.Range oRange;


            oWorkSheet.get_Range("A1", "E1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "E1");
            oRange.FormulaR1C1 = "Inspector Time Service - SIB   " + dBegDate.ToShortDateString() + " - " + dEndDate.ToShortDateString();
            
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Field Rep";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 20;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Completed";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "AVG Comp";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Returns";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Pct Returned";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);     
        
        
        }

        static void createHeader360(Excel._Worksheet oWorkSheet)
        {

            Excel.Range oRange;


            oWorkSheet.get_Range("A1", "F1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "F1");
            oRange.FormulaR1C1 = "Inspector Time Service - 360   " + dBegDate.ToShortDateString() + " - " + dEndDate.ToShortDateString();

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Field Rep";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 20;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Completed";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "AVG First Comp";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "AVG Last Comp";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Returns";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 6] = "Pct Returned";
            oRange = oWorkSheet.get_Range("F3", "F3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);


        }

        static void createHeaderTot(Excel._Worksheet oWorkSheet)
        {

            Excel.Range oRange;


            oWorkSheet.get_Range("A1", "E1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "E1");
            oRange.FormulaR1C1 = "Inspector Time Service  " + dBegDate.ToShortDateString() + " - " + dEndDate.ToShortDateString();

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Field Rep";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 20;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Completed";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "AVG Comp Days";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Returns";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Pct Returned";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        }

  
        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data, 
			string cell1, string cell2,string format)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }



        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "InspTimeService";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by InspTimeService Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                if (sMode == "C")
                {
                    oMail.MailTo = "jeff@sibfla.com";
                }
                else {
                    //oMail.MailTo = "jeff@sibfla.com";
                    oMail.MailTo = "troy@sibfla.com;tiana@sibfla.com;andrea@sibfla.com;randy@sibfla.com;karla@sibfla.com;michael@sibfla.com;thomas@sibfla.com;";
                }
                oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }



    }
}
