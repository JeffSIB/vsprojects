﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;
using System.Text.RegularExpressions;
using WinSCP;


namespace importCitizens
{

    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyNumber { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string AgentFirstName { get; set; }
            public string AgentLastName { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgentEmail { get; set; }
            public string AgentPhone { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string InsuredEmail { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            //public string BusinessOperations { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string ISOClass { get; set; }
            public string YearBuilt { get; set; }
            public string Occupancy { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }
            public string Dimensions { get; set; }
            public string SecurityGate { get; set; }
            public string SecurityGuard { get; set; }
            public string ActivityID { get; set; }

        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
        static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string cfg_SQLMainSIBIConnStr = ConfigurationManager.AppSettings["SIBIConnStr"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];

        static LogUtils.LogUtils oLU;

        //Used for email message body  
        static StringBuilder sbEmail = new StringBuilder();
        static StringBuilder sbConfEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {

            string sExcelFileName = "";
            string sFileName = "";
            int iNumDownloaded = 0;
            int iNumRenamed = 0;
            int iNumProcessed = 0;

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                // Look for file passed via command line - manual import
                if (args.Length > 0)
                {
                    // MANUAL IMPORT

                    sExcelFileName = args[0];

                    // does file exist?
                    FileInfo fi = new FileInfo(cfg_sourcedir + sExcelFileName);
                    if (!fi.Exists)
                    {
                        // send email
                        throw new ApplicationException("File does not exist: " + cfg_sourcedir + sExcelFileName);
                    }
                    importFile(fi.Name);
                    iNumDownloaded = 1;

                    oLU.WritetoLog("Manual import of: " + sExcelFileName);
                }
                else
                {
                    // AUTOMATED IMPORT

                    // check for files on FTP site
                    iNumDownloaded = getSFTP();

                    // if SFTP returns error
                    if (iNumDownloaded < 0)
                    {
                        throw new ApplicationException("SFTP Download return error");
                    }
                    // nothing downloaded
                    else if (iNumDownloaded == 0)
                    {
                        oLU.WritetoLog("Nothing downloaded");
                    }

                    // process files
                    else
                    {
                        oLU.WritetoLog(iNumDownloaded.ToString() + " files downloaded");

                        DirectoryInfo diImportFiles = new DirectoryInfo(cfg_sourcedir);
                        FileInfo[] fiImportFiles = diImportFiles.GetFiles("*.xls*");

                        // remove spaces from file name
                        foreach (FileInfo file in fiImportFiles)
                        {
                            // remove spaces from file names
                            sFileName = file.Name.Replace(" ", String.Empty);
                            file.MoveTo(cfg_sourcedir + sFileName);
                            iNumRenamed++;
                        }

                        fiImportFiles = diImportFiles.GetFiles("*.xls*");
                        foreach (FileInfo file in fiImportFiles)
                        {
                            // import files
                            importFile(file.Name);
                            iNumProcessed++;
                        }
                    }

                    oLU.WritetoLog(iNumDownloaded.ToString() + " files downloaded");
                    oLU.WritetoLog(iNumRenamed.ToString() + " files renamed");
                    oLU.WritetoLog(iNumProcessed.ToString() + " files processed");

                    // make sure the same number of files downloaded were processed
                    if (iNumRenamed != iNumDownloaded || iNumProcessed != iNumDownloaded)
                    {
                        oLU.WritetoLog("**** Files processed <> files downloaded");
                        throw new ApplicationException("Number of files processed not equal to number downloaded");
                    }

                }   // AUTOMATED IMPORT

                oLU.closeLog();

                if (iNumDownloaded > 0)
                    sendLogEmail(sbEmail.ToString());

            }

            catch (Exception ex)
            {
                oLU.WritetoLog("Error initializing importCitizens\r\n\r\n" + ex.Message);
                oLU.closeLog();
                sendErrEmail("Error initializing importCitizens\r\n\r\n" + ex.Message);
                return;
            }
        }

        static void importFile(string sExcelFileName)
        {
            string sRush = "";
            string sFirstName = "";
            string sLastName = "";
            string sInsName = "";
            string sComments = "";
            string sConfEmail = "UW.Vendor.Admin@citizensfla.com";
            //string sConfEmail = "jeff@sibfla.com";

            bool bImportSuccess = false;

            try
            {

                oLU.WritetoLog("Processing: " + sExcelFileName);
                sbEmail.Append(System.Environment.NewLine + "Processing: " + sExcelFileName + System.Environment.NewLine);
                
                var excel = new ExcelQueryFactory(cfg_sourcedir + sExcelFileName);

                excel.AddMapping<ImportRecord>(x => x.PolicyNumber, "POLICY_NUMBER");

                excel.AddMapping<ImportRecord>(x => x.InspectionType, "Inspection Type");

                excel.AddMapping<ImportRecord>(x => x.InsuredFirstName, "INSURED_FIRST_NAME");
                excel.AddMapping<ImportRecord>(x => x.InsuredLastName, "INSURED_LAST_NAME");

                excel.AddMapping<ImportRecord>(x => x.MailAddress1, "INSURED_MAILING_ADDRESS");
                excel.AddMapping<ImportRecord>(x => x.MailAddress2, "INSURED_MAILING_ADDRESS2");
                excel.AddMapping<ImportRecord>(x => x.MailCity, "INSURED_MAILING_CITY");
                excel.AddMapping<ImportRecord>(x => x.MailState, "INSURED_MAILING_STATE");
                excel.AddMapping<ImportRecord>(x => x.MailZip, "INSURED_MAILING_ZIP");
                
                excel.AddMapping<ImportRecord>(x => x.ContactPhoneHome, "INSURED_HOME_PHONE");
                excel.AddMapping<ImportRecord>(x => x.ContactPhoneWork, "INSURED_WORK_PHONE");
                excel.AddMapping<ImportRecord>(x => x.ContactPhoneCell, "INSURED_ALTERNATE_PHONE");
                excel.AddMapping<ImportRecord>(x => x.InsuredEmail, "insured_email");

                excel.AddMapping<ImportRecord>(x => x.LocationAddress1, "PROPERTY_ADDRESS");
                excel.AddMapping<ImportRecord>(x => x.LocationAddress2, "PROPERTY_ADDRESS2");
                excel.AddMapping<ImportRecord>(x => x.LocationCity, "PROPERTY_CITY");
                excel.AddMapping<ImportRecord>(x => x.LocationState, "PROPERTY_STATE");
                excel.AddMapping<ImportRecord>(x => x.LocationZip, "PROPERTY_ZIP");

                excel.AddMapping<ImportRecord>(x => x.AgentFirstName, "AGENT_FIRST_NAME");
                excel.AddMapping<ImportRecord>(x => x.AgentLastName, "AGENT_LAST_NAME");
                excel.AddMapping<ImportRecord>(x => x.AgentEmail, "AGENT_EMAIL");
                excel.AddMapping<ImportRecord>(x => x.AgentPhone, "AGENT_PHONE");   // Added 6/21


                excel.AddMapping<ImportRecord>(x => x.EffectiveDate, "policy_effective_Date");
                excel.AddMapping<ImportRecord>(x => x.Comments, "Special Instructions");

                excel.AddMapping<ImportRecord>(x => x.YearBuilt, "YEAR_BUILD");   // Added 6/21
                excel.AddMapping<ImportRecord>(x => x.Dimensions, "DIMENSIONS");
                excel.AddMapping<ImportRecord>(x => x.SecurityGate, "LOCKED_SECURITY_GATE");    // Added 6/21
                excel.AddMapping<ImportRecord>(x => x.SecurityGuard, "SECURITY_GUARD");    // Added 6/21
                excel.AddMapping<ImportRecord>(x => x.ActivityID, "ACTIVITY_ID");   // Added 6/21

                var rows = (from row in excel.Worksheet<ImportRecord>(0) select row).ToList();

                // Are there any rows to import?
                if (rows.Count() > 0)
                {

                    sbConfEmail.Clear();
                    buildConfHeader();

                    foreach (var row in rows)
                    {

                        // Skip if the current row is the header row - some files do not pick up a second row
                        if (row.PolicyNumber != "POLICY_NUMBER")
                        {
                            ImportRequests oAPI = new ImportRequests();
                            oAPI.CustomerUserName = "APIProd";
                            oAPI.CustomerPassword = "Sutton2012";

                            oAPI.CustomerAccount = "7072";

                            // Inspection type
                            if (row.InspectionType.ToUpper().Contains("TIE-DOWN"))
                            {
                                oAPI.InspectionType = "7072-GCTD";
                            }
                            else if (row.InspectionType.ToUpper().Contains("INTERIOR-EXTERIOR"))
                            {
                                oAPI.InspectionType = "7072-GCU";
                            }
                            else
                            {
                                throw new ApplicationException("Invalid inspection type: " + oAPI.InspectionType);
                            }

                            // Add items to comments
                            sComments = "";
                            if (row.Comments != null)
                            {
                                sComments = row.Comments.Trim();
                                sComments = sComments.Replace("Auto Generated Inspection", "");
                            }

                            if (row.Dimensions != null)
                            {
                                sComments = "Dimensions: " + row.Dimensions + "<br />" + sComments;
                            }
                            if (row.YearBuilt != null)
                            {
                                sComments = "Year built: " + row.YearBuilt + "<br />" + sComments;
                            }
                            if (row.SecurityGate != null)
                            {
                                sComments = "Locked security gate: " + row.SecurityGate + "<br />" + sComments;
                            }
                            if (row.SecurityGuard != null)
                            {
                                sComments = "Security guard: " + row.SecurityGuard + "<br />" + sComments;
                            }

                            sRush = "N";

                            // Insured name
                            if (row.InsuredFirstName != null)
                                sFirstName = row.InsuredFirstName.Trim() + " ";
                            else
                                sFirstName = "";

                            if (row.InsuredLastName != null)
                                sLastName = row.InsuredLastName.Trim();
                            else
                                sLastName = "";

                            sInsName = sFirstName + sLastName;

                            oAPI.EmailConfirmation = "";
                            oAPI.PolicyNumber = row.PolicyNumber;
                            oAPI.EffectiveDate = row.EffectiveDate;
                            oAPI.Underwriter = "";
                            oAPI.UnderwriterFirstName = "";
                            oAPI.UnderwriterLastName = "";
                            oAPI.InsuranceCompany = "";
                            oAPI.Producer = "";
                            oAPI.RushHandling = sRush;
                            oAPI.InsuredName = sInsName;
                            oAPI.ContactName = "";
                            oAPI.ContactPhoneHome = row.ContactPhoneHome;
                            oAPI.ContactPhoneWork = row.ContactPhoneWork;
                            oAPI.ContactPhoneCell = row.ContactPhoneCell;
                            oAPI.InsuredEmail = row.InsuredEmail;

                            // Test mailing address
                            // If not valid use location
                            if (IsZipCode(row.MailZip) && row.MailAddress1.Length > 0 && row.MailCity.Length > 0 && row.MailState.Length > 0)
                            {
                                oAPI.MailAddress1 = row.MailAddress1;
                                oAPI.MailAddress2 = row.MailAddress2;
                                oAPI.MailCity = row.MailCity;
                                oAPI.MailState = row.MailState;
                                oAPI.MailZip = row.MailZip;
                            }
                            else
                            {
                                oAPI.MailAddress1 = row.LocationAddress1;
                                oAPI.MailAddress2 = row.LocationAddress2;
                                oAPI.MailCity = row.LocationCity;
                                oAPI.MailState = row.LocationState;
                                oAPI.MailZip = row.LocationZip;

                                sComments += "<br />Mailing address: " + row.MailAddress1 + " " + row.MailAddress2 + " " + row.MailCity + ", " + row.MailState + " " + row.MailZip + "<br />";
                            }

                            oAPI.LocationAddress1 = row.LocationAddress1;
                            oAPI.LocationAddress2 = row.LocationAddress2;
                            oAPI.LocationCity = row.LocationCity;
                            oAPI.LocationState = row.LocationState;
                            oAPI.LocationZip = row.LocationZip;
                            oAPI.LocationContactName = "";
                            oAPI.LocationContactPhone = "";
                            oAPI.Comments = sComments;
                            oAPI.CoverageA = "";
                            oAPI.YearBuilt = row.YearBuilt;
                            oAPI.ActivityID = row.ActivityID;

                            // Agency / Agent
                            oAPI.AgencyAgentName = row.AgentFirstName.Trim() + " " + row.AgentLastName.Trim();
                            oAPI.AgentCode = "";
                            oAPI.AgencyAgentPhone = row.AgentPhone;
                            oAPI.AgentFax = "";
                            oAPI.AgencyAgentContact = "";
                            oAPI.AgentEmail = row.AgentEmail;
                            oAPI.AgentAddress1 = "5656 Central Ave";
                            oAPI.AgentAddress2 = "";
                            oAPI.AgentCity = "St. Petersburg";
                            oAPI.AgentState = "FL";
                            oAPI.AgentZip = "33707";


                            //**********************************
                            // UNCOMMENT FOR TEST 
                            //oAPI.CustomerUserName = "APITest";
                            //oAPI.CustomerAccount = "9998";
                            //oAPI.InspectionType = "9998RE";
                            //oAPI.EffectiveDate = "";

                            //**********************************

                            oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + row.PolicyNumber);
                            sbEmail.Append("Importing Policy# " + row.PolicyNumber + System.Environment.NewLine);

                            string sRet = oAPI.ImportCitizens();

                            oLU.WritetoLog("oAPI.Import return for for Policy# " + row.PolicyNumber + "\r\n\r\n" + sRet);

                            var importResults = sRet.FromJSON<List<ImportResult>>();

                            foreach (var importResult in importResults)
                            {

                                if (importResult.Successful)
                                {
                                    oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                                    sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + System.Environment.NewLine);
                                    bImportSuccess = true;
                                    buildConfLine(importResult.CaseNumber.ToString(), oAPI.PolicyNumber, sInsName, oAPI.LocationAddress1 + " " + oAPI.LocationAddress2 + " " + oAPI.LocationCity + ", " + oAPI.LocationState + " " + oAPI.LocationZip);
                                }
                                else
                                {
                                    bImportSuccess = false;
                                    oLU.WritetoLog("**** Import failed **** "); if (importResult.Errors != null)
                                        sbEmail.Append("**** Import failed ****" + System.Environment.NewLine);

                                    foreach (var error in importResult.Errors)
                                    {
                                        oLU.WritetoLog("Error: " + error.ErrorText);
                                        sbEmail.Append("Error: " + error.ErrorText + System.Environment.NewLine);
                                    }
                                }

                            }   // foreach importResult

                        }    // skip header row

                    }   // foreach row

                }   // rows > 0
                else
                {
                    // Empty file
                    oLU.WritetoLog("No rows in file");
                    sbEmail.Append("No rows in file" + System.Environment.NewLine);
                    bImportSuccess = false;
                }

            }   //try

            catch (Exception ex)
            {
                bImportSuccess = false;
                oLU.WritetoLog("Import Error: \r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail(sbEmail.ToString());
            }

            finally
            {
                // Copy source file to holding area and delete temp
                string sSourceName = cfg_sourcedir + sExcelFileName;
                string sDestName = "";

                //if successful - copy to sibidata\OrchidExcel\Processed
                //if failed - copy to sibidata\OrchidExcel\fail
                if (bImportSuccess)
                {
                    sDestName = cfg_compdir + sExcelFileName;
                    buildConfFooter();
                    sendConfEmail(sbConfEmail.ToString(), sConfEmail);
                }
                else
                {
                    sDestName = cfg_faildir + sExcelFileName;
                    sendErrEmail(sbEmail.ToString());
                }

                // Copy Excel
                File.Copy(sSourceName, sDestName);
                if (File.Exists(sDestName))
                {
                    File.Delete(sSourceName);
                }
                else
                {
                    throw new ApplicationException("Copy failed for: " + sExcelFileName);
                }

            }
            
        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import Citizens Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import Citizens Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static bool IsZipCode(string zipCode)
        {
            bool bRet = false;

            string pattern = @"^\d{5}(?:[-\s]\d{4})?$";
            Regex regex = new Regex(pattern);

            bRet =  regex.IsMatch(zipCode);

            return bRet;
        }
        static void buildConfHeader()
        {
            // begin header
            sbConfEmail.Append("<html><head><title>Sutton Inspection Bureau, Inc. of Florida</title>" + System.Environment.NewLine);

            // Style
            sbConfEmail.Append("<style type='text/css'>" + System.Environment.NewLine);
            sbConfEmail.Append(".stdText { FONT-SIZE: 8pt; COLOR: black;Font-FAMILY: Verdana,Tahoma,Arial}" + System.Environment.NewLine);

            //end header
            sbConfEmail.Append("</style></head><body>" + System.Environment.NewLine);

            // begin body
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'>" + System.Environment.NewLine);
            sbConfEmail.Append("<tr><td width='10'>&nbsp;</td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='left' width='350'><span class='stdText'>Sutton Inspection Bureau, Inc. of Florida</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='right' width='300'><span class='stdText'>&nbsp;</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("</tr><tr><td>&nbsp;</td>");
            sbConfEmail.Append("<td align='left'><span class='stdText'>Inspection Request Confirmation</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='right'><span class='stdText'>" + DateTime.Now.ToString() + "</span></td></tr></table>" + System.Environment.NewLine);

            // HR
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='0' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650'><hr align='left' width='650'></td></tr></table>" + System.Environment.NewLine);

            // Start table
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'>");

            // Header row
            sbConfEmail.Append("<tr><td align='center' width='100' class='stdText'>Case #</td>");
            sbConfEmail.Append("<td align='left' width='100' class='stdText'>Policy number</td>");
            sbConfEmail.Append("<td align='center' width='460' class='stdText'>Insured / Location</td>");
        }

        static void buildConfLine(string sCaseNum, string sPolicy, string sInsured, string sLocation)
        {
            sbConfEmail.Append("<tr><td align='center' width='100' class='stdText'>" + sCaseNum + "</td>");
            sbConfEmail.Append("<td align='left' width='100' class='stdText'> &nbsp;" + sPolicy + "</td>");
            sbConfEmail.Append("<td align='left' width='460' class='stdText'> &nbsp;" + sInsured + "</td>");
            sbConfEmail.Append("<tr><td class='stdText'> &nbsp;</td><td class='stdText'> &nbsp;</td><td align='left' class='stdText'>" + sLocation + "</td></tr>");
        }

        static void buildConfFooter()
        {
            sbConfEmail.Append("</table></form></body><HTML>");
        }

        static void sendConfEmail(string bodytext, string sConfEmail)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "inspections@sibfla.com";
            oMail.MailTo = sConfEmail;
            oMail.MailBCC = "jeff@sibfla.com";
            oMail.MsgSubject = "Inspection request confirmation";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static int getSFTP()
        {

            // return number of files downloaded or -1 on error
            int iNumDownloaded = 0;

            try
            {
                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = "sftp1.citizensfla.com",
                    UserName = "inspections_sutton_svc",
                    //Password = "121F2CLy",
                    Password = "5PMuT^QUpS&pMZn",
                    SshHostKeyFingerprint = "ssh-rsa 2048 12:52:0e:d1:16:c0:e1:ba:3d:20:81:e6:3e:3c:0c:d1"
                };

                using (Session session = new Session())
                {

                    session.SessionLogPath = @"c:\automationlogs\ImportCitizens\FTPLogs.txt";

                    // Connect
                    session.Open(sessionOptions);

                    // Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    //transferResult = session.GetFiles("CSV UPLOAD/*", cfg_sourcedir,true,transferOptions);
                    transferResult = session.GetFiles("SUTTON_OUTBOUND/*", cfg_sourcedir, true, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        oLU.WritetoLog(transfer.FileName + " downloaded");
                        sbEmail.Append("\r\n" + transfer.FileName + " downloaded");
                        iNumDownloaded++;
                    }

                }

                sbEmail.Append("\r\n" + iNumDownloaded.ToString() + " files downloaded\r\n\r\n---- End FTP transfer\r\n");
            }

            catch (Exception ex)
            {
                sbEmail.Append("**** Error in FTP Transfer\r\n\r\n" + ex.Message + "\r\n\r\n");
                iNumDownloaded = -1;
            }

            return iNumDownloaded;
        }

    }
}
