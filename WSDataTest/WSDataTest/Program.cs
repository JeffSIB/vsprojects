﻿using System;
using RestSharp;

namespace WSDataTest
{
    public class DataObject
    {
        public string Name { get; set; }
    }

    public class Class1
    {
        static void Main(string[] args)
        {

            var client = new RestClient("https://ecommerce3.sibfla.com/api/carrier/v1/CompletedInspectionFormDataSet");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/xml");
 //           request.AddParameter("application/xml", "<InspectionInfoRequest xmlns=\"http://schemas.datacontract.org/2004/07/LC360API.Carrier_V1\">\r\n    <Password>Sutton0920$</Password>\r\n    <UserName>SuttonAPITest</UserName>\r\n    <InspectionID>2bec6a6b-90a7-4d98-ae7c-7c95653f47d2</InspectionID>\r\n</InspectionInfoRequest>", ParameterType.RequestBody);
            request.AddParameter("application/xml", "<InspectionInfoRequest xmlns=\"http://schemas.datacontract.org/2004/07/LC360API.Carrier_V1\"><Password>{Sutton Provided Password}</Password> <UserName>{Sutton Supplied Username}</UserName> <InspectionID>2bec6a6b-90a7-4d98-ae7c-7c95653f47d2</InspectionID> </InspectionInfoRequest>", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
        }
    }
}
