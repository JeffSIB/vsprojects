﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="Dashboard20.Main" %>

<%@ MasterType VirtualPath="~/SiteMaster.master" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
    </script>
    <title>Sutton Inspection Bureau - Home</title>
</asp:Content>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="divCreate" style="padding: 10px; margin: 10px; border: 1px solid #808080;">
        <%--       <table style="width: 100%;">
            <tr>
                <td style="width: 80%">
                    <asp:Label ID="Label3" runat="server" CssClass="StdText" Text="Current case load"></asp:Label>
                </td>
                <td style="width: 20%; text-align: right;">
                    <asp:Label ID="lblCurrStatProcTime" runat="server" CssClass="StdText" >
							        &nbsp;As of
                    </asp:Label>
                </td>
            </tr>
        </table>--%>
        <table id="tblCurrent" style="border-right: #d3d3d3 thin solid; border-top: #d3d3d3 thin solid; border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid"
            cellspacing="0" cellpadding="0" width="100%" border="1" runat="server">
            <tr>
                <td style="width: 100%; text-align: left; background-color: #006DC5;">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 80%;">
                                <asp:Label ID="Label4" runat="server" CssClass="SectionHdrText">
							        &nbsp;Current case load
                                </asp:Label>
                            </td>
                            <td style="width: 20%; text-align: right;">
                                <asp:Label ID="Label35" runat="server" CssClass="SectionHdrTextSmall">
							        &nbsp;Last updated 
                                </asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="background-color: #fbfbfb">
                <td>
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 11%; text-align: center">
                                <asp:Label ID="Label2" runat="server" CssClass="StdTextSmallGrey">
							        Open cases
                                </asp:Label>
                            </td>
                            <td style="width: 11%; text-align: center">
                                <asp:Label ID="Label1" runat="server" CssClass="StdTextSmallGrey">
							        Pending review
                                </asp:Label>
                            </td>
                            <td style="width: 11%; text-align: center">
                                <asp:Label ID="Label5" runat="server" CssClass="StdTextSmallGrey">
							        Pending assign
                                </asp:Label>
                            </td>
                            <td style="width: 11%; text-align: center">
                                <asp:Label ID="Label6" runat="server" CssClass="StdTextSmallGrey">
							        Under 30
                                </asp:Label>
                            </td>
                            <td style="width: 11%; text-align: center">
                                <asp:Label ID="Label7" runat="server" CssClass="StdTextSmallGrey">
							        30 - 59
                                </asp:Label>
                            </td>
                            <td style="width: 11%; text-align: center">
                                <asp:Label ID="Label8" runat="server" CssClass="StdTextSmallGrey">
							        60 - 89
                                </asp:Label>
                            </td>
                            <td style="width: 11%; text-align: center">
                                <asp:Label ID="Label9" runat="server" CssClass="StdTextSmallGrey">
							        90 - 120
                                </asp:Label>
                            </td>
                            <td style="width: 11%; text-align: center">
                                <asp:Label ID="Label10" runat="server" CssClass="StdTextSmallGrey">
							        Over 120
                                </asp:Label>
                            </td>
                            <td style="width: 11%; text-align: center">
                                <asp:Label ID="Label11" runat="server" CssClass="StdTextSmallGrey">
							       Oldest
                                </asp:Label>
                            </td>

                        </tr>

                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotOpenCases" runat="server" Width="90%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotPendReview" runat="server" Width="90%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotPendAssign" runat="server" Width="90%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotUnder30" runat="server" Width="90%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTot3059" runat="server" Width="90%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTot6089" runat="server" Width="90%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTot90120" runat="server" Width="90%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotOver120" runat="server" Width="90%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lbl360Oldest" runat="server" Width="90%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>

                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <%-- TODAY --%>
        <table id="tblToday" style="border-right: #d3d3d3 thin solid; border-top: #d3d3d3 thin solid; border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid"
            cellspacing="0" cellpadding="0" width="100%" border="1" runat="server">
            <tr>
                <td style="width: 100%; text-align: left; background-color: #006DC5;">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 80%;">
                                <asp:Label ID="Label3" runat="server" CssClass="SectionHdrText">
							        &nbsp;Today
                                </asp:Label>
                            </td>
                            <td style="width: 20%; text-align: right">
                                <asp:Label ID="Label12" runat="server" CssClass="SectionHdrTextSmall">
							        &nbsp;
                                </asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="background-color: #fbfbfb">
                <td>
                    <table style="width: 100%;">
                        <tr style="text-align: center">
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label13" runat="server" CssClass="StdTextSmallGrey">
							        Ordered
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label15" runat="server" CssClass="StdTextSmallGrey">
							        Completed
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label30" runat="server" CssClass="StdTextSmallGrey">
							        Billed
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label17" runat="server" CssClass="StdTextSmallGrey">
							        Paid
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label19" runat="server" CssClass="StdTextSmallGrey">
							        Return/QA Reject
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label14" runat="server" CssClass="StdTextSmallGrey">
							        Return/QA Reject %
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotDailyNew" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotDailyComp" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotDailyBilled" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotDailyPaid" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotDailyReturned" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblDailyRejectPct" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <%-- WEEK --%>
        <table id="Table1" style="border-right: #d3d3d3 thin solid; border-top: #d3d3d3 thin solid; border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid"
            cellspacing="0" cellpadding="0" width="100%" border="1" runat="server">
            <tr>
                <td style="width: 100%; text-align: left; background-color: #006DC5;">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 80%;">
                                <asp:Label ID="Label16" runat="server" CssClass="SectionHdrText">
							        &nbsp;This Week
                                </asp:Label>
                            </td>
                            <td style="width: 20%; text-align: right">
                                <asp:Label ID="Label18" runat="server" CssClass="SectionHdrText">
							        &nbsp;
                                </asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="background-color: #fbfbfb">
                <td>
                    <table style="width: 100%;">
                        <tr style="text-align: center">
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label20" runat="server" CssClass="StdTextSmallGrey">
							        Ordered
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label21" runat="server" CssClass="StdTextSmallGrey">
							        Completed
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label22" runat="server" CssClass="StdTextSmallGrey">
							        Billed
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label23" runat="server" CssClass="StdTextSmallGrey">
							        Paid
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label24" runat="server" CssClass="StdTextSmallGrey">
							        Return/QA Reject
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label25" runat="server" CssClass="StdTextSmallGrey">
							        Return/QA Reject %
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotWeekNew" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotWeekComp" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotWeekBilled" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotWeekPaid" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotWeekReturned" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblWeekRejectPct" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <%-- MONTH --%>
        <table id="Table2" style="border-right: #d3d3d3 thin solid; border-top: #d3d3d3 thin solid; border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid"
            cellspacing="0" cellpadding="0" width="100%" border="1" runat="server">
            <tr>
                <td style="width: 100%; text-align: left; background-color: #006DC5;">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 80%;">
                                <asp:Label ID="Label26" runat="server" CssClass="SectionHdrText">
							        &nbsp;This Month
                                </asp:Label>
                            </td>
                            <td style="width: 20%; text-align: right">
                                <asp:Label ID="Label27" runat="server" CssClass="SectionHdrText">
							        &nbsp;
                                </asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="background-color: #fbfbfb">
                <td>
                    <table style="width: 100%;">
                        <tr style="text-align: center">
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label28" runat="server" CssClass="StdTextSmallGrey">
							        Ordered
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label29" runat="server" CssClass="StdTextSmallGrey">
							        Completed
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label31" runat="server" CssClass="StdTextSmallGrey">
							        Billed
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label32" runat="server" CssClass="StdTextSmallGrey">
							        Paid
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label33" runat="server" CssClass="StdTextSmallGrey">
							        Return/QA Reject
                                </asp:Label>
                            </td>
                            <td style="width: 15%; text-align: center">
                                <asp:Label ID="Label34" runat="server" CssClass="StdTextSmallGrey">
							        Return/QA Reject %
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotMonthNew" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotMonthComp" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotMonthBilled" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotMonthPaid" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotMonthReturned" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMonthRejectPct" runat="server" Width="60%" Height="15px" CssClass="CurCaseDataLable"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <table id="Table3" style="border-right: #d3d3d3 thin solid; border-top: #d3d3d3 thin solid; border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid"
            cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
            <tr>
                <td style="width: 80%; text-align: left">
                    <asp:Label ID="Label36" runat="server" CssClass="StdText">&nbsp;&nbsp;Weekly New and Completed Cases</asp:Label>
                </td>
                <td style="width: 20%; text-align: right" class="StdTextSmall">
                    <table style="border:none">
                        <tr>
                            <td>
                                <div style="width: 15px; height: 15px; background-color: darkorange">
                                    &nbsp;
                                </div>
                            </td>
                            <td>
                                <asp:Label ID="Label37" runat="server" CssClass="StdTextSmallGrey">New</asp:Label>&nbsp;/&nbsp;
                            </td>
                            <td>
                                <div style="width: 15px; height: 15px; background: navy;">
                                    &nbsp;
                                </div>
                            </td>
                            <td>
                                <asp:Label ID="Label38" runat="server" CssClass="StdTextSmallGrey">Completed</asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Image ID="cWeeklyNewComp" Width="750px" runat="server" ImageUrl="~/Images/Weekgraph.png" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
