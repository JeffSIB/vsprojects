﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Word = Microsoft.Office.Interop.Word;

namespace TowerHillDelivery
{
    class Program
    {

            static void Main(string[] args)
        {

            Setup setup = new Setup();

        }


        /// <summary>
        /// Instantiate properties from the AppSettings located in the App.config.
        /// </summary>
        public class Setup
        {

            private string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
            private string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
            private string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            private string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
            private string cfg_recroot = ConfigurationManager.AppSettings["RecRoot"];
            private string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
            private string cfg_finaldocroot = ConfigurationManager.AppSettings["FinalDocRoot"];
            private string cfg_finaldocsent = ConfigurationManager.AppSettings["FinalDocSent"];
            private string cfg_pdf2word = ConfigurationManager.AppSettings["PDF2Word"];
            private string cfg_exportapp = ConfigurationManager.AppSettings["ExportApp"];
            private string cfg_recapp = ConfigurationManager.AppSettings["RecApp"];
            private string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];


            private LogUtils.LogUtils oLU;
            private bool bErr = false;
            private string sPolicy = "";
            private string sInsured = "";
            private string sCaseNum = "";
            private string sCaseType = "";
            
            //Used for email message body.  
            private StringBuilder sbEmail = new StringBuilder();


            public Setup()
            {
                int iCasesExported = 0;

                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                bool bExport = true;
                bool bCreateDocs = true;
                bool bCreateRecs = true;
                bool bCreateFinal = true;
                bool bMailFiles = true;

                DirectoryInfo diFiles;
                DirectoryInfo diPDF;
                DirectoryInfo diDOC;
                DirectoryInfo diFinal;
                DirectoryInfo diCaseFiles;
                
                FileInfo[] fiPDFFiles;
                FileInfo[] fiDocs;
                FileInfo[] fiFinal;
                FileInfo fiWord;
                FileInfo fiRec;

                sbEmail.Append("Begin Processing  " + DateTime.Now.ToString() + "\r\n\r\n");

                //***********************************************************
                // Export PDF's from 360
                if (bExport)
                {
                    try
                    {

                        // Delete all files in PDF Export directory.
                        string[] sFiles = Directory.GetFiles(cfg_pdfroot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);                        

                        // Export PDF's from 360
                        sbEmail.Append("---- Begin exporting completed cases from 360\r\n\r\n");

                        var proc = Process.Start(cfg_exportapp);
                        proc.WaitForExit();
                        var exitCode = proc.ExitCode;
                        sbEmail.Append("Exporter returned: " + exitCode.ToString() + "\r\n");

                        // exit code should be the number of cases exported
                        iCasesExported = (int)exitCode;

                        Console.WriteLine(exitCode.ToString());

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR IN EXPORT:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                    }

                    diPDF = new DirectoryInfo(cfg_pdfroot);
                    fiPDFFiles = diPDF.GetFiles("*.pdf");

                    if (!bErr && fiPDFFiles.Count() != iCasesExported)
                    {
                        sbEmail.Append("**** PDF count does not match number exported ****\r\n");
                        sbEmail.Append("Exported: " + iCasesExported.ToString() + " - In folder: " + fiPDFFiles.Count().ToString() + "\r\n\r\n");
                        bErr = true;
                    }
                                       
                }
                //***********************************************************
                // Convert PDF to Word for all PDF files that were extracted

                if (bCreateDocs && !bErr)
                {

                    sbEmail.Append("\r\n\r\n---- Begin conversion of PDF to Word\r\n");

                    try
                    {

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        if (fiPDFFiles.Count() != iCasesExported && !bExport)
                        {
                            sbEmail.Append("**** PDF count does not match number exported ****\r\n");
                            sbEmail.Append("Exported: " + iCasesExported.ToString() + " - In folder: " + fiPDFFiles.Count().ToString() + "\r\n\r\n");
                        }


                        foreach (FileInfo file in fiPDFFiles)
                        {

                            sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));

                            ProcessStartInfo psi = new ProcessStartInfo();
                            psi.FileName = "\"" + cfg_pdf2word + "\"";
                            psi.Arguments = " -q -m -r -i " + cfg_pdfroot + file.Name + " -o " + cfg_pdfroot + sCaseNum + ".doc";

                            var proc1 = Process.Start(psi);
                            proc1.WaitForExit();
                            var exitCode1 = proc1.ExitCode;
                            Console.WriteLine(exitCode1.ToString());

                            // verify word doc was created
                            fiWord = new FileInfo(cfg_pdfroot + sCaseNum + ".doc");
                            if (fiWord.Exists)
                            {
                                // success
                                sbEmail.Append("Conversion successful: " + sCaseNum + "\r\n");
                            }
                            else
                            {
                                // fail
                                sbEmail.Append("**** Word conversion failed: " + sCaseNum + "\r\n");
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR CREATING RECS:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                    }

                    // wait 5 seconds for temp files to clear
                    System.Threading.Thread.Sleep(5000);
                    
                    diPDF = new DirectoryInfo(cfg_pdfroot);
                    fiPDFFiles = diPDF.GetFiles("*.pdf");

                    diDOC = new DirectoryInfo(cfg_pdfroot);
                    fiDocs = diDOC.GetFiles("*.doc");

                    if (fiDocs.Count() != fiPDFFiles.Count())
                    {
                        sbEmail.Append("**** DOC count does not match number exported ****");
                        sbEmail.Append("Exported: " + fiPDFFiles.Count().ToString() + " - In folder: " + fiDocs.Count().ToString() + "\r\n\r\n");
                        bErr = true;
                    }

                }

                if (bCreateRecs && !bErr)
                {

                    //***********************************************************
                    // Build Rec docs for all PDF files that were extracted


                    //************************************************
                    // Test for connection to casefiles
                    //************************************************
                    try
                    {
                        diCaseFiles = new DirectoryInfo(cfg_casefilesroot);
                        if (!diCaseFiles.Exists)
                            throw new ApplicationException("Unable to access case files root: " + cfg_casefilesroot + "\r\n");                      

                    }
                    catch (Exception ex)
                    { 
                        sbEmail.Append("**** ERROR ACCESSING CASEFILES FOLDER:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                    }

                    if (!bErr)
                    {

                        sbEmail.Append("\r\n\r\n---- Begin creating REC Docs\r\n");

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        foreach (FileInfo file in fiPDFFiles)
                        {

                            sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));

                            ProcessStartInfo psi = new ProcessStartInfo();
                            psi.FileName = cfg_recapp;
                            psi.Arguments = sCaseNum;

                            var proc1 = Process.Start(psi);
                            proc1.WaitForExit();
                            var exitCode1 = proc1.ExitCode;
                            Console.WriteLine(exitCode1.ToString());

                            // verify rec doc was created
                            fiRec = new FileInfo(cfg_recroot + sCaseNum + "Recs.docx");
                            if (fiRec.Exists)
                            {
                                // success
                                sbEmail.Append("Rec doc created for: " + sCaseNum + "\r\n");
                            }
                            else
                            {
                                // fail
                                sbEmail.Append("No rec doc for : " + sCaseNum + "\r\n");
                            }
                            fiRec = null;
                        }
                    }
                }

                if (bCreateFinal && !bErr)
                {

                    //***********************************************************
                    // Append Rec doc to report if it exists
                    // Save report as docx if no recs

                    sbEmail.Append("\r\n\r\n---- Begin creating Final Docs\r\n");

                    try
                    {

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        foreach (FileInfo file in fiPDFFiles)
                        {

                            sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));

                            // verify report doc exists
                            fiWord = new FileInfo(cfg_pdfroot + sCaseNum + ".doc");
                            if (fiWord.Exists)
                            {

                                // If there is a rec doc - append it to the report and save
                                fiRec = new FileInfo(cfg_recroot + sCaseNum + "Recs.docx");
                                if (fiRec.Exists)
                                {

                                    // Append rec doc
                                    if (AppendRecs(cfg_pdfroot + sCaseNum + ".doc", cfg_recroot + fiRec.Name, cfg_finaldocroot + sCaseNum + ".docx"))
                                    {
                                        sbEmail.Append("Rec doc appended successfully for: " + sCaseNum + "\r\n");
                                    }
                                    else
                                    {
                                        sbEmail.Append("**** Append Rec doc failed for: " + sCaseNum + "\r\n");
                                    }
                                }
                                else
                                {
                                    // No recs - save report as docs
                                    if (SaveAsDocx(cfg_pdfroot + sCaseNum + ".doc", cfg_finaldocroot + sCaseNum + ".docx"))
                                    {
                                        sbEmail.Append("Report saved as docx - no recs: " + sCaseNum + "\r\n");
                                    }
                                    else
                                    {
                                        sbEmail.Append("**** Error saving report as docx - no recs: " + sCaseNum + "\r\n");
                                    }
                                }
                            }
                            else
                            {
                                // No word doc for report
                                sbEmail.Append("**** No word file for: " + sCaseNum + "\r\n");
                            }
                        
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR IN CREATEFINAL\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                    }


                }


                //***********************************************************
                // Mail PDF's
                if (bMailFiles && !bErr)
                {

                    sbEmail.Append("\r\n\r\n---- Begin emailing reports\r\n");

                    try
                    {

                        diDOC = new DirectoryInfo(cfg_finaldocroot);
                        fiDocs = diDOC.GetFiles("*.docx");
                    
                        foreach (FileInfo file in fiDocs)
                        {
                            sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));
                            if (GetCaseInfo(sCaseNum))
                            {
                                oLU.WritetoLog("Mailing case:" + sCaseNum + "  " + sPolicy + " " + sInsured);
                                sbEmail.Append("Sending: " + sPolicy + " " + sInsured + "   " + file.FullName + "\r\n\r\n");

                                //Send non productive reports to Darlene
                                if (sCaseType.ToUpper().Contains("NON PRODUCTIVE"))
                                {
                                    oLU.WritetoLog("NON PRODUCTIVE: " + sCaseNum + "  " + sPolicy + " " + sInsured);
                                    sendCaseEmail(sInsured + " " + sPolicy, file.FullName, true);
                                }
                                else
                                {
                                    // if Drone report - add Drone to subject
                                    if (sCaseType.ToUpper().Contains("DRONE"))
                                    {
                                        sendCaseEmail("DRONE - " + sInsured + " " + sPolicy, file.FullName, false);
                                    }
                                    else
                                    {
                                        sendCaseEmail(sInsured + " " + sPolicy, file.FullName, false);
                                    }
                                }

                                System.Threading.Thread.Sleep(5000);
                                file.CopyTo(cfg_finaldocroot + "\\sent\\" + file.Name,true);
                                file.Delete();
                            }
                            else
                            {
                                sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("GetInfo failed for case: " + sCaseNum);                      
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** Error mailing report for case:" + sCaseNum + "\r\n" + ex.Message + "\r\n\r\n");
                        bErr = true;
                    }
                    
                }

                //**********************************************************
                //  TODO:
                //      Clean up PDF dir after email
                //**********************************************************

                // Done - write to log, send email  and clean up
                sbEmail.Append("\r\nEnd Processing  " + DateTime.Now.ToString() + "\r\n\r\n");
                oLU.WritetoLog(sbEmail.ToString());
                oLU.closeLog();

                if (bErr)
                {
                    sendLogEmail("**** ERROR RECORDED - TowerHill Delivery Processing", sbEmail.ToString());
                }
                else
                {
                    sendLogEmail("TowerHill Delivery Processing", sbEmail.ToString());
                }
            }

            private void sendCaseEmail(string sSubject, string sAtt, bool bNonPprod)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
                string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "sibflamail@sibfla.com";

                    if (bNonPprod)
                        //oMail.MailTo = "dohara@thig.com";
                        oMail.MailTo = "rdickens @thig.com";
                    else
                        oMail.MailTo = "comminspec@thig.com";

                    //oMail.MailTo = "jeff@sibfla.com";
                    //oMail.MailBCC = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    oMail.Attachment = sAtt;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }
                
            }

            private void sendLogEmail(string sSubject, string sBodyText)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "noreply@sibfla.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }


            }

            private bool GetCaseInfo(string sCaseNum)
            {

                bool bRetVal = false;

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;


                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseInfo_CaseNum";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {

                            sInsured = sqlReader.GetSqlString(2).ToString();
                            sPolicy = sqlReader.GetSqlString(3).ToString();
                            sCaseType = sqlReader.GetSqlString(9).ToString();

                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }

                    bRetVal = true;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();


                }

                return bRetVal;
            }

            private bool AppendRecs(string sReportDoc, string sRecDoc, string sFinalDoc)
            {
                // Append rec doc to report doc
                // Save as DOCX

                bool bRet = false;

                object oMissing = System.Reflection.Missing.Value;

                //Start Word and open report doc
                Word._Application oWord;
                Word._Document oDoc;
                oWord = new Word.Application();
                oWord.Visible = false;
                oDoc = oWord.Documents.Open(sReportDoc, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                try
                {

                    // Move to end of file
                    Word.Selection selection = oWord.Selection;
                    selection.EndKey(Word.WdUnits.wdStory);

                    // Insert blank page
                    selection.InsertNewPage();

                    // Insert rec doc
                    selection.InsertFile(sRecDoc);

                    // Set page margins
                    selection.PageSetup.TopMargin = 18;
                    selection.PageSetup.BottomMargin = 18;
                    selection.PageSetup.LeftMargin = 36;
                    selection.PageSetup.RightMargin = 36;

                    // Save doc
                    oWord.ActiveDocument.SaveAs2(sFinalDoc, Word.WdSaveFormat.wdFormatXMLDocument);
                    
                    bRet = true;

                }
                catch (Exception ex)
                {

                    //pass exception to calling proc
                    throw ex;

                }

                finally
                {
                    if (oDoc != null)
                    {
                        oDoc.Close();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                        oDoc = null;
                    }
                    if (oWord != null)
                    {
                        oWord.Quit();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oWord);
                        oWord = null;
                    }
                    GC.Collect();
                                        
                }

                return bRet;

            }

            private bool SaveAsDocx(string sReportDoc, string sFinalDoc)
            {
                // Save report doc as DOCX - used when there are no recs

                bool bRet = false;

                object oMissing = System.Reflection.Missing.Value;

                //Start Word and open report doc
                Word._Application oWord;
                Word._Document oDoc;
                oWord = new Word.Application();
                oWord.Visible = false;
                oDoc = oWord.Documents.Open(sReportDoc, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                try
                {

                    // Save as doc
                    oWord.ActiveDocument.SaveAs2(sFinalDoc, Word.WdSaveFormat.wdFormatXMLDocument);
                    bRet = true;

                }
                catch (Exception ex)
                {

                    //pass exception to calling proc
                    throw ex;

                }

                finally
                {
                    if (oDoc != null)
                    {
                        oDoc.Close();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                        oDoc = null;
                    }
                    if (oWord != null)
                    {
                        oWord.Quit();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oWord);
                        oWord = null;
                    }
                    GC.Collect();

                }

                return bRet;

            }
        }
    }
}
