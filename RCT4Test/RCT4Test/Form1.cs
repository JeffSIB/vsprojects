﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RCT4Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RCT4Utils.RCT4 oRCT4 = new RCT4Utils.RCT4();

            oRCT4.APIUserName = "Sutton_GFT";
            oRCT4.APIPassword = "Welcome18";
            oRCT4.UserLogin = "MLSutton";
            oRCT4.InsuredName = "Test Insured";
            oRCT4.Policy = "TestPolicy";
            oRCT4.StreetAddress = "4810 W Melrose Ave";
            oRCT4.City = "Tampa";
            oRCT4.State = "FL";
            oRCT4.PostalCode = "33629";
            oRCT4.ValuationUID = "9900099";
            oRCT4.CoverageAIn = 500000;

            string sRetVal = oRCT4.CreateValuation();

            tbResults.Text = sRetVal;

        }
    }
}
