﻿using System;
using WebSupergoo.ABCpdf10;
using System.Configuration;


namespace Dashboard430
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_SIBIConnStr;
        static string cfg_SIBUtilConnStr;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static string cfg_deliverto;
        static string cfg_deliverto8;
        static bool mbErr;
        static string msErrMsg;
        static string sMode;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";

            try
            {

                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                //cfg_SIBIConnStr = colNameVal.Get("SIBIConnStr");
                //cfg_SIBUtilConnStr = colNameVal.Get("SIBUtilConnStr");
                //cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_deliverto = colNameVal.Get("deliverto");
                cfg_deliverto8 = colNameVal.Get("deliverto8");

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  4 = 4:30pm
                //  7 = 7pm
                //  8 = 8am

                if (args.Length < 1)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                sMode = args[0];
                if (sMode != "4" && sMode != "7" & sMode != "8" & sMode != "0")
                {
                    throw new SystemException("Invalid parameter supplied");
                }

                // Build file name
                DateTime dRunDate = new DateTime();
                dRunDate = DateTime.Today;

                string sDateDisp = dRunDate.Month + "/" + dRunDate.Day + "/" + dRunDate.Year;
                string sFileDate = dRunDate.Month.ToString() + dRunDate.Day.ToString() + dRunDate.Year.ToString();
                string sEmailSubject = "Dashboard snapshot  " + sDateDisp + "  ";
                string sPDFFileName = "";

                if (sMode == "4")
                {
                    sEmailSubject += "4:30pm";
                    sPDFFileName = cfg_outputdir + "Dashboard1630_" + sFileDate + ".PDF";
                }
                else if (sMode == "7")
                {
                    sEmailSubject += "7:00pm";
                    sPDFFileName = cfg_outputdir + "Dashboard1900_" + sFileDate + ".PDF";
                }
                else if (sMode == "0")
                {
                    sEmailSubject += "11:30pm";
                    sPDFFileName = cfg_outputdir + "Dashboard2330_" + sFileDate + ".PDF";
                }
                else
                {
                    sEmailSubject += "8:00am";
                    sPDFFileName = cfg_outputdir + "Dashboard0800_" + sFileDate + ".PDF";
                }


                // Create PDF
                oLU.WritetoLog("Creating PDF");
                XSettings.InstallLicense("X/VKS0cNn5FhpydaGfTQKt+0efQWCtVwkfTQwuG8Vh5slArDfyGiNq1RDWxxjAcIC9Is / ztbpiBwyqAoT7N1zlM6Z2OfA3I4tGZLYdf9U03h1h3G5o9adrPFLGKub2slRr1yVsBU / kd9BSZ2piGZKQR9ey2dSHivx84 + 6lFwff7T3L0j8h3j92bBoKdMhjgYQwMI + WhIrBupGhB4B5VdkExRrUXsGdlZF + icGmnYTqqYM1NI");


                Doc docDB = new Doc();
                docDB.AddImageUrl("http://dashboard.sibfla.com/");
                docDB.Save(sPDFFileName);
                docDB.Clear();

                oLU.WritetoLog("PDF created");

                // Send email
                oLU.WritetoLog("Sending email");
                sendPDF(sEmailSubject, sPDFFileName);
                oLU.WritetoLog("Done");

            }
            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
            }
        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Dashboard430 Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet != "")
            {
                oLU.WritetoLog("sendErrEmail returned: " + sRet);
            }
        }

        static void sendPDF(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Dashboard snapshot generated " + DateTime.Now.ToString();

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";

                if (sMode == "0" || sMode == "8")
                    oMail.MailTo = cfg_deliverto8;
                else
                    oMail.MailTo = cfg_deliverto;

                //oMail.MailTo = "jeff@sibfla.com";
                oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;

                if (sRet != "")
                {
                    throw new Exception("SendPDF returned: " + sRet);
                }

            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;
            }
        }
    }
}
