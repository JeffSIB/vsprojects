Imports System.Net.Mail
Imports System.Net.Mime


Public Class SendMail

    Dim msMailTo As String = ""
    Dim msMailFrom As String = ""
    Dim msMailCC As String = ""
    Dim msMailBCC As String = ""
    Dim msMsgSubject As String = ""
    Dim msMsgBody As String = ""
    Dim msSMTPServer As String = ""
    Dim mbSendHTML As Boolean = False
    Dim msAttachment As String = ""
    Dim msAttachment2 As String = ""

    Public Property MailTo() As String
        Get
            Return msMailTo
        End Get
        Set(ByVal Value As String)
            msMailTo = Value
        End Set
    End Property

    Public Property MailFrom() As String
        Get
            Return msMailFrom
        End Get
        Set(ByVal Value As String)
            msMailFrom = Value
        End Set
    End Property
    Public Property MailCC() As String
        Get
            Return msMailCC
        End Get
        Set(ByVal Value As String)
            msMailCC = Value
        End Set
    End Property
    Public Property MailBCC() As String
        Get
            Return msMailBCC
        End Get
        Set(ByVal Value As String)
            msMailBCC = Value
        End Set
    End Property

    Public Property MsgSubject() As String
        Get
            Return msMsgSubject
        End Get
        Set(ByVal Value As String)
            msMsgSubject = Value
        End Set
    End Property
    Public Property MsgBody() As String
        Get
            Return msMsgBody
        End Get
        Set(ByVal Value As String)
            msMsgBody = Value
        End Set
    End Property

    Public Property SMTPServer() As String
        Get
            Return msSMTPServer
        End Get
        Set(ByVal Value As String)
            msSMTPServer = Value
        End Set
    End Property

    Public WriteOnly Property SendHTML() As Boolean
        Set(ByVal Value As Boolean)
            mbSendHTML = Value
        End Set
    End Property
    Public WriteOnly Property Attachment() As String
        Set(ByVal Value As String)
            msAttachment = Value
        End Set
    End Property
    Public WriteOnly Property Attachment2() As String
        Set(ByVal Value As String)
            msAttachment2 = Value
        End Set
    End Property
    Public Function Send() As String

        Dim sRet As String = ""

        Try


            'string[] words = s.Split(' ');
            ' foreach (string word in words)
            ' {
            '     Console.WriteLine(word);
            ' }

            Dim message As New MailMessage()

            Dim mailClient As New SmtpClient(msSMTPServer)
            mailClient.UseDefaultCredentials = True


            Dim sTo As String()
            sTo = msMailTo.Split(";")

            Dim sRecip As String
            For Each sRecip In sTo

                If sRecip.Length > 0 Then
                    message.To.Add(New MailAddress(sRecip))
                End If

            Next

            Dim sCC As String()
            sCC = msMailCC.Split(";")

            Dim sCCRecip As String
            For Each sCCRecip In sCC

                If sCCRecip.Length > 0 Then
                    message.CC.Add(New MailAddress(sCCRecip))
                End If

            Next

            Dim sBCC As String()
            sBCC = msMailBCC.Split(";")

            Dim sBCCRecip As String
            For Each sBCCRecip In sBCC

                If sBCCRecip.Length > 0 Then
                    message.Bcc.Add(New MailAddress(sBCCRecip))
                End If

            Next

            'If msMailBCC.Length > 0 Then
            '    Dim maBCC As New MailAddress(msMailBCC)
            '    message.Bcc.Add(maBCC)
            'End If

            message.From = New MailAddress(msMailFrom)
            message.Subject = msMsgSubject
            message.Body = msMsgBody

            ' Add attachment if necessary
            If msAttachment.Length > 0 Then

                ' Create  the file attachment  
                Dim data As New Attachment(msAttachment, MediaTypeNames.Application.Octet)

                ' Add the file attachment to the message
                message.Attachments.Add(data)
            End If

            ' Add second attachment if necessary
            If msAttachment2.Length > 0 Then

                ' Create  the file attachment  
                Dim data2 As New Attachment(msAttachment2, MediaTypeNames.Application.Octet)

                ' Add the file attachment to the message
                message.Attachments.Add(data2)
            End If

            If msMailBCC.Length > 0 Then
                Dim maBCC As New MailAddress(msMailBCC)
                message.Bcc.Add(maBCC)
            End If

            If mbSendHTML Then
                message.IsBodyHtml = True
            Else
                message.IsBodyHtml = False
            End If

            mailClient.Send(message)

            For Each att As Net.Mail.Attachment In message.Attachments
                att.Dispose()
            Next

            message.Attachments.Dispose()
            message = Nothing


            mailClient.Dispose()

        Catch e As System.Exception
            sRet = e.Message

        End Try

        Return sRet

    End Function


End Class
