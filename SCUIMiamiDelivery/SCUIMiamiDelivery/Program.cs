﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Word = Microsoft.Office.Interop.Word;
using WinSCP;


namespace SCUIMiamiDelivery
{
    class Program
    {

        static void Main(string[] args)
        {

            Setup setup = new Setup();

        }


        /// <summary>
        /// Instantiate properties from the AppSettings located in the App.config.
        /// </summary>
        public class Setup
        {

            private string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
            private string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
            private string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            private string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
            private string cfg_recroot = ConfigurationManager.AppSettings["RecRoot"];
            private string cfg_FTPRoot = ConfigurationManager.AppSettings["FTPRoot"];
            private string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
            private string cfg_exportapp = ConfigurationManager.AppSettings["ExportApp"];
            private string cfg_recapp = ConfigurationManager.AppSettings["RecApp"];
            private string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
            private string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];

            private LogUtils.LogUtils oLU;
            private bool bErr = false;
            private string sPolicy = "";
            private string sInsured = "";
            private string sCaseNum = "";
            private Guid guCaseID;
            private string sIRFileNum = "";
            private string sCustNum = "";
            private string sCustName = "";
            private string sAmount = "";
            private string sOutputFileName = "";
            private string sDeliveryDate = "";
            private string sBatchNum = "";

            //Used for email message body.  
            private StringBuilder sbEmail = new StringBuilder();

            //Used for transmittal message body.  
            private StringBuilder sbmsgBody = new StringBuilder();

            public Setup()
            {
                int iCasesExported = 0;

                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                bool bExport = true;
                bool bCreateRecs = false;   // no recs as of 5/11/17
                bool bFTPFiles = true; 

                DirectoryInfo diPDF;
                DirectoryInfo diRec;
                DirectoryInfo diCaseFiles;

                FileInfo[] fiPDFFiles;
                FileInfo[] fiRECFiles;
                FileInfo fiRec;
                oLU.WritetoLog("Begin Processing  " + DateTime.Now.ToString());
                sbEmail.Append("Begin Processing  " + DateTime.Now.ToString() + "\r\n\r\n");

                //***********************************************************
                // Export PDF's from 360
                if (bExport)
                {
                    try
                    {

                        // Delete all files in PDF Export directory.
                        string[] sFiles = Directory.GetFiles(cfg_pdfroot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);

                        // Export PDF's from 360
                        oLU.WritetoLog(" Begin exporting completed cases from 360");
                        sbEmail.Append("---- Begin exporting completed cases from 360\r\n\r\n");
                        oLU.WritetoLog(cfg_exportapp);

                        var proc = Process.Start(cfg_exportapp);
                        proc.WaitForExit();
                        var exitCode = proc.ExitCode;
                        sbEmail.Append("Exporter returned: " + exitCode.ToString() + "\r\n");

                        // exit code should be the number of cases exported
                        iCasesExported = (int)exitCode;

                        Console.WriteLine(exitCode.ToString());

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR IN EXPORT:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                    }

                    diPDF = new DirectoryInfo(cfg_pdfroot);
                    fiPDFFiles = diPDF.GetFiles("*.pdf");

                    if (fiPDFFiles.Count() != iCasesExported)
                    {
                        sbEmail.Append("**** PDF count does not match number exported ****\r\n");
                        sbEmail.Append("Exported: " + iCasesExported.ToString() + " - In folder: " + fiPDFFiles.Count().ToString() + "\r\n\r\n");
                    }

                }

                if (bCreateRecs && !bErr)
                {

                    //***********************************************************
                    // Build Rec docs for all PDF files that were extracted


                    //************************************************
                    // Test for connection to casefiles
                    //************************************************
                    try
                    {
                        diCaseFiles = new DirectoryInfo(cfg_casefilesroot);
                        if (!diCaseFiles.Exists)
                            throw new ApplicationException("Unable to access case files root: " + cfg_casefilesroot + "\r\n");

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR ACCESSING CASEFILES FOLDER:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                    }

                    if (!bErr)
                    {

                        // Delete all files in Rec folder.
                        string[] sFiles = Directory.GetFiles(cfg_recroot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);

                        sbEmail.Append("\r\n\r\n---- Begin creating REC Docs\r\n");

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        foreach (FileInfo file in fiPDFFiles)
                        {

                            sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));

                            ProcessStartInfo psi = new ProcessStartInfo();
                            psi.FileName = cfg_recapp;
                            psi.Arguments = sCaseNum;

                            var proc1 = Process.Start(psi);
                            proc1.WaitForExit();
                            var exitCode1 = proc1.ExitCode;
                            Console.WriteLine(exitCode1.ToString());

                            // verify rec doc was created
                            fiRec = new FileInfo(cfg_recroot + sCaseNum + ".PDF");
                            if (fiRec.Exists)
                            {
                                // success
                                sbEmail.Append("Rec doc created for: " + sCaseNum + "\r\n");
                            }
                            else
                            {
                                // fail
                                sbEmail.Append("No rec doc for : " + sCaseNum + "\r\n");
                            }
                            fiRec = null;
                        }
                    }
                }

                //***********************************************************
                // rename PDF's & Recs to ImageRight file spec
                // create transmittal email and send if FTP is successful
                // copy to FTP folder
                if (bFTPFiles && !bErr)
                {

                    // Delete all files in FTP folder.
                    string[] sFiles = Directory.GetFiles(cfg_FTPRoot);
                    foreach (string sFile in sFiles)
                        File.Delete(sFile);

                    sbEmail.Append("\r\n\r\n---- Begin renaming reports\r\n");

                    int iUnique = 1;

                    // Create email header
                    DateTime dNow = DateTime.Today;
                    sDeliveryDate = dNow.ToShortDateString();
                    sBatchNum = dNow.Year.ToString() + dNow.Month.ToString() + dNow.Day.ToString();
                    buildHeader();

                    ///////////////////////////////////
                    // PDF's
                    ///////////////////////////////////
                    try
                    {

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        // for each PDF 
                        foreach (FileInfo pdfFile in fiPDFFiles)
                        {

                            // Get case # from file name
                            sCaseNum = pdfFile.Name.Substring(0, pdfFile.Name.IndexOf("."));

                            // Get CaseID & customer number
                            if (!GetCaseInfo(sCaseNum))
                            {
                                sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("GetInfo failed for case: " + sCaseNum);
                            }

                            if (sIRFileNum == "")
                            {
                                sIRFileNum = "99999999";
                            }

                            // replace any illegal characters with "-"
                            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                            {
                                sIRFileNum = sIRFileNum.Replace(c, '-');
                            }

                            // Create email line item
                            buildLineItem(sPolicy,sIRFileNum,sInsured,sCaseNum,sAmount);

                            // Build Report file name
                            sOutputFileName = cfg_FTPRoot + "FLPC_" + sIRFileNum + "_11000_INSP_________Inspection_36_1_NULL_5_Inspection__C__";

                            //    MIAMI                 sOutputFileName = cfg_FTPRoot + "SCTP_" + sIRFileNum + "_" + "Policy Information 0" + "_" + "INSP" + "__" + "_______" + "Insp Rcvd" + "_" + "612" + "_" + "614" + "_" + "CHDAVIS" + "_" + "5" + "_" + "Inspection Received" + "__" + "C" + "_" + "SCTP" + "_";
                            // FLPC_NPP8397934_11000_INSP_________Inspection_36_1_NULL_5_Inspection__C__1.pdf


                            // If it exists - add digit to end
                            if (File.Exists(sOutputFileName + iUnique.ToString() + ".pdf"))
                            {

                                iUnique = 2;
                                while (true)
                                {

                                    if (File.Exists(sOutputFileName + iUnique.ToString() + ".pdf"))
                                    {
                                        iUnique++;
                                        if (iUnique > 10)
                                        {
                                            throw new ApplicationException(sOutputFileName + iUnique.ToString() + ".pdf" + " could not be saved - exists.");
                                        }

                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }

                            // Copy PDF to FTP folder under new name
                            pdfFile.CopyTo(sOutputFileName + iUnique.ToString() + ".pdf");
                        }

                        // build email footer
                        buildFooter();

                        ////////////////////////////////////////
                        //Recs
                        ///////////////////////////////////////
                        iUnique = 1;
                        diRec = new DirectoryInfo(cfg_recroot);
                        fiRECFiles = diRec.GetFiles("*.pdf");

                        // for each PDF 
                        foreach (FileInfo fiRecDoc in fiRECFiles)
                        {

                            // Get case # from file name
                            sCaseNum = fiRecDoc.Name.Substring(0, fiRecDoc.Name.IndexOf("."));

                            // Get CaseID & customer number
                            if (!GetCaseInfo(sCaseNum))
                            {
                                sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("GetInfo failed for case: " + sCaseNum);
                            }
                            if (sIRFileNum == "")
                            {
                                sIRFileNum = "99999999";
                            }

                            // replace any illegal characters with "-"
                            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                            {
                                sIRFileNum = sIRFileNum.Replace(c, '-');
                            }

                            // Build Rec file name
                            sOutputFileName = cfg_FTPRoot + "SCTP_" + sIRFileNum + "_" + "Policy Information 0" + "_" + "INSP" + "__" + "_______" + "Recommendation Memo" + "_" + "" + "_" + "" + "_" + "CHDAVIS" + "_" + "5" + "_" + "Inspection Received" + "__" + "C" + "_" + "SCTP" + "_";

                            // If it exists - add digit to end
                            if (File.Exists(sOutputFileName + iUnique.ToString() + ".pdf"))
                            {

                                iUnique = 2;
                                while (true)
                                {

                                    if (File.Exists(sOutputFileName + iUnique.ToString() + ".pdf"))
                                    {
                                        iUnique++;
                                        if (iUnique > 10)
                                        {
                                            throw new ApplicationException(sOutputFileName + iUnique.ToString() + ".pdf" + " could not be saved - exists.");
                                        }

                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }

                            // Copy to FTP folder under new name
                            fiRecDoc.CopyTo(sOutputFileName + iUnique.ToString() + ".pdf");
                        }   // for each rec

                    }
                    catch (Exception ex)
                    {
                        bErr = true;
                        sbEmail.Append("**** Error processing files for case:" + sCaseNum + "\r\n" + ex.Message + "\r\n\r\n");
                    }

//***********************
//                    sendTransmittal();
//***********************
                    //////////////////////////////
                    // FTP all files in PDF folder
                    //////////////////////////////
                    int iNumUploaded = 0;
                    int iNumToUpload = 0;

                    diPDF = new DirectoryInfo(cfg_FTPRoot);
                    fiPDFFiles = diPDF.GetFiles("*.*");

                    iNumToUpload = fiPDFFiles.Count();
                    if (iNumToUpload == 0)
                    {
                        sbEmail.Append("\r\n\r\n---- No Files to FTP\r\n");
                    }
                    else
                    {
                        sbEmail.Append("\r\n\r\n---- Begin FTP transfer\r\n");
                        sbEmail.Append("\r\n\r\n" + iNumToUpload.ToString() + " files to upload\r\n");
                        try
                        {
                            // Setup session options
                            SessionOptions sessionOptions = new SessionOptions
                            {
                                Protocol = Protocol.Sftp,
                                HostName = "Sftp.scui.com",
                                UserName = "SibFla",
                                Password = "W36CkHwA",
                                SshHostKeyFingerprint = "ssh-dss 1024 80:83:cd:a3:b2:eb:90:c1:b7:8d:81:62:db:a3:79:21",
                            };

                            using (Session session = new Session())
                            {

                                session.SessionLogPath = @"\\nas\apps\automationlogs\SCUIMiami\FTPLogs.txt";

                                // Connect
                                session.Open(sessionOptions);

                                // Upload files
                                TransferOptions transferOptions = new TransferOptions();
                                transferOptions.TransferMode = TransferMode.Binary;
                                transferOptions.ResumeSupport.State = TransferResumeSupportState.Off;

                                TransferOperationResult transferResult;
                                transferResult = session.PutFiles(cfg_FTPRoot + "*", "/Usr/SibFla/Miami/", false, transferOptions);

                                // Throw on any error
                                transferResult.Check();

                                // Print results
                                foreach (TransferEventArgs transfer in transferResult.Transfers)
                                {
                                    oLU.WritetoLog(transfer.FileName + " uploaded");
                                    iNumUploaded++;
                                }

                            }

                            sbEmail.Append("\r\n" + iNumUploaded.ToString() + " files uploaded\r\n\r\n---- End FTP transfer\r\n");
                        }
                        catch (Exception ex)
                        {
                            bErr = true;
                            sbEmail.Append("**** Error in FTP Transfer\r\n\r\n" + ex.Message + "\r\n\r\n");
                        }

                    }
                    // Done - write to log, send email  and clean up
                    sbEmail.Append("\r\nEnd Processing  " + DateTime.Now.ToString() + "\r\n\r\n");
                    oLU.WritetoLog(sbEmail.ToString());
                    oLU.closeLog();

                    if (iNumUploaded > 0)
                    {
                        // No transmittal per Jonathan 5/11/17
                        //sendTransmittal();
                    }

                }    //if FTP

                if (bErr)
                {
                    sendLogEmail("**** ERROR RECORDED - SCUI Miami Delivery Processing ****", sbEmail.ToString());
                }
                else
                {
                    sendLogEmail("SCUI Miami Delivery Processing", sbEmail.ToString());
                }

            }  


            private void sendLogEmail(string sSubject, string sBodyText)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "noreply@sibfla.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;
                }
            }

            private void sendTransmittal()
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "sibflamail@sibfla.com";
                    //oMail.MailTo = "chdavis@scui.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    oMail.MailBCC = "jeff@sibfla.com";
                    oMail.MsgSubject = "Transmittal - " + DateTime.Today.ToShortDateString();
                    oMail.MsgBody = sbmsgBody.ToString();
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = true;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;
                }
            }
            private bool GetCaseInfo(string sCaseNum)
            {

                bool bRetVal = false;

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseInfo_CaseNum";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {
                            guCaseID = sqlReader.GetGuid(1);
                            sPolicy = sqlReader.GetSqlString(3).ToString();
                            sInsured = sqlReader.GetSqlString(2).ToString();
                            sCustName = sqlReader.GetSqlString(10).ToString().Trim();
                            sCustNum = sqlReader.GetSqlString(11).ToString().Trim();
                            sAmount = sqlReader.GetSqlDecimal(12).ToString();
                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }

                    // IR FileNum
                    sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                    sqlCmd1.Parameters.AddWithValue("@fieldname", "ImageRight File Number");

                    sIRFileNum = "";
                    object oRetVal = sqlCmd1.ExecuteScalar();

                    if (oRetVal != null)
                    {
                        sIRFileNum = (string)oRetVal;
                    }

                    if (sIRFileNum == "")
                    {
                        sbEmail.Append("\r\n" + "No IR File Num for " + sCaseNum + " \r\n");
                    }

                    bRetVal = true;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();



                }

                return bRetVal;
            }

            private void buildHeader()
            {

                // <head>
                sbmsgBody = sbmsgBody.Append("<html><head><title>Sutton Inspection Bureau, Inc.of Florida</title> " + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<style type='text/css'>.stdText {font-size: 8pt;COLOR: black;font-family: Verdana,Tahoma,Arial;}" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append(".stdTextBold {font-size: 8pt;color: black;font-family: Verdana,Tahoma,Arial;font-weight: bold;}" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append(".largeText {font-size: 12pt;color: black;font-family: Verdana,Tahoma,Arial;font-weight: bold;}</style></head>" + System.Environment.NewLine);

                // <body>
                sbmsgBody = sbmsgBody.Append("<body><table cellSpacing = '0' cellPadding = '2' width = '760' border = '0'><tr><td width = '10'> &nbsp;</td><td align = 'left' width = '475'><span class='stdText'>Sutton Inspection Bureau, Inc.of Florida</span></td><td width = '275'> &nbsp;</td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td> &nbsp;</td><td align = 'left'><span class='stdText'>5656 Central Avenue</span></td><td align = 'right'><span class='largeText'>Delivery Transmittal</span></td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td> &nbsp;</td><td align = 'left'><span class='stdText'>St.Petersburg,  FL 33707-1718</span></td><td>&nbsp;</td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='0' width='760' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='750'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='760' border='0'><tr><td width = '10'>&nbsp;</td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align= right' width= 60'><span Class='stdText'>Account:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='left' width='340'><span Class='stdText'>SCU - Miami</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right' width='240'><span Class='stdText'>Delivery date:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right' width='110'><span Class='stdText'>" + sDeliveryDate + "</span></td></tr>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right'><span Class='stdText'>Batch Number:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right'><span Class='stdText'>" + sBatchNum.Trim() + "</span></td></tr></table>" + System.Environment.NewLine);

                //sbmsgBody = sbmsgBody.Append("</table>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='0' width='760' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='750'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='760' style='border - bottom:solid; '><tr><td align = 'center' width = '120'><span Class='stdTextBold'> Policy </span></td>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> IR File</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='360'><span Class='stdTextBold'> Insured </span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> Case</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> Amount </span></td></tr>" + System.Environment.NewLine);

            }

            private void buildFooter()
            {
                sbmsgBody = sbmsgBody.Append("</table></body></html>");

            }

            private void buildLineItem(string sPolicy, string sIR, string sInsured, string sCaseNum, string sAmount)
            {

                sbmsgBody = sbmsgBody.Append("<tr><td align='center'><span Class='stdText'>" + sPolicy + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'> " + sIR + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='left'><span Class='stdText'>" + sInsured + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'>" + sCaseNum + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'>" + sAmount + "</span></td></tr>");
                
            }
        }
    }
}
