﻿using CE_AddUpdate.IValuationService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CE_AddUpdate
{
    class CEGetValuation
    {
        private string _contextUserName = ConfigurationManager.AppSettings["ContextUserName"];
        private string _contextUserPassword = ConfigurationManager.AppSettings["ContextUserPassword"];
        private string _contextAlias = ConfigurationManager.AppSettings["ContextAlias"];
        private Guid _contextCompanyId = new Guid(ConfigurationManager.AppSettings["CompanyId"]);
        private string _contextLogonUser = ConfigurationManager.AppSettings["LogonUser"];

        private ChannelFactory<IValuation> _channelFactory = new ChannelFactory<IValuation>("ClearUsernameBinding_IValuation");
        ResponseGetValuation _response = null;
        public CEGetValuation()
        {
            _channelFactory.Credentials.UserName.UserName = _contextUserName + "@" + _contextAlias;
            _channelFactory.Credentials.UserName.Password = _contextUserPassword;
        }

        public string CEGetValuationbyId(long valuationid)
        {
            string policynumber = "";
                
            IValuation ivaluation = _channelFactory.CreateChannel();

             GetValuationRequest request = new GetValuationRequest();

            request.CompanyID = _contextCompanyId;
            request.Logon = _contextLogonUser;

            request.ValuationID = valuationid;

            try
            {
                _response = ivaluation.GetValuation(request);

                if ((_response.Errors.Length > 0))
                {
                    foreach (ErrorDetail error in _response.Errors)
                    {
                        MessageBox.Show(error.Description);
                    }
                }
                else
                {
                    if (_response.Status == ResponseStatusType.Success)
                    {
                        policynumber = _response.Valuation.ValuationNumber;
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }

            return policynumber; //0 = unsuccessful response
        }

        public ResponseGetValuation ReturnGetValuationResponse()
        {
            return _response;
        }
    }
}


