﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CE_AddUpdate
{
    public partial class frmMain : Form
    {
        CEGetValuation nmGetValuation = null;
        public frmMain()
        {
            InitializeComponent();

            txtBusName.Text = "Test Business Name";
            txtAddress.Text = "32325 Marchmont Cr";
            txtCity.Text = "Dade City";
            txtState.Text = "FL";
            txtZip.Text = "33523";            

        }

        private void btAddVal_Click(object sender, EventArgs e)
        {
            CEAddValuationMin nmAddValuationMin = new CEAddValuationMin();
            long newValuationId = nmAddValuationMin.CEAddValuationMinimum(txtBusName.Text, txtAddress.Text, txtCity.Text, txtState.Text, txtZip.Text);

            txtValuationId.Text = newValuationId.ToString();
        }

        private void btGet_Click(object sender, EventArgs e)
        {
            nmGetValuation = new CEGetValuation();
            string curPolNum = nmGetValuation.CEGetValuationbyId(Convert.ToInt32(txtValuationId.Text));

            txtCurPolNum.Text = curPolNum;
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            CEUpdateValuation nmUpdateValuation = new CEUpdateValuation();
            nmUpdateValuation.UpdateValuation(txtNewPolicyNumber.Text, nmGetValuation.ReturnGetValuationResponse());
        }
    }
}
