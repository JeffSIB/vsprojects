﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="GoogleAPITest.Main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Your Data on Google Map </title>
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=AIzaSyBGuqU1wHBdKT55s2rPWXBdGdOPGK2Xl0M" type="text/javascript">
    </script>
    <script type='text/javascript'>
        function initialize() {
            if (GBrowserIsCompatible()) {
                var map = new GMap2(document.getElementById('map_canvas'));
                map.setCenter(new GLatLng(51.5, -0.1167), 2);
                map.addOverlay(new GMarker(new GLatLng(43.6667, -79.4168)));
                //map.setUIToDefault();
            }
        } </script>
</head>

<body onload="initialize()" onunload="GUnload()">
    <form id="form1" runat="server">
        <asp:Panel ID="Panel1" runat="server">
            <asp:Literal ID="js" runat="server"></asp:Literal>
            <div id="map_canvas" style="width: 100%; height: 728px; margin-bottom: 2px;">
            </div>
        </asp:Panel>
    </form>
</body>
</html>
