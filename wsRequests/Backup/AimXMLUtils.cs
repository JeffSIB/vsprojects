using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.IO;
using ImportRequest360;




namespace AIMXML
{
    public class AIMXMLUtils
    {

        // module wide vars
        //XmlTextReader XTR;
        string msReturnStr;
        
        // Property Vars
        private string msXSDFile;
        private string msXMLFile;
        private string msXMLFilePath;
        private string msUserID;
        private string msSQL360ConnectStr;
        private string msSQLSIBConnectStr;

        private string msReturnKey;
        private string msSMTPerver;


        public struct psKeyData
        {
            public string sRequestType;
            public string sMasterAcnt;
            public string sRequestedBy;
            public string sEmailConfirmation;
            public string sAgencyAgentName;
            public string sAgencyAgentContactName;
            public string sAgencyAgentPhone;
            public string sAgentAddress1;
            public string sAgentAddress2;
            public string sAgentCity;
            public string sAgentState;
            public string sAgentZip;
            public string sAgentFax;
            public string sAgentCode;
            public string sPolicyNumber;
            public string sInsuranceCo;
            public string sProducer;
            public string sRushHandling;
            public string sInsuredName;
            public string sMailAddress1;
            public string sMailAddress2;
            public string sMailCity;
            public string sMailState;
            public string sMailZip;
            public string sContactName;
            public string sContactPhone;
            public string sContactPhoneHome;
            public string sContactPhoneWork;
            public string sContactPhoneCell;
            public string sBusinessOperations;
            public string sLocationAddress1;
            public string sLocationAddress2;
            public string sLocationCity;
            public string sLocationState;
            public string sLocationZip;
            public string sLocationContactName;
            public string sLocationContactPhone;
            public string sComments;
            public string sPhotoCode;
            public string sPhotoOther;
            public string sAuto;
            public string sLiability;
            public string sProperty;
            public string sPackage;
            public string sMiscellaneous;
            public string sInspectionType;
            public string sEffectiveDate;
            public string sUnderwriter;
            public string sUnderwriterFirst;
            public string sUnderwriterLast;
            public string sUnderwriterCorrEmail;
            public string sUnderwriterRptEmail;
            public string sUnderwriterPhone;
            public string sBuildingCost;
            public string sBusinessTotalRevenue;
            public string sContentsCost;
            public string sCoverageA;
            public string sISOClass;
            public string sOccupancy;
            public string sYearBuilt;
            public string sImageRightFileNum;
            public string sImageRightDrawer;
            public string sImageRightUserID;
            public string sGenericFieldName;
            public string sGenericFieldValue;
            public string sGenericFieldName2;
            public string sGenericFieldValue2;
            public string sGenericFieldName3;
            public string sGenericFieldValue3;
        }

        private psKeyData sKeyData = new psKeyData();

        // Property declarations

		// Schema file:
		public string SchemaFile
		{
			get 
			{
                return msXSDFile; 
			}
			set 
			{
                msXSDFile = value; 
			}
		}

        // XML file:
        public string XMLFile
        {
            get
            {
                return msXMLFile;
            }
            set
            {
                msXMLFile = value;
            }
        }

        // XML file path:
        public string XMLFilePath
        {
            get
            {
                return msXMLFilePath;
            }
            set
            {
                msXMLFilePath = value;
            }
        }

        // UserID:
        public string UserID
        {
            get
            {
                return msUserID;
            }
            set
            {
                msUserID = value;
            }
        }
        
        // SQL360ConnectStr:
        public string SQL360ConnectStr
        {
            get
            {
                return msSQL360ConnectStr;
            }
            set
            {
                msSQL360ConnectStr = value;
            }
        }

        // SQLSIBConnectStr:
        public string SQLSIBConnectStr
        {
            get
            {
                return msSQLSIBConnectStr;
            }
            set
            {
                msSQLSIBConnectStr = value;
            }
        }

        // SMTP Server
        public string SMTPServer
        {
            get
            {
                return msSMTPerver;
            }
            set
            {
                msSMTPerver = value;
            }
        }


        /// <summary>
        /// createKey 
        /// </summary>
        /// <returns></returns>
        public string createKey()
        {

            string sXMLFile = msXMLFilePath + msXMLFile;
            msReturnStr = "";
            string sErrText = "";

            XmlTextReader reader = null;

            try
            {

                // verify xml file exists
                if (!File.Exists(sXMLFile))
                {
                    throw new ApplicationException("File does not exist: " + sXMLFile);
                }

                sKeyData.sMasterAcnt = "";
                sKeyData.sRequestType = "";
                sKeyData.sRequestedBy = "";
                sKeyData.sEmailConfirmation = "";
                sKeyData.sAgencyAgentName = "";
                sKeyData.sAgencyAgentContactName = "";
                sKeyData.sAgencyAgentPhone = "";
                sKeyData.sAgentAddress1 = "";
                sKeyData.sAgentAddress2 = "";
                sKeyData.sAgentCity = "";
                sKeyData.sAgentState = "";
                sKeyData.sAgentZip = "";
                sKeyData.sAgentCode = "";
                sKeyData.sAgentFax = "";
                sKeyData.sPolicyNumber = "";
                sKeyData.sInsuranceCo = "";
                sKeyData.sProducer = "";
                sKeyData.sRushHandling = "N";
                sKeyData.sInsuredName = "";
                sKeyData.sMailAddress1 = "";
                sKeyData.sMailAddress2 = "";
                sKeyData.sMailCity = "";
                sKeyData.sMailState = "";
                sKeyData.sMailZip = "";
                sKeyData.sContactName = "";
                sKeyData.sContactPhone = "";
                sKeyData.sContactPhoneHome = "";
                sKeyData.sContactPhoneWork = "";
                sKeyData.sContactPhoneCell = "";
                sKeyData.sBusinessOperations = "";
                sKeyData.sLocationAddress1 = "";
                sKeyData.sLocationAddress2 = "";
                sKeyData.sLocationCity = "";
                sKeyData.sLocationState = "";
                sKeyData.sLocationZip = "";
                sKeyData.sLocationContactName = "";
                sKeyData.sLocationContactPhone = "";
                sKeyData.sComments = "";
                sKeyData.sPhotoCode = "";
                sKeyData.sPhotoOther = "";
                sKeyData.sAuto = "";
                sKeyData.sLiability = "";
                sKeyData.sProperty = "";
                sKeyData.sPackage = "";
                sKeyData.sMiscellaneous = "";
                sKeyData.sInspectionType = "";
                sKeyData.sEffectiveDate = "";
                sKeyData.sUnderwriter = "";
                sKeyData.sUnderwriterFirst = "";
                sKeyData.sUnderwriterLast = "";
                sKeyData.sUnderwriterCorrEmail = "";
                sKeyData.sUnderwriterRptEmail = "";
                sKeyData.sUnderwriterPhone = "";
                sKeyData.sBuildingCost = "";
                sKeyData.sBusinessTotalRevenue = "";
                sKeyData.sContentsCost = "";
                sKeyData.sCoverageA = "";
                sKeyData.sISOClass = "";
                sKeyData.sOccupancy = "";
                sKeyData.sYearBuilt = "";
                sKeyData.sImageRightFileNum = "";
                sKeyData.sImageRightDrawer = "";
                sKeyData.sImageRightUserID = "";
                sKeyData.sGenericFieldName = "";
                sKeyData.sGenericFieldValue = "";
                sKeyData.sGenericFieldName2 = "";
                sKeyData.sGenericFieldValue2 = "";
                sKeyData.sGenericFieldName3 = "";
                sKeyData.sGenericFieldValue3 = "";

                // Load the reader with the data file and ignore all white space nodes.         
                reader = new XmlTextReader(sXMLFile);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                XmlReaderSettings readersettings = new XmlReaderSettings();
                readersettings.CheckCharacters = false;

                // Parse the file and store each of the elements in a structure.
                while (reader.Read())
                {

                    if (reader.MoveToContent() == XmlNodeType.Element && reader.Name != "Request")
                    {

                        switch (reader.Name)
                        {
                            case "CustomerAccount":
                                sKeyData.sMasterAcnt = reader.ReadString();
                                break;
                            case "InspectionCategory":
                                sKeyData.sRequestType = reader.ReadString();
                                break;
                            case "RequestedBy":
                                sKeyData.sRequestedBy = reader.ReadString();
                                break;
                            case "EmailConfirmation":
                                sKeyData.sEmailConfirmation = reader.ReadString();
                                break;
                            case "AgencyAgentName":
                                sKeyData.sAgencyAgentName = reader.ReadString();
                                break;
                            case "AgencyAgentCode":
                                sKeyData.sAgentCode = reader.ReadString();
                                break;
                            case "AgencyAgentPhone":
                                sKeyData.sAgencyAgentPhone = reader.ReadString();
                                break;
                            case "AgencyAgentFax":
                                sKeyData.sAgentFax = reader.ReadString();
                                break;
                            case "AgencyAgentContactName":
                                sKeyData.sAgencyAgentContactName = reader.ReadString();
                                break;
                            case "AgencyAgentAddress1":
                                sKeyData.sAgentAddress1 = reader.ReadString();
                                break;
                            case "AgencyAgentAddress2":
                                sKeyData.sAgentAddress2 = reader.ReadString();
                                break;
                            case "AgencyAgentCity":
                                sKeyData.sAgentCity = reader.ReadString();
                                break;
                            case "AgencyAgentState":
                                sKeyData.sAgentState = reader.ReadString();
                                break;
                            case "AgencyAgentZip":
                                sKeyData.sAgentZip = stripLastFour(reader.ReadString());
                                break;
                            case "PolicyNumber":
                                sKeyData.sPolicyNumber = reader.ReadString();
                                break;
                            case "InsuranceCompany":
                                sKeyData.sInsuranceCo = reader.ReadString();
                                break;
                            case "Producer":
                                sKeyData.sProducer = reader.ReadString();
                                break;
                            case "RushHandling":
                                sKeyData.sRushHandling = reader.ReadString();
                                break;
                            case "InsuredName":
                                sKeyData.sInsuredName = reader.ReadString();
                                break;
                            case "MailAddress1":
                                sKeyData.sMailAddress1 = reader.ReadString();
                                break;
                            case "MailAddress2":
                                sKeyData.sMailAddress2 = reader.ReadString();
                                break;
                            case "MailCity":
                                sKeyData.sMailCity = reader.ReadString();
                                break;
                            case "MailState":
                                sKeyData.sMailState = reader.ReadString();
                                break;
                            case "MailZip":
                                sKeyData.sMailZip = stripLastFour(reader.ReadString());
                                break;
                            case "ContactName":
                                sKeyData.sContactName = reader.ReadString();
                                break;
                            case "ContactPhoneHome":
                                sKeyData.sContactPhoneHome = reader.ReadString();
                                break;
                            case "ContactPhoneWork":
                                sKeyData.sContactPhoneWork = reader.ReadString();
                                break;
                            case "ContactPhoneCell":
                                sKeyData.sContactPhoneCell = reader.ReadString();
                                break;
                            case "BusinessOperations":
                                sKeyData.sBusinessOperations = reader.ReadString();
                                break;
                            case "LocationAddress1":
                                sKeyData.sLocationAddress1 = reader.ReadString();
                                break;
                            case "LocationAddress2":
                                sKeyData.sLocationAddress2 = reader.ReadString();
                                break;
                            case "LocationCity":
                                sKeyData.sLocationCity = reader.ReadString();
                                break;
                            case "LocationState":
                                sKeyData.sLocationState = reader.ReadString();
                                break;
                            case "LocationZip":
                                sKeyData.sLocationZip = stripLastFour(reader.ReadString());
                                break;
                            case "LocationContactName":
                                sKeyData.sLocationContactName = reader.ReadString();
                                break;
                            case "LocationContactPhone":
                                sKeyData.sLocationContactPhone = reader.ReadString();
                                break;
                            case "Comments":
                                sKeyData.sComments = reader.ReadString();
                                break;
                            case "PhotoCode":
                                sKeyData.sPhotoCode = reader.ReadString();
                                break;
                            case "PhotoOther":
                                sKeyData.sPhotoOther = reader.ReadString();
                                break;
                            case "InspectionType":
                                sKeyData.sInspectionType = reader.ReadString();
                                break;
                            case "EffectiveDate":
                                sKeyData.sEffectiveDate = reader.ReadString();
                                break;
                            case "Underwriter":
                                sKeyData.sUnderwriter = reader.ReadString();
                                break;
                            case "UnderwriterFirstName":
                                sKeyData.sUnderwriterFirst = reader.ReadString();
                                break;
                            case "UnderwriterLastName":
                                sKeyData.sUnderwriterLast = reader.ReadString();
                                break;
                            case "UnderwriterPhone":
                                sKeyData.sUnderwriterPhone = reader.ReadString();
                                break;
                            case "UnderwriterContactEmail":
                                sKeyData.sUnderwriterCorrEmail = reader.ReadString();
                                break;
                            case "UnderwriterReportEmail":
                                sKeyData.sUnderwriterRptEmail = reader.ReadString();
                                break;
                            case "BuildingCost":
                                sKeyData.sBuildingCost = reader.ReadString();
                                break;
                            case "BuildingCosts":
                                sKeyData.sBuildingCost = reader.ReadString();
                                break;
                            case "BusinessTotalRevenue":
                                sKeyData.sBusinessTotalRevenue = reader.ReadString();
                                break;
                            case "ContentsCost":
                                sKeyData.sContentsCost = reader.ReadString();
                                break;
                            case "CoverageA":
                                sKeyData.sCoverageA = reader.ReadString();
                                break;
                            case "ISOClass":
                                sKeyData.sISOClass = reader.ReadString();
                                break;
                            case "Occupancy":
                                sKeyData.sOccupancy = reader.ReadString();
                                break;
                            case "YearBuilt":
                                sKeyData.sYearBuilt = reader.ReadString();
                                break;
                            case "ImageRightFileNum":
                                sKeyData.sImageRightFileNum = reader.ReadString();
                                break;
                            case "ImageRightDrawer":
                                sKeyData.sImageRightDrawer = reader.ReadString();
                                break;
                            case "ImageRightUserID":
                                sKeyData.sImageRightUserID = reader.ReadString();
                                break;
                            case "GenericField1Name":
                                sKeyData.sGenericFieldName = reader.ReadString();
                                break;
                            case "GenericField1Value":
                                sKeyData.sGenericFieldValue = reader.ReadString();
                                break;
                            case "GenericFieldName2":
                                sKeyData.sGenericFieldName2 = reader.ReadString();
                                break;
                            case "GenericFieldValue2":
                                sKeyData.sGenericFieldValue2 = reader.ReadString();
                                break;
                            case "GenericFieldName3":
                                sKeyData.sGenericFieldName3 = reader.ReadString();
                                break;
                            case "GenericFieldValue3":
                                sKeyData.sGenericFieldValue3 = reader.ReadString();
                                break;
                        }   // switch

                    }   //if (reader.MoveToContent()

                }   //while

                // Copy location to mailing for JHA
                // Defauly request type to Commercial
                if (sKeyData.sMasterAcnt == "7002")
                {
                    sKeyData.sMailAddress1 = sKeyData.sLocationAddress1;
                    sKeyData.sMailAddress2 = sKeyData.sLocationAddress2;
                    sKeyData.sMailCity = sKeyData.sLocationCity;
                    sKeyData.sMailState = sKeyData.sLocationState;
                    sKeyData.sMailZip = sKeyData.sLocationZip;
                    sKeyData.sRequestType = "Commercial";
                }

                // Defauly request type to Commercial for TAPCO & HULL SIB
                if (sKeyData.sMasterAcnt == "7150" || sKeyData.sMasterAcnt == "7180" || sKeyData.sMasterAcnt == "7070" || sKeyData.sMasterAcnt == "7290" || sKeyData.sMasterAcnt == "7275"  || sKeyData.sMasterAcnt == "7025" || sKeyData.sMasterAcnt == "7164")
                {
                    sKeyData.sRequestType = "Commercial";
                }
                

                // Validate required fields
                string sValidation = validateXML();

                // Check inspection type
                string sITRet = parseInspectionType(sKeyData.sMasterAcnt, sKeyData.sInspectionType);
                sKeyData.sInspectionType = sITRet;

                // Return exception if necessary
                if (sValidation.Length > 0 || sITRet.Length == 0)
                {
                    if (sValidation.Length > 0)
                    {
                        msReturnStr = buildResponse(msUserID, "Exception", sValidation);
                        sErrText = msReturnStr;
                    }
                    if (sITRet.Length == 0)
                    {
                        msReturnStr = buildResponse(msUserID, "Exception", "Invalid inspection type: " + sKeyData.sInspectionType);
                        sErrText = msReturnStr;
                    }
                }
                else    // create and return key
                {
                    //**************************************************************


                    if (sKeyData.sMasterAcnt == "7002" || sKeyData.sMasterAcnt == "7275")
                    {
                        msReturnStr = CreateKeySIB();
                    }
                    else
                    {
                        msReturnStr = createKey360();
                    }

                }
            }   //try

            catch (Exception ex)
            {
                if (sErrText.Length > 0)
                {
                    throw new ApplicationException("Error processing request: " + sErrText);
                }
                else
                {
                    throw new ApplicationException("Error processing request: " + ex.Message);
                }
            }

            finally
            {
                if (sErrText.Length > 0)
                {
                    sendErrEmail(sErrText);
                }


                if (reader != null)
                    reader.Close();
            }

            return msReturnStr;
        }

        /// <summary>
        /// createKey360
        /// </summary>
        /// <returns></returns>
        public string createKey360()
        {

            string sXMLFile = msXMLFilePath + msXMLFile;
            msReturnStr = "";
            string sErrText = "";

            XmlTextReader reader = null;

            try
            {
                
                string sKey = "";

                ImportRequests oAPI = new ImportRequests();
                oAPI.CustomerUserName = "APIProd";
                oAPI.CustomerPassword = "Sutton2012";
                oAPI.CustomerAccount = sKeyData.sMasterAcnt;

                // Underwriter
                // If underwriter first and last name are blank and Underwriter has value, try to split it on space
                if (sKeyData.sUnderwriterFirst.Length == 0 && sKeyData.sUnderwriterLast.Length == 0)
                {

                    if (sKeyData.sUnderwriter.Length > 0)
                    {
                        if (sKeyData.sUnderwriter.Contains(" "))
                        {
                            int iSpace = sKeyData.sUnderwriter.IndexOf(" ");
                            sKeyData.sUnderwriterFirst = sKeyData.sUnderwriter.Substring(0, iSpace);
                            sKeyData.sUnderwriterLast = sKeyData.sUnderwriter.Substring(iSpace, sKeyData.sUnderwriter.Length - iSpace);
                        }
                    }
                }
                
                // Load values into API
                oAPI.EmailConfirmation = "";
                oAPI.RequestedBy = sKeyData.sRequestedBy;
                oAPI.PolicyNumber = sKeyData.sPolicyNumber;
                oAPI.EffectiveDate = sKeyData.sEffectiveDate;
                oAPI.InspectionType = sKeyData.sInspectionType;

                oAPI.Underwriter = sKeyData.sUnderwriter;
                oAPI.UnderwriterFirstName = sKeyData.sUnderwriterFirst;
                oAPI.UnderwriterLastName = sKeyData.sUnderwriterLast;
                oAPI.UnderwriterCorrEmail = sKeyData.sUnderwriterCorrEmail;
                oAPI.UnderwriterPhone = sKeyData.sUnderwriterPhone;
                oAPI.UnderwriterRptEmail = sKeyData.sUnderwriterRptEmail;

                oAPI.AgencyAgentName = sKeyData.sAgencyAgentName;
                oAPI.AgentCode = sKeyData.sAgentCode;
                oAPI.AgencyAgentPhone = sKeyData.sAgencyAgentPhone;
                oAPI.AgentFax = sKeyData.sAgentFax;
                oAPI.AgencyAgentContact = sKeyData.sAgencyAgentContactName.Length > 0 ? sKeyData.sAgencyAgentContactName : sKeyData.sProducer;
                oAPI.AgentAddress1 = sKeyData.sAgentAddress1;
                oAPI.AgentAddress2 = sKeyData.sAgentAddress2;
                oAPI.AgentCity = sKeyData.sAgentCity;
                oAPI.AgentState = sKeyData.sAgentState;
                oAPI.AgentZip = sKeyData.sAgentZip;
                oAPI.AgentCode = sKeyData.sAgentCode;                             

                oAPI.InsuranceCompany = sKeyData.sInsuranceCo;
                oAPI.Producer = sKeyData.sProducer;
                oAPI.RushHandling = sKeyData.sRushHandling;
                oAPI.InsuredName = sKeyData.sInsuredName;
                oAPI.ContactName = sKeyData.sContactName;
                oAPI.ContactPhoneHome = sKeyData.sContactPhoneHome;
                oAPI.ContactPhoneWork = sKeyData.sContactPhoneWork;
                oAPI.ContactPhoneCell = sKeyData.sContactPhoneCell;
                oAPI.MailAddress1 = sKeyData.sMailAddress1;
                oAPI.MailAddress2 = sKeyData.sMailAddress2;
                oAPI.MailCity = sKeyData.sMailCity;
                oAPI.MailState = sKeyData.sMailState;
                oAPI.MailZip = sKeyData.sMailZip;
                oAPI.LocationAddress1 = sKeyData.sLocationAddress1;
                oAPI.LocationAddress2 = sKeyData.sLocationAddress2;
                oAPI.LocationCity = sKeyData.sLocationCity;
                oAPI.LocationState = sKeyData.sLocationState;
                oAPI.LocationZip = sKeyData.sLocationZip;
                oAPI.LocationContactName = sKeyData.sLocationContactName;
                oAPI.LocationContactPhone = sKeyData.sLocationContactPhone;
                oAPI.CoverageA = sKeyData.sCoverageA;
                oAPI.GenericField1Name = sKeyData.sGenericFieldName;
                oAPI.GenericField1Value = sKeyData.sGenericFieldValue;
                oAPI.BuildingCost = sKeyData.sBuildingCost;
                oAPI.Occupancy = sKeyData.sOccupancy;
                oAPI.YearBuilt = sKeyData.sYearBuilt;
                oAPI.ImageRightFileNum = sKeyData.sImageRightFileNum;

                //**********************************
                // UNCOMMENT FOR TEST 
                //oAPI.CustomerUserName = "APITest";
                //oAPI.CustomerAccount = "9998";
                //oAPI.InspectionType = "9998RE";

                //oAPI.AgentAddress1 = "Agent address 1";
                //oAPI.AgentAddress2 = "Agent address 2";
                //oAPI.AgentCity = "Agent city";
                //oAPI.AgentState = "FL";
                //oAPI.AgentZip = "33601";
                //oAPI.AgentCode = "AGNT1";
                //oAPI.AgentFax = "222-222-2222";

                //oAPI.UnderwriterCorrEmail = "uwcorremail@sibfla.com";
                //oAPI.UnderwriterRptEmail = "uwreportemailsibfla.com";
                //oAPI.UnderwriterPhone = "999-999-9999";

                //**********************************


                // build comments
                sKeyData.sComments += System.Environment.NewLine;

                if (sKeyData.sBuildingCost.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Building cost: " + sKeyData.sBuildingCost + System.Environment.NewLine;
                }
                if (sKeyData.sBusinessTotalRevenue.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Business total revenue: " + sKeyData.sBusinessTotalRevenue + System.Environment.NewLine;
                }
                if (sKeyData.sContentsCost.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Contents cost: " + sKeyData.sContentsCost + System.Environment.NewLine;
                }
                if (sKeyData.sCoverageA.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Coverage A: " + sKeyData.sCoverageA + System.Environment.NewLine;
                }
                if (sKeyData.sISOClass.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "ISO Class: " + sKeyData.sISOClass + System.Environment.NewLine;
                }
                if (sKeyData.sOccupancy.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Occupancy: " + sKeyData.sOccupancy + System.Environment.NewLine;
                }
                if (sKeyData.sYearBuilt.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Year built: " + sKeyData.sYearBuilt + System.Environment.NewLine;
                }
                if (sKeyData.sEffectiveDate.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Effective date: " + sKeyData.sEffectiveDate + System.Environment.NewLine;
                }

                oAPI.Comments = sKeyData.sComments;


                // Call import method
                string sRet = "";

                if (oAPI.CustomerAccount == "7307" || oAPI.CustomerAccount == "7271")
                    sRet = oAPI.ImportJHA();
                else
                    sRet = oAPI.Import();

                // Process return
                var importResults = sRet.FromJSON<List<ImportResult>>();

                foreach (var importResult in importResults)
                {

                    if (importResult.Successful)
                    {

                        sKey = importResult.CaseNumber.ToString();
                    }
                    else
                    {
                        if (importResult.Errors != null)
                            foreach (var error in importResult.Errors)
                            {
                                sErrText += error.ErrorText + System.Environment.NewLine;
                            }
                    }
                }

                    
                // key created
                if (sKey.Length > 0)
                {
                    msReturnStr = buildResponse(msUserID, "Key", sKey);

                    // send email conf if necessary
                    if (sKeyData.sEmailConfirmation.Length > 0)
                    {
                        sendEmailConf(sKey, sKeyData);
                    }

                }
                else
                {
                    msReturnStr = buildResponse(msUserID, "Exception", sErrText);
                    sErrText += "sKey.Length = 0" + System.Environment.NewLine;
                    }

            }   //try

            catch (Exception ex)
            {
                if (sErrText.Length > 0)
                {
                    throw new ApplicationException("Error processing request: " + sErrText);
                }
                else
                {
                    throw new ApplicationException("INTERNAL [createKey]: " + ex.Message);
                }
            }

            finally
            {
                //if (sErrText.Length > 0)
                //{
                //    sendErrEmail(sErrText);
                //}


                if (reader != null)
                    reader.Close();
            }

            return msReturnStr;
        }

        private string parseInspectionType(string sCustNum, string sSourceType)
        {

            string sRetVal = "";
            sSourceType = sSourceType.ToUpper();

            if (sCustNum == "7277")
            {

                switch (sSourceType)
                {
                    case "Basic Inspection":
                        sRetVal = "7277-R-E-I"; // Residential Exterior Orchid Import
                        break;
                    case "Basic w/RCE":
                        sRetVal = "7277-R-E-E2V-I"; // Residential Exterior Orchid w/e2value Import 
                        break;
                    case "Basic w/4 PT & Int Photo":
                        sRetVal = "7277-R-IE-U-I";  // Residential Interior & Exterior Orchid w/update Import
                        break;
                    case "Basic w/RCE & Int Photos": 
                        sRetVal = "7277-R-IE-E2V-I";  // Residential Interior & Exterior Orchid w/e2value Import 
                        break;
                    case "All Inclusive Inspection":
                        sRetVal = "7277-R-IE-E2V-U-I"; //Residential Interior & Exterior Orchid  w/update & e2value Import
                        break;
                    case "Builder's Risk":
                        sRetVal = "7277-BR-I";  // Builders Risk Orchid Import
                        break;
                    case "Excess Flood":
                        sRetVal = "7277-Flood-I"; // Residential Flood Orchid Import
                        break;
                    default:
                        sRetVal = "";
                        break;
                }

            }
            // Hull Personal St Pete
            else if (sCustNum == "7033")
            {

                switch (sSourceType)
                {
                    
                    case "R-BR":
                        sRetVal = "7033-R-BR";
                        break;
                    case "R-E":
                        sRetVal = "7033-R-E";
                        break;
                    case "R-E-U":
                        sRetVal = "7033-R-E-U";
                        break;
                    case "TX-R-E-U":
                        sRetVal = "7033-TX-R-E-U";
                        break;
                    case "R-E-U-A":
                        sRetVal = "7033-R-E-U-A";
                        break;
                    case "R-E-E2V":
                        sRetVal = "7033-R-E-E2V";
                        break;
                    case "R-E-E2V-U":
                        sRetVal = "7033-R-E-E2V-U";
                        break;
                    case "TX-R-E-E2V-U":
                        sRetVal = "7033-TX-R-E-E2V-U";
                        break;
                    case "R-LIAB":
                        sRetVal = "7033-R-LIAB";
                        break;
                    case "R-FLOOD":
                        sRetVal = "7033-R-FLOOD";
                        break;
                    case "R-IE-E2V":
                        sRetVal = "7033-R-IE-E2V";
                        break;
                    case "R-IE-E2V-U":
                        sRetVal = "7033-R-IE-E2V-U";
                        break;
                    case "R-IE-E2V-U-A":
                        sRetVal = "7033-R-IE-E2V-U-A";
                        break;
                    case "R-VL":
                        sRetVal = "7033-R-VL";
                        break;
                    case "R-PO":
                        sRetVal = "7033-R-PO";
                        break;
                    default:
                        sRetVal = "";
                        break;
                }
            }
            // NRS
            else if (sCustNum == "7295")
            {

                switch (sSourceType)
                {

                    case "R-BR":
                        sRetVal = "7295-R-BR";
                        break;
                    case "R-E":
                        sRetVal = "7295-R-E";
                        break;
                    case "R-E-U":
                        sRetVal = "7295-TX-R-E-U";
                        break;
                    case "TX-R-E-U":
                        sRetVal = "7295-R-E-U";
                        break;
                    case "R-E-U-A":
                        sRetVal = "7295-R-E-U-A";
                        break;
                    case "R-E-E2V":
                        sRetVal = "7295-R-E-E2V";
                        break;
                    case "TX-R-E-E2V-U":
                        sRetVal = "7295-TX-R-E-E2V-U";
                        break;
                    case "R-E-E2V-U":
                        sRetVal = "7295-R-E-E2V-U";
                        break;
                    case "R-LIAB":
                        sRetVal = "7295-R-LIAB";
                        break;
                    case "R-FLOOD":
                        sRetVal = "7295-R-FLOOD";
                        break;
                    case "R-IE-E2V":
                        sRetVal = "7295-R-IE-E2V";
                        break;
                    case "R-IE-E2V-U":
                        sRetVal = "7295-R-IE-E2V-U";
                        break;
                    case "R-IE-E2V-U-A":
                        sRetVal = "7295-R-IE-E2V-U-A";
                        break;
                    case "R-VL":
                        sRetVal = "7295-R-VL";
                        break;
                    case "R-PO":
                        sRetVal = "7295-R-PO";
                        break;
                    default:
                        sRetVal = "";
                        break;
                }
            }
            // Hull Personal FLL
            else if (sCustNum == "7064")
            {

                switch (sSourceType)
                {
                    case "R-BR":
                        sRetVal = "7064-R-BR";
                        break;
                    case "R-E":
                        sRetVal = "7064-R-E";
                        break;
                    case "R-E-U":
                        sRetVal = "7064-R-E-U";
                        break;
                    case "R-E-E2V":
                        sRetVal = "7064-R-E-E2V";
                        break;
                    case "R-E-E2V-U":
                        sRetVal = "7064-R-E-E2V-U";
                        break;
                    case "R-LIAB":
                        sRetVal = "7064-R-LIAB";
                        break;
                    case "R-FLOOD":
                        sRetVal = "7064-R-FLOOD";
                        break;
                    case "R-IE-E2V":
                        sRetVal = "7064-R-IE-E2V";
                        break;
                    case "R-IE-E2V-U":
                        sRetVal = "7064-R-IE-E2V-U";
                        break;
                    case "R-VL":
                        sRetVal = "7064-R-VL";
                        break;
                    case "R-PO":
                        sRetVal = "7064-R-PO";
                        break;
                    default:
                        sRetVal = "";
                        break;
                }

            }

            // Coastal
            else if (sCustNum == "7195")
            {
                switch (sSourceType.ToUpper())
                {
                    case "CONDO":
                        sRetVal = "7195-CONDO";
                        break;

                    case "PACK":
                        sRetVal = "7195-CONDO";
                        break;

                    case "LIAB":
                        sRetVal = "7195-LIAB";
                        break;

                    case "PROP":
                        sRetVal = "7195-PROP";
                        break;

                    default:
                        sRetVal = "";
                        break;
                }
            }

            // Coastal / Cypress
            else if (sCustNum == "7324")
            {
                switch (sSourceType.ToUpper())
                {
                    case "PROP":
                        sRetVal = "7324-PROP";
                        break;

                    default:
                        sRetVal = "";
                        break;
                }
            }

            // Hull Personal CLT
            else if (sCustNum == "7289")
            {

                switch (sSourceType)
                {
                    case "R-BR":
                        sRetVal = "7289-R-BR";
                        break;
                    case "R-E":
                        sRetVal = "7289-R-E";
                        break;
                    case "R-E-U":
                        sRetVal = "7289-R-E-U";
                        break;
                    case "R-E-E2V":
                        sRetVal = "7289-R-E-E2V";
                        break;
                    case "R-E-E2V-U":
                        sRetVal = "7289-R-E-E2V-U";
                        break;
                    case "R-LIAB":
                        sRetVal = "7289-R-LIAB";
                        break;
                    case "R-FLOOD":
                        sRetVal = "7289-R-FLOOD";
                        break;
                    case "R-IE-E2V":
                        sRetVal = "7289-R-IE-E2V";
                        break;
                    case "R-IE-E2V-U":
                        sRetVal = "7289-R-IE-E2V-U";
                        break;
                    case "R-VL":
                        sRetVal = "7289-R-VL";
                        break;
                    case "R-PO":
                        sRetVal = "7289-R-PO";
                        break;
                    default:
                        sRetVal = "";
                        break;
                }
            }

            //John Handle Residential / Flood - 360
            else if (sCustNum == "7271" || sCustNum == "7307")
            {
                sRetVal = sCustNum + "-" + sSourceType;            
            }

            //Seacoast 
            else if (sCustNum == "7011" || sCustNum == "7029")
            {
                sRetVal = sCustNum + "-" + sSourceType;
            }
                
            //John Handle Commercial - SIB
            else if (sCustNum == "7002")
            {
                sRetVal = sSourceType;
            }

            //TAPCO - SIB
            else if (sCustNum == "7275")
            {
                sRetVal = sSourceType;
            }

            //Hull Commercial - 360
            else if (sKeyData.sMasterAcnt == "7070" || sKeyData.sMasterAcnt == "7180" || sKeyData.sMasterAcnt == "7025" ||sKeyData.sMasterAcnt == "7290" || sKeyData.sMasterAcnt == "7164" || sKeyData.sMasterAcnt == "7150")
            {

                switch (sSourceType)
                {
                    case "1056":
                    case "CJ":
                        sRetVal = sKeyData.sMasterAcnt + "-CJ";
                        break;
                    case "C":
                        sRetVal = sKeyData.sMasterAcnt + "-C";
                        break;
                    case "1009":
                    case "CM":
                        sRetVal = sKeyData.sMasterAcnt + "-CM";
                        break;
                    case "1099":
                    case "DB":
                        sRetVal = sKeyData.sMasterAcnt + "-DB";
                        break;
                    case "1012LG":
                    case "GLS":
                        sRetVal = sKeyData.sMasterAcnt + "-GLS";
                        break;
                    case "1008":
                    case "GL":
                        sRetVal = sKeyData.sMasterAcnt + "GL";
                        break;
                    case "1012KG":
                    case "GPS":
                        sRetVal = sKeyData.sMasterAcnt + "-GPS";
                        break;
                    case "1008P":
                    case "GP":
                        sRetVal = sKeyData.sMasterAcnt + "-GP";
                        break;
                    case "1097":
                    case "IMF":
                        sRetVal = sKeyData.sMasterAcnt + "-IMF";
                        break;
                    case "1012L":
                    case "LIABS":
                        sRetVal = sKeyData.sMasterAcnt + "-LIABS";
                        break;
                    case "1024":
                    case "LIAB":
                        sRetVal = sKeyData.sMasterAcnt + "-LIAB";
                        break;
                    case "1028":
                    case "LL":
                        sRetVal = sKeyData.sMasterAcnt + "-LL";
                        break;
                    case "1091":
                    case "MHPP":
                        sRetVal = sKeyData.sMasterAcnt + "-MHPP";
                        break;
                    case "MHPL":
                        sRetVal = sKeyData.sMasterAcnt + "-MHPL";
                        break;
                    case "1012K":
                    case "PACKS":
                        sRetVal = sKeyData.sMasterAcnt + "-PACKS";
                        break;
                    case "1057":
                    case "PACK":
                        sRetVal = sKeyData.sMasterAcnt + "-PACK";
                        break;
                    case "1012P":
                    case "PROPS":
                        sRetVal = sKeyData.sMasterAcnt + "-PROPS";
                        break;
                    case "1061":
                    case "PROP":
                        sRetVal = sKeyData.sMasterAcnt + "-PROP";
                        break;
                    case "1004":
                    case "R":
                        sRetVal = sKeyData.sMasterAcnt + "-R";
                        break;
                    case "1014":
                    case "REC":
                        sRetVal = sKeyData.sMasterAcnt + "-REC";
                        break;
                    case "1056R":
                    case "RT":
                        sRetVal = sKeyData.sMasterAcnt + "-RT";
                        break;
                    case "1056C":
                    case "TC":
                        sRetVal = sKeyData.sMasterAcnt + "-TC";
                        break;
                    case "1056T":
                    case "TL":
                        sRetVal = sKeyData.sMasterAcnt + "-TL";
                        break;
                    case "1015":
                    case "VP":
                        sRetVal = sKeyData.sMasterAcnt + "-VP";
                        break;
                    case "C-BR":
                        sRetVal = sKeyData.sMasterAcnt + "-C-BR";
                        break;
                    case "MTC":
                        sRetVal = sKeyData.sMasterAcnt + "-MTC";
                        break;
                    case "TA":
                        sRetVal = sKeyData.sMasterAcnt + "-TA";
                        break;
                    case "CRL":
                        sRetVal = sKeyData.sMasterAcnt + "-CRL";
                        break;
                    case "VL":
                        sRetVal = sKeyData.sMasterAcnt + "-VL";
                        break;
                    case "TGL":
                        sRetVal = sKeyData.sMasterAcnt + "-TGL";
                        break;
                    case "TMTC":
                        sRetVal = sKeyData.sMasterAcnt + "-TMTC";
                        break;
                    case "TVL":
                        sRetVal = sKeyData.sMasterAcnt + "-TVL";
                        break;
                    default:
                        sRetVal = "";
                        break;
                }
            }         

            return sRetVal;
        
        }
         
        /// <summary>
        /// validateXML()
        /// </summary>
        /// <returns></returns>
        private string validateXML()
        {

            string sRet = "";

            if (sKeyData.sMasterAcnt.Length == 0)
            {
                return "MasterAcnt required";
            }

            if (sKeyData.sInspectionType.Length == 0)
            {
                return "InspectionType required";
            }

            if (sKeyData.sPolicyNumber.Length == 0)
            {
                return "Policy number required";
            }

            if (sKeyData.sInsuredName.Length == 0)
            {
                return "Insured name required";
            }

            if (sKeyData.sMailAddress1.Length == 0)
            {
                return "Complete US mailing address required";
            }

            if (sKeyData.sMailCity.Length == 0)
            {
                return "Complete US mailing address required";
            }

            if (sKeyData.sMailState.Length == 0)
            {
                return "Complete US mailing address required";
            }

            if (sKeyData.sMailZip.Length == 0)
            {
                return "Complete US mailing address required";
            }

            if (sKeyData.sLocationAddress1.Length == 0)
            {
                return "Location address required";
            }

            if (sKeyData.sLocationCity.Length == 0)
            {
                return "Location address required";
            }

            if (sKeyData.sLocationState.Length == 0)
            {
                return "Location address required";
            }

            if (sKeyData.sLocationZip.Length == 0)
            {
                return "Location address required";
            }

            if (sKeyData.sRushHandling.Length > 0)
            {
                sKeyData.sRushHandling = sKeyData.sRushHandling.ToUpper();
                if (sKeyData.sRushHandling != "Y" && sKeyData.sRushHandling != "N")
                    return "RushHandling must be Y/N";
            }

            if (sKeyData.sCoverageA.Length > 0)
            {
                sKeyData.sCoverageA = sKeyData.sCoverageA.Replace("$", "");
            }

            return sRet;

        }

        /// <summary>
        /// buildXML()
        /// </summary>
        /// <returns></returns>
        private string buildResponse(string sUserID, string sElement, string sData)
        {
            string sRet = "";
            string sRSPFile = msXMLFilePath + "RSP" + msXMLFile;

            XmlTextWriter xmlWriter = null;
            xmlWriter = new XmlTextWriter(sRSPFile, null);

            try
            {
                DateTime dTimeStamp = DateTime.Now;

                xmlWriter.Formatting = Formatting.None;
                xmlWriter.Namespaces = false;

                xmlWriter.WriteStartDocument();

                xmlWriter.WriteStartElement("", "UploadRequestResponse", "");

                xmlWriter.WriteStartElement("", "UserID", "");
                xmlWriter.WriteString(sUserID);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("", "TimeStamp", "");
                xmlWriter.WriteString(dTimeStamp.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("", sElement, "");
                xmlWriter.WriteString(sData);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();

                xmlWriter.Flush();
                xmlWriter.Close();

                if (File.Exists(sRSPFile))
                {

                    using (StreamReader sr = new StreamReader(sRSPFile))
                    {
                        sRet = sr.ReadToEnd();
                        sr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("INTERNAL [buildResponse]: " + ex.Message);
            }
            finally
            { 
                if (xmlWriter != null)
                {
                    xmlWriter.Close();
                }
            }

            return sRet;
        }

        /// <summary>
        /// getXML
        /// </summary>
        /// <param name="xmlFileName"></param>
        /// <returns></returns>
        private string getXML(string xmlFileName)
        {
            string sRetVal = "";

            try
            {
                if (!File.Exists(xmlFileName))
                {
                    throw new ApplicationException("Exception in getXML: XML file missing: " + xmlFileName);

                }

                using (StreamReader sr = new StreamReader(xmlFileName))
                {
                    sRetVal = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("INTERNAL [getXML]: " + ex.Message);
            }

            return sRetVal;

        }

        private string stripLastFour(string sSource)
        {

            string sRetVal = sSource.Trim();

            if (sSource.Length > 5)
            {
                sRetVal = sSource.Substring(0, 5);
            }

            return sRetVal;
        
        }
 

        private void sendEmailConf(string sCaseNum, psKeyData sKeyData)
        {

            try
            {

                EmailConfirmation360.EmailConf360 oEM = new EmailConfirmation360.EmailConf360();

                oEM.cfg_360ConnStr = msSQL360ConnectStr;
                oEM.cfg_smtpserver = msSMTPerver;
                oEM.CaseNum = sCaseNum;
                oEM.CustNum = sKeyData.sMasterAcnt;
                oEM.InsuredName = sKeyData.sInsuredName;
                oEM.RequestedBy = sKeyData.sRequestedBy;

                // Force email conf for Hull/RPS
                if (sKeyData.sMasterAcnt == "7033" || sKeyData.sMasterAcnt == "7295")
                {
                    oEM.Recip = "TSanJulian@nrsinsurance.com";
                }
                else
                {
                    oEM.Recip = sKeyData.sEmailConfirmation;
                }
                oEM.DateSubmitted = DateTime.Now.ToString();
                oEM.InsAdd1 = sKeyData.sMailAddress1;
                oEM.InsAdd2 = sKeyData.sMailAddress2;
                oEM.InsCity = sKeyData.sMailCity;
                oEM.InsState = sKeyData.sMailState;
                oEM.InsZip = sKeyData.sMailZip;
                oEM.InsuredContactName = sKeyData.sContactName;
                oEM.InsuredContactPhoneHome = sKeyData.sContactPhoneHome;
                oEM.InsuredContactPhoneWork = sKeyData.sContactPhoneWork;
                oEM.InsuredContactPhoneCell = sKeyData.sContactPhoneCell;
                oEM.PolicyNum = sKeyData.sPolicyNumber;
                oEM.Agent = sKeyData.sAgencyAgentName;
                oEM.AgentPhone = sKeyData.sAgencyAgentPhone;
                oEM.InsuranceCo = sKeyData.sInsuranceCo;
                oEM.Underwriter = sKeyData.sUnderwriter;
                oEM.LocAdd1 = sKeyData.sLocationAddress1;
                oEM.LocAdd2 = sKeyData.sLocationAddress2;
                oEM.LocCity = sKeyData.sLocationCity;
                oEM.LocState = sKeyData.sLocationState;
                oEM.LocZip = sKeyData.sLocationZip;
                oEM.LocContact = sKeyData.sLocationContactName;
                oEM.LocContactPhone = sKeyData.sLocationContactPhone;
                oEM.InspectionType = sKeyData.sInspectionType;
                oEM.Comments = sKeyData.sComments;


                string sRet = oEM.sendEmailConf();
                if (sRet.Length > 0)
                    throw new ApplicationException(sRet);
                
            }
            catch (Exception ex)
            {
                string sErrText = "Error sending confirmation for case#: " + sCaseNum + System.Environment.NewLine + ex.Message;
                sendErrEmail(sErrText);
            }


        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            string smtpserver = colNameVal.Get("smtpserver");

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by wsRequests **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }


        private string CreateKeySIB()
        {

            string sKeyNum = "";
            msReturnStr = "";
            string sErrText = "";

            try
            {
                SqlConnection sqlConn = new SqlConnection(msSQLSIBConnectStr);
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandText = "sp_WebRequest_Insert_Hull";
                sqlCmd.Connection = sqlConn;

                // Establish SQL connection
                sqlConn.Open();

                // Populate stored procedure values
                sqlCmd.Parameters.AddWithValue("@Mode", sKeyData.sRequestType);
                sqlCmd.Parameters.AddWithValue("@MasterAcnt", sKeyData.sMasterAcnt);
                sqlCmd.Parameters.AddWithValue("@UserID", msUserID);
                sqlCmd.Parameters.AddWithValue("@EmailConf", sKeyData.sEmailConfirmation);
                sqlCmd.Parameters.AddWithValue("@Handling", sKeyData.sRushHandling);
                sqlCmd.Parameters.AddWithValue("@RequestedBy", sKeyData.sRequestedBy);
                sqlCmd.Parameters.AddWithValue("@PolicyNum", sKeyData.sPolicyNumber);
                sqlCmd.Parameters.AddWithValue("@Agency", sKeyData.sAgencyAgentName);
                sqlCmd.Parameters.AddWithValue("@AgencyPhone", sKeyData.sAgencyAgentPhone);
                sqlCmd.Parameters.AddWithValue("@InsuranceCo", sKeyData.sInsuranceCo);
                sqlCmd.Parameters.AddWithValue("@Producer", sKeyData.sProducer);
                sqlCmd.Parameters.AddWithValue("@InsuredName", sKeyData.sInsuredName);
                sqlCmd.Parameters.AddWithValue("@InsuredAdd1", sKeyData.sMailAddress1);
                sqlCmd.Parameters.AddWithValue("@InsuredAdd2", sKeyData.sMailAddress2);
                sqlCmd.Parameters.AddWithValue("@InsuredCity", sKeyData.sMailCity);
                sqlCmd.Parameters.AddWithValue("@InsuredState", sKeyData.sMailState);
                sqlCmd.Parameters.AddWithValue("@InsuredZip", sKeyData.sMailZip);
                sqlCmd.Parameters.AddWithValue("@InsuredContact", sKeyData.sContactName);
                sqlCmd.Parameters.AddWithValue("@InsuredPhone", sKeyData.sContactPhone);
                sqlCmd.Parameters.AddWithValue("@BusinessOperations", sKeyData.sBusinessOperations);
                sqlCmd.Parameters.AddWithValue("@Loc1Add1", sKeyData.sLocationAddress1);
                sqlCmd.Parameters.AddWithValue("@Loc1Add2", sKeyData.sLocationAddress2);
                sqlCmd.Parameters.AddWithValue("@Loc1City", sKeyData.sLocationCity);
                sqlCmd.Parameters.AddWithValue("@Loc1State", sKeyData.sLocationState);
                sqlCmd.Parameters.AddWithValue("@Loc1Zip", sKeyData.sLocationZip);
                sqlCmd.Parameters.AddWithValue("@Loc1Contact", sKeyData.sLocationContactName);
                sqlCmd.Parameters.AddWithValue("@Loc1Phone", sKeyData.sLocationContactPhone);
                sqlCmd.Parameters.AddWithValue("@Photo", sKeyData.sPhotoCode);
                sqlCmd.Parameters.AddWithValue("@PhotoOther", sKeyData.sPhotoOther);


                // Underwriter
                // If underwriter first and last name are present, combine them into Underwriter
                if (sKeyData.sUnderwriterFirst.Length > 0 || sKeyData.sUnderwriterLast.Length > 0)
                {
                    sKeyData.sUnderwriter = sKeyData.sUnderwriterFirst + sKeyData.sUnderwriterLast.Length;
                }


                // build comments
                sKeyData.sComments += System.Environment.NewLine;

                if (sKeyData.sContactPhoneWork.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Insured work phone: " + sKeyData.sContactPhoneWork + System.Environment.NewLine;
                }
                if (sKeyData.sContactPhoneCell.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Insured cell phone: " + sKeyData.sContactPhoneCell + System.Environment.NewLine;
                }
                if (sKeyData.sBuildingCost.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Building cost: " + sKeyData.sBuildingCost + System.Environment.NewLine;
                }
                if (sKeyData.sBusinessTotalRevenue.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Business total revenue: " + sKeyData.sBusinessTotalRevenue + System.Environment.NewLine;
                }
                if (sKeyData.sContentsCost.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Contents cost: " + sKeyData.sContentsCost + System.Environment.NewLine;
                }
                if (sKeyData.sCoverageA.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Coverage A: " + sKeyData.sCoverageA + System.Environment.NewLine;
                }
                if (sKeyData.sISOClass.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "ISA Class: " + sKeyData.sISOClass + System.Environment.NewLine;
                }
                if (sKeyData.sOccupancy.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Occupancy: " + sKeyData.sOccupancy + System.Environment.NewLine;
                }
                if (sKeyData.sYearBuilt.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Year built: " + sKeyData.sYearBuilt + System.Environment.NewLine;
                }
                if (sKeyData.sEffectiveDate.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Effective date: " + sKeyData.sEffectiveDate + System.Environment.NewLine;
                }
                if (sKeyData.sUnderwriter.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Underwriter: " + sKeyData.sUnderwriter + System.Environment.NewLine;
                }
                if (sKeyData.sUnderwriterPhone.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Underwriter phone: " + sKeyData.sUnderwriterPhone + System.Environment.NewLine;
                }
                if (sKeyData.sUnderwriterCorrEmail.Length > 0)
                {
                    sKeyData.sComments += System.Environment.NewLine + "Underwriter email: " + sKeyData.sUnderwriterCorrEmail + System.Environment.NewLine;
                }


                //sqlCmd.Parameters.AddWithValue("@Comments", sKeyData.sComments.Trim());

                // Build inspection types
                sKeyData.sLiability = "";
                sKeyData.sProperty = "";
                sKeyData.sPackage = "";
                sKeyData.sAuto = "";
                sKeyData.sMiscellaneous = "";

                string sInspType = sKeyData.sInspectionType.Trim().ToUpper();

                if (sInspType == "NR" || sInspType == "1004") // 1004
                {
                    sKeyData.sLiability += "~CM01";
                }
                if (sInspType == "GP") //1008P
                {
                    sKeyData.sAuto += "~CK04";
                }
                if (sInspType == "G") //1008
                {
                    sKeyData.sAuto += "~CA03";
                }
                if (sInspType == "TA") // 1011
                {
                    sKeyData.sLiability += "~CA02";
                }
                if (sInspType.Contains("1012L"))
                {
                    sKeyData.sLiability += "~CL12";
                }
                if (sInspType.Contains("1012P"))
                {
                    sKeyData.sProperty += "~CP06";
                }
                if (sInspType.Contains("1012K"))
                {
                    sKeyData.sPackage += "~CK01";
                }
                //if (sInspType.Contains("1012LG"))
                //{
                //    sKeyData.sLiability = "~CL30";
                //}
                //if (sInspType.Contains("1012KG"))
                //{
                //    sKeyData.sPackage = "~CK30";
                //}
                if (sInspType == "LIAB" || sInspType == "1024") // 1024
                {
                    sKeyData.sLiability = "~CL07";
                }
                if (sInspType == "LL") //1028
                {
                    sKeyData.sLiability += "~CL08";
                }

                if (sInspType == "REST" || sInspType == "1054") //1054
                {
                    sKeyData.sPackage = "~CK22";
                }
                if (sInspType == "TC" || sInspType == "1056C")    //1056c
                {
                    sKeyData.sLiability = "~CL15";
                }
                else if (sInspType == "TL" || sInspType == "1056T")   //1056T
                {
                    sKeyData.sLiability = "~CL09";
                }
                else if (sInspType == "TR" || sInspType == "1056R")   // 1056R
                {
                    sKeyData.sLiability = "~CL17";
                }
                if (sInspType == "CJ" || sInspType == "1056") //1056
                {
                    sKeyData.sLiability = "~CL11";
                }
                if (sInspType == "PACK" || sInspType == "1057") // 1057
                {
                    sKeyData.sPackage = "~CK02";
                }
                if (sInspType == "PROP" || sInspType == "1061") //1061
                {
                    sKeyData.sProperty = "~CP02";
                }
                //if (sInspType.Contains("1080A"))
                //{
                //    if (sInspType.Contains("1080AB"))
                //    {
                //        sKeyData.sProperty = "~PP01";
                //    }
                //    else
                //    {
                //        // 1080A
                //        sKeyData.sProperty = "~PP02";
                //    }
                //}
                if (sInspType == "LIAB-R")    // 1080L
                {
                    sKeyData.sLiability = "~PL01";
                }
                //if (sInspType.Contains("1081"))   // 1081
                //{
                //    sKeyData.sProperty = "~PP03";
                //}
                if (sInspType == "NR" || sInspType == "1090") //1090
                {
                    sKeyData.sProperty = "~CM15";
                }
                if (sInspType == "MHPP") // 1091
                {
                    sKeyData.sProperty = "~MHPP";
                }

                if (sInspType == "1014") //1014
                {
                    sKeyData.sMiscellaneous = "~CM07";
                }


                // remove leading ~
                if (sKeyData.sAuto.StartsWith("~"))
                {
                    sKeyData.sAuto = sKeyData.sAuto.Substring(1);
                }

                if (sKeyData.sLiability.StartsWith("~"))
                {
                    sKeyData.sLiability = sKeyData.sLiability.Substring(1);
                }

                if (sKeyData.sProperty.StartsWith("~"))
                {
                    sKeyData.sProperty = sKeyData.sProperty.Substring(1);
                }

                if (sKeyData.sPackage.StartsWith("~"))
                {
                    sKeyData.sPackage = sKeyData.sPackage.Substring(1);
                }

                if (sKeyData.sMiscellaneous.StartsWith("~"))
                {
                    sKeyData.sMiscellaneous = sKeyData.sMiscellaneous.Substring(1);
                }

                // unhandled inspection type
                if (sKeyData.sAuto.Length == 0 && sKeyData.sLiability.Length == 0 && sKeyData.sProperty.Length == 0 && sKeyData.sPackage.Length == 0 && sKeyData.sMiscellaneous.Length == 0)
                {
                    sKeyData.sComments = "**** UNHANDLED INSPECTION TYPE: " + sKeyData.sInspectionType + " ****" + System.Environment.NewLine + sKeyData.sComments;
                }

                sqlCmd.Parameters.AddWithValue("@Comments", sKeyData.sComments.Trim());
                sqlCmd.Parameters.AddWithValue("@Auto", sKeyData.sAuto);
                sqlCmd.Parameters.AddWithValue("@Liability", sKeyData.sLiability);
                sqlCmd.Parameters.AddWithValue("@Property", sKeyData.sProperty);
                sqlCmd.Parameters.AddWithValue("@Package", sKeyData.sPackage);
                sqlCmd.Parameters.AddWithValue("@Miscellaneous", "");
    
                sqlCmd.Parameters.Add("@KeyNum", SqlDbType.Int, 4);
                sqlCmd.Parameters["@KeyNum"].Direction = ParameterDirection.Output;

                // Execute Insert
                sqlCmd.ExecuteNonQuery();

                sKeyNum = sqlCmd.Parameters["@KeyNum"].Value.ToString();

                sqlConn.Close();
                sqlConn = null;

                // key created
                if (sKeyNum.Length > 0)
                {
                    msReturnStr = buildResponse(msUserID, "Key", sKeyNum);

                    // send email conf if necessary
                    if (sKeyData.sEmailConfirmation.Length > 0)
                    {
                        sendEmailConf(sKeyNum, sKeyData);
                    }

                }
                else
                {
                    msReturnStr = buildResponse(msUserID, "Exception", "Request creation failed");
                    sErrText += "sKey.Length = 0" + System.Environment.NewLine;
                }

            }
            catch (Exception ex)

            {

                if (sErrText.Length > 0)
                {
                    throw new ApplicationException("Error processing request: " + sErrText);
                }
                else
                {
                    throw new ApplicationException("INTERNAL [sqlInsert]: " + ex.Message);
                }

            }
            finally
            {

            }
            
            return msReturnStr;
        }
                
    }
}
