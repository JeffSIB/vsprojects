﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Xml;

namespace wsRequests
{
    /// <summary>
    /// wsRequests web service
    /// </summary>
    /// 

    [WebService(Namespace = "http://webservices3.sibfla.com/wsRequests/",
            Description = "Sutton Inspection Bureau client web services")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class wsRequests : System.Web.Services.WebService
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_SQLSIBConnStr;
        static string cfg_SQL360ConnStr;
        static string cfg_XMLFilePath;
        static string cfg_XSDFile;
        static string msXMLFileName;

        [WebMethod(Description = "Create a new inspection request")]
        public string UploadRequest(string userid, string pw, string xmlString)
        {

            string sReturnStr = "";
            string sRetVal = "";
            string sXMLStat = "";
            msXMLFileName = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_XMLFilePath = colNameVal.Get("xmlfilepath");
                cfg_XSDFile = colNameVal.Get("xsdfile");
                cfg_SQLSIBConnStr = colNameVal.Get("SQLSIBConnStr");
                cfg_SQL360ConnStr = colNameVal.Get("SQL360ConnStr");

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                // validate parameters
                if (userid.Length == 0)
                {
                    sRetVal = "User id required";
                    throw new ApplicationException(sRetVal);
                }

                if (pw.Length == 0)
                {
                    sRetVal = "Password required";
                    throw new ApplicationException(sRetVal);
                }

                if (xmlString.Length == 0)
                {
                    sRetVal = "XML string cannot be empty";
                    throw new ApplicationException(sRetVal);
                }

                // Validate ID/PW
                if (!validateUser(userid, pw))
                {
                    sRetVal = "Unauthorized: " + userid + " - " + pw;
                    throw new ApplicationException(sRetVal);
                }

                // Record login
                oLU.WritetoLog("Login: " + userid);

                DateTime dWork = DateTime.Now;
                msXMLFileName = userid + dWork.Day.ToString() + dWork.Minute.ToString() + dWork.Second.ToString() + ".xml";


                // Record XML write
                oLU.WritetoLog("Writing XML: [" + cfg_XMLFilePath + msXMLFileName + "] " + xmlString.Length.ToString() + " bytes");

                // write inbound XML to file
                string xmlRet = writeXML(xmlString, cfg_XMLFilePath + msXMLFileName);
                if (xmlRet.Length != 0)
                {

                    sRetVal = "XML Error: " + userid;
                    throw new ApplicationException(sRetVal);

                }

                // Record attempt to create key
                oLU.WritetoLog("Processing XML:");

                // validate XML and create key
                AIMXML.AIMXMLUtils oXMLUtils;
                oXMLUtils = new AIMXML.AIMXMLUtils();
                oXMLUtils.XMLFile = msXMLFileName;
                oXMLUtils.XMLFilePath = cfg_XMLFilePath;
                oXMLUtils.UserID = userid;
                oXMLUtils.SQL360ConnectStr = cfg_SQL360ConnStr;
                oXMLUtils.SQLSIBConnectStr = cfg_SQLSIBConnStr;
                oXMLUtils.SMTPServer = cfg_smtpserver;

                // create key
                sXMLStat = oXMLUtils.createKey();

                // Record return from createKey()
                oLU.WritetoLog("CreateKey returned:");
                oLU.WritetoLog(sXMLStat);

                if (sXMLStat.Length == 0)
                {
                    oLU.WritetoLog("CreateKey returned null");
                    oLU.WritetoLog(sXMLStat);
                    sRetVal = "Unexpected error creating request";
                    throw new ApplicationException(sRetVal);
                }

                oXMLUtils = null;

            }

            catch (Exception ex)
            {

                //catch exception  
                oLU.WritetoLog(ex.Message);
                if (ex.Message.Contains("INTERNAL"))
                {
                    sReturnStr = buildResponse(userid, "Exception", "Internal error processing request. Please contact Sutton Inspection Bureau technical support.");
                }
                else
                {
                    sReturnStr = buildResponse(userid, "Exception", ex.Message);
                }
                oLU.WritetoLog("XXXXX" + sReturnStr);
                sendErrEmail(msXMLFileName + "\r\n" + ex.Message);

            }

            finally
            {
                // close log
                oLU.closeLog();
            }

            // return XML string to caller

            // Unknown error - empty response from importer and no error string
            if (sReturnStr.Length == 0 && sXMLStat.Length == 0)
            {
                sReturnStr = buildResponse(userid, "Exception", "Unknown error processing request");
            }

            // Exception occurred - return exception description
            if (sReturnStr.Length > 0)
            {
                sendErrEmail("WebService Import Failed: " + System.Environment.NewLine + sReturnStr);
                return sReturnStr;
            }
            else
            {
                // Success - return output from importer
                return sXMLStat;
            }

        }


        static bool validateUser(string id, string pw)
        {

            if (id == "SIBHull" && pw == "SuttonWS0511")
            {
                return true;
            }
            else if (id == "SIBOrchid" && pw == "SuttonWS0512")
            {
                return true;
            }
            else if (id == "SIBCoastal" && pw == "SuttonWS0513")
            {
                return true;
            }
            else if (id == "SIBSeacoast" && pw == "SuttonWS0215")
            {
                return true;
            }
            else if (id == "SIBJHA" && pw == "SuttonWS0915")
            {
                return true;
            }
            else if (id == "SIBTAPCO" && pw == "SuttonWS0516")
            {
                return true;
            }



            else
            {
                return false;
            }

        }

        static string writeXML(string xmlString, string xmlFileName)
        {
            string sRetVal = "";


            try
            {
                if (xmlString.Length == 0)
                {
                    oLU.WritetoLog("**** XML string empty: " + xmlFileName);
                    return sRetVal;
                }

                StreamWriter sr = new StreamWriter(xmlFileName);
                sr.Write(xmlString);
                sr.Flush();
                sr.Close();
                sr = null;
            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sRetVal = ex.Message;
                sendErrEmail("WriteXML " + ex.Message);
            }

            return sRetVal;

        }


        /// <summary>
        /// buildXML()
        /// </summary>
        /// <returns></returns>
        private string buildResponse(string sUserID, string sElement, string sData)
        {
            string sRet = "";
            string sRSPFile = cfg_XMLFilePath + "RSP" + msXMLFileName;

            XmlTextWriter xmlWriter = null;
            xmlWriter = new XmlTextWriter(sRSPFile, null);

            try
            {
                DateTime dTimeStamp = DateTime.Now;

                xmlWriter.Formatting = Formatting.None;
                xmlWriter.Namespaces = false;

                xmlWriter.WriteStartDocument();

                xmlWriter.WriteStartElement("", "UploadRequestResponse", "");

                xmlWriter.WriteStartElement("", "UserID", "");
                xmlWriter.WriteString(sUserID);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("", "TimeStamp", "");
                xmlWriter.WriteString(dTimeStamp.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("", sElement, "");
                xmlWriter.WriteString(sData);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();

                xmlWriter.Flush();
                xmlWriter.Close();

                if (File.Exists(sRSPFile))
                {

                    using (StreamReader sr = new StreamReader(sRSPFile))
                    {
                        sRet = sr.ReadToEnd();
                        sr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (xmlWriter != null)
                {
                    xmlWriter.Close();
                }
            }

            return sRet;
        }

        //private void sendEmailConf(String sKey, string sEmail)
        //{

        //    // load configuration values from app.config
        //    System.Collections.Specialized.NameValueCollection colNameVal;
        //    colNameVal = System.Configuration.ConfigurationManager.AppSettings;
        //    cfg_smtpserver = colNameVal.Get("smtpserver");
        //    cfg_SQLConnStr = colNameVal.Get("SQLConnStr");

        //    EmailConfirmation360.EmailConf360 oEmailConf = new EmailConfirmation360.EmailConf360();

        //    oEmailConf.SQLConnString = cfg_SQLConnStr;
        //    oEmailConf.SMTPServer = cfg_smtpserver;

        //    bool bRet = oEmailConf.SendEmailConf("9095718", "jeff@sibfla.com", "H");

        //}

        static void sendErrEmail(string bodytext)
        {

            string sRet;

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            string smtpserver = colNameVal.Get("smtpserver");

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by wsRequests **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }
    }

}