﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Net;
using System.Web.Services;
using System.IO;
using System.Xml;
using LC360API.Carrier_V1;
using System.Runtime.Serialization.Json;
using System.Reflection;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace wsRequests
{
    /// <summary>
    /// wsRequests web service
    /// </summary>
    /// 

    [WebService(Namespace = "http://webservices.sibfla.com/wsRequests/",
            Description = "Sutton Inspection Bureau client web services")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    // publish to \\192.168.10.50\webservices1 (webservices) webservices3 = webservices

    public static class Helpers
    {
        public static T FromJSON<T>(this string json)
        {
            if (!string.IsNullOrEmpty(json))
            {
                using (MemoryStream ms = new MemoryStream(ASCIIEncoding.Default.GetBytes(json)))
                {
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
                    return (T)ser.ReadObject(ms);
                }
            }
            else
            {
                ConstructorInfo cInfo = typeof(T).GetConstructor(new Type[] { });
                return (T)cInfo.Invoke(null);
            }
        }


        /// <summary>
        /// Creates JSON string from an object (Can't do Anonymous types use ToJSON2 to do that)
        /// </summary>
        /// <param name="obj">Object to make into JSON string</param>
        /// <returns>JSON formatted string containing property structure of passed object</returns>
        public static string ToJSON(this object obj)
        {
            string json = string.Empty;
            DataContractJsonSerializer ser = new DataContractJsonSerializer(obj.GetType());
            using (MemoryStream ms = new MemoryStream())
            {
                ser.WriteObject(ms, obj);
                json = Encoding.Default.GetString(ms.ToArray());
            }

            return json;
        }

        public static string MakeJSONHTTPPost(this object objectToPostAsJSON, string url)
        {
            return objectToPostAsJSON.ToJSON().MakeJSONHTTPPost(url);
        }


        public static string MakeJSONHTTPPost(this string jsonToPost, string url)
        {
            Uri address = new Uri(url);

            // Create the web request  
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;

            // Set type to POST  
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Timeout = 1000 * 60 * 60;
            request.UserAgent = "LC360 Sample App";
            request.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
            //request.Accept = "*/*";

            // Create a byte array of the data we want to send  
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(jsonToPost);

            // Set the content length in the request headers  
            request.ContentLength = byteData.Length;

            // Write data  
            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }

            // Get response  
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                // Get the response stream  
                StreamReader reader = new StreamReader(response.GetResponseStream());

                // Request Result Text 
                string result = reader.ReadToEnd();

                return result;
            }
        }
    }



    public class wsRequests : System.Web.Services.WebService
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_SQL360UtilConnStr;
        static string cfg_XMLFilePath;
        static string cfg_XSDFile;
        static string msXMLFileName;
        static string cfg_CaseFilesRoot;


        [WebMethod(Description = "Create a new inspection request")]
        public string UploadRequest(string userid, string pw, string xmlString)
        {

            string sReturnStr = "";
            string sRetVal = "";
            string sXMLStat = "";
            msXMLFileName = "";

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_XMLFilePath = colNameVal.Get("xmlfilepath");
                cfg_XSDFile = colNameVal.Get("xsdfile");
                //cfg_SQL360ConnStr = colNameVal.Get("SQL360ConnStr");
                cfg_SQL360UtilConnStr = colNameVal.Get("SQL360UtilConnStr");

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                // validate parameters
                if (userid.Length == 0)
                {
                    sRetVal = "User id required";
                    throw new ApplicationException(sRetVal);
                }

                if (pw.Length == 0)
                {
                    sRetVal = "Password required";
                    throw new ApplicationException(sRetVal);
                }
                
                if (xmlString.Length == 0)
                {
                    sRetVal = "XML string cannot be empty";
                    throw new ApplicationException(sRetVal);
                }

                // Validate ID/PW
                if (!validateUser(userid, pw))
                {
                    sRetVal = "Unauthorized: " + userid + " - " + pw;
                    throw new ApplicationException(sRetVal);
                }

                // Record login
                oLU.WritetoLog("Login: " + userid);

                DateTime dWork = DateTime.Now;
                msXMLFileName = userid + dWork.Day.ToString() + dWork.Minute.ToString() + dWork.Second.ToString() + ".xml";

                // escape "&"
                xmlString = xmlString.Replace("&", "&amp;");

                // Record XML write
                oLU.WritetoLog("Writing XML: [" + cfg_XMLFilePath + msXMLFileName + "] " + xmlString.Length.ToString() + " bytes");

                // write inbound XML to file
                string xmlRet = writeXML(xmlString, cfg_XMLFilePath + msXMLFileName);
                if (xmlRet.Length != 0)
                {

                    sRetVal = "XML Error: " + userid;
                    throw new ApplicationException(sRetVal);

                }

                // Record attempt to create key
                oLU.WritetoLog("Processing XML:");

                // validate XML and create key
                AIMXML.AIMXMLUtils oXMLUtils;
                oXMLUtils = new AIMXML.AIMXMLUtils();
                oXMLUtils.XMLFile = msXMLFileName;
                oXMLUtils.XMLFilePath = cfg_XMLFilePath;
                oXMLUtils.UserID = userid;
                oXMLUtils.SQL360UtilConnectStr = cfg_SQL360UtilConnStr;
                oXMLUtils.SMTPServer = cfg_smtpserver;

                // create key
                sXMLStat = oXMLUtils.createKey();

                // Record return from createKey()
                oLU.WritetoLog("CreateKey returned:");
                oLU.WritetoLog(sXMLStat);

                if (sXMLStat.Length == 0)
                {
                    oLU.WritetoLog("CreateKey returned null");
                    oLU.WritetoLog(sXMLStat);
                    sRetVal = "Unexpected error creating request";
                    throw new ApplicationException(sRetVal);
                }

                oXMLUtils = null;

                // close log
                oLU.closeLog();

            }

            catch (Exception ex)
            {

                //catch exception  
                oLU.WritetoLog(ex.Message);
                if (ex.Message.Contains("INTERNAL"))
                {
                    sReturnStr = buildResponse("UploadRequestResponse", userid, "Exception", "Internal error processing request. Please contact Sutton Inspection Bureau technical support.");
                }
                else
                {
                    sReturnStr = buildResponse("UploadRequestResponse", userid, "Exception", ex.Message);
                }
                oLU.WritetoLog("XXXXX" + sReturnStr);
                oLU.closeLog();
                sendErrEmail(msXMLFileName + "\r\n" + ex.Message);
            }

            oLU = null;

            // return XML string to caller

            // Unknown error - empty response from importer and no error string
            if (sReturnStr.Length == 0 && sXMLStat.Length == 0)
            {
                sReturnStr = buildResponse("UploadRequestResponse", userid, "Exception", "Unknown error processing request");
            }

            // Exception occurred - return exception description
            if (sReturnStr.Length > 0)
            {
                sendErrEmail("WebService Import Failed: " + System.Environment.NewLine + sReturnStr);
                return sReturnStr;
            }
            else if (sXMLStat.ToUpper().Contains("EXCEPTION"))
            {
                // Exception creating case
                sendErrEmail("WebService Import Failed: " + System.Environment.NewLine + sXMLStat);
                return sXMLStat;
            }
            else 
            {
                // Success - return output from importer
                return sXMLStat;
            }

        }

        //--------------------------------------------------------------------------------------------------------
        [WebMethod(Description = "Reject a report for further processing")]
        public string RejectCase(string userid, string pw, string xmlString)
        {

            string sReturnStr = "";
            string sRetVal = "";
            string sXMLStat = "";
            msXMLFileName = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_XMLFilePath = colNameVal.Get("xmlfilepath");
                cfg_XSDFile = colNameVal.Get("xsdfile");
                cfg_SQL360UtilConnStr = colNameVal.Get("SQL360UtilConnStr");

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                // validate parameters
                if (userid.Length == 0)
                {
                    sRetVal = "User id required";
                    throw new ApplicationException(sRetVal);
                }

                if (pw.Length == 0)
                {
                    sRetVal = "Password required";
                    throw new ApplicationException(sRetVal);
                }

                if (xmlString.Length == 0)
                {
                    sRetVal = "XML string cannot be empty";
                    throw new ApplicationException(sRetVal);
                }

                // Validate ID/PW
                if (!validateUser(userid, pw))
                {
                    sRetVal = "Unauthorized: " + userid + " - " + pw;
                    throw new ApplicationException(sRetVal);
                }

                // Record login
                oLU.WritetoLog("Login: " + userid);

                DateTime dWork = DateTime.Now;
                msXMLFileName = userid + dWork.Day.ToString() + dWork.Minute.ToString() + dWork.Second.ToString() + ".xml";


                // Record XML write
                oLU.WritetoLog("Writing XML: [" + cfg_XMLFilePath + msXMLFileName + "] " + xmlString.Length.ToString() + " bytes");

                // write inbound XML to file
                string xmlRet = writeXML(xmlString, cfg_XMLFilePath + msXMLFileName);
                if (xmlRet.Length != 0)
                {

                    sRetVal = "XML Error: " + userid;
                    throw new ApplicationException(sRetVal);

                }

                // Record attempt to create key
                oLU.WritetoLog("Processing XML:");

                // validate XML and process
                AIMXML.AIMXMLUtils oXMLUtils;
                oXMLUtils = new AIMXML.AIMXMLUtils();
                oXMLUtils.XMLFile = msXMLFileName;
                oXMLUtils.XMLFilePath = cfg_XMLFilePath;
                oXMLUtils.UserID = userid;
                oXMLUtils.SQL360UtilConnectStr = cfg_SQL360UtilConnStr;
                oXMLUtils.SMTPServer = cfg_smtpserver;

                // process reject
                sXMLStat = oXMLUtils.rejectCase();

                // Record return from rejectCase()
                oLU.WritetoLog("rejectCase returned:");
                oLU.WritetoLog(sXMLStat);

                if (sXMLStat.Length == 0)
                {
                    oLU.WritetoLog("CreateKey returned null");
                    oLU.WritetoLog(sXMLStat);
                    sRetVal = "Unexpected error creating request";
                    throw new ApplicationException(sRetVal);
                }

                oXMLUtils = null;

            }

            catch (Exception ex)
            {

                //catch exception  
                oLU.WritetoLog(ex.Message);
                if (ex.Message.Contains("INTERNAL"))
                {
                    sReturnStr = buildResponse("RejectCaseResponse", userid, "Exception", "Internal error processing request. Please contact Sutton Inspection Bureau technical support.");
                }
                else
                {
                    sReturnStr = buildResponse("RejectCaseResponse", userid, "Exception", ex.Message);
                }
                oLU.WritetoLog("XXXXX" + sReturnStr);
                sendErrEmail(msXMLFileName + "\r\n" + ex.Message);

            }

            finally
            {
                // close log
                if (oLU != null)
                {
                    oLU.closeLog();
                }
            }

            // return XML string to caller

            // Unknown error - empty response from importer and no error string
            if (sReturnStr.Length == 0 && sXMLStat.Length == 0)
            {
                sReturnStr = buildResponse("RejectCaseResponse", userid, "Exception", "Unknown error processing request");
            }

            // Exception occurred - return exception description
            if (sReturnStr.Length > 0)
            {
                sendErrEmail("WebService Reject Case Failed: " + System.Environment.NewLine + sReturnStr);
                return sReturnStr;
            }
            else
            {
                // Success - return output from importer
                return sXMLStat;
            }

        }

        //--------------------------------------------------------------------------------------------------------
        [WebMethod(Description = "Query the status of an existing case")]
        public string GetCaseStatus(string userid, string pw, string caseNumber)
        {

            string sReturnStr = "";
            string sRetVal = "";
            string sCaseStatus = "";
            msXMLFileName = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_XMLFilePath = colNameVal.Get("xmlfilepath");
                cfg_XSDFile = colNameVal.Get("xsdfile");
                cfg_SQL360UtilConnStr = colNameVal.Get("SQL360UtilConnStr");

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                // validate parameters
                if (userid.Length == 0)
                {
                    sRetVal = "User id required";
                    throw new ApplicationException(sRetVal);
                }

                if (pw.Length == 0)
                {
                    sRetVal = "Password required";
                    throw new ApplicationException(sRetVal);
                }

                if (caseNumber.Length == 0)
                {
                    sRetVal = "Case number required";
                    throw new ApplicationException(sRetVal);
                }

                // Validate ID/PW
                if (!validateUser(userid, pw))
                {
                    sRetVal = "Unauthorized: " + userid + " - " + pw;
                    throw new ApplicationException(sRetVal);
                }

                // valid account numbers for downloading
                string sAcntNum = "";
                if (userid == "SIBLONDONUW")
                    sAcntNum = "7305";

                // Record login
                oLU.WritetoLog("Login: " + userid);

                // Record request
                oLU.WritetoLog("Processing request for status on case#: " + caseNumber);

                // get status
                sCaseStatus = getCaseStatus360(cfg_SQL360UtilConnStr, caseNumber, sAcntNum);

                // Record return from getCaseStatus()
                if (sCaseStatus.Length == 0)
                {
                    oLU.WritetoLog("getCaseStatus returned null");
                    sRetVal = "Unexpected error getting case status";
                    throw new ApplicationException(sRetVal);
                }
                else
                {
                    oLU.WritetoLog("getCaseStatus returned:" + sCaseStatus);
                }
            }

            catch (Exception ex)
            {

                //catch exception  
                oLU.WritetoLog(ex.Message);
                if (ex.Message.Contains("INTERNAL"))
                {
                    sReturnStr = buildStatusResponse("GetCaseStatusResponse", caseNumber, "Exception", "Internal error processing request. Please contact Sutton Inspection Bureau technical support.");
                }
                else
                {
                    sReturnStr = buildStatusResponse("GetCaseStatusResponse", caseNumber, "Exception", ex.Message);
                }
                oLU.WritetoLog(sReturnStr);
                sendErrEmail(ex.Message);
            }

            finally
            {
                // close log
                if (oLU != null)
                {
                    oLU.closeLog();
                }
            }

            // return XML string to caller

            // Unknown error
            if (sReturnStr.Length == 0 && sCaseStatus.Length == 0)
            {
                sReturnStr = buildStatusResponse("GetCaseStatusResponse", caseNumber, "Exception", "Unknown error processing request");
            }

            // Exception occurred - return exception description
            if (sReturnStr.Length > 0)
            {
                sendErrEmail("WebService getCaseStatus Failed: " + System.Environment.NewLine + sReturnStr);
            }
            else
            {
                // Success - return case status
                sReturnStr = buildStatusResponse("GetCaseStatusResponse", caseNumber, "Status", sCaseStatus);
            }

            return sReturnStr;

        }

        //--------------------------------------------------------------------------------------------------------
        [WebMethod(Description = "Download PDF of completed report.")]
        public string DownloadReport(string userid, string pw, string caseNumber)
        {

            string sReturnStr = "";
            string sRetVal = "";
            string sCaseStatus = "";

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");
            cfg_XMLFilePath = colNameVal.Get("xmlfilepath");
            cfg_XSDFile = colNameVal.Get("xsdfile");
            cfg_SQL360UtilConnStr = colNameVal.Get("SQL360UtilConnStr");
            cfg_CaseFilesRoot = colNameVal.Get("CaseFilesRoot");

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();

            try
            {

                // validate parameters
                if (userid.Length == 0)
                {
                    sRetVal = "User id required";
                    throw new ApplicationException(sRetVal);
                }

                if (pw.Length == 0)
                {
                    sRetVal = "Password required";
                    throw new ApplicationException(sRetVal);
                }

                if (caseNumber.Length == 0)
                {
                    sRetVal = "Case number required";
                    throw new ApplicationException(sRetVal);
                }

                // Validate ID/PW
                if (!validateUser(userid, pw))
                {
                    sRetVal = "Unauthorized: " + userid + " - " + pw;
                    throw new ApplicationException(sRetVal);
                }

                // Record login
                oLU.WritetoLog("Login: " + userid);
                oLU.WritetoLog("Downloading case#: " + caseNumber);

                // valid account numbers for downloading
                string sAcntNum = "";
                if (userid == "SIBLONDONUW")
                    sAcntNum = "7305";

                // check case status
                sCaseStatus = getCaseStatus360(cfg_SQL360UtilConnStr, caseNumber, sAcntNum).ToUpper();
                if (sCaseStatus == "UNAUTHORIZED")
                {
                    throw new ApplicationException("Not authorized to download");
                }
                else if(sCaseStatus != "COMPLETE")
                {
                    throw new ApplicationException("Case has not been completed");
                }

                string strdocPath = getCasePDFFile(cfg_SQL360UtilConnStr, cfg_CaseFilesRoot, caseNumber, sAcntNum);
                oLU.WritetoLog("getCasePDFFile returned: " + strdocPath);

                FileStream objfilestream = new FileStream(strdocPath, FileMode.Open, FileAccess.Read);
                int len = (int)objfilestream.Length;
                Byte[] documentcontents = new Byte[len];
                objfilestream.Read(documentcontents, 0, len);
                objfilestream.Close();

                string sEncodedfile = Convert.ToBase64String(documentcontents);

                return sEncodedfile;

              }

            catch (Exception ex)
            {

                //catch exception  
                oLU.WritetoLog(ex.Message);
                if (ex.Message.Contains("INTERNAL"))
                {
                    sReturnStr = buildStatusResponse("DownloadReportResponse", caseNumber, "Exception", "Internal error processing request. Please contact Sutton Inspection Bureau technical support.");
                }
                else
                {
                    sReturnStr = buildStatusResponse("DownloadReportResponse", caseNumber, "Exception", ex.Message);
                }
                oLU.WritetoLog(sReturnStr);
                sendErrEmail(userid + "\r\n" + ex.Message);

                return sReturnStr;
            }

            finally
            {
                // close log
                if (oLU != null)
                {
                    oLU.closeLog();
                }
            }

            // return XML string to caller

            // Unknown error - empty response from importer and no error string
            //if (sReturnStr.Length == 0 && sXMLStat.Length == 0)
            //{
            //    sReturnStr = buildResponse(userid, "Exception", "Unknown error processing request");
            //}

            //// Exception occurred - return exception description
            //if (sReturnStr.Length > 0)
            //{
            //    sendErrEmail("WebService getCaseStatus Failed: " + System.Environment.NewLine + sReturnStr);
            //    return sReturnStr;
            //}
            //else
            //{
            //    // Success - return output from importer
            //    return sXMLStat;
            //}

        }

        static bool validateUser(string id, string pw)
        {

            if (id == "SIBHull" && pw == "SuttonWS0511")
            {
                return true;
            }
            else if (id == "SIBOrchid" && pw == "SuttonWS0512")
            {
                return true;
            }
            else if (id == "Orchid7443" && pw == "ZeoU39c8#")
            {
                return true;
            }
            else if (id == "SIBCoastal" && pw == "SuttonWS0513")
            {
                return true;
            }
            else if (id == "CCU7449" && pw == "BsKqKf!#")  // Coastal/CUNA
            {
                return true;
            }
            else if (id == "SIBSeacoast" && pw == "SuttonWS0215")
            {
                return true;
            }
            else if (id == "SIBJHA" && pw == "SuttonWS0915")
            {
                return true;
            }
            else if (id == "SIBTAPCO" && pw == "SuttonWS0516")
            {
                return true;
            }
            else if (id == "SIBBANKERS" && pw == "SuttonWS0418$")
            {
                return true;
            }
            else if (id == "SIBTHIG" && pw == "SF5RsvKX")
            {
                return true;
            }
            else if (id == "SIBLONDONUW" && pw == "SIb05@18$")
            {
                return true;
            }
            else if (id == "SIBHALCYON" && pw == "CaRsjKzX#")
            {
                return true;
            }
            else if (id == "SuttonAPITest" && pw == "Sutton0920$")
            {
                return true;
            }
            else if (id == "SIU7245" && pw == "1aCaDixo")
            {
                return true;
            }
            else if (id == "SIU7247" && pw == "wUVUsw66")
            {
                return true;
            }
            else if (id == "SIU7357" && pw == "gL2rubu6")
            {
                return true;
            }
            else if (id == "CIT7072" && pw == "vEoS8jK9!")
            {
                return true;
            }
            else if (id == "CT7453" && pw == "e&kW$4cH")
            {
                return true;
            }


            else
            {
                return false;
            }

        }

        static string writeXML(string xmlString, string xmlFileName)
        {
            string sRetVal = "";


            try
            {
                if (xmlString.Length == 0)
                {
                    oLU.WritetoLog("**** XML string empty: " + xmlFileName);
                    return sRetVal;
                }

                StreamWriter sr = new StreamWriter(xmlFileName);
                sr.Write(xmlString);
                sr.Flush();
                sr.Close();
                sr = null;
            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sRetVal = ex.Message;
                sendErrEmail("WriteXML " + ex.Message);
            }

            return sRetVal;

        }


        /// <summary>
        /// buildXML()
        /// </summary>
        /// <returns></returns>
        private string buildResponse(string sMode, string sUserID, string sElement, string sData)
        {
            string sRet = "";
            string sRSPFile = cfg_XMLFilePath + "RSP" + msXMLFileName;

            XmlTextWriter xmlWriter = null;
            xmlWriter = new XmlTextWriter(sRSPFile, null);

            try
            {
                DateTime dTimeStamp = DateTime.Now;

                xmlWriter.Formatting = Formatting.None;
                xmlWriter.Namespaces = false;

                xmlWriter.WriteStartDocument();

                xmlWriter.WriteStartElement("", sMode, "");

                xmlWriter.WriteStartElement("", "UserID", "");
                xmlWriter.WriteString(sUserID);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("", "TimeStamp", "");
                xmlWriter.WriteString(dTimeStamp.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("", sElement, "");
                xmlWriter.WriteString(sData);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();

                xmlWriter.Flush();
                xmlWriter.Close();

                if (File.Exists(sRSPFile))
                {

                    using (StreamReader sr = new StreamReader(sRSPFile))
                    {
                        sRet = sr.ReadToEnd();
                        sr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (xmlWriter != null)
                {
                    xmlWriter.Close();
                }
            }

            return sRet;
        }

        private string buildStatusResponse(string sMode, string sCaseNum, string sElement, string sData)
        {
            string sRet = "";
            string sRSPFile = cfg_XMLFilePath + "RSP" + msXMLFileName;

            XmlTextWriter xmlWriter = null;
            xmlWriter = new XmlTextWriter(sRSPFile, null);

            try
            {
                DateTime dTimeStamp = DateTime.Now;

                xmlWriter.Formatting = Formatting.None;
                xmlWriter.Namespaces = false;

                xmlWriter.WriteStartDocument();

                xmlWriter.WriteStartElement("", sMode, "");
                
                xmlWriter.WriteStartElement("", "TimeStamp", "");
                xmlWriter.WriteString(dTimeStamp.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("", "Case Number", "");
                xmlWriter.WriteString(sCaseNum);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("", sElement, "");
                xmlWriter.WriteString(sData);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();

                xmlWriter.Flush();
                xmlWriter.Close();

                if (File.Exists(sRSPFile))
                {

                    using (StreamReader sr = new StreamReader(sRSPFile))
                    {
                        sRet = sr.ReadToEnd();
                        sr.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (xmlWriter != null)
                {
                    xmlWriter.Close();
                }
            }

            return sRet;
        }

        //private void sendEmailConf(String sKey, string sEmail)
        //{

        //    // load configuration values from app.config
        //    System.Collections.Specialized.NameValueCollection colNameVal;
        //    colNameVal = System.Configuration.ConfigurationManager.AppSettings;
        //    cfg_smtpserver = colNameVal.Get("smtpserver");
        //    cfg_SQLConnStr = colNameVal.Get("SQLConnStr");

        //    EmailConfirmation360.EmailConf360 oEmailConf = new EmailConfirmation360.EmailConf360();

        //    oEmailConf.SQLConnString = cfg_SQLConnStr;
        //    oEmailConf.SMTPServer = cfg_smtpserver;

        //    bool bRet = oEmailConf.SendEmailConf("9095718", "jeff@sibfla.com", "H");

        //}

        static void sendErrEmail(string bodytext)
        {

            string sRet;

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            string smtpserver = colNameVal.Get("smtpserver");

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by wsRequests **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        private string getCasePDFFile(string sSQL360UtilConnectStr, string sCaseFilesRoot, string sCaseNum, string sAcntNum)
        {

            string sRet = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            string sPDFID = "";
            string sCaseID = "";
            string sCustNum = "";
            int iCaseNum = Convert.ToInt32(sCaseNum)
;

            try
            {
                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(sSQL360UtilConnectStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetCasePDFFile";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", iCaseNum);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();
                    sPDFID = sqlReader.GetSqlGuid(1).ToString();
                    sCaseID = sqlReader.GetSqlGuid(2).ToString();
                    sCustNum = sqlReader.GetSqlString(3).ToString();

                    // if customer account does not match account on case
                    if (sAcntNum != sCustNum)
                        throw new ApplicationException("Not authorized to download");

                    sRet = sCaseFilesRoot + sCaseID + @"\" + sPDFID + ".pdf";
                }
                else
                {
                    // pdf info not found - 
                    throw new ApplicationException("PDF not available for download");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                if (sqlReader != null)
                    sqlReader.Close();

                // close objects
                if (sqlConn1 != null)
                    sqlConn1.Close();
            }

            return sRet;
        }

        private string getCaseStatus360(string sSQL360UtilConnectStr, string sCaseNum, string sAcntNum)
        {

            string sStatus = "";
            string sCustNum = "";
            object oRetVal = null;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {
                // verify case belongs to requestor
                sqlConn1 = new SqlConnection(sSQL360UtilConnectStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetLookupID";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();
                oRetVal = sqlCmd1.ExecuteScalar();

                if (oRetVal != null)
                {
                    sCustNum = (string)oRetVal;
                }

                // if customer account does not match account on case
                if (sAcntNum != sCustNum)
                {
                    sStatus = "Unauthorized";
                    throw new ApplicationException("Unauthorized");
                }
                else
                {
                    sqlCmd1.CommandText = "sp_GetCaseStatus";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    oRetVal = sqlCmd1.ExecuteScalar();

                    if (oRetVal != null)
                    {
                        sStatus = (string)oRetVal;
                    }
                }
            }
            catch (Exception ex)
            {
                //record exception  
                throw ex;
            }

            finally
            {

                // close objects
                if (sqlConn1 != null)
                    sqlConn1.Close();
            }

            return sStatus;
        }

    }
    
        
}