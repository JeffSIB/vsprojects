﻿using System;
using System.Windows.Forms;


namespace APITest
{
    public partial class Form1 : Form
    {

        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_360ConnStr;
        static string cfg_outputdir;

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
            cfg_360ConnStr = colNameVal.Get("360ConnStr");
            cfg_outputdir = colNameVal.Get("outputdir");


        }

        private void bGetXML_Click(object sender, EventArgs e)
        {

            FormDataUtils.FormDataUtils fdu = new FormDataUtils.FormDataUtils();

            fdu.SQL360UtilConnectStr = cfg_360UtilConnStr;
            fdu.SQL360ConnectStr = cfg_360ConnStr;
            fdu.CaseNumber = "9850969";
            string sRetVal = fdu.LUW_GetDataForCase();


        }

    }

}
