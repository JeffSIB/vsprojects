﻿namespace APITest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bGetXML = new System.Windows.Forms.Button();
            this.tbResults = new System.Windows.Forms.TextBox();
            this.tbCaseNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bGetXML
            // 
            this.bGetXML.Location = new System.Drawing.Point(202, 12);
            this.bGetXML.Name = "bGetXML";
            this.bGetXML.Size = new System.Drawing.Size(75, 23);
            this.bGetXML.TabIndex = 0;
            this.bGetXML.Text = "Get XML";
            this.bGetXML.UseVisualStyleBackColor = true;
            this.bGetXML.Click += new System.EventHandler(this.bGetXML_Click);
            // 
            // tbResults
            // 
            this.tbResults.Location = new System.Drawing.Point(12, 61);
            this.tbResults.Multiline = true;
            this.tbResults.Name = "tbResults";
            this.tbResults.Size = new System.Drawing.Size(673, 188);
            this.tbResults.TabIndex = 1;
            // 
            // tbCaseNum
            // 
            this.tbCaseNum.Location = new System.Drawing.Point(53, 12);
            this.tbCaseNum.Name = "tbCaseNum";
            this.tbCaseNum.Size = new System.Drawing.Size(107, 20);
            this.tbCaseNum.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Case #";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 261);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbCaseNum);
            this.Controls.Add(this.tbResults);
            this.Controls.Add(this.bGetXML);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bGetXML;
        private System.Windows.Forms.TextBox tbResults;
        private System.Windows.Forms.TextBox tbCaseNum;
        private System.Windows.Forms.Label label1;
    }
}

