﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace FormDataUtils
{
    public class FormDataUtils
    {

        public string CaseNumber { get; set; }
        public string SQL360ConnectStr { get; set; }
        public string SQL360UtilConnectStr { get; set; }


        private class CaseForms
        {
            public Guid FormGUID { get; set; }
            public Guid FormInstance { get; set; }
        }

        private class qData
        {
            public string TableName { get; set; }
            public string ColName { get; set; }
        }

        private class DataPoints
        {
            public string Question { get; set; }
            public string Value { get; set; }
        }

        private List<CaseForms> FormsOnCase = new List<CaseForms>();
        private List<string> Questions = new List<string>();
        private List<DataPoints> FormData = new List<DataPoints>();

        public string LUW_GetDataForCase()
        {
            // test case 9850969


            string sReturnStr = "";

            //testSQL();

            // Load datapoints into list Questions
            loadQuestions();

            // Populate FormsOnCase with FormID & FormInstanceID for each form on case
            getFormsOnCase();

            // For each data point, load value into FormData List
            foreach (string sQuestion in Questions)
            {
                loadFormData(sQuestion);
            }

            return sReturnStr;
        }

        private void testSQL()
        {
            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            string sColName = "Occupancy";
            string sTableName = "Form." + "BriefExteriorWOE2VAL511011";
            string sFormInstance = "1a6eeb3b-2dbb-4c9f-8e7e-9644144e0d49";

            sqlConn1 = new SqlConnection(SQL360ConnectStr);
            sqlCmd1 = new SqlCommand();
            sqlCmd1.CommandType = CommandType.Text;
            sqlCmd1.CommandText = "select " + sColName + " from " + sTableName + " where formid = '" + sFormInstance + "'";

            sqlCmd1.Connection = sqlConn1;
            sqlConn1.Open();

            string sRet = sqlCmd1.ExecuteScalar().ToString();

            sqlConn1.Close();
            sqlConn1 = null;
            sqlCmd1 = null;



        }

        private void loadQuestions()
        {

            Questions.Add("Occupancy");
            Questions.Add("PlumbingCondition");

        }


        private void getFormsOnCase()
        {

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;


            sqlConn1 = new SqlConnection(SQL360UtilConnectStr);
            sqlCmd1 = new SqlCommand();
            sqlCmd1.CommandType = CommandType.StoredProcedure;
            sqlCmd1.CommandText = "sp_GetFormsForCase";

            sqlCmd1.Connection = sqlConn1;
            sqlCmd1.Parameters.Clear();
            sqlCmd1.Parameters.AddWithValue("@casenum", CaseNumber);
            sqlConn1.Open();

            sqlReader = sqlCmd1.ExecuteReader();

            if (sqlReader.HasRows)
            {
                sqlReader.Read();

                // loop through rows
                do
                {
                    CaseForms cf = new CaseForms();

                    cf.FormGUID = (Guid)sqlReader.GetGuid(0);
                    cf.FormInstance = (Guid)sqlReader.GetGuid(1);

                    FormsOnCase.Add(cf);

                } while (sqlReader.Read());     // 360

            }

            sqlReader.Close();
            sqlConn1.Close();
            sqlConn1 = null;
            sqlCmd1 = null;
        }

        private void loadFormData(string sQuestion)
        {

            // For each form on the case
            foreach (CaseForms form in FormsOnCase)
            {

                //Get the form table and col fom the data dic
                qData qd = getQuestionInfo(sQuestion, form.FormGUID.ToString());




            }


        }

        private qData getQuestionInfo(string sQuestion, string sFormID)
        {

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            qData qdRet = new qData();

            sqlConn1 = new SqlConnection(SQL360UtilConnectStr);
            sqlCmd1 = new SqlCommand();
            sqlCmd1.CommandType = CommandType.Text;
            sqlCmd1.CommandText = "select FormTableName, DBColName from FormDataDictionary where DataName = '" + sQuestion + "' and FormID = '" + sFormID + "'";

            sqlCmd1.Connection = sqlConn1;
            sqlConn1.Open();

            sqlReader = sqlCmd1.ExecuteReader();

            if (sqlReader.HasRows)
            {
                sqlReader.Read();

                do
                {

                    qdRet.TableName = sqlReader.GetString(0);
                    qdRet.ColName = sqlReader.GetString(1);

                } while (sqlReader.Read()); 

            }

            sqlReader.Close();
            sqlConn1.Close();
            sqlConn1 = null;
            sqlCmd1 = null;

            return qdRet;

        }
    }

}
