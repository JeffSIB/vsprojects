using System;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.IO;


namespace FormsByClient
{
    public partial class frmMain : Form
    {
        //public Xceed.Grid.GridControl xNewGrid = new GridControl();
        private string rptTitle = "";

        StreamReader fileToPrint;
        Font printFont;

        static FormsByClient.Program cProgram = new Program();


        public frmMain()
        {
            InitializeComponent();

            pbLoad.Visible = false;
            tbLoading.Visible = false;
            bProcess.Visible = true;
            dtpBegDate.Value = DateAndTime.DateAdd(DateInterval.Month, -6, DateAndTime.Now);

        }
        // private void prepareGrid()
        //{

        //    // place grid on panel
        //    this.panelMain.Controls.Add(xNewGrid);

        //    //GroupByRow groupByRow = new GroupByRow();
        //    //xNewGrid.FixedHeaderRows.Add(groupByRow);

        //    //// The following line creates a group-by row style (GroupByRowStyle), which sets 
        //    //// the appearance of the various grid elements contained in the group-by row as 
        //    //// well as the style of the group-by row.
        //    //GroupByRowStyle style = new GroupByRowStyle();

        //    //// The following section sets the appearance of a few of grouByRow's grid elements 
        //    //// via the GroupByRowStyle.
        //    //style.CellBackColor = Color.OliveDrab;
        //    //style.CellForeColor = Color.FloralWhite;
        //    //style.CellFont = new Font("Verdana", 8, FontStyle.Italic | FontStyle.Bold);

        //    //// The following line applies the group-by row style to groupByRow.
        //    //style.Apply(groupByRow);


        //    // The following line creates a style sheet (StyleSheet), which sets the appearance 
        //    // (Font, Color, Height,...) of the various grid elements present on the grid.
        //    StyleSheet styleSheet = new StyleSheet();

        //    // The following section sets the appearance of a few grid elements via the style sheet.
        //    styleSheet.ColumnManagerRow.ForeColor = Color.DarkBlue;
        //    styleSheet.GroupManagerRow.Font = new Font("Verdana", 7, FontStyle.Italic | FontStyle.Bold);
        //    styleSheet.GroupFooter.Font = new Font("Verdana", 8, FontStyle.Italic);
        //    styleSheet.GroupByRow.CellForeColor = Color.DarkBlue;

        //    // The folowing line applies the style sheet to the grid.
        //    xNewGrid.ApplyStyleSheet(styleSheet);


        //    // header row
        //    ColumnManagerRow xNewGridColumnManagerRow = new ColumnManagerRow();
        //    xNewGrid.FixedHeaderRows.Add(xNewGridColumnManagerRow);

        //    //COLUMNS
        //    xNewGrid.Columns.Add(new Column("Form", typeof(string)));
        //    xNewGrid.Columns["Form"].Title = "Form";
        //    xNewGrid.Columns["Form"].Width = 68;
        //    xNewGrid.Columns["Form"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Left;

        //    xNewGrid.Columns.Add(new Column("Ordered", typeof(int)));
        //    xNewGrid.Columns["Ordered"].Title = "Ordered";
        //    xNewGrid.Columns["Ordered"].Width = 68;
        //    xNewGrid.Columns["Ordered"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;

        //    xNewGrid.Columns.Add(new Column("AvgCost", typeof(string)));
        //    xNewGrid.Columns["AvgCost"].Title = "AvgCost";
        //    xNewGrid.Columns["AvgCost"].Width = 68;
        //    xNewGrid.Columns["AvgCost"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;


        //    //grid wide settings
        //    xNewGrid.ReadOnly = true;
        //    xNewGrid.Dock = DockStyle.Fill;
        //    //xNewGrid.KeyDown += new KeyEventHandler(xNewGrid_KeyDown);
        //    xNewGrid.FixedColumnSplitter.Visible = false;
        //    xNewGrid.RowSelectorPane.Visible = false;

        //    // prevent cell navigation
        //    xNewGrid.AllowCellNavigation = false;

        //}

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //private void quickReportToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    this.xNewGrid.ReportSettings.Title = rptTitle;

        //    // This is the base font that will be used throughout the report
        //    Font font = new Font("Arial", 9);
        //    this.xNewGrid.ReportStyle.Font = font;

        //    Report report = new Report(xNewGrid);
        //    report.ReportStyleSheet.PageHeader.BottomBorder.Thickness = ReportLineThickness.SingleThin;
        //    report.ReportStyleSheet.PageHeader.BottomMargin = 5;
        //    report.ReportStyleSheet.PageHeader.Font = new Font(xNewGrid.ReportStyle.Font.FontFamily, 12, FontStyle.Bold);

        //    report.ReportStyleSheet.PageHeader.LeftElement.TextFormat = "%Title%";
        //    report.ReportStyleSheet.PageHeader.RightElement.TextFormat =
        //      "Page : %Page format=000%" + Environment.NewLine +
        //      "Date : %DateTime format=g%";
        //    report.PrintPreview();
        //}

        //private void designExportToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    GenerateReportForm reportForm = new GenerateReportForm(xNewGrid);
        //    reportForm.ShowDialog();

        //}

        private void frmMain_Load(object sender, EventArgs e)
        {
            tbAccount.Focus();
        }

        private void bProcess_Click(object sender, EventArgs e)
        {

            string sAcnt = tbAccount.Text;
            string sBegDate = dtpBegDate.Value.ToShortDateString();
            string sEndDate = dtpEndDate.Value.ToShortDateString();

            tbResults.Text = "";

            if (sAcnt.Length < 4 && sAcnt.ToUpper() != "ALL")
            {
                MessageBox.Show("Please enter a valid account", "Forms by client");
                tbAccount.Focus();
                return;
            }

            if (DateAndTime.DateDiff(DateInterval.Day, dtpBegDate.Value, DateAndTime.Now, FirstDayOfWeek.System, FirstWeekOfYear.System) < 0)
            {
                MessageBox.Show("Beginning date cannot be in the future.", "Forms by client");
                return;
            }

            if (sAcnt.ToUpper() == "ALL")
            {

                if (DateAndTime.DateDiff(DateInterval.Month, dtpBegDate.Value, dtpEndDate.Value, FirstDayOfWeek.System, FirstWeekOfYear.System) > 3)
                {
                    MessageBox.Show("Report period must be a maximum of 3 months when using ALL accounts.", "Forms by client");
                    return;
                }
            }
            else
            {
                if (DateAndTime.DateDiff(DateInterval.Month, dtpBegDate.Value, dtpEndDate.Value, FirstDayOfWeek.System, FirstWeekOfYear.System) > 12)
                {
                    MessageBox.Show("Report period must be a maximum of 12 months", "Forms by client");
                    return;
                }
            }
            if (DateAndTime.DateDiff(DateInterval.Day, dtpBegDate.Value, dtpEndDate.Value, FirstDayOfWeek.System, FirstWeekOfYear.System) < 0)
            {
                MessageBox.Show("End date must be greater then beginning date.", "Forms by client");
                return;
            }


            bProcess.Visible = false;
            tbLoading.Visible = true;
            this.Refresh();

            if (bProcess.Text == "Refresh")
            {
                tbLog.Text = "";
                //bProcess.Text == "Process";
            }

            if (sAcnt.Contains(","))
            {
                tbLog.Text = "Processing report for account " + sAcnt + " for " + sBegDate + " - " + sEndDate + "\r\n\r\n";
                rptTitle = "Forms ordered by accounts " + sAcnt + " " + sBegDate + " - " + sEndDate;
            }
            else if (sAcnt.ToUpper() == "ALL")
            {
                tbLog.Text = "Processing report for ALL accounts for " + sBegDate + " - " + sEndDate + "\r\n\r\n";
                rptTitle = "Forms ordered for ALL accounts " + sBegDate + " - " + sEndDate;
            }
            else
            {
                tbLog.Text = "Processing report for account " + sAcnt + " for " + sBegDate + " - " + sEndDate + "\r\n\r\n";
                rptTitle = "Forms ordered by account " + sAcnt + " " + sBegDate + " - " + sEndDate;
            }

            this.Refresh();

            //prepareGrid();

            cProgram.LoadData(cbDetail.Checked, tbResults, sBegDate, sEndDate, sAcnt, pbLoad, this, tbLog, rptTitle);

            pbLoad.Visible = false;
            tbLoading.Visible = false;
            bProcess.Text = "Refresh";
            bProcess.Visible = true;


        }


        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {


            //string sVer =  System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();

            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                string sVer = ad.CurrentVersion.ToString();

                MessageBox.Show("Program version: " + sVer, "Forms by client");
            }
            else
            {
                MessageBox.Show("Program version: Not available", "Forms by client");
            }

        }

        private void tbAccount_TextChanged(object sender, EventArgs e)
        {

            if (tbAccount.Text.ToUpper() == "ALL")
            {
                lblDateRange.Text = "3 month maximum";
                lblDateRange.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblDateRange.Text = "1 year maximum";
                lblDateRange.ForeColor = System.Drawing.Color.Black;

            }


        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string sTempDir = Environment.GetEnvironmentVariable("TEMP");

            StreamWriter fs;
            string sTempFile = sTempDir + "\\FBC" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".txt";

            try
            {
                // Create the file.
                fs = File.CreateText(sTempFile);
                fs.Flush();
                fs.Close();
                fs = null;

                // Open for write
                fs = File.AppendText(sTempFile);

                fs.WriteLine("Forms by Client");
                fs.WriteLine(" ");
                fs.WriteLine(tbResults.Text);
                fs.WriteLine(" ");
                fs.WriteLine(" ");
                fs.WriteLine(DateAndTime.Now.ToString());

                fs.Flush();
                fs.Close();

                printDialog1.AllowSomePages = false;
                printDialog1.ShowHelp = false;
                printDialog1.Document = printDocument1;

                DialogResult result = printDialog1.ShowDialog();

                // If the result is OK then print the document.
                if (result == DialogResult.OK)
                {
                    fileToPrint = new System.IO.StreamReader(sTempFile);
                    printFont = new System.Drawing.Font("Courier New", 10);
                    printDocument1.Print();
                    fileToPrint.Close();
                }

            }


            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float yPos = 0f;
            int count = 0;
            float leftMargin = e.MarginBounds.Left;
            float topMargin = e.MarginBounds.Top;
            string line = null;
            float linesPerPage = e.MarginBounds.Height / printFont.GetHeight(e.Graphics);
            while (count < linesPerPage)
            {
                line = fileToPrint.ReadLine();
                if (line == null)
                {
                    break;
                }
                yPos = topMargin + count * printFont.GetHeight(e.Graphics);
                e.Graphics.DrawString(line, printFont, Brushes.Black, leftMargin, yPos, new StringFormat());
                count++;
            }
            if (line != null)
            {
                e.HasMorePages = true;
            }


        }

    }
}