using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Xceed.Grid;
using Xceed.Grid.Collections;
using Xceed.Grid.Editors;
using System.Data.SqlTypes;
using Microsoft.VisualBasic.ApplicationServices;



namespace FormsByClient
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_logfilename;
        static string cfg_SQLMainConnStr;

        frmDetail frmD;


        [STAThread]
        static void Main()
        {

            try
            {
                Xceed.Grid.Licenser.LicenseKey = "GRD38KM45BRABWYYN5A";

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmMain());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void LoadData(bool bDetail, TextBox tbResults, string sBegDate, string sEndDate, string sAcnt, ProgressBar pbLoad,Form frmMain, TextBox tbLog, string sRptTitle)
        {

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            //SqlConnection sqlConn2 = null;
            //SqlCommand sqlCmd2 = null;
            SqlDataReader sqlReader = null;


            // if detail 
            if (frmD != null)
            {
                frmD.Close();
                frmD = null;
            }
            frmD = new frmDetail();
            frmD.RptTitle = sRptTitle;

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_logfilename = colNameVal.Get("logfilename");
            cfg_SQLMainConnStr = colNameVal.Get("SQLMainConnStr");

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("begin main:");

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_SQLMainConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // update on screen log display
                tbLog.Text = tbLog.Text + "Running query...\r\n";
                frmMain.Refresh();


                // Get keys from sibtbl
                if (sAcnt.Contains(","))
                {
                    sqlCmd1.CommandType = CommandType.Text;
                    sqlCmd1.CommandText = "SELECT options, requesttype, inv_amt, keynumber, datecomp FROM sibtbl WHERE ([datecomp] BETWEEN '" + sBegDate + "' AND '" + sEndDate + "') AND [deleted_flag]=0 AND (";

                    char[] delim = { ','};
                    string[] sAcnts = sAcnt.Split(delim);

                    if (sAcnts.Length == 0)
                    {
                        throw new System.Exception("Invalid account entry.\r\nMultiple accounts must be specified like 9999,9999,9999");
                    }

                    bool orFlag = false;
                    string acnt;
                    foreach (string s in sAcnts)
                    {
                        acnt = s.Trim();
                        if (acnt.Length != 4)
                        {
                            throw new System.Exception("Invalid account entry.\r\nMultiple accounts must be specified like 9999,9999,9999");
                        }
                        if (orFlag)
                        {
                            sqlCmd1.CommandText += " OR ";
                        }
                        else
                        {
                            orFlag = true;
                        }

                        sqlCmd1.CommandText += "acnt_num = " + acnt;
                    }
                    sqlCmd1.CommandText += ")";
                    sqlCmd1.Parameters.Clear();

                    
                }
                else if (sAcnt.ToUpper() == "ALL")
                {
                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_FormsByClient_GetALLKeys";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@begdate", sBegDate);
                    sqlCmd1.Parameters.AddWithValue("@enddate", sEndDate);
                }
                else
                {
                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_FormsByClient_GetKeys";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@begdate", sBegDate);
                    sqlCmd1.Parameters.AddWithValue("@enddate", sEndDate);
                    sqlCmd1.Parameters.AddWithValue("@acnt", sAcnt);
                }


                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {

                    int iKeyNum = 0;
                    string sOptions = "";
                    string sReqType = "";
                    decimal dInvAmt = 0;
                    int iTotReports = 0;
                    decimal dTotInv = 0;
                    DateTime dDateComp = DateTime.Now;

                    int iF1003 = 0;
                    int iF1004 = 0;
                    int iF1005 = 0;
                    int iF1006 = 0;
                    int iF1007 = 0;
                    int iF1008 = 0;
                    int iF1008P = 0;
                    int iF1010 = 0;
                    int iF1011 = 0;
                    int iF1012P = 0;
                    int iF1012K = 0;
                    int iF1012L = 0;
                    int iF1012LG = 0;
                    int iF1012KG = 0;
                    int iF1014 = 0;
                    int iF1015 = 0;
                    int iF1023 = 0;
                    int iF1024 = 0;
                    int iF1028 = 0;
                    int iF1037 = 0;
                    int iF1039 = 0;
                    int iF1052 = 0;
                    int iF1053B = 0;
                    int iF1054 = 0;
                    int iF1056 = 0;
                    int iF1056C = 0;
                    int iF1056R = 0;
                    int iF1056T = 0;
                    int iF1057 = 0;
                    int iF1061 = 0;
                    int iF1080 = 0;
                    int iF1080AB = 0;
                    int iF1080B = 0;
                    int iF1080L = 0;
                    int iF1081 = 0;
                    int iF1082 = 0;
                    int iF1090 = 0;
                    int iF1091 = 0;
                    int iF1092 = 0;
                    int iF1097 = 0;
                    int iF1096 = 0;
                    int iF1098 = 0;
                    int iF1099 = 0;
                    int iF2002 = 0;
                    int iF3000 = 0;
                    int iF3005 = 0;
                    int iF3006 = 0;
                    int iF3009 = 0;
                    int iF3022 = 0;
                    int iF3006A = 0;
                    int iF4000 = 0;
                    int iF6056 = 0;
                    int iF6056C = 0;
                    int iF6054 = 0;


                    decimal dF1003 = 0;
                    decimal dF1004 = 0;
                    decimal dF1005 = 0;
                    decimal dF1006 = 0;
                    decimal dF1007 = 0;
                    decimal dF1008 = 0;
                    decimal dF1008P = 0;
                    decimal dF1010 = 0;
                    decimal dF1011 = 0;
                    decimal dF1012P = 0;
                    decimal dF1012K = 0;
                    decimal dF1012L = 0;
                    decimal dF1012LG = 0;
                    decimal dF1012KG = 0;
                    decimal dF1014 = 0;
                    decimal dF1015 = 0;
                    decimal dF1023 = 0;
                    decimal dF1024 = 0;
                    decimal dF1028 = 0;
                    decimal dF1037 = 0;
                    decimal dF1039 = 0;
                    decimal dF1052 = 0;
                    decimal dF1053B = 0;
                    decimal dF1054 = 0;
                    decimal dF1056 = 0;
                    decimal dF1056C = 0;
                    decimal dF1056R = 0;
                    decimal dF1056T = 0;
                    decimal dF1057 = 0;
                    decimal dF1061 = 0;
                    decimal dF1080 = 0;
                    decimal dF1080AB = 0;
                    decimal dF1080B = 0;
                    decimal dF1080L = 0;
                    decimal dF1081 = 0;
                    decimal dF1082 = 0;
                    decimal dF1090 = 0;
                    decimal dF1091 = 0;
                    decimal dF1092 = 0;
                    decimal dF1097 = 0;
                    decimal dF1096 = 0;
                    decimal dF1098 = 0;
                    decimal dF1099 = 0;
                    decimal dF2002 = 0;
                    decimal dF3000 = 0;
                    decimal dF3005 = 0;
                    decimal dF3006 = 0;
                    decimal dF3009 = 0;
                    decimal dF3006A = 0;
                    decimal dF3022 = 0;
                    decimal dF4000 = 0;
                    decimal dF6054 = 0;
                    decimal dF6056 = 0;
                    decimal dF6056C = 0;
                    bool bFound = false;
                    int itotalcases = 0;



                    //CREATE PROCEDURE [sp_FormsByClient_GetKeys]  

                    //    @BegDate	datetime,
                    //    @EndDate	datetime,
                    //    @Acnt		int


                    //AS SELECT

                    //    [options],
                    //    [requesttype],
                    //    [inv_amt],
                    //    [keynumber],
                    //    [datecomp]

                    //FROM sibtbl 

                    //WHERE

                    //    (  acnt_num = @Acnt AND ([datecomp]  BETWEEN @BegDate AND @EndDate) AND [deleted_flag]=0 )

                    // count records
                    int iRecCount = 0;
                    int iCurRec = 0;
                    sqlReader.Read();

                    if (sqlReader.HasRows)
                    {
                        do
                        {
                            iRecCount++;
                        } while (sqlReader.Read());


                        if (iRecCount > 100)
                        {
                            pbLoad.Visible = true;
                            pbLoad.Minimum = 0;
                            pbLoad.Maximum = 100;
                            pbLoad.Value = 1;
                            tbLog.Text = tbLog.Text + "Processing " + iRecCount.ToString() + " records.\r\n";
                            //frmMain.Refresh();

                        }
                    }
                    else
                    {
                        MessageBox.Show("There is no data for the selection you entered.");
                        tbLog.Text = tbLog.Text + "No data found for the given selections.\r\n";

                    }

                    if (bDetail)
                    {
                        frmD.xNewGrid.BeginInit();
                    }


                    // update on screen log display
                    tbLog.Text = tbLog.Text + "Loading data...\r\n";
                    frmMain.Refresh();
                    
                    // read again to go to beginning of data
                    sqlReader.Close();
                    sqlReader = sqlCmd1.ExecuteReader();
                    sqlReader.Read();

                    // loop through keys
                    do
                    {

                        if (sqlReader.IsDBNull(0))
                        {
                            sOptions = "";
                        }
                        else
                        {
                            sOptions = sqlReader.GetSqlString(0).ToString().ToUpper();
                            sOptions = sOptions.Trim();
                        }

                        if (sqlReader.IsDBNull(1))
                        {
                            sReqType = "";
                        }
                        else
                        {
                            sReqType = sqlReader.GetSqlString(1).ToString().ToUpper();
                            if (sReqType.Length > 1)
                            {
                                sReqType = sReqType.Remove(1);
                                if (sReqType != "C" && sReqType != "P")
                                    sReqType = "";
                            }
                            else
                            {
                                sReqType = "";
                            }
                        }

                        if (sqlReader.IsDBNull(2))
                        {
                            dInvAmt = 0;
                        }
                        else
                        {
                            dInvAmt = (decimal)sqlReader.GetSqlMoney(2);
                            dTotInv += dInvAmt;
                            iTotReports += 1;
                        }

                        iKeyNum = (int)sqlReader.GetInt32(3);

                        dDateComp = sqlReader.GetDateTime(4);

                        if (bDetail)
                        {
                            Xceed.Grid.DataRow dataRow = frmD.xNewGrid.DataRows.AddNew();
                            dataRow.Cells["KeyNumber"].Value = iKeyNum;
                            dataRow.Cells["DateComp"].Value = dDateComp;
                            dataRow.Cells["InvAmt"].Value = dInvAmt;
                            dataRow.Cells["Options"].Value = sOptions;
                            dataRow.Height = dataRow.Height * countNewLines(sOptions);


                            dataRow.EndEdit();
                        }

                        // if data is not null
                        if (sOptions != "" && sReqType != "")
                        {

                            bFound = false;

                            if (sOptions.IndexOf("ORM 1003") > 0)
                            {
                                iF1003 += 1;
                                dF1003 += dInvAmt;
                                bFound = true;
                            }

                            if (sOptions.IndexOf("ORM 1004") > 0)
                            {
                                iF1004 += 1;
                                dF1004 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1005") > 0)
                            {
                                iF1005 += 1;
                                dF1005 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1006") > 0)
                            {
                                iF1006 += 1;
                                dF1006 += dInvAmt;
                                bFound = true;
                            }

                            if (sOptions.IndexOf("ORM 1007") > 0)
                            {
                                iF1007 += 1;
                                dF1007 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("1008") > 0)
                            {
                                if (sOptions.IndexOf("1008P") > 0)
                                {
                                    iF1008P += 1;
                                    dF1008P += dInvAmt;
                                    bFound = true;
                                }
                                else
                                {
                                    iF1008 += 1;
                                    dF1008 += dInvAmt;
                                    bFound = true;
                                }
                            }
                            if (sOptions.IndexOf("ORM 1010") > 0)
                            {
                                iF1010 += 1;
                                dF1010 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1011") > 0)
                            {
                                iF1011 += 1;
                                dF1011 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("1012P") > 0)
                            {
                                iF1012P += 1;
                                dF1012P += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("1012K") > 0)
                            {
                                if (sOptions.IndexOf("1012KG") > 0)
                                {
                                    iF1012KG += 1;
                                    dF1012KG += dInvAmt;
                                    bFound = true;
                                }
                                else
                                {
                                    iF1012K += 1;
                                    dF1012K += dInvAmt;
                                    bFound = true;
                                }
                            }
                            if (sOptions.IndexOf("1012L") > 0)
                            {
                                if (sOptions.IndexOf("1012LG") > 0)
                                {
                                    iF1012LG += 1;
                                    dF1012LG += dInvAmt;
                                    bFound = true;
                                }
                                else
                                {
                                    iF1012L += 1;
                                    dF1012L += dInvAmt;
                                    bFound = true;
                                }
                            }

                            if (sOptions.IndexOf("ORM 1014") > 0)
                            {
                                iF1014 += 1;
                                dF1014 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1015") > 0)
                            {
                                iF1015 += 1;
                                dF1015 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1023") > 0)
                            {
                                iF1023 += 1;
                                dF1023 += dInvAmt;
                                bFound = true;
                            }

                            if (sOptions.IndexOf("ORM 1024") > 0)
                            {
                                iF1024 += 1;
                                dF1024 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1028") > 0)
                            {
                                iF1028 += 1;
                                dF1028 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1037") > 0)
                            {
                                iF1037 += 1;
                                dF1037 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1039") > 0)
                            {
                                iF1039 += 1;
                                dF1039 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1052") > 0)
                            {
                                iF1052 += 1;
                                dF1052 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("1053B") > 0)
                            {
                                iF1053B += 1;
                                dF1053B += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1054") > 0)
                            {
                                iF1054 += 1;
                                dF1054 += dInvAmt;
                                bFound = true;
                            }
                            // 1056
                            if (sOptions.IndexOf("ORM 1056") > 0)
                            {
                                if (sOptions.IndexOf("1056C") > 0)
                                {
                                    iF1056C += 1;
                                    dF1056C += dInvAmt;
                                    bFound = true;
                                }
                                else if (sOptions.IndexOf("1056R") > 0)
                                {
                                    iF1056R += 1;
                                    dF1056R += dInvAmt;
                                    bFound = true;
                                }
                                else if (sOptions.IndexOf("1056T") > 0)
                                {
                                    iF1056T += 1;
                                    dF1056T += dInvAmt;
                                    bFound = true;
                                }
                                else
                                {
                                    iF1056 += 1;
                                    dF1056 += dInvAmt;
                                    bFound = true;
                                }
                            }

                            if (sOptions.IndexOf("ORM 1057") > 0)
                            {
                                iF1057 += 1;
                                dF1057 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1061") > 0)
                            {
                                iF1061 += 1;
                                dF1061 += dInvAmt;
                                bFound = true;
                            }

                            // 1080 / 1080AB / 1080B / 1080L
                            if (sOptions.IndexOf("080") > 0)
                            {
                                if (sOptions.IndexOf("080A,RES") > 0)
                                {
                                    iF1080AB += 1;
                                    dF1080AB += dInvAmt;
                                    bFound = true;
                                }

                                else if (sOptions.IndexOf("080B") > 0)
                                {
                                    iF1080B += 1;
                                    dF1080B += dInvAmt;
                                    bFound = true;
                                }

                                else if (sOptions.IndexOf("080L") > 0)
                                {
                                    iF1080L += 1;
                                    dF1080L += dInvAmt;
                                    bFound = true;
                                }

                                else 
                                {
                                    iF1080 += 1;
                                    dF1080 += dInvAmt;
                                    bFound = true;
                                }

                            }
                            if (sOptions.IndexOf("081") > 0)
                            {
                                iF1081 += 1;
                                dF1081 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1082") > 0)
                            {
                                iF1082 += 1;
                                dF1082 += dInvAmt;
                                bFound = true;
                            }

                            if (sOptions.IndexOf("ORM 1090") > 0)
                            {
                                iF1090 += 1;
                                dF1090 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1091") > 0)
                            {
                                iF1091 += 1;
                                dF1091 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 1092") > 0)
                            {
                                iF1092 += 1;
                                dF1092 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("1096") > 0)
                            {
                                iF1096 += 1;
                                dF1096 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("1097") > 0)
                            {
                                iF1097 += 1;
                                dF1097 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("1098") > 0)
                            {
                                iF1098 += 1;
                                dF1098 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("1099") > 0)
                            {
                                iF1099 += 1;
                                dF1099 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 2002") > 0)
                            {
                                iF2002 += 1;
                                dF2002 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 3000") > 0)
                            {
                                iF3000 += 1;
                                dF3000 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("3005") > 0)
                            {
                                iF3005 += 1;
                                dF3005 += dInvAmt;
                                bFound = true;
                            }

                            if (sOptions.IndexOf("3006") > 0)
                            {
                                if (sOptions.IndexOf("3006A") > 0)
                                {
                                    iF3006A += 1;
                                    dF3006A += dInvAmt;
                                    bFound = true;
                                }
                                else
                                {
                                    iF3006 += 1;
                                    dF3006 += dInvAmt;
                                    bFound = true;
                                }
                            }

                            if (sOptions.IndexOf("3009") > 0)
                            {
                                iF3009 += 1;
                                dF3009 += dInvAmt;
                                bFound = true;
                            }

                            if (sOptions.IndexOf("ORM 3022") > 0)
                            {
                                iF3022 += 1;
                                dF3022 += dInvAmt;
                                bFound = true;
                            }

                            if (sOptions.IndexOf("ORM 4000") > 0)
                            {
                                iF4000 += 1;
                                dF4000 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 6054") > 0)
                            {
                                iF6054 += 1;
                                dF6054 += dInvAmt;
                                bFound = true;
                            }
                            if (sOptions.IndexOf("ORM 6056") > 0)
                            {

                                if (sOptions.IndexOf("ORM 6056C") > 0)
                                {
                                    iF6056C += 1;
                                    dF6056C += dInvAmt;
                                    bFound = true;
                                }
                                else
                                {
                                    iF6056 += 1;
                                    dF6056 += dInvAmt;
                                    bFound = true;
                                }
                            }

                            if (bFound)
                            {
                                itotalcases += 1;
                            }
                            else
                            {
                                oLU.WritetoLog(sOptions);
                                tbLog.Text = tbLog.Text + "**** Unhandled option:\r\n" + sOptions + "\r\n\r\n";

                            }
                        }

                        iCurRec++;

                        // update progress bar
                        if (iCurRec % 10 == 0)
                        {
                            pbLoad.Value = (iCurRec * 100) / iRecCount;
                            frmMain.Refresh();
                        }


                    } while (sqlReader.Read());
                    sqlReader.Close();

                    if (sAcnt.Length > 4)
                    {
                        tbResults.Text = "Accounts: " + System.Environment.NewLine;
                        tbResults.Text += "   " + sAcnt + System.Environment.NewLine;
                    }
                    else
                    {
                        tbResults.Text = "Account: " + sAcnt + System.Environment.NewLine;
                    }
                    tbResults.Text += " " + System.Environment.NewLine;

                    tbResults.Text += "Period: " + sBegDate + " - " + sEndDate + System.Environment.NewLine;
                    tbResults.Text += " " + System.Environment.NewLine;

                    tbResults.Text += "Total reports: " + iTotReports.ToString("#,#") + System.Environment.NewLine;

                    tbResults.Text += "  Avg invoice: " + (dTotInv/iTotReports).ToString("C") + System.Environment.NewLine;

                    tbResults.Text += " " + System.Environment.NewLine; 
                    tbResults.Text += "Form".PadRight(8) + "# ordered".PadLeft(8) + "Avg Inv".PadLeft(12) + System.Environment.NewLine;


                    if (iF1003 > 0)
                    {
                        tbResults.Text += "1003".PadRight(8) + iF1003.ToString("#,0").PadLeft(8) + AvgCost(dF1003, iF1003) + System.Environment.NewLine;
                    }
                    if (iF1004 > 0)
                    {
                        tbResults.Text += "1004".PadRight(8) + iF1004.ToString("#,0").PadLeft(8) + AvgCost(dF1004, iF1004) + System.Environment.NewLine;
                    }
                    if (iF1005 > 0)
                    {
                        tbResults.Text += "1005".PadRight(8) + iF1005.ToString("#,0").PadLeft(8) + AvgCost(dF1005, iF1005) + System.Environment.NewLine;
                    }
                    if (iF1006 > 0)
                    {
                        tbResults.Text += "1006".PadRight(8) + iF1006.ToString("#,0").PadLeft(8) + AvgCost(dF1006, iF1006) + System.Environment.NewLine;
                    }
                    if (iF1007 > 0)
                    {
                        tbResults.Text += "1007".PadRight(8) + iF1007.ToString("#,0").PadLeft(8) + AvgCost(dF1007, iF1007) + System.Environment.NewLine;
                    }
                    if (iF1008 > 0)
                    {
                        tbResults.Text += "1008".PadRight(8) + iF1008.ToString("#,0").PadLeft(8) + AvgCost(dF1008, iF1008) + System.Environment.NewLine;
                    }
                    if (iF1008P > 0)
                    {
                        tbResults.Text += "1008P".PadRight(8) + iF1008P.ToString("#,0").PadLeft(8) + AvgCost(dF1008P, iF1008P) + System.Environment.NewLine;
                    }
                    if (iF1010 > 0)
                    {
                        tbResults.Text += "1010".PadRight(8) + iF1010.ToString("#,0").PadLeft(8) + AvgCost(dF1010, iF1010) + System.Environment.NewLine;
                    }
                    if (iF1011 > 0)
                    {
                        tbResults.Text += "1011".PadRight(8) + iF1011.ToString("#,0").PadLeft(8) + AvgCost(dF1011, iF1011) + System.Environment.NewLine;
                    }
                    if (iF1012P > 0)
                    {
                        tbResults.Text += "1012P".PadRight(8) + iF1012P.ToString("#,0").PadLeft(8) + AvgCost(dF1012P, iF1012P) + System.Environment.NewLine;
                    }
                    if (iF1012K > 0)
                    {
                        tbResults.Text += "1012K".PadRight(8) + iF1012K.ToString("#,0").PadLeft(8) + AvgCost(dF1012K, iF1012K) + System.Environment.NewLine;
                    }
                    if (iF1012L > 0)
                    {
                        tbResults.Text += "1012L".PadRight(8) + iF1012L.ToString("#,0").PadLeft(8) + AvgCost(dF1012L, iF1012L) + System.Environment.NewLine;
                    }
                    if (iF1012KG > 0)
                    {
                        tbResults.Text += "1012KG".PadRight(8) + iF1012KG.ToString("#,0").PadLeft(8) + AvgCost(dF1012KG, iF1012KG) + System.Environment.NewLine;
                    }
                    if (iF1012LG > 0)
                    {
                        tbResults.Text += "1012LG".PadRight(8) + iF1012LG.ToString("#,0").PadLeft(8) + AvgCost(dF1012LG, iF1012LG) + System.Environment.NewLine;
                    }

                    if (iF1014 > 0)
                    {
                        tbResults.Text += "1014".PadRight(8) + iF1014.ToString("#,0").PadLeft(8) + AvgCost(dF1014, iF1014) + System.Environment.NewLine;
                    }
                    if (iF1015 > 0)
                    {
                        tbResults.Text += "1015".PadRight(8) + iF1015.ToString("#,0").PadLeft(8) + AvgCost(dF1015, iF1015) + System.Environment.NewLine;
                    }
                    if (iF1023 > 0)
                    {
                        tbResults.Text += "1023".PadRight(8) + iF1023.ToString("#,0").PadLeft(8) + AvgCost(dF1023, iF1023) + System.Environment.NewLine;
                    }
                    if (iF1024 > 0)
                    {
                        tbResults.Text += "1024".PadRight(8) + iF1024.ToString("#,0").PadLeft(8) + AvgCost(dF1024, iF1024) + System.Environment.NewLine;
                    }
                    if (iF1028 > 0)
                    {
                        tbResults.Text += "1028".PadRight(8) + iF1028.ToString("#,0").PadLeft(8) + AvgCost(dF1028, iF1028) + System.Environment.NewLine;
                    }
                    if (iF1037 > 0)
                    {
                        tbResults.Text += "1037".PadRight(8) + iF1037.ToString("#,0").PadLeft(8) + AvgCost(dF1037, iF1037) + System.Environment.NewLine;
                    }
                    if (iF1039 > 0)
                    {
                        tbResults.Text += "1039".PadRight(8) + iF1039.ToString("#,0").PadLeft(8) + AvgCost(dF1039, iF1039) + System.Environment.NewLine;
                    }
                    if (iF1052 > 0)
                    {
                        tbResults.Text += "1052".PadRight(8) + iF1052.ToString("#,0").PadLeft(8) + AvgCost(dF1052, iF1052) + System.Environment.NewLine;
                    }
                    if (iF1053B > 0)
                    {
                        tbResults.Text += "1053B".PadRight(8) + iF1053B.ToString("#,0").PadLeft(8) + AvgCost(dF1053B, iF1053B) + System.Environment.NewLine;
                    }
                    if (iF1054 > 0)
                    {
                        tbResults.Text += "1054".PadRight(8) + iF1054.ToString("#,0").PadLeft(8) + AvgCost(dF1054, iF1054) + System.Environment.NewLine;
                    }
                    if (iF1056 > 0)
                    {
                        tbResults.Text += "1056".PadRight(8) + iF1056.ToString("#,0").PadLeft(8) + AvgCost(dF1056, iF1056) + System.Environment.NewLine;
                    }
                    if (iF1056C > 0)
                    {
                        tbResults.Text += "1056C".PadRight(8) + iF1056C.ToString("#,0").PadLeft(8) + AvgCost(dF1056C, iF1056C) + System.Environment.NewLine;

                    }
                    if (iF1056R > 0)
                    {
                        tbResults.Text += "1056R".PadRight(8) + iF1056R.ToString("#,0").PadLeft(8) + AvgCost(dF1056R, iF1056R) + System.Environment.NewLine;
                    }
                    if (iF1056T > 0)
                    {
                        tbResults.Text += "1056T".PadRight(8) + iF1056T.ToString("#,0").PadLeft(8) + AvgCost(dF1056T, iF1056T) + System.Environment.NewLine;
                    }
                    if (iF1057 > 0)
                    {
                        tbResults.Text += "1057".PadRight(8) + iF1057.ToString("#,0").PadLeft(8) + AvgCost(dF1057, iF1057) + System.Environment.NewLine;
                    }
                    if (iF1061 > 0)
                    {
                        tbResults.Text += "1061".PadRight(8) + iF1061.ToString("#,0").PadLeft(8) + AvgCost(dF1061, iF1061) + System.Environment.NewLine;
                    }
                    if (iF1080 > 0)
                    {
                        tbResults.Text += "1080A".PadRight(8) + iF1080.ToString("#,0").PadLeft(8) + AvgCost(dF1080, iF1080) + System.Environment.NewLine;
                    }
                    if (iF1080B > 0)
                    {
                        tbResults.Text += "1080B".PadRight(8) + iF1080B.ToString("#,0").PadLeft(8) + AvgCost(dF1080B, iF1080B) + System.Environment.NewLine;
                    }
                    if (iF1080AB > 0)
                    {
                        tbResults.Text += "1080AB".PadRight(8) + iF1080AB.ToString("#,0").PadLeft(8) + AvgCost(dF1080AB, iF1080AB) + System.Environment.NewLine;
                    }
                    if (iF1080L > 0)
                    {
                        tbResults.Text += "1080L".PadRight(8) + iF1080L.ToString("#,0").PadLeft(8) + AvgCost(dF1080L, iF1080L) + System.Environment.NewLine;
                    }
                    if (iF1081 > 0)
                    {
                        tbResults.Text += "1081".PadRight(8) + iF1081.ToString("#,0").PadLeft(8) + AvgCost(dF1081, iF1081) + System.Environment.NewLine;
                    }
                    if (iF1082 > 0)
                    {
                        tbResults.Text += "1082".PadRight(8) + iF1082.ToString("#,0").PadLeft(8) + AvgCost(dF1082, iF1082) + System.Environment.NewLine;
                    }
                    if (iF1090 > 0)
                    {
                        tbResults.Text += "1090".PadRight(8) + iF1090.ToString("#,0").PadLeft(8) + AvgCost(dF1090, iF1090) + "\r\n";
                    }
                    if (iF1091 > 0)
                    {
                        tbResults.Text += "1091".PadRight(8) + iF1091.ToString("#,0").PadLeft(8) + AvgCost(dF1091, iF1091) + System.Environment.NewLine;
                    }
                    if (iF1092 > 0)
                    {
                        tbResults.Text += "1092".PadRight(8) + iF1092.ToString("#,0").PadLeft(8) + AvgCost(dF1092, iF1092) + System.Environment.NewLine;
                    }
                    if (iF1096 > 0)
                    {
                        tbResults.Text += "1096".PadRight(8) + iF1096.ToString("#,0").PadLeft(8) + AvgCost(dF1096, iF1096) + System.Environment.NewLine;
                    }
                    if (iF1097 > 0)
                    {
                        tbResults.Text += "1097".PadRight(8) + iF1097.ToString("#,0").PadLeft(8) + AvgCost(dF1097, iF1097) + System.Environment.NewLine;
                    }
                    if (iF1098 > 0)
                    {
                        tbResults.Text += "1098".PadRight(8) + iF1098.ToString("#,0").PadLeft(8) + AvgCost(dF1098, iF1098) + System.Environment.NewLine;
                    }
                    if (iF1099 > 0)
                    {
                        tbResults.Text += "1099".PadRight(8) + iF1099.ToString("#,0").PadLeft(8) + AvgCost(dF1099, iF1099) + System.Environment.NewLine;
                    }
                    if (iF2002 > 0)
                    {
                        tbResults.Text += "2002".PadRight(8) + iF2002.ToString("#,0").PadLeft(8) + AvgCost(dF2002, iF2002) + System.Environment.NewLine;
                    }
                    if (iF3000 > 0)
                    {
                        tbResults.Text += "3000".PadRight(8) + iF3000.ToString("#,0").PadLeft(8) + AvgCost(dF3000, iF3000) + System.Environment.NewLine;
                    }
                    if (iF3005 > 0)
                    {
                        tbResults.Text += "3005".PadRight(8) + iF3005.ToString("#,0").PadLeft(8) + AvgCost(dF3005, iF3005) + System.Environment.NewLine;
                    }

                    if (iF3006 > 0)
                    {
                        tbResults.Text += "3006".PadRight(8) + iF3006.ToString("#,0").PadLeft(8) + AvgCost(dF3006, iF3006) + System.Environment.NewLine;
                    }

                    if (iF3009 > 0)
                    {
                        tbResults.Text += "3009".PadRight(8) + iF3009.ToString("#,0").PadLeft(8) + AvgCost(dF3009, iF3009) + System.Environment.NewLine;
                    }

                    if (iF3022 > 0)
                    {
                        tbResults.Text += "3022".PadRight(8) + iF3022.ToString("#,0").PadLeft(8) + AvgCost(dF3022, iF3022) + System.Environment.NewLine;
                    }

                    if (iF3006A > 0)
                    {
                        tbResults.Text += "3006A".PadRight(8) + iF3006A.ToString("#,0").PadLeft(8) + AvgCost(dF3006A, iF3006A) + System.Environment.NewLine;
                    }
                    if (iF4000 > 0)
                    {
                        tbResults.Text += "4000".PadRight(8) + iF4000.ToString("#,0").PadLeft(8) + AvgCost(dF4000, iF4000) + System.Environment.NewLine;
                    }
                    if (iF6054 > 0)
                    {
                        tbResults.Text += "6054".PadRight(8) + iF6054.ToString("#,0").PadLeft(8) + AvgCost(dF6054, iF6054) + System.Environment.NewLine;
                    }
                    if (iF6056 > 0)
                    {
                        tbResults.Text += "6056".PadRight(8) + iF6056.ToString("#,0").PadLeft(8) + AvgCost(dF6056, iF6056) + System.Environment.NewLine;
                    }
                    if (iF6056C > 0)
                    {
                        tbResults.Text += "6056C".PadRight(8) + iF6056C.ToString("#,0").PadLeft(8) + AvgCost(dF6056C, iF6056C) + System.Environment.NewLine;
                    }

                    if (bDetail)
                    {
                        frmD.xNewGrid.EndInit();
                        frmD.xNewGrid_Resize(null, null);
                        frmD.Show();
                    }

                    oLU.WritetoLog("Total cases: " + itotalcases.ToString());
                    tbLog.Text = tbLog.Text + "Total cases processed " + itotalcases.ToString() + "\r\n";

                }

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);
                MessageBox.Show(ex.Message, "Error",MessageBoxButtons.OK,MessageBoxIcon.Error);

                //mbSendErrorMail = true;
            }

            finally
            {

                tbLog.Text += "Processing complete";
                frmMain.Focus();

                // close objects
                oLU.closeLog();

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                //if (sqlConn2 != null)
                //    sqlConn2.Close();

            }

        }

        private string AvgCost(decimal dCost, int dCount)
        { 
            decimal dRet = 0;

            if (dCount > 0)
            {

                dRet = dCost / dCount;
            
            }

            return dRet.ToString("C").PadLeft(13);
        
        }

        // IsNumeric Function
        static bool IsNumeric(object Expression)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum;

            // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
            double retNum;

            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        private int countNewLines(string strSource)
        {
            int iFound = 0;
            int iTotFinds = 1;

            if (strSource != "")
            {

                for (int i = 0; i < strSource.Length; i++)
                {

                    iFound = strSource.IndexOf(System.Environment.NewLine, i);

                    if (iFound > 0)
                    {
                        iTotFinds++;
                        i = iFound;
                    }
                    else
                        break;
                }
            }

            if (iTotFinds == 1 && strSource.Length > 125)
            {
                iTotFinds++;
            }

            return iTotFinds;


        }

      }
    }
