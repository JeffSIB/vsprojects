using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Xceed.Grid;
using Xceed.Grid.Reporting;
using Xceed.Grid.Editors;
using Xceed.Grid.Viewers;
using Xceed.Grid.Collections;
using Xceed.Editors;

namespace FormsByClient
{
    public partial class frmDetail : Form
    {

        public Xceed.Grid.GridControl xNewGrid = new GridControl();
        ColumnManagerRow xNewGridColumnManagerRow;

        private string msRptTitle;
        public string RptTitle
        {

            set
            {
                msRptTitle = value;
            }
        }

        public frmDetail()
        {
            InitializeComponent();
            prepareGrid();
        }

        public void prepareGrid()
        {

            // place grid on panel
            this.pnlMain.Controls.Add(xNewGrid);

            //GroupByRow groupByRow = new GroupByRow();
            //xNewGrid.FixedHeaderRows.Add(groupByRow);

            //// The following line creates a group-by row style (GroupByRowStyle), which sets 
            //// the appearance of the various grid elements contained in the group-by row as 
            //// well as the style of the group-by row.
            //GroupByRowStyle style = new GroupByRowStyle();

            //// The following section sets the appearance of a few of grouByRow's grid elements 
            //// via the GroupByRowStyle.
            //style.CellBackColor = Color.OliveDrab;
            //style.CellForeColor = Color.FloralWhite;
            //style.CellFont = new Font("Verdana", 8, FontStyle.Italic | FontStyle.Bold);

            //// The following line applies the group-by row style to groupByRow.
            //style.Apply(groupByRow);


            // The following line creates a style sheet (StyleSheet), which sets the appearance 
            // (Font, Color, Height,...) of the various grid elements present on the grid.
            StyleSheet styleSheet = new StyleSheet();

            // The following section sets the appearance of a few grid elements via the style sheet.
            styleSheet.ColumnManagerRow.ForeColor = Color.DarkBlue;
            styleSheet.GroupManagerRow.Font = new Font("Verdana", 7, FontStyle.Italic | FontStyle.Bold);
            styleSheet.GroupFooter.Font = new Font("Verdana", 8, FontStyle.Italic);
            styleSheet.GroupByRow.CellForeColor = Color.DarkBlue;

            // The folowing line applies the style sheet to the grid.
            xNewGrid.ApplyStyleSheet(styleSheet);

            // header row
            xNewGridColumnManagerRow = new ColumnManagerRow();
            xNewGrid.FixedHeaderRows.Add(xNewGridColumnManagerRow);

            //COLUMNS
            xNewGrid.Columns.Add(new Column("KeyNumber", typeof(int)));
            xNewGrid.Columns["KeyNumber"].Title = "Key Number";
            xNewGrid.Columns["KeyNumber"].Width = 85;
            xNewGrid.Columns["KeyNumber"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            xNewGrid.Columns["KeyNumber"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            //xNewGrid.DataRowTemplate.Cells["KeyNumber"].MouseDown += new MouseEventHandler(GridNew_cell_MouseDown);

            xNewGrid.Columns.Add(new Column("DateComp", typeof(DateTime)));
            xNewGrid.Columns["DateComp"].Title = "Date Comp";
            xNewGrid.Columns["DateComp"].Width = 85;
            xNewGrid.Columns["DateComp"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            xNewGrid.Columns["DateComp"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            //xNewGrid.DataRowTemplate.Cells["DateComp"].MouseDown += new MouseEventHandler(GridNew_cell_MouseDown);

            xNewGrid.Columns.Add(new Column("InvAmt", typeof(decimal)));
            xNewGrid.Columns["InvAmt"].Title = "Invoice";
            xNewGrid.Columns["InvAmt"].Width = 68;
            xNewGrid.Columns["InvAmt"].FormatSpecifier = "c";
            xNewGrid.Columns["InvAmt"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            //xNewGrid.DataRowTemplate.Cells["InvAmt"].MouseDown += new MouseEventHandler(GridNew_cell_MouseDown);

            xNewGrid.Columns.Add(new Column("Options", typeof(string)));
            xNewGrid.Columns["Options"].Title = "Options";
            xNewGrid.Columns["Options"].Width = 250;
            xNewGrid.Columns["Options"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Left;
            //xNewGrid.DataRowTemplate.Cells["Options"].MouseDown += new MouseEventHandler(GridNew_cell_MouseDown);

            //grid wide settings
            xNewGrid.ReadOnly = true;
            xNewGrid.Dock = DockStyle.Fill;
            xNewGrid.KeyDown += new KeyEventHandler(xNewGrid_KeyDown);
            xNewGrid.FixedColumnSplitter.Visible = false;
            xNewGrid.RowSelectorPane.Visible = false;

            // prevent cell navigation
            xNewGrid.AllowCellNavigation = false;

            // resize
            xNewGrid.Resize += new System.EventHandler(xNewGrid_Resize);


        }

        // Grid resize
        public void xNewGrid_Resize(object sender, System.EventArgs e)
        {
            int iAvailWidth = xNewGrid.DisplayRectangle.Width;
            int iTotColWidth = 0;
            int iResizeCol = 3;
            int iAdjustment = 20;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xNewGrid.Columns)
            {
                if (column.Index != iResizeCol)
                {
                    iTotColWidth += column.Width;
                }

            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xNewGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xNewGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.Width = iWidth;
            }
            else
            {
                //xResizeColumn.Width = iWidth;
            }
        }

        private void GridNew_cell_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left) && (e.Clicks >= 2))
                this.GridNew_editRow(((Cell)sender).ParentRow);
        }

        private void GridNew_editRow(CellRow folderRow)
        {
            MessageBox.Show(folderRow.Cells["Options"].Value.ToString(), "Options for key " + folderRow.Cells["KeyNumber"].Value.ToString(), MessageBoxButtons.OK);
        }

        private void xNewGrid_KeyDown(object sender, KeyEventArgs e)
        ///<summary>
        ///
        {
            switch (e.KeyData)
            {
                case Keys.Enter:
                    {
                        CellRow currentRow = xNewGrid.CurrentRow as CellRow;

                        if (currentRow != null)
                            this.GridNew_editRow(currentRow);
                        break;
                    }

                case Keys.Back:
                    //this.NavigateUp();
                    break;
            }
        }

  
        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.xNewGrid.ReportSettings.Title = msRptTitle;
            this.xNewGrid.ReportSettings.Landscape = false;

            xNewGrid_ResizeForPrint(600);

            // base font for the report
            Font font = new Font("Arial", 9);
            this.xNewGrid.ReportStyle.Font = font;

            Report report = new Report(xNewGrid);
            ReportStyleSheet reportStyleSheet = report.ReportStyleSheet;


            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            reportStyleSheet.Grid.DataRows[0].BackColor = Color.FromArgb(205, 255, 205);
            reportStyleSheet.Grid.DataRows[1].BackColor = Color.Transparent;
            reportStyleSheet.Grid.RowHeightExpansion = RowHeightExpansion.ExpandWithoutLimits;

            report.ReportStyleSheet.PageHeader.BottomBorder.Thickness = ReportLineThickness.SingleThin;
            report.ReportStyleSheet.PageHeader.BottomMargin = 5;
            report.ReportStyleSheet.PageHeader.Font = new Font(xNewGrid.ReportStyle.Font.FontFamily, 10, FontStyle.Bold);

            this.xNewGridColumnManagerRow.ReportStyle.RepeatOnEachPage = true;
            this.xNewGridColumnManagerRow.ReportStyle.BackColor = Color.DarkGray;
            this.xNewGridColumnManagerRow.ReportStyle.ForeColor = Color.White;
            this.xNewGridColumnManagerRow.ReportStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            this.xNewGridColumnManagerRow.ReportStyle.WordWrap = true;
            this.xNewGridColumnManagerRow.ReportStyle.RowHeightExpansion = RowHeightExpansion.ExpandWithoutLimits;
            this.xNewGridColumnManagerRow.ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            this.xNewGridColumnManagerRow.ReportStyle.VerticalAlignment = VerticalAlignment.Bottom;

            report.ReportStyleSheet.PageHeader.LeftElement.TextFormat = "Forms by Client Report" + Environment.NewLine + "%Title%";
            report.ReportStyleSheet.PageHeader.RightElement.TextFormat =
              "Page : %Page format=000%" + Environment.NewLine +
              "Date : %DateTime format=g%";


            report.PrintPreview();
        }

        // resize grid for print
        private void xNewGrid_ResizeForPrint(int iAvailWidth)
        {
            int iTotColWidth = 0;
            int iResizeCol = 3;
            int iAdjustment = 0;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xNewGrid.Columns)
            {
                if (column.Index != iResizeCol)
                    iTotColWidth += column.Width;
            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xNewGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xNewGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
            else
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
        }

        private void frmDetail_Shown(object sender, EventArgs e)
        {
            this.Text = msRptTitle;

        }
    
    }


}