﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Word = Microsoft.Office.Interop.Word;
using WinSCP;


namespace MacDuffDelivery
{
    class Program
    {

        static void Main(string[] args)
        {

            Setup setup = new Setup();

        }


        /// <summary>
        /// Instantiate properties from the AppSettings located in the App.config.
        /// </summary>
        public class Setup
        {

            private string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
            private string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
            private string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            private string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
            private string cfg_emailroot = ConfigurationManager.AppSettings["PDFRoot"];
            private string cfg_recroot = ConfigurationManager.AppSettings["RecRoot"];
            private string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
            private string cfg_exportapp = ConfigurationManager.AppSettings["ExportApp"];
            private string cfg_recapp = ConfigurationManager.AppSettings["RecApp"];
            private string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];

            private LogUtils.LogUtils oLU;
            private bool bErr = false;
            private string sPolicy = "";
            private string sInsured = "";
            private string sCaseNum = "";
            private string sOrderedBy = "";

            //Used for email message body.  
            private StringBuilder sbEmail = new StringBuilder();

            //Used for transmittal message body.  
            private StringBuilder sbmsgBody = new StringBuilder();

            public Setup()
            {
                int iCasesExported = 0;

                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                bool bExport = true;
                bool bCreateRecs = true;
                bool bEmailFiles = true;

                DirectoryInfo diPDF;
                DirectoryInfo diCaseFiles;

                FileInfo[] fiPDFFiles;
                FileInfo fiRec;
                oLU.WritetoLog("Begin Processing  " + DateTime.Now.ToString());
                sbEmail.Append("Begin Processing  " + DateTime.Now.ToString() + "\r\n\r\n");

                //***********************************************************
                // Export PDF's from 360
                if (bExport)
                {
                    try
                    {

                        // Delete all files in PDF Export directory.
                        string[] sFiles = Directory.GetFiles(cfg_pdfroot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);

                        // Export PDF's from 360
                        oLU.WritetoLog(" Begin exporting completed cases from 360");
                        sbEmail.Append("---- Begin exporting completed cases from 360\r\n\r\n");
                        oLU.closeLog();

                        oLU.OpenLog();
                        oLU.WritetoLog(cfg_exportapp);

                        var proc = Process.Start(cfg_exportapp);
                        proc.WaitForExit();
                        var exitCode = proc.ExitCode;
                        sbEmail.Append("Exporter returned: " + exitCode.ToString() + "\r\n");

                        // exit code should be the number of cases exported
                        iCasesExported = (int)exitCode;

                        Console.WriteLine(exitCode.ToString());

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR IN EXPORT:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                        oLU.closeLog();

                        oLU.OpenLog();

                    }

                    diPDF = new DirectoryInfo(cfg_pdfroot);
                    fiPDFFiles = diPDF.GetFiles("*.pdf");

                    if (fiPDFFiles.Count() != iCasesExported)
                    {
                        sbEmail.Append("**** PDF count does not match number exported ****\r\n");
                        sbEmail.Append("Exported: " + iCasesExported.ToString() + " - In folder: " + fiPDFFiles.Count().ToString() + "\r\n\r\n");
                    }

                }

                if (bCreateRecs && !bErr)
                {

                    //***********************************************************
                    // Build Rec docs for all PDF files that were extracted


                    //************************************************
                    // Test for connection to casefiles
                    //************************************************
                    try
                    {
                        diCaseFiles = new DirectoryInfo(cfg_casefilesroot);
                        if (!diCaseFiles.Exists)
                            throw new ApplicationException("Unable to access case files root: " + cfg_casefilesroot + "\r\n");

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR ACCESSING CASEFILES FOLDER:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                    }

                    if (!bErr)
                    {

                        // Delete all files in Rec folder.
                        string[] sFiles = Directory.GetFiles(cfg_recroot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);

                        sbEmail.Append("\r\n\r\n---- Begin creating REC Docs\r\n");

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        foreach (FileInfo file in fiPDFFiles)
                        {

                            sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));

                            ProcessStartInfo psi = new ProcessStartInfo();
                            psi.FileName = cfg_recapp;
                            psi.Arguments = sCaseNum;

                            var proc1 = Process.Start(psi);
                            proc1.WaitForExit();
                            var exitCode1 = proc1.ExitCode;
                            Console.WriteLine(exitCode1.ToString());

                            // verify rec doc was created
                            fiRec = new FileInfo(cfg_recroot + sCaseNum + ".doc");
                            if (fiRec.Exists)
                            {
                                // success
                                sbEmail.Append("Rec doc created for: " + sCaseNum + "\r\n");
                            }
                            else
                            {
                                // fail
                                sbEmail.Append("No rec doc for : " + sCaseNum + "\r\n");
                            }
                            fiRec = null;
                        }
                    }
                }

                //***********************************************************
                // Email PDF and Recs to person that ordered
                if (bEmailFiles && !bErr)
                {

                    sbEmail.Append("\r\n\r\n---- Begin emailing reports\r\n");

                    try
                    {

                        // Email all files in PDF folder - attach Rec if exist
                        int iNumToUpload = 0;
                        string sRecDoc = "";

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.PDF");

                        iNumToUpload = fiPDFFiles.Count();
                        if (iNumToUpload == 0)
                        {
                            sbEmail.Append("\r\n\r\n---- No Files to send\r\n");
                        }
                        else
                        {

                            foreach (FileInfo file in fiPDFFiles)
                            {
                                sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));

                                // If no ordered by email send to customerservices
                                GetCaseInfo(sCaseNum);
                                if (sOrderedBy == "")
                                {
                                    sOrderedBy = "customerservices@sibfla.com";
                                    sbEmail.Append("**** No ordered by email for case: " + sCaseNum + "\r\n\r\n");
                                    bErr = true;
                                }

                                // if rec doc exists, send it
                                sRecDoc = cfg_recroot + "\\" + sCaseNum + ".doc";
                                if (!File.Exists(sRecDoc))
                                {
                                    sRecDoc = "";
                                }

                                sbEmail.Append("Sending case:" + sCaseNum + " to " + sOrderedBy + "\r\n\r\n");

                                if (sRecDoc == "")
                                {
                                    sbEmail.Append("No Rec for : " + sCaseNum + "\r\n\r\n");
                                }
                                else
                                {
                                    sbEmail.Append("Sending Rec for: " + sCaseNum + " to " + sOrderedBy + "\r\n\r\n");
                                }

                                sendCaseEmail(sCaseNum + " - " + sPolicy + " " + sInsured, file.FullName, sRecDoc, sOrderedBy);

                                System.Threading.Thread.Sleep(5000);

                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR IN EXPORT:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                    }                                        
                }

                // Done - write to log, send email  and clean up
                sbEmail.Append("\r\nEnd Processing  " + DateTime.Now.ToString() + "\r\n\r\n");
                oLU.WritetoLog(sbEmail.ToString());
                oLU.closeLog();

                if (bErr)
                {
                    sendLogEmail("**** ERROR RECORDED - MacDuff Delivery Processing", sbEmail.ToString());
                }
                else
                {
                    sendLogEmail("MacDuff Delivery Processing", sbEmail.ToString());
                }


            }   //

            private void sendLogEmail(string sSubject, string sBodyText)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "noreply@sibfla.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;
                }
            }

  
            private string GetCaseOrderedBy(string sCaseNum)
            {

                string sRetVal = "";

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseOrderedBy";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {
                            sRetVal = sqlReader.GetSqlString(0).ToString();
                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }
                                        
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();
                    
                }

                return sRetVal;
            }

            private bool GetCaseInfo(string sCaseNum)
            {

                bool bRetVal = false;

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseInfo_CaseNum";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {

                            sInsured = sqlReader.GetSqlString(2).ToString();
                            sPolicy = sqlReader.GetSqlString(3).ToString();
                            sOrderedBy = sqlReader.GetSqlString(15).ToString();

                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }

                    bRetVal = true;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();


                }

                return bRetVal;
            }

            private void sendCaseEmail(string sCaseNum, string sPDF, string sRec, string sToEmail)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
                string sBodyText = "";
                if (sToEmail == "customerservice@sibfla.com")
                {
                    sBodyText = "UNABLE TO DETERMINE EMAIL FOR PERSON THAT ORDERED THIS CASE." + "\r\n\r\n" + "PLEASE FORWARD CASE TO APPROPRIATE PERSON";
                }
                else
                {
                    sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";
                }

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "sibflamail@sibfla.com";
                    oMail.MailTo = sToEmail;
                    //oMail.MailTo = "jeff@sibfla.com";
                    //oMail.MailBCC = "jeff@sibfla.com";
                    oMail.MsgSubject = "Completed inspection: " + sCaseNum;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    oMail.Attachment = sPDF;
                    oMail.Attachment2 = sRec;
                    sRet = oMail.Send();
                    if (sRet != "")
                    {
                        throw new Exception("SendMail returned: " + sRet);
                    }
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

            }

 
        }
    }
}
