﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;


namespace importSIU
{

    /// <summary>
    /// Called directly from imptweb on RudyPC
    /// Must reside on H:\VS2010\importSIU\importSIU\bin\Release
    /// </summary>
    /// 

    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyNumber { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            //public string BusinessOperations { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string ISOClass { get; set; }
            public string YearBuilt { get; set; }
            public string Occupancy { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }
        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        //static string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
        //static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        //static string cfg_SQLMainSIBIConnStr = ConfigurationManager.AppSettings["SIBIConnStr"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_archivedir = ConfigurationManager.AppSettings["ArchiveDir"];


        static LogUtils.LogUtils oLU;
        static bool bErr = false;
        static string sImportFileName = "";

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();

        //Used for confirmatino email message body.  
        static StringBuilder sbConfEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {


            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                if (args.Length == 0)
                {
                    throw new ApplicationException("No arguments passed");
                }

                // get import file name from command line
                sImportFileName = args[0];

                // does file exist?
                FileInfo fi = new FileInfo(cfg_sourcedir + sImportFileName);
                if (!fi.Exists)
                {
                    // send email
                    throw new ApplicationException("File does not exist: " + sImportFileName);
                }

            }

            catch (Exception ex)
            {
                oLU.closeLog();
                sendErrEmail("Error initializing imptSIU\r\n\r\n" + ex.Message);
                return;
            }


            // Process file
            bool bErr = false;
            try
            {

                oLU.WritetoLog("Processing: " + sImportFileName);
                sbEmail.Append(System.Environment.NewLine + "Processing: " + sImportFileName + System.Environment.NewLine);

                int iLen = 0;

                StreamReader srSource = new StreamReader(cfg_sourcedir + sImportFileName);
                string sLine = "";

                // read line 
                while ((sLine = srSource.ReadLine()) != null)
                {
                    iLen = sLine.Length;

                    if (iLen > 400)
                    {
                        if (!ImportLine(sLine))
                            bErr = true;
                    }
                    // Invalid
                    else
                    {
                        oLU.WritetoLog("Import Error: \r\n\r\nInvalid length: " + iLen.ToString());
                        sbEmail.Append("Exception Logged" + System.Environment.NewLine + "Invalid length: " + iLen.ToString() + System.Environment.NewLine);
                        bErr = true;
                    }

                }

                srSource.Close();

                // If import is successful, copy to archive dir and delete source file
                if (!bErr)
                {
                    string sSourceName = cfg_sourcedir + sImportFileName;
                    string sDestName = cfg_archivedir + sImportFileName;

                    File.Copy(sSourceName, sDestName);

                    if (File.Exists(sDestName))
                    {
                        File.Delete(sSourceName);
                    }
                    else
                    {
                        throw new ApplicationException("Copy failed for: " + sImportFileName);
                    }
                }


                //// err if nothing in conf email
                //if (sbConfEmail.Length == 0)
                //{
                //    bErr = true;
                //    throw new ApplicationException("Confirmation email empty");
                //}

                // Send confirmation
                //if (!bErr)
                //{
                //    string filenameWithoutPath = Path.GetFileName(sImportFileName);
                //    //sendConfEmail(sbConfEmail.ToString(), filenameWithoutPath);
                //}


            }   //try

            catch (Exception ex)
            {
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail("Exception logged" + System.Environment.NewLine + ex.Message);
            }

            finally
            {
                sendLogEmail(sbEmail.ToString());
                oLU.closeLog();
            }
        }

        // Import Residential //
        static bool ImportLine(string sLine)
        {
            bool bRetVal = false;
            string sInspType = "";
            //string sUWFirst = "";
            //string sUWLast = "";
            //string sUW = "";

            try

            {
                ImportRequests oAPI = new ImportRequests();
                oAPI.CustomerUserName = "APIProd";
                oAPI.CustomerPassword = "Sutton2012";
                oAPI.CustomerAccount = "7245";

                oAPI.EmailConfirmation = "";
                oAPI.PolicyNumber = sLine.Substring(6, 20).Trim();
                oAPI.EffectiveDate = "";

                oAPI.UnderwriterFirstName = "";
                oAPI.UnderwriterLastName = "";
                oAPI.UnderwriterPhone = "";
                oAPI.UnderwriterCorrEmail = "";
                oAPI.UnderwriterRptEmail = "";

                oAPI.AgencyAgentName = sLine.Substring(66, 30).Trim();
                oAPI.AgencyAgentPhone = parsePhone(sLine.Substring(96, 10).Trim());
                oAPI.AgentFax = parsePhone(sLine.Substring(106, 10).Trim());
                oAPI.AgentAddress1 = "5656 Central Ave";
                oAPI.AgentAddress2 = "";
                oAPI.AgentCity = "St. Petersburg";
                oAPI.AgentState = "FL";
                oAPI.AgentZip = "33707";

                oAPI.InsuranceCompany = "SIU";
                ////oAPI.Producer = row.Producer;

                oAPI.RushHandling = "N";
                oAPI.InsuredName = sLine.Substring(116, 30).Trim();
                //oAPI.ContactName = sFields[6];
                oAPI.ContactPhoneHome = parsePhone(sLine.Substring(217, 10).Trim());
                oAPI.MailAddress1 = sLine.Substring(146, 30).Trim();
                oAPI.MailAddress2 = "";
                oAPI.MailCity = sLine.Substring(176, 20).Trim();
                oAPI.MailState = sLine.Substring(196, 2).Trim();
                oAPI.MailZip = sLine.Substring(198, 5).Trim();
                oAPI.LocationAddress1 = sLine.Substring(146, 30).Trim();
                oAPI.LocationAddress2 = "";
                oAPI.LocationCity = sLine.Substring(176, 20).Trim();
                oAPI.LocationState = sLine.Substring(196, 2).Trim();
                oAPI.LocationZip = sLine.Substring(198, 5).Trim();
                sInspType = sLine.Substring(26, 10).Trim();

                ////oAPI.LocationContactName = row.ContactName;
                ////oAPI.LocationContactPhone = row.LocationContactPhone;

                oAPI.CoverageA = sLine.Substring(225, 8).Trim();
                oAPI.YearBuilt = sLine.Substring(233, 4).Trim();
                oAPI.Comments += System.Environment.NewLine + System.Environment.NewLine + sLine.Substring(261, 255).Trim(); ;

                //oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + sFields[21]);
                //sbEmail.Append("Importing Policy# " + sFields[21] + System.Environment.NewLine);

                oAPI.InspectionType = parseInspectionType(sInspType);
                if (oAPI.InspectionType == "")
                {
                    throw new Exception("Invalid inspection type: " + oAPI.InspectionType);
                }

                string sRet = oAPI.ImportSIU();

                //oLU.WritetoLog("oAPI.Import return for for Policy# " + sFields[21] + "\r\n\r\n" + sRet);

                var importResults = sRet.FromJSON<List<ImportResult>>();

                foreach (var importResult in importResults)
                {

                    if (importResult.Successful)
                    {
                        oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                        sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + System.Environment.NewLine);
                        //sbConfEmail.Append(sFields[31] + "\t" + importResult.CaseNumber.ToString() + "\t" + sFields[21] + "\t" + sFields[0] + System.Environment.NewLine);
                        bRetVal = true;
                    }
                    else
                    {
                        oLU.WritetoLog("**** Import failed **** "); if (importResult.Errors != null)
                        sbEmail.Append("**** Import failed ****" + System.Environment.NewLine);
                        sendErrEmail(sbEmail.ToString());

                        foreach (var error in importResult.Errors)
                        {
                            oLU.WritetoLog("Error: " + error.ErrorText);
                            sbEmail.Append("Error: " + error.ErrorText + System.Environment.NewLine);
                        }
                    }

                    if ((bool)importResult.Duplicate)
                    {
                        oLU.WritetoLog("Duplicate case");
                        sbEmail.Append("Duplicate case" + System.Environment.NewLine);
                    }
                }

            }
            catch (Exception ex)
            {
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail(sbEmail.ToString());
            }

            return bRetVal;
        }

        static string parseInspectionType(string sInspType)
        {
            string sRetVal = "";

            switch (sInspType)
            {
                case "1014":
                    sRetVal = "7245-R";
                    break;
                case "1024":
                    sRetVal = "7245-LIABS";
                    break;
                case "LIABS":
                    sRetVal = "7245-LIABS";
                    break;
                case "1061":
                    sRetVal = "7245-PROP";
                    break;
                case "1057":
                    sRetVal = "7245-PACKS";
                    break;
                case "1003":
                    sRetVal = "7245-C-BR";
                    break;
                case "C-BR":
                    sRetVal = "7245-C-BR";
                    break;
                case "PROPS":
                    sRetVal = "7245-PROPS";
                    break;
                case "PROPL":
                    sRetVal = "7245-PROPS";
                    break;
                case "PACKS":
                    sRetVal = "7245-PACKS";
                    break;
                case "PACKL":
                    sRetVal = "7245-PACKS";
                    break;
                case "TL":
                    sRetVal = "7245-TL";
                    break;
                case "TC":
                    sRetVal = "7245-TC";
                    break;
                case "LL":
                    sRetVal = "7245-LL";
                    break;
                case "RT":
                    sRetVal = "7245-RT";
                    break;
                case "MHPL":
                    sRetVal = "7245-MHPL";
                    break;
                case "MHPP":
                    sRetVal = "7245-MHPP";
                    break;
                default:
                    sRetVal = "";
                    break;
            }


            return sRetVal;
        }

        static string parsePhone(string sPhone)
        {
            string sRetVal = "";

            sPhone = sPhone.Trim();
            if (sPhone.Length == 10)
            {
                sRetVal = sPhone.Substring(0, 3) + "-" + sPhone.Substring(3, 3) + "-" + sPhone.Substring(6, 4);
            }

            return sRetVal;
        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import SIU Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import SIU Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendConfEmail(string bodytext, string sFileName)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "sibflamail@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MailBCC = "jeff@sibfla.com";
            DateTime dDate = DateTime.Today;
            oMail.MsgSubject = "Import of " + sFileName + " on " + dDate.ToShortDateString();
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }
    }
}
