﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;


namespace importRTSpecTitan
{


    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyNumber { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string AgencyAgentEmail { get; set; }
            public string AgencyAgentContact { get; set; }

            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string ContactEmail { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            public string BusinessOperations { get; set; }
            public string LocationFullAddress { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string CoverageB { get; set; }
            public string CoverageC { get; set; }
            public string CoverageD { get; set; }
            public string CoverageE { get; set; }
            public string CoverageF { get; set; }
            public string ISOClass { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }

            public string Construction { get; set; }
            public string YearBuilt { get; set; }
            public string DwellingType { get; set; }
            public string NumberOfFloors { get; set; }
            public string Occupancy { get; set; }
            public string SquareFootage { get; set; }
            public string DistanceToCoast { get; set; }
            public string RoofYear { get; set; }
            public string RoofMaterials { get; set; }
            public string RoofGeometry { get; set; }
            public string Usage { get; set; }
            public string WindMitigation { get; set; }
            public string PCClass { get; set; }
            public string RoofCladding { get; set; }
            public string RoofCondition { get; set; }
            public string BuildingCladding { get; set; }
            public string CompanyPolicyForm { get; set; }
            public string CentralFireAlarm { get; set; }
            public string CentralBurglarAlarm { get; set; }
            public string CentralAlarm { get; set; }
            public string PolicyStatus { get; set; }
            public string PoolFenced { get; set; }
            public string Heating { get; set; }
            public string HeatingFullPartial { get; set; }
            public string HeatingUpdate { get; set; }
            public string HeatingYear { get; set; }
            public string PoolAboveGround { get; set; }
            public string Farming { get; set; }
            public string NumberOfFamilies { get; set; }
            public string Pets { get; set; }
            public string Plumbing { get; set; }
            public string PlumbingFullPartial { get; set; }
            public string PlumbingUpdate { get; set; }
            public string PlumbingYear { get; set; }
            public string PoolSlide { get; set; }
            public string PoolDivingBoard { get; set; }
            public string PoolScreenEncl { get; set; }
            public string Pool { get; set; }
            public string Updates { get; set; }
            public string Wiring { get; set; }
            public string WiringFullPartial { get; set; }
            public string WiringUpdate { get; set; }
            public string WiringYear { get; set; }

            public string AltContactName { get; set; }
            public string AltContactPhone { get; set; }
            public string AltContactMPhone { get; set; }
            public string AltContactEmail { get; set; }

            public string Propane { get; set; }
            public string Valuation { get; set; }
            public string RatingArea { get; set; }
            public string RatingUnits { get; set; }
            public string RatingReceipts { get; set; }
            public string RatingPayroll { get; set; }
            public string RatingSubCost { get; set; }
            public string RatingOther { get; set; }
            public string ProSafeGuard { get; set; }
            public string SpecInst { get; set; }

            public string DeliverRptTo { get; set; }
            public string DeliverOtherTo { get; set; }






        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        //static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];


        static LogUtils.LogUtils oLU;
        static bool bErr = false;

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {

            string sExcelFileName = "";

            bool bImportSuccess = false;

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                if (args.Length == 0)
                {
                    throw new ApplicationException("No arguments passed");                
                }

                // get excel file name from command line
                sExcelFileName = args[0];

                // does file exist?
                FileInfo fi = new FileInfo(cfg_sourcedir + sExcelFileName);
                if (!fi.Exists)
                {
                    // send email
                    throw new ApplicationException("File does not exist: " + sExcelFileName);
                }

            }

            catch (Exception ex)
            {
                oLU.closeLog();
                sendErrEmail("Error initializing importRTSpecTitan\r\n\r\n" + ex.Message);
                return;
            }
            
 
            try
            {

                oLU.WritetoLog("Processing: " + sExcelFileName);
                sbEmail.Append(System.Environment.NewLine + "Processing: " + sExcelFileName + System.Environment.NewLine);

                var excel = new ExcelQueryFactory(cfg_sourcedir + sExcelFileName);
                excel.AddMapping<ImportRecord>(x => x.RushHandling, "Rush");
                excel.AddMapping<ImportRecord>(x => x.ContactName, "Contact_Name");
                excel.AddMapping<ImportRecord>(x => x.ContactPhoneHome, "Contact_Phone");
                excel.AddMapping<ImportRecord>(x => x.ContactEmail, "Contact_Email");
                excel.AddMapping<ImportRecord>(x => x.InsuredName, "Insured_Name");
                excel.AddMapping<ImportRecord>(x => x.PolicyNumber, "Policy_Number");
                excel.AddMapping<ImportRecord>(x => x.EffectiveDate, "Effective_Date");
                excel.AddMapping<ImportRecord>(x => x.BusinessOperations, "Type_of_Business");  // add to occupancy
                excel.AddMapping<ImportRecord>(x => x.LocationAddress1, "Location_Address_Line_1");
                excel.AddMapping<ImportRecord>(x => x.LocationAddress2, "Location_Address_Line_2");
                excel.AddMapping<ImportRecord>(x => x.LocationCity, "Location_City");
                excel.AddMapping<ImportRecord>(x => x.LocationState, "Location_State");
                excel.AddMapping<ImportRecord>(x => x.LocationZip, "Location_Zip");
                excel.AddMapping<ImportRecord>(x => x.InspectionType, "Report_Type");

                excel.AddMapping<ImportRecord>(x => x.AgencyAgentName, "Agency_Name");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentPhone, "Agency_Phone");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentContact, "Agency_Contact");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentEmail, "Agency_Contact_Email");

                excel.AddMapping<ImportRecord>(x => x.Propane, "Propane_on_Site");
                excel.AddMapping<ImportRecord>(x => x.Valuation, "ITV_Report");
                excel.AddMapping<ImportRecord>(x => x.RatingArea, "Rating_Area");
                excel.AddMapping<ImportRecord>(x => x.RatingUnits, "Rating_Units");
                excel.AddMapping<ImportRecord>(x => x.RatingReceipts, "Rating_Sales");
                excel.AddMapping<ImportRecord>(x => x.RatingPayroll, "Rating_Payroll");
                excel.AddMapping<ImportRecord>(x => x.RatingSubCost, "Rating_Cost");
                excel.AddMapping<ImportRecord>(x => x.RatingOther, "Rating_Other");
                excel.AddMapping<ImportRecord>(x => x.ProSafeGuard, "Verify_Protective_safeguards");
                excel.AddMapping<ImportRecord>(x => x.SpecInst, "Instructions");
                excel.AddMapping<ImportRecord>(x => x.DeliverRptTo, "Return_Completed_Report_Email");
                excel.AddMapping<ImportRecord>(x => x.DeliverOtherTo, "RT_Inspection_Contact");


                var rows = (from row in excel.Worksheet<ImportRecord>(0) select row).ToList();

                foreach (var row in rows)
                {

                    // skip row if policy number is empty
                    if (row.PolicyNumber.Length > 0)
                    {

                        ImportRequests oAPI = new ImportRequests();
                        oAPI.CustomerUserName = "APIProd";
                        oAPI.CustomerPassword = "Sutton2012";
                        oAPI.CustomerAccount = "7441";

                        // standard values provided
                        oAPI.RushHandling = row.RushHandling;
                        oAPI.ContactName = row.ContactName;
                        //oAPI.InsuredEmail = row.ContactEmail;
                        oAPI.InsuredName = row.InsuredName;
                        oAPI.ContactPhoneWork = "";
                        oAPI.ContactPhoneCell = "";
                        oAPI.ContactPhoneHome = row.ContactPhoneHome;
                        oAPI.PolicyNumber = row.PolicyNumber;
                        oAPI.EffectiveDate = row.EffectiveDate;
                        oAPI.CoverageA = "";
                        oAPI.LocationAddress1 = row.LocationAddress1;
                        oAPI.LocationAddress2 = row.LocationAddress2;
                        oAPI.LocationCity = row.LocationCity;
                        oAPI.LocationState = row.LocationState;
                        oAPI.LocationZip = row.LocationZip;
                        oAPI.MailAddress1 = row.LocationAddress1;
                        oAPI.MailAddress2 = row.LocationAddress2; ;
                        oAPI.MailCity = row.LocationCity;
                        oAPI.MailState = row.LocationState;
                        oAPI.MailZip = row.LocationZip;
                        oAPI.InsuranceCompany = "";
                        oAPI.Producer = "";
                        oAPI.DeliverToEmail = row.DeliverRptTo;


                        // Agency / Agent
                        oAPI.AgencyAgentName = row.AgencyAgentName;
                        oAPI.AgentCode = "";
                        oAPI.AgencyAgentPhone = row.AgencyAgentPhone;
                        oAPI.AgentFax = "";
                        oAPI.AgencyAgentContact = row.AgencyAgentContact;
                        oAPI.AgencyAgentEmail = row.AgencyAgentEmail;
                        oAPI.AgentAddress1 = "5656 Central Ave";
                        oAPI.AgentAddress2 = "";
                        oAPI.AgentCity = "St. Petersburg";
                        oAPI.AgentState = "FL";
                        oAPI.AgentZip = "33707";


                        // standard values not provided
                        oAPI.EmailConfirmation = "";
                        oAPI.Underwriter = "";
                        oAPI.UnderwriterFirstName = "";
                        oAPI.UnderwriterLastName = "";
                        oAPI.LocationContactPhone = "";
                        oAPI.LocationContactName = "";
                        oAPI.LocationContactPhone = "";

                        // inspection type
                        string sInspType = "";
                        string sTypeIn = row.InspectionType;

                        if (row.InspectionType == null)
                        {
                            row.InspectionType = "";
                        }
                        else
                        {
                            sTypeIn = row.InspectionType.ToUpper();

                            //PREXA
                            if (row.PolicyNumber.ToUpper().StartsWith("PXA"))
                            {
                                if (sTypeIn.Contains("CONDO"))
                                {
                                    sInspType = "7441-PXA-IE-PROP-CHMA";
                                }
                                else if (sTypeIn.Contains("PROPERTY"))
                                {
                                    sInspType = "7441-PXA-IE-PROP";
                                }
                                else if (sTypeIn.Contains("EARTH"))
                                {
                                    sInspType = "7441-PXA-EARTH";
                                }
                                else if (sTypeIn.Contains("FLOOD"))
                                {
                                    sInspType = "7441-PXA-FLOOD";
                                }
                                else
                                {
                                    sInspType = "7441-PXA-IE-PROP-CHMA";
                                    sbEmail.Append("**** Unrecognized inspection type on Policy# " + row.PolicyNumber + " - " + sTypeIn + System.Environment.NewLine);
                                    oLU.WritetoLog(System.Environment.NewLine + "**** Unrecognized inspection type on Policy# " + row.PolicyNumber + " - " + sTypeIn);
                                }
                            }
                            else
                            {

                                if (sTypeIn.Contains("ALARM") || sTypeIn.Contains("PHONE") || sTypeIn.Contains("RENEWAL"))
                                {
                                    sInspType = "7441-PHONE";
                                }
                                else if (sTypeIn.Contains("JOBSITE"))
                                {
                                    sInspType = "7441-JS";
                                }
                                else if (sTypeIn.Contains("PHOTO"))
                                {
                                    sInspType = "7441-EXT-PACK";
                                }
                                else if (sTypeIn.Contains("BASIC") || sTypeIn.Contains("CHURCH"))
                                {
                                    sInspType = "7441-IE-PACK";
                                }
                                else if (sTypeIn.Contains("BUILDER"))
                                {
                                    sInspType = "7441-IE-PROP";
                                }
                                else if (sTypeIn.Contains("LIABILITY"))
                                {
                                    sInspType = "7441-IE-LIAB";
                                }
                                else if (sTypeIn.Contains("GENERAL") || sTypeIn.Contains("GOLF") || sTypeIn.Contains("HOTEL") || sTypeIn.Contains("PACKAGE"))
                                {
                                    sInspType = "7441-IE-PACK";
                                }
                                else if (sTypeIn.Contains("PROPERTY"))
                                {
                                    sInspType = "7441-IE-PROP";
                                }
                                else if (sTypeIn.Contains("MONO-LINE WIND"))
                                {
                                    sInspType = "7441-IE-PROP";
                                }
                                else
                                {
                                    sInspType = "7441-IE-PACK";
                                    sbEmail.Append("**** Unrecognized inspection type on Policy# " + row.PolicyNumber + " - " + sTypeIn + System.Environment.NewLine);
                                    oLU.WritetoLog(System.Environment.NewLine + "**** Unrecognized inspection type on Policy# " + row.PolicyNumber + " - " + sTypeIn);

                                }
                            }   // PREXA
                        }   // = ""

                        // If inspection type is blank - throw error
                        if (sInspType.Length == 0)
                        {
                            throw new Exception("Invalid Inspection Type - File name: " + sExcelFileName + " - Policy#: " + row.PolicyNumber);
                        }

                        oAPI.InspectionType = sInspType;



                        // comments
                        oAPI.Comments = "";

                        if (row.SpecInst.Length > 0)
                            oAPI.Comments += "\r\nSpecial Instructions: " + row.SpecInst;

                        if (row.ContactEmail.Length > 0)
                            oAPI.Comments += "\r\nInsured contact email:" + row.ContactEmail;

                        if (row.Propane.ToUpper().Contains("YES"))
                            oAPI.Comments += "\r\n" + row.Propane;

                        if (row.RatingOther.Length > 0)
                            oAPI.Comments += "\r\n" + "Other rating basis: " + row.RatingOther;

                        if (row.ProSafeGuard != "N")
                            oAPI.Comments += "\r\n" + "Additional items to verify: " + row.ProSafeGuard;

                        if (row.DeliverOtherTo.Length > 0)
                            oAPI.Comments += "\r\n" + "NOT FOR INSPECTOR USE - RT inspection contact: " + row.DeliverOtherTo;


                        // additional info - value +"|" - no leading "|"
                        oAPI.AdditionalFeeAddOns = "";
                        if (row.Valuation.ToUpper() == "Y")
                            oAPI.AdditionalFeeAddOns = "Valuation|";

                        // Custom/Generic field values
                        oAPI.Occupancy = row.BusinessOperations ?? "";

                        oAPI.RatingArea = "";
                        oAPI.RatingUnits = "";
                        oAPI.RatingReceipts = "";
                        oAPI.RatingPayroll = "";
                        oAPI.RatingSubCost = "";

                        if (row.RatingArea.ToUpper() == "Y")
                            oAPI.RatingArea = "X";
                        if (row.RatingUnits.ToUpper() == "Y")
                            oAPI.RatingUnits = "X";
                        if (row.RatingReceipts.ToUpper() == "Y")
                            oAPI.RatingReceipts = "X";
                        if (row.RatingPayroll.ToUpper() == "Y")
                            oAPI.RatingPayroll = "X";
                        if (row.RatingSubCost.ToUpper() == "Y")
                            oAPI.RatingSubCost = "X";



                        //**********************************
                        // UNCOMMENT FOR TEST 
                        //oAPI.CustomerUserName = "APITest";
                        //oAPI.CustomerAccount = "9998";
                        //oAPI.InspectionType = "9998RE";
                        //oAPI.EffectiveDate = "";

                        //**********************************

                        oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + row.PolicyNumber);
                        sbEmail.Append("Importing Policy# " + row.PolicyNumber + System.Environment.NewLine);

                        string sRet = oAPI.ImportRTSpec();

                        oLU.WritetoLog("oAPI.Import return for for Policy# " + row.PolicyNumber + "\r\n\r\n" + sRet);

                        var importResults = sRet.FromJSON<List<ImportResult>>();

                        foreach (var importResult in importResults)
                        {

                            if (importResult.Successful)
                            {
                                oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                                sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + System.Environment.NewLine);
                            }
                            else
                            {
                                oLU.WritetoLog("**** Import failed **** "); if (importResult.Errors != null)
                                    sbEmail.Append("**** Import failed ****" + System.Environment.NewLine);

                                foreach (var error in importResult.Errors)
                                {
                                    oLU.WritetoLog("Error: " + error.ErrorText);
                                    sbEmail.Append("Error: " + error.ErrorText + System.Environment.NewLine);
                                }
                            }

                            if ((bool)importResult.Duplicate)
                            {
                                oLU.WritetoLog("Duplicate case");
                                sbEmail.Append("Duplicate case" + System.Environment.NewLine);
                            }
                        }

                    }   // Policy number not empty

                }   // foreach row in sheet

                bImportSuccess = true;
                
            }   //try

            catch (Exception ex)
            {
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail("Exception logged" + System.Environment.NewLine + ex.Message);
            }

            finally
            {

                // Copy source file to holding area and delete temp
                string sSourceName = cfg_sourcedir + sExcelFileName;
                string sDestName = "";

                //if successful - copy to Processed
                //if failed - Fail
                if (bImportSuccess)
                {
                    sDestName = cfg_compdir + sExcelFileName;
                }
                else
                {
                    sDestName = cfg_faildir + sExcelFileName;
                }

                // Copy Excel
                File.Copy(sSourceName, sDestName);
                if (File.Exists(sDestName))
                {
                    File.Delete(sSourceName);
                }
                else
                {
                    throw new ApplicationException("Copy failed for: " + sExcelFileName);
                }

                sendLogEmail(sbEmail.ToString());
                oLU.closeLog();
            }
            
        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import importRTSpecTitan Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by importRTSpecTitan Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }
    }
}
