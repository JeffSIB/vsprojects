﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace ThresholdReport
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360ConnStr;
        static string cfg_360UtilConnStr;
        static string cfg_SQLMainSIBIConnStr;
        static string cfg_SQLMainUTILConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static string cfg_deliverto;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Range oRange = null;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;

        static void Main(string[] args)
        {
            
            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");
            cfg_360ConnStr = colNameVal.Get("360ConnStr");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");
            cfg_SQLMainUTILConnStr = colNameVal.Get("SQLMainUTILConnStr");
            cfg_outputdir = colNameVal.Get("outputdir");
            cfg_emailnotify = colNameVal.Get("emailnotify");
            cfg_deliverto = colNameVal.Get("deliverto");


            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();
            oLU.WritetoLog("++++ Begin ++++");

            // 360 

            Process360();

            // Build SIB Work table containing all pending items
            //BuildSIBZipCodeSkillSet();

            // SIB - Current period  
            //ProcessSIB();

            // Totals 
            BuildExcel();

            oLU.closeLog();

        }


        static void Process360()
        {

            // Populate table ThresholdReport with cases with a status of Ordered
                       
            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iRet = 0;
            string sDistrict = "";
            string sState = "";
            string sCity = "";
            string sCounty = "";
            int i1CM = 0;
            int i2CM = 0;
            int i3CM = 0;
            int i1RES = 0;
            int i2RES = 0;
            int i3RES = 0;
            int iTEL = 0;
            int iNP = 0;
            int iWC = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {


                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd2 = new SqlCommand();
                sqlConn1.Open();

                // clear table 
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "DELETE FROM ThresholdReport";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.ExecuteNonQuery();                                
                
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "dbo.sp_ThresholdReport_Pivot";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();

                // Second SQL conn for updating table
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "dbo.sp_ThresholdReport_Update";
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                // Get all completed cases
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through districts
                    do
                    {

                        //[1CM], [2CM], [3CM], [1RES], [2RES], [3RES], [NP], [TEL], [WC]

                        //District code
                        if (sqlReader.IsDBNull(0))
                        {
                            sDistrict = "*NA*";
                        }
                        else
                        {
                            sDistrict = sqlReader.GetSqlString(0).ToString();
                        }

                        //City
                        if (sqlReader.IsDBNull(1))
                        {
                            sCity = "";
                        }
                        else
                        {
                            sCity = sqlReader.GetSqlString(1).ToString();
                        }

                        //County
                        if (sqlReader.IsDBNull(2))
                        {
                            sCounty = "";
                        }
                        else
                        {
                            sCounty = sqlReader.GetSqlString(2).ToString();
                        }

                        //State
                        if (sqlReader.IsDBNull(3))
                        {
                            sState = "";
                        }
                        else
                        {
                            sState = sqlReader.GetSqlString(3).ToString();
                        }




                        // num ordered by skill set
                        i1CM = (int)sqlReader.GetSqlInt32(4);
                        i2CM = (int)sqlReader.GetSqlInt32(5);
                        i3CM = (int)sqlReader.GetSqlInt32(6);
                        i1RES = (int)sqlReader.GetSqlInt32(7);
                        i2RES = (int)sqlReader.GetSqlInt32(8);
                        i3RES = (int)sqlReader.GetSqlInt32(9);
                        iNP = (int)sqlReader.GetSqlInt32(10);
                        iTEL = (int)sqlReader.GetSqlInt32(11);
                        iWC = (int)sqlReader.GetSqlInt32(12);

                        // Insert into table
                        sqlCmd2.Parameters.Clear();

                        sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                        sqlCmd2.Parameters.AddWithValue("@city", sCity);
                        sqlCmd2.Parameters.AddWithValue("@county", sCounty);
                        sqlCmd2.Parameters.AddWithValue("@state", sState);
                        sqlCmd2.Parameters.AddWithValue("@1CMCur", i1CM);
                        sqlCmd2.Parameters.AddWithValue("@2CMCur", i2CM);
                        sqlCmd2.Parameters.AddWithValue("@3CMCur", i3CM);
                        sqlCmd2.Parameters.AddWithValue("@1RESCur", i1RES);
                        sqlCmd2.Parameters.AddWithValue("@2RESCur", i2RES);
                        sqlCmd2.Parameters.AddWithValue("@3RESCur", i3RES);
                        sqlCmd2.Parameters.AddWithValue("@NPCur", iNP);
                        sqlCmd2.Parameters.AddWithValue("@TELCur", iTEL);
                        sqlCmd2.Parameters.AddWithValue("@WCCur", iWC);

                        iRet = sqlCmd2.ExecuteNonQuery();

                        if (iRet == 0)
                            throw new SystemException("sp_ThresholdReport_Update returned 0 for " + sDistrict);

                    } while (sqlReader.Read());     // districts

                    sqlReader.Close();

                }   // has rows

                oLU.WritetoLog("---- End ----");

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn1.Close();
            }
        }

        static void ProcessSIB()
        {

            // Populate table ThresholdReport (in 360)

            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iRet = 0;
            string sDistrict = "";
            string sState = "";
            string sCity = "";
            int i1CM = 0;
            int i2CM = 0;
            int i3CM = 0;
            int i1RES = 0;
            int i2RES = 0;
            int i3RES = 0;
            int iTEL = 0;
            int iNP = 0;
            int iWC = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin SIB ++++");

                // set up SQL connection (SIBUtil)
                sqlConn1 = new SqlConnection(cfg_SQLMainUTILConnStr);
                sqlCmd1 = new SqlCommand();
                sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd2 = new SqlCommand();
                sqlConn1.Open();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "dbo.sp_ThresholdReport_Pivot";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();

                // Second SQL conn for updating table
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "dbo.sp_ThresholdReport_Update";
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                // Read piviot query
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through records
                    do
                    {

                        //[1CM], [2CM], [3CM], [1RES], [2RES], [3RES], [NP], [TEL], [WC]

                        //District code
                        if (sqlReader.IsDBNull(0))
                        {
                            sDistrict = "*NA*";
                        }
                        else
                        {
                            sDistrict = sqlReader.GetSqlString(0).ToString();
                        }

                        //City
                        if (sqlReader.IsDBNull(1))
                        {
                            sCity = "";
                        }
                        else
                        {
                            sCity = sqlReader.GetSqlString(1).ToString();
                        }

                        //State
                        if (sqlReader.IsDBNull(2))
                        {
                            sState = "";
                        }
                        else
                        {
                            sState = sqlReader.GetSqlString(2).ToString();
                        }


                        //****** add city and state to insert ****


                        // num ordered by skill set
                        i1CM = (int)sqlReader.GetSqlInt32(3);
                        i2CM = (int)sqlReader.GetSqlInt32(4);
                        i3CM = (int)sqlReader.GetSqlInt32(5);
                        i1RES = (int)sqlReader.GetSqlInt32(6);
                        i2RES = (int)sqlReader.GetSqlInt32(7);
                        i3RES = (int)sqlReader.GetSqlInt32(8);
                        iNP = (int)sqlReader.GetSqlInt32(9);
                        iTEL = (int)sqlReader.GetSqlInt32(10);
                        iWC = (int)sqlReader.GetSqlInt32(11);

                        // Insert into table
                        sqlCmd2.Parameters.Clear();

                        sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                        sqlCmd2.Parameters.AddWithValue("@city", sCity);
                        sqlCmd2.Parameters.AddWithValue("@state", sState);
                        sqlCmd2.Parameters.AddWithValue("@1CMCur", i1CM);
                        sqlCmd2.Parameters.AddWithValue("@2CMCur", i2CM);
                        sqlCmd2.Parameters.AddWithValue("@3CMCur", i3CM);
                        sqlCmd2.Parameters.AddWithValue("@1RESCur", i1RES);
                        sqlCmd2.Parameters.AddWithValue("@2RESCur", i2RES);
                        sqlCmd2.Parameters.AddWithValue("@3RESCur", i3RES);
                        sqlCmd2.Parameters.AddWithValue("@NPCur", iNP);
                        sqlCmd2.Parameters.AddWithValue("@TELCur", iTEL);
                        sqlCmd2.Parameters.AddWithValue("@WCCur", iWC);

                        iRet = sqlCmd2.ExecuteNonQuery();

                        if (iRet == 0)
                            throw new SystemException("sp_ThresholdReport_Update returned 0 for " + sDistrict);

                    } while (sqlReader.Read());     // districts

                    sqlReader.Close();

                }   // has rows

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End ----");

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn1.Close();


            }


        }

        static void BuildExcel()
        {

            int iRow = 0;
            string sDistrict = "";
            string sCity = "";
            string sCounty = "";
            string sState = "";
            int i1CM = 0;
            int i2CM = 0;
            int i3CM = 0;
            int i1RES = 0;
            int i2RES = 0;
            int i3RES = 0;
            int iTEL = 0;
            int iNP = 0;
            int iWC = 0;
            int iDistTotal = 0;

            int i1CMTot = 0;
            int i2CMTot = 0;
            int i3CMTot = 0;
            int i1RESTot = 0;
            int i2RESTot = 0;
            int i3RESTot = 0;
            int iTELTot = 0;
            int iNPTot = 0;
            int iWCTot = 0;
            int iTotal = 0;


            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            string sDateDisp = dStatDate.Month + "-" + dStatDate.Day + "-" + dStatDate.Year;
           // string sEmailSubject = "Customer Rejects - " + dBegDate.Month + "/" + dBegDate.Day + " - " + dEndDate.Month + "/" + dEndDate.Day + ", " + dBegDate.Year.ToString();
            string sExcelFileNameNoEx = cfg_outputdir + "ThresholdReport_" + sDateDisp;
            string sExcelFileName = sExcelFileNameNoEx + ".xlsx";

            try
            {

                oExcel = new Excel.Application();
                oExcel.Visible = false;
                oWorkbook = oExcel.Workbooks.Add(1);
                o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                //o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing,oSIBWorksheet,oMissing,oMissing);
                o360Worksheet.Name = "Threshold";
                createHeader(o360Worksheet);

                iRow = 4;

                // set up SQL connection (SIBUtil)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlConn1.Open();

                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT * FROM dbo.ThresholdReport ORDER BY State, County, City, District";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {

                    sqlReader.Read();

                    // loop through districts
                    do
                    {

                        //[1CM], [2CM], [3CM], [1RES], [2RES], [3RES], [NP], [TEL], [WC]

                        //District code
                        if (sqlReader.IsDBNull(0))
                        {
                            sDistrict = "*NA*";
                        }
                        else
                        {
                            sDistrict = sqlReader.GetSqlString(0).ToString();
                        }

                        //City
                        if (sqlReader.IsDBNull(1))
                        {
                            sCity = "";
                        }
                        else
                        {
                            sCity = sqlReader.GetSqlString(1).ToString();
                        }

                        //County
                        if (sqlReader.IsDBNull(2))
                        {
                            sCounty = "";
                        }
                        else
                        {
                            sCounty = sqlReader.GetSqlString(2).ToString();
                        }



                        //State
                        if (sqlReader.IsDBNull(3))
                        {
                            sState = "";
                        }
                        else
                        {
                            sState = sqlReader.GetSqlString(3).ToString();
                        }

                        // num ordered by skill set
                        i1CM = (int)sqlReader.GetSqlInt32(4);
                        i2CM = (int)sqlReader.GetSqlInt32(5);
                        i3CM = (int)sqlReader.GetSqlInt32(6);
                        i1RES = (int)sqlReader.GetSqlInt32(7);
                        i2RES = (int)sqlReader.GetSqlInt32(8);
                        i3RES = (int)sqlReader.GetSqlInt32(9);
                        iNP = (int)sqlReader.GetSqlInt32(10);
                        iTEL = (int)sqlReader.GetSqlInt32(11);
                        iWC = (int)sqlReader.GetSqlInt32(12);

                        iDistTotal = i1CM + i2CM + i3CM + i1RES + i2RES + i3RES + iNP + iTEL + iWC;
                        i1CMTot += i1CM;
                        i2CMTot += i2CM;
                        i3CMTot += i3CM;
                        i1RESTot += i1RES;
                        i2RESTot += i2RES;
                        i3RESTot += i3RES;
                        iNPTot += iNP;
                        iTELTot += iTEL;
                        iWCTot += iWC;
                        iTotal += iDistTotal;

                        addData(o360Worksheet, iRow, 1, sDistrict.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                        addData(o360Worksheet, iRow, 2, sCity.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                        addData(o360Worksheet, iRow, 3, sCounty.ToString(), "C" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                        addData(o360Worksheet, iRow, 4, sState.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "", "");
                        addData(o360Worksheet, iRow, 5, iDistTotal.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 6, i1CM.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 7, i2CM.ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 8, i3CM.ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 9, i1RES.ToString(), "I" + iRow.ToString(), "I" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 10, i2RES.ToString(), "J" + iRow.ToString(), "J" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 11, i3RES.ToString(), "K" + iRow.ToString(), "K" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 12, iNP.ToString(), "L" + iRow.ToString(), "L" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 13, iTEL.ToString(), "M" + iRow.ToString(), "M" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 14, iWC.ToString(), "N" + iRow.ToString(), "N" + iRow.ToString(), "", "C");

                        iRow++;

                    } while (sqlReader.Read());     // districts

                    addData(o360Worksheet, iRow, 1, "Total", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                    addData(o360Worksheet, iRow, 5, iTotal.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "", "C");
                    addData(o360Worksheet, iRow, 6, i1CMTot.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "", "C");
                    addData(o360Worksheet, iRow, 7, i2CMTot.ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "", "C");
                    addData(o360Worksheet, iRow, 8, i3CMTot.ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "", "C");
                    addData(o360Worksheet, iRow, 9, i1RESTot.ToString(), "I" + iRow.ToString(), "I" + iRow.ToString(), "", "C");
                    addData(o360Worksheet, iRow, 10, i2RESTot.ToString(), "J" + iRow.ToString(), "J" + iRow.ToString(), "", "C");
                    addData(o360Worksheet, iRow, 11, i3RESTot.ToString(), "K" + iRow.ToString(), "K" + iRow.ToString(), "", "C");
                    addData(o360Worksheet, iRow, 12, iNPTot.ToString(), "L" + iRow.ToString(), "L" + iRow.ToString(), "", "C");
                    addData(o360Worksheet, iRow, 13, iTELTot.ToString(), "M" + iRow.ToString(), "M" + iRow.ToString(), "", "C");
                    addData(o360Worksheet, iRow, 14, iWCTot.ToString(), "N" + iRow.ToString(), "N" + iRow.ToString(), "", "C");

                    sqlReader.Close();

                }   // has rows

                //// Chart
                //Excel.ChartObjects xlCharts = (Excel.ChartObjects)o360Worksheet.ChartObjects(Type.Missing);

                //// District A
                //Excel.ChartObject ChartA = (Excel.ChartObject)xlCharts.Add(10, 300, 300, 250);
                //Excel.Chart chartPageA = ChartA.Chart;
                //chartPageA.HasTitle = true;
                //chartPageA.HasLegend = true;
                //chartPageA.ChartTitle.Text = "District A";

                //Excel.SeriesCollection seriesCollection = chartPageA.SeriesCollection();
                //Excel.Series series1 = seriesCollection.NewSeries();
                //series1.XValues = o360Worksheet.Range["C3", "K3"];
                //series1.Values = o360Worksheet.Range["C4", "K4"];

                //chartPageA.ChartType = Excel.XlChartType.xlPie;
                //Excel.Axis axis = chartPageA.Axes(Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary) as Excel.Axis;
                //series1.ApplyDataLabels(Excel.XlDataLabelsType.xlDataLabelsShowPercent, true, true, false, false, false, false, true);

                //// District B
                //Excel.ChartObject ChartB = (Excel.ChartObject)xlCharts.Add(310, 300, 300, 250);
                //Excel.Chart chartPageB = ChartB.Chart;
                //chartPageB.HasTitle = true;
                //chartPageB.HasLegend = true;
                //chartPageB.ChartTitle.Text = "District B";

                //Excel.SeriesCollection seriesCollectionB = chartPageB.SeriesCollection();
                //Excel.Series seriesB = seriesCollectionB.NewSeries();
                //seriesB.XValues = o360Worksheet.Range["C3", "K3"];
                //seriesB.Values = o360Worksheet.Range["C5", "K5"];

                //chartPageB.ChartType = Excel.XlChartType.xlPie;
                //Excel.Axis axisB = chartPageB.Axes(Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary) as Excel.Axis;
                //seriesB.ApplyDataLabels(Excel.XlDataLabelsType.xlDataLabelsShowPercent, true, true, false, false, false, false, true);

                //// District C
                //Excel.ChartObject ChartC = (Excel.ChartObject)xlCharts.Add(610, 300, 300, 250);
                //Excel.Chart chartPageC = ChartC.Chart;
                //chartPageC.HasTitle = true;
                //chartPageC.HasLegend = true;
                //chartPageC.ChartTitle.Text = "District C";

                //Excel.SeriesCollection seriesCollectionC = chartPageC.SeriesCollection();
                //Excel.Series seriesC = seriesCollectionC.NewSeries();
                //seriesC.XValues = o360Worksheet.Range["C3", "K3"];
                //seriesC.Values = o360Worksheet.Range["C6", "K6"];

                //chartPageC.ChartType = Excel.XlChartType.xlPie;
                //Excel.Axis axisC = chartPageB.Axes(Excel.XlAxisType.xlValue, Microsoft.Office.Interop.Excel.XlAxisGroup.xlPrimary) as Excel.Axis;
                //seriesC.ApplyDataLabels(Excel.XlDataLabelsType.xlDataLabelsShowPercent, true, true, false, false, false, false, true);


                oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.Close(true, oMissing, oMissing);
                oExcel.Quit();
                msMsgBody += "Processing completed for 360" + System.Environment.NewLine + cfg_outputdir + "ReviewerStatsSIB_" + sDateDisp + System.Environment.NewLine + System.Environment.NewLine;

                releaseObject(oExcel);
                releaseObject(oWorkbook);
                releaseObject(o360Worksheet);
                //sendExcelFile(sEmailSubject, sExcelFileName);
                msMsgBody += "File sent " + sExcelFileName;
                oLU.WritetoLog("---- End ----");

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }


        }




        static void FinalizeTable()
        {

            // Populate table ZipCodeDistrict2Month
            // Current vs Previous month determine by bCurPD
            // CurPD = true gets data 1 month prior to run date 
            // CurPD = false gets data 2 months prior to run date             

             int iRet = 0;
            string sDistrict = "";
            string sSubDistrict = "";
            int i1CM = 0;
            int i2CM = 0;
            int i3CM = 0;
            int i1RES = 0;
            int i2RES = 0;
            int i3RES = 0;
            int iTEL = 0;
            int iNP = 0;
            int iWC = 0;
            int iTotal = 0;
            int iTotalP = 0;
            int i1CMP = 0;
            int i2CMP = 0;
            int i3CMP = 0;
            int i1RESP = 0;
            int i2RESP = 0;
            int i3RESP = 0;
            int iTELP = 0;
            int iNPP = 0;
            int iWCP = 0;

            decimal dTotPct = 0;
            decimal d1CMPct = 0;
            decimal d2CMPct = 0;
            decimal d3CMPct = 0;
            decimal d1RESPct = 0;
            decimal d2RESPct = 0;
            decimal d3RESPct = 0;
            decimal dTELPct = 0;
            decimal dNPPct = 0;
            decimal dWCPct = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Finalize ++++");

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd2 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT * FROM ZipCodeDistrict2Month";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // Second SQL conn for updating table
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "dbo.sp_ZipCodeDist2Month_Finalize";
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                // process all rows
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        sSubDistrict = sqlReader.GetSqlString(1).ToString();

                        sDistrict = sSubDistrict.Substring(0, 1);

                        // get current & prev month numbers
                        i1CM = (int)sqlReader.GetSqlInt32(5);
                        i1CMP = (int)sqlReader.GetSqlInt32(6);
                        i2CM = (int)sqlReader.GetSqlInt32(8);
                        i2CMP = (int)sqlReader.GetSqlInt32(9);
                        i3CM = (int)sqlReader.GetSqlInt32(11);
                        i3CMP = (int)sqlReader.GetSqlInt32(12);
                        i1RES = (int)sqlReader.GetSqlInt32(14);
                        i1RESP = (int)sqlReader.GetSqlInt32(15);
                        i2RES = (int)sqlReader.GetSqlInt32(17);
                        i2RESP = (int)sqlReader.GetSqlInt32(18);
                        i3RES = (int)sqlReader.GetSqlInt32(20);
                        i3RESP = (int)sqlReader.GetSqlInt32(21);
                        iNP = (int)sqlReader.GetSqlInt32(23);
                        iNPP = (int)sqlReader.GetSqlInt32(24);
                        iTEL = (int)sqlReader.GetSqlInt32(26);
                        iTELP = (int)sqlReader.GetSqlInt32(27);
                        iWC = (int)sqlReader.GetSqlInt32(29);
                        iWCP = (int)sqlReader.GetSqlInt32(30);
                        
                        
                        // Update table
                        sqlCmd2.Parameters.Clear();

                        iTotal = i1CM + i2CM + i3CM + i1RES + i2RES + i3RES + iNP + iTEL + iWC;
                        iTotalP = i1CMP + i2CMP + i3CMP + i1RESP + i2RESP + i3RESP + iNPP + iTELP + iWCP;

                        dTotPct = calcPct(iTotal, iTotalP);
                        d1CMPct = calcPct(i1CM, i1CMP);
                        d2CMPct = calcPct(i2CM, i2CMP);
                        d3CMPct = calcPct(i3CM, i3CMP);
                        d1RESPct = calcPct(i1RES, i1RESP);
                        d2RESPct = calcPct(i2RES, i2RESP);
                        d3RESPct = calcPct(i3RES, i3RESP);
                        dNPPct = calcPct(iNP, iNPP);
                        dTELPct = calcPct(iTEL, iTELP);
                        dWCPct = calcPct(iWC, iWCP);

                        sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                        sqlCmd2.Parameters.AddWithValue("@SubDistrict", sSubDistrict);
                        sqlCmd2.Parameters.AddWithValue("@TotPctChange", dTotPct);
                        sqlCmd2.Parameters.AddWithValue("@1CMPctChange", d1CMPct);
                        sqlCmd2.Parameters.AddWithValue("@2CMPctChange", d2CMPct);
                        sqlCmd2.Parameters.AddWithValue("@3CMPctChange", d3CMPct);
                        sqlCmd2.Parameters.AddWithValue("@1RESPctChange", d1RESPct);
                        sqlCmd2.Parameters.AddWithValue("@2RESPctChange", d2RESPct);
                        sqlCmd2.Parameters.AddWithValue("@3RESPctChange", d3RESPct);
                        sqlCmd2.Parameters.AddWithValue("@NPPctChange", dNPPct);
                        sqlCmd2.Parameters.AddWithValue("@TELPctChange", dTELPct);
                        sqlCmd2.Parameters.AddWithValue("@WCPctChange", dWCPct);

                        //sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                        //sqlCmd2.Parameters.AddWithValue("@1CMPctChange", 0);
                        //sqlCmd2.Parameters.AddWithValue("@2CMPctChange", 0);
                        //sqlCmd2.Parameters.AddWithValue("@3CMPctChange", 0);
                        //sqlCmd2.Parameters.AddWithValue("@1RESPctChange", .1);
                        //sqlCmd2.Parameters.AddWithValue("@2RESPctChange", .2);
                        //sqlCmd2.Parameters.AddWithValue("@3RESPctChange", .3);
                        //sqlCmd2.Parameters.AddWithValue("@NPPctChange", 0);
                        //sqlCmd2.Parameters.AddWithValue("@TELPctChange", 0);
                        //sqlCmd2.Parameters.AddWithValue("@WCPctChange", 0);

                        iRet = sqlCmd2.ExecuteNonQuery();

                        //if (iRet != 1)
                        //    throw new SystemException("sp_sp_ZipCodeDist2Month_Finalize returned " + iRet.ToString() + " for " + sDistrict);

                    } while (sqlReader.Read());     // districts

                    sqlReader.Close();

                }   // has rows


                // Totals

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "dbo.sp_ZipCodeDist2Month_Totals";
                sqlCmd1.Connection = sqlConn1;
                //sqlConn1.Open();

                // Second SQL conn for updating table
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "dbo.sp_ZipCodeDist2Month_TotRow";
                sqlCmd2.Connection = sqlConn2;
                //sqlConn2.Open();

                // process all rows
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {
                           
                        sDistrict = sqlReader.GetSqlString(0).ToString();
                        sSubDistrict = sDistrict + "Total";

                        // get current & prev month totals
                        iTotal = (int)sqlReader.GetSqlInt32(1);
                        iTotalP = (int)sqlReader.GetSqlInt32(2);

                        i1CM = (int)sqlReader.GetSqlInt32(3);
                        i1CMP = (int)sqlReader.GetSqlInt32(4);
                        i2CM = (int)sqlReader.GetSqlInt32(5);
                        i2CMP = (int)sqlReader.GetSqlInt32(6);
                        i3CM = (int)sqlReader.GetSqlInt32(7);
                        i3CMP = (int)sqlReader.GetSqlInt32(8);
                        i1RES = (int)sqlReader.GetSqlInt32(9);
                        i1RESP = (int)sqlReader.GetSqlInt32(10);
                        i2RES = (int)sqlReader.GetSqlInt32(11);
                        i2RESP = (int)sqlReader.GetSqlInt32(12);
                        i3RES = (int)sqlReader.GetSqlInt32(13);
                        i3RESP = (int)sqlReader.GetSqlInt32(14);
                        iNP = (int)sqlReader.GetSqlInt32(15);
                        iNPP = (int)sqlReader.GetSqlInt32(16);
                        iTEL = (int)sqlReader.GetSqlInt32(17);
                        iTELP = (int)sqlReader.GetSqlInt32(18);
                        iWC = (int)sqlReader.GetSqlInt32(19);
                        iWCP = (int)sqlReader.GetSqlInt32(20);


                        // Update table - write total row
                        sqlCmd2.Parameters.Clear();

                        dTotPct = calcPct(iTotal, iTotalP);
                        d1CMPct = calcPct(i1CM, i1CMP);
                        d2CMPct = calcPct(i2CM, i2CMP);
                        d3CMPct = calcPct(i3CM, i3CMP);
                        d1RESPct = calcPct(i1RES, i1RESP);
                        d2RESPct = calcPct(i2RES, i2RESP);
                        d3RESPct = calcPct(i3RES, i3RESP);
                        dNPPct = calcPct(iNP, iNPP);
                        dTELPct = calcPct(iTEL, iTELP);
                        dWCPct = calcPct(iWC, iWCP);

                        sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                        sqlCmd2.Parameters.AddWithValue("@SubDistrict", sSubDistrict);
                        sqlCmd2.Parameters.AddWithValue("@Total",iTotal );
                        sqlCmd2.Parameters.AddWithValue("@TotalPrev",iTotalP );
                        sqlCmd2.Parameters.AddWithValue("@TotalPctChange", dTotPct);
                        sqlCmd2.Parameters.AddWithValue("@1CMCur",i1CM);
                        sqlCmd2.Parameters.AddWithValue("@1CMPrev",i1CMP );
                        sqlCmd2.Parameters.AddWithValue("@1CMPctChange", d1CMPct);
                        sqlCmd2.Parameters.AddWithValue("@2CMCur",i2CM);
                        sqlCmd2.Parameters.AddWithValue("@2CMPrev",i2CMP);
                        sqlCmd2.Parameters.AddWithValue("@2CMPctChange", d2CMPct);
                        sqlCmd2.Parameters.AddWithValue("@3CMCur",i3CM);
                        sqlCmd2.Parameters.AddWithValue("@3CMPrev",i3CMP);                      
                        sqlCmd2.Parameters.AddWithValue("@3CMPctChange", d3CMPct);
                        sqlCmd2.Parameters.AddWithValue("@1RESCur", i1RES);
                        sqlCmd2.Parameters.AddWithValue("@1RESPrev", i1RESP);
                        sqlCmd2.Parameters.AddWithValue("@1RESPctChange", d1RESPct);
                        sqlCmd2.Parameters.AddWithValue("@2RESCur", i2RES);
                        sqlCmd2.Parameters.AddWithValue("@2RESPrev", i2RESP);
                        sqlCmd2.Parameters.AddWithValue("@2RESPctChange", d2RESPct);
                        sqlCmd2.Parameters.AddWithValue("@3RESCur", i3RES);
                        sqlCmd2.Parameters.AddWithValue("@3RESPrev", i3RESP);
                        sqlCmd2.Parameters.AddWithValue("@3RESPctChange", d3RESPct);
                        sqlCmd2.Parameters.AddWithValue("@NPCur", iNP);
                        sqlCmd2.Parameters.AddWithValue("@NPPrev", iNPP);
                        sqlCmd2.Parameters.AddWithValue("@NPPctChange", dNPPct);
                        sqlCmd2.Parameters.AddWithValue("@TELCur", iTEL);
                        sqlCmd2.Parameters.AddWithValue("@TELPrev", iTELP);
                        sqlCmd2.Parameters.AddWithValue("@TELPctChange", dTELPct);
                        sqlCmd2.Parameters.AddWithValue("@WCCur", iWC);
                        sqlCmd2.Parameters.AddWithValue("@WCPrev", iWCP);
                        sqlCmd2.Parameters.AddWithValue("@WCPctChange", dWCPct);  

                        iRet = sqlCmd2.ExecuteNonQuery();

                        //if (iRet != 1)
                        //    throw new SystemException("sp_sp_ZipCodeDist2Month_Finalize returned " + iRet.ToString() + " for " + sDistrict);

                    } while (sqlReader.Read());     // districts

                    sqlReader.Close();

                }   // has rows
            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End TwoMonth ----");

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn1.Close();


            }


        }


        static void BuildSIBZipCodeSkillSet()
        {

            // Populate table ZipCodeSkillSetWork_O with all currently pending items

            int iRet = 0;
            int iKeynumber = 0;
            string sZipCode = "";
            string sOptions = "";
            string sSkillSet = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin BuildSIBZipCodeSkillSet ++++");

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_SQLMainUTILConnStr);
                sqlCmd1 = new SqlCommand();
                sqlConn2 = new SqlConnection(cfg_SQLMainUTILConnStr);
                sqlCmd2 = new SqlCommand();
                sqlConn1.Open();

                // Second SQL conn for updating table
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                sqlCmd2.CommandType = CommandType.Text;

                sqlCmd2.CommandText = "DELETE FROM ZipCodeSkillSetWork_O";

                sqlCmd2.Connection = sqlConn2;
                sqlCmd2.ExecuteNonQuery();

                sqlCmd1.CommandType = CommandType.StoredProcedure;

                sqlCmd1.CommandText = "dbo.sp_BuildZipCodeSkillSet_GetPending";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();

                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "dbo.sp_ZipCodeSkillSetWork_Insert_O";

                // Get all completed cases
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through cases
                    do
                    {
                        iKeynumber = (int)sqlReader.GetSqlInt32(0);

                        //Location zip
                        if (sqlReader.IsDBNull(1))
                        {
                            sZipCode = "";
                        }
                        else
                        {
                            sZipCode = sqlReader.GetSqlString(1).ToString();

                            // if zip is < 5 ignore it
                            if (sZipCode.Length < 5)
                                sZipCode = "";

                            // trim to 5 chars max
                            else if (sZipCode.Length > 5)
                                sZipCode = sZipCode.Remove(5);

                            if (sZipCode.Length == 5 && sZipCode.Substring(0, 1) != "3")
                                sZipCode = "";

                            if (!IsNumeric(sZipCode))
                                sZipCode = "";
                        }

                        //Options
                        if (sqlReader.IsDBNull(2))
                        {
                            sOptions = "";
                        }
                        else
                        {
                            sOptions = sqlReader.GetSqlString(2).ToString();
                            sOptions = sOptions.ToUpper();
                        }

                        // If zipcode is valid
                        if (sZipCode != "" && sOptions != "")
                        {

                            // set SkillSet based on form in Options
                            if (sOptions.IndexOf("ORM 1010") > 0)
                            {
                                sSkillSet = "1CM";
                            }
                            else if (sOptions.IndexOf("ORM 1016") > 0)
                            {
                                sSkillSet = "1CM";
                            }
                            else if (sOptions.IndexOf("ORM 1028") > 0)
                            {
                                sSkillSet = "1CM";
                            }
                            else if (sOptions.IndexOf("ORM 1080L") > 0)
                            {
                                sSkillSet = "1RES";
                            }
                            else if (sOptions.IndexOf("ORM 1081") > 0)
                            {
                                sSkillSet = "1RES";
                            }
                            else if (sOptions.IndexOf("ORM 1061") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1091") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1007") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1008") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1008P") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1015") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1024") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1039") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1054") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1057") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1091L") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1095") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1097") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1098") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 3005") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 3007") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 3008") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6010") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6020") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6030") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6040") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1052") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 1080AB") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 4000") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 4010") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 1003") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1004") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1005") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1012") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1014") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1099") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1037") > 0)
                            {
                                sSkillSet = "3RES";
                            }
                            else if (sOptions.IndexOf("ORM 1080A") > 0)
                            {
                                sSkillSet = "3RES";
                            }
                            else if (sOptions.IndexOf("ORM 7131") > 0)
                            {
                                sSkillSet = "NP";
                            }
                            else if (sOptions.IndexOf("ORM 1011") > 0)
                            {
                                sSkillSet = "TEL";
                            }
                            else if (sOptions.IndexOf("ORM 1045") > 0)
                            {
                                sSkillSet = "TEL";
                            }
                            else if (sOptions.IndexOf("ORM 1056") > 0)
                            {
                                if (sOptions.IndexOf("ORM 1056C") > 0)
                                {
                                    sSkillSet = "TEL";
                                }
                                else if (sOptions.IndexOf("ORM 1056T") > 0)
                                {
                                    sSkillSet = "TEL";
                                }
                                else if (sOptions.IndexOf("ORM 1056R") > 0)
                                {
                                    sSkillSet = "TEL";
                                }
                                else
                                {
                                    sSkillSet = "2CM";
                                }
                            }
                            else if (sOptions.IndexOf("ORM 1023") > 0)
                            {
                                sSkillSet = "WC";
                            }

                            // log no skillset
                            if (sSkillSet == "")
                            {
                                oLU.WritetoLog("Keynumber: " + iKeynumber.ToString() + " - undetermined skill set");
                            }
                            else
                            {
                                //if (sSkillSet == "TEL")
                                //{
                                // Insert into table
                                sqlCmd2.Parameters.Clear();

                                sqlCmd2.Parameters.AddWithValue("@keynumber", iKeynumber);
                                sqlCmd2.Parameters.AddWithValue("@ZipCode", sZipCode);
                                sqlCmd2.Parameters.AddWithValue("@SkillSetCode", sSkillSet);

                                iRet = sqlCmd2.ExecuteNonQuery();

                                if (iRet == 0)
                                    throw new SystemException("dbo.sp_ZipCodeSkillSetWork_Insert returned 0 for " + iKeynumber.ToString());
                                //}
                            }
                        }
                    } while (sqlReader.Read());     // cases

                    sqlReader.Close();

                }   // has rows

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End BuildSIBZipCodeSkillSet ----");

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn1.Close();

            }

        }
        
        static decimal calcPct(int iCur, int iPrev)
        {
            
            decimal dRetVal = 0;

            if (iCur == 0 && iPrev == 0)
                return dRetVal;

            decimal dCur = Convert.ToDecimal(iCur);
            decimal dPrev = Convert.ToDecimal(iPrev);

            if (dCur == 0)
            {
                return 0 - dPrev;
            }
            if (dPrev == 0)
            {
                return dCur;
            }

            if (dCur - dPrev > 0)
            {
                dRetVal = 1 - (dPrev / dCur);  
            }
            else
            {
                dRetVal = 0 - (1-(dCur / dPrev));            
            }

            return dRetVal;
        
        }

        // IsNumeric Function
        static bool IsNumeric(object Expression)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum;

            // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
            double retNum;

            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }		
 

    static void createHeader(Excel._Worksheet oWorkSheet)
    {

        DateTime dStatDate = DateTime.Now;
        String sStatTime = DateTime.Now.ToString();

        Excel.Range oRange;
        
        oWorkSheet.get_Range("A1", "N1").Merge(false);
        oRange = oWorkSheet.get_Range("A1", "N1");
            oRange.FormulaR1C1 = "Threshold Report as of " + sStatTime;

        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        oRange.Font.Size = 16;
        oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

        oWorkSheet.Cells[3, 1] = "District";
        oRange = oWorkSheet.get_Range("A3", "A3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 2] = "City";
        oRange = oWorkSheet.get_Range("B3", "B3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 3] = "County";
        oRange = oWorkSheet.get_Range("C3", "C3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 4] = "State";
        oRange = oWorkSheet.get_Range("D3", "D3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 5] = "Total";
        oRange = oWorkSheet.get_Range("E3", "E3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 6] = "Com 1";
        oRange = oWorkSheet.get_Range("F3", "F3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 7] = "Com 2";
        oRange = oWorkSheet.get_Range("G3", "G3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 8] = "Com 3";
        oRange = oWorkSheet.get_Range("H3", "H3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 9] = "Res 1";
        oRange = oWorkSheet.get_Range("I3", "I3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 10] = "Res 2";
        oRange = oWorkSheet.get_Range("J3", "J3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 11] = "Res 3";
        oRange = oWorkSheet.get_Range("K3", "K3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 12] = "NP";
        oRange = oWorkSheet.get_Range("L3", "L3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 13] = "Tel";
        oRange = oWorkSheet.get_Range("M3", "M3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        oWorkSheet.Cells[3, 14] = "WC";
        oRange = oWorkSheet.get_Range("N3", "N3");
        oRange.ColumnWidth = 15;
        oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
        oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        }




        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data,
        string cell1, string cell2, string format, string sHorizAlign)
    {
        oWorkSheet.Cells[row, col] = data;
        oRange = oWorkSheet.get_Range(cell1, cell2);
        oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        oRange.NumberFormat = format;
        if (sHorizAlign == "C")
        {
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        }
        else
        {
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        }
    }

    static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
        string cell1, string cell2)
    {
        oWorkSheet.Cells[row, col] = data;
        oRange = oWorkSheet.get_Range(cell1, cell2);
        oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
    }



    static void releaseObject(object obj)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }
        catch (Exception ex)
        {
            obj = null;
        }
        finally
        {
            GC.Collect();
        }
    }

    static void sendEmail(string sMsgBody)
    {

        string sRet;
        SendMail.SendMail oMail;
        oMail = new SendMail.SendMail();


        oMail.MailFrom = "jeff@sibfla.com";
        oMail.MailTo = cfg_emailnotify;
        oMail.MsgSubject = "Customer Rejects";
        oMail.MsgBody = sMsgBody;
        oMail.SMTPServer = cfg_smtpserver;
        oMail.SendHTML = true;
        sRet = oMail.Send();
        oMail = null;

        if (sRet.Length > 0)
        {
            oLU.WritetoLog("Error in module SendMail: " + sRet);
            mbErr = true;

        }

    }

    static void sendErrEmail(string sMsgBody)
    {

        string sRet;
        //'SendMail oMail;
        SendMail.SendMail oMail;
        oMail = new SendMail.SendMail();

        oMail.MailFrom = "jeff@sibfla.com";
        oMail.MailTo = "jeff@sibfla.com";
        oMail.MsgSubject = "** Errors logged by CustRejects Automation **";
        oMail.MsgBody = sMsgBody;
        oMail.SMTPServer = cfg_smtpserver;
        oMail.SendHTML = true;
        sRet = oMail.Send();
        oMail = null;

    }

    static void sendExcelFile(string sSubject, string sAtt)
    {

        string sRet;
        string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

        try
        {

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = cfg_deliverto;
            //oMail.MailBCC = "jeff@sibfla.com";
            oMail.MsgSubject = sSubject;
            oMail.MsgBody = sBodyText;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            oMail.Attachment = sAtt;
            sRet = oMail.Send();
            oMail = null;
        }
        catch (Exception ex)
        {

            //record exception  
            throw ex;

        }

    }

    }



}
