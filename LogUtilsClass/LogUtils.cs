using System;
using System.IO;

namespace LogUtils
{
	/// <summary>
	/// Summary description for LogUtils.
	/// </summary>
	public class LogUtils
	{

		// StreamWriter var
		private StreamWriter cSR;

		// Property Vars
		private string csLogFileName;
        private bool mbSessionHeader;

		// Property declarations
		// Log file name:
		public string logFileName
		{
			get 
			{
				return csLogFileName; 
			}
			set 
			{
				csLogFileName = value; 
			}
		}

        // Session Header/Footer :
        public bool sessionHeader
        {
            get
            {
                return mbSessionHeader;
            }
            set
            {
                mbSessionHeader = value;
            }
        }

		// Class constructor
		public LogUtils()
		{
            // default session header to trus
            mbSessionHeader = true;
		}

		/// <summary>
		/// OpenLog() 
		/// Opens log file
		/// 
		///		Property (logFileName) must be populated
		/// </summary>
		/// <returns>
		///		Success - empty string
		///		Failure - error text
		///</returns>

		public void OpenLog() 
		{

			try
			{
				if (csLogFileName == "")
				{
					return;
				}

				if (File.Exists(csLogFileName))
				{
					cSR = File.AppendText(csLogFileName);
					cSR.WriteLine();
				}
				else
				{
					cSR = File.CreateText(csLogFileName);
				}

                if (mbSessionHeader)
                {
                    cSR.WriteLine("------------------------------");
                    cSR.WriteLine("Session opened: " + DateTime.Now.ToString());
                    cSR.WriteLine("");
                }

			}
			catch (Exception ex)
			{
				throw ex;
			}
			return;
		}

		public void WritetoLog(string logText)
		{
			cSR.WriteLine(logText);
		}


		public void closeLog()
		{
            if (mbSessionHeader)
            {
                cSR.WriteLine();
                cSR.WriteLine("Session closed: " + DateTime.Now.ToString());
            }
			cSR.Close();
		}


	

	}
}


