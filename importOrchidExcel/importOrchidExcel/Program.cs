﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;


namespace importOrchidExcel
{


    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyNumber { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            //public string BusinessOperations { get; set; }
            public string LocationFullAddress { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string CoverageB { get; set; }
            public string CoverageC { get; set; }
            public string CoverageD { get; set; }
            public string CoverageE { get; set; }
            public string CoverageF { get; set; }
            public string ISOClass { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }

            public string Construction { get; set; }
            public string YearBuilt { get; set; }
            public string DwellingType { get; set; }
            public string NumberOfFloors { get; set; }
            public string Occupancy { get; set; }
            public string SquareFootage { get; set; }
            public string DistanceToCoast { get; set; }
            public string RoofYear { get; set; }
            public string RoofMaterials { get; set; }
            public string RoofGeometry { get; set; }
            public string Usage { get; set; }
            public string WindMitigation { get; set; }
            public string PCClass { get; set; }
            public string RoofCladding { get; set; }
            public string RoofCondition { get; set; }
            public string BuildingCladding { get; set; }
            public string CompanyPolicyForm { get; set; }
            public string CentralFireAlarm { get; set; }
            public string CentralBurglarAlarm { get; set; }
            public string CentralAlarm { get; set; }
            public string PolicyStatus { get; set; }
            public string PoolFenced { get; set; }
            public string Heating { get; set; }
            public string HeatingFullPartial { get; set; }
            public string HeatingUpdate { get; set; }
            public string HeatingYear { get; set; }
            public string PoolAboveGround { get; set; }
            public string Farming { get; set; }
            public string NumberOfFamilies { get; set; }
            public string Pets { get; set; }
            public string Plumbing { get; set; }
            public string PlumbingFullPartial { get; set; }
            public string PlumbingUpdate { get; set; }
            public string PlumbingYear { get; set; }
            public string PoolSlide { get; set; }
            public string PoolDivingBoard { get; set; }
            public string PoolScreenEncl { get; set; }
            public string Pool { get; set; }
            public string Updates { get; set; }
            public string Wiring { get; set; }
            public string WiringFullPartial { get; set; }
            public string WiringUpdate { get; set; }
            public string WiringYear { get; set; }

            public string AgentEmail { get; set; }
            public string AltContactName { get; set; }
            public string AltContactPhone { get; set; }
            public string AltContactMPhone { get; set; }
            public string AltContactEmail { get; set; }
        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        //static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];


        static LogUtils.LogUtils oLU;
        static bool bErr = false;

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {

            string sExcelFileName = "";

            bool bImportSuccess = false;

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                if (args.Length == 0)
                {
                    throw new ApplicationException("No arguments passed");                
                }

                // get excel file name from command line
                sExcelFileName = args[0];

                // does file exist?
                FileInfo fi = new FileInfo(cfg_sourcedir + sExcelFileName);
                if (!fi.Exists)
                {
                    // send email
                    throw new ApplicationException("File does not exist: " + sExcelFileName);
                }

            }

            catch (Exception ex)
            {
                oLU.closeLog();
                sendErrEmail("Error initializing imptOrchidExcel\r\n\r\n" + ex.Message);
                return;
            }
            
 
            try
            {

                oLU.WritetoLog("Processing: " + sExcelFileName);
                sbEmail.Append(System.Environment.NewLine + "Processing: " + sExcelFileName + System.Environment.NewLine);

                var excel = new ExcelQueryFactory(cfg_sourcedir + sExcelFileName);
                excel.AddMapping<ImportRecord>(x => x.InsuredName, "Policy # - Insured");
                excel.AddMapping<ImportRecord>(x => x.ContactPhoneWork, "Policy # - Customer Phone Business");
                excel.AddMapping<ImportRecord>(x => x.ContactPhoneHome, "Policy # - Customer Phone Residence");
                excel.AddMapping<ImportRecord>(x => x.PolicyNumber, "Policy #");
                excel.AddMapping<ImportRecord>(x => x.EffectiveDate, "Policy Effective Date");
                excel.AddMapping<ImportRecord>(x => x.CoverageA, "Coverage A");
                excel.AddMapping<ImportRecord>(x => x.LocationAddress1, "Property Street");
                excel.AddMapping<ImportRecord>(x => x.LocationCity, "Property City");
                excel.AddMapping<ImportRecord>(x => x.LocationState, "Property State");
                excel.AddMapping<ImportRecord>(x => x.LocationZip, "Property Zip Code");
                excel.AddMapping<ImportRecord>(x => x.InsuranceCompany, "Policy Company");
                excel.AddMapping<ImportRecord>(x => x.Construction, "Construction");
                excel.AddMapping<ImportRecord>(x => x.YearBuilt, "Year Built");
                excel.AddMapping<ImportRecord>(x => x.DwellingType, "Dwelling Type");
                excel.AddMapping<ImportRecord>(x => x.NumberOfFloors, "Number of Floors");
                excel.AddMapping<ImportRecord>(x => x.Occupancy, "Occupancy");
                excel.AddMapping<ImportRecord>(x => x.SquareFootage, "Square Footage");
                excel.AddMapping<ImportRecord>(x => x.InspectionType, "Inspection Type");

                // Newer format
                //excel.AddMapping<ImportRecord>(x => x.DistanceToCoast, "Distance To Coast  ");

                // Old format
                excel.AddMapping<ImportRecord>(x => x.DistanceToCoast, "Distance To Coast");

                excel.AddMapping<ImportRecord>(x => x.RoofYear, "Roof Year");
                excel.AddMapping<ImportRecord>(x => x.RoofMaterials, "Roof Materials");
                excel.AddMapping<ImportRecord>(x => x.RoofGeometry, "Roof Geometry");
                excel.AddMapping<ImportRecord>(x => x.Usage, "Usage");
                excel.AddMapping<ImportRecord>(x => x.WindMitigation, "Wind Mitigation");
                excel.AddMapping<ImportRecord>(x => x.PCClass, "PC Class");
                excel.AddMapping<ImportRecord>(x => x.RoofCladding, "RMS - Roof Cladding");
                excel.AddMapping<ImportRecord>(x => x.CompanyPolicyForm, "Company Policy Form");
                excel.AddMapping<ImportRecord>(x => x.RoofCondition, "Roof Condition");
                excel.AddMapping<ImportRecord>(x => x.BuildingCladding, "LN - Building Cladding");
                excel.AddMapping<ImportRecord>(x => x.CentralFireAlarm, "Central Fire Alarm");
                excel.AddMapping<ImportRecord>(x => x.CentralBurglarAlarm, "Central Burglar");
                excel.AddMapping<ImportRecord>(x => x.CentralAlarm, "Central Alarm");
                excel.AddMapping<ImportRecord>(x => x.PolicyStatus, "Policy Status");
                excel.AddMapping<ImportRecord>(x => x.PoolFenced, "Fenced Pool");
                excel.AddMapping<ImportRecord>(x => x.Heating, "Heating");
                excel.AddMapping<ImportRecord>(x => x.HeatingFullPartial, "Heating Full/Partial");
                excel.AddMapping<ImportRecord>(x => x.HeatingUpdate, "Heating Update");
                excel.AddMapping<ImportRecord>(x => x.HeatingYear, "Heating Year");
                excel.AddMapping<ImportRecord>(x => x.PoolAboveGround, "Is Pool Above Ground?");
                excel.AddMapping<ImportRecord>(x => x.NumberOfFamilies, "Number of Families");
                excel.AddMapping<ImportRecord>(x => x.Pets, "Pets");
                excel.AddMapping<ImportRecord>(x => x.Plumbing, "Plumbing");
                excel.AddMapping<ImportRecord>(x => x.PlumbingFullPartial, "Plumbing Full/Partial");
                excel.AddMapping<ImportRecord>(x => x.PlumbingUpdate, "Plumbing Updates");
                excel.AddMapping<ImportRecord>(x => x.PlumbingYear, "Plumbing Year");
                excel.AddMapping<ImportRecord>(x => x.PoolSlide, "Pool Slide");
                excel.AddMapping<ImportRecord>(x => x.PoolDivingBoard, "Pool Diving Board");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentPhone, "Producer Primary Phone");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentName, "Policy # - Agency Name");
                excel.AddMapping<ImportRecord>(x => x.Producer, "Policy # - Customer - License # - Producer Name");
                excel.AddMapping<ImportRecord>(x => x.PoolScreenEncl, "Screen Enclosure");
                excel.AddMapping<ImportRecord>(x => x.Pool, "Swimming Pool");
                excel.AddMapping<ImportRecord>(x => x.Updates, "Updates");
                excel.AddMapping<ImportRecord>(x => x.Wiring, "Wiring");
                excel.AddMapping<ImportRecord>(x => x.WiringFullPartial, "Wiring Full/Partial");
                excel.AddMapping<ImportRecord>(x => x.WiringUpdate, "Wiring Update");
                excel.AddMapping<ImportRecord>(x => x.AgentEmail, "Inspector Email Address");
                excel.AddMapping<ImportRecord>(x => x.AltContactName, "Contact Name for Inspection Purpose");
                excel.AddMapping<ImportRecord>(x => x.AltContactPhone, "Inspection Contact Primary Phone");
                excel.AddMapping<ImportRecord>(x => x.AltContactMPhone, "Inspection Contact Mobile Phone");
                excel.AddMapping<ImportRecord>(x => x.AltContactEmail, "Inspection Contact Email Address");


                var rows = (from row in excel.Worksheet<ImportRecord>(0) select row).ToList();

                foreach (var row in rows)
                {

                    // skip row if policy number is empty
                    if (row.PolicyNumber.Length > 0)
                    {

                        ImportRequests oAPI = new ImportRequests();
                        oAPI.CustomerUserName = "APIProd";
                        oAPI.CustomerPassword = "Sutton2012";
                        oAPI.CustomerAccount = "7277";

                        // standard values provided
                        oAPI.InsuredName = row.InsuredName;
                        oAPI.ContactPhoneWork = row.ContactPhoneWork;
                        oAPI.ContactPhoneCell = "";
                        oAPI.ContactPhoneHome = row.ContactPhoneHome;
                        oAPI.PolicyNumber = row.PolicyNumber;
                        oAPI.EffectiveDate = row.EffectiveDate;
                        oAPI.CoverageA = row.CoverageA;
                        oAPI.LocationAddress1 = row.LocationAddress1;
                        oAPI.LocationAddress2 = "";
                        oAPI.LocationCity = row.LocationCity;
                        oAPI.LocationState = row.LocationState;
                        oAPI.LocationZip = row.LocationZip;
                        oAPI.MailAddress1 = row.LocationAddress1;
                        oAPI.MailAddress2 = "";
                        oAPI.MailCity = row.LocationCity;
                        oAPI.MailState = row.LocationState;
                        oAPI.MailZip = row.LocationZip;
                        oAPI.InsuranceCompany = row.InsuranceCompany;
                        oAPI.AgentEmail = row.AgentEmail;
                        oAPI.Producer = row.Producer;


                        // Agency / Agent
                        oAPI.AgencyAgentName = row.AgencyAgentName;
                        oAPI.AgentCode = "";
                        oAPI.AgencyAgentPhone = row.AgencyAgentPhone;
                        oAPI.AgentFax = "";
                        oAPI.AgencyAgentContact = "";
                        oAPI.AgentAddress1 = "5656 Central Ave";
                        oAPI.AgentAddress2 = "";
                        oAPI.AgentCity = "St. Petersburg";
                        oAPI.AgentState = "FL";
                        oAPI.AgentZip = "33707";

                        string sAltContact = "Location contact: ";
                        if (row.AltContactName != null && row.AltContactName.Length > 0)
                            sAltContact += row.AltContactName;
                        if (row.AltContactPhone != null && row.AltContactPhone.Length > 0)
                            sAltContact += " Phone: " + row.AltContactPhone;
                        if (row.AltContactMPhone != null && row.AltContactMPhone.Length > 0)
                            sAltContact += " Cell: " + row.AltContactMPhone;
                        if (row.AltContactEmail != null && row.AltContactEmail.Length > 0)
                            sAltContact += " Email: " + row.AltContactEmail;

                        if (sAltContact.Length > 20)
                            oAPI.AltLocationContact = sAltContact;
                        else
                            oAPI.AltLocationContact = "";

                        // standard values not provided
                        oAPI.EmailConfirmation = "";
                        oAPI.Underwriter = "";
                        oAPI.UnderwriterFirstName = "";
                        oAPI.UnderwriterLastName = "";
                        oAPI.RushHandling = "N";
                        oAPI.ContactName = "";
                        oAPI.LocationContactPhone = "";
                        oAPI.LocationContactName = "";
                        oAPI.LocationContactPhone = "";

                        // Convert CoverageA to numeric
                        double dCovA = Convert.ToDouble(row.CoverageA);

                        // Convert year built to numeric
                        int iYearBuilt = Convert.ToInt32(row.YearBuilt);
                        int iYearsOld = 0;
                        if (iYearBuilt > 0)
                        {
                            iYearsOld = DateTime.Now.Year - iYearBuilt;
                        }

                        // inspection type
                        string sInspType = "";
                        string sTypeIn = "";

                        if (row.InspectionType == null)
                        {
                            row.InspectionType = "";
                        }
                        else
                        {
                            sTypeIn = row.InspectionType.ToUpper();

                            if (sTypeIn == "BASIC W/RCE")
                            {
                                //sInspType = "7277-R-E-I"; 1/20/22
                                sInspType = "7277-R-E-E2V-I";  
                            }
                            else if (sTypeIn == "BASIC INSPECTION" || sTypeIn == "BASIC")
                            {
                                //sInspType = "7277-R-E-E2V-I";
                                sInspType = "7277-R-E-I";
                            }
                            else if (sTypeIn == "BASIC W/RCE & INTER PHOTO" || sTypeIn == "BASIC W/RCE & INT PHOTOS" || sTypeIn == "BASIC W/INTERIOR & RCE" || sTypeIn == "BASIC W/RCE & INTERIOR PHOTOS")                            
                            {
                                sInspType = "7277-R-IE-E2V-I";
                            }
                            else if (sTypeIn.Contains("ALL INCLUSIVE"))
                            {
                                sInspType = "7277-R-IE-E2V-U-I";
                            }
                            else if (sTypeIn.Contains("BUILDERS RISK"))
                            {
                                sInspType = "7277-BR-I";
                            }
                            else if (sTypeIn == "EXCESS FLOOD")
                            {
                                sInspType = "7277-Flood-I";
                            }
                        }

                        // If inspection type is blank - throw error
                        if (sInspType.Length == 0)
                        {
                            throw new Exception("Invalid Inspection Type - File name: " + sExcelFileName + " - Policy#: " + row.PolicyNumber);
                        }

                        oAPI.InspectionType = sInspType;


                        // Old method for determining inspection type
                        // Change on 10/6/21 JK
                        //inspection type
                        //string sInspType = "";
                        //bool bUpdate = false;

                        //if (row.InspectionType == null)
                        //    row.InspectionType = "";

                        //handle empty CompanyPolicyForm
                        //if (row.CompanyPolicyForm == null)
                        //    row.CompanyPolicyForm = "";

                        //Builders risk
                        //if (row.CompanyPolicyForm.ToUpper().Contains("HO3-BR"))
                        //{
                        //    sInspType = "7277-BR-I"; // Builder's Risk / Builders Risk Orchid Import
                        //}
                        //else if (row.PolicyStatus.ToUpper().Contains("RENEWAL") || row.CompanyPolicyForm.ToUpper().Contains("HO4"))
                        //{
                        //    sInspType = "7277-R-E-I"; //Basic Inspection / Residential Exterior Orchid Import
                        //}
                        //else
                        //{

                        //    CoverageA provided?
                        //    if (dCovA > 0)
                        //    {

                        //        if it's over $650000 
                        //        if (dCovA > 650000)
                        //        {

                        //            Year built > 30
                        //            if (iYearsOld > 30)
                        //            {
                        //                sInspType = "7277-R-IE-E2V-U-I"; // All Inclusive Inspection / Residential Interior & Exterior Orchid  w/update & e2value Import
                        //                bUpdate = true;
                        //            }
                        //            else
                        //            {
                        //                sInspType = "7277-R-IE-E2V-I"; // Basic w/RCE & Int Photos / Residential Exterior Orchid w/e2value Import
                        //            }
                        //        }
                        //        else
                        //        {
                        //            If it's > 0 but < $650000

                        //             Year built > 30
                        //            if (iYearsOld > 30)
                        //            {

                        //                sInspType = "7277-R-E-E2V-U-I"; // Res Ext w/Update / Residential Exterior Orchid w/update & e2value Import
                        //                bUpdate = true;
                        //            }
                        //            else
                        //            {
                        //                sInspType = "7277-R-E-E2V-I"; // Basic w/RCE / Residential Exterior Orchid w/e2value Import
                        //            }
                        //        }
                        //    }

                        //}

                        //Flood
                        //if (row.InspectionType.ToUpper() == "EXCESS FLOOD")
                        //    sInspType = "7277-Flood-I";

                        //If inspection type is blank - throw error
                        //if (sInspType.Length == 0)
                        //{
                        //    throw new Exception("No Inspection Type - File name: " + sExcelFileName + " - Policy#: " + row.PolicyNumber);
                        //}

                        //oAPI.InspectionType = sInspType;

                        // comments
                        oAPI.Comments = "";

                        // Custom/Generic field values
                        oAPI.Occupancy = row.Occupancy ?? "Not Provided";
                        oAPI.Usage = row.Usage ?? "Not Provided";
                        oAPI.DwellingType = row.DwellingType ?? "Not Provided";
                        oAPI.Stories = row.NumberOfFloors ?? "Not Provided";
                        oAPI.YearBuilt = row.YearBuilt ?? "Not Provided";
                        oAPI.ExteriorWallType = row.Construction ?? "Not Provided";
                        oAPI.ExteriorCoating = row.BuildingCladding ?? "Not Provided";
                        oAPI.RoofGeometry = row.RoofGeometry ?? "Not Provided";
                        oAPI.RoofType = row.RoofMaterials ?? "Not Provided";
                        oAPI.RoofYear = row.RoofYear ?? "Not Provided";
                        oAPI.RoofCondition = row.RoofCondition ?? "Not Provided";
                        oAPI.PoolPresent = row.Pool ?? "Not Provided";
                        oAPI.PoolAboveGround = row.PoolAboveGround ?? "Not Provided";
                        oAPI.PoolScreenEnclosurePresent = row.PoolScreenEncl ?? "Not Provided";
                        oAPI.PoolFencePresent = row.PoolFenced ?? "Not Provided";
                        oAPI.PoolDivingBoardPresent = row.PoolDivingBoard ?? "Not Provided";
                        oAPI.PoolSlidePresent = row.PoolSlide ?? "Not Provided";
                        oAPI.WindstormProtectiveDevices = row.WindMitigation ?? "Not Provided";
                        oAPI.ProtectionClass = row.PCClass ?? "Not Provided";
                        oAPI.CentralStationFireAlarmPresent = row.CentralFireAlarm ?? "Not Provided";
                        oAPI.CentralStationBurglarAlarmPresent = row.CentralBurglarAlarm ?? "Not Provided";
                        oAPI.CentralAlarmPresent = row.CentralAlarm ?? "Not Provided";

                        oAPI.Dog = row.Pets ?? "Not Provided";
                        oAPI.DistanceCoast = row.DistanceToCoast ?? "Not Provided";

                        // Update
                        //if (bUpdate)
                        //{

                            oAPI.ElectricalUpdates = row.WiringUpdate ?? "Not Provided";
                            oAPI.ElectricalFullPartial = row.WiringFullPartial ?? "Not Provided";
                            oAPI.ElectricalYear = row.WiringYear ?? "Not Provided";
                            oAPI.HvacUpdates = row.HeatingUpdate ?? "Not Provided";
                            oAPI.HvacFullPartial = row.HeatingFullPartial ?? "Not Provided";
                            oAPI.HvacYear = row.HeatingYear ?? "Not Provided";
                            oAPI.PlumbingUpdates = row.PlumbingUpdate ?? "Not Provided";
                            oAPI.PlumbingFullPartial = row.PlumbingFullPartial ?? "Not Provided";
                            oAPI.PlumbingYear = row.PlumbingYear ?? "Not Provided";                        
                        //}

                        //**********************************
                        // UNCOMMENT FOR TEST 
                        //oAPI.CustomerUserName = "APITest";
                        //oAPI.CustomerAccount = "9998";
                        //oAPI.InspectionType = "9998RE";
                        //oAPI.EffectiveDate = "";

                        //**********************************

                        oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + row.PolicyNumber);
                        sbEmail.Append("Importing Policy# " + row.PolicyNumber + System.Environment.NewLine);

                        string sRet = oAPI.ImportOrchid();

                        oLU.WritetoLog("oAPI.Import return for for Policy# " + row.PolicyNumber + "\r\n\r\n" + sRet);

                        var importResults = sRet.FromJSON<List<ImportResult>>();

                        foreach (var importResult in importResults)
                        {

                            if (importResult.Successful)
                            {
                                oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                                sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + System.Environment.NewLine);
                            }
                            else
                            {
                                oLU.WritetoLog("**** Import failed **** "); if (importResult.Errors != null)
                                    sbEmail.Append("**** Import failed ****" + System.Environment.NewLine);

                                foreach (var error in importResult.Errors)
                                {
                                    oLU.WritetoLog("Error: " + error.ErrorText);
                                    sbEmail.Append("Error: " + error.ErrorText + System.Environment.NewLine);
                                }
                            }

                            if ((bool)importResult.Duplicate)
                            {
                                oLU.WritetoLog("Duplicate case");
                                sbEmail.Append("Duplicate case" + System.Environment.NewLine);
                            }
                        }

                    }   // Policy number not empty

                }   // foreach row in sheet

                bImportSuccess = true;
                
            }   //try

            catch (Exception ex)
            {
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail("Exception logged" + System.Environment.NewLine + ex.Message);
            }

            finally
            {

                // Copy source file to holding area and delete temp
                string sSourceName = cfg_sourcedir + sExcelFileName;
                string sDestName = "";

                //if successful - copy to sibidata\OrchidExcel\Processed
                //if failed - copy to sibidata\OrchidExcel\fail
                if (bImportSuccess)
                {
                    sDestName = cfg_compdir + sExcelFileName;
                }
                else
                {
                    sDestName = cfg_faildir + sExcelFileName;
                }

                // Copy Excel
                File.Copy(sSourceName, sDestName);
                if (File.Exists(sDestName))
                {
                    File.Delete(sSourceName);
                }
                else
                {
                    throw new ApplicationException("Copy failed for: " + sExcelFileName);
                }

                sendLogEmail(sbEmail.ToString());
                oLU.closeLog();
            }
            
        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import OrchidExcel Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import OrchidExcel Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }
    }
}
