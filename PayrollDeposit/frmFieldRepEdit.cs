﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PayrollDeposit
{
    public partial class frmFieldRepEdit : Form
    {

        private string cfg_SQL360UtilConnStr;
        private bool bNewInsp = false;
        public string FieldRepID { get; set; }
        public string FielRepNumber { get; set; }
        public string FieldRepName { get; set; }

        public frmFieldRepEdit()
        {
            InitializeComponent();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmFieldRepEdit_Load(object sender, EventArgs e)
        {

            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_SQL360UtilConnStr = colNameVal.Get("SQL360UtilConnStr");
            
            string sAcntName = "";
            string sAcntCode = "";
            string sRouteNum = "";
            string sAcntNum = "";
            bool bIsChecking = false;

            try{

                
                // init SQL connection
                SqlDataReader sqlDR;
                SqlConnection sqlConn = new SqlConnection(cfg_SQL360UtilConnStr);
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConn;
                sqlConn.Open();

                // get inspector info from FieldRepPayroll table
                sqlCommand.CommandText = "SELECT frp.BankAccountName, frp.PayrollNum, frp.RoutingNumber, frp.AccountNumber, frp.Checking ";
                sqlCommand.CommandText += "FROM Sutton360Utils.dbo.FieldRepPayroll frp ";
                sqlCommand.CommandText += "WHERE frp.FieldRepID = '" + FieldRepID + "'";
            
                sqlDR = sqlCommand.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {
                    // add row to grid

                    // loop through active inspectors
                    while (sqlDR.Read())
                    {

                        try
                        {

                            sAcntName = sqlDR.IsDBNull(0) ? "" : sqlDR.GetSqlString(0).ToString();
                            sAcntCode = sqlDR.IsDBNull(1) ? "" : sqlDR.GetSqlString(1).ToString();
                            sRouteNum = sqlDR.IsDBNull(2) ? "" : sqlDR.GetSqlString(2).ToString();
                            sAcntNum = sqlDR.IsDBNull(3) ? "" : sqlDR.GetSqlString(3).ToString();
                            bIsChecking = sqlDR.GetBoolean(4); 
                                                        
                            tbInspNum.Text = FielRepNumber;
                            tbInspName.Text = FieldRepName;
                            tbBankAcntName.Text = sAcntName;
                            tbAcntCode.Text = sAcntCode;
                            tbRouteNum.Text = sRouteNum;
                            tbAcntNum.Text = sAcntNum;

                            if (bIsChecking)
                                rbChecking.Checked = true;
                            else
                                rbSavings.Checked = true;
                                                        
                        }
                        finally
                        {

                        }	//try


                    }	// while


                }	// has rows
                else
                {

                    if (MessageBox.Show("Unable to locate inspector information.\r\n\r\nIf this is a new inspector, click OK to create the account.", "Inspector Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.OK);
                    {

                        try
                        {

                            sqlDR.Close();

                            sqlCommand.CommandText = "INSERT INTO Sutton360Utils.dbo.FieldRepPayroll ";
                            sqlCommand.CommandText += "([FieldRepID],[BankAccountName],[AccountNumber],[RoutingNumber],[PayrollNum],[Checking])";
                            sqlCommand.CommandText += " VALUES ";
                            sqlCommand.CommandText += "('" + FieldRepID + "','','','','','true')";
                            object oRetVal = sqlCommand.ExecuteNonQuery();

                            bNewInsp = true;

                            MessageBox.Show("Inspector account updated successfully.\r\n\r\nPlease reselect the inspector from the grid.", "Update successful", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString(), "Inspector account update failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            
                sqlConn.Close();
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
                if (bNewInsp)
                {
                    this.Close();
                }
            }

        }

        private void bSubmit_Click(object sender, EventArgs e)
        {

            string sAcntName = tbBankAcntName.Text.Trim();
            string sAcntCode = tbAcntCode.Text.Trim();
            string sRouteNum = tbRouteNum.Text.Trim();
            string sAcntNum = tbAcntNum.Text.Trim();
            int iChecking = 0;
            
            if (rbChecking.Checked)
                iChecking = 1;

            object oRetVal;

            // init SQL connection
            SqlConnection sqlConn = new SqlConnection(cfg_SQL360UtilConnStr);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConn;

            if (sRouteNum.Length > 0 && sRouteNum.Length != 9)
            {
                MessageBox.Show("Routing number must be 9 digits","Inspector Information",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }

            if (sRouteNum.IndexOf(" ") != -1)
            {
                MessageBox.Show("Routing cannot contain spaces.", "Inspector Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            if (sAcntNum.IndexOf(" ") != -1)
            {
                MessageBox.Show("Account cannot contain spaces.", "Inspector Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }



            try
            {

                sqlConn.Open();

                // update FieldRepPayroll table
                sqlCommand.CommandText = "UPDATE Sutton360Utils.dbo.FieldRepPayroll ";
                sqlCommand.CommandText += "Set BankAccountName = '" + sAcntName + "', PayrollNum = '" + sAcntCode + "', RoutingNumber = '" + sRouteNum + "', AccountNumber ='" + sAcntNum + "', Checking = " + iChecking.ToString();
                sqlCommand.CommandText += " WHERE FieldRepID = '" + FieldRepID + "'";
                
                oRetVal = sqlCommand.ExecuteNonQuery();

                bSubmit.Enabled = false;
                bCancel.Text = "Close";
                MessageBox.Show("Inspector account updated successfully.", "Update successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Inspector account update failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
               sqlConn.Close();
            }
        }

     
    }
}
