﻿namespace PayrollDeposit
{
    partial class frmFieldRepList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRefreshGrid = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPrintInspectorList = new System.Windows.Forms.ToolStripMenuItem();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.msMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Location = new System.Drawing.Point(0, 107);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(760, 559);
            this.pnlMain.TabIndex = 0;
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile,
            this.tsmiRefreshGrid,
            this.tsmiPrint});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(760, 24);
            this.msMain.TabIndex = 1;
            this.msMain.Text = "menuStrip1";
            // 
            // tsmiFile
            // 
            this.tsmiFile.Name = "tsmiFile";
            this.tsmiFile.Size = new System.Drawing.Size(35, 20);
            this.tsmiFile.Text = "File";
            this.tsmiFile.Click += new System.EventHandler(this.tsmiFile_Click);
            // 
            // tsmiRefreshGrid
            // 
            this.tsmiRefreshGrid.Name = "tsmiRefreshGrid";
            this.tsmiRefreshGrid.Size = new System.Drawing.Size(73, 20);
            this.tsmiRefreshGrid.Text = "Refresh list";
            this.tsmiRefreshGrid.Click += new System.EventHandler(this.tsmiRefreshGrid_Click);
            // 
            // tsmiPrint
            // 
            this.tsmiPrint.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiPrintInspectorList});
            this.tsmiPrint.Name = "tsmiPrint";
            this.tsmiPrint.Size = new System.Drawing.Size(41, 20);
            this.tsmiPrint.Text = "Print";
            // 
            // tsmiPrintInspectorList
            // 
            this.tsmiPrintInspectorList.Name = "tsmiPrintInspectorList";
            this.tsmiPrintInspectorList.Size = new System.Drawing.Size(147, 22);
            this.tsmiPrintInspectorList.Text = "Inspector list";
            this.tsmiPrintInspectorList.Click += new System.EventHandler(this.tsmiPrintInspectorList_Click);
            // 
            // tbTitle
            // 
            this.tbTitle.BackColor = System.Drawing.Color.White;
            this.tbTitle.ForeColor = System.Drawing.Color.Black;
            this.tbTitle.Location = new System.Drawing.Point(9, 27);
            this.tbTitle.Multiline = true;
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.ReadOnly = true;
            this.tbTitle.Size = new System.Drawing.Size(743, 74);
            this.tbTitle.TabIndex = 2;
            this.tbTitle.TabStop = false;
            this.tbTitle.Text = "This area is used to enter and update inspector bank account information.\r\n\r\nDoub" +
                "le click an inspector to edit.\r\nClick the Refresh List menu item to reload the l" +
                "ist.";
            // 
            // frmFieldRepList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(760, 668);
            this.Controls.Add(this.tbTitle);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.msMain);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.msMain;
            this.MaximizeBox = false;
            this.Name = "frmFieldRepList";
            this.Text = "Inspector fee information";
            this.Load += new System.EventHandler(this.FieldRepMaint_Load);
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiFile;
        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.ToolStripMenuItem tsmiRefreshGrid;
        private System.Windows.Forms.ToolStripMenuItem tsmiPrint;
        private System.Windows.Forms.ToolStripMenuItem tsmiPrintInspectorList;
    }
}