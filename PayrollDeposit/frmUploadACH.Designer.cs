﻿namespace PayrollDeposit
{
    partial class frmUploadACH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tbFileToUpload = new System.Windows.Forms.TextBox();
            this.tbFileUpMsg = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bUpload = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.DimGray;
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.MaximumSize = new System.Drawing.Size(500, 20);
            this.textBox1.MinimumSize = new System.Drawing.Size(0, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(500, 15);
            this.textBox1.TabIndex = 0;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "Upload ACH file to Bank of America for processing.";
            // 
            // tbFileToUpload
            // 
            this.tbFileToUpload.BackColor = System.Drawing.Color.White;
            this.tbFileToUpload.Location = new System.Drawing.Point(6, 45);
            this.tbFileToUpload.Name = "tbFileToUpload";
            this.tbFileToUpload.ReadOnly = true;
            this.tbFileToUpload.Size = new System.Drawing.Size(548, 20);
            this.tbFileToUpload.TabIndex = 1;
            this.tbFileToUpload.TabStop = false;
            // 
            // tbFileUpMsg
            // 
            this.tbFileUpMsg.BackColor = System.Drawing.Color.White;
            this.tbFileUpMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbFileUpMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFileUpMsg.ForeColor = System.Drawing.Color.Black;
            this.tbFileUpMsg.Location = new System.Drawing.Point(6, 19);
            this.tbFileUpMsg.MinimumSize = new System.Drawing.Size(0, 20);
            this.tbFileUpMsg.Name = "tbFileUpMsg";
            this.tbFileUpMsg.ReadOnly = true;
            this.tbFileUpMsg.Size = new System.Drawing.Size(548, 15);
            this.tbFileUpMsg.TabIndex = 2;
            this.tbFileUpMsg.TabStop = false;
            this.tbFileUpMsg.Text = "The following file will be uploaded to Bank of America:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbFileUpMsg);
            this.groupBox1.Controls.Add(this.tbFileToUpload);
            this.groupBox1.Location = new System.Drawing.Point(12, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(560, 78);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " File to Upload ";
            // 
            // bUpload
            // 
            this.bUpload.Location = new System.Drawing.Point(211, 188);
            this.bUpload.Name = "bUpload";
            this.bUpload.Size = new System.Drawing.Size(75, 23);
            this.bUpload.TabIndex = 4;
            this.bUpload.Text = "Upload File";
            this.bUpload.UseVisualStyleBackColor = true;
            this.bUpload.Click += new System.EventHandler(this.bUpload_Click);
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(306, 188);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 5;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // tbResult
            // 
            this.tbResult.BackColor = System.Drawing.Color.White;
            this.tbResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbResult.ForeColor = System.Drawing.Color.Maroon;
            this.tbResult.Location = new System.Drawing.Point(71, 138);
            this.tbResult.Name = "tbResult";
            this.tbResult.ReadOnly = true;
            this.tbResult.Size = new System.Drawing.Size(451, 19);
            this.tbResult.TabIndex = 6;
            this.tbResult.TabStop = false;
            this.tbResult.Text = "tbResult";
            this.tbResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // frmUploadACH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(592, 231);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bUpload);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUploadACH";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Upload ACH File to Bank of America";
            this.Load += new System.EventHandler(this.frmUploadACH_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox tbFileToUpload;
        private System.Windows.Forms.TextBox tbFileUpMsg;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bUpload;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.TextBox tbResult;
    }
}