﻿namespace PayrollDeposit
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPrintSummary = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiInspectorMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiInspectors = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiACH = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiBuildACH = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiUploadACH = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExport = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dtpPayDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.bLoadData = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbTotPeriodPR = new System.Windows.Forms.TextBox();
            this.tbNumInspectors = new System.Windows.Forms.TextBox();
            this.msMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Location = new System.Drawing.Point(5, 107);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(760, 559);
            this.pnlMain.TabIndex = 0;
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.tsmiPrint,
            this.tsmiInspectorMenu,
            this.tsmiACH,
            this.exportToolStripMenuItem});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(760, 24);
            this.msMain.TabIndex = 1;
            this.msMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(93, 22);
            this.tsmiExit.Text = "Exit";
            this.tsmiExit.Click += new System.EventHandler(this.tsmiExit_Click);
            // 
            // tsmiPrint
            // 
            this.tsmiPrint.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiPrintSummary});
            this.tsmiPrint.Name = "tsmiPrint";
            this.tsmiPrint.Size = new System.Drawing.Size(44, 20);
            this.tsmiPrint.Text = "Print";
            // 
            // tsmiPrintSummary
            // 
            this.tsmiPrintSummary.Name = "tsmiPrintSummary";
            this.tsmiPrintSummary.Size = new System.Drawing.Size(145, 22);
            this.tsmiPrintSummary.Text = "Fee summary";
            this.tsmiPrintSummary.Click += new System.EventHandler(this.tsmiPrintSummary_Click);
            // 
            // tsmiInspectorMenu
            // 
            this.tsmiInspectorMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiInspectors});
            this.tsmiInspectorMenu.Name = "tsmiInspectorMenu";
            this.tsmiInspectorMenu.Size = new System.Drawing.Size(73, 20);
            this.tsmiInspectorMenu.Text = "Inspectors";
            this.tsmiInspectorMenu.Click += new System.EventHandler(this.tsmiInspectorMenu_Click);
            // 
            // tsmiInspectors
            // 
            this.tsmiInspectors.Name = "tsmiInspectors";
            this.tsmiInspectors.Size = new System.Drawing.Size(191, 22);
            this.tsmiInspectors.Text = "Account maintenance";
            this.tsmiInspectors.Click += new System.EventHandler(this.tsmiInspectors_Click);
            // 
            // tsmiACH
            // 
            this.tsmiACH.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiBuildACH,
            this.tsmiUploadACH});
            this.tsmiACH.Name = "tsmiACH";
            this.tsmiACH.Size = new System.Drawing.Size(65, 20);
            this.tsmiACH.Text = "ACH File";
            // 
            // tsmiBuildACH
            // 
            this.tsmiBuildACH.Name = "tsmiBuildACH";
            this.tsmiBuildACH.Size = new System.Drawing.Size(159, 22);
            this.tsmiBuildACH.Text = "Build ACH file";
            this.tsmiBuildACH.Click += new System.EventHandler(this.tsmiBuildACH_Click);
            // 
            // tsmiUploadACH
            // 
            this.tsmiUploadACH.Name = "tsmiUploadACH";
            this.tsmiUploadACH.Size = new System.Drawing.Size(159, 22);
            this.tsmiUploadACH.Text = "Upload ACH file";
            this.tsmiUploadACH.Click += new System.EventHandler(this.tsmiUploadACH_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiExport});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // tsmiExport
            // 
            this.tsmiExport.Name = "tsmiExport";
            this.tsmiExport.Size = new System.Drawing.Size(210, 22);
            this.tsmiExport.Text = "Export current period fees";
            this.tsmiExport.Click += new System.EventHandler(this.tsmiExport_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            this.textBox1.Location = new System.Drawing.Point(5, 27);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(430, 74);
            this.textBox1.TabIndex = 2;
            // 
            // dtpPayDate
            // 
            this.dtpPayDate.Location = new System.Drawing.Point(74, 66);
            this.dtpPayDate.Name = "dtpPayDate";
            this.dtpPayDate.Size = new System.Drawing.Size(194, 20);
            this.dtpPayDate.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(47, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(351, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Select the pay date for the current period. This should be the Friday date.";
            // 
            // bLoadData
            // 
            this.bLoadData.AutoSize = true;
            this.bLoadData.Location = new System.Drawing.Point(274, 66);
            this.bLoadData.Name = "bLoadData";
            this.bLoadData.Size = new System.Drawing.Size(92, 23);
            this.bLoadData.TabIndex = 5;
            this.bLoadData.Text = "Load data";
            this.bLoadData.UseVisualStyleBackColor = true;
            this.bLoadData.Click += new System.EventHandler(this.bLoadData_Click);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.ForeColor = System.Drawing.Color.Black;
            this.textBox2.Location = new System.Drawing.Point(441, 27);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(315, 74);
            this.textBox2.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(472, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Total fees for the period";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(470, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Number of inspectors paid";
            // 
            // tbTotPeriodPR
            // 
            this.tbTotPeriodPR.BackColor = System.Drawing.Color.White;
            this.tbTotPeriodPR.ForeColor = System.Drawing.Color.Black;
            this.tbTotPeriodPR.Location = new System.Drawing.Point(607, 42);
            this.tbTotPeriodPR.Name = "tbTotPeriodPR";
            this.tbTotPeriodPR.ReadOnly = true;
            this.tbTotPeriodPR.Size = new System.Drawing.Size(100, 20);
            this.tbTotPeriodPR.TabIndex = 9;
            this.tbTotPeriodPR.TabStop = false;
            // 
            // tbNumInspectors
            // 
            this.tbNumInspectors.BackColor = System.Drawing.Color.White;
            this.tbNumInspectors.ForeColor = System.Drawing.Color.Black;
            this.tbNumInspectors.Location = new System.Drawing.Point(606, 69);
            this.tbNumInspectors.Name = "tbNumInspectors";
            this.tbNumInspectors.ReadOnly = true;
            this.tbNumInspectors.Size = new System.Drawing.Size(100, 20);
            this.tbNumInspectors.TabIndex = 10;
            this.tbNumInspectors.TabStop = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(760, 668);
            this.Controls.Add(this.tbNumInspectors);
            this.Controls.Add(this.tbTotPeriodPR);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.bLoadData);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpPayDate);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.msMain);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.msMain;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "Inspector Fee Processing";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
        private System.Windows.Forms.ToolStripMenuItem tsmiPrint;
        private System.Windows.Forms.ToolStripMenuItem tsmiPrintSummary;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DateTimePicker dtpPayDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bLoadData;
        private System.Windows.Forms.ToolStripMenuItem tsmiInspectorMenu;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbTotPeriodPR;
        private System.Windows.Forms.TextBox tbNumInspectors;
        private System.Windows.Forms.ToolStripMenuItem tsmiACH;
        private System.Windows.Forms.ToolStripMenuItem tsmiInspectors;
        private System.Windows.Forms.ToolStripMenuItem tsmiBuildACH;
        private System.Windows.Forms.ToolStripMenuItem tsmiUploadACH;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiExport;
    }
}

