﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Diagnostics;


namespace PayrollDeposit
{
    public partial class frmUploadACH : Form
    {
        private string cfg_ACHFilePath;
        private string cfg_ACHArchiveFilePath;
        private string cfg_logfilename;
        private string msExceptionList;

        private LogUtils.LogUtils oLU;

        DirectoryInfo diSource;
        FileInfo[] fiSource;

        public frmUploadACH()
        {
            InitializeComponent();
        }

        private void frmUploadACH_Load(object sender, EventArgs e)
        {

            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_ACHFilePath = colNameVal.Get("ACHFilePath");
            cfg_ACHArchiveFilePath = colNameVal.Get("ACHArchiveFilePath");
            cfg_logfilename = colNameVal.Get("logfilename");

            bUpload.Enabled = false;

            try
            {

                diSource = new DirectoryInfo(cfg_ACHFilePath);
                fiSource = diSource.GetFiles("*.txt");

                if (fiSource.Length == 0)
                {
                    tbResult.Text = "No ACH files were found in the upload folder.";
                    throw new SystemException("No ACH files were found in the upload folder:\r\n" + cfg_ACHFilePath);
                }

                if (fiSource.Length > 1)
                {
                    tbResult.Text = "Multiple ACH files were found in the upload folder.";
                    throw new SystemException("Multiple ACH files were found in the upload folder:\r\n" + cfg_ACHFilePath + "\r\n\r\nThis must be corrected before the process can continue.");
                }
                                
                tbFileToUpload.Text = fiSource[0].Name;
                bUpload.Enabled = true;

                tbResult.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show("The following error occurred while attempting to upload the ACH file: \r\n\r\n" + ex.Message, "ACH File Upload Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {

                diSource = null;
                fiSource = null;

            }

        }

        private void bUpload_Click(object sender, EventArgs e)
        {

            int iNumUploaded = 0;

            this.Cursor = Cursors.WaitCursor;
            tbResult.Text = "Uploading file - please wait...";
            bUpload.Enabled = false;

            this.Refresh();

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();

            msExceptionList = "";

            try
            {

                diSource = new DirectoryInfo(cfg_ACHFilePath);
                fiSource = diSource.GetFiles("*.txt");

                string sLogDate = DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Second.ToString();
                //string sLogName = "C:\\temp\\aiexport\\SFTPLog_" + sLogDate + ".xml";
                string sLogName = "J:\\BOAInspectorPayroll\\Logs\\SFTPLog_" + sLogDate + ".xml";

                // Run WinSCP process
                Process winscp = new Process();
                winscp.StartInfo.FileName = "winscp.com";
                winscp.StartInfo.Arguments = "/log=\"" + sLogName + "\"";
                winscp.StartInfo.UseShellExecute = false;
                winscp.StartInfo.RedirectStandardInput = true;
                winscp.StartInfo.RedirectStandardOutput = true;
                winscp.StartInfo.CreateNoWindow = true;
                winscp.Start();

                winscp.StandardInput.WriteLine("option batch abort");
                winscp.StandardInput.WriteLine("option confirm off");
                winscp.StandardInput.WriteLine("open BOA_ACH");
                winscp.StandardInput.WriteLine("ls");

                oLU.WritetoLog("Begin SFTP PUT - " + cfg_ACHFilePath + fiSource[0].Name);

                winscp.StandardInput.WriteLine("put " + cfg_ACHFilePath + fiSource[0].Name);
                iNumUploaded++;

                oLU.WritetoLog("End SFTP PUT - " + cfg_ACHFilePath + fiSource[0].Name);

                winscp.StandardInput.Close();

                // Collect all output
                string output = winscp.StandardOutput.ReadToEnd();

                // Wait until WinSCP finishes
                winscp.WaitForExit();

                oLU.WritetoLog("SFTP Waitfor Reached");
                oLU.WritetoLog(output);

                // Exit code <> 0
                if (winscp.ExitCode != 0)
                {
                    oLU.WritetoLog("WinSCP exit code indicates upload was not successful.\r\nWinscp.ExitCode=" + winscp.ExitCode.ToString());
                    msExceptionList += "WinSCP did not return the expected exit code of 0.\r\nThis is typically an indication the upload failed.\r\nVerify Pagent is running\r\nwinscp.ExitCode=" + winscp.ExitCode.ToString() + "\r\n\r\n";
                }

                // Parse and interpret the XML log
                // (Note that in case of fatal failure the log file may not exist at all)
                if (File.Exists(sLogName))
                {
                    XPathDocument log = new XPathDocument(sLogName);
                    XmlNamespaceManager ns = new XmlNamespaceManager(new NameTable());
                    ns.AddNamespace("w", "http://winscp.net/schema/session/1.0");
                    XPathNavigator nav = log.CreateNavigator();

                    // Success (0) or error?
                    if (winscp.ExitCode != 0)
                    {
                        // See if there are any messages associated with the error
                        foreach (XPathNavigator message in nav.Select("//w:message", ns))
                        {
                            oLU.WritetoLog("WinSCP log contains the following error: " + message.Value);
                            msExceptionList += "WinSCP log contains the following error: " + message.Value + "\r\n";

                        }
                    }

                }
                else
                {
                    oLU.WritetoLog("Unable to locate WinSCP log file: " + sLogName );
                    msExceptionList += "Unable to locate WinSCP log file: " + sLogName + "\r\nThis typically indicates the upload was not successful.\r\n";                                
                }

                this.Cursor = Cursors.Default;
                bCancel.Text = "Close";

                if (msExceptionList.Length == 0)
                {
                    tbResult.Text = "Upload complete";
                    MessageBox.Show("ACH file uploaded successfully.\r\nPlease verify that Bank of America has received the file.", "ACH File Uploaded", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    // Move ACH file to archive
                    fiSource[0].MoveTo(cfg_ACHArchiveFilePath + fiSource[0].Name);
                }

                else           
                {
                    tbResult.Text = "Process completed with errors";
                    MessageBox.Show("Issues were found during upload of ACH file.\r\n\r\nMore information will be displayed after you click OK.", "Error uploading ACH file", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    frmMessage frm = new frmMessage();
                    frm.Text = "ACH File Upload Exceptions";
                    frm.MessageText = msExceptionList;
                    frm.ShowDialog();
                }
  
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                oLU.WritetoLog(ex.Message);
                tbResult.Text = "**** UPLOAD FAILED ****";
                MessageBox.Show("The following error occurred while attempting to upload the ACH file: \r\n\r\n" + ex.Message, "ACH File Upload Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                // Close log
                oLU.closeLog();
                oLU = null;

                diSource = null;
                fiSource = null;

                this.Cursor = Cursors.Default;

            }

        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
