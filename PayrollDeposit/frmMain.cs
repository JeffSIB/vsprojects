﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Xceed.Grid;
using Xceed.Grid.Reporting;
using Xceed.Grid.Editors;
using Xceed.Grid.Viewers;
using Xceed.Grid.Collections;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Web.Security;



namespace PayrollDeposit
{
    public partial class frmMain : Form
    {

        //private string cfg_SQL360ConnStr;
        private string cfg_SQL360UtilConnStr;
        private string cfg_SQLMainSIBIConnStr;
        static string cfg_WebSQLConnStr;
        private string cfg_ACHFilePath;
        private string cfg_ExportFilePath;
        private string msExceptionList;
        private decimal mdRouteHashTotal;
        private string cfg_InspFeeAcnt;
        private string cfg_CashAcnt;
        private DateTime mdPayDate;
        private string msGridSortCol;
        private bool mbGridSortDir;

        // report totals
        private int miRPTInspCount;
        private decimal mdRPTTotalPRAmt;


        private string rptTitle = "";
        private Xceed.Grid.GridControl xGrid = new GridControl();
        ColumnManagerRow xGridColumnManagerRow;
        
        private SqlConnection sqlConnSIB = null;
        private SqlCommand sqlCmdSIB = null;
        private SqlDataReader sqlReaderSIB = null;

        private SqlConnection sqlConn360 = null;
        private SqlCommand sqlCmd360 = null;


        public frmMain()
        {
            InitializeComponent();
        }

 
        private void frmMain_Load(object sender, EventArgs e)
        {
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            //cfg_SQL360ConnStr = colNameVal.Get("SQL360ConnStr");
            cfg_SQL360UtilConnStr = colNameVal.Get("SQL360UtilConnStr");
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");
            cfg_WebSQLConnStr = colNameVal.Get("WebSQLConnStr");
            cfg_ACHFilePath = colNameVal.Get("ACHFilePath");
            cfg_ExportFilePath = colNameVal.Get("ExportFilePath");
            cfg_InspFeeAcnt = colNameVal.Get("InspectorFeeAccount");
            cfg_CashAcnt = colNameVal.Get("CashAccount");

            // default date picker to next Friday
            DateTime today = new DateTime();
            today = DateTime.Today;
            int daysUntilFriday = ((int)DayOfWeek.Friday- (int)today.DayOfWeek + 7) % 7;
            DateTime nextFriday = today.AddDays(daysUntilFriday);
            dtpPayDate.Value = nextFriday;

            tbNumInspectors.Text = "";
            tbTotPeriodPR.Text = "";
            tsmiPrint.Enabled = false;
            tsmiBuildACH.Enabled = false;
            tsmiExport.Enabled = false;
            tsmiACH.Enabled = true;

        }
        
        private void bLoadData_Click(object sender, EventArgs e)
        {

            DateTime dPRDate = dtpPayDate.Value;
            mdPayDate = dtpPayDate.Value;

            if (dPRDate.DayOfWeek != DayOfWeek.Friday)
            {
                if (MessageBox.Show("Pay date should be a Friday.\r\nContinue ?", "Verify pay date", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
            }

            tbNumInspectors.Text = "";
            tbTotPeriodPR.Text = "";
            tsmiPrint.Enabled = false;
            tsmiExport.Enabled = false;

            // place grid on panel
            this.pnlMain.Controls.Add(xGrid);
            xGrid.Visible = false;

            // grid sort order
            msGridSortCol = "";
            mbGridSortDir = true;

            // load payroll data
            xGrid.Clear();

            this.Refresh();
            Application.DoEvents();

            PrepareGrid();

        }
         
        private void PrepareGrid()
        {
            try
            {

                xGrid.Visible = false;
                xGrid.Clear();
                this.Cursor = Cursors.WaitCursor;
                this.Refresh();
                Application.DoEvents();

                // header row
                xGridColumnManagerRow = new ColumnManagerRow();
                xGrid.FixedHeaderRows.Add(xGridColumnManagerRow);

                //COLUMNS
                xGrid.Columns.Add(new Column("Insp", typeof(string)));
                xGrid.Columns["Insp"].Title = "Inspector";
                xGrid.Columns["Insp"].Width = 90;
                xGrid.Columns["Insp"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["Insp"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["Insp"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("InspName", typeof(string)));
                xGrid.Columns["InspName"].Title = "Inspector name";
                xGrid.Columns["InspName"].Width = 140;
                xGrid.Columns["InspName"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["InspName"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["InspName"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("PRNum", typeof(string)));
                xGrid.Columns["PRNum"].Title = "Code";
                xGrid.Columns["PRNum"].Width = 70;
                xGrid.Columns["PRNum"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["PRNum"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center; 
                xGrid.DataRowTemplate.Cells["PRNum"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("SIBAmount", typeof(decimal)));
                xGrid.Columns["SIBAmount"].Title = "SIB";
                xGrid.Columns["SIBAmount"].Width = 90;
                xGrid.Columns["SIBAmount"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Right;
                xGrid.Columns["SIBAmount"].FormatSpecifier = "c2";
                xGrid.Columns["SIBAmount"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["SIBAmount"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);
                
                xGrid.Columns.Add(new Column("360Amount", typeof(decimal)));
                xGrid.Columns["360Amount"].Title = "360";
                xGrid.Columns["360Amount"].Width = 90;
                xGrid.Columns["360Amount"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Right;
                xGrid.Columns["360Amount"].FormatSpecifier = "c2";
                xGrid.Columns["360Amount"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["360Amount"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("TotalPay", typeof(decimal)));
                xGrid.Columns["TotalPay"].Title = "Total";
                xGrid.Columns["TotalPay"].Width = 90;
                xGrid.Columns["TotalPay"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Right;
                xGrid.Columns["TotalPay"].FormatSpecifier = "c2";
                xGrid.Columns["TotalPay"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["TotalPay"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("FieldRepID", typeof(string)));
                xGrid.Columns["FieldRepID"].Visible = false;
                xGrid.Columns["FieldRepID"].Width = 0;


                //grid wide settings
                xGrid.ReadOnly = true;
                xGrid.Dock = DockStyle.Fill;
                xGrid.FixedColumnSplitter.Visible = false;
                xGrid.RowSelectorPane.Visible = false;
                xGrid.KeyDown += new KeyEventHandler(xGrid_KeyDown);

                // prevent cell navigation
                xGrid.AllowCellNavigation = false;

                // resize
                xGrid.Resize += new System.EventHandler(xGrid_Resize);

                // Sort
                //xGrid.SortedColumnsChanged += new EventHandler(xGrid_SortedColumnsChanged);

                // declare vars
                // get paydae from main screen - typically Friday
                DateTime dPayDate = dtpPayDate.Value;
                
                // Set 360 pay date to pick up anything paid on Wednesday or Thursday
                DateTime d360PayDate = dPayDate.AddDays(-2);
                DateTime d360PayDatePlus1 = d360PayDate.AddDays(2);

                ///////////////////////////////////////////////////////////
                // Adjust 360 date if not done on Wednesday 
                //DateTime d360PayDate = dPayDate.AddDays(-1);
                //DateTime d360PayDatePlus1 = d360PayDate.AddDays(1);
                ///////////////////////////////////////////////////////////

                string msPayDate = dPayDate.ToShortDateString();

                rptTitle = "Pay period ending " + dPayDate.Month.ToString() + "/" + dPayDate.Day.ToString() + "/" + dPayDate.Year.ToString();
                this.Text = "Inspector fees - period ending " + dPayDate.Month.ToString() + "/" + dPayDate.Day.ToString() + "/" + dPayDate.Year.ToString();

                string sFieldRepNumber = "";
                string sFieldRepID = "";
                string s360FieldRepID = "";
                string sFieldRepFirstName = "";
                string sFieldRepLastName = "";
                string sFieldRepName = "";
                string sAcntName = "";
                string sPayrollNum = "";


                decimal dSIBPay = 0;
                decimal d360Pay = 0;
                int iInspCount = 0;
                decimal dTotalPayroll = 0;
                decimal dTotalInspPay = 0;

                // init SQL for calcSIBPR
                sqlConnSIB = new SqlConnection(cfg_SQLMainSIBIConnStr);
                sqlCmdSIB = new SqlCommand();
                sqlCmdSIB.Connection = sqlConnSIB;
                sqlConnSIB.Open();


                // init SQL for calc360PR
                sqlConn360 = new SqlConnection(cfg_SQL360UtilConnStr);
                sqlCmd360 = new SqlCommand();
                sqlCmd360.Connection = sqlConn360;
                sqlConn360.Open();
                
                // init SQL connection
                SqlDataReader sqlDR;
                SqlConnection sqlConn = new SqlConnection(cfg_SQL360UtilConnStr);
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConn;
                sqlConn.Open();

                // get all active inspectors
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "sp_PayrollProc_LoadData";

                //sqlCommand.CommandText = "SELECT fr.LoginID, fr.FieldRepNumber,ui.FirstName, ui.LastName, frp.BankAccountName, fr.FieldRepID, frp.PayrollNum ";
                //sqlCommand.CommandText += "FROM Sutton.dbo.FieldReps fr inner join Sutton.dbo.UserInfos ui on ui.UserID = fr.LoginID inner join Sutton.dbo.aspnet_Membership aspm on aspm.UserId = fr.LoginID ";
                //sqlCommand.CommandText += "LEFT OUTER JOIN Sutton360Utils.dbo.FieldRepPayroll frp on frp.FieldRepID = fr.LoginID ";
                //sqlCommand.CommandText += "WHERE aspm.IsApproved = 1  ";
                //sqlCommand.CommandText += "WHERE fr.FieldRepNumber = 'M31'";
                //sqlCommand.CommandText += "ORDER BY fr.FieldRepNumber";

                sqlDR = sqlCommand.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {
                    // add row to grid
                    xGrid.BeginInit();
                    
                    // loop through active inspectors
                    while (sqlDR.Read())
                    {

                        try
                        {

                            sFieldRepID = sqlDR.GetSqlGuid(0).ToString();
                            sFieldRepNumber = sqlDR.GetSqlString(1).ToString();

                            sFieldRepFirstName = sqlDR.IsDBNull(2) ? "" : sqlDR.GetSqlString(2).ToString();
                            sFieldRepLastName = sqlDR.IsDBNull(3) ? "" : sqlDR.GetSqlString(3).ToString();
                            sFieldRepName = sFieldRepFirstName.Length > 0 ? sFieldRepFirstName + " " : "";
                            sFieldRepName += sFieldRepLastName;
                            sAcntName = sqlDR.IsDBNull(4) ? "" : sqlDR.GetSqlString(4).ToString();
                            if (sAcntName.Length > 0)
                                sFieldRepName = sAcntName;
                            s360FieldRepID = sqlDR.GetSqlGuid(5).ToString();
                            sPayrollNum = sqlDR.IsDBNull(6) ? "" : sqlDR.GetSqlString(6).ToString();
                            
                            // Get any SIBOffice pay
                            //dSIBPay = calcSIBOfficePR(sFieldRepNumber, dPayDate);

                            //*************************************************
                            //Use for paying on Thursday
                            //DateTime dManualPayDate = dPayDate.AddDays(1);
                            //dSIBPay = calcSIBOfficePR(sFieldRepNumber, dManualPayDate);
                            //
                            //*************************************************

                            dSIBPay = Convert.ToDecimal("0.00");

                            // Get any 360 pay
                            d360Pay = calc360PR(s360FieldRepID, d360PayDate, d360PayDatePlus1);
                            
                            dTotalInspPay = dSIBPay + d360Pay;

                            //if (sFieldRepNumber == "R40")
                            if (dTotalInspPay > 0 || sFieldRepNumber == "C73" || sFieldRepNumber == "J30" || sFieldRepNumber == "E16" || sFieldRepNumber == "R40" || sFieldRepNumber == "J28" || sFieldRepNumber == "F36" || sFieldRepNumber == "L41" || sFieldRepNumber == "B81" || sFieldRepNumber == "P46" || sFieldRepNumber == "R20" || sFieldRepNumber == "C59" || sFieldRepNumber == "R23" || sFieldRepNumber == "O1" || sFieldRepNumber == "S39" || sFieldRepNumber == "M57" || sFieldRepNumber == "F33" || sFieldRepNumber == "D32" || sFieldRepNumber == "M70" || sFieldRepNumber == "K31" || sFieldRepNumber == "S72" || sFieldRepNumber == "C55" || sFieldRepNumber == "M68" || sFieldRepNumber == "R43" || sFieldRepNumber == "R40" || sFieldRepNumber == "J28" || sFieldRepNumber == "F36" || sFieldRepNumber == "L41" || sFieldRepNumber == "B81" || sFieldRepNumber == "P46" || sFieldRepNumber == "R20" || sFieldRepNumber == "C59" || sFieldRepNumber == "R23" || sFieldRepNumber == "O1" || sFieldRepNumber == "S39" || sFieldRepNumber == "M57" || sFieldRepNumber == "F33" || sFieldRepNumber == "D32" || sFieldRepNumber == "M70" || sFieldRepNumber == "K31" || sFieldRepNumber == "S72" || sFieldRepNumber == "C55" || sFieldRepNumber == "S77")
                            {

                                Xceed.Grid.DataRow dataRow = xGrid.DataRows.AddNew();
                                dataRow.Cells["Insp"].Value = sFieldRepNumber;
                                dataRow.Cells["InspName"].Value = sFieldRepName;
                                dataRow.Cells["PRNum"].Value = sPayrollNum;
                                dataRow.Cells["SIBAmount"].Value = dSIBPay;
                                dataRow.Cells["360Amount"].Value = d360Pay;
                                dataRow.Cells["TotalPay"].Value = dTotalInspPay;
                                dataRow.Cells["FieldRepID"].Value = sFieldRepID;

                                dataRow.EndEdit();

                                iInspCount += 1;
                                dTotalPayroll += dTotalInspPay;

                            }
                            
                        }
                        finally
                        {
                            xGrid.EndInit();

                        }	//try


                    }	// while


                }	// has rows

                sqlConn.Close();

                // if there is no sorted column - sort by Insp num
                if (msGridSortCol == "")
                {
                    xGrid.Columns["Insp"].SortDirection = SortDirection.Ascending;
                    msGridSortCol = "Insp";
                    mbGridSortDir = true;
                }
                else
                {
                    if (mbGridSortDir)
                        xGrid.Columns[msGridSortCol].SortDirection = SortDirection.Ascending;
                    else
                        xGrid.Columns[msGridSortCol].SortDirection = SortDirection.Descending;
                }

                if (iInspCount == 0)
                {
                    MessageBox.Show("There were no items found for the pay date selected.\r\nEither you have selected an incorrect date or pay has not been set for the period.", "No pay data found for period", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    tbTotPeriodPR.Text = dTotalPayroll.ToString("c2");
                    tbNumInspectors.Text = iInspCount.ToString();
                    miRPTInspCount = iInspCount;
                    mdRPTTotalPRAmt = dTotalPayroll;
                    tsmiPrint.Enabled = true;
                    tsmiBuildACH.Enabled = true;
                    tsmiExport.Enabled = true;
                    xGrid.Visible = true;
                    xGrid_Resize(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void xGrid_cell_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left) && (e.Clicks >= 2))
                this.xGrid_editRow(((Cell)sender).ParentRow);
        }

        private void xGrid_editRow(CellRow folderRow)
        {

            decimal dTotPay = Convert.ToDecimal(folderRow.Cells["TotalPay"].Value);

            frmAdjustFees frm = new frmAdjustFees();
            frm.FielRepNumber = folderRow.Cells["Insp"].Value.ToString();
            frm.FieldRepName = folderRow.Cells["InspName"].Value.ToString();
            frm.TotalInspPay = dTotPay.ToString("c2");
            frm.ShowDialog();

            this.Cursor = Cursors.WaitCursor;

            dTotPay = frm.decNewTotFees;

            folderRow.Cells["TotalPay"].Value = dTotPay;
            frm.Close();
            xGrid.Refresh();

            // recalc payroll
            ReadOnlyDataRowList dataRows = xGrid.GetSortedDataRows(true);
            
            int iInspCount = 0;
            decimal dTotalPayroll = 0;
            decimal dPayAmount = 0;

            foreach (Xceed.Grid.DataRow dr in dataRows)
            {
                dPayAmount = (decimal)dr.Cells["TotalPay"].Value;
                iInspCount += 1;
                dTotalPayroll += dPayAmount;
            }

            tbTotPeriodPR.Text = dTotalPayroll.ToString("c2");
            tbNumInspectors.Text = iInspCount.ToString();
            miRPTInspCount = iInspCount;
            mdRPTTotalPRAmt = dTotalPayroll;

            this.Cursor = Cursors.Default;
            this.Refresh();
            Application.DoEvents();

        }

        private void xGrid_KeyDown(object sender, KeyEventArgs e)
        ///<summary>
        ///
        {
            switch (e.KeyData)
            {
                case Keys.Enter:
                    {
                        CellRow currentRow = xGrid.CurrentRow as CellRow;

                        if (currentRow != null)
                            this.xGrid_editRow(currentRow);
                        break;
                    }

                case Keys.Back:
                    //this.NavigateUp();
                    break;
            }
        }

        private void xGrid_SortedColumnsChanged(object sender, System.EventArgs e)
        {

            // preserve existing sort order
            SortedColumnList sclxTemp = xGrid.SortedColumns;

            foreach (Xceed.Grid.Column column in sclxTemp)
            {
                if (column.SortIndex == 0)
                {
                    msGridSortCol = column.FieldName.ToString();
                    mbGridSortDir = (column.SortDirection == SortDirection.Ascending);
                }

            }
        }

        // Grid resize
        private void xGrid_Resize(object sender, System.EventArgs e)
        {
            int iAvailWidth = xGrid.DisplayRectangle.Width;
            int iTotColWidth = 0;
            int iResizeCol = 1;
            int iAdjustment = 0;

            // minimized
            if (iAvailWidth == 0)
                return;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xGrid.Columns)
            {
                if (column.Index != iResizeCol)
                {
                    Console.WriteLine(column.FieldName);
                    iTotColWidth += column.Width;
                }

            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.Width = iWidth;
            }
            else
            {
                xResizeColumn.Width = iWidth;
            }
        }

        // resize grid for print
        private void xGrid_ResizeForPrint(int iAvailWidth)
        {
            int iTotColWidth = 0;
            int iResizeCol = 1;
            int iAdjustment = 10;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xGrid.Columns)
            {
                if (column.Index != iResizeCol)
                    iTotColWidth += column.Width;
            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
            else
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
        }


        private void tsmiExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsmiPrintSummary_Click(object sender, EventArgs e)
        {
            this.xGrid.ReportSettings.Title = rptTitle;
            this.xGrid.ReportSettings.Landscape = true;

            xGrid_ResizeForPrint(750);

            // base font for the report
            Font font = new Font("Arial", 9);
            this.xGrid.ReportStyle.Font = font;

            Report report = new Report(xGrid);
            ReportStyleSheet reportStyleSheet = report.ReportStyleSheet;


            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            reportStyleSheet.Grid.DataRows[0].BackColor = Color.FromArgb(205, 255, 205);
            reportStyleSheet.Grid.DataRows[1].BackColor = Color.Transparent;

            report.ReportStyleSheet.PageHeader.BottomBorder.Thickness = ReportLineThickness.SingleThin;
            report.ReportStyleSheet.PageHeader.BottomMargin = 5;
            report.ReportStyleSheet.PageHeader.Font = new Font(xGrid.ReportStyle.Font.FontFamily, 10, FontStyle.Bold);

            this.xGridColumnManagerRow.ReportStyle.RepeatOnEachPage = true;
            this.xGridColumnManagerRow.ReportStyle.BackColor = Color.DarkGray;
            this.xGridColumnManagerRow.ReportStyle.ForeColor = Color.White;
            this.xGridColumnManagerRow.ReportStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            this.xGridColumnManagerRow.ReportStyle.WordWrap = true;
            this.xGridColumnManagerRow.ReportStyle.RowHeightExpansion = RowHeightExpansion.ExpandWithoutLimits;
            this.xGridColumnManagerRow.ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            this.xGridColumnManagerRow.ReportStyle.VerticalAlignment = VerticalAlignment.Bottom;

            report.ReportStyleSheet.PageHeader.LeftElement.TextFormat = "Inspector Fees" + Environment.NewLine + "%Title%";
            report.ReportStyleSheet.PageHeader.RightElement.TextFormat =
              "Page : %Page format=000%" + Environment.NewLine +
              "Date : %DateTime format=g%";

            report.ReportStyleSheet.PageFooter.Font = new Font(xGrid.ReportStyle.Font.FontFamily, 10, FontStyle.Bold);
            report.ReportStyleSheet.PageFooter.TopBorder.Thickness = ReportLineThickness.SingleThin;
            report.ReportStyleSheet.PageFooter.LeftElement.TextFormat = miRPTInspCount.ToString() + " inspectors.";
            report.ReportStyleSheet.PageFooter.RightElement.TextFormat = "Total fees: " + mdRPTTotalPRAmt.ToString("c2");
            report.PrintPreview();
        }
      
 
        private decimal calcSIBOfficePR(string sInspNum, DateTime dPayDate)
        {

            decimal dPaidTotal = 0;
            decimal dPayAmount = 0;

            try
            {
                
                // set up SQL connection (SQLMain)
                sqlCmdSIB.Connection = sqlConnSIB;
                sqlCmdSIB.CommandType = CommandType.StoredProcedure;
                sqlCmdSIB.CommandText = "sp_InspFees_GetByPeriod";
                sqlCmdSIB.Parameters.Clear();
                sqlCmdSIB.Parameters.AddWithValue("@insp", sInspNum);
                sqlCmdSIB.Parameters.AddWithValue("@paydate", dPayDate);

                // Get open cases
                sqlReaderSIB = sqlCmdSIB.ExecuteReader();

                if (sqlReaderSIB.HasRows)
                {

                    sqlReaderSIB.Read();

                    do
                    {

                        dPaidTotal = (decimal)sqlReaderSIB.GetSqlMoney(14);
                        dPayAmount += dPaidTotal;


                    } while (sqlReaderSIB.Read());

                }

                sqlReaderSIB.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error calculating SIBOffice fees for " + sInspNum+ "\r\n" + ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return dPayAmount;

        }

        private decimal calc360PR(string sFieldRepID, DateTime dBegDate, DateTime dEndDate)
        {

            // Get pay for a single inspector with a generation date in the given range
            decimal dPaidTotal = 0;
            decimal dPayAmount = 0;

            try
            {

                // set up SQL connection (SQL360)
                sqlCmd360.Connection = sqlConn360;
                sqlCmd360.CommandType = CommandType.StoredProcedure;
                sqlCmd360.CommandText = "sp_InspFees_GetByPeriod";
                sqlCmd360.Parameters.Clear();
                sqlCmd360.Parameters.AddWithValue("@FieldRepID", sFieldRepID);
                sqlCmd360.Parameters.AddWithValue("@begdate",dBegDate );
                sqlCmd360.Parameters.AddWithValue("@enddate", dEndDate);
                
                // Get open cases
                sqlReaderSIB = sqlCmd360.ExecuteReader();

                if (sqlReaderSIB.HasRows)
                {

                    sqlReaderSIB.Read();

                    do
                    {

                        dPaidTotal = (decimal)sqlReaderSIB.GetSqlMoney(0);
                        dPayAmount += dPaidTotal;


                    } while (sqlReaderSIB.Read());

                }

                sqlReaderSIB.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error calculating 360 fees for /r/n" + ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return dPayAmount;

        }

        private void tsmiInspectors_Click(object sender, EventArgs e)
        {

            frmFieldRepList frmList = new frmFieldRepList();
            frmList.ShowDialog();

        }

        private void tsmiBuildACH_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            string sFileHeaderRecord = "";
            string sCompanyBatchRecord = "";
            string sDetailRecord = "";

            string sFieldRepNumber = "";
            string sFieldRepID = "";
            int iTotalLineItems = 0;
            decimal dTotalPayroll = 0;
            decimal dPayAmount = 0;
            string sPayAmount = "";

            msExceptionList = "";


            try{

                ACHBuilder achb = new ACHBuilder();


                // get data from grid
                ReadOnlyDataRowList dataRows = xGrid.GetSortedDataRows(true);
                if (dataRows.Count == 0)
                {
                    MessageBox.Show("No data to process.\r\n\r\nPay period must be loaded in order to create ACH file.", "ACH File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // ACH File name
                string sACHFileName = cfg_ACHFilePath + "SuttonACH" + DateTime.Now.ToString("MMddyyyy") +".txt";

                // delete file if it exists
                FileInfo fiACH = new FileInfo(sACHFileName);
                if (fiACH.Exists)
                {
                    fiACH.Delete();
                }
                fiACH = null;
            
                // Open ACH file
                StreamWriter swACH = new StreamWriter(sACHFileName,false);
            
                // Header record
                sFileHeaderRecord = achb.BuildFileHeader();
                swACH.WriteLine(sFileHeaderRecord);
                swACH.Flush();

                // Batch header record
                // mdPayDate sets when funds hit account
                sCompanyBatchRecord = achb.BuildCompanyBatchRecord(mdPayDate);
                swACH.WriteLine(sCompanyBatchRecord);
                swACH.Flush();

                // Detail records
                foreach (Xceed.Grid.DataRow dr in dataRows)
                {

                    sFieldRepNumber = dr.Cells["Insp"].Value.ToString();
                    sFieldRepID = dr.Cells["FieldRepID"].Value.ToString();
                    dPayAmount = (decimal)dr.Cells["TotalPay"].Value;
                    sPayAmount = String.Format("{0:0.00}", dPayAmount);

                    if (dPayAmount > 0)
                    {

                        // Build detail record
                        sDetailRecord = BuildDetailRecord(achb, sFieldRepID, sFieldRepNumber, sPayAmount);

                        // if record was successfully created - write it to the file
                        if (sDetailRecord.Length > 0)
                        {

                            swACH.WriteLine(sDetailRecord);
                            swACH.Flush();

                            // Totals
                            iTotalLineItems++;
                            dTotalPayroll += dPayAmount;
                        }
                    }
                }

                // Batch trailer record
                string sTotalLineItems = iTotalLineItems.ToString();
                sTotalLineItems= sTotalLineItems.PadLeft(6, Convert.ToChar("0"));
                
                // Remove decimal from pay total
                string sTotalPayroll = String.Format("{0:0.00}", dTotalPayroll);
                sTotalPayroll = RemoveDecimal(sTotalPayroll);
                sTotalPayroll = sTotalPayroll.PadLeft(12, Convert.ToChar("0"));

                string sRouteHash = String.Format("{0:0}", mdRouteHashTotal);
                sRouteHash = sRouteHash.PadLeft(10, Convert.ToChar("0"));

                sCompanyBatchRecord = achb.BuildCompanyBatchTrailerRecord(sTotalLineItems, sTotalPayroll, sRouteHash);
                swACH.WriteLine(sCompanyBatchRecord);
                swACH.Flush();


                // File control record
                sTotalLineItems = iTotalLineItems.ToString();
                sTotalLineItems = sTotalLineItems.PadLeft(8, Convert.ToChar("0"));
 
                int iBlockCount = iTotalLineItems + 4;
                string sBlockCount = iTotalLineItems.ToString();
                sCompanyBatchRecord = achb.BuildFileControlRecord(sTotalLineItems, sTotalPayroll, sRouteHash,sBlockCount);
                swACH.WriteLine(sCompanyBatchRecord);
                swACH.Flush();

                swACH.Close();
                achb = null;

                this.Cursor = Cursors.Default;

                if (msExceptionList.Length > 0)
                {

                    // delete file if it exists
                    fiACH = new FileInfo(sACHFileName);
                    if (fiACH.Exists)
                    {
                        fiACH.Delete();
                    }
                    fiACH = null;
                    
                    MessageBox.Show("Exceptions were found while creating the ACH file. These must be corrected before the process can continue.\r\n\r\nA list will be displayed after you click OK.", "Unable to create ACH file", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    frmMessage frm = new frmMessage();
                    frm.Text = "ACH File Exceptions";
                    frm.MessageText = msExceptionList;
                    frm.ShowDialog();
                    
                }
                else
                {
                    //MessageBox.Show("ACH file was created successfully\r\n\r\nFile name:  " + sACHFileName + "\r\nSettlement date: " + mdPayDate.ToShortDateString() + "\r\nTotal line items: " + iTotalLineItems.ToString() + "\r\nTotal payroll amount: " + dTotalPayroll.ToString("c2"), "ACH Created Successfully", MessageBoxButtons.OK, MessageBoxIcon.Information); ;

                    frmMessage frm = new frmMessage();
                    frm.Text = "ACH File Created";
                    frm.MessageText = "ACH file was created successfully\r\n\r\nFile name:  " + sACHFileName + "\r\nSettlement date: " + mdPayDate.ToShortDateString() + "\r\nTotal line items: " + iTotalLineItems.ToString() + "\r\nTotal amount: " + dTotalPayroll.ToString("c2");
                    frm.ShowDialog();
                }
                
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("The following error occurred while attempting to create the ACH file: \r\n\r\n" + ex.ToString(), "ACH File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }



        }

        private string BuildDetailRecord(ACHBuilder achb, string sFieldRepID, string sFieldRepNum, string sPayAmount)
        
        {

            string sRetVal = "";
            string sAcntName = "";
            string sAcntCode = "";
            string sRouteNum = "";
            string sAcntNum = "";
            string sFieldRepFirstName = "";
            string sFieldRepLastName = "";
            string sFieldRepName = "";
            bool bIsChecking = false;
            bool bRepErr = false;
            string sRouteHash = "";
            decimal dRouteHash = 0;



            try
            {
                
                // init SQL connection
                SqlDataReader sqlDR;
                SqlConnection sqlConn = new SqlConnection(cfg_SQL360UtilConnStr);
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConn;
                sqlConn.Open();

                // get inspectors info from FieldRepPayroll table
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "sp_PayrollProc_ACHDetail";
                sqlCommand.Parameters.Clear();
                sqlCommand.Parameters.AddWithValue("@fieldrepid", sFieldRepID);

                //sqlCommand.CommandText = "SELECT fr.FieldRepNumber, ui.FirstName, ui.LastName, frp.BankAccountName, frp.PayrollNum, frp.RoutingNumber, frp.AccountNumber, fr.LoginID, frp.Checking ";
                //sqlCommand.CommandText += "FROM Sutton.dbo.FieldReps fr inner join Sutton.dbo.UserInfos ui on ui.UserID = fr.LoginID ";
                //sqlCommand.CommandText += "LEFT OUTER JOIN Sutton360Utils.dbo.FieldRepPayroll frp on frp.FieldRepID = fr.LoginID ";
                //sqlCommand.CommandText += "WHERE fr.LoginID = '" + sFieldRepID + "'";

                sqlDR = sqlCommand.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    while (sqlDR.Read())
                    {

                        try
                        {

                            bRepErr = false;

                            sFieldRepNum = sqlDR.GetSqlString(0).ToString();
                            sFieldRepFirstName = sqlDR.IsDBNull(1) ? "" : sqlDR.GetSqlString(1).ToString();
                            sFieldRepLastName = sqlDR.IsDBNull(2) ? "" : sqlDR.GetSqlString(2).ToString();
                            sFieldRepName = sFieldRepFirstName.Length > 0 ? sFieldRepFirstName + " " : "";
                            sFieldRepName += sFieldRepLastName;
                            sAcntName = sqlDR.IsDBNull(3) ? "" : sqlDR.GetSqlString(3).ToString();

                            // if account name is blank use reps name
                            if (sAcntName.Length == 0)
                            {
                                sAcntName = sFieldRepName;
                            }

                            // Account name cannot be blank
                            if (sAcntName.Length == 0)
                            {
                                msExceptionList += "Account name is blank for " + sFieldRepNum + "\r\n";
                                bRepErr = true;
                            }

                            sAcntCode = sqlDR.IsDBNull(4) ? "" : sqlDR.GetSqlString(4).ToString();

                            sRouteNum = sqlDR.IsDBNull(5) ? "" : sqlDR.GetSqlString(5).ToString();

                            // Routing number missing or less than 9 characters
                            if (sRouteNum.Length < 9)
                            {
                                msExceptionList += "Routing number missing or less than 9 characters for " + sFieldRepNum + "\r\n";
                                bRepErr = true;
                            }

                            // Routing number contains spaces
                            if (sRouteNum.IndexOf(" ") != -1)
                            {
                                msExceptionList += "Routing number contains spaces for " + sFieldRepNum + "\r\n";
                                bRepErr = true;
                            }


                            sAcntNum = sqlDR.IsDBNull(6) ? "" : sqlDR.GetSqlString(6).ToString();
                            // Account number cannot be blank
                            if (sAcntNum.Length == 0)
                            {
                                msExceptionList += "Account number missing for " + sFieldRepNum + "\r\n";
                                bRepErr = true;
                            }

                            // Account number contains spaces
                            if (sAcntNum.IndexOf(" ") != -1)
                            {
                                msExceptionList += "Account number contains spaces for " + sFieldRepNum + "\r\n";
                                bRepErr = true;
                            }

                            // Checking / savings
                            bIsChecking = sqlDR.GetBoolean(8);

                            // Remove decimal from pay
                            sPayAmount = RemoveDecimal(sPayAmount);
                            if (sPayAmount == "")
                            {
                                msExceptionList += "No decimal in pay for " + sFieldRepNum + "\r\n";
                                bRepErr = true;
                            }
                            else
                            {
                                sPayAmount = sPayAmount.PadLeft(10, Convert.ToChar("0"));
                            }

                            // Build detail line
                            if (!bRepErr)
                            {

                                sRetVal = achb.BuildDetailRecord(sRouteNum, sAcntNum, bIsChecking, sPayAmount, sAcntName);

                                // Calc routing number hash total
                                sRouteHash = sRouteNum.Substring(0, 8);
                                dRouteHash = Convert.ToDecimal(sRouteHash);
                                mdRouteHashTotal += dRouteHash;

                            }
                            

                        }
                        catch (Exception ex)
                        {
                            msExceptionList += "Error retrieving information for " + sFieldRepNum + "\r\n" + ex.Message;
                        }	//try


                    }	// while


                }	// has rows
                else
                {
                    msExceptionList += "No information found for " + sFieldRepNum + "\r\n";
                }

                sqlConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }

            return sRetVal;
        
        }


        private string RemoveDecimal(string sAmount)
        {

            // Remove decimal from pay
            int iPos = 0;
            string sWork1 = "";
            string sWork2 = "";

            iPos = sAmount.IndexOf(".");
            if (iPos > 0)
            {
                sWork1 = sAmount.Remove(iPos, sAmount.Length - iPos);
                sWork2 = sAmount.Remove(0, iPos + 1);
                sAmount = sWork1 + sWork2;
            }
            else
            {
                sAmount = "";
            }

            return sAmount;
        
        }


        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            //MembershipUser usr = Membership.GetUser("L5");
            //bool test = usr.IsApproved;
        }

        private void tsmiUploadACH_Click(object sender, EventArgs e)
        {
            frmUploadACH frm = new frmUploadACH();
            frm.ShowDialog();

        }

        private void tsmiExport_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            string sErrMsg = "";
            
            string sLineItem = "";

            string sFieldRepNumber = "";
            string sFieldRepName = "";
            string sFieldRepID = "";
            string sPRNum = "";
            decimal dPayAmount = 0;
            decimal dTotPay = 0;
            string sPayAmount = "";
            string sRefNum = mdPayDate.ToString("MMddyy");

            string sTransDesc = "";
           
            msExceptionList = "";


            try
            {

                // get data from grid
                ReadOnlyDataRowList dataRows = xGrid.GetSortedDataRows(true);
                if (dataRows.Count == 0)
                {
                    MessageBox.Show("No data to process.\r\n\r\nPay period must be loaded in order to create export file.", "Export Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Export File name
                string sExportFileName = cfg_ExportFilePath + "SUT" + mdPayDate.ToString("MMdd") + ".txt";

                // delete file if it exists
                FileInfo fiExport = new FileInfo(sExportFileName);
                if (fiExport.Exists)
                {
                    fiExport.Delete();
                }
                fiExport = null;

                // Open export file
                StreamWriter swExport = new StreamWriter(sExportFileName, false);
                
                // Detail records
                foreach (Xceed.Grid.DataRow dr in dataRows)
                {

                    sFieldRepNumber = dr.Cells["Insp"].Value.ToString();
                    sFieldRepID = dr.Cells["FieldRepID"].Value.ToString();
                    sFieldRepName = dr.Cells["InspName"].Value.ToString();
                    dPayAmount = (decimal)dr.Cells["TotalPay"].Value;
                    sPayAmount = String.Format("{0:0.00}", dPayAmount);
                    sPRNum = dr.Cells["PRNum"].Value.ToString();

                    if (dPayAmount > 0)
                    {

                        // sum payroll
                        dTotPay += dPayAmount;

                        //**** handle no payroll number ****
                        if (sPRNum.Length == 0)
                        {
                            sErrMsg += "No vendor number for inspector " + sFieldRepNumber + "\r\n";
                        }

                        sTransDesc = sFieldRepNumber + " " + sFieldRepName;
                        if (sTransDesc.Length > 25)
                        {
                            sTransDesc = sTransDesc.Substring(0, 25);
                        }

                        // enclose in quotes if comma 
                        if (sTransDesc.Contains(",") || sTransDesc.Contains(";"))
                        {
                            sTransDesc = '"' + sTransDesc + '"';
                        }


                        // Build detail record
                        // source code, batch number, reference number
                        sLineItem = "1,0," + sRefNum + "," + mdPayDate.ToString("yyMMdd") + "," + sPRNum.Trim() + "," + sTransDesc + "," + cfg_InspFeeAcnt + "," + sPayAmount + "," + "0";

                        swExport.WriteLine(sLineItem);
                        swExport.Flush();
                    }

                }

                // Offset transaction
                sPayAmount = "-"+ String.Format("{0:0.00}", dTotPay);

                // source code, batch number, reference number
                sLineItem = "1,0," + "," + mdPayDate.ToString("yyMMdd") + "," + ",Cash Offset," + cfg_CashAcnt + "," + sPayAmount + "," + "0";

                swExport.WriteLine(sLineItem);
                swExport.Flush();

                 swExport.Close();

                this.Cursor = Cursors.Default;

                if (sErrMsg.Length == 0)
                {
                    MessageBox.Show("Export file was created successfully\r\n\r\nFile name:  " + sExportFileName, "Export File Created Successfully", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
                }
                else
                {
                    // delete file if it exists
                    fiExport = new FileInfo(sExportFileName);
                    if (fiExport.Exists)
                    {
                        fiExport.Delete();
                    }
                    fiExport = null;

                    MessageBox.Show("Export file could not be created due to the folowing issues:\r\n\r\n" + sErrMsg, "Export File Error", MessageBoxButtons.OK, MessageBoxIcon.Error); ;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("The following error occurred while attempting to create the export file: \r\n\r\n" + ex.Message.ToString(), "Export File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }



        }

        private void tsmiAddInspToSIB_Click(object sender, EventArgs e)
        {

        }

        private void tsmiInspectorMenu_Click(object sender, EventArgs e)
        {

        }
    }
}
