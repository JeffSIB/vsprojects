﻿namespace PayrollDeposit
{
    partial class frmAdjustFees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbInspNum = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbInspName = new System.Windows.Forms.TextBox();
            this.tbTotalPay = new System.Windows.Forms.TextBox();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.bSubmit = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(58, 221);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inspector number";
            // 
            // tbInspNum
            // 
            this.tbInspNum.BackColor = System.Drawing.Color.White;
            this.tbInspNum.ForeColor = System.Drawing.Color.DimGray;
            this.tbInspNum.Location = new System.Drawing.Point(153, 218);
            this.tbInspNum.Name = "tbInspNum";
            this.tbInspNum.ReadOnly = true;
            this.tbInspNum.Size = new System.Drawing.Size(59, 20);
            this.tbInspNum.TabIndex = 1;
            this.tbInspNum.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(67, 247);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Inspector name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(28, 274);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Total fees for the period";
            // 
            // tbInspName
            // 
            this.tbInspName.BackColor = System.Drawing.Color.White;
            this.tbInspName.ForeColor = System.Drawing.Color.DimGray;
            this.tbInspName.Location = new System.Drawing.Point(153, 244);
            this.tbInspName.Name = "tbInspName";
            this.tbInspName.ReadOnly = true;
            this.tbInspName.Size = new System.Drawing.Size(325, 20);
            this.tbInspName.TabIndex = 7;
            this.tbInspName.TabStop = false;
            // 
            // tbTotalPay
            // 
            this.tbTotalPay.BackColor = System.Drawing.Color.White;
            this.tbTotalPay.ForeColor = System.Drawing.Color.Black;
            this.tbTotalPay.Location = new System.Drawing.Point(153, 271);
            this.tbTotalPay.Name = "tbTotalPay";
            this.tbTotalPay.Size = new System.Drawing.Size(145, 20);
            this.tbTotalPay.TabIndex = 10;
            // 
            // tbTitle
            // 
            this.tbTitle.BackColor = System.Drawing.Color.Maroon;
            this.tbTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTitle.ForeColor = System.Drawing.Color.White;
            this.tbTitle.Location = new System.Drawing.Point(0, 0);
            this.tbTitle.Margin = new System.Windows.Forms.Padding(5, 15, 5, 5);
            this.tbTitle.Multiline = true;
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.ReadOnly = true;
            this.tbTitle.Size = new System.Drawing.Size(511, 186);
            this.tbTitle.TabIndex = 12;
            this.tbTitle.TabStop = false;
            this.tbTitle.Text = "Inspector number and name must be changed in 360.";
            // 
            // bSubmit
            // 
            this.bSubmit.Location = new System.Drawing.Point(168, 315);
            this.bSubmit.Name = "bSubmit";
            this.bSubmit.Size = new System.Drawing.Size(75, 23);
            this.bSubmit.TabIndex = 13;
            this.bSubmit.Text = "Submit";
            this.bSubmit.UseVisualStyleBackColor = true;
            this.bSubmit.Click += new System.EventHandler(this.bSubmit_Click);
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(269, 315);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 14;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // frmAdjustFees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(511, 360);
            this.ControlBox = false;
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bSubmit);
            this.Controls.Add(this.tbTitle);
            this.Controls.Add(this.tbTotalPay);
            this.Controls.Add(this.tbInspName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbInspNum);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAdjustFees";
            this.Text = "Edit Inspector Fees";
            this.Load += new System.EventHandler(this.frmAdjustFees_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbInspNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbInspName;
        private System.Windows.Forms.TextBox tbTotalPay;
        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.Button bSubmit;
        private System.Windows.Forms.Button bCancel;
    }
}