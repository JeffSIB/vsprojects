﻿namespace PayrollDeposit
{
    partial class frmFieldRepEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbInspNum = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbInspName = new System.Windows.Forms.TextBox();
            this.tbBankAcntName = new System.Windows.Forms.TextBox();
            this.tbAcntCode = new System.Windows.Forms.TextBox();
            this.tbRouteNum = new System.Windows.Forms.TextBox();
            this.tbAcntNum = new System.Windows.Forms.TextBox();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.bSubmit = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.rbChecking = new System.Windows.Forms.RadioButton();
            this.rbSavings = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(61, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inspector number";
            // 
            // tbInspNum
            // 
            this.tbInspNum.BackColor = System.Drawing.Color.White;
            this.tbInspNum.ForeColor = System.Drawing.Color.DimGray;
            this.tbInspNum.Location = new System.Drawing.Point(156, 65);
            this.tbInspNum.Name = "tbInspNum";
            this.tbInspNum.ReadOnly = true;
            this.tbInspNum.Size = new System.Drawing.Size(59, 20);
            this.tbInspNum.TabIndex = 1;
            this.tbInspNum.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(70, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Inspector name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(31, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Name on bank account";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(25, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Internal accounting code";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(68, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Routing number";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 199);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Bank account number";
            // 
            // tbInspName
            // 
            this.tbInspName.BackColor = System.Drawing.Color.White;
            this.tbInspName.ForeColor = System.Drawing.Color.DimGray;
            this.tbInspName.Location = new System.Drawing.Point(156, 91);
            this.tbInspName.Name = "tbInspName";
            this.tbInspName.ReadOnly = true;
            this.tbInspName.Size = new System.Drawing.Size(325, 20);
            this.tbInspName.TabIndex = 7;
            this.tbInspName.TabStop = false;
            // 
            // tbBankAcntName
            // 
            this.tbBankAcntName.BackColor = System.Drawing.Color.White;
            this.tbBankAcntName.ForeColor = System.Drawing.Color.Black;
            this.tbBankAcntName.Location = new System.Drawing.Point(156, 144);
            this.tbBankAcntName.Name = "tbBankAcntName";
            this.tbBankAcntName.Size = new System.Drawing.Size(325, 20);
            this.tbBankAcntName.TabIndex = 9;
            // 
            // tbAcntCode
            // 
            this.tbAcntCode.BackColor = System.Drawing.Color.White;
            this.tbAcntCode.ForeColor = System.Drawing.Color.Black;
            this.tbAcntCode.Location = new System.Drawing.Point(156, 118);
            this.tbAcntCode.Name = "tbAcntCode";
            this.tbAcntCode.Size = new System.Drawing.Size(59, 20);
            this.tbAcntCode.TabIndex = 8;
            // 
            // tbRouteNum
            // 
            this.tbRouteNum.BackColor = System.Drawing.Color.White;
            this.tbRouteNum.ForeColor = System.Drawing.Color.Black;
            this.tbRouteNum.Location = new System.Drawing.Point(156, 170);
            this.tbRouteNum.Name = "tbRouteNum";
            this.tbRouteNum.Size = new System.Drawing.Size(145, 20);
            this.tbRouteNum.TabIndex = 10;
            // 
            // tbAcntNum
            // 
            this.tbAcntNum.BackColor = System.Drawing.Color.White;
            this.tbAcntNum.ForeColor = System.Drawing.Color.Black;
            this.tbAcntNum.Location = new System.Drawing.Point(156, 196);
            this.tbAcntNum.Name = "tbAcntNum";
            this.tbAcntNum.Size = new System.Drawing.Size(325, 20);
            this.tbAcntNum.TabIndex = 11;
            // 
            // tbTitle
            // 
            this.tbTitle.BackColor = System.Drawing.Color.White;
            this.tbTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbTitle.ForeColor = System.Drawing.Color.DimGray;
            this.tbTitle.Location = new System.Drawing.Point(0, 0);
            this.tbTitle.Margin = new System.Windows.Forms.Padding(5, 15, 5, 5);
            this.tbTitle.Multiline = true;
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.ReadOnly = true;
            this.tbTitle.Size = new System.Drawing.Size(511, 25);
            this.tbTitle.TabIndex = 12;
            this.tbTitle.TabStop = false;
            this.tbTitle.Text = "Inspector number and name must be changed in 360.";
            // 
            // bSubmit
            // 
            this.bSubmit.Location = new System.Drawing.Point(167, 267);
            this.bSubmit.Name = "bSubmit";
            this.bSubmit.Size = new System.Drawing.Size(75, 23);
            this.bSubmit.TabIndex = 13;
            this.bSubmit.Text = "Submit";
            this.bSubmit.UseVisualStyleBackColor = true;
            this.bSubmit.Click += new System.EventHandler(this.bSubmit_Click);
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(268, 267);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 14;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // rbChecking
            // 
            this.rbChecking.AutoSize = true;
            this.rbChecking.Location = new System.Drawing.Point(157, 222);
            this.rbChecking.Name = "rbChecking";
            this.rbChecking.Size = new System.Drawing.Size(70, 17);
            this.rbChecking.TabIndex = 15;
            this.rbChecking.TabStop = true;
            this.rbChecking.Text = "Checking";
            this.rbChecking.UseVisualStyleBackColor = true;
            // 
            // rbSavings
            // 
            this.rbSavings.AutoSize = true;
            this.rbSavings.Location = new System.Drawing.Point(247, 222);
            this.rbSavings.Name = "rbSavings";
            this.rbSavings.Size = new System.Drawing.Size(63, 17);
            this.rbSavings.TabIndex = 16;
            this.rbSavings.TabStop = true;
            this.rbSavings.Text = "Savings";
            this.rbSavings.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(81, 224);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Account type";
            // 
            // frmFieldRepEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(511, 313);
            this.ControlBox = false;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.rbSavings);
            this.Controls.Add(this.rbChecking);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bSubmit);
            this.Controls.Add(this.tbTitle);
            this.Controls.Add(this.tbAcntNum);
            this.Controls.Add(this.tbRouteNum);
            this.Controls.Add(this.tbAcntCode);
            this.Controls.Add(this.tbBankAcntName);
            this.Controls.Add(this.tbInspName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbInspNum);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFieldRepEdit";
            this.Text = "Edit Inspector Account Information";
            this.Load += new System.EventHandler(this.frmFieldRepEdit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbInspNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbInspName;
        private System.Windows.Forms.TextBox tbBankAcntName;
        private System.Windows.Forms.TextBox tbAcntCode;
        private System.Windows.Forms.TextBox tbRouteNum;
        private System.Windows.Forms.TextBox tbAcntNum;
        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.Button bSubmit;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.RadioButton rbChecking;
        private System.Windows.Forms.RadioButton rbSavings;
        private System.Windows.Forms.Label label7;
    }
}