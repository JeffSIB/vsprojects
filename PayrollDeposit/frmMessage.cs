﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace PayrollDeposit
{
    public partial class frmMessage : Form
    {
        StreamReader fileToPrint;
        Font printFont;

        public string MessageText { get; set; }

        public frmMessage()
        {
            InitializeComponent();
            tbText.GotFocus += delegate { tbText.Select(tbText.TextLength, 0); };

        }

        private void frmMessage_Load(object sender, EventArgs e)
        {
            tbText.Text = MessageText;
        }

        private void tsmiPrint_Click(object sender, EventArgs e)
        {
            string sTempDir = Environment.GetEnvironmentVariable("TEMP");

            StreamWriter fs;
            string sTempFile = sTempDir + "\\PR" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".txt";

            try
            {
                // Create the file.
                fs = File.CreateText(sTempFile);
                fs.Flush();
                fs.Close();
                fs = null;

                // Open for write
                fs = File.AppendText(sTempFile);

                fs.WriteLine("Inspector Fees");
                fs.WriteLine(" ");
                fs.WriteLine(tbText.Text);
                fs.WriteLine(" ");
                fs.WriteLine(" ");
                fs.WriteLine(DateTime.Now.ToString());

                fs.Flush();
                fs.Close();

                printDialog1.AllowSomePages = false;
                printDialog1.ShowHelp = false;
                printDialog1.Document = printDocument1;

                DialogResult result = printDialog1.ShowDialog();

                // If the result is OK then print the document.
                if (result == DialogResult.OK)
                {
                    fileToPrint = new System.IO.StreamReader(sTempFile);
                    printFont = new System.Drawing.Font("Courier New", 10);
                    printDocument1.Print();
                    fileToPrint.Close();
                }

            }


            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float yPos = 0f;
            int count = 0;
            float leftMargin = e.MarginBounds.Left;
            float topMargin = e.MarginBounds.Top;
            string line = null;
            float linesPerPage = e.MarginBounds.Height / printFont.GetHeight(e.Graphics);
            while (count < linesPerPage)
            {
                line = fileToPrint.ReadLine();
                if (line == null)
                {
                    break;
                }
                yPos = topMargin + count * printFont.GetHeight(e.Graphics);
                e.Graphics.DrawString(line, printFont, Brushes.Black, leftMargin, yPos, new StringFormat());
                count++;
            }
            if (line != null)
            {
                e.HasMorePages = true;
            }


        }
    }
}
