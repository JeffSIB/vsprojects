﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PayrollDeposit
{
    public partial class frmRebuildTables : Form
    {

        //private string cfg_SQL360ConnStr;
        private string cfg_SQL360UtilConnStr;
        private string cfg_SQLMainSIBIConnStr;


        public frmRebuildTables()
        {
            InitializeComponent();
        }

        private void frmRebuildTables_Load(object sender, EventArgs e)
        {
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            //cfg_SQL360ConnStr = colNameVal.Get("SQL360ConnStr");
            cfg_SQL360UtilConnStr = colNameVal.Get("SQL360UtilConnStr");
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");

            bClose.Visible = false;

            rebuildTables();

        }

        private void rebuildTables()
        {

            try
            {

                tbStatus.Text = "Rebuilding tables - please wait...";
                this.Refresh();
                this.Cursor = Cursors.WaitCursor;

                // init SQL connection
                SqlDataReader sqlDR;
                SqlConnection sqlConn = new SqlConnection(cfg_SQL360UtilConnStr);
                SqlConnection sqlConn2 = new SqlConnection(cfg_SQL360UtilConnStr);
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConn;
                sqlConn.Open();

                SqlCommand sqlCommand2 = new SqlCommand();
                sqlCommand2.CommandType = CommandType.Text;
                sqlCommand2.Connection = sqlConn2;
                sqlConn2.Open();

                object oRetVal;
                string sFieldRepID = "";
                string sFieldRepNumber = "";
                string sFieldRepFirstName = "";
                string sFieldRepLastName = "";
                string sFieldRepName = "";
                int iRecAdded = 0;

                // get all rows from 360 FieldReps table
                sqlCommand.CommandType = CommandType.Text;
                //sqlCommand.CommandText = "sp_Payroll_ByInsp_ByPeriod";
                //sqlCommand.Parameters.Clear();
                //sqlCommand.Parameters.AddWithValue("@begdate", dPayDate);
                //sqlCommand.Parameters.AddWithValue("@enddate", dPayDatePlus1);

                sqlCommand.CommandText = "SELECT fr.LoginID, fr.FieldRepNumber, ui.FirstName, ui.LastName ";
                sqlCommand.CommandText += "FROM Sutton.dbo.FieldReps fr inner join Sutton.dbo.UserInfos ui on ui.UserID = fr.LoginID ";
                sqlCommand.CommandText += "ORDER BY fr.FieldRepNumber";

                sqlDR = sqlCommand.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        try
                        {
                            //LoginID from 360 FieldRep table
                            sFieldRepID = sqlDR.GetSqlGuid(0).ToString();

                            sFieldRepNumber = sqlDR.GetSqlString(1).ToString();
                            sFieldRepFirstName = sqlDR.IsDBNull(2) ? "" : sqlDR.GetSqlString(2).ToString();
                            sFieldRepLastName = sqlDR.IsDBNull(3) ? "" : sqlDR.GetSqlString(3).ToString();
                            sFieldRepName = sFieldRepFirstName.Length > 0 ? sFieldRepFirstName + " " : "";
                            sFieldRepName += sFieldRepLastName;

                            // Does it exist in FieldRepPayroll table
                            sqlCommand2.CommandText = "SELECT frp.FieldRepID ";
                            sqlCommand2.CommandText += "FROM Sutton360Utils.dbo.FieldRepPayroll frp ";
                            sqlCommand2.CommandText += "WHERE frp.FieldRepID = '" + sFieldRepID + "'";

                            oRetVal = sqlCommand2.ExecuteScalar();

                            if (oRetVal == null)
                            {

                                sqlCommand2.CommandText = "INSERT INTO Sutton360Utils.dbo.FieldRepPayroll ";
                                sqlCommand2.CommandText += "([FieldRepID]) VALUES ('" + sFieldRepID + "')";
                                oRetVal = sqlCommand2.ExecuteNonQuery();

                                // add row to FieldRepPayroll table
                                tbResults.Text += "Creating entry for " + sFieldRepNumber + " - " + sFieldRepName + System.Environment.NewLine;
                                iRecAdded++;

                            }


                        }   //try

                        finally
                        {

                        }


                    }	// while

                    if (iRecAdded > 0)
                    {
                        tbResults.Text += "Process complete - " + iRecAdded.ToString() + " records added." + System.Environment.NewLine;
                    }
                    else
                    {
                        tbResults.Text += "Process complete - no adjustments required." + System.Environment.NewLine;
                    }


                }	// has rows
                else
                {

                    throw new SystemException("No 360 inspector records located.");

                    //MessageBox.Show("There were no items found for the payroll date selected.\r\nEither you have selected an incorrect date or payroll has not been set for the period.", "No payroll data found for period", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

                sqlConn.Close();

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                tbResults.Text += "Process ended with the following error:" + System.Environment.NewLine;
                tbResults.Text += ex.ToString() + System.Environment.NewLine;
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {
                bClose.Visible = true;
                this.Cursor = Cursors.Default;
                tbStatus.Text = "Process complete";
            }
        
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
