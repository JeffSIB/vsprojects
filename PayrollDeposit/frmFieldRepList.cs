﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Xceed.Grid;
using Xceed.Grid.Reporting;
using Xceed.Grid.Editors;
using Xceed.Grid.Viewers;
using Xceed.Grid.Collections;
using System.Data.SqlClient;

namespace PayrollDeposit
{
    public partial class frmFieldRepList : Form
    {

        //private string cfg_SQL360ConnStr;
        private string cfg_SQL360UtilConnStr;
        private string cfg_SQLMainSIBIConnStr;
        private string rptTitle = "";
        private Xceed.Grid.GridControl xGrid = new GridControl();
        ColumnManagerRow xGridColumnManagerRow;
        private string msGridSortCol;
        private bool mbGridSortDir;


        public frmFieldRepList()
        {
            InitializeComponent();
        }

        private void FieldRepMaint_Load(object sender, EventArgs e)
        {
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            //cfg_SQL360ConnStr = colNameVal.Get("SQL360ConnStr");
            cfg_SQL360UtilConnStr = colNameVal.Get("SQL360UtilConnStr");
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");


            // place grid on panel
            this.pnlMain.Controls.Add(xGrid);

            // grid sort order
            msGridSortCol = "";
            mbGridSortDir = true;

            xGrid.Clear();
            this.PrepareGrid();

        }

        private void PrepareGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xGrid.Clear();

                // header row
                xGridColumnManagerRow = new ColumnManagerRow();
                xGrid.FixedHeaderRows.Add(xGridColumnManagerRow);

                //COLUMNS
                xGrid.Columns.Add(new Column("InspNum", typeof(string)));
                xGrid.Columns["InspNum"].Title = "Insp";
                xGrid.Columns["InspNum"].Width = 50;
                xGrid.Columns["InspNum"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["InspNum"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["InspNum"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("InspName", typeof(string)));
                xGrid.Columns["InspName"].Title = "Inspector name";
                xGrid.Columns["InspName"].Width = 170;
                xGrid.Columns["InspName"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["InspName"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["InspName"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("InspAcntName", typeof(string)));
                xGrid.Columns["InspAcntName"].Title = "Bank account name";
                xGrid.Columns["InspAcntName"].Width = 200;
                xGrid.Columns["InspAcntName"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["InspAcntName"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["InspAcntName"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("InspAcntCode", typeof(string)));
                xGrid.Columns["InspAcntCode"].Title = "Code";
                xGrid.Columns["InspAcntCode"].Width = 50;
                xGrid.Columns["InspAcntCode"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["InspAcntCode"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["InspAcntCode"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("RouteNum", typeof(string)));
                xGrid.Columns["RouteNum"].Title = "Routing";
                xGrid.Columns["RouteNum"].Width = 120;
                xGrid.Columns["RouteNum"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["RouteNum"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["RouteNum"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("AcntNum", typeof(string)));
                xGrid.Columns["AcntNum"].Title = "Account";
                xGrid.Columns["AcntNum"].Width = 120;
                xGrid.Columns["AcntNum"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["AcntNum"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["AcntNum"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("FieldRepID", typeof(string)));
                xGrid.Columns["FieldRepID"].Visible = false;
                xGrid.Columns["FieldRepID"].Width = 0;

                
                //grid wide settings
                xGrid.ReadOnly = true;
                xGrid.Dock = DockStyle.Fill;
                xGrid.FixedColumnSplitter.Visible = false;
                xGrid.RowSelectorPane.Visible = false;
                xGrid.KeyDown += new KeyEventHandler(xGrid_KeyDown);

                // prevent cell navigation
                xGrid.AllowCellNavigation = false;

                // resize
                xGrid.Resize += new System.EventHandler(xGrid_Resize);

                // Sort
                xGrid.SortedColumnsChanged += new EventHandler(xGrid_SortedColumnsChanged);

                //rptTitle = "Pay period ending " + dPayDate.Month.ToString() + "/" + dPayDate.Day.ToString() + "/" + dPayDate.Year.ToString();
                //this.Text = "Inspector payroll - Pay period ending " + dPayDate.Month.ToString() + "/" + dPayDate.Day.ToString() + "/" + dPayDate.Year.ToString();

                string sFieldRepNumber = "";
                string sFieldRepID = "";
                string sFieldRepFirstName = "";
                string sFieldRepLastName = "";
                string sFieldRepName = "";
                string sAcntName = "";
                string sAcntCode = "";
                string sRouteNum = "";
                string sAcntNum = "";

                int iInspCount = 0;

                // init SQL connection
                SqlDataReader sqlDR;
                SqlConnection sqlConn = new SqlConnection(cfg_SQL360UtilConnStr);
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConn;
                sqlConn.Open();

                // get all active inspectors
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Clear();
                sqlCommand.CommandText = "sp_PayrollProc_GetInspInfo";

                //sqlCommand.CommandText = "SELECT fr.FieldRepNumber, ui.FirstName, ui.LastName, frp.BankAccountName, frp.PayrollNum, frp.RoutingNumber, frp.AccountNumber, fr.LoginID ";
                //sqlCommand.CommandText += "FROM Sutton.dbo.FieldReps fr inner join Sutton.dbo.UserInfos ui on ui.UserID = fr.LoginID ";
                //sqlCommand.CommandText += "LEFT OUTER JOIN Sutton360Utils.dbo.FieldRepPayroll frp on frp.FieldRepID = fr.LoginID ";
                //sqlCommand.CommandText += "ORDER BY fr.FieldRepNumber";

                sqlDR = sqlCommand.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {
                    // add row to grid
                    xGrid.BeginInit();

                    // loop through active inspectors
                    while (sqlDR.Read())
                    {

                        try
                        {

                            //LoginID from 360 FieldRep table
                            //sFieldRepID = sqlDR.GetSqlGuid(0).ToString();

                            sFieldRepNumber = sqlDR.GetSqlString(0).ToString();
                            sFieldRepFirstName = sqlDR.IsDBNull(1) ? "" : sqlDR.GetSqlString(1).ToString();
                            sFieldRepLastName = sqlDR.IsDBNull(2) ? "" : sqlDR.GetSqlString(2).ToString();
                            sFieldRepName = sFieldRepFirstName.Length > 0 ? sFieldRepFirstName + " " : "";
                            sFieldRepName += sFieldRepLastName;
                            sAcntName = sqlDR.IsDBNull(3) ? "" : sqlDR.GetSqlString(3).ToString();
                            sAcntCode = sqlDR.IsDBNull(4) ? "" : sqlDR.GetSqlString(4).ToString();
                            sRouteNum = sqlDR.IsDBNull(5) ? "" : sqlDR.GetSqlString(5).ToString();
                            sAcntNum = sqlDR.IsDBNull(6) ? "" : sqlDR.GetSqlString(6).ToString();
                            sFieldRepID = sqlDR.GetSqlGuid(7).ToString();

                            iInspCount += 1;

                            Xceed.Grid.DataRow dataRow = xGrid.DataRows.AddNew();
                            dataRow.Cells["InspNum"].Value = sFieldRepNumber;
                            dataRow.Cells["InspName"].Value = sFieldRepName;
                            dataRow.Cells["InspAcntName"].Value = sAcntName;
                            dataRow.Cells["InspAcntCode"].Value = sAcntCode;
                            dataRow.Cells["RouteNum"].Value = sRouteNum;
                            dataRow.Cells["AcntNum"].Value = sAcntNum;
                            dataRow.Cells["FieldRepID"].Value = sFieldRepID;

                            dataRow.EndEdit();

                        }
                        finally
                        {
                            xGrid.EndInit();

                        }	//try


                    }	// while

                    xGrid_Resize(null, null);

                }	// has rows

                sqlConn.Close();

                // if there is no sorted column - sort by Insp num
                if (msGridSortCol == "")
                {
                    xGrid.Columns["InspNum"].SortDirection = SortDirection.Ascending;
                    msGridSortCol = "InspNum";
                    mbGridSortDir = true;
                }
                else
                {
                    if (mbGridSortDir)
                        xGrid.Columns[msGridSortCol].SortDirection = SortDirection.Ascending;
                    else
                        xGrid.Columns[msGridSortCol].SortDirection = SortDirection.Descending;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void xGrid_cell_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left) && (e.Clicks >= 2))
                this.xGrid_editRow(((Cell)sender).ParentRow);
        }

        private void xGrid_editRow(CellRow folderRow)
        {

            frmFieldRepEdit frm = new frmFieldRepEdit();
            frm.FieldRepID = folderRow.Cells["FieldRepID"].Value.ToString();
            frm.FielRepNumber = folderRow.Cells["InspNum"].Value.ToString();
            frm.FieldRepName = folderRow.Cells["InspName"].Value.ToString();
            frm.ShowDialog();

        }
        private void xGrid_KeyDown(object sender, KeyEventArgs e)
        ///<summary>
        ///
        {
            switch (e.KeyData)
            {
                case Keys.Enter:
                    {
                        CellRow currentRow = xGrid.CurrentRow as CellRow;

                        if (currentRow != null)
                            this.xGrid_editRow(currentRow);
                        break;
                    }

                case Keys.Back:
                    //this.NavigateUp();
                    break;
            }
        }

        private void xGrid_SortedColumnsChanged(object sender, System.EventArgs e)
        {

            // preserve existing sort order
            SortedColumnList sclxTemp = xGrid.SortedColumns;

            foreach (Xceed.Grid.Column column in sclxTemp)
            {
                if (column.SortIndex == 0)
                {
                    msGridSortCol = column.FieldName.ToString();
                    mbGridSortDir = (column.SortDirection == SortDirection.Ascending);
                }

            }
        }

        // Grid resize
        private void xGrid_Resize(object sender, System.EventArgs e)
        {
            int iAvailWidth = xGrid.DisplayRectangle.Width;
            int iTotColWidth = 0;
            int iResizeCol = 2;
            int iAdjustment = 0;

            // minimized
            if (iAvailWidth == 0)
                return;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xGrid.Columns)
            {
                if (column.Index != iResizeCol)
                {
                    Console.WriteLine(column.FieldName);
                    iTotColWidth += column.Width;
                }

            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.Width = iWidth;
            }
            else
            {
                xResizeColumn.Width = iWidth;
            }
        }

        // resize grid for print
        private void xGrid_ResizeForPrint(int iAvailWidth)
        {
            int iTotColWidth = 0;
            int iResizeCol = 2;
            int iAdjustment = 10;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xGrid.Columns)
            {
                if (column.Index != iResizeCol)
                    iTotColWidth += column.Width;
            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
            else
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
        }

        private void tsmiFile_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsmiRefreshGrid_Click(object sender, EventArgs e)
        {
            xGrid.Clear();
            this.PrepareGrid();
        }

        private void tsmiPrintInspectorList_Click(object sender, EventArgs e)
        {
            this.xGrid.ReportSettings.Title = rptTitle;
            this.xGrid.ReportSettings.Landscape = true;

            xGrid_ResizeForPrint(850);
            
            // base font for the report
            Font font = new Font("Arial", 9);
            this.xGrid.ReportStyle.Font = font;

            Report report = new Report(xGrid);
            ReportStyleSheet reportStyleSheet = report.ReportStyleSheet;
            
            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            reportStyleSheet.Grid.DataRows[0].BackColor = Color.FromArgb(205, 255, 205);
            reportStyleSheet.Grid.DataRows[1].BackColor = Color.Transparent;

            report.ReportStyleSheet.PageHeader.BottomBorder.Thickness = ReportLineThickness.SingleThin;
            report.ReportStyleSheet.PageHeader.BottomMargin = 5;
            report.ReportStyleSheet.PageHeader.Font = new Font(xGrid.ReportStyle.Font.FontFamily, 10, FontStyle.Bold);

            this.xGridColumnManagerRow.ReportStyle.RepeatOnEachPage = true;
            this.xGridColumnManagerRow.ReportStyle.BackColor = Color.DarkGray;
            this.xGridColumnManagerRow.ReportStyle.ForeColor = Color.White;
            this.xGridColumnManagerRow.ReportStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            this.xGridColumnManagerRow.ReportStyle.WordWrap = true;
            this.xGridColumnManagerRow.ReportStyle.RowHeightExpansion = RowHeightExpansion.ExpandWithoutLimits;
            this.xGridColumnManagerRow.ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            this.xGridColumnManagerRow.ReportStyle.VerticalAlignment = VerticalAlignment.Bottom;

            report.ReportStyleSheet.PageHeader.LeftElement.TextFormat = "Inspector Bank Account Information" + Environment.NewLine;
            report.ReportStyleSheet.PageHeader.RightElement.TextFormat =
              "Page : %Page format=000%" + Environment.NewLine +
              "Date : %DateTime format=g%";

            report.ReportStyleSheet.PageFooter.Font = new Font(xGrid.ReportStyle.Font.FontFamily, 10, FontStyle.Bold);
            report.ReportStyleSheet.PageFooter.TopBorder.Thickness = ReportLineThickness.SingleThin;
            //report.ReportStyleSheet.PageFooter.LeftElement.TextFormat = miInspCount.ToString() + " inspectors.";
            //report.ReportStyleSheet.PageFooter.RightElement.TextFormat = "Total payroll: " + mdTotalPRAmt.ToString("c2");
            report.PrintPreview();
        }

    }
}
