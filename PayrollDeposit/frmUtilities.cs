﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PayrollDeposit
{
    public partial class frmUtilities : Form
    {
        public frmUtilities()
        {
            InitializeComponent();
        }

        private void bRebuildTables_Click(object sender, EventArgs e)
        {
            frmRebuildTables frmRBT = new frmRebuildTables();
            frmRBT.ShowDialog();
            
        }
    }
}
