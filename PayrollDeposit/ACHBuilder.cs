﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PayrollDeposit
{
    public class ACHBuilder
    {
                 
        public string BuildFileHeader()
        {
             
            // Build file header record
            //                      1         2         3         4         5
            //             12345678901234567890123456789012345678901234567890
            string sFHR = "101 0119002541592056754";

            // creation date
            string sFileCreationDate = DateTime.Now.ToString("yyMMddhhmm");
            sFHR += sFileCreationDate;

            // File ID modifier, record size, blocking factor, format code
            sFHR += "A094101";

            // Destination
            sFHR += "BANK OF AMERICA".PadRight(23);

            // Company name
            sFHR += "Sutton Inspection".PadRight(23);

            // Reference code (8)
            sFHR += "        ";

 
            return sFHR;

        }

        public string BuildCompanyBatchRecord(DateTime dPayDate)
        {

            //                      1         2         3         4         5
            //             12345678901234567890123456789012345678901234567890
            string sCBR = "5200Sutton Inspectio";

            // Company discretionary (20)
            sCBR += "Pay period: " + dPayDate.ToString("MM/dd/yy");

            // Company ID, entry class code
            sCBR += "1592056754CCD";

            // Printed on receivers transaction
            sCBR += "FEES".PadRight(10);

            // Date funds post to receivers account
            sCBR += dPayDate.ToString("yyMMdd") + dPayDate.ToString("yyMMdd"); 

            // status code, originting DFI
            sCBR += "   101190025";

            // batch number
            sCBR += "1".PadLeft(7, Convert.ToChar("0"));
 
            return sCBR;
        
        }

        public string BuildCompanyBatchTrailerRecord(string sDetailRecords, string sTotalPayroll, string sRouteHash)
        {


            //                      1         2         3         4         5
            //             12345678901234567890123456789012345678901234567890
            string sCBR = "8200";

            // Number of detail records
            sCBR += sDetailRecords;

            // Routing code hash
            sCBR += sRouteHash.Substring(sRouteHash.Length - 10);

            // Total Debit
            sCBR += "000000000000";

            // Total Credits
            sCBR += sTotalPayroll;

            // Company ID, entry class code
            sCBR += "1592056754";

            // 55-79
            string sFiller = new string(Convert.ToChar(" "), 25);
            sCBR += sFiller;

            // originting DFI
            sCBR += "01190025";                         

            // batch number
            sCBR += "1".PadLeft(7, Convert.ToChar("0"));
            
            return sCBR;



        }


        public string BuildFileControlRecord(string sDetailRecords, string sTotalPayroll, string sRouteHash, string sBlockCount)
        {


            //                      1         2         3         4         5
            //             12345678901234567890123456789012345678901234567890
            string sCBR = "9000001";

            // Block count
            sCBR += sBlockCount.PadLeft(6, Convert.ToChar("0"));

            // Number of detail records
            sCBR += sDetailRecords;

            // Routing code hash
            sCBR += sRouteHash.Substring(sRouteHash.Length - 10);

            // Total Debit
            sCBR += "000000000000";

            // Total Credits
            sCBR += sTotalPayroll;

            // 56-94
            string sFiller = new string(Convert.ToChar(" "), 39);
            sCBR += sFiller;

            // originting DFI

            return sCBR;

        }

        public string BuildDetailRecord(string sRouteNum, string sAcntNum, bool bChecking, string sPayAmount, string sBankAcntName)
        {


            //                      1         2         3         4         5
            //             12345678901234567890123456789012345678901234567890
            string sDR = "6";

            // Transaction code (checking / savings)
            if (bChecking)
                sDR += "22";
            else
                sDR += "32";

            // Routing num
            sDR += sRouteNum;

            // Account number
            sDR += sAcntNum.PadRight(17);

            // Pay amount
            sDR += sPayAmount;

            // Sutton ID
            sDR += "SIBFLA".PadRight(15);

            // Inspector bank account name
            if (sBankAcntName.Length > 22)
            {
                sBankAcntName = sBankAcntName.Substring(0, 22);
            }
            else
            {
                sBankAcntName = sBankAcntName.PadRight(22);
            }
            sDR += sBankAcntName;

            // BOA draft - not used plus 0 for no addenda
            sDR += "  0";

            // Trace number 80-87 + 7 0s
            sDR += "011900250000000";

            
            return sDR;

        }

        private string formatDollarAmount(string sAmount)
        {
            string sRetVal = "";






            return sRetVal;
        }


    }
}
