﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PayrollDeposit
{
    public partial class frmAdjustFees : Form
    {

        //private string cfg_SQL360UtilConnStr;
        private bool bNewInsp = false;
        public string FieldRepID { get; set; }
        public string FielRepNumber { get; set; }
        public string FieldRepName { get; set; }
        public string TotalInspPay { get; set; }
        public decimal decNewTotFees = 0;

        public frmAdjustFees()
        {
            InitializeComponent();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAdjustFees_Load(object sender, EventArgs e)
        {

            tbTitle.Text = "**** PLEASE READ THIS CAREFULLY ****\r\n\r\nYou may adjust an inspectors fees for the period by entering the desired amount below.\r\n\r\nThis adjustment will only be in effect for the life of the current grid. If you reload the period, the changes will be lost.\r\n\r\nYou must build the ACH File and Export File without reloading the period.";

            try
            {
                // strip $ from amount
                TotalInspPay = TotalInspPay.Replace("$", "");

                tbInspNum.Text = FielRepNumber;
                tbInspName.Text = FieldRepName;
                tbTotalPay.Text = TotalInspPay;
                
                decNewTotFees = Convert.ToDecimal(tbTotalPay.Text);                 
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
                if (bNewInsp)
                {
                    this.Close();
                }
            }

        }

        private void bSubmit_Click(object sender, EventArgs e)
        {

            try
            {
                decNewTotFees = Convert.ToDecimal(tbTotalPay.Text);                 
                this.Hide();
            }
            catch
            {
                MessageBox.Show("Amount entered for total fees is not a valid amount.\r\n\r\nVerify and/or reenter the amount.\r\n\r\nDo not enter a dollar sign ($)", "Total Fees Invalid", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            
            }

            //if (sRouteNum.Length > 0 && sRouteNum.Length != 9)
            //{
            //    MessageBox.Show("Routing number must be 9 digits","Inspector Information",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            //    return;
            //}

            //if (sRouteNum.IndexOf(" ") != -1)
            //{
            //    MessageBox.Show("Routing cannot contain spaces.", "Inspector Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}


            //if (sAcntNum.IndexOf(" ") != -1)
            //{
            //    MessageBox.Show("Account cannot contain spaces.", "Inspector Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}


        }

             
    }
}
