﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Configuration;
using Word = Microsoft.Office.Interop.Word;
using System.Text.RegularExpressions;

namespace SCUIBocaRecs
{



    public class Recs
    {
        private readonly string sRecText;
        public string RecText { get { return sRecText; } }

        private readonly Guid guRecID;
        public Guid RecID { get { return guRecID; } }

        private readonly string sRecType;
        public string RecType { get { return sRecType; } }

        public Recs(string RecText, Guid RecID, string RecType)
        {
            this.sRecText = RecText;
            this.guRecID = RecID;
            this.sRecType = RecType;
        }
    }

    public class Photos
    {

        private readonly Guid guRecID;
        public Guid RecID { get { return guRecID; } }

        private readonly Guid guPhotoID;
        public Guid PhotoID { get { return guPhotoID; } }

        public Photos(Guid RecID, Guid PhotoID)
        {
            this.guRecID = RecID;
            this.guPhotoID = PhotoID;
        }
    }
    
    class SCUIRecs
    {


        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
        static string cfg_outputdir = ConfigurationManager.AppSettings["OutputDir"];
        static string cfg_templatedir = ConfigurationManager.AppSettings["TemplateDir"];

        static List<Recs> LRecs;
        static List<Photos> LPhotos;

        static bool bGenRecs;
        static bool bSugRecs;

        static string sCaseNum;
        static string sInsured;
        static string sPolicy;
        static string sAdd1;
        static string sAdd2;
        static string sCSZ;
        static string sLocAdd1;
        static string sLocAdd2;
        static string sLocCSZ;
        static string sCaseType;
        static string sInsCo;
        static string sProducer;
        static string sProducerPhone;
        static string sCustNumber;
        static bool bErr;

        static void Main(string[] args)
        {

            string sCaseID = "";
            Guid guCaseID;
            sCaseNum = "";
            FileInfo fiRec;

            try
            {
                // get file name from command line
                sCaseID = args[0];

                // nothing passed
                if (sCaseID.Length == 0)
                {
                    // send email
                    throw new ApplicationException("No case id passed.");
                }

                // case # or GUID
                if (sCaseID.Length < 9)
                {
                    sCaseID = GetCaseID(sCaseID);
                    try
                    {
                        guCaseID = new Guid(sCaseID);
                    }
                    catch
                    {
                        throw new ApplicationException("Convert Case Number to case ID failed for case num: " + sCaseNum);
                    }
                }
                else
                {
                    guCaseID = new Guid(sCaseID);
                }
            }

            catch (Exception ex)
            {
                sendErrEmail("****SCUIRecs Error\r\n\r\n" + ex.Message);
                logError(ex.Message);
                return;
            }


            try

            {

                bErr = false;
                LRecs = new List<Recs>();
                LPhotos = new List<Photos>();

                // get case info
                if (!GetCaseInfo(guCaseID))
                {
                    bErr = true;
                    throw new ApplicationException("GetCaseInfo returned error.");
                }

                // Delete existing rec doc (could be removed during reopen)
                fiRec = new FileInfo(cfg_outputdir + sCaseNum + "Recs.docx");
                if (fiRec.Exists)
                {
                    fiRec.Delete();
                }
                
                // get recs
                bGenRecs = false;
                bSugRecs = false;
                int iGetRecs = GetRecs(guCaseID);

                // error
                if (iGetRecs < 0)
                {
                    bErr = true;
                    throw new ApplicationException("GetRecs returned error.");                
                }

                // no recs
                if (iGetRecs == 0)
                { 
                    logError("No recs for case: " + sCaseNum);                
                }
                // recs present - build doc
                else 
                {
                    // get photos
                    for (int i = 0; i < LRecs.Count; i++)
                    {
                        if (!GetPhotos(LRecs[i].RecID))
                        {
                            bErr = true;
                            throw new ApplicationException("GetPhotos returned error.");
                        }

                    }

                    // build doc
                    if (buildDoc(guCaseID))
                    {
                        logError("Rec doc created for: " + sCaseNum);
                    }
                    else
                    {
                        bErr = true;
                        logError("buildDoc returned error processing case - " + sCaseNum);
                        throw new ApplicationException(" buildDoc returned error processing case - " + sCaseNum);
                    }

                }
                Console.WriteLine("Done");

            }

            catch (Exception ex)
            {
                sendErrEmail("****SCUIRecs Error\r\n\r\n" + ex.Message);
                logError(ex.Message);
                return;
            }
        }

       

        static bool buildDoc(Guid guCaseID)
        {

            bool bRet = false;
            string sPhotoFile = "";

            object oMissing = System.Reflection.Missing.Value;
            object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

            //Start Word and create a new document.
            Word._Application oWord;
            Word._Document oDoc;
            oWord = new Word.Application();
            oWord.Visible = false;
            oDoc = oWord.Documents.Add(cfg_templatedir + "SCU7194RecDocTemplate2.dotx", ref oMissing,
                ref oMissing, ref oMissing);
            
            try
            {

                // Styles //
                ///////////
                // Header 
                Word.Style oPageHeaderStyle = oDoc.Styles.Add("PageHeader", Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph);
                oPageHeaderStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                oPageHeaderStyle.Font.Name = "Arial";
                oPageHeaderStyle.Font.Size = 18;
                oPageHeaderStyle.Font.Bold = 0;

                // Header text
                Word.Style oHeaderTextStyle = oDoc.Styles.Add("HeaderText", Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph);
                oHeaderTextStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oHeaderTextStyle.ParagraphFormat.SpaceAfter = 0;
                oHeaderTextStyle.Font.Name = "Arial";
                oHeaderTextStyle.Font.Size = 10;
                oHeaderTextStyle.Font.Bold = 0;

                // Rec Header text
                Word.Style oRecHeaderTextStyle = oDoc.Styles.Add("RecHeaderText", Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph);
                oRecHeaderTextStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oRecHeaderTextStyle.ParagraphFormat.SpaceAfter = 0;
                oRecHeaderTextStyle.Font.Name = "Arial";
                oRecHeaderTextStyle.Font.Size = 10;
                oRecHeaderTextStyle.Font.Bold = 1;

                // Body
                Word.Style oBodyTextStyle = oDoc.Styles.Add("BodyText", Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph);
                oBodyTextStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oBodyTextStyle.ParagraphFormat.SpaceAfter = 10;
                oBodyTextStyle.Font.Name = "Arial";
                oBodyTextStyle.Font.Size = 10;
                oBodyTextStyle.Font.Bold = 0;


                Word.Range oRng = oDoc.Range();


                // Name, address, etc.
                oRng.set_Style(oHeaderTextStyle);
                Word.Table oTbl = oDoc.Tables.Add(oRng, 13, 2,ref oMissing, ref oMissing);
                oTbl.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleNone;
                oTbl.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleNone;
                oTbl.Columns[1].Width = 60;
                oTbl.Columns[2].Width = 420;
                
                oTbl.Cell(1, 1).Range.Text = "Date: ";
                oTbl.Cell(1, 2).Range.Text = "";
                oTbl.Cell(1, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(1, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(2, 1).Range.Text = " ";
                oTbl.Cell(2, 2).Range.Text = " ";

                oTbl.Cell(3, 1).Range.Text = "To:";
                oTbl.Cell(3, 2).Range.Text = sInsured;
                oTbl.Cell(3, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(3, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(4, 1).Range.Text = " ";
                oTbl.Cell(4, 2).Range.Text = sAdd1;
                oTbl.Cell(4, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(4, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(5, 1).Range.Text = " ";
                oTbl.Cell(5, 2).Range.Text = sCSZ;
                oTbl.Cell(5, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(5, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(6, 1).Range.Text = " ";
                oTbl.Cell(6, 2).Range.Text = " ";

                oTbl.Cell(7, 1).Range.Text = "From:";
                oTbl.Cell(7, 2).Range.Text = sProducer;
                oTbl.Cell(7, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(7, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(8, 1).Range.Text = "Phone:";
                oTbl.Cell(8, 2).Range.Text = sProducerPhone;
                oTbl.Cell(8, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(8, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(9, 1).Range.Text = " ";
                oTbl.Cell(9, 2).Range.Text = " ";

                oTbl.Cell(10, 1).Range.Text = "Location:";
                oTbl.Cell(10, 2).Range.Text = sLocAdd1;
                oTbl.Cell(10, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(10, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(11, 1).Range.Text = " ";
                oTbl.Cell(11, 2).Range.Text = sLocCSZ;
                oTbl.Cell(11, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(11, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(12, 1).Range.Text = " ";
                oTbl.Cell(12, 2).Range.Text = " ";
                
                oTbl.Cell(13, 1).Range.Text = "RE:";
                oTbl.Cell(13, 2).Range.Text = sPolicy;
                oTbl.Cell(13, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(13, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oRng.InsertAfter(" ");
                oRng.InsertParagraphAfter();

                oRng.InsertAfter("Sutton Inspection Bureau completed a loss control survey of your company recently in order to provide an analysis of your operations. The purpose of the survey was to assess hazards and controls pertaining to your insurance policy. We appreciated your cooperation during the inspection visit.\v");
                oRng.InsertParagraphAfter();
                oRng.InsertAfter("Please respond in writing within 20 days of the date of this letter as to status of the recommendations listed. Failure to respond may put your insurance coverage at risk. Please use the space below to provide what actions have been taken for each recommendation listed.\v");
                oRng.InsertParagraphAfter();
                oRng.InsertAfter("Thank you again for your cooperation.\v\v");
                oRng.InsertParagraphAfter();
                oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);


                oRng.set_Style(oHeaderTextStyle);

                // Recs
                List<string> sRecTest = new List<string>();

                // Set format for numbered list 
                //oWord.ListGalleries[Word.WdListGalleryType.wdNumberGallery].ListTemplates[1].ListLevels[1].NumberFormat = "%1.)";

                object n = 1;
                Word.ListTemplate oLT = oWord.ListGalleries[Word.WdListGalleryType.wdNumberGallery].ListTemplates.get_Item(ref n);

                // Recs
                foreach (Recs sRec in LRecs)
                {
                    oRng.Font.Bold = 0;

                    oRng.ListFormat.ApplyListTemplateWithLevel(oLT, true, oMissing, oMissing, oMissing);

                    oRng.InsertBefore( "Action Taken:" + Convert.ToChar(11) + Convert.ToChar(11) + "________________________________________________________________ " + Convert.ToChar(11) + Convert.ToChar(11));
                    oRng.InsertBefore("  " + Convert.ToChar(11) + Convert.ToChar(11));

                    // photos
                    foreach (Photos sPhoto in LPhotos)
                    {
                        if (sPhoto.RecID == sRec.RecID)
                        {
                            //sPhotoFile = cfg_casefilesroot + guCaseID + @"\" + sPhoto.PhotoID + "-175.jpg";
                            sPhotoFile = cfg_casefilesroot + guCaseID + @"\" + sPhoto.PhotoID + ".jpg";
                            oRng.InlineShapes.AddPicture(sPhotoFile, oMissing, oMissing, oRng);
                        }
                    }
                    oRng.InsertBefore(sRec.RecText + Convert.ToChar(11) + Convert.ToChar(11));
                    oRng.InsertParagraphAfter();
                    oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);


                    oRng.ListFormat.RemoveNumbers(Word.WdNumberType.wdNumberParagraph);
                    oRng.InsertParagraphAfter();

                    oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);
                }



                // Footer
                Word.Paragraph oPara4;
                oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                oRng.set_Style(oBodyTextStyle);
                oPara4 = oDoc.Content.Paragraphs.Add(oRng);
                oPara4.Range.InsertParagraphBefore();

                oRng.set_Style(oBodyTextStyle);
                oPara4.Range.Text = " ";
                oPara4.Range.InsertParagraphAfter();
                oPara4.Range.InsertAfter("____________________________________________ _________________________");
                oPara4.Range.InsertParagraphAfter();
                oPara4.Range.InsertAfter("Print Name:                                                                       Date:");
                oPara4.Range.InsertParagraphAfter();
                oPara4.Range.InsertAfter("  ");
                oPara4.Range.InsertParagraphAfter();
                oPara4.Range.InsertAfter("____________________________________________ ");
                oPara4.Range.InsertParagraphAfter();
                oPara4.Range.InsertAfter("Signature");
                oPara4.Range.InsertParagraphAfter();

                oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);

                oWord.ActiveDocument.SaveAs2(cfg_outputdir + sCaseNum + ".doc",Word.WdSaveFormat.wdFormatDocument97);
                //oWord.ActiveDocument.SaveAs(cfg_outputdir + sCaseNum + "Recs.pdf",17);

                bRet = true;
            }
            catch (Exception ex)
            {

                //record exception  
                logError(ex.Message);

            }

            finally 
            {
                if (oDoc != null)
                {
                    oDoc.Close(false);
                    oDoc = null;
                }
                if (oWord != null)
                {
                    oWord.Quit();
                    oWord = null;
                }
            
            }

                return bRet;

        }

        static int GetRecs(Guid guCaseID)
        {

            int iRetVal = -1;

            string sRecText = "";
            string sRecType = "";
            Guid guRecGuid;

            //struRecs SRecs = new struRecs();

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetRecsForCase";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // Get all recs 
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {
                        // rec text
                        if (sqlReader.IsDBNull(0))
                        {
                            sRecText = "";
                        }
                        else
                        {
                            sRecText = sqlReader.GetSqlString(0).ToString();
                        }
                        sRecText = StripTagsCharArray(sRecText);

                        //rec guid
                        guRecGuid = (Guid)sqlReader.GetSqlGuid(7);

                        // rec type
                        if (sqlReader.IsDBNull(6))
                        {
                            sRecType = "General";
                        }
                        else
                        {
                            sRecType = sqlReader.GetSqlString(6).ToString();
                        }
                        
                        if (sRecType.ToUpper().Contains("SUGG"))
                        {
                            bSugRecs = true;
                        }
                        else
                        {
                            bGenRecs = true;
                        }
                        
                        Recs oRecs = new Recs(sRecText, guRecGuid,sRecType);
                        LRecs.Add(oRecs);

                    } while (sqlReader.Read());

                    sqlReader.Close();
                    iRetVal = 1;

                }
                else
                {
                    // no recs
                    iRetVal = 0;
                }
            }
            catch (Exception ex)
            {

                //record exception  
                sendErrEmail("**** SCUIRecs Error\r\n\r\n" + ex.Message);
                logError(ex.Message);

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();


            }

            return iRetVal;
        }


        static bool GetCaseInfo(Guid guCaseID)
        {

            bool bRetVal = false;

            string sCity = "";
            string sState = "";
            string sZip = "";
            sInsCo = "";


            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetCaseInfo";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        sCaseNum = sqlReader.GetSqlInt32(0).ToString();
                        sInsured = sqlReader.GetSqlString(2).ToString();
                        sPolicy = sqlReader.GetSqlString(3).ToString();
                        sAdd1 = sqlReader.GetSqlString(4).ToString();
                        if (!sqlReader.IsDBNull(5))
                        {
                            sAdd2 = sqlReader.GetSqlString(5).ToString();
                        }
                        sCity = sqlReader.GetSqlString(6).ToString();
                        sState = sqlReader.GetSqlString(7).ToString();
                        sZip = sqlReader.GetSqlString(8).ToString();
                        sCSZ = sCity + ", " + sState + " " + sZip;
                        sCaseType = sqlReader.GetSqlString(9).ToString();
                        sProducer = sqlReader.GetSqlString(10).ToString();
                        sProducerPhone = sqlReader.GetSqlString(12).ToString();
                        sCustNumber = sqlReader.GetSqlString(13).ToString().Trim();

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }

                sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                sqlCmd1.Parameters.AddWithValue("@fieldname", "Insurance Company");

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        if (!sqlReader.IsDBNull(0))
                        {
                            sInsCo = sqlReader.GetSqlString(0).ToString();
                        }

                    } while (sqlReader.Read());

                }
                sqlReader.Close();


                sqlCmd1.CommandText = "sp_GetLocationAddr";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        sLocAdd1 = sqlReader.GetSqlString(0).ToString();
                        sCity = sqlReader.GetSqlString(1).ToString();
                        sState = sqlReader.GetSqlString(2).ToString();
                        sZip = sqlReader.GetSqlString(3).ToString();
                        sLocCSZ = sCity + ", " + sState + " " + sZip;

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }


                bRetVal = true;
            }
            catch (Exception ex)
            {

                //record exception  
                logError(ex.Message);

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();


            }

            return bRetVal;
        }


        static string GetCaseID(string sCaseNum)
        {

            int iCaseNum = Convert.ToInt32(sCaseNum);
            string sCaseID = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetCaseGUID";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", iCaseNum);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        sCaseID = sqlReader.GetGuid(0).ToString();

                    } while (sqlReader.Read());

                    sqlReader.Close();
                }
            }
            catch (Exception ex)
            {

                //record exception  
                logError(ex.Message);

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

            return sCaseID;
        }

        static bool GetPhotos(Guid guRecID)
        {

            bool bRetVal = false;


            Guid guPhotoGuid;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetPhotosForRec";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@CaseFormRecID", guRecID);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // Get all photos for rec
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        //photo guid
                        guPhotoGuid = (Guid)sqlReader.GetSqlGuid(0);

                        Photos oPhoto = new Photos(guRecID, guPhotoGuid);
                        LPhotos.Add(oPhoto);

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }

                bRetVal = true;
            }
            catch (Exception ex)
            {

                //record exception  
                logError(ex.Message);

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();


            }

            return bRetVal;
        }

        static string StripTagsCharArray(string source)
        {

            string sRet = "";
            //char CrLf = char(13) + char(10);

            source = string.Join(" ", Regex.Split(source, @"(?:\r\n|\n|\r|\t)"));
            
            // replace known strings
            source = source.Replace("&nbsp;", " ");
            source = source.Replace("\\r;", "");
            source = source.Replace("\\n;", "");
            source = source.Replace("&amp;", "&");
            source = source.Replace("&ldquo;", "'");
            source = source.Replace("&rdquo;", "'");
            source = source.Replace("&lsquo;", "'");
            source = source.Replace("&rsquo;", "'");
            source = source.Replace("&ndash;", "-");
            source = source.Replace("&mdash;", "-");
            source = source.Replace("&quot;", "'");

            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }



            sRet = new string(array, 0, arrayIndex);

            // remove /r/n from front of string
            if (sRet.StartsWith("\r\n"))
            {
                sRet = sRet.Remove(0, 3);
            }

            // remove /r/n from front of string
            if (sRet.EndsWith("\r\n"))
            {
                sRet = sRet.Remove(sRet.Length-2, 2);
            }
            
            return sRet;
        }


        static void logError(string sErrText)
        {

            // record error message to log file
            LogUtils.LogUtils oLU;

            oLU = new LogUtils.LogUtils();

            oLU.logFileName = cfg_logfilename;

            oLU.OpenLog();
            oLU.WritetoLog(sErrText);

            oLU.closeLog();

        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "SCUI Rec Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import SCUI Rec Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }
    }
}
