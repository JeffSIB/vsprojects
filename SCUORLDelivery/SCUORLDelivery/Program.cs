﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using WinSCP;

namespace SCUORLDelivery
{
    class Program
    {

        static void Main(string[] args)
        {
            Setup setup = new Setup();
        }

        /// <summary>
        /// Instantiate properties from the AppSettings located in the App.config.
        /// </summary>
        public class Setup
        {

            private string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
            private string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
            private string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            private string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
            private string cfg_recroot = ConfigurationManager.AppSettings["RecRoot"];
            private string cfg_valuationroot = ConfigurationManager.AppSettings["ValuationRoot"];
            private string cfg_FTPRoot = ConfigurationManager.AppSettings["FTPRoot"];
            private string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
            private string cfg_exportapp = ConfigurationManager.AppSettings["ExportApp"];
            private string cfg_recapp = ConfigurationManager.AppSettings["RecApp"];
            private string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];

            private LogUtils.LogUtils oLU;
            private bool bErr = false;
            private string sPolicy = "";
            private string sAddLine1 = "";
            private string sInsured = "";
            private string sCaseNum = "";
            private Guid guCaseID;
            private string sIRFileNum = "";
            private string sIRUserID = "";
            private string sCustNum = "";
            private string sCustName = "";
            private string sAmount = "";
            private string sOutputFileName = "";
            private string sValuationOutputFileName = "";

            //Used for email message body.  
            private StringBuilder sbEmail = new StringBuilder();

            //Used for transmittal message body.  
            private StringBuilder sbmsgBody = new StringBuilder();



            public Setup()
            {
                int iCasesExported = 0;

                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                bool bExport = true;
                bool bCreateRecs = true;
                bool bFTPFiles = true;

                // build list of accounts that if ordered by, set IRUserID to "IR_Offshore"
                List<string> lOffshore = new List<string>();                
                lOffshore.Add("DCANTEROS");
                lOffshore.Add("RCAJONTOY");
                lOffshore.Add("RMARFIL");
                lOffshore.Add("EALVAREZ");
                lOffshore.Add("DLAYGO");
                lOffshore.Add("KDELEON");
                lOffshore.Add("KMARTINEZ");
                lOffshore.Add("CBARBADO");
                lOffshore.Add("MFERRER");
                lOffshore.Add("EESTILLORE");
                lOffshore.Add("KAVENA");
                lOffshore.Add("MFUGIAO");
                lOffshore.Add("BCALUPIG");
                lOffshore.Add("BBANATE");
                lOffshore.Add("GNAVARRO");
                lOffshore.Add("JMAGLAYA");
                lOffshore.Add("DSAQUISAME");
                lOffshore.Add("MMALOMA");
                lOffshore.Add("MTANZUACO");
                lOffshore.Add("ODONILA");
                lOffshore.Add("JCABITON");
                lOffshore.Add("DIGNACIO");
                lOffshore.Add("DDAZA");

                DirectoryInfo diPDF;
                DirectoryInfo diRec;
                DirectoryInfo diCaseFiles;

                FileInfo[] fiPDFFiles;
                FileInfo[] fiRECFiles;
                FileInfo fiRec;
                oLU.WritetoLog("Begin Processing  " + DateTime.Now.ToString());
                sbEmail.Append("Begin Processing  " + DateTime.Now.ToString() + "\r\n\r\n");

                //***********************************************************
                // Export PDF's from 360
                if (bExport)
                {
                    try
                    {

                        // Delete all files in PDF Export directory.
                        string[] sFiles = Directory.GetFiles(cfg_pdfroot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);

                        // Export PDF's from 360
                        oLU.WritetoLog(" Begin exporting completed cases from 360");
                        sbEmail.Append("---- Begin exporting completed cases from 360\r\n\r\n");
                        oLU.closeLog();

                        oLU.OpenLog();
                        oLU.WritetoLog(cfg_exportapp);

                        var proc = Process.Start(cfg_exportapp);
                        proc.WaitForExit();
                        var exitCode = proc.ExitCode;
                        sbEmail.Append("Exporter returned: " + exitCode.ToString() + "\r\n");

                        // exit code should be the number of cases exported
                        iCasesExported = (int)exitCode;

                        Console.WriteLine(exitCode.ToString());

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR IN EXPORT:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                        oLU.closeLog();

                        oLU.OpenLog();

                    }

                    diPDF = new DirectoryInfo(cfg_pdfroot);
                    fiPDFFiles = diPDF.GetFiles("*.pdf");

                    if (fiPDFFiles.Count() != iCasesExported)
                    {
                        sbEmail.Append("**** PDF count does not match number exported ****\r\n");
                        sbEmail.Append("Exported: " + iCasesExported.ToString() + " - In folder: " + fiPDFFiles.Count().ToString() + "\r\n\r\n");
                    }

                }

                if (bCreateRecs && !bErr)
                {

                    //***********************************************************
                    // Build Rec docs for all PDF files that were extracted


                    //************************************************
                    // Test for connection to casefiles
                    //************************************************
                    try
                    {
                        diCaseFiles = new DirectoryInfo(cfg_casefilesroot);
                        if (!diCaseFiles.Exists)
                            throw new ApplicationException("Unable to access case files root: " + cfg_casefilesroot + "\r\n");

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR ACCESSING CASEFILES FOLDER:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                    }

                    if (!bErr)
                    {

                        try
                        {

                            // Delete all files in Rec folder.
                            string[] sFiles = Directory.GetFiles(cfg_recroot);
                            foreach (string sFile in sFiles)
                                File.Delete(sFile);

                            sbEmail.Append("\r\n\r\n---- Begin creating REC Docs\r\n");

                            diPDF = new DirectoryInfo(cfg_pdfroot);
                            fiPDFFiles = diPDF.GetFiles("*.pdf");

                            foreach (FileInfo file in fiPDFFiles)
                            {

                                sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));

                                ProcessStartInfo psi = new ProcessStartInfo();
                                psi.FileName = cfg_recapp;
                                psi.Arguments = sCaseNum;

                                var proc1 = Process.Start(psi);
                                proc1.WaitForExit();
                                var exitCode1 = proc1.ExitCode;
                                Console.WriteLine(exitCode1.ToString());

                                // verify rec doc was created
                                fiRec = new FileInfo(cfg_recroot + sCaseNum + ".docx");
                                if (fiRec.Exists)
                                {
                                    // success
                                    sbEmail.Append("Rec doc created for: " + sCaseNum + "\r\n");
                                }
                                else
                                {
                                    // fail
                                    sbEmail.Append("No rec doc for : " + sCaseNum + "\r\n");
                                }
                                fiRec = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            sbEmail.Append("**** Error creating recs:\r\n" + ex.Message);
                            oLU.WritetoLog(sbEmail.ToString());
                            bErr = true;
                        }

                    }
                }

                //***********************************************************
                // rename PDF's, valuations & Recs to ImageRight file spec
                // create transmittal email and send if FTP is successful
                // copy to FTP folder based on account
                if (bFTPFiles && !bErr)
                {

                    // Delete all files in FTP folder.
                    string[] sFiles = Directory.GetFiles(cfg_FTPRoot + @"Transportation");
                    foreach (string sFile in sFiles)
                        File.Delete(sFile);

                    sFiles = Directory.GetFiles(cfg_FTPRoot + @"Underwriting");
                    foreach (string sFile in sFiles)
                        File.Delete(sFile);


                    sbEmail.Append("\r\n\r\n---- Begin renaming reports\r\n");

                    int iUnique = 1;
                    bool bValuation = false;

                    ///////////////////////////////////
                    // PDF's & Valuations
                    // Look for valuation pdf on J: drive. Rename and copy to FTP if present
                    ///////////////////////////////////
                    string sInvalidChars = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                    string sBucket = "";

                    try
                    {

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        // for each PDF 
                        foreach (FileInfo pdfFile in fiPDFFiles)
                        {

                            // Get case # from file name
                            sCaseNum = pdfFile.Name.Substring(0, pdfFile.Name.IndexOf("."));
                            sbEmail.Append("\r\nCase #: " + sCaseNum + "\r\n");

                            // Check for valuation in j:\360valuations - format casenumber.pdf
                            bValuation = false;
                            sValuationOutputFileName = "";
                            if (File.Exists(cfg_valuationroot + sCaseNum + ".pdf"))
                            { 
                                bValuation = true;
                            }
                            sbEmail.Append("    Valuation found: " + bValuation.ToString() + "\r\n");

                            // Get CaseID & customer number
                            if (!GetCaseInfo(sCaseNum))
                            {
                                sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("GetInfo failed for case: " + sCaseNum);
                            }

                            if (sIRFileNum == "")
                            {
                                sIRFileNum = "99999999";
                            }

                            sbEmail.Append("    Original sIRUSerid: " + sIRUserID + "\r\n");


                            // If sIRUserID (left of person ordering email) is in list, change to "IR_Offshore"
                            if (lOffshore.IndexOf(sIRUserID.ToUpper()) != -1)
                            {
                                sIRUserID = "IR_Offshore";
                            }

                            sbEmail.Append("    Translated sIRUSerid: " + sIRUserID + "\r\n");

                            // Build Report file name
                            //sOutputFileName = sIRDrawer_msIRFileType_sIRFile_msIRPageDesc(address)_sIRUID_iUnique.pdf
                            if (sCustNum == "7338")   //Orlando Transportation
                            {
                                sOutputFileName = @"SCU Transp Orlando_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + sIRUserID + "_@_";

                                if (bValuation)
                                {
                                    sValuationOutputFileName = @"SCU Transp Orlando_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "-Valuation_@_" + sIRUserID + "_@_1.pdf";
                                }

                                sBucket = @"Transportation\";

                            }
                            else if (sCustNum == "7026" || sCustNum == "7004")    //Orlando - Personal & Commercial
                            {
                                sOutputFileName = @"SCU Orlando_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + sIRUserID + "_@_";

                                if (bValuation)
                                {
                                    sValuationOutputFileName = @"SCU Orlando_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "-Valuation_@_" + sIRUserID + "_@_1.pdf";
                                }

                                sBucket = @"Underwriting\";
                            }
                            else
                            {
                                throw new Exception("Invalid Customer Number " + sCaseNum);
                            }

                            // If it exists - add digit to end
                            if (File.Exists(cfg_FTPRoot + sBucket + sOutputFileName + iUnique.ToString() + ".pdf"))
                            {

                                iUnique = 2;
                                while (true)
                                {

                                    if (File.Exists(cfg_FTPRoot + sBucket + sOutputFileName + iUnique.ToString() + ".pdf"))
                                    {
                                        iUnique++;
                                        if (iUnique > 10)
                                        {
                                            throw new ApplicationException(sOutputFileName + iUnique.ToString() + ".pdf" + " could not be saved - exists.");
                                        }

                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }

                            // replace any illegal characters with "-"

                            foreach (char c in sInvalidChars)
                            {
                                sOutputFileName = sOutputFileName.Replace(c, '-');
                            }

                            // Copy PDF to FTP folder under new name
                            pdfFile.CopyTo(cfg_FTPRoot + sBucket + sOutputFileName + iUnique.ToString() + ".pdf");
                            sbEmail.Append("    PDF Copied: " + sOutputFileName + iUnique.ToString() + ".pdf" + "\r\n");


                            // Copy valuation to FTP folder under new name if present
                            if (bValuation)
                            {
                                File.Copy(cfg_valuationroot + sCaseNum + ".pdf", cfg_FTPRoot + sBucket + sValuationOutputFileName);
                                sbEmail.Append("    Valuation copied: " + sValuationOutputFileName + "\r\n");

                            }

                        }

                        ////////////////////////////////////////
                        //Recs
                        ///////////////////////////////////////
                        iUnique = 1;
                        diRec = new DirectoryInfo(cfg_recroot);
                        fiRECFiles = diRec.GetFiles("*.docx");
                        sbEmail.Append("\r\n\r\n==== RECS ====\r\n\r\n");

                        // for each file 
                        foreach (FileInfo fiRecDoc in fiRECFiles)
                        {

                            // Get case # from file name
                            sCaseNum = fiRecDoc.Name.Substring(0, fiRecDoc.Name.IndexOf("."));
                            sbEmail.Append("Case #: " + sCaseNum + "\r\n");

                            // Get CaseID & customer number
                            if (!GetCaseInfo(sCaseNum))
                            {
                                sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("GetInfo failed for case: " + sCaseNum);
                            }

                            if (sIRFileNum == "")
                            {
                                sIRFileNum = "99999999";
                            }

                            sbEmail.Append("    Original (rec) sIRUSerid: " + sIRUserID + "\r\n");


                            // If sIRUserID (left of person ordering email) is in list, change to "IR_Offshore"
                            if (lOffshore.IndexOf(sIRUserID.ToUpper()) != -1)
                            {
                                sIRUserID = "IR_Offshore";
                            }

                            sbEmail.Append("    Translated sIRUSerid: " + sIRUserID + "\r\n");


                            // Build Rec file name
                            if (sCustNum == "7338")   //Orlando Transportation
                            {
                                sOutputFileName = @"SCU Transp Orlando_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + sIRUserID + "_@_";
                                sBucket = @"Transportation\";
                            }
                            else if (sCustNum == "7026" || sCustNum == "7004")    //Orlando - Personal & Commercial
                            {
                                sOutputFileName = @"SCU Orlando_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + sIRUserID + "_@_";
                                sBucket = @"Underwriting\";

                            }
                            else
                            {
                                throw new Exception("Invalid Customer Number " + sCaseNum);
                            }

                            // If it exists - add digit to end
                            if (File.Exists(cfg_FTPRoot + sBucket + sOutputFileName + iUnique.ToString() + ".docx"))
                            {

                                iUnique = 2;
                                while (true)
                                {

                                    if (File.Exists(cfg_FTPRoot + sBucket + sOutputFileName + iUnique.ToString() + ".docx"))
                                    {
                                        iUnique++;
                                        if (iUnique > 10)
                                        {
                                            throw new ApplicationException(sOutputFileName + iUnique.ToString() + ".docx" + " could not be saved - exists.");
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }

                            // replace any illegal characters with "-"
                            foreach (char c in sInvalidChars)
                            {
                                sOutputFileName = sOutputFileName.Replace(c, '-');
                            }

                            // Copy to FTP folder under new name
                            fiRecDoc.CopyTo(cfg_FTPRoot + sBucket + sOutputFileName + iUnique.ToString() + ".docx");
                            sbEmail.Append("    Rec Copied: " + sOutputFileName + iUnique.ToString() + ".docx" + "\r\n");

                        }   // for each rec

                    }
                    catch (Exception ex)
                    {
                        bErr = true;
                        sbEmail.Append("**** Error processing files for case:" + sCaseNum + "\r\n" + ex.Message + "\r\n\r\n");
                    }

                    //////////////////////////////
                    // FTP all files in FTP folders
                    //////////////////////////////

                    //// Transportation folder ////
                    int iNumUploaded = 0;
                    int iNumToUpload = 0;

                    diPDF = new DirectoryInfo(cfg_FTPRoot + @"Transportation\");
                    fiPDFFiles = diPDF.GetFiles("*.*");

                    iNumToUpload = fiPDFFiles.Count();
                    if (iNumToUpload == 0)
                    {
                        sbEmail.Append("\r\n\r\n---- No Files to FTP in Transportation folder\r\n");
                    }
                    else
                    {
                        sbEmail.Append("\r\n\r\n---- Begin FTP transfer\r\n");
                        sbEmail.Append("\r\n\r\n" + iNumToUpload.ToString() + " files to upload in Transportation folder\r\n");
                        try
                        {
                            // Setup session options
                            SessionOptions sessionOptions = new SessionOptions
                            {
                                Protocol = Protocol.Sftp,
                                HostName = "Sftp.scui.com",
                                UserName = "SibFla",
                                Password = "W36CkHwA",
                                SshHostKeyFingerprint = "ssh-rsa 2048 5e:02:ae:fd:e3:cb:2e:1e:af:a0:8d:13:f4:1c:b4:a6",
                            };

                            //SshHostKeyFingerprint = "ssh-dss 1024 80:83:cd:a3:b2:eb:90:c1:b7:8d:81:62:db:a3:79:21",

                            using (Session session = new Session())
                            {

                                session.SessionLogPath = @"c:\automationlogs\SCUORL\FTPLogs.txt";

                                // Connect
                                session.Open(sessionOptions);

                                // Upload files
                                TransferOptions transferOptions = new TransferOptions();
                                transferOptions.TransferMode = TransferMode.Binary;
                                transferOptions.ResumeSupport.State = TransferResumeSupportState.Off;

                                TransferOperationResult transferResult;
                                transferResult = session.PutFiles(cfg_FTPRoot + @"Transportation\*", "/Transportation/", false, transferOptions);

                                // Throw on any error
                                transferResult.Check();

                                // Print results
                                foreach (TransferEventArgs transfer in transferResult.Transfers)
                                {
                                    oLU.WritetoLog(transfer.FileName + " uploaded");
                                    iNumUploaded++;
                                }

                            }

                            sbEmail.Append("\r\n" + iNumUploaded.ToString() + " files uploaded to Transportation\r\n\r\n---- End FTP transfer\r\n");
                        }
                        catch (Exception ex)
                        {
                            bErr = true;
                            sbEmail.Append("**** Error in FTP Transfer\r\n\r\n" + ex.Message + "\r\n\r\n");
                        }

                    }

                    //// Underwriting folder ////

                    iNumUploaded = 0;
                    iNumToUpload = 0;

                    diPDF = new DirectoryInfo(cfg_FTPRoot + @"Underwriting\");
                    fiPDFFiles = diPDF.GetFiles("*.*");

                    iNumToUpload = fiPDFFiles.Count();
                    if (iNumToUpload == 0)
                    {
                        sbEmail.Append("\r\n\r\n---- No Files to FTP in Underwriting folder\r\n");
                    }
                    else
                    {
                        sbEmail.Append("\r\n\r\n---- Begin FTP transfer\r\n");
                        sbEmail.Append("\r\n\r\n" + iNumToUpload.ToString() + " files to upload in Underwriting folder\r\n");
                        try
                        {
                            // Setup session options
                            SessionOptions sessionOptions = new SessionOptions
                            {
                                Protocol = Protocol.Sftp,
                                HostName = "Sftp.scui.com",
                                UserName = "SibFla",
                                Password = "W36CkHwA",
                                SshHostKeyFingerprint = "ssh-rsa 2048 5e:02:ae:fd:e3:cb:2e:1e:af:a0:8d:13:f4:1c:b4:a6",
                            };

                            using (Session session = new Session())
                            {

                                session.SessionLogPath = @"c:\automationlogs\SCUORL\FTPLogs.txt";

                                // Connect
                                session.Open(sessionOptions);

                                // Upload files
                                TransferOptions transferOptions = new TransferOptions();
                                transferOptions.TransferMode = TransferMode.Binary;
                                transferOptions.ResumeSupport.State = TransferResumeSupportState.Off;
                                transferOptions.PreserveTimestamp = false;

                                TransferOperationResult transferResult;
                                transferResult = session.PutFiles(cfg_FTPRoot + @"Underwriting\*", "/Underwriting/", false, transferOptions);

                                // Throw on any error
                                transferResult.Check();

                                // Print results
                                foreach (TransferEventArgs transfer in transferResult.Transfers)
                                {
                                    oLU.WritetoLog(transfer.FileName + " uploaded");
                                    iNumUploaded++;
                                }
                            }

                            sbEmail.Append("\r\n" + iNumUploaded.ToString() + " files uploaded to Underwriting\r\n\r\n---- End FTP transfer\r\n");
                        }
                        catch (Exception ex)
                        {
                            bErr = true;
                            sbEmail.Append("**** Error in FTP Transfer (Underwriting)\r\n\r\n" + ex.Message + "\r\n\r\n");
                        }

                    }
                    // Done - write to log, send email  and clean up
                    sbEmail.Append("\r\nEnd Processing  " + DateTime.Now.ToString() + "\r\n\r\n");
                    oLU.WritetoLog(sbEmail.ToString());
                    oLU.closeLog();

                }   //if FTP

                if (bErr)
                {
                    sendLogEmail("**** ERROR RECORDED - SCUORL Delivery Processing ****", sbEmail.ToString());
                }
                else
                {
                    sendLogEmail("SCUORL Delivery Processing", sbEmail.ToString());
                }
            }

            private void sendLogEmail(string sSubject, string sBodyText)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "noreply@sibfla.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;
                }
            }

            private bool GetCaseInfo(string sCaseNum)
            {

                bool bRetVal = false;

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseInfo_CaseNum";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {
                            guCaseID = sqlReader.GetGuid(1);
                            sInsured = sqlReader.GetSqlString(2).ToString();
                            sPolicy = sqlReader.GetSqlString(3).ToString();
                            sAddLine1 = sqlReader.GetSqlString(4).ToString();
                            sCustName = sqlReader.GetSqlString(10).ToString().Trim();
                            sCustNum = sqlReader.GetSqlString(11).ToString().Trim();
                            sAmount = sqlReader.GetSqlDecimal(12).ToString();
                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }

                    // IR FileNum
                    sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                    sqlCmd1.Parameters.AddWithValue("@fieldname", "ImageRight File Number");

                    sIRFileNum = "";
                    object oRetVal = sqlCmd1.ExecuteScalar();

                    if (oRetVal != null)
                    {
                        sIRFileNum = (string)oRetVal;
                    }

                    if (sIRFileNum == "")
                    {
                        sbEmail.Append("\r\n" + "No IR File Num for " + sCaseNum + " \r\n");
                    }

                    // email pf person ordering (IRUSerID)
                    sqlCmd1.CommandText = "sp_GetCaseOrderedBy";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);

                    sIRUserID = "";
                    oRetVal = sqlCmd1.ExecuteScalar();

                    if (oRetVal != null)
                    {
                        sIRUserID = (string)oRetVal;
                    }

                    if (sIRUserID == "")
                    {
                        sbEmail.Append("\r\n" + "No email for person ordering (IRUserID) for " + sCaseNum + " \r\n");
                    }
                    else
                    {
                        // Get the left part of the email for the IRUserID
                        int iPos = sIRUserID.IndexOf("@");
                        if (iPos > 0)
                        {
                            sIRUserID = sIRUserID.Substring(0, iPos);
                        }
                        else
                        {
                            sbEmail.Append("\r\n" + "Invalid person ordering email (IRUserID) for " + sCaseNum + " \r\n");
                            sIRUserID = "UNKNOWN";
                        }
                    }

                    bRetVal = true;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();
                }

                return bRetVal;
            }
        }
    }
}
