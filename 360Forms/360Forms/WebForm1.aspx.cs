﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _360Forms
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblCustomer.Text = "MJ Kelly of FL";
            lblInsured.Text = "Insured name goes here and could be long";
            lblPolicy.Text = "Policy Number 889900";
            lblUnderwriter.Text = "Underwriter name";
            lblAddress.Text = "Address line 1";
            lblCSZ.Text = "City, State Zip";
            lblCaseNum.Text = "9999999";
            lblCaseType.Text = "Case type goes here......";
            lblFormName.Text = "Sutton Liability Short Form v5.15.16 ";
            lblDateInsp.Text = "4/4/2017";
            lblInspector.Text = "T11";
            lblInterview.Text = "Name of person / Role";
            lblAddressCorrect.Text = "No - see update below";
            lblAddressUpdate.Text = "1234 Main Street East<br />Saint Petersburg, FL 33707";
        }
    }
}