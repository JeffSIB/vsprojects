﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="_360Forms.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="repeat" style="width: 680px">
            <table style="width: 100%">
                <tr>
                    <td style="width: 250px; vertical-align: bottom;">
                        <img src="https://ecommerce3.sibfla.com/images/custom/mainLogo.png" style="width: 125px" />
                    </td>
                    <td>&nbsp;     </td>
                    <td style="text-align: right">
                        <span style="font-family: arial; font-size: 9pt; font-weight: bold;">www.sibfla.com</span>
                    </td>
                </tr>
            </table>
            <table style="width: 100%; border-top: 1px solid #CCC; border-bottom: 1px solid #CCC; font-family: Arial Narrow, Arial; font-size: 8pt;">
                <tr>
                    <td style="width: 30%">
                        <asp:Label ID="lblCustomer" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                    <td style="width: 40%">
                        <asp:Label ID="Label1" runat="server" Font-Bold="True">Insured:&nbsp;</asp:Label>
                        <asp:Label ID="lblInsured" runat="server"></asp:Label>
                    </td>
                    <td style="width: 30%">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True">Policy Number:&nbsp;</asp:Label>
                        <asp:Label ID="lblPolicy" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label8" runat="server" Font-Bold="True">Underwriter:&nbsp;</asp:Label>
                        <asp:Label ID="lblUnderwriter" runat="server"></asp:Label>
                    </td>
                    <td rowspan="2">
                        <table>
                            <tr>
                                <td style="width: 15%; text-align: left;">
                                    <asp:Label ID="Label4" runat="server" Font-Bold="True">Address:&nbsp;</asp:Label>
                                </td>
                                <td style="width: 85%; text-align: left;">
                                    <asp:Label ID="lblAddress" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>
                                    <asp:Label ID="lblCSZ" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <asp:Label ID="Label6" runat="server" Font-Bold="True">Case Number:&nbsp;</asp:Label>
                        <asp:Label ID="lblCaseNum" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Font-Bold="True">&nbsp;</asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label10" runat="server" Font-Bold="True">Case Type:&nbsp;</asp:Label>
                        <asp:Label ID="lblCaseType" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table style="width: 100%; border-top: 1px solid #CCC; border-bottom: 1px solid #CCC; font-family: Arial Narrow, Arial; font-size: 8pt;">
                <tr>
                    <td style="width: 30%; text-align: center; font-size: 14pt; font-family: Arial Narrow, Arial; font-variant: small-caps;">
                        <asp:Label ID="lblFormName" runat="server" Font-Bold="True">Sutton Form Name</asp:Label>
                    </td>
                </tr>
            </table>

            <table style="width: 100%; border-collapse: separate; border-spacing: 5px;" >
                <tr>
                    <td style="width: 50%; vertical-align: top; border: thin solid">
                        <table style="width: 100%; font-family: Arial; font-size: 8pt; padding: 5px; border-collapse: separate; border-spacing: 5px;">
                            <tr>
                                <td style="width: 38%;"></td>
                                <td style="width: 28%;"></td>
                                <td style="width: 25%;"></td>
                                <td style="width: 10%;"></td>
                            </tr>
                            <tr style="background-color: #666666; border-style: solid; color: #FFFFFF">
                                <td colspan="4" style="text-align: center;">
                                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="9pt" Font-Names="Arial Narrow, Arial">Inspection Information</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <asp:Label ID="Label13" runat="server" Font-Bold="True">Date Inspected:</asp:Label>
                                </td>
                                <td style="text-align: left;">
                                    <asp:Label ID="lblDateInsp" runat="server"></asp:Label>
                                </td>
                                <td style="text-align: left;">
                                    <asp:Label ID="Label14" runat="server" Font-Bold="True">Inspector ID:</asp:Label>
                                </td>
                                <td style="text-align: left;">
                                    <asp:Label ID="lblInspector" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <asp:Label ID="Label15" runat="server" Font-Bold="True">Person interviewed:</asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblInterview" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <asp:Label ID="Label16" runat="server" Font-Bold="True">Address correct:</asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblAddressCorrect" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">&nbsp;
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblAddressUpdate" runat="server"></asp:Label>
                                </td>
                            </tr>

                        </table>
                    </td>
                    <td style="width: 1%">&nbsp;
                    </td>
                    <td style="width: 49%; vertical-align: top; border: thin solid">
                        <table style="width: 100%; font-family: Arial; font-size: 8pt; padding: 5px; border-collapse: separate; border-spacing: 5px;">
                            <tr>
                                <td style="width: 50%;"></td>
                                <td style="width: 50%;"></td>
                            </tr>
                            <tr style="background-color: #666666; border-style: solid; color: #FFFFFF">
                                <td colspan="2" style="text-align: center;">
                                    <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Size="9pt" Font-Names="Arial Narrow, Arial">Insured Information</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <asp:Label ID="Label9" runat="server"></asp:Label>
                                </td>
                                <td style="text-align: right;">
                                    <asp:Label ID="Label11" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="Label12" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
