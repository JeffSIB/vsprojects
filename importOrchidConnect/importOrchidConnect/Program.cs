﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;


namespace importOrchidConnect
{

    /// <summary>
    /// </summary>
    /// 

    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyNumber { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            //public string BusinessOperations { get; set; }
            public string LocationFullAddress { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string LocationContactEmail { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string CoverageB { get; set; }
            public string CoverageC { get; set; }
            public string CoverageD { get; set; }
            public string CoverageE { get; set; }
            public string CoverageF { get; set; }
            public string ISOClass { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }

            public string Construction { get; set; }
            public string YearBuilt { get; set; }
            public string DwellingType { get; set; }
            public string NumberOfFloors { get; set; }
            public string Occupancy { get; set; }
            public string SquareFootage { get; set; }
            public string DistanceToCoast { get; set; }
            public string RoofYear { get; set; }
            public string RoofMaterials { get; set; }
            public string RoofGeometry { get; set; }
            public string Usage { get; set; }
            public string WindMitigation { get; set; }
            public string PCClass { get; set; }
            public string RoofCladding { get; set; }
            public string RoofCondition { get; set; }
            public string BuildingCladding { get; set; }
            public string CompanyPolicyForm { get; set; }
            public string FireAlarm { get; set; }
            public string BurglarAlarm { get; set; }
            public string CentralAlarm { get; set; }
            public string PolicyStatus { get; set; }
            public string FenceToCode { get; set; }
            public string Unfenced { get; set; }
            public string Heating { get; set; }
            public string HeatingFullPartial { get; set; }
            public string HeatingUpdate { get; set; }
            public string HeatingYear { get; set; }
            public string PoolAboveGround { get; set; }
            public string Farming { get; set; }
            public string NumberOfFamilies { get; set; }
            public string OtherPets { get; set; }
            public string Dogs { get; set; }
            public string Secured { get; set; }

            public string Plumbing { get; set; }
            public string PlumbingFullPartial { get; set; }
            public string PlumbingUpdate { get; set; }
            public string PlumbingYear { get; set; }
            public string PoolSlide { get; set; }
            public string PoolDivingBoard { get; set; }
            public string PoolScreenEncl { get; set; }
            public string Pool { get; set; }
            public string Updates { get; set; }
            public string WiringYear { get; set; }

            public string AgentEmail { get; set; }
            public string AltContactName { get; set; }
            public string AltContactPhone { get; set; }
            public string AltContactMPhone { get; set; }
            public string AltContactEmail { get; set; }

            public string SidingWood { get; set; }
            public string SidingDryVit { get; set; }
            public string SidingEIFS { get; set; }
            public string SidingVinyl { get; set; }
            public string SidingMasonite { get; set; }
            public string SidingOther { get; set; }

        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
        static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string cfg_SQLMainSIBIConnStr = ConfigurationManager.AppSettings["SIBIConnStr"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];


        static LogUtils.LogUtils oLU;
        static bool bErr = false;

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {

            string sExcelFileName = "";
            string sPoolFenced = "";

            bool bImportSuccess = false;

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                if (args.Length == 0)
                {
                    throw new ApplicationException("No arguments passed");                
                }

                // get excel file name from command line
                sExcelFileName = args[0];

                // does file exist?
                FileInfo fi = new FileInfo(cfg_sourcedir + sExcelFileName);
                if (!fi.Exists)
                {
                    // send email
                    throw new ApplicationException("File does not exist: " + sExcelFileName);
                }

            }

            catch (Exception ex)
            {
                oLU.closeLog();
                sendErrEmail("Error initializing imptOrchidExcel\r\n\r\n" + ex.Message);
                return;
            }
            
 
            try
            {

                oLU.WritetoLog("Processing: " + sExcelFileName);
                sbEmail.Append(System.Environment.NewLine + "Processing: " + sExcelFileName + System.Environment.NewLine);

                var excel = new ExcelQueryFactory(cfg_sourcedir + sExcelFileName);
                excel.AddMapping<ImportRecord>(x => x.InsuredName, "Insured Name");
                excel.AddMapping<ImportRecord>(x => x.ContactPhoneHome, "Insured Phone Number");
                excel.AddMapping<ImportRecord>(x => x.LocationContactName, "Inspection Contact Name");
                excel.AddMapping<ImportRecord>(x => x.LocationContactPhone, "Inspection Contact Phone #");
                excel.AddMapping<ImportRecord>(x => x.LocationContactEmail, "Inspection Contact Email");

                excel.AddMapping<ImportRecord>(x => x.AgencyAgentName, "Agency Name");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentPhone, "Producer Primary Phonee");
                excel.AddMapping<ImportRecord>(x => x.Producer, "Producer Name");

                excel.AddMapping<ImportRecord>(x => x.PolicyNumber, "Policy #");
                excel.AddMapping<ImportRecord>(x => x.EffectiveDate, "Policy Effective Date");
                excel.AddMapping<ImportRecord>(x => x.CoverageA, "Coverage A");
                excel.AddMapping<ImportRecord>(x => x.LocationAddress1, "Property Street");
                excel.AddMapping<ImportRecord>(x => x.LocationCity, "Property City");
                excel.AddMapping<ImportRecord>(x => x.LocationState, "Property State");
                excel.AddMapping<ImportRecord>(x => x.LocationZip, "Property Zip Code");
                excel.AddMapping<ImportRecord>(x => x.InsuranceCompany, "Policy Company");
                excel.AddMapping<ImportRecord>(x => x.Construction, "Construction");
                excel.AddMapping<ImportRecord>(x => x.YearBuilt, "Year Built");
                excel.AddMapping<ImportRecord>(x => x.DwellingType, "Dwelling Type");
                excel.AddMapping<ImportRecord>(x => x.NumberOfFloors, "Number of Floors");
                excel.AddMapping<ImportRecord>(x => x.Occupancy, "Occupancy");
                excel.AddMapping<ImportRecord>(x => x.SquareFootage, "Square Footage");
                excel.AddMapping<ImportRecord>(x => x.InspectionType, "Inspection Type");
                excel.AddMapping<ImportRecord>(x => x.DistanceToCoast, "Distance To Coast");
                excel.AddMapping<ImportRecord>(x => x.RoofYear, "Roof Year");
                excel.AddMapping<ImportRecord>(x => x.RoofMaterials, "Roof Materials");
                excel.AddMapping<ImportRecord>(x => x.RoofGeometry, "Roof Geometry");

                excel.AddMapping<ImportRecord>(x => x.WindMitigation, "Wind Mitigation");
                excel.AddMapping<ImportRecord>(x => x.PCClass, "PC Class");

                excel.AddMapping<ImportRecord>(x => x.RoofCondition, "Roof Condition");

                excel.AddMapping<ImportRecord>(x => x.BurglarAlarm, "Burglar Alarm");
                excel.AddMapping<ImportRecord>(x => x.FireAlarm, "Fire Alarm");
                excel.AddMapping<ImportRecord>(x => x.Updates, "Any Updates");
                excel.AddMapping<ImportRecord>(x => x.HeatingYear, "Heating / AC Year");
                excel.AddMapping<ImportRecord>(x => x.PlumbingYear, "Plumbing Year");
                excel.AddMapping<ImportRecord>(x => x.WiringYear, "Wiring Year");
                excel.AddMapping<ImportRecord>(x => x.OtherPets, "Other Pets");
                excel.AddMapping<ImportRecord>(x => x.Dogs, "Dog");

                excel.AddMapping<ImportRecord>(x => x.Pool, "Swimming Pool");
                excel.AddMapping<ImportRecord>(x => x.PoolSlide, "Pool Slide");
                excel.AddMapping<ImportRecord>(x => x.PoolDivingBoard, "Pool Diving Board");
                excel.AddMapping<ImportRecord>(x => x.PoolAboveGround, "Is Pool Above Ground?");
                excel.AddMapping<ImportRecord>(x => x.PoolScreenEncl, "Screen Enclosure");
                excel.AddMapping<ImportRecord>(x => x.FenceToCode, "Fence to Code");
                excel.AddMapping<ImportRecord>(x => x.Unfenced, "Unfenced");
                excel.AddMapping<ImportRecord>(x => x.Secured, "Secured Community / Building");

                excel.AddMapping<ImportRecord>(x => x.SidingWood, "Type of Siding – Wood Shake");
                excel.AddMapping<ImportRecord>(x => x.SidingDryVit, "Type of Siding – Dry Vit");
                excel.AddMapping<ImportRecord>(x => x.SidingEIFS, "Type of Siding – EIFS");
                excel.AddMapping<ImportRecord>(x => x.SidingVinyl, "Type of Siding – Vinyl");
                excel.AddMapping<ImportRecord>(x => x.SidingMasonite, "Type of Siding – Masonite");
                excel.AddMapping<ImportRecord>(x => x.SidingOther, "Type of Siding – Other");

                var rows = (from row in excel.Worksheet<ImportRecord>(0) select row).ToList();

                foreach (var row in rows)
                {

                    // skip row if policy number is empty
                    if (row.PolicyNumber != null)
                    {

                        ImportRequests oAPI = new ImportRequests();
                        oAPI.CustomerUserName = "APIProd";
                        oAPI.CustomerPassword = "Sutton2012";
                        oAPI.CustomerAccount = "7339";

                        // standard values provided
                        oAPI.InsuredName = row.InsuredName;
                        oAPI.ContactPhoneWork = "";
                        oAPI.ContactPhoneCell = "";
                        oAPI.ContactPhoneHome = row.ContactPhoneHome;
                        oAPI.PolicyNumber = row.PolicyNumber;
                        oAPI.EffectiveDate = row.EffectiveDate;
                        oAPI.CoverageA = row.CoverageA;
                        oAPI.LocationAddress1 = row.LocationAddress1;
                        oAPI.LocationAddress2 = "";
                        oAPI.LocationCity = row.LocationCity;
                        oAPI.LocationState = row.LocationState;
                        oAPI.LocationZip = row.LocationZip;
                        oAPI.MailAddress1 = row.LocationAddress1;
                        oAPI.MailAddress2 = "";
                        oAPI.MailCity = row.LocationCity;
                        oAPI.MailState = row.LocationState;
                        oAPI.MailZip = row.LocationZip;
                        oAPI.InsuranceCompany = row.InsuranceCompany;
                        oAPI.AgentEmail = "";
                        oAPI.Producer = row.Producer;
                        oAPI.Construction = row.Construction;
                                                
                        // Agency / Agent
                        oAPI.AgencyAgentName = row.AgencyAgentName;
                        oAPI.AgentCode = "";
                        oAPI.AgencyAgentPhone = row.AgencyAgentPhone;
                        oAPI.AgentFax = "";
                        oAPI.AgencyAgentContact = row.Producer;
                        oAPI.AgentAddress1 = "5656 Central Ave";
                        oAPI.AgentAddress2 = "";
                        oAPI.AgentCity = "St. Petersburg";
                        oAPI.AgentState = "FL";
                        oAPI.AgentZip = "33707";

                        oAPI.AltLocationContact = "";

                        // standard values not provided
                        oAPI.EmailConfirmation = "";
                        oAPI.Underwriter = "";
                        oAPI.UnderwriterFirstName = "";
                        oAPI.UnderwriterLastName = "";
                        oAPI.RushHandling = "N";
                        oAPI.ContactName = "";

                        // Convert CoverageA to numeric
                        double dCovA = Convert.ToDouble(row.CoverageA);

                        // Convert year built to numeric
                        int iYearBuilt = Convert.ToInt32(row.YearBuilt);
                        int iYearsOld = 0;
                        if (iYearBuilt > 0)
                        {
                            iYearsOld = DateTime.Now.Year - iYearBuilt;
                        }


                        // inspection type
                        string sInspType = "";
                        string sTypeIn = "";

                        if (row.InspectionType == null)
                        {
                            row.InspectionType = "";
                        }
                        else
                        {
                            sTypeIn = row.InspectionType.ToUpper();

                            if (sTypeIn == "BASIC W/RCE")
                            {
                                sInspType = "7339-R-E-I";
                            }
                            else if (sTypeIn == "BASIC INSPECTION" || sTypeIn == "BASIC")
                            {
                                sInspType = "7339-R-E-E2V-I";
                            }
                            else if (sTypeIn == "BASIC W/RCE & INTER PHOTO" || sTypeIn == "BASIC W/INTERIOR & RCE")
                            {
                                sInspType = "7339-R-IE-E2V-I";
                            }
                            else if (sTypeIn == "ALL INCLUSIVE")
                            {
                                sInspType = "7339-R-IE-E2V-U-I";
                            }
                            else if (sTypeIn == "BUILDERS RISK")
                            {
                                sInspType = "7339-BR-I";
                            }
                            else if (sTypeIn == "EXCESS FLOOD")
                            {
                                sInspType = "7339-Flood-I";
                            }
                        }
                        
                        // If inspection type is blank - throw error
                        if (sInspType.Length == 0)
                        {
                            throw new Exception("Invalid Inspection Type - File name: " + sExcelFileName + " - Policy#: " + row.PolicyNumber);                        
                        }

                        oAPI.InspectionType = sInspType;

                        //Reverse pool fence
                        sPoolFenced = row.Unfenced;
                        if (sPoolFenced.ToUpper() == "NO")
                            sPoolFenced = "Yes";
                        if (sPoolFenced.ToUpper() == "YES")
                            sPoolFenced = "No";

                        // comments
                        oAPI.Comments = "";

                        // Custom/Generic field values
                        oAPI.BurglarlAlarmPresent = row.BurglarAlarm ?? "Not Provided";
                        oAPI.FirelAlarmPresent = row.FireAlarm ?? "Not Provided";
                        oAPI.DistanceCoast = row.DistanceToCoast ?? "Not Provided";
                        oAPI.Occupancy = row.Occupancy ?? "Not Provided";
                        oAPI.DwellingType = row.DwellingType ?? "Not Provided";
                        oAPI.Stories = row.NumberOfFloors ?? "Not Provided";
                        oAPI.YearBuilt = row.YearBuilt ?? "Not Provided";

                        oAPI.SidingWood = row.SidingWood ?? "Not Provided";
                        oAPI.SidingDryVit = row.SidingDryVit ?? "Not Provided";
                        oAPI.SidingEIFS = row.SidingEIFS ?? "Not Provided";
                        oAPI.SidingVinyl = row.SidingVinyl ?? "Not Provided";
                        oAPI.SidingMasonite = row.SidingMasonite ?? "Not Provided";
                        oAPI.SidingOther = row.SidingOther ?? "Not Provided";

                        oAPI.Dog = row.Dogs ?? "Not Provided";
                        oAPI.OtherPets = row.OtherPets ?? "Not Provided";

                        oAPI.PoolPresent = row.Pool ?? "Not Provided";
                        oAPI.PoolAboveGround = row.PoolAboveGround ?? "Not Provided";
                        oAPI.PoolDivingBoardPresent = row.PoolDivingBoard ?? "Not Provided";
                        oAPI.PoolSlidePresent = row.PoolSlide ?? "Not Provided";
                        oAPI.PoolScreenEnclosurePresent = row.PoolScreenEncl ?? "Not Provided";
                        //oAPI.FenceToCode = row.FenceToCode ?? "Not Provided";
                        oAPI.PoolFencePresent = sPoolFenced ?? "Not Provided";
                        oAPI.ProtectionClass = row.PCClass ?? "Not Provided";
                        oAPI.RoofType = row.RoofMaterials ?? "Not Provided";
                        oAPI.RoofGeometry = row.RoofGeometry ?? "Not Provided";
                        oAPI.RoofYear = row.RoofYear ?? "Not Provided";
                        oAPI.WindstormProtectiveDevices = row.WindMitigation ?? "Not Provided";
                        oAPI.HeatingYear = row.HeatingYear ?? "Not Provided";
                        oAPI.PlumbingYear = row.PlumbingYear ?? "Not Provided";
                        oAPI.WiringYear = row.WiringYear ?? "Not Provided";
                        oAPI.Secured = row.Secured ?? "Not Provided";
                        oAPI.Updates = row.Updates ?? "Not Provided";


                        //**********************************
                        // UNCOMMENT FOR TEST 
                        //oAPI.CustomerUserName = "APITest";
                        //oAPI.CustomerAccount = "9998";
                        //oAPI.InspectionType = "9998RE";
                        //oAPI.EffectiveDate = "";

                        //**********************************

                        oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + row.PolicyNumber);
                        sbEmail.Append("Importing Policy# " + row.PolicyNumber + System.Environment.NewLine);

                        string sRet = oAPI.ImportOrchidConnect();

                        oLU.WritetoLog("oAPI.Import return for for Policy# " + row.PolicyNumber + "\r\n\r\n" + sRet);

                        var importResults = sRet.FromJSON<List<ImportResult>>();

                        foreach (var importResult in importResults)
                        {

                            if (importResult.Successful)
                            {
                                oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                                sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + System.Environment.NewLine);
                            }
                            else
                            {
                                oLU.WritetoLog("**** Import failed **** "); if (importResult.Errors != null)
                                    sbEmail.Append("**** Import failed ****" + System.Environment.NewLine);

                                foreach (var error in importResult.Errors)
                                {
                                    oLU.WritetoLog("Error: " + error.ErrorText);
                                    sbEmail.Append("Error: " + error.ErrorText + System.Environment.NewLine);
                                }
                            }

                            if ((bool)importResult.Duplicate)
                            {
                                oLU.WritetoLog("Duplicate case");
                                sbEmail.Append("Duplicate case" + System.Environment.NewLine);
                            }
                        }

                    }   // Policy number not empty

                }   // foreach row in sheet

                bImportSuccess = true;
                
            }   //try

            catch (Exception ex)
            {
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail("Exception logged" + System.Environment.NewLine + ex.Message);
            }

            finally
            {

                // Copy source file to holding area and delete temp
                string sSourceName = cfg_sourcedir + sExcelFileName;
                string sDestName = "";

                //if successful - copy to sibidata\OrchidExcel\Processed
                //if failed - copy to sibidata\OrchidExcel\fail
                if (bImportSuccess)
                {
                    sDestName = cfg_compdir + sExcelFileName;
                }
                else
                {
                    sDestName = cfg_faildir + sExcelFileName;
                }

                // Copy Excel
                File.Copy(sSourceName, sDestName);
                if (File.Exists(sDestName))
                {
                    File.Delete(sSourceName);
                }
                else
                {
                    throw new ApplicationException("Copy failed for: " + sExcelFileName);
                }

                sendLogEmail(sbEmail.ToString());
                oLU.closeLog();
            }
            
        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import Orchid Connect Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import Orchid Connect Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }
    }
}
