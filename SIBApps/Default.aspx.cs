using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace SIBApps
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbOpenItems_Click(object sender, EventArgs e)
        {
            Response.Redirect("OpenItems/publish.htm");
        }

        protected void lbReturns_Click(object sender, EventArgs e)
        {
            Response.Redirect("Returns/publish.htm");
        }

        protected void lbFormsByCLient_Click(object sender, EventArgs e)
        {
            Response.Redirect("FormsByClient/publish.htm");
        }

        protected void lbImageView_Click(object sender, EventArgs e)
        {
            Response.Redirect("ImageView/publish.htm");
        }

        protected void lbRSUpdate_Click(object sender, EventArgs e)
        {
            Response.Redirect("RSUpdate.aspx");
        }

        protected void lb7132Log_Click(object sender, EventArgs e)
        {
            Response.Redirect("7132Log/publish.htm");
        }

        protected void lbMSB_Click(object sender, EventArgs e)
        {
            Response.Redirect("MSBWorksheet/publish.htm");
        }

        protected void lbSFM_Click(object sender, EventArgs e)
        {
            Response.Redirect("SIM/publish.htm");
        }

        protected void lbAssignMgr_Click(object sender, EventArgs e)
        {
            Response.Redirect("AssignManager/publish.htm");
        }

        protected void lb2200Log_Click(object sender, EventArgs e)
        {
            Response.Redirect("2200log/publish.htm");
        }

        protected void lb7241MHP_Click(object sender, EventArgs e)
        {
            Response.Redirect("7241MHPLog/publish.htm");
        }

        protected void lbInspCost_Click(object sender, EventArgs e)
        {
            Response.Redirect("InspCostByClient/publish.htm");
        }

        protected void lb7138List_Click(object sender, EventArgs e)
        {
            Response.Redirect("7138List/publish.htm");
        }

        protected void lbReview_Click(object sender, EventArgs e)
        {
            Response.Redirect("Review/publish.htm");
        }

        protected void lbCompViewer_Click(object sender, EventArgs e)
        {
            Response.Redirect("CompletedItems/publish.htm");
        }

        protected void lbInvoicing_Click(object sender, EventArgs e)
        {
            Response.Redirect("Invoicing/publish.htm");
        }

        protected void lbE2VLog_Click(object sender, EventArgs e)
        {
            Response.Redirect("E2VLog/publish.htm");
        }

        protected void lb360Payroll_Click(object sender, EventArgs e)
        {
            Response.Redirect("360Payroll/publish.htm");
        }

        protected void lbInspPayroll_Click(object sender, EventArgs e)
        {
            Response.Redirect("InspPayroll/publish.htm");
        }

        protected void lbInspPRDeposits_Click(object sender, EventArgs e)
        {
            Response.Redirect("PayrollDeposit/publish.htm");
        }
        protected void lbInvoicing360_Click(object sender, EventArgs e)
        {
            Response.Redirect("Invoicing360/publish.htm");
        }

        protected void lbStatements_Click(object sender, EventArgs e)
        {
            Response.Redirect("StatementEmail/publish.htm");
        }
        
        protected void lbStmtDisc_Click(object sender, EventArgs e)
        {
            Response.Redirect("Statements/publish.htm");
        }

        protected void lb360Utils_Click(object sender, EventArgs e)
        {
            Response.Redirect("360Utils/publish.htm");
        }

        protected void lbSurveyCount_Click(object sender, EventArgs e)
        {
            Response.Redirect("SurveyCount/publish.htm");
        }

        protected void lb360RecHaz_Click(object sender, EventArgs e)
        {
            Response.Redirect("360RecHaz/publish.htm");
        }
    }
}
