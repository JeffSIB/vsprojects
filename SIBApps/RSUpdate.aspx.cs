using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace InspWeb
{
    public partial class RSUpdate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void lbRSConfig_Click(object sender, EventArgs e)
        {
            FileDownload("RSConfig.exe");
        }

        protected void FileDownload(string sFileName)
        {

            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            string sRootDir = colNameVal.Get("DownloadDir");

            // set the http content type 
            Response.ContentType = "APPLICATION/ZIP";

            // initialize the http content-disposition header
            string sdisHeader = "Attachment; Filename=" + sFileName;

            Response.AppendHeader("Content-Disposition", sdisHeader);

            //transfer the file byte-by-byte to the response object
            System.IO.FileInfo fileToDownload = new System.IO.FileInfo(sRootDir + sFileName);
            Response.Flush();
            Response.WriteFile(fileToDownload.FullName);
            Response.Flush();
            Response.Close();

        }


    }
}
