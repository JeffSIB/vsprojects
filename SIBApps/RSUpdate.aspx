<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RSUpdate.aspx.cs" Inherits="InspWeb.RSUpdate" %>
<%@ Register TagPrefix="uc1" TagName="SIBHeader" Src="SIBMiniHeader.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <title>Sutton Inspection Bureau</title>

    <script language="javascript">
     
        function CancelForm()
        {
			location.href = "Default.aspx";
        }	
        
        function showHelp()
        {
            alert("This function is not currently operational.");
        }
            
        runtimeVersion = "2.0.0";

        function Initialize()
        {
            if (HasRuntimeVersion(runtimeVersion))
          {
            tblCLR.style.display = "none";
          }
        }
        function HasRuntimeVersion(v)
        {
          var va = GetVersion(v);
          var i;
          var a = navigator.userAgent.match(/\.NET CLR [0-9.]+/g);
          if (a != null)
            for (i = 0; i < a.length; ++i)
              if (CompareVersions(va, GetVersion(a[i])) <= 0)
		        return true;
          return false;
        }
        function GetVersion(v)
        {
          var a = v.match(/([0-9]+)\.([0-9]+)\.([0-9]+)/i);
            return a.slice(1);
        }
        function CompareVersions(v1, v2)
        {
          for (i = 0; i < v1.length; ++i)
          {
            var n1 = new Number(v1[i]);
            var n2 = new Number(v2[i]);
            if (n1 < n2)
              return -1;
            if (n1 > n2)
              return 1;
          }
          return 0;
        }            

    </script>

</head>
<body vlink="#28fbf4" alink="#28fb54" link="#285b54" leftmargin="0" background="Images/bluebkg.jpg"
    topmargin="0" >
    <form id="frmRSUpdate" method="post" runat="server">
        <uc1:SIBHeader ID="SIBHeader1" runat="server"></uc1:SIBHeader>
        <div id="div1" runat="server" style="left: 10px; width: 700px; position: absolute">
            <table width="700" style="border-bottom: gray thin solid">
                <tr>
                    <td width="600">
                        <asp:Label ID="Label13" runat="server" CssClass="StdTextGreySmallCap">
								&nbsp;Drawing program update
                        </asp:Label>
                    </td>
                    <td align="center" width="100">
                        <input class="StdTextSmall" onclick="CancelForm()" tabindex="500" type="button" value="Cancel"
                            name="bCancel">
                    </td>
                </tr>
            </table>
            <table cellspacing="5" cellpadding="0" width="600" border="0">
                <tr>
                    <td width="5">
                        &nbsp;</td>
                    <td width="595" class="StdTextSmallGrey">
                        <p>
                            Follow the steps below to update SibOffice for RapidSketch.</p>
                    </td>
                </tr>
            </table>
            <br>
            <table id="Table2" style="border-right: #c0c0c0 thin solid; border-top: #c0c0c0 thin solid;
                border-left: #c0c0c0 thin solid; border-bottom: #c0c0c0 thin solid" cellspacing="0"
                cellpadding="0" width="600" border="0" runat="server">
                <tr bgcolor="#e4ffe4" height="25">
                    <td width="700" class="StdTextSmall">
                        &nbsp;&nbsp;<b>Install RapidSketch</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="5" cellpadding="2" width="600" border="0">
                            <tr>
                                <td align="center" width="25" bgcolor="#3300ff">
                                    <asp:Label ID="Label3" runat="server" CssClass="StdTextLargeWhite" BackColor="#3300ff">
											<b>1.</b>
                                    </asp:Label>
                                </td>
                                <td align="left" width="575">
                                    <asp:Label ID="Label1" runat="server" CssClass="StdTextSmall">
											Click the link below to download the file to your computer.<br>
											<span class="StdTextSmallMaroon">When asked to Run or Save, select <b>Run.</b></span>
											<p>When the installation program starts, accept all of the default options.
											</p>
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;<asp:HyperLink ID="hlRSInstall" runat="server" NavigateUrl="http://sibapps/RSUpdate/RapidSketchInstall.exe">Download RapidSketch application</asp:HyperLink><br>
                                    <span class="StdTextSmallGrey">(File size is 38Mb - download could take several minutes.)
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <table id="Table1" style="border-right: #c0c0c0 thin solid; border-top: #c0c0c0 thin solid;
                border-left: #c0c0c0 thin solid; border-bottom: #c0c0c0 thin solid" cellspacing="0"
                cellpadding="0" width="600" border="0" runat="server">
                <tr bgcolor="#e4ffe4" height="25">
                    <td width="700" class="StdTextSmall">
                        &nbsp;&nbsp;<b>Update configuration and forms</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="5" cellpadding="2" width="600" border="0">
                            <tr>
                                <td align="center" width="25" bgcolor="#3300ff">
                                    <asp:Label ID="Label2" runat="server" CssClass="StdTextLargeWhite" BackColor="#3300ff">
											<b>2.</b>
                                    </asp:Label>
                                </td>
                                <td align="left" width="575">
                                    <asp:Label ID="Label4" runat="server" CssClass="StdTextSmall">
											Click the link below to download the file to your computer.<br>
											<span class="StdTextSmallMaroon">When asked to Run or Save, select <b>Run.</b></span>
											<p>When the WinZip Self-Extractor starts, click <b>Unzip.</b><br>
												When the files have been unzipped, click OK.</p>
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;<asp:HyperLink ID="hlRSConfig" runat="server" NavigateUrl="http://sibapps/RSUpdate/RSConfigSO.exe">Download RapidSketch configuration</asp:HyperLink><br>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>            
            <table width="600" cellspacing="5">
                <tr>
                    <td width="600" align="center" valign="middle" class="StdTextSmall">
                        <a class="botLinks" tabindex="40" href="#" onclick="CancelForm()">Return to main page</a>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
