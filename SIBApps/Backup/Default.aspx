<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SIBApps._Default" %>

<%@ Register TagPrefix="uc1" TagName="SIBHeader" Src="SIBHeader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Sutton Inspection Bureau</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />
</head>
<body background="Images/bluebkg.jpg" leftmargin="0" topmargin="0" alink="#28fb54"
    vlink="#28fbf4" link="#285b54">
    <form id="frmDefault" runat="server">
    <uc1:SIBHeader ID="SIBHeader1" runat="server"></uc1:SIBHeader>
    <br>
    <div>
        <table width="370">
            <tr valign="middle">
                <td>
                    <table width="370" style="border-bottom: gray thin solid;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" style="width: 370px">
                                <asp:Label ID="lblInspHdr" runat="server" CssClass="StdTextSmallGreySmallCap">
                                    Internal Software Applications
                                </asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="middle">
                <td>
                    <table width="370" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" style="width: 370px">
                                <asp:Label ID="Label1" runat="server" CssClass="StdTextSmallGrey">
                                    Use these options to install applications on your workstation.
                                </asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="25" valign="middle">
                <td style="height: 25px">
                    <table width="370" style="border-right: #d3d3d3 thin solid; border-top: #d3d3d3 thin solid;
                        border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid" cellpadding="0"
                        cellspacing="0" bgcolor="#f5f5f5">
                        <tr bgcolor="#73929a">
                            <td width="370" height="20" align="left">
                                <asp:Label ID="Label4" runat="server" CssClass="StdTextSmallWhite">
							                    &nbsp;Case management
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbOpenItems" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbOpenItems_Click">
							                        Open item manager
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server" CssClass="StdTextSmallGrey">
							                    &nbsp;Provides detailed information on open items
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbReturns" CssClass="StdTextSmall" CausesValidation="False" runat="server"
                                                OnClick="lbReturns_Click">
							                        Returned item viewer
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label11" runat="server" CssClass="StdTextSmallGrey">
							                    &nbsp;Provides detailed information on returned items
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbFormsByClient" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbFormsByCLient_Click">
							                        Forms by Client reporting
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" CssClass="StdTextSmallGrey">
							                    &nbsp;Reports detailing type of forms ordered by client
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbInspCost" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbInspCost_Click">
							                        Inspector Cost by Client
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label7" runat="server" CssClass="StdTextSmallGrey">
							                    &nbsp;Invoice and inspector costs by client reporting
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbAssignMgr" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbAssignMgr_Click">
							                        Inspector assignment manager
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label10" runat="server" CssClass="StdTextSmallGrey">
							                    &nbsp;Reporting and management utility for paperless inspector assignments
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbReview" CssClass="StdTextSmall" CausesValidation="False" runat="server"
                                                OnClick="lbReview_Click">
							                        Review
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label12" runat="server" CssClass="StdTextSmallGrey">
							                    &nbsp;Paperless review of received cases
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbE2VLog" CssClass="StdTextSmall" CausesValidation="False" runat="server"
                                                OnClick="lbE2VLog_Click">
							                        E2V Log
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label13" runat="server" CssClass="StdTextSmallGrey">
							                    &nbsp;E2Value Residential Estimate manager
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                       <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbSurveyCount" CssClass="StdTextSmall" CausesValidation="False" runat="server"
                                                OnClick="lbSurveyCount_Click">
							                        Survey Count
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label16" runat="server" CssClass="StdTextSmallGrey">
							                    &nbsp;Shows items requested by account by day
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr height="25" valign="middle">
                <td style="height: 25px">
                    <table width="370" style="border-right: #d3d3d3 thin solid; border-top: #d3d3d3 thin solid;
                        border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid" cellpadding="0"
                        cellspacing="0" bgcolor="#f5f5f5">
                        <tr bgcolor="#73929a">
                            <td width="370" height="20" align="left">
                                <asp:Label ID="Label2" runat="server" CssClass="StdTextSmallWhite">
							                    &nbsp;SibOffice related
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lb7132Log" CssClass="StdTextSmall" CausesValidation="False" runat="server"
                                                OnClick="lb7132Log_Click">
							                        7132 Log
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="LinkButton1" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lb2200Log_Click">
							                        MSB Form 2200 Log
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lb7241MHP" CssClass="StdTextSmall" CausesValidation="False" runat="server"
                                                OnClick="lb7241MHP_Click">
							                        7241MHP Form Log
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbRSUpdate" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbRSUpdate_Click">
							                        RapidSketch/1053B update
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbSFM" CssClass="StdTextSmall" CausesValidation="False" runat="server"
                                                OnClick="lbSFM_Click">
							                        Supplemental Information Manager (SIM)
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbCompViewer" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbCompViewer_Click">
							                        Completed Items Viewer
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif" />
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbInvoicing" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbInvoicing_Click">
							                        Invoicing - SIBOffice
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif" />
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbInvoicing360" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbInvoicing360_Click">
							                        Invoicing - 360
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif" />
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbStatements" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbStatements_Click">
							                        Statement Processing
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                       <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif" />
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbStmtDisc" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbStmtDisc_Click">
							                        Statement Discounts
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif" />
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lb360Payroll" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lb360Payroll_Click">
							                        Inspector Payroll - 360
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif" />
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbInspPayroll" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbInspPayroll_Click">
							                        Inspector Payroll - SIBOffice
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif" />
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbInspPRDeposits" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbInspPRDeposits_Click">
							                        Inspector Payroll Deposits
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr height="25" valign="middle">
                <td style="height: 25px">
                    <table width="370" style="border-right: #d3d3d3 thin solid; border-top: #d3d3d3 thin solid;
                        border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid" cellpadding="0"
                        cellspacing="0" bgcolor="#f5f5f5">
                        <tr bgcolor="#73929a">
                            <td width="370" height="20" align="left">
                                <asp:Label ID="Label14" runat="server" CssClass="StdTextSmallWhite">
							                    &nbsp;360 Utilities
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lb360Utils" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lb360Utils_Click">
							                        360 Utilities
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label15" runat="server" CssClass="StdTextSmallGrey">
							                    &nbsp;Utility deleting test cases from 360
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lb360RecHaz" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lb360RecHaz_Click">
							                        360 Recs & Hazards Report
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label17" runat="server" CssClass="StdTextSmallGrey">
							                    &nbsp;Create Excel sheet containing recs and hazards for a form
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr height="25" valign="middle">
                <td style="height: 25px">
                    <table width="370" style="border-right: #d3d3d3 thin solid; border-top: #d3d3d3 thin solid;
                        border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid" cellpadding="0"
                        cellspacing="0" bgcolor="#f5f5f5">
                        <tr bgcolor="#73929a">
                            <td width="370" height="20" align="left">
                                <asp:Label ID="Label3" runat="server" CssClass="StdTextSmallWhite">
							                    &nbsp;Inspector applications
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbMSB" CssClass="StdTextSmall" CausesValidation="False" runat="server"
                                                OnClick="lbMSB_Click">
							                        MSB Worksheet
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr height="25" valign="middle">
                <td style="height: 25px">
                    <table width="370" style="border-right: #d3d3d3 thin solid; border-top: #d3d3d3 thin solid;
                        border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid" cellpadding="0"
                        cellspacing="0" bgcolor="#f5f5f5">
                        <tr bgcolor="#73929a">
                            <td width="370" height="20" align="left">
                                <asp:Label ID="Label8" runat="server" CssClass="StdTextSmallWhite">
							                    &nbsp;Utilities
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="370">
                                    <tr>
                                        <td width="20" class="StdText" align="right">
                                            <img height="10" width="10" src="Images/blueBullet.gif">
                                        </td>
                                        <td width="350" align="left">
                                            <asp:LinkButton ID="lbImageView" CssClass="StdTextSmall" CausesValidation="False"
                                                runat="server" OnClick="lbImageView_Click">
							                        ImageView
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="Label9" runat="server" CssClass="StdTextSmallGrey">
							                    &nbsp;Utility for viewing images
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
