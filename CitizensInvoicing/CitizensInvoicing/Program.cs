﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Microsoft.Office;
namespace CitizensInvoicing
{
    public static class Program
    {
        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static string cfg_deliverto;
        static string cfg_CustName;
        static string cfg_CustAddress;
        static string cfg_CustCSZ;

        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Range oRange = null;
        static string sReportDate;
        static string sReportTitle;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;
        static DataTable dt360;
        static DataSet dsReportData;
        static string sCustNumber;
        static int cfg_TimeZoneOffset;
        static string sMode;
        static DateTime dInvoiceDate;
        static string sInvoiceDate;
        static DateTime dInvoiceDispDate;
        static string sInvoiceDispDate;
        static string sInvoiceNum;
        static decimal decTotalInvAmt;
        static int iInvoiceNumber;
        static Guid guInvoiceID;


        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";
            bool bRetVal = false;

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_deliverto = colNameVal.Get("deliverto");

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                // Run for 7072, 7542


                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //
                //  Customer Number
                //      Customer number to process
                //
                //  Invoice Date
                //      Invoice date in 360
                //
                //  Invoice display date
                //      Date displayed on invoice
                //
                //  Example:
                //  06/30/2021 7/1/2021 7072
                //  06/30/2021 7/1/2021 7542

                sMode = "";
                if (args.Length < 3)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                // set report period based on parammeter passed
                DateTime dt = new DateTime();

                // Invoice date (date invoice was produced in 360)
                // Validate and store in dInvoiceDate
                sInvoiceDate = args[0];
                if (!DateTime.TryParse(sInvoiceDate, out dInvoiceDate))
                {
                    throw new SystemException("Invalid invoice date supplied");
                }

                // Invoice display date (date that appears on invoice)
                // Validate and store in sInvoiceDate
                sInvoiceDispDate = args[1];
                if (DateTime.TryParse(sInvoiceDispDate, out dInvoiceDispDate))
                {
                    sInvoiceDispDate = dInvoiceDispDate.ToShortDateString();
                }
                else
                {
                    throw new SystemException("Invalid invoice display date supplied");
                }

                // Customer number
                // Validate and store in sCustNumber
                sCustNumber = args[2];
                if (sCustNumber == "7072")
                {
                    cfg_CustName = "Citizens Property Insurance Corp - 7072 - Personal";
                    cfg_CustAddress = "PO Box 10749";
                    cfg_CustCSZ = "Tallahassee , FL 32302";
                }
                else if (sCustNumber == "7452")
                {
                    cfg_CustName = "Citizens Property Insurance Corp - 7452 - Commercial";
                    cfg_CustAddress = "PO Box 10749";
                    cfg_CustCSZ = "Tallahassee , FL 32302";
                }
                else
                {
                    throw new SystemException("Invalid customer number supplied");
                }

                // set report run date
                dt = dInvoiceDispDate;
                sReportDate = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                // Set report title
                string sDateDisp = dt.Month.ToString() + "/" + dt.Day.ToString() + "/" + dt.Year.ToString();
                string sEmailSubject = "";
                string sExcelFileNameNoEx = "";
                string sExcelFileName = "";

                sExcelFileNameNoEx = cfg_outputdir + "Citizens" + sCustNumber + dt.Month.ToString() + dt.Day.ToString() + dt.Year.ToString();
                sExcelFileName = sExcelFileNameNoEx + ".xlsx";
                sReportTitle = "Citizens " + sCustNumber + " - " + sInvoiceDispDate;

                sEmailSubject = sReportTitle;
                int iRow = 0;

                // Get invoice info if it exists
                // Loads GUID InvoiceID into guInvoiceID, Invoice Number into iInvoiceNumber, amount into decTotalInvAmt
                if (!getInvoice(dInvoiceDate, sCustNumber))
                {
                    throw new Exception("getInvoice returned false");
                }

                if (iInvoiceNumber == 0)
                {
                    throw new Exception("No Invoice for period");
                }

                sInvoiceNum = iInvoiceNumber.ToString();

                // Create a DataSet.
                dsReportData = new DataSet("reportdata");
                dt360 = new DataTable("360");
                dsReportData.Tables.Add(dt360);

                try
                {
                    dt360.Columns.Add("CaseNum", typeof(int));
                    dt360.Columns.Add("Policy", typeof(string));
                    dt360.Columns.Add("Insured", typeof(string));
                    //dt360.Columns.Add("Address1", typeof(string));
                    //dt360.Columns.Add("Address2", typeof(string));
                    //dt360.Columns.Add("City", typeof(string));
                    //dt360.Columns.Add("State", typeof(string));
                    //dt360.Columns.Add("Zip", typeof(string));
                    //dt360.Columns.Add("Completed", typeof(string));
                    //dt360.Columns.Add("UWFirst", typeof(string));
                    //dt360.Columns.Add("UWLast", typeof(string));
                    dt360.Columns.Add("AmtBilled", typeof(decimal));
                    //dt360.Columns.Add("CaseType", typeof(string));

                    // Load invoice line items into data table
                    if (!getInvoicedItems(guInvoiceID,false))
                    {
                        throw new Exception("Error loading invoice line items");
                    }

                    // sort table
                    //dt360.DefaultView.Sort = "acnt ASC";
                    dt360 = dt360.DefaultView.ToTable();

                    if (dt360.Rows.Count > 0)
                    {

                        oExcel = new Excel.Application();
                        oExcel.Visible = true;
                        oWorkbook = oExcel.Workbooks.Add(1);
                        o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                        o360Worksheet.Name = "Invoice";
                        createHeader(o360Worksheet);

                        iRow = 11;
                        decTotalInvAmt = 0;

                        // loop through invoice line items
                        foreach (DataRow row in dt360.Rows)
                        {
                            addData(o360Worksheet, iRow, 1, row[1].ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                            addData(o360Worksheet, iRow, 2, row[2].ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                            addData(o360Worksheet, iRow, 3, sInvoiceDate, "C" + iRow.ToString(), "C" + iRow.ToString(), "MM/DD/YYYY", "C");
                            addData(o360Worksheet, iRow, 4, row[0].ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "", "");
                            addData(o360Worksheet, iRow, 5, sInvoiceNum, "E" + iRow.ToString(), "E" + iRow.ToString(), "", "C");
                            addData(o360Worksheet, iRow, 6, row[3].ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "$#,##0.00", "C");
                            iRow++;
                            decTotalInvAmt += Convert.ToDecimal(row[3]);

                        } // foreach


                        // Total
                        addData(o360Worksheet, iRow, 5, "Total", "E" + iRow.ToString(), "E" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 6, decTotalInvAmt.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "$#,##0.00", "C");

                        // Save Excel
                        oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                        string sPDFFile = sExcelFileNameNoEx + ".pdf";
                        oWorkbook.ExportAsFixedFormat2(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, sPDFFile);
                        oWorkbook.Close(true, oMissing, oMissing);
                        oExcel.Quit();
                        msMsgBody += "Processing completed" + System.Environment.NewLine + cfg_outputdir + sExcelFileNameNoEx + System.Environment.NewLine + System.Environment.NewLine;

                        releaseObject(oExcel);
                        releaseObject(oWorkbook);
                        releaseObject(o360Worksheet);
                        sendExcelFile(sEmailSubject, sPDFFile);
                        msMsgBody += "File sent " + sExcelFileName;

                    }   //(dtFinal.Rows.Count > 0)

                }   // try

                catch (Exception ex)
                {
                    //record exception   
                    oLU.WritetoLog(ex.Message);
                    mbErr = true;
                    msErrMsg = ex.Message;
                }

            }

            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    //sendEmail(msMsgBody);
                }

            }
        }


        static bool getInvoicedItems(Guid guInvoiceID, bool bAdmitted)
        {

            bool bRet = false;
            int iCaseNum = 0;
            string sPolicy = "";

            string sInsured = "";
            string sAddress1 = "";
            string sAddress2 = "";
            string sCity = "";
            string sState = "";
            string sZip = "";
            DateTime dCompleted;
            string sUWFirst = "";
            string sUWLast = "";
            decimal dcAmtBilled = 0;
            string sCaseType = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // init SQL connection for read query
                SqlDataReader sqlDR;
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;


                // get items on invoice
                sqlCmd1.CommandText = "sp_GetCasesOnInvoice";
                sqlCmd1.Parameters.AddWithValue("@InvoiceID", guInvoiceID);
                sqlConn1.Open();
                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        iCaseNum = (int)sqlDR.GetSqlInt32(0);

                        sPolicy = getSQLString(sqlDR, 1).ToString();
                        sInsured = getSQLString(sqlDR, 2).ToString();
                        sAddress1 = getSQLString(sqlDR, 3).ToString();
                        sAddress2 = getSQLString(sqlDR, 4).ToString();
                        sCity = getSQLString(sqlDR, 5).ToString();
                        sState = getSQLString(sqlDR, 6).ToString();
                        sZip = getSQLString(sqlDR, 7).ToString();
                        dCompleted = (DateTime)sqlDR.GetSqlDateTime(8);
                        sUWFirst = getSQLString(sqlDR, 9).ToString();
                        sUWLast = getSQLString(sqlDR, 10).ToString();
                        dcAmtBilled = (decimal)(sqlDR.GetSqlDecimal(11));
                        sCaseType = getSQLString(sqlDR, 12).ToString();


                        dt360.Rows.Add(iCaseNum, sPolicy, sInsured, dcAmtBilled);

                    }	// while


                }   // has rows
                sqlDR.Close();

                bRet = true;

            }
            catch (Exception ex)
            {

                oLU.WritetoLog(ex.Message);

            }

            finally
            {
                if (sqlConn1 != null)

                    sqlConn1.Close();
            }

            return bRet;
        }

        public static string getSQLString(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return string.Empty;
        }

        static bool getInvoice(DateTime dInvoiceDate, string sCustNum)
        {

            // Get invoice info for customer / date

            bool bRet = false;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // init SQL connection for read query
                SqlDataReader sqlDR;
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;

                sqlCmd1.CommandText = "sp_GetInvoiceForCust";
                sqlCmd1.Parameters.AddWithValue("@invdate", dInvoiceDate);
                sqlCmd1.Parameters.AddWithValue("@CustNum", sCustNum);
                sqlConn1.Open();
                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        if (sqlDR.IsDBNull(0))
                        {
                            decTotalInvAmt = 0;
                        }
                        else
                        {
                            decTotalInvAmt = (decimal)(sqlDR.GetSqlMoney(0));
                        }

                        iInvoiceNumber = (int)sqlDR.GetSqlInt32(1);

                        guInvoiceID = (Guid)sqlDR.GetSqlGuid(2);

                    }	// while


                }   // has rows
                sqlDR.Close();

                bRet = true;

            }
            catch (Exception ex)
            {

                oLU.WritetoLog(ex.Message);

            }

            finally
            {
                if (sqlConn1 != null)

                    sqlConn1.Close();
            }

            return bRet;
        }

       

    

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeader(Excel._Worksheet oWorkSheet)
        {

            string sCustName = cfg_CustName;

            oWorkSheet.Shapes.AddPicture("C:\\temp\\SuttonLogo.jpg", Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoCTrue, 0, 0, 117, 56);

            Excel.Range oRange;

            oWorkSheet.get_Range("B1", "F1").Merge(false);
            oRange = oWorkSheet.get_Range("B1", "F1");
            oRange.FormulaR1C1 = "Sutton Inspection Bureau, Inc.";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 14;
            oRange.Font.Bold = true;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            //oWorkSheet.get_Range("D1", "E1").Merge(false);
            //oRange = oWorkSheet.get_Range("D1", "E1");
            //oRange.FormulaR1C1 = "INVOICE ";
            //oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            //oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            //oRange.Font.Size = 18;
            //oRange.Font.Bold = false;
            //oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.get_Range("B2", "F2").Merge(false);
            oRange = oWorkSheet.get_Range("B2", "F2");
            oRange.FormulaR1C1 = "5656 Central Avenue";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 10;
            oRange.Font.Bold = false;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            //oWorkSheet.get_Range("D2", "E2").Merge(false);
            //oRange = oWorkSheet.get_Range("D2", "E2");
            //oRange.FormulaR1C1 = "Invoice date: 6/1/2021";
            //oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            //oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            //oRange.Font.Size = 10;
            //oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);


            oWorkSheet.get_Range("B3", "F3").Merge(false);
            oRange = oWorkSheet.get_Range("B3", "F3");
            oRange.FormulaR1C1 = "St. Petersburg, FL 33707";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 10;
            oRange.Font.Bold = false;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            oRange = oWorkSheet.get_Range("A4", "F4");
            oRange.FormulaR1C1 = " ";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 9;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGray);
 
            oWorkSheet.get_Range("A5", "C5").Merge(false);
            oRange = oWorkSheet.get_Range("A5", "C5");
            oRange.FormulaR1C1 = sCustName;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 12;
            oRange.Font.Bold = true;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            oWorkSheet.get_Range("D5", "F5").Merge(false);
            oRange = oWorkSheet.get_Range("D5", "F5");
            oRange.FormulaR1C1 = "Invoice #: " + sInvoiceNum;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 12;
            oRange.Font.Bold = true;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            oWorkSheet.get_Range("A6", "C6").Merge(false);
            oRange = oWorkSheet.get_Range("A6", "C6");
            oRange.FormulaR1C1 = cfg_CustAddress + " " + cfg_CustCSZ;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 10;
            oRange.Font.Bold = false;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            oWorkSheet.get_Range("D6", "F6").Merge(false);
            oRange = oWorkSheet.get_Range("D6", "F6");
            oRange.FormulaR1C1 = "Invoice date: " + sInvoiceDispDate;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 10;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            oRange = oWorkSheet.get_Range("A7", "F7");
            oRange.FormulaR1C1 = " ";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 9;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGray);

            oWorkSheet.get_Range("A8", "D8").Merge(false);
            oRange = oWorkSheet.get_Range("A8", "D8");
            oRange.FormulaR1C1 = "Agreement Number: 21-20-0022-03              Contract Manager: Christina Jones";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 9;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            //oWorkSheet.get_Range("C8", "D8").Merge(false);
            //oRange = oWorkSheet.get_Range("C8", "D8");
            //oRange.FormulaR1C1 = "Contractor Name – Jeremy Pope";
            //oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            //oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            //oRange.Font.Size = 9;
            //oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            oWorkSheet.get_Range("E8", "F8").Merge(false);
            oRange = oWorkSheet.get_Range("E8", "F8");
            oRange.FormulaR1C1 = "Sutton FEIN – 59 2056754";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 9;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            oRange = oWorkSheet.get_Range("A9", "F9");
            oRange.FormulaR1C1 = " ";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 10;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGray);
            
            oWorkSheet.Cells[10, 1] = "Policy";
            oRange = oWorkSheet.get_Range("A10", "A10");
            oRange.ColumnWidth = 12;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[10, 2] = "Insured";
            oRange = oWorkSheet.get_Range("B10", "B10");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[10, 3] = "Date";
            oRange = oWorkSheet.get_Range("C10", "C10");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[10, 4] = "Key";
            oRange = oWorkSheet.get_Range("D10", "D10");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[10, 5] = "Ref #";
            oRange = oWorkSheet.get_Range("E10", "E10");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[10, 6] = "Amount";
            oRange = oWorkSheet.get_Range("F10", "F10");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        }




        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2, string format, string sHorizAlign)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            oRange.Font.Size = 8;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else if (sHorizAlign == "R")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
        }

        static void addDataWColor(Excel._Worksheet oWorkSheet, int row, int col, string data,
           string cell1, string cell2, string format, string sHorizAlign, int ibgColor)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }

            if (ibgColor == 1)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.PaleGreen);
            }
            else if (ibgColor == 2)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightPink);
            }
            else if (ibgColor == 3)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            }

        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Font.Size = 8;
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }

        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "RPS Monthly Invoice";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by CitizensInvoicing Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = cfg_deliverto;

                //oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }
    }
}
