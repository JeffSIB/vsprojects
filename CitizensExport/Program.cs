﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Net.Mail;

namespace CitizensExport
{
    class Program
    {

        static int Main(string[] args)
        {

            int iRetCode = 0;

            Setup setup = new Setup();

            iRetCode = setup.iCaseCount;

            return iRetCode;
        }
    }

    /// <summary>
    /// Instantiate properties from the AppSettings located in the App.config.
    /// Run exporter.
    /// Write log file.
    /// </summary>
    public class Setup
    {
        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties
 
        private string companyName = ConfigurationManager.AppSettings["CompanyName"];
        private string customerName = ConfigurationManager.AppSettings["CustomerName"];
        private string customersToExport = ConfigurationManager.AppSettings["CustomersToExport"];
        private string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        private string logsDir = ConfigurationManager.AppSettings["LogsDir"] + @"\" + DateTime.Now.Year.ToString() + @"\" + DateTime.Now.Month.ToString().PadLeft(2, '0') + @"\" + DateTime.Now.Day.ToString().PadLeft(2, '0');
        private string returnFormData = ConfigurationManager.AppSettings["ReturnFormData"];
        private string returnPDF = ConfigurationManager.AppSettings["ReturnPDF"];
        private string returnRawImages = ConfigurationManager.AppSettings["ReturnRawImages"];
        private string emailSendToLive = ConfigurationManager.AppSettings["EmailSendToLive"];
        private string emailFromLive = ConfigurationManager.AppSettings["EmailFromLive"];
        private string emailSendToDebug = ConfigurationManager.AppSettings["EmailSendToDebug"];
        private string emailFromDebug = ConfigurationManager.AppSettings["EmailFromDebug"];

        private string emailErrorSendTo = ConfigurationManager.AppSettings["EmailErrorSendTo"];
        private string pdfdir = ConfigurationManager.AppSettings["pdfdir"];

        private string SQLConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];

        public int iCaseCount { get; set; }      
        
        //Used for email message body.  Not currently being used.
        private StringBuilder email = new StringBuilder();

        #endregion

        public Setup()
        {
            iCaseCount = 0;

            try
            {
                //Create log directory.
                Directory.CreateDirectory(logsDir);

                //Start building the email using StringBuilder.
                email.Append("Started " + customerName + " Export For " + companyName + " " + DateTime.Now + Environment.NewLine);

                //Instantiate export properties which will be used in the Class.
                CitizensExport export = new CitizensExport 
                {
                    CompanyName = companyName,
                    CustomerName = customerName,
                    CustomersToExport = customersToExport,
                    CompletedMinDate = completedMinDate,
                    LogsDir = logsDir,
                    ReturnFormData = returnFormData,
                    ReturnPDF = returnPDF,
                    ReturnRawImages = returnRawImages,
                    EmailSendToLive = emailSendToLive,
                    EmailFromLive = emailFromLive,
                    EmailSendToDebug = emailSendToDebug,
                    EmailFromDebug = emailFromDebug,
                    PDFDir = pdfdir,
                    cfg_360UtilConnStr= SQLConnStr
                };

                //Export logger 
                export.LogReported += new LC360Data.ReportLogHandler(import_LogReported);

                //Run Exporter
                Executer.Controller.ExecutePorter(export);

                // get number of exported cases
                iCaseCount = export.ExportCount;
                
                //Write log file to directory specified in AppSettings located in the App.config file.
                System.IO.File.WriteAllText(logsDir + @"\" + export.SessionID + "_log.txt", email.ToString());

                // look for errors  
                if (email.ToString().ToUpper().Contains("ERROR"))
                {
                    throw new Exception("Exporter returned error \r\n\r\n" + email.ToString());
                }

            }
            catch (Exception ex)
            {
                SendErrorEmail(ex.Message);
            }

        }

        /// <summary>
        /// Log Handler
        /// </summary>
        /// <param name="log"></param>
        /// <param name="porter"></param>
        void import_LogReported(LC360Data.Log log, LC360Data.IPortBase porter)
        {
            email.Append(Environment.NewLine + "Code: " + log.LogCode.ToString() + " - " + log.Decription);
            Console.WriteLine(Environment.NewLine + "Code: " + log.LogCode.ToString() + " - " + log.Decription);
        }

        void SendErrorEmail(string sText)
        {
            try
            {
#if DEBUG
                string from = emailFromDebug;
                string to = emailLogs;
#else
                string from = emailFromLive;
                string to = emailErrorSendTo;
#endif

                using (MailMessage msg = new MailMessage())
                {
                    msg.From = new MailAddress(from);
                    msg.To.Add(new MailAddress(to));
                    msg.Subject = "**** Citizens Export Error ****";

                    msg.IsBodyHtml = false;

                    msg.Body = sText;

                    Send(msg);
                }
            }
            catch
            {
            }
        }

        static void Send(MailMessage message)
        {
            try
            {
                SmtpClient client = new SmtpClient();
                client.Send(message);
            }
            catch
            {

            }
        }
    }
}