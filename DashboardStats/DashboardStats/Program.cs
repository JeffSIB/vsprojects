using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Globalization;
using System.Threading;


namespace DashboardStats
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static DashboardStats360.Program oDB360;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_SIBIConnStr;
        static string cfg_SIBUtilConnStr;
        static string cfg_360UtilConnStr;
        static string cfg_InspHoldDir;
        static int cfg_TimeZoneOffset;

        static void Main(string[] args)
        {
            string sErr = "None";

            try
            {

                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //  A = accounting summary email - 7pm
                //  P = accounting summary email - 12:05am
                //  C = weekly client update
                //  E = end of day
                //  F = full
                if (args.Length < 1)
                {
                    foreach (string s in args)
                    {
                        sErr += s + " - ";
                    }
                    throw new SystemException("Invalid number of arguments supplied: " + sErr);
                }

                //////////////////////////////////////////////////////////

                DateTime dt = new DateTime(); 
                dt = DateTime.Today;

                ///////////////////////////////////////////////////////////////


                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_SIBIConnStr = colNameVal.Get("SIBIConnStr");
                cfg_SIBUtilConnStr = colNameVal.Get("SIBUtilConnStr");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_InspHoldDir = colNameVal.Get("inspholddir");
                cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));

                oDB360 = new DashboardStats360.Program();

                //// initialize log file class
                //oLU = new LogUtils.LogUtils();

                //// set log file name
                //oLU.logFileName = cfg_logfilename;

                //// open log file
                //oLU.OpenLog();
                //oLU.WritetoLog("Begin session");

                if (args[0].ToUpper() == "C")
                {

                    DateTime dCHDate = DateTime.Today;
                    buildClientHist(dCHDate);
                }
                else if (args[0].ToUpper() == "A")
                {
                    // Accounting summary for current day 
                    DateTime dTempDate = DateTime.Now;

                    // Note: Accounting Summary calculates monthly values for the date passed. Those values are 'as of' the time it was run.
                    //oLU.WritetoLog("Call AS");
                    AccountingSummary(dTempDate);
                    //DateTime dReportDate = DateTime.Today;
                    //AccountingSummaryPD(dReportDate);

                }
                else if (args[0].ToUpper() == "P")
                {
                    // Accounting summary - run after midnight for previous day
                    DateTime dReportDate = DateTime.Today;
                    dReportDate = dReportDate.AddDays(-1);
                    AccountingSummaryPD(dReportDate);
                }

                else if (args[0].ToUpper() == "E")
                {
                    /////////////////////////////////////
                    // CALL AFTER MIDNIGHT
                    DateTime dDate = DateTime.Today.AddDays(-1);
                    DateTime d360Date = dDate.AddHours(cfg_TimeZoneOffset);

                    //Day(dDate);
                    //oDB360.Day(d360Date);

                    //Week(dDate);
                    //oDB360.Week(d360Date);

                    //Month(dDate);
                    //oDB360.Month(d360Date);

                    SalesHist(dDate);

                    MonthlySalesHist(dDate);

                    oDB360.SalesHist(d360Date);
                    oDB360.MonthlySalesHist(d360Date);


                }
                else if (args[0].ToUpper() == "F")
                {
                    DateTime dDate = DateTime.Today;
                    DateTime d360Date = dDate.AddHours(cfg_TimeZoneOffset);

                    Full();
                    oDB360.Full();

                    Day(dDate);
                    oDB360.Day(d360Date);

                    Week(dDate);
                    oDB360.Week(d360Date);

                    Month(dDate);
                    oDB360.Month(d360Date);

                }
                else
                {
                    throw new SystemException("Invalid arguments supplied: " + args[0].ToUpper());
                }
 
            }

            catch (Exception ex)
            {

                //record exception  
                //oLU.WritetoLog(ex.Message);

            }

            //oLU.closeLog();

        }

        static void Full()
        {

            String sStatDate = DateTime.Now.ToShortDateString();
            DateTime dStatDate = DateTime.Parse(sStatDate);
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iKeyNumber = 0;
            int iAcnt = 0;
            string sInsp = "";
            int iRet = 0;
            int idaysold = 0;
            bool bAssigned = false;
            string sAssigner1 = "";
            string sAssigner2 = "";

            int itotalopen = 0;
            int iunder30 = 0;
            int i30to59 = 0;
            int i60to89 = 0;
            int i90to120 = 0;
            int iover120 = 0;
            int iRush = 0;
            int iInReview = 0;
            int iPendingAssign = 0;
            int iOldestCaseDays = 0;
            int iOldestCaseKey = 0;
            int iNewCases = 0;
            int iCompleted = 0;
            int iAssign1 = 0;
            int iAssign2 = 0;
            bool bLocked = false;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;

                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_SIBIConnStr = colNameVal.Get("SIBIConnStr");
                cfg_SIBUtilConnStr = colNameVal.Get("SIBUtilConnStr");
                sAssigner1 = colNameVal.Get("assigner1");
                sAssigner2 = colNameVal.Get("assigner2");

                cfg_InspHoldDir = colNameVal.Get("inspholddir");

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin SIB Full ++++");

                // set up SQL connection (SIBTBL)
                sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_DBStats_GetOpen";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();


                // Get all open keys from SibTBL
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through open keys
                    do
                    {

                        //[keynumber],
                        //datediff(day,datereq,getdate())  as daysold,
                        //[insp],
                        //[acnt_num],
                        //[reviewed_flag],
                        //[reviewed_by]

                        //key
                        if (sqlReader.IsDBNull(0))
                        {
                            throw new SystemException("SQL returned empty Key#");
                        }
                        else
                        {
                            iKeyNumber = (int)sqlReader.GetSqlInt32(0);
                        }

                        //daysold
                        if (sqlReader.IsDBNull(1))
                        {
                            throw new SystemException("SQL returned empty DaysOld");
                        }
                        else
                        {
                            idaysold = (int)sqlReader.GetSqlInt32(1);
                        }

                        // insp
                        if (sqlReader.IsDBNull(2))
                        {
                            throw new SystemException("SQL returned empty Insp#");
                        }
                        else
                        {
                            sInsp = sqlReader.GetSqlString(2).ToString();
                        }

                        //acnt
                        if (sqlReader.IsDBNull(3))
                        {
                            throw new SystemException("SQL returned empty Acnt#");
                        }
                        else
                        {
                            iAcnt = (int)sqlReader.GetSqlInt32(3);
                        }

                        // reviewed_flag
                        if (sqlReader.IsDBNull(4))
                        {
                            throw new SystemException("SQL returned empty Reviewed_Flag");
                        }
                        else
                        {
                            bAssigned = (bool)sqlReader.GetSqlBoolean(4);
                        }

                        bLocked = sqlReader.GetBoolean(6);


                        // total open items
                        itotalopen += 1;

                        // Pending assign

                        if (bAssigned == false)
                            iPendingAssign += 1;

                        // oldest case
                        if (idaysold > iOldestCaseDays)
                        {
                            iOldestCaseDays = idaysold;
                            iOldestCaseKey = iKeyNumber;
                        }
                        // increment aging counters
                        if (idaysold < 30)
                        {
                            iunder30 += 1;
                        }
                        else if (idaysold > 29 && idaysold < 60)
                        {
                            i30to59 += 1;
                        }
                        else if (idaysold > 59 && idaysold < 90)
                        {
                            i60to89 += 1;
                        }
                        else if (idaysold > 89 && idaysold < 121)
                        {
                            i90to120 += 1;
                        }
                        else if (idaysold > 120)
                        {
                            iover120 += 1;
                        }

                        //if (bLocked)
                        //    iInReview += 1;

                        //} while (itotalopen < 1);     // open keys

                    } while (sqlReader.Read());     // open keys

                    sqlReader.Close();

                    // get new requests
                    sqlCmd1.CommandText = "sp_Count_NewReq";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@curdate", sStatDate);
                    iNewCases = (int)sqlCmd1.ExecuteScalar();

                    // get completed
                    sqlCmd1.CommandText = "sp_Count_Completed";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@curdate", sStatDate);
                    iCompleted = (int)sqlCmd1.ExecuteScalar();

                    // get in Review
                    sqlCmd1.CommandText = "sp_Count_InReview";
                    sqlCmd1.Parameters.Clear();
                    iInReview = (int)sqlCmd1.ExecuteScalar();

                    //// get assign by 1
                    //sqlCmd1.CommandText = "sp_Count_AssignBy";
                    //sqlCmd1.Parameters.Clear();
                    //sqlCmd1.Parameters.AddWithValue("@curdate", sStatDate);
                    //iAssign1 = (int)sqlCmd1.ExecuteScalar();

                    sqlConn1.Close();
                }



                // set up SQL connection 
                sqlConn2 = new SqlConnection(cfg_SIBUtilConnStr);
                sqlCmd2 = new SqlCommand();

                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "sp_Dashboard_Insert";
                sqlCmd2.Connection = sqlConn2;
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlConn2.Open();
                sqlCmd2.Parameters.Clear();

                //@statusdate datetime,
                //@opencases int,
                //@pendingreview int,
                //@pendingassign int,
                //@assignby1 int,
                //@assignby2 int,
                //@assignby3 int,
                //@completed int,
                //@newcases  int,
                //@oldestopen int,
                //@openunder30 int,
                //@open30to59 int,
                //@open60to89 int,
                //@open90to120 int,
                //@openover120 int

                sqlCmd2.Parameters.AddWithValue("@statusdate", dStatDate);
                sqlCmd2.Parameters.AddWithValue("@statustime", sStatTime);
                sqlCmd2.Parameters.AddWithValue("@opencases", itotalopen);
                sqlCmd2.Parameters.AddWithValue("@pendingreview", iInReview);
                sqlCmd2.Parameters.AddWithValue("@pendingassign", iPendingAssign);
                sqlCmd2.Parameters.AddWithValue("@assignby1", 0);
                sqlCmd2.Parameters.AddWithValue("@assignby2", 0);
                sqlCmd2.Parameters.AddWithValue("@assignby3", 0);
                sqlCmd2.Parameters.AddWithValue("@completed", iCompleted);
                sqlCmd2.Parameters.AddWithValue("@newcases", iNewCases);
                sqlCmd2.Parameters.AddWithValue("@oldestopen", iOldestCaseDays);
                sqlCmd2.Parameters.AddWithValue("@openunder30", iunder30);
                sqlCmd2.Parameters.AddWithValue("@open30to59", i30to59);
                sqlCmd2.Parameters.AddWithValue("@open60to89", i60to89);
                sqlCmd2.Parameters.AddWithValue("@open90to120", i90to120);
                sqlCmd2.Parameters.AddWithValue("@openover120", iover120);

                iRet = sqlCmd2.ExecuteNonQuery();

                if (iRet == 0)
                    throw new SystemException("spDBStat_Insert returned 0 for " + iKeyNumber.ToString());

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End SIB Full ----");
                oLU.closeLog();

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn2.Close();

            }

        
        }




        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;
        
        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;
            
            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }

        //static void EndofDay(DateTime dDate)
        //{

        //    // MUST BE RUN AFTER MIDNIGHT to catpure stats for previous day
        //    // updates previous day, weekly and monthly totals
        //    CultureInfo info = Thread.CurrentThread.CurrentCulture;

        //    //DateTime dNow = DateTime.Today;
        //    DateTime dNow = dDate;
        //    DateTime dYesterday = dNow.AddDays(-1);
        //    DateTime dBegOfWeek = FirstDayofWeek(dYesterday);
        //    DateTime dBegOfMonth = FirstDayOfMonth(dYesterday);

        //    //String sStatDate = DateTime.Now.ToShortDateString();
        //    //DateTime dStatDate = DateTime.Parse(sStatDate);
        //    String sWorkDate = dYesterday.ToShortDateString();
        //    String sWorkDatePlus1 = dYesterday.AddDays(1).ToShortDateString();
        //    String sStatDate = dDate.ToShortDateString();
        //    DateTime dStatDate = dDate;
        //    String sStatTime = dDate.ToShortTimeString();

        //    int iNewCases = 0;
        //    int iCompleted = 0;
        //    int iReturned = 0;
        //    int iTraced = 0;
        //    decimal dAmountInv = 0;
        //    decimal dInspPaid = 0;

        //    int iRet = 0;

        //    SqlConnection sqlConn1 = null;
        //    SqlCommand sqlCmd1 = null;
        //    SqlConnection sqlConn2 = null;
        //    SqlCommand sqlCmd2 = null;

        //    try
        //    {

        //        // initialize log file class
        //        oLU = new LogUtils.LogUtils();

        //        // set log file name
        //        oLU.logFileName = cfg_logfilename;

        //        // open log file
        //        oLU.OpenLog();
        //        oLU.WritetoLog("++++ Begin End of Day ++++");

        //        // set up SQL connection (SIBTBL)
        //        sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
        //        sqlCmd1 = new SqlCommand();
        //        sqlCmd1.CommandType = CommandType.StoredProcedure;
        //        sqlCmd1.Connection = sqlConn1;
        //        sqlConn1.Open();

        //        // get new requests
        //        sqlCmd1.CommandText = "sp_Count_NewReq";
        //        sqlCmd1.Parameters.Clear();
        //        sqlCmd1.Parameters.AddWithValue("@curdate", sWorkDate);
        //        iNewCases = (int)sqlCmd1.ExecuteScalar();

        //        // get completed
        //        sqlCmd1.CommandText = "sp_Count_Completed";
        //        sqlCmd1.Parameters.Clear();
        //        sqlCmd1.Parameters.AddWithValue("@curdate", sWorkDate);
        //        iCompleted = (int)sqlCmd1.ExecuteScalar();

        //        // get returned
        //        sqlCmd1.CommandText = "sp_Count_Returned";
        //        sqlCmd1.Parameters.Clear();
        //        sqlCmd1.Parameters.AddWithValue("@begdate", sWorkDate);
        //        sqlCmd1.Parameters.AddWithValue("@enddate", sWorkDatePlus1);
        //        iReturned = (int)sqlCmd1.ExecuteScalar();

        //        // get traced
        //        sqlCmd1.CommandText = "sp_Count_Traced";
        //        sqlCmd1.Parameters.Clear();
        //        sqlCmd1.Parameters.AddWithValue("@begdate", sWorkDate);
        //        sqlCmd1.Parameters.AddWithValue("@enddate", sWorkDatePlus1);
        //        iTraced = (int)sqlCmd1.ExecuteScalar();

        //        // get amount invoiced
        //        sqlCmd1.CommandText = "sp_Sum_InvAmt_Range";
        //        sqlCmd1.Parameters.Clear();
        //        sqlCmd1.Parameters.AddWithValue("@begdate", sWorkDate);
        //        sqlCmd1.Parameters.AddWithValue("@enddate", sWorkDatePlus1);
        //        dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

        //        // get amount paid to the inspectors
        //        sqlCmd1.CommandText = "sp_Sum_InspChgs_Range";
        //        sqlCmd1.Parameters.Clear();
        //        sqlCmd1.Parameters.AddWithValue("@begdate", sWorkDate);
        //        sqlCmd1.Parameters.AddWithValue("@enddate", sWorkDatePlus1);
        //        dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

        //        sqlConn1.Close();

        //        // set up SQL connection 
        //        sqlConn2 = new SqlConnection(cfg_SIBUtilConnStr);
        //        sqlCmd2 = new SqlCommand();

        //        ////////////////////////////////////////////////////////////////
        //        // Yesterday
        //        ////////////////////////////////////////////////////////////////
        //        sqlCmd2.CommandType = CommandType.StoredProcedure;
        //        sqlCmd2.CommandText = "sp_Dashboard_Stats_Overwrite";
        //        sqlCmd2.Connection = sqlConn2;
        //        sqlCmd2.CommandType = CommandType.StoredProcedure;
        //        sqlConn2.Open();
        //        sqlCmd2.Parameters.Clear();

        //        sqlCmd2.Parameters.AddWithValue("@stattype", "D");
        //        sqlCmd2.Parameters.AddWithValue("@statusdate", dStatDate);
        //        sqlCmd2.Parameters.AddWithValue("@statusperiod", dYesterday);
        //        sqlCmd2.Parameters.AddWithValue("@statustime", sStatTime);
        //        sqlCmd2.Parameters.AddWithValue("@newcases", iNewCases);
        //        sqlCmd2.Parameters.AddWithValue("@completed", iCompleted);
        //        sqlCmd2.Parameters.AddWithValue("@returneditems", iReturned);
        //        sqlCmd2.Parameters.AddWithValue("@traceditems", iTraced);
        //        sqlCmd2.Parameters.AddWithValue("@amtinvoiced", dAmountInv);
        //        sqlCmd2.Parameters.AddWithValue("@insppaid", dInspPaid);

        //        iRet = sqlCmd2.ExecuteNonQuery();

        //        if (iRet == 0)
        //            throw new SystemException("sp_Dashboard_Stats_Overwrite returned 0");

        //        ////////////////////////////////////////////////////////////////
        //        // Week
        //        ////////////////////////////////////////////////////////////////
        //        sqlCmd2.CommandType = CommandType.StoredProcedure;
        //        sqlCmd2.CommandText = "sp_Dashboard_Stats_Append";
        //        sqlCmd2.Connection = sqlConn2;
        //        sqlCmd2.CommandType = CommandType.StoredProcedure;
        //        sqlCmd2.Parameters.Clear();

        //        sqlCmd2.Parameters.AddWithValue("@stattype", "W");
        //        sqlCmd2.Parameters.AddWithValue("@statusdate", dStatDate);
        //        sqlCmd2.Parameters.AddWithValue("@statusperiod", dBegOfWeek);
        //        sqlCmd2.Parameters.AddWithValue("@statustime", sStatTime);
        //        sqlCmd2.Parameters.AddWithValue("@newcases", iNewCases);
        //        sqlCmd2.Parameters.AddWithValue("@completed", iCompleted);
        //        sqlCmd2.Parameters.AddWithValue("@returneditems", iReturned);
        //        sqlCmd2.Parameters.AddWithValue("@traceditems", iTraced);
        //        sqlCmd2.Parameters.AddWithValue("@onhold", 0);
        //        sqlCmd2.Parameters.AddWithValue("@receivedfrominsp", 0);

        //        iRet = sqlCmd2.ExecuteNonQuery();

        //        if (iRet == 0)
        //            throw new SystemException("sp_Dashboard_Stats_Overwrite returned 0");

        //        ////////////////////////////////////////////////////////////////
        //        // Month
        //        ////////////////////////////////////////////////////////////////
        //        sqlCmd2.CommandType = CommandType.StoredProcedure;
        //        sqlCmd2.CommandText = "sp_Dashboard_Stats_Append";
        //        sqlCmd2.Connection = sqlConn2;
        //        sqlCmd2.CommandType = CommandType.StoredProcedure;
        //        sqlCmd2.Parameters.Clear();

        //        sqlCmd2.Parameters.AddWithValue("@stattype", "M");
        //        sqlCmd2.Parameters.AddWithValue("@statusdate", dStatDate);
        //        sqlCmd2.Parameters.AddWithValue("@statusperiod", dBegOfMonth);
        //        sqlCmd2.Parameters.AddWithValue("@statustime", sStatTime);
        //        sqlCmd2.Parameters.AddWithValue("@newcases", iNewCases);
        //        sqlCmd2.Parameters.AddWithValue("@completed", iCompleted);
        //        sqlCmd2.Parameters.AddWithValue("@returneditems", iReturned);
        //        sqlCmd2.Parameters.AddWithValue("@traceditems", iTraced);
        //        sqlCmd2.Parameters.AddWithValue("@onhold", 0);
        //        sqlCmd2.Parameters.AddWithValue("@receivedfrominsp", 0);

        //        iRet = sqlCmd2.ExecuteNonQuery();

        //        if (iRet == 0)
        //            throw new SystemException("sp_Dashboard_Stats_Append returned 0");


        //    }

        //    catch (Exception ex)
        //    {

        //        //record exception  
        //        oLU.WritetoLog(ex.Message);

        //    }

        //    finally
        //    {

        //        // close objects
        //        oLU.WritetoLog("---- End of Day ----");
        //        oLU.closeLog();

        //        if (sqlConn1 != null)
        //            sqlConn1.Close();

        //        if (sqlConn2 != null)
        //            sqlConn2.Close();

        //    }



        //}

        //static void Today()
        //{

        //    String sStatDate = DateTime.Now.ToShortDateString();
        //    String sStatDatePlus1 = DateTime.Now.AddDays(1).ToShortDateString();
        //    DateTime dStatDate = DateTime.Parse(sStatDate);
        //    String sStatTime = DateTime.Now.ToShortTimeString();

        //    int iNewCases = 0;
        //    int iCompleted = 0;
        //    int iReturned = 0;
        //    int iTraced = 0;
        //    int iRet = 0;

        //    SqlConnection sqlConn1 = null;
        //    SqlCommand sqlCmd1 = null;
        //    SqlConnection sqlConn2 = null;
        //    SqlCommand sqlCmd2 = null;

        //    try
        //    {

        //        // load configuration values from app.config
        //        System.Collections.Specialized.NameValueCollection colNameVal;
        //        colNameVal = System.Configuration.ConfigurationManager.AppSettings;

        //        cfg_smtpserver = colNameVal.Get("smtpserver");
        //        cfg_logfilename = colNameVal.Get("logfilename");
        //        cfg_SIBIConnStr = colNameVal.Get("SIBIConnStr");
        //        cfg_SIBUtilConnStr = colNameVal.Get("SIBUtilConnStr");

        //        // initialize log file class
        //        oLU = new LogUtils.LogUtils();

        //        // set log file name
        //        oLU.logFileName = cfg_logfilename;

        //        // open log file
        //        oLU.OpenLog();

        //        // set up SQL connection (SIBTBL)
        //        sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
        //        sqlCmd1 = new SqlCommand();
        //        sqlCmd1.CommandType = CommandType.StoredProcedure;
        //        sqlCmd1.Connection = sqlConn1;
        //        sqlConn1.Open();

        //        // get new requests
        //        sqlCmd1.CommandText = "sp_Count_NewReq";
        //        sqlCmd1.Parameters.Clear();
        //        sqlCmd1.Parameters.AddWithValue("@curdate", sStatDate);
        //        iNewCases = (int)sqlCmd1.ExecuteScalar();

        //        // get completed
        //        sqlCmd1.CommandText = "sp_Count_Completed";
        //        sqlCmd1.Parameters.Clear();
        //        sqlCmd1.Parameters.AddWithValue("@curdate", sStatDate);
        //        iCompleted = (int)sqlCmd1.ExecuteScalar();

        //        // get returned
        //        sqlCmd1.CommandText = "sp_Count_Returned";
        //        sqlCmd1.Parameters.Clear();
        //        sqlCmd1.Parameters.AddWithValue("@begdate", sStatDate);
        //        sqlCmd1.Parameters.AddWithValue("@enddate", sStatDatePlus1);
        //        iReturned = (int)sqlCmd1.ExecuteScalar();

        //        // get traced
        //        sqlCmd1.CommandText = "sp_Count_Traced";
        //        sqlCmd1.Parameters.Clear();
        //        sqlCmd1.Parameters.AddWithValue("@begdate", sStatDate);
        //        sqlCmd1.Parameters.AddWithValue("@enddate", sStatDatePlus1);
        //        iTraced = (int)sqlCmd1.ExecuteScalar();


        //        sqlConn1.Close();

        //        // set up SQL connection 
        //        sqlConn2 = new SqlConnection(cfg_SIBUtilConnStr);
        //        sqlCmd2 = new SqlCommand();

        //        sqlCmd2.CommandType = CommandType.StoredProcedure;
        //        sqlCmd2.CommandText = "sp_Dashboard_Stats_Overwrite";
        //        sqlCmd2.Connection = sqlConn2;
        //        sqlCmd2.CommandType = CommandType.StoredProcedure;
        //        sqlConn2.Open();
        //        sqlCmd2.Parameters.Clear();

        //        sqlCmd2.Parameters.AddWithValue("@stattype", "D");
        //        sqlCmd2.Parameters.AddWithValue("@statusdate", dStatDate);
        //        sqlCmd2.Parameters.AddWithValue("@statusperiod", dStatDate);
        //        sqlCmd2.Parameters.AddWithValue("@statustime", sStatTime);
        //        sqlCmd2.Parameters.AddWithValue("@newcases", iNewCases);
        //        sqlCmd2.Parameters.AddWithValue("@completed", iCompleted);
        //        sqlCmd2.Parameters.AddWithValue("@returneditems", iReturned);
        //        sqlCmd2.Parameters.AddWithValue("@traceditems", iTraced);
        //        sqlCmd2.Parameters.AddWithValue("@onhold", 0);
        //        sqlCmd2.Parameters.AddWithValue("@receivedfrominsp", 0);
                
        //        iRet = sqlCmd2.ExecuteNonQuery();

        //        if (iRet == 0)
        //            throw new SystemException("sp_Dashboard_Stats_Overwrite returned 0");

        //    }

        //    catch (Exception ex)
        //    {

        //        //record exception  
        //        oLU.WritetoLog(ex.Message);

        //    }

        //    finally
        //    {

        //        // close objects
        //        oLU.closeLog();

        //        if (sqlConn1 != null)
        //            sqlConn1.Close();

        //        if (sqlConn2 != null)
        //            sqlConn2.Close();

        //    }


        //}

        static void Day(DateTime dDate)
        {

            // Creates day row for date passed
            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dDatePlus1 = dDate.AddDays(1);

            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iNewCases = 0;
            int iCompleted = 0;
            int iReturned = 0;
            int iTraced = 0;
            decimal dAmountInv = 0;
            decimal dInspPaid = 0;
            int iCompLT30 = 0;

            int iRet = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Day ++++");

                // set up SQL connection (SIBTBL)
                sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get new requests
                sqlCmd1.CommandText = "sp_Count_NewReq";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@curdate", dDate);
                iNewCases = (int)sqlCmd1.ExecuteScalar();

                // get completed
                sqlCmd1.CommandText = "sp_Count_Completed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@curdate", dDate);
                iCompleted = (int)sqlCmd1.ExecuteScalar();

                // get returned
                sqlCmd1.CommandText = "sp_Count_Returned";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                iReturned = (int)sqlCmd1.ExecuteScalar();

                // get traced
                sqlCmd1.CommandText = "sp_Count_Traced";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                iTraced = (int)sqlCmd1.ExecuteScalar();

                // get amount invoiced
                sqlCmd1.CommandText = "sp_Sum_InvAmt_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                // get amount paid to the inspectors
                sqlCmd1.CommandText = "sp_Sum_InspChgs_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                // get compLT30
                sqlCmd1.CommandText = "sp_Count_CompLT30";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                iCompLT30 = (int)sqlCmd1.ExecuteScalar();
                                
                sqlConn1.Close();

                // set up SQL connection 
                sqlConn2 = new SqlConnection(cfg_SIBUtilConnStr);
                sqlCmd2 = new SqlCommand();

                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "sp_Dashboard_Stats_Overwrite";
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();
                sqlCmd2.Parameters.Clear();

                sqlCmd2.Parameters.AddWithValue("@stattype", "D");
                sqlCmd2.Parameters.AddWithValue("@statusdate", dStatDate);
                sqlCmd2.Parameters.AddWithValue("@statusperiod", dDate);
                sqlCmd2.Parameters.AddWithValue("@statustime", sStatTime);
                sqlCmd2.Parameters.AddWithValue("@newcases", iNewCases);
                sqlCmd2.Parameters.AddWithValue("@completed", iCompleted);
                sqlCmd2.Parameters.AddWithValue("@returneditems", iReturned);
                sqlCmd2.Parameters.AddWithValue("@traceditems", iTraced);
                sqlCmd2.Parameters.AddWithValue("@amtinvoiced", dAmountInv);
                sqlCmd2.Parameters.AddWithValue("@insppaid", dInspPaid);
                sqlCmd2.Parameters.AddWithValue("@complt30", iCompLT30);

                iRet = sqlCmd2.ExecuteNonQuery();

                if (iRet == 0)
                    throw new SystemException("sp_Dashboard_Stats_Overwrite returned 0");


            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End Day ----");
                oLU.closeLog();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn2.Close();

            }
        }


        static void Week(DateTime dDate)
        {

            // Creates week row for week containing date passed
            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dBegOfWeek = FirstDayofWeek(dDate);
            DateTime dEndOfWeek = dBegOfWeek.AddDays(6);

            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iNewCases = 0;
            int iCompleted = 0;
            int iReturned = 0;
            int iTraced = 0;
            decimal dAmountInv = 0;
            decimal dInspPaid = 0;
            int iCompLT30 = 0;

            int iRet = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Week ++++");

                // set up SQL connection (SIBTBL)
                sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get new requests
                sqlCmd1.CommandText = "sp_Count_NewReq_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                iNewCases = (int)sqlCmd1.ExecuteScalar();

                // get completed
                sqlCmd1.CommandText = "sp_Count_Completed_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                iCompleted = (int)sqlCmd1.ExecuteScalar();

                // get returned
                sqlCmd1.CommandText = "sp_Count_Returned";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                iReturned = (int)sqlCmd1.ExecuteScalar();

                // get traced
                sqlCmd1.CommandText = "sp_Count_Traced";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                iTraced = (int)sqlCmd1.ExecuteScalar();

                // get amount invoiced
                sqlCmd1.CommandText = "sp_Sum_InvAmt_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                // get amount paid to the inspectors
                sqlCmd1.CommandText = "sp_Sum_InspChgs_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                // get compLT30
                sqlCmd1.CommandText = "sp_Count_CompLT30";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                iCompLT30 = (int)sqlCmd1.ExecuteScalar();


                sqlConn1.Close();

                // set up SQL connection 
                sqlConn2 = new SqlConnection(cfg_SIBUtilConnStr);
                sqlCmd2 = new SqlCommand();
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "sp_Dashboard_Stats_Overwrite";
                sqlCmd2.Connection = sqlConn2;
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.Parameters.Clear();

                sqlCmd2.Parameters.AddWithValue("@stattype", "W");
                sqlCmd2.Parameters.AddWithValue("@statusdate", dStatDate);
                sqlCmd2.Parameters.AddWithValue("@statusperiod", dBegOfWeek);
                sqlCmd2.Parameters.AddWithValue("@statustime", sStatTime);
                sqlCmd2.Parameters.AddWithValue("@newcases", iNewCases);
                sqlCmd2.Parameters.AddWithValue("@completed", iCompleted);
                sqlCmd2.Parameters.AddWithValue("@returneditems", iReturned);
                sqlCmd2.Parameters.AddWithValue("@traceditems", iTraced);
                sqlCmd2.Parameters.AddWithValue("@amtinvoiced", dAmountInv);
                sqlCmd2.Parameters.AddWithValue("@insppaid", dInspPaid);
                sqlCmd2.Parameters.AddWithValue("@complt30", iCompLT30);

                iRet = sqlCmd2.ExecuteNonQuery();

                if (iRet == 0)
                    throw new SystemException("sp_Dashboard_Stats_Overwrite returned 0");


            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End Week ----");
                oLU.closeLog();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn2.Close();

            }
                      

        }


        static void Month(DateTime dDate)
        {


            // Creates week row for week containing date passed
            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dBegOfMonth = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth = LastDayOfMonth(dDate);

            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iNewCases = 0;
            int iCompleted = 0;
            int iReturned = 0;
            int iTraced = 0;
            decimal dAmountInv = 0;
            decimal dInspPaid = 0;
            int iCompLT30 = 0;

            int iRet = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Month ++++");

                // set up SQL connection (SIBTBL)
                sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get new requests
                sqlCmd1.CommandText = "sp_Count_NewReq_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                iNewCases = (int)sqlCmd1.ExecuteScalar();

                // get completed
                sqlCmd1.CommandText = "sp_Count_Completed_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                iCompleted = (int)sqlCmd1.ExecuteScalar();

                // get returned
                sqlCmd1.CommandText = "sp_Count_Returned";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                iReturned = (int)sqlCmd1.ExecuteScalar();

                // get traced
                sqlCmd1.CommandText = "sp_Count_Traced";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                iTraced = (int)sqlCmd1.ExecuteScalar();

                // get amount invoiced
                sqlCmd1.CommandText = "sp_Sum_InvAmt_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                // get amount paid to the inspectors
                sqlCmd1.CommandText = "sp_Sum_InspChgs_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                // get compLT30
                sqlCmd1.CommandText = "sp_Count_CompLT30";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                iCompLT30 = (int)sqlCmd1.ExecuteScalar();


                sqlConn1.Close();

                // set up SQL connection 
                sqlConn2 = new SqlConnection(cfg_SIBUtilConnStr);
                sqlCmd2 = new SqlCommand();
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "sp_Dashboard_Stats_Overwrite";
                sqlCmd2.Connection = sqlConn2;
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.Parameters.Clear();

                sqlCmd2.Parameters.AddWithValue("@stattype", "M");
                sqlCmd2.Parameters.AddWithValue("@statusdate", dStatDate);
                sqlCmd2.Parameters.AddWithValue("@statusperiod", dBegOfMonth);
                sqlCmd2.Parameters.AddWithValue("@statustime", sStatTime);
                sqlCmd2.Parameters.AddWithValue("@newcases", iNewCases);
                sqlCmd2.Parameters.AddWithValue("@completed", iCompleted);
                sqlCmd2.Parameters.AddWithValue("@returneditems", iReturned);
                sqlCmd2.Parameters.AddWithValue("@traceditems", iTraced);
                sqlCmd2.Parameters.AddWithValue("@amtinvoiced", dAmountInv);
                sqlCmd2.Parameters.AddWithValue("@insppaid", dInspPaid);
                sqlCmd2.Parameters.AddWithValue("@complt30", iCompLT30);


                iRet = sqlCmd2.ExecuteNonQuery();

                if (iRet == 0)
                    throw new SystemException("sp_Dashboard_Stats_Overwrite returned 0");

                oLU.WritetoLog("Date range: " + dBegOfMonth.ToString() + " - " + dEndOfMonth.ToString());

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End Month ----");
                oLU.closeLog();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn2.Close();

            }

        }

        static void Period(DateTime dDate)
        {


            // Creates period row for period (1/15 - 16-EOM) containing date passed
            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dBegOfMonth = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth = LastDayOfMonth(dDate);
            DateTime dBegOfPeriod;
            DateTime dEndOfPeriod;

            // set period dates
            if (dDate.Day < 16)
                dBegOfPeriod = dBegOfMonth;
            else
                dBegOfPeriod = dBegOfMonth.AddDays(15);

            // End of period = actual end date plus 1 to account for all items through midnight on the last day
            if (dDate.Day < 16)
                dEndOfPeriod = dBegOfMonth.AddDays(15);
            else
                dEndOfPeriod = dEndOfMonth.AddDays(1);


            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iNewCases = 0;
            int iCompleted = 0;
            int iReturned = 0;
            int iTraced = 0;
            decimal dAmountInv = 0;
            decimal dInspPaid = 0;
            int iCompLT30 = 0;

            int iRet = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Period ++++");

                // set up SQL connection (SIBTBL)
                sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get amount invoiced
                sqlCmd1.CommandText = "sp_Total_Billed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfPeriod);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfPeriod);
                dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                // get amount paid to the inspectors
                sqlCmd1.CommandText = "sp_Total_Paid";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfPeriod);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfPeriod);
                dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                sqlConn1.Close();

                // set up SQL connection 
                sqlConn2 = new SqlConnection(cfg_SIBUtilConnStr);
                sqlCmd2 = new SqlCommand();
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "sp_Dashboard_Stats_Overwrite";
                sqlCmd2.Connection = sqlConn2;
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.Parameters.Clear();

                sqlCmd2.Parameters.AddWithValue("@stattype", "P");
                sqlCmd2.Parameters.AddWithValue("@statusdate", dStatDate);
                sqlCmd2.Parameters.AddWithValue("@statusperiod", dBegOfPeriod);
                sqlCmd2.Parameters.AddWithValue("@statustime", sStatTime);
                sqlCmd2.Parameters.AddWithValue("@newcases", iNewCases);
                sqlCmd2.Parameters.AddWithValue("@completed", iCompleted);
                sqlCmd2.Parameters.AddWithValue("@returneditems", iReturned);
                sqlCmd2.Parameters.AddWithValue("@traceditems", iTraced);
                sqlCmd2.Parameters.AddWithValue("@amtinvoiced", dAmountInv);
                sqlCmd2.Parameters.AddWithValue("@insppaid", dInspPaid);
                sqlCmd2.Parameters.AddWithValue("@complt30", iCompLT30);


                iRet = sqlCmd2.ExecuteNonQuery();

                if (iRet == 0)
                    throw new SystemException("sp_Dashboard_Stats_Overwrite returned 0");

                oLU.WritetoLog("Date range: " + dBegOfMonth.ToString() + " - " + dEndOfMonth.ToString());

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End Period ----");
                oLU.closeLog();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn2.Close();

            }

        }

         static void buildHistoricalData()
         {

            // NOT USED
             oDB360.buildHistoricalData();

             //try
             //{

             //    DateTime dCurDate = new DateTime(2009, 3, 2);
             //    DateTime dEndDate = new DateTime(2010, 3, 1);
             //    do
             //    {

             //        updateCompLT30(dCurDate);
             //        dCurDate = dCurDate.AddMonths(1);

             //    } while (dCurDate < dEndDate);
             //}
             //catch (Exception ex)
             //{

             //    //record exception  

             //}

          }

         static void updateCompLT30(DateTime dDate)
         {

             // update compLT30 in DBStat table

             CultureInfo info = Thread.CurrentThread.CurrentCulture;
             DateTime dBegOfMonth = FirstDayOfMonth(dDate);
             DateTime dEndOfMonth = LastDayOfMonth(dDate);

             int iCompLT30 = 0;
             int iComp = 0;
             int iNew = 0;
             decimal dAmountInv = 0;
             decimal dInspPaid = 0;

             int iRet = 0;

             SqlConnection sqlConn1 = null;
             SqlCommand sqlCmd1 = null;
             SqlConnection sqlConn2 = null;
             SqlCommand sqlCmd2 = null;

             try
             {

                 // set up SQL connection (SIBTBL)
                 sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
                 sqlCmd1 = new SqlCommand();
                 sqlCmd1.CommandType = CommandType.StoredProcedure;
                 sqlCmd1.Connection = sqlConn1;
                 sqlConn1.Open();

                 // get new
                 sqlCmd1.CommandText = "sp_Count_NewReq_Range";
                 sqlCmd1.Parameters.Clear();
                 sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                 sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                 iNew = (int)sqlCmd1.ExecuteScalar();

                 // get comp
                 sqlCmd1.CommandText = "sp_Count_Completed_Range";
                 sqlCmd1.Parameters.Clear();
                 sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                 sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                 iComp = (int)sqlCmd1.ExecuteScalar();


                 // get compLT30
                 sqlCmd1.CommandText = "sp_Count_CompLT30";
                 sqlCmd1.Parameters.Clear();
                 sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                 sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                 iCompLT30 = (int)sqlCmd1.ExecuteScalar();


                 // get amount invoiced
                 sqlCmd1.CommandText = "sp_Sum_InvAmt_Range";
                 sqlCmd1.Parameters.Clear();
                 sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                 sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                 dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                 // get amount paid to the inspectors
                 sqlCmd1.CommandText = "sp_Sum_InspChgs_Range";
                 sqlCmd1.Parameters.Clear();
                 sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                 sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                 dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                 sqlConn1.Close();

                 // set up SQL connection 
                 sqlConn2 = new SqlConnection(cfg_SIBUtilConnStr);
                 sqlCmd2 = new SqlCommand();

                 sqlCmd2.CommandType = CommandType.Text;
                 sqlCmd2.CommandText = "UPDATE Dashboard_Stats SET newcases = " + iNew + ", complt30 = " + iCompLT30 + ", completed = " + iComp + ", amtinvoiced = " + dAmountInv + ", insppaid = " + dInspPaid + " WHERE (stattype = 'M' AND statusperiod = '" + dBegOfMonth + "')";
                 sqlCmd2.Connection = sqlConn2;
                 sqlConn2.Open();
                 iRet = sqlCmd2.ExecuteNonQuery();

             }

             catch (Exception ex)
             {

                 //record exception  
                 oLU.WritetoLog(ex.Message);

             }

             finally
             {

                 if (sqlConn1 != null)
                     sqlConn1.Close();

                 if (sqlConn2 != null)
                     sqlConn2.Close();

             }


         }


         static void buildRevenueHist(DateTime dDate)
         {

             // update invamt and insppaid in DBStat table

             // Get first day (Sunday) of current week
             CultureInfo info = Thread.CurrentThread.CurrentCulture;
             DateTime dBegOfWeek = FirstDayofWeek(dDate);

             // First day of previous week
             dBegOfWeek = dBegOfWeek.AddDays(-7);

             DateTime dEndOfWeek = dBegOfWeek.AddDays(6);

             decimal dAmountInv = 0;
             decimal dInspPaid = 0;
             int iRet = 0;

             SqlConnection sqlConn1 = null;
             SqlCommand sqlCmd1 = null;
             SqlConnection sqlConn2 = null;
             SqlCommand sqlCmd2 = null;

             try
             {

                 // set up SQL connection (SIBTBL)
                 sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
                 sqlCmd1 = new SqlCommand();
                 sqlCmd1.CommandType = CommandType.StoredProcedure;
                 sqlCmd1.Connection = sqlConn1;
                 sqlConn1.Open();

                 // get amount invoiced
                 sqlCmd1.CommandText = "sp_Sum_InvAmt_Range";
                 sqlCmd1.Parameters.Clear();
                 sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                 sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                 dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                 // get amount paid to the inspectors
                 sqlCmd1.CommandText = "sp_Sum_InspChgs_Range";
                 sqlCmd1.Parameters.Clear();
                 sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                 sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                 dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                 sqlConn1.Close();

                 // set up SQL connection 
                 sqlConn2 = new SqlConnection(cfg_SIBUtilConnStr);
                 sqlCmd2 = new SqlCommand();

                 sqlCmd2.CommandType = CommandType.Text;
                 sqlCmd2.CommandText = "UPDATE Dashboard_Stats SET amtinvoiced = " + dAmountInv + ", insppaid = " + dInspPaid + " WHERE (stattype = 'M' AND statusperiod = '" + dBegOfWeek + "')";
                 sqlCmd2.Connection = sqlConn2;
                 sqlConn2.Open();
                 iRet = sqlCmd2.ExecuteNonQuery();

             }

             catch (Exception ex)
             {

                 //record exception  
                 Console.WriteLine(ex.Message);

             }

             finally
             {

                 if (sqlConn1 != null)
                     sqlConn1.Close();

                 if (sqlConn2 != null)
                     sqlConn2.Close();

             }         
         
         
         }


         static void SalesHist(DateTime dDate)
         {


             // Cauculate sales history and insert into Dashboard_Sales_Hist
             CultureInfo info = Thread.CurrentThread.CurrentCulture;
             DateTime dBegDate = FirstDayOfMonth(dDate);
             DateTime dEndDate = dDate;

             int iRet = 0;

             SqlConnection sqlConn2 = null;
             SqlCommand sqlCmd2 = null;

             try
             {

                 // initialize log file class
                 oLU = new LogUtils.LogUtils();

                 // set log file name
                 oLU.logFileName = cfg_logfilename;

                 // open log file
                 oLU.OpenLog();
                 oLU.WritetoLog("++++ Begin SalesHist ++++");

                 // set up SQL connection 
                 sqlConn2 = new SqlConnection(cfg_SIBUtilConnStr);
                 sqlCmd2 = new SqlCommand();
                 sqlCmd2.Connection = sqlConn2;
                 sqlConn2.Open();

                 sqlCmd2.CommandType = CommandType.StoredProcedure;
                 sqlCmd2.CommandText = "sp_Dashboard_SalesHist_Update";
                 sqlCmd2.Connection = sqlConn2;
                 sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandTimeout = 180;
                 sqlCmd2.Parameters.Clear();

                 sqlCmd2.Parameters.AddWithValue("@begdate", dBegDate);
                 sqlCmd2.Parameters.AddWithValue("@enddate", dEndDate);

                 iRet = sqlCmd2.ExecuteNonQuery();

                 if (iRet == 0)
                     throw new SystemException("sp_Dashboard_SalesHist_Update");

                 oLU.WritetoLog("Date range: " + dBegDate.ToString() + " - " + dEndDate.ToString());

             }

             catch (Exception ex)
             {

                 //record exception  
                 oLU.WritetoLog(ex.Message);

             }

             finally
             {

                 // close objects
                 oLU.WritetoLog("---- End SalesHist ----");
                 oLU.closeLog();

                 if (sqlConn2 != null)
                     sqlConn2.Close();

             }


         }

        static void MonthlySalesHist(DateTime dDate)
        {

            // Cauculate sales history and insert into Dashboard_Sales_Hist
            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            int iRet = 0;

            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;


            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Monthly SalesHist ++++");

                // set up SQL connection 
                sqlConn2 = new SqlConnection(cfg_SIBUtilConnStr);
                sqlCmd2 = new SqlCommand();
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                // Clear table
                sqlCmd2.CommandType = CommandType.Text;
                sqlCmd2.CommandText = "DELETE FROM dbo.Dashboard_MonthlySalesHist";
                sqlCmd2.Connection = sqlConn2;
                iRet = sqlCmd2.ExecuteNonQuery();

                // set up for sp
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "sp_Dashboard_MonthlySalesHist_Update";
                sqlCmd2.Connection = sqlConn2;

                // set session end date 12 months prior
                DateTime dEndOfRun = dDate.AddMonths(-13);

                // set initial dates
                DateTime dBegDate = FirstDayOfMonth(dDate);
                DateTime dEndDate = dDate;

                do
                {
                    sqlCmd2.Parameters.Clear();
                    sqlCmd2.Parameters.AddWithValue("@begdate", dBegDate);
                    sqlCmd2.Parameters.AddWithValue("@enddate", dEndDate);

                    iRet = sqlCmd2.ExecuteNonQuery();

                    if (iRet == 0)
                        throw new SystemException("sp_Dashboard_MonthlySalesHist_Update");

                    oLU.WritetoLog("Date range: " + dBegDate.ToString() + " - " + dEndDate.ToString());

                    dBegDate = dBegDate.AddMonths(-1);
                    dEndDate = LastDayOfMonth(dBegDate);

                } while (dBegDate > dEndOfRun);

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End SalesHist ----");
                oLU.closeLog();

                if (sqlConn2 != null)
                    sqlConn2.Close();

            }


        }
        static void buildClientHist(DateTime dDate)
        {
            // Calculate weekly data and insert into ClientHistory table
            // Creates data for week prior to date passed
            // Should be called on Sunday to calc data for previous week

            // Runs query returning all accounts with completed reports during the period
            // Calls processClient to build row in clienthstory table

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            int iAcnt = 0;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin ClientHist ++++");

                CultureInfo info = Thread.CurrentThread.CurrentCulture;

                // Get first day (Sunday) of current week
                DateTime dBegOfWeek = FirstDayofWeek(dDate);

                // First day of previous week
                dBegOfWeek = dBegOfWeek.AddDays(-7);

                DateTime dEndOfWeek = dBegOfWeek.AddDays(6);

                oLU.WritetoLog("Period: " + dBegOfWeek.ToShortDateString() + " - " + dEndOfWeek.ToShortDateString());

                // set up SQL connection (SIBTBL)
                sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // set up SQL connection (SIBTBL)
                sqlConn2 = new SqlConnection(cfg_SIBIConnStr);
                sqlCmd2 = new SqlCommand();
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.Connection = sqlConn2;
                sqlCmd2.CommandText = "sp_ClientHistBuild_Get";
                sqlCmd2.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd2.Parameters.AddWithValue("@enddate", dEndOfWeek);
                sqlConn2.Open();

                SqlDataReader sqlReader = null;
                // Get all open keys from SibTBL
                sqlReader = sqlCmd2.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through open keys
                    do
                    {

                        //acnt
                        if (!sqlReader.IsDBNull(0))
                        {
                            iAcnt = (int)sqlReader.GetSqlInt32(0);
                            processClient(dBegOfWeek, dEndOfWeek, iAcnt, sqlConn1, sqlCmd1);
                        }

                    } while (sqlReader.Read());      

                    sqlReader.Close();
                }
            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End Client Hist ----");
                oLU.closeLog();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn2.Close();

            }
        }

        static void processClient(DateTime dBegOfWeek, DateTime dEndOfWeek, int iAcnt, SqlConnection sqlConn1, SqlCommand sqlCmd1)
        {

            int iNewCases = 0;
            int iCompleted = 0;
            decimal dAmountInv = 0;
            decimal dInspPaid = 0;
            int iRet = 0;

            try
            {

                // get new requests
                sqlCmd1.CommandText = "sp_Count_NewReq_Acnt_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                sqlCmd1.Parameters.AddWithValue("@acnt", iAcnt);
                iNewCases = (int)sqlCmd1.ExecuteScalar();

                // get completed
                sqlCmd1.CommandText = "sp_Count_Completed_Acnt_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                sqlCmd1.Parameters.AddWithValue("@acnt", iAcnt);
                iCompleted = (int)sqlCmd1.ExecuteScalar();

                // get amount invoiced
                sqlCmd1.CommandText = "sp_Sum_InvAmt_Acnt_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                sqlCmd1.Parameters.AddWithValue("@acnt", iAcnt);
                dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                // get amount paid to the inspectors
                sqlCmd1.CommandText = "sp_Sum_InspChgs_Acnt_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                sqlCmd1.Parameters.AddWithValue("@acnt", iAcnt);
                dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                // Insert record into ClientHistory
                sqlCmd1.CommandText = "sp_ClientHistory_Insert";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@acntnum", iAcnt);
                sqlCmd1.Parameters.AddWithValue("@weekbeginning", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@requested", iNewCases);
                sqlCmd1.Parameters.AddWithValue("@completed", iCompleted);
                sqlCmd1.Parameters.AddWithValue("@invoicedamount", dAmountInv);
                sqlCmd1.Parameters.AddWithValue("@inspcharges", dInspPaid);

                iRet = sqlCmd1.ExecuteNonQuery();

                if (iRet == 0)
                    throw new SystemException("sp_ClientHistory_Insert returned 0 - acnt#: " + iAcnt.ToString());


            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }
        
        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
            {    
                return new DateTime(
                    dateTime.Year,
                    dateTime.Month,
                    dateTime.Day,
                    hours,
                    minutes,
                    seconds,
                    milliseconds,
                    dateTime.Kind);
        }

        static void AccountingSummaryOLD(DateTime dDate)
        {
            // Calc amount invoiced for periods (1/15 - 16-EOM) in month from date passed
            
            // Save date for display
            DateTime dReportDate = dDate;

            // set time to 12:00am
            dDate = ChangeTime(dDate, 0, 0, 0, 0);
            
            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dBegOfMonth = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth = LastDayOfMonth(dDate);
            DateTime dBegOfMonth360 = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth360 = LastDayOfMonth(dDate);
            DateTime dFirstPdBeg;
            DateTime dFirstPdEnd;
            DateTime dFirstPdBeg360;
            DateTime dFirstPdEnd360;
            DateTime dSecPdBeg;
            DateTime dSecPdEnd;
            DateTime dSecPdBeg360;
            DateTime dSecPdEnd360;
            
            string sMsgBody = ""; 
            string sRet = "xxxxx";

            // fix beg / end of month for 360 GMT
            dBegOfMonth360 = dBegOfMonth360.AddHours(cfg_TimeZoneOffset);
            dEndOfMonth360 = dEndOfMonth360.AddHours(cfg_TimeZoneOffset);

            // Current day
            int iNumInvoicedSIB = 0;
            decimal dAmtInvoicedSIB = 0;
            int iNumInvoiced360 = 0;
            decimal dAmtInvoiced360 = 0;            

            // first period
            decimal dFirstPdAmtInvSIB = 0;
            decimal dFirstPdAmtInv360 = 0;
            int iFirstPdNumInvSIB = 0;
            int iFirstPdNumInv360 = 0;

            // second period
            decimal dSecPdAmtInvSIB = 0;
            decimal dSecPdAmtInv360 = 0;
            int iSecPdNumInvSIB = 0;
            int iSecPdNumInv360 = 0;
                        
            // Payroll
            decimal dPendingPRSIB = 0;
            decimal dPendingPR360 = 0;
            decimal dPendingTotal = 0;

            // Open cases
            int iOpenCasesSIB = 0;
            int iOpenCases360 = 0;
            int iTotalOpen = 0;

            // new requests
            int iNewReqTotal = 0;
            int iNewReqSIB = 0;
            int iNewReq360 = 0;

            // set period dates (current month)
            dFirstPdBeg = dBegOfMonth;
            dFirstPdEnd = dBegOfMonth.AddDays(14);

            dFirstPdBeg360 = dBegOfMonth360;
            dFirstPdEnd360 = dBegOfMonth360.AddDays(15);

            // End of period = actual end date plus 1 to account for all items through midnight on the last day
            dSecPdBeg = dBegOfMonth.AddDays(15);
            dSecPdEnd = dEndOfMonth;

            dSecPdBeg360 = dBegOfMonth360.AddDays(15);
            dSecPdEnd360 = dEndOfMonth360.AddDays(1);


            // status date
            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            // SQL
            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin AccountingSummary ++++");

                // set up SQL connection (SIBTBL)
                sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get number of items invoiced today in SIB  
                sqlCmd1.CommandText = "sp_Count_Invoiced";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                iNumInvoicedSIB = (int)sqlCmd1.ExecuteScalar();
                
                // get dollar amount of items invoiced today in SIB  
                sqlCmd1.CommandText = "sp_Sum_Invoiced";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                dAmtInvoicedSIB = (decimal)sqlCmd1.ExecuteScalar();

                // get total open cases - SIB
                sqlCmd1.CommandText = "sp_DBStats_CountOpen";
                sqlCmd1.Parameters.Clear();
                iOpenCasesSIB = (int)sqlCmd1.ExecuteScalar();
                 
                // get pending payroll SIB  
                sqlCmd1.CommandText = "sp_InspPayroll_TotalPending";
                sqlCmd1.Parameters.Clear();
                dPendingPRSIB = (decimal)sqlCmd1.ExecuteScalar();

                // get pending payroll 360
                dPendingPR360 = oDB360.OutstandingPR();

                // Total PR
                dPendingTotal = dPendingPRSIB + dPendingPR360;
                
                // get total open items - 360
                iOpenCases360 = oDB360.OpenItems();
                
                // get number of items first completed today in 360 
                // set start date to 7:06PM previous day
                DateTime dWorkDate = dDate.AddDays(-1);
                dWorkDate = ChangeTime(dWorkDate, 19, 6, 0, 0);

                // Account for 360 storing time in GMT
                dWorkDate = dWorkDate.AddHours(cfg_TimeZoneOffset);
                
                iNumInvoiced360 = oDB360.CountCompleted(dWorkDate);

                // get total billing for items first completed today in 360 
                // set start date to 7:06PM previous day
                dAmtInvoiced360 = oDB360.TotalBilled(dWorkDate);
                                
                // get amount invoiced SIB - first period
                sqlCmd1.CommandText = "sp_Total_Billed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dFirstPdBeg);
                sqlCmd1.Parameters.AddWithValue("@enddate", dFirstPdEnd);
                dFirstPdAmtInvSIB = (decimal)sqlCmd1.ExecuteScalar();

                // get number of items invoiced SIB - first period
                sqlCmd1.CommandText = "sp_Count_Invoiced_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dFirstPdBeg);
                sqlCmd1.Parameters.AddWithValue("@enddate", dFirstPdEnd);
                iFirstPdNumInvSIB = (int)sqlCmd1.ExecuteScalar();

                // if run date is in second period 
                if (dDate.Day > 15)
                {
                    // get amount invoiced SIB - second period
                    sqlCmd1.CommandText = "sp_Total_Billed";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@begdate", dSecPdBeg);
                    sqlCmd1.Parameters.AddWithValue("@enddate", dSecPdEnd);
                    dSecPdAmtInvSIB = (decimal)sqlCmd1.ExecuteScalar();

                    // get number of items invoiced SIB - second period
                    sqlCmd1.CommandText = "sp_Count_Invoiced_Range";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@begdate", dSecPdBeg);
                    sqlCmd1.Parameters.AddWithValue("@enddate", dSecPdEnd);
                    iSecPdNumInvSIB = (int)sqlCmd1.ExecuteScalar();

                }

                // get amount invoiced 360 - first period
                dFirstPdAmtInv360 = oDB360.TotalBilledFirstPd(dFirstPdBeg360, dFirstPdEnd360, true);

                // get number of items billed today in 360 
                // set start date to 7:06PM previous day
                dWorkDate = dDate.AddDays(-1);
                dWorkDate = ChangeTime(dWorkDate, 19, 6, 0, 0);

                // Account for 360 storing time in GMT
                dWorkDate = dWorkDate.AddHours(cfg_TimeZoneOffset);

                iNumInvoiced360 = oDB360.CountCompleted(dWorkDate);

                // if run date is in second period -  get amount invoiced 360 - second period
                if (dDate.Day > 15)
                {
                    dSecPdAmtInv360 = oDB360.TotalBilledSecondPd(dSecPdBeg360, dSecPdEnd360, true);
                    //iSecPdNumInv360 = oDB360
                }

                decimal dFirstPdTotal = dFirstPdAmtInvSIB + dFirstPdAmtInv360;
                decimal dSecondPdTotal = dSecPdAmtInvSIB + dSecPdAmtInv360;
                decimal dMonthTotal = dFirstPdTotal + dSecondPdTotal;

                iTotalOpen = iOpenCases360 + iOpenCasesSIB;

                sMsgBody = "Accounting Summary" + " as of " + dStatDate.ToShortDateString() + "  " + sStatTime + System.Environment.NewLine + System.Environment.NewLine;
                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "Today" + System.Environment.NewLine;
                sMsgBody += "Total billed - SIB:  " + String.Format("{0,4}", iNumInvoicedSIB) + "     " + dAmtInvoicedSIB.ToString("C") +  System.Environment.NewLine;
                sMsgBody += "Total billed - 360:  " + String.Format("{0,4}", iNumInvoiced360) + "     " + dAmtInvoiced360.ToString("C") + System.Environment.NewLine;
                int iTotBilled = iNumInvoiced360 + iNumInvoicedSIB;
                decimal dTotBilled = dAmtInvoicedSIB + dAmtInvoiced360;
                sMsgBody += "Total billed today:  " + String.Format("{0,4}", iTotBilled) + "     " + dTotBilled.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;
                
                sMsgBody += "First Billing Period" + System.Environment.NewLine;
                sMsgBody += "Total billed - SIB:  " + dFirstPdAmtInvSIB.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Total billed - 360:  " + dFirstPdAmtInv360.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Total first period:  " + dFirstPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "Second Billing Period" + System.Environment.NewLine;
                sMsgBody += "Total billed - SIB:  " + dSecPdAmtInvSIB.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Total billed - 360:  " + dSecPdAmtInv360.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Total second period: " + dSecondPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + " total:         " + dMonthTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "Pending Inspector Payroll" + System.Environment.NewLine;
                sMsgBody += "Total outstanding - SIB:  " + dPendingPRSIB.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Total outstanding - 360:  " + dPendingPR360.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Total pending payroll:  " + dPendingTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "Open cases: " + iTotalOpen.ToString("#,0") + System.Environment.NewLine;
                sMsgBody += "       SIB: " + iOpenCasesSIB.ToString("#,0") + System.Environment.NewLine;
                sMsgBody += "       360: " + iOpenCases360.ToString("#,0") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "New Inspection Requests" + System.Environment.NewLine;
                iNewReqSIB = NewRequests(dDate,"D","SIB");
                iNewReq360 = NewRequests(dDate, "D", "360");
                iNewReqTotal = iNewReqSIB + iNewReq360;
                sMsgBody += "Today: " + iNewReqTotal.ToString("#,0") + System.Environment.NewLine;
                sMsgBody += "  SIB: " + iNewReqSIB.ToString("#,0") + System.Environment.NewLine;
                sMsgBody += "  360: " + iNewReq360.ToString("#,0") + System.Environment.NewLine + System.Environment.NewLine;

                iNewReqSIB = NewRequests(dDate, "W", "SIB");
                iNewReq360 = NewRequests(dDate, "W", "360");
                iNewReqTotal = iNewReqSIB + iNewReq360;
                sMsgBody += "This week: " + iNewReqTotal.ToString("#,0") + System.Environment.NewLine;
                sMsgBody += "      SIB: " + iNewReqSIB.ToString("#,0") + System.Environment.NewLine;
                sMsgBody += "      360: " + iNewReq360.ToString("#,0") + System.Environment.NewLine + System.Environment.NewLine;

                iNewReqSIB = NewRequests(dDate, "M", "SIB");
                iNewReq360 = NewRequests(dDate, "M", "360");
                iNewReqTotal = iNewReqSIB + iNewReq360;
                sMsgBody += "This month: " + iNewReqTotal.ToString("#,0") + System.Environment.NewLine;
                sMsgBody += "       SIB: " + iNewReqSIB.ToString("#,0") + System.Environment.NewLine;
                sMsgBody += "       360: " + iNewReq360.ToString("#,0") + System.Environment.NewLine;

                sRet = "x";

                //------------------------------//
                // set period dates (prev month)//
                //------------------------------//
                dDate = dDate.AddMonths(-1);
                dDate = LastDayOfMonth(dDate);
                dBegOfMonth = FirstDayOfMonth(dDate);
                dEndOfMonth = LastDayOfMonth(dDate);
                dFirstPdBeg = dBegOfMonth;
                dFirstPdEnd = dBegOfMonth.AddDays(14);
                dFirstPdBeg360 = dBegOfMonth;
                dFirstPdEnd360 = dBegOfMonth.AddDays(15);
                
                dSecPdBeg = dBegOfMonth.AddDays(15);
                dSecPdEnd = dEndOfMonth;
                dSecPdBeg360 = dBegOfMonth.AddDays(15);
                dSecPdEnd360 = dEndOfMonth.AddDays(1); 


                // Account for 360 storing dates in GMT
                dBegOfMonth360 = dBegOfMonth.AddHours(cfg_TimeZoneOffset);
                dEndOfMonth360 = dEndOfMonth.AddHours(cfg_TimeZoneOffset);
                dFirstPdBeg360 = dFirstPdBeg360.AddHours(cfg_TimeZoneOffset);
                dFirstPdEnd360 = dFirstPdEnd360.AddHours(cfg_TimeZoneOffset);
                dSecPdBeg360 = dSecPdBeg360.AddHours(cfg_TimeZoneOffset);
                dSecPdEnd360 = dSecPdEnd360.AddHours(cfg_TimeZoneOffset);
                
                dFirstPdAmtInvSIB = 0;
                dSecPdAmtInvSIB = 0;
                dFirstPdAmtInv360 = 0;
                dSecPdAmtInv360 = 0;

                // get amount invoiced SIB - first period
                sqlCmd1.CommandText = "sp_Total_Billed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dFirstPdBeg);
                sqlCmd1.Parameters.AddWithValue("@enddate", dFirstPdEnd);
                dFirstPdAmtInvSIB = (decimal)sqlCmd1.ExecuteScalar();

                // get amount invoiced SIB - second period
                sqlCmd1.CommandText = "sp_Total_Billed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dSecPdBeg);
                sqlCmd1.Parameters.AddWithValue("@enddate", dSecPdEnd);
                dSecPdAmtInvSIB = (decimal)sqlCmd1.ExecuteScalar();

                // get amount invoiced 360 - first period
                dFirstPdAmtInv360 = oDB360.TotalBilledFirstPd(dFirstPdBeg360, dFirstPdEnd360, false);

                // get amount invoiced 360 - second period
                dSecPdAmtInv360 = oDB360.TotalBilledSecondPd(dSecPdBeg360, dSecPdEnd360, false);

                dFirstPdTotal = dFirstPdAmtInvSIB + dFirstPdAmtInv360;
                dSecondPdTotal = dSecPdAmtInvSIB + dSecPdAmtInv360;
                dMonthTotal = dFirstPdTotal + dSecondPdTotal;

                sMsgBody += System.Environment.NewLine + System.Environment.NewLine;
                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "First Billing Period" + System.Environment.NewLine;
                sMsgBody += "Total billed - SIB:  " + dFirstPdAmtInvSIB.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Total billed - 360:  " + dFirstPdAmtInv360.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Total first period:  " + dFirstPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "Second Billing Period" + System.Environment.NewLine;
                sMsgBody += "Total billed - SIB:  " + dSecPdAmtInvSIB.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Total billed - 360:  " + dSecPdAmtInv360.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Total second period: " + dSecondPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + " total:         " + dMonthTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "NOTE:" + System.Environment.NewLine;
                sMsgBody += "      360 dollar amounts are estimates until invoiced." + System.Environment.NewLine;
                sMsgBody += "      360 values are current as of the time this report was run." + System.Environment.NewLine;
                sMsgBody += "      SIB values are as of the most recent invoice run." + System.Environment.NewLine;

                
                //// get amount paid to the inspectors
                //sqlCmd1.CommandText = "sp_Total_Paid";
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfPeriod);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfPeriod);
                //dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                sqlConn1.Close();

                SendMail.SendMail oMail = new SendMail.SendMail();
                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = "axel@sibfla.com; randy@sibfla.com; andrea@sibfla.com; jeff@sibfla.com; tiana@sibfla.com; thomas@sibfla.com; Michael@sibfla.com; Jordan@sibfla.com;";
                //oMail.MailTo = "jeff@sibfla.com";
                oMail.MsgSubject = "Accounting Summary - " + dReportDate.ToLongDateString();
                oMail.MsgBody = sMsgBody;
                oMail.SMTPServer = cfg_smtpserver;

                oMail.SendHTML = false;
                                
                sRet = oMail.Send();
                oMail = null;

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End AccountingSummary ++++");
                oLU.closeLog();

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

        
        }

        static void AccountingSummaryPD(DateTime dDate)
        {
            
            ///////////////////////////////////////
            // Run after midnight for previous day
            ///////////////////////////////////////

            // Calc amount invoiced for periods (1/15 - 16-EOM) in month from date passed

            // Save date for display
            DateTime dReportDate = dDate;

            // set time to 12:00am
            dDate = ChangeTime(dDate, 0, 0, 0, 0);

            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dBegOfMonth = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth = LastDayOfMonth(dDate);
            DateTime dBegOfMonth360 = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth360 = LastDayOfMonth(dDate);
            DateTime dFirstPdBeg;
            DateTime dFirstPdEnd;
            DateTime dFirstPdBeg360;
            DateTime dFirstPdEnd360;
            DateTime dSecPdBeg;
            DateTime dSecPdEnd;
            DateTime dSecPdBeg360;
            DateTime dSecPdEnd360;

            string sMsgBody = "";
            string sRet = "xxxxx";

            // fix beg / end of month for 360 GMT
            dBegOfMonth360 = dBegOfMonth360.AddHours(cfg_TimeZoneOffset);
            dEndOfMonth360 = dEndOfMonth360.AddHours(cfg_TimeZoneOffset);

            // Current day
            int iNumInvoicedSIB = 0;
            decimal dAmtInvoicedSIB = 0;
            int iNumInvoiced360 = 0;
            decimal dAmtInvoiced360 = 0;

            // first period
            decimal dFirstPdAmtInvSIB = 0;
            decimal dFirstPdAmtInv360 = 0;
            int iFirstPdNumInvSIB = 0;
            int iFirstPdNumInv360 = 0;

            // second period
            decimal dSecPdAmtInvSIB = 0;
            decimal dSecPdAmtInv360 = 0;
            int iSecPdNumInvSIB = 0;
            int iSecPdNumInv360 = 0;

            // Payroll
            decimal dPendingPRSIB = 0;
            decimal dPendingPR360 = 0;
            decimal dPendingTotal = 0;

            // Open cases
            int iOpenCasesSIB = 0;
            int iOpenCases360 = 0;
            int iTotalOpen = 0;

            // new requests
            int iNewReqTotal = 0;
            int iNewReqSIB = 0;
            int iNewReq360 = 0;

            // set period dates (current month)
            dFirstPdBeg = dBegOfMonth;
            dFirstPdEnd = dBegOfMonth.AddDays(14);

            dFirstPdBeg360 = dBegOfMonth360;
            dFirstPdEnd360 = dBegOfMonth360.AddDays(15);

            // End of period = actual end date plus 1 to account for all items through midnight on the last day
            dSecPdBeg = dBegOfMonth.AddDays(15);
            dSecPdEnd = dEndOfMonth;

            dSecPdBeg360 = dBegOfMonth360.AddDays(15);
            dSecPdEnd360 = dEndOfMonth360.AddDays(1);


            // status date
            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            // SQL
            //SqlConnection sqlConn1 = null;
            //SqlCommand sqlCmd1 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin AccountingSummary ++++");

                // set up SQL connection (SIBTBL)
                //sqlConn1 = new SqlConnection(cfg_SIBIConnStr);
                //sqlCmd1 = new SqlCommand();
                //sqlCmd1.CommandType = CommandType.StoredProcedure;
                //sqlCmd1.Connection = sqlConn1;
                //sqlConn1.Open();

                // get number of items invoiced today in SIB  
                //sqlCmd1.CommandText = "sp_Count_Invoiced";
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                //iNumInvoicedSIB = (int)sqlCmd1.ExecuteScalar();
                iNumInvoicedSIB = 0;

                // get dollar amount of items invoiced today in SIB  
                //sqlCmd1.CommandText = "sp_Sum_Invoiced";
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                //dAmtInvoicedSIB = (decimal)sqlCmd1.ExecuteScalar();
                dAmtInvoicedSIB = 0;

                // get total open cases - SIB
                //sqlCmd1.CommandText = "sp_DBStats_CountOpen";
                //sqlCmd1.Parameters.Clear();
                //iOpenCasesSIB = (int)sqlCmd1.ExecuteScalar();
                iOpenCasesSIB = 0;

                // get pending payroll SIB  
                //sqlCmd1.CommandText = "sp_InspPayroll_TotalPending";
                //sqlCmd1.Parameters.Clear();
                //dPendingPRSIB = (decimal)sqlCmd1.ExecuteScalar();
                dPendingPRSIB = 0;

                // get pending payroll 360
                dPendingPR360 = oDB360.OutstandingPR();

                // Total PR
                dPendingTotal = dPendingPRSIB + dPendingPR360;

                // get total open items - 360
                iOpenCases360 = oDB360.OpenItems();

                // get number of items first completed today in 360 
                // set start date to 1 second after midnight previous day
                DateTime dWorkDate = dDate;
                dWorkDate = ChangeTime(dWorkDate, 0, 0, 1, 0);

                // Account for 360 storing time in GMT
                dWorkDate = dWorkDate.AddHours(cfg_TimeZoneOffset);

                iNumInvoiced360 = oDB360.CountCompleted(dWorkDate);

                // get total billing for items first completed today in 360 
                // set start date to 7:06PM previous day
                dAmtInvoiced360 = oDB360.TotalBilled(dWorkDate);

                // get amount invoiced SIB - first period
                //sqlCmd1.CommandText = "sp_Total_Billed";
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dFirstPdBeg);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dFirstPdEnd);
                //dFirstPdAmtInvSIB = (decimal)sqlCmd1.ExecuteScalar();
                dFirstPdAmtInvSIB = 0;

                // get number of items invoiced SIB - first period
                //sqlCmd1.CommandText = "sp_Count_Invoiced_Range";
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dFirstPdBeg);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dFirstPdEnd);
                //iFirstPdNumInvSIB = (int)sqlCmd1.ExecuteScalar();
                iFirstPdNumInvSIB = 0;

                // if run date is in second period 
                if (dDate.Day > 15)
                {
                    // get amount invoiced SIB - second period
                    //sqlCmd1.CommandText = "sp_Total_Billed";
                    //sqlCmd1.Parameters.Clear();
                    //sqlCmd1.Parameters.AddWithValue("@begdate", dSecPdBeg);
                    //sqlCmd1.Parameters.AddWithValue("@enddate", dSecPdEnd);
                    //dSecPdAmtInvSIB = (decimal)sqlCmd1.ExecuteScalar();
                    dSecPdAmtInvSIB = 0;

                    // get number of items invoiced SIB - second period
                    //sqlCmd1.CommandText = "sp_Count_Invoiced_Range";
                    //sqlCmd1.Parameters.Clear();
                    //sqlCmd1.Parameters.AddWithValue("@begdate", dSecPdBeg);
                    //sqlCmd1.Parameters.AddWithValue("@enddate", dSecPdEnd);
                    //iSecPdNumInvSIB = (int)sqlCmd1.ExecuteScalar();
                    iSecPdNumInvSIB = 0;

                }

                // get amount invoiced 360 - first period
                dFirstPdAmtInv360 = oDB360.TotalBilledFirstPd(dFirstPdBeg360, dFirstPdEnd360, true);

                // get number of items billed today in 360 
                // set start date to midnight previous day
                dWorkDate = dDate;
                dWorkDate = ChangeTime(dWorkDate, 0, 0, 0, 0);

                // Account for 360 storing time in GMT
                dWorkDate = dWorkDate.AddHours(cfg_TimeZoneOffset);

                iNumInvoiced360 = oDB360.CountCompleted(dWorkDate);

                // if run date is in second period -  get amount invoiced 360 - second period
                if (dDate.Day > 15)
                {
                    dSecPdAmtInv360 = oDB360.TotalBilledSecondPd(dSecPdBeg360, dSecPdEnd360, true);
                }

                decimal dFirstPdTotal = dFirstPdAmtInvSIB + dFirstPdAmtInv360;
                decimal dSecondPdTotal = dSecPdAmtInvSIB + dSecPdAmtInv360;
                decimal dMonthTotal = dFirstPdTotal + dSecondPdTotal;

                iTotalOpen = iOpenCases360 + iOpenCasesSIB;

                sMsgBody = "Accounting Summary for " + dReportDate.ToLongDateString() +  "   Run Date " + dStatDate.ToShortDateString() + "  " + sStatTime + System.Environment.NewLine + System.Environment.NewLine;
                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + System.Environment.NewLine + System.Environment.NewLine;

                //sMsgBody += "Today" + System.Environment.NewLine;
                //sMsgBody += "Total billed - SIB:  " + String.Format("{0,4}", iNumInvoicedSIB) + "     " + dAmtInvoicedSIB.ToString("C") + System.Environment.NewLine;
                //sMsgBody += "Total billed - 360:  " + String.Format("{0,4}", iNumInvoiced360) + "     " + dAmtInvoiced360.ToString("C") + System.Environment.NewLine;
                int iTotBilled = iNumInvoiced360 + iNumInvoicedSIB;
                decimal dTotBilled = dAmtInvoicedSIB + dAmtInvoiced360;
                sMsgBody += "Total billed today:  " + String.Format("{0,4}", iTotBilled) + "     " + dTotBilled.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                //sMsgBody += "" + System.Environment.NewLine;
                //sMsgBody += "Total billed - SIB:  " + dFirstPdAmtInvSIB.ToString("C") + System.Environment.NewLine;
                //sMsgBody += "Total billed - 360:  " + dFirstPdAmtInv360.ToString("C") + System.Environment.NewLine;
                sMsgBody += "First Billing Period:  " + dFirstPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                //sMsgBody += "Second Billing Period" + System.Environment.NewLine;
                //sMsgBody += "Total billed - SIB:  " + dSecPdAmtInvSIB.ToString("C") + System.Environment.NewLine;
                //sMsgBody += "Total billed - 360:  " + dSecPdAmtInv360.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Second Billing Period: " + dSecondPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + " total:         " + dMonthTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                //sMsgBody += "Pending Inspector Payroll" + System.Environment.NewLine;
                //sMsgBody += "Total outstanding - SIB:  " + dPendingPRSIB.ToString("C") + System.Environment.NewLine;
                //sMsgBody += "Total outstanding - 360:  " + dPendingPR360.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Pending Inspector Payroll:  " + dPendingTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "Open cases: " + iTotalOpen.ToString("#,0") + System.Environment.NewLine + System.Environment.NewLine;
                //sMsgBody += "       SIB: " + iOpenCasesSIB.ToString("#,0") + System.Environment.NewLine;
                //sMsgBody += "       360: " + iOpenCases360.ToString("#,0") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "New Inspection Requests" + System.Environment.NewLine;
                //iNewReqSIB = NewRequests(dDate, "D", "SIB");
                iNewReq360 = NewRequests(dDate, "D", "360");
                iNewReqTotal = iNewReq360;
                sMsgBody += "Today: " + iNewReqTotal.ToString("#,0") + System.Environment.NewLine;
                //sMsgBody += "  SIB: " + iNewReqSIB.ToString("#,0") + System.Environment.NewLine;
                //sMsgBody += "  360: " + iNewReq360.ToString("#,0") + System.Environment.NewLine + System.Environment.NewLine;

                //iNewReqSIB = NewRequests(dDate, "W", "SIB");
                iNewReq360 = NewRequests(dDate, "W", "360");
                iNewReqTotal = iNewReq360;
                sMsgBody += "This week: " + iNewReqTotal.ToString("#,0") + System.Environment.NewLine;
                //sMsgBody += "      SIB: " + iNewReqSIB.ToString("#,0") + System.Environment.NewLine;
                //sMsgBody += "      360: " + iNewReq360.ToString("#,0") + System.Environment.NewLine + System.Environment.NewLine;

                //iNewReqSIB = NewRequests(dDate, "M", "SIB");
                iNewReq360 = NewRequests(dDate, "M", "360");
                iNewReqTotal = iNewReq360;
                sMsgBody += "This month: " + iNewReqTotal.ToString("#,0") + System.Environment.NewLine;
                //sMsgBody += "       SIB: " + iNewReqSIB.ToString("#,0") + System.Environment.NewLine;
                //sMsgBody += "       360: " + iNewReq360.ToString("#,0") + System.Environment.NewLine;

                sRet = "x";

                //------------------------------//
                // set period dates (prev month)//
                //------------------------------//
                dDate = dDate.AddMonths(-1);
                dDate = LastDayOfMonth(dDate);
                dBegOfMonth = FirstDayOfMonth(dDate);
                dEndOfMonth = LastDayOfMonth(dDate);
                dFirstPdBeg = dBegOfMonth;
                dFirstPdEnd = dBegOfMonth.AddDays(14);
                dFirstPdBeg360 = dBegOfMonth;
                dFirstPdEnd360 = dBegOfMonth.AddDays(15);

                dSecPdBeg = dBegOfMonth.AddDays(15);
                dSecPdEnd = dEndOfMonth;
                dSecPdBeg360 = dBegOfMonth.AddDays(15);
                dSecPdEnd360 = dEndOfMonth.AddDays(1);


                // Account for 360 storing dates in GMT
                dBegOfMonth360 = dBegOfMonth.AddHours(cfg_TimeZoneOffset);
                dEndOfMonth360 = dEndOfMonth.AddHours(cfg_TimeZoneOffset);
                dFirstPdBeg360 = dFirstPdBeg360.AddHours(cfg_TimeZoneOffset);
                dFirstPdEnd360 = dFirstPdEnd360.AddHours(cfg_TimeZoneOffset);
                dSecPdBeg360 = dSecPdBeg360.AddHours(cfg_TimeZoneOffset);
                dSecPdEnd360 = dSecPdEnd360.AddHours(cfg_TimeZoneOffset);

                dFirstPdAmtInvSIB = 0;
                dSecPdAmtInvSIB = 0;
                dFirstPdAmtInv360 = 0;
                dSecPdAmtInv360 = 0;

                // get amount invoiced SIB - first period
                //sqlCmd1.CommandText = "sp_Total_Billed";
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dFirstPdBeg);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dFirstPdEnd);
                //dFirstPdAmtInvSIB = (decimal)sqlCmd1.ExecuteScalar();
                dFirstPdAmtInvSIB = 0;

                // get amount invoiced SIB - second period
                //sqlCmd1.CommandText = "sp_Total_Billed";
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dSecPdBeg);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dSecPdEnd);
                //dSecPdAmtInvSIB = (decimal)sqlCmd1.ExecuteScalar();
                dSecPdAmtInvSIB = 0;

                // get amount invoiced 360 - first period
                dFirstPdAmtInv360 = oDB360.TotalBilledFirstPd(dFirstPdBeg360, dFirstPdEnd360, false);

                // get amount invoiced 360 - second period
                dSecPdAmtInv360 = oDB360.TotalBilledSecondPd(dSecPdBeg360, dSecPdEnd360, false);

                dFirstPdTotal = dFirstPdAmtInv360;
                dSecondPdTotal = dSecPdAmtInv360;
                dMonthTotal = dFirstPdTotal + dSecondPdTotal;

                sMsgBody += System.Environment.NewLine + System.Environment.NewLine;
                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + System.Environment.NewLine + System.Environment.NewLine;

                //sMsgBody += "First Billing Period" + System.Environment.NewLine;
                //sMsgBody += "Total billed - SIB:  " + dFirstPdAmtInvSIB.ToString("C") + System.Environment.NewLine;
                //sMsgBody += "Total billed - 360:  " + dFirstPdAmtInv360.ToString("C") + System.Environment.NewLine;
                sMsgBody += "First Billing Period:  " + dFirstPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                //sMsgBody += "Second Billing Period" + System.Environment.NewLine;
                //sMsgBody += "Total billed - SIB:  " + dSecPdAmtInvSIB.ToString("C") + System.Environment.NewLine;
                //sMsgBody += "Total billed - 360:  " + dSecPdAmtInv360.ToString("C") + System.Environment.NewLine;
                sMsgBody += "Second Billing Period: " + dSecondPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + " total:         " + dMonthTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "NOTE:" + System.Environment.NewLine;
                sMsgBody += "      360 dollar amounts are estimates until invoiced." + System.Environment.NewLine;
                sMsgBody += "      360 values are current as of the time this report was run." + System.Environment.NewLine;
                //sMsgBody += "      SIB values are as of the most recent invoice run." + System.Environment.NewLine;


                //// get amount paid to the inspectors
                //sqlCmd1.CommandText = "sp_Total_Paid";
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfPeriod);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfPeriod);
                //dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                //sqlConn1.Close();

                SendMail.SendMail oMail = new SendMail.SendMail();
                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = "axel@sibfla.com; randy@sibfla.com; andrea@sibfla.com; jeff@sibfla.com; tiana@sibfla.com; thomas@sibfla.com; Michael@sibfla.com; Jordan@sibfla.com;";
                //oMail.MailTo = "jeff@sibfla.com";
                oMail.MsgSubject = "Accounting Summary - " + dReportDate.ToLongDateString();
                oMail.MsgBody = sMsgBody;
                oMail.SMTPServer = cfg_smtpserver;

                oMail.SendHTML = false;

                sRet = oMail.Send();
                oMail = null;

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End AccountingSummary ++++");
                oLU.closeLog();

                //if (sqlConn1 != null)
                //    sqlConn1.Close();

            }


        }

        static void AccountingSummary(DateTime dDate)
        {

            ///////////////////////////////////////
            // Runs at 7pm, period 12am - 7pm
            ///////////////////////////////////////

            // Calc amount invoiced for periods (1/15 - 16-EOM) in month from date passed

            // Save date for display
            DateTime dReportDate = dDate;

            // set time to 12:00am
            dDate = ChangeTime(dDate, 0, 0, 0, 0);

            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dBegOfMonth = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth = LastDayOfMonth(dDate);
            DateTime dBegOfMonth360 = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth360 = LastDayOfMonth(dDate);
            DateTime dFirstPdBeg;
            DateTime dFirstPdEnd;
            DateTime dFirstPdBeg360;
            DateTime dFirstPdEnd360;
            DateTime dSecPdBeg;
            DateTime dSecPdEnd;
            DateTime dSecPdBeg360;
            DateTime dSecPdEnd360;

            string sMsgBody = "";
            string sRet = "xxxxx";

            // fix beg / end of month for 360 GMT
            dBegOfMonth360 = dBegOfMonth360.AddHours(cfg_TimeZoneOffset);
            dEndOfMonth360 = dEndOfMonth360.AddHours(cfg_TimeZoneOffset);

            // Current day
            int iNumInvoicedSIB = 0;
            decimal dAmtInvoicedSIB = 0;
            int iNumInvoiced360 = 0;
            decimal dAmtInvoiced360 = 0;

            // first period
            decimal dFirstPdAmtInvSIB = 0;
            decimal dFirstPdAmtInv360 = 0;
            int iFirstPdNumInvSIB = 0;
            int iFirstPdNumInv360 = 0;

            // second period
            decimal dSecPdAmtInvSIB = 0;
            decimal dSecPdAmtInv360 = 0;
            int iSecPdNumInvSIB = 0;
            int iSecPdNumInv360 = 0;

            // Payroll
            decimal dPendingPRSIB = 0;
            decimal dPendingPR360 = 0;
            decimal dPendingTotal = 0;

            // Open cases
            int iOpenCasesSIB = 0;
            int iOpenCases360 = 0;
            int iTotalOpen = 0;

            // new requests
            int iNewReqTotal = 0;
            int iNewReqSIB = 0;
            int iNewReq360 = 0;

            // set period dates (current month)
            dFirstPdBeg = dBegOfMonth;
            dFirstPdEnd = dBegOfMonth.AddDays(14);

            dFirstPdBeg360 = dBegOfMonth360;
            dFirstPdEnd360 = dBegOfMonth360.AddDays(15);

            // End of period = actual end date plus 1 to account for all items through midnight on the last day
            dSecPdBeg = dBegOfMonth.AddDays(15);
            dSecPdEnd = dEndOfMonth;

            dSecPdBeg360 = dBegOfMonth360.AddDays(15);
            dSecPdEnd360 = dEndOfMonth360.AddDays(1);

            // status date
            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin AccountingSummary ++++");

                iNumInvoicedSIB = 0;
                dAmtInvoicedSIB = 0;
                iOpenCasesSIB = 0;
                dPendingPRSIB = 0;

                // get pending payroll 360
                dPendingPR360 = oDB360.OutstandingPR();

                // Total PR
                dPendingTotal = dPendingPRSIB + dPendingPR360;

                // get total open items - 360
                iOpenCases360 = oDB360.OpenItems();

                // get number of items first completed today in 360 
                // set start date to 1 second after midnight 
                DateTime dWorkDate = dDate;
                dWorkDate = ChangeTime(dWorkDate, 0, 0, 1, 0);

                // Account for 360 storing time in GMT
                dWorkDate = dWorkDate.AddHours(cfg_TimeZoneOffset);

                iNumInvoiced360 = oDB360.CountCompleted(dWorkDate);

                // get total billing for items first completed today in 360 
                dAmtInvoiced360 = oDB360.TotalBilled(dWorkDate);

                dFirstPdAmtInvSIB = 0;
                iFirstPdNumInvSIB = 0;

                // if run date is in second period 
                if (dDate.Day > 15)
                {
                    dSecPdAmtInvSIB = 0;
                    iSecPdNumInvSIB = 0;
                }

                // get amount invoiced 360 - first period
                dFirstPdAmtInv360 = oDB360.TotalBilledFirstPd(dFirstPdBeg360, dFirstPdEnd360, true);

                // get number of items billed today in 360 
                // set start date to midnight 
                dWorkDate = dDate;
                dWorkDate = ChangeTime(dWorkDate, 0, 0, 0, 0);

                // Account for 360 storing time in GMT
                dWorkDate = dWorkDate.AddHours(cfg_TimeZoneOffset);

                iNumInvoiced360 = oDB360.CountCompleted(dWorkDate);

                // if run date is in second period -  get amount invoiced 360 - second period
                if (dDate.Day > 15)
                {
                    dSecPdAmtInv360 = oDB360.TotalBilledSecondPd(dSecPdBeg360, dSecPdEnd360, true);
                }

                decimal dFirstPdTotal = dFirstPdAmtInvSIB + dFirstPdAmtInv360;
                decimal dSecondPdTotal = dSecPdAmtInvSIB + dSecPdAmtInv360;
                decimal dMonthTotal = dFirstPdTotal + dSecondPdTotal;

                iTotalOpen = iOpenCases360 + iOpenCasesSIB;

                sMsgBody = "Accounting Summary for " + dReportDate.ToLongDateString() + "   Run Date " + dStatDate.ToShortDateString() + "  " + sStatTime + System.Environment.NewLine + System.Environment.NewLine;
                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + System.Environment.NewLine + System.Environment.NewLine;

                int iTotBilled = iNumInvoiced360 + iNumInvoicedSIB;
                decimal dTotBilled = dAmtInvoicedSIB + dAmtInvoiced360;
                sMsgBody += "Total billed today:  " + String.Format("{0,4}", iTotBilled) + "     " + dTotBilled.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "First Billing Period:  " + dFirstPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "Second Billing Period: " + dSecondPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + " total:         " + dMonthTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "Pending Inspector Payroll:  " + dPendingTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "Open cases: " + iTotalOpen.ToString("#,0") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "New Inspection Requests" + System.Environment.NewLine;
                iNewReq360 = NewRequests(dDate, "D", "360");
                iNewReqTotal = iNewReq360;
                sMsgBody += "Today: " + iNewReqTotal.ToString("#,0") + System.Environment.NewLine;

                iNewReq360 = NewRequests(dDate, "W", "360");
                iNewReqTotal = iNewReq360;
                sMsgBody += "This week: " + iNewReqTotal.ToString("#,0") + System.Environment.NewLine;

                iNewReq360 = NewRequests(dDate, "M", "360");
                iNewReqTotal = iNewReq360;
                sMsgBody += "This month: " + iNewReqTotal.ToString("#,0") + System.Environment.NewLine;

                sRet = "x";

                //------------------------------//
                // set period dates (prev month)//
                //------------------------------//
                dDate = dDate.AddMonths(-1);
                dDate = LastDayOfMonth(dDate);
                dBegOfMonth = FirstDayOfMonth(dDate);
                dEndOfMonth = LastDayOfMonth(dDate);
                dFirstPdBeg = dBegOfMonth;
                dFirstPdEnd = dBegOfMonth.AddDays(14);
                dFirstPdBeg360 = dBegOfMonth;
                dFirstPdEnd360 = dBegOfMonth.AddDays(15);

                dSecPdBeg = dBegOfMonth.AddDays(15);
                dSecPdEnd = dEndOfMonth;
                dSecPdBeg360 = dBegOfMonth.AddDays(15);
                dSecPdEnd360 = dEndOfMonth.AddDays(1);


                // Account for 360 storing dates in GMT
                dBegOfMonth360 = dBegOfMonth.AddHours(cfg_TimeZoneOffset);
                dEndOfMonth360 = dEndOfMonth.AddHours(cfg_TimeZoneOffset);
                dFirstPdBeg360 = dFirstPdBeg360.AddHours(cfg_TimeZoneOffset);
                dFirstPdEnd360 = dFirstPdEnd360.AddHours(cfg_TimeZoneOffset);
                dSecPdBeg360 = dSecPdBeg360.AddHours(cfg_TimeZoneOffset);
                dSecPdEnd360 = dSecPdEnd360.AddHours(cfg_TimeZoneOffset);

                dFirstPdAmtInvSIB = 0;
                dSecPdAmtInvSIB = 0;
                dFirstPdAmtInv360 = 0;
                dSecPdAmtInv360 = 0;
                dFirstPdAmtInvSIB = 0;
                dSecPdAmtInvSIB = 0;

                // get amount invoiced 360 - first period
                dFirstPdAmtInv360 = oDB360.TotalBilledFirstPd(dFirstPdBeg360, dFirstPdEnd360, false);

                // get amount invoiced 360 - second period
                dSecPdAmtInv360 = oDB360.TotalBilledSecondPd(dSecPdBeg360, dSecPdEnd360, false);

                dFirstPdTotal = dFirstPdAmtInv360;
                dSecondPdTotal = dSecPdAmtInv360;
                dMonthTotal = dFirstPdTotal + dSecondPdTotal;

                sMsgBody += System.Environment.NewLine + System.Environment.NewLine;
                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "First Billing Period:  " + dFirstPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "Second Billing Period: " + dSecondPdTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += String.Format("{0:MMMMMMMM yyyy}", dDate) + " total:         " + dMonthTotal.ToString("C") + System.Environment.NewLine + System.Environment.NewLine;

                sMsgBody += "NOTE:" + System.Environment.NewLine;
                sMsgBody += "      360 dollar amounts are estimates until invoiced." + System.Environment.NewLine;
                sMsgBody += "      360 values are current as of the time this report was run." + System.Environment.NewLine;

                SendMail.SendMail oMail = new SendMail.SendMail();
                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = "axel@sibfla.com; randy@sibfla.com; andrea@sibfla.com; jeff@sibfla.com; tiana@sibfla.com; thomas@sibfla.com; Michael@sibfla.com; Jordan@sibfla.com;";
                //oMail.MailTo = "jeff@sibfla.com";
                oMail.MsgSubject = "Accounting Summary - " + dReportDate.ToLongDateString();
                oMail.MsgBody = sMsgBody;
                oMail.SMTPServer = cfg_smtpserver;

                oMail.SendHTML = false;

                sRet = oMail.Send();
                oMail = null;

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End AccountingSummary ++++");
                oLU.closeLog();

                //if (sqlConn1 != null)
                //    sqlConn1.Close();

            }


        }
        static int NewRequests(DateTime dDate, string sPeriod, string sMode)
        {
               
            int iRetVal = 0;

            int iSIB = 0;
            int i360 = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;



            try
            {

                if (sMode == "SIB")
                {
                    // SIBOffice //
                    // set up SQL connection 
                    sqlConn1 = new SqlConnection(cfg_SIBUtilConnStr);
                    sqlCmd1 = new SqlCommand();
                    sqlCmd1.CommandType = CommandType.Text;
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    // Day
                    if (sPeriod.ToUpper() == "D")
                    {
                        sqlCmd1.CommandText = "SELECT newcases FROM Dashboard_Stats WHERE stattype='D' AND statusperiod = '" + dDate.ToString() + "'";
                        iSIB = (int)sqlCmd1.ExecuteScalar();
                    }

                    // Week
                    if (sPeriod.ToUpper() == "W")
                    {
                        DateTime dBegOfWeek = FirstDayofWeek(dDate);
                        sqlCmd1.CommandText = "SELECT newcases FROM Dashboard_Stats WHERE stattype='W' AND statusperiod = '" + dBegOfWeek.ToString() + "'";
                        iSIB = (int)sqlCmd1.ExecuteScalar();
                    }

                    // Month
                    if (sPeriod.ToUpper() == "M")
                    {
                        DateTime dBegOfMonth = FirstDayOfMonth(dDate);
                        sqlCmd1.CommandText = "SELECT newcases FROM Dashboard_Stats WHERE stattype='M' AND statusperiod = '" + dBegOfMonth.ToString() + "'";
                        iSIB = (int)sqlCmd1.ExecuteScalar();
                    }

                    iRetVal = iSIB;

                    sqlConn1.Close();
                    sqlCmd1 = null;

                }

                if (sMode == "360")
                {
                    // 360 //
                    // set up SQL connection 
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();
                    sqlCmd1.CommandType = CommandType.Text;
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    // Day
                    if (sPeriod.ToUpper() == "D")
                    {
                        sqlCmd1.CommandText = "SELECT newcases FROM Dashboard_Stats WHERE stattype='D' AND statusperiod = '" + dDate.ToString() + "'";
                        i360 = (int)sqlCmd1.ExecuteScalar();
                    }

                    // Week
                    if (sPeriod.ToUpper() == "W")
                    {
                        DateTime dBegOfWeek = FirstDayofWeek(dDate);
                        sqlCmd1.CommandText = "SELECT newcases FROM Dashboard_Stats WHERE stattype='W' AND statusperiod = '" + dBegOfWeek.ToString() + "'";
                        i360 = (int)sqlCmd1.ExecuteScalar();
                    }

                    // Month
                    if (sPeriod.ToUpper() == "M")
                    {
                        DateTime dBegOfMonth = FirstDayOfMonth(dDate);
                        sqlCmd1.CommandText = "SELECT newcases FROM Dashboard_Stats WHERE stattype='M' AND statusperiod = '" + dBegOfMonth.ToString() + "'";
                        i360 = (int)sqlCmd1.ExecuteScalar();
                    }

                    iRetVal = i360;

                    sqlConn1.Close();
                }

            }

            catch (Exception ex)
            {

                iRetVal = 0;
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

            return iRetVal;
        }
            
    }
            
    }



