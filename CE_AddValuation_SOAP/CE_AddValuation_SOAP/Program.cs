﻿using AddValuationSOAP.Corelogic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AddValuationSOAP
{
    class Program
    {

        static void Main(string[] args)
        {
            //Some values below such as Username are pulled from the App.config file in this project.

            ChannelFactory<IValuation> channelFactory = new ChannelFactory<IValuation>("ClearUsernameBinding_IValuation");

            channelFactory.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AdminUser"] 
                    + "@" + ConfigurationManager.AppSettings["CompanyAlias"];
            channelFactory.Credentials.UserName.Password = ConfigurationManager.AppSettings["Password"];
            IValuation iValuation = channelFactory.CreateChannel();

            AddValuationRequest request = new AddValuationRequest();
            request.CompanyID = new Guid (ConfigurationManager.AppSettings["CompanyId"]);
            request.Logon = ConfigurationManager.AppSettings["AdminUser"];
            request.CalculateValuation = true;
            request.ReturnAddedValuation = true;

            request.Valuation = new Valuation();
            request.Valuation.AssignedUser = ConfigurationManager.AppSettings["AdminUser"];
            //request.Valuation.ValuationNumber = "Test12345"; //Optional. If omitted, CoreLogic assigns a unique value.

            //Business Address is not required, but optional.
            request.Valuation.Business = new Business();
            request.Valuation.Business.Name = "THE IRON HORSE HOTEL";
            request.Valuation.Business.Address = new Address();
            request.Valuation.Business.Address.Line1 = "500 W Florida";
            request.Valuation.Business.Address.City = "Milwaukee";
            request.Valuation.Business.Address.PostalCode = "53024";
            request.Valuation.Business.Address.RegionCode = "WI";

            request.Valuation.EffectiveDate = DateTime.Now;
            
            //Single location.
            request.Valuation.Locations = new Location[1];
            request.Valuation.Locations[0] = new Location();
            request.Valuation.Locations[0].Address = new Address();
            request.Valuation.Locations[0].Address.Line1 = "500 W Florida";
            request.Valuation.Locations[0].Address.City = "Milwaukee";
            request.Valuation.Locations[0].Address.PostalCode = "53024";
            request.Valuation.Locations[0].Address.RegionCode = "WI";

            request.Valuation.Locations[0].IsHeadquarters = true;

            //Single building.
            request.Valuation.Locations[0].Buildings = new Building[1];
            request.Valuation.Locations[0].Buildings[0] = new Building();
            request.Valuation.Locations[0].Buildings[0].Name = "Building 1"; //Optional
            request.Valuation.Locations[0].Buildings[0].Type = BuildingType.Commercial;

            //Single section.
            request.Valuation.Locations[0].Buildings[0].Sections = new BuildingSection[1];
            request.Valuation.Locations[0].Buildings[0].Sections[0] = new BuildingSection();
            request.Valuation.Locations[0].Buildings[0].Sections[0].Name = "Section 1"; //Optional
            request.Valuation.Locations[0].Buildings[0].Sections[0].Stories = 6;
            request.Valuation.Locations[0].Buildings[0].Sections[0].GrossFloorArea = 95788;
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionQualityCode = "2.0";
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes = new ConstructionTypeCollection();
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes.BuildingType = BuildingType.Commercial;
            
            //Single construction type.
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes.ConstructionTypes = new ConstructionType[1];
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes.ConstructionTypes[0] = new ConstructionType();
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes.ConstructionTypes[0].ConstructionCode = "1";
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes.ConstructionTypes[0].Percent = 100;

            //Single occupancy
            request.Valuation.Locations[0].Buildings[0].Sections[0].Occupancies = new OccupancyTypeCollection();
            request.Valuation.Locations[0].Buildings[0].Sections[0].Occupancies.Occupancies = new OccupancyType[1];
            request.Valuation.Locations[0].Buildings[0].Sections[0].Occupancies.Occupancies[0] = new OccupancyType();
            request.Valuation.Locations[0].Buildings[0].Sections[0].Occupancies.Occupancies[0].Code = 1550;
            request.Valuation.Locations[0].Buildings[0].Sections[0].Occupancies.Occupancies[0].Percent = 100;

            try
            {
                //Call Commercial ExpressLync to create valuation.
                ResponseNewID response = iValuation.AddValuation(request);

                //Output values from response.
                var totalValuationRCV = response.Valuation.Totals.Rcv;
                Console.WriteLine("Policy Number: " + response.Valuation.ValuationNumber);
                Console.WriteLine("Valuation ID: " + response.Valuation.ID);
                Console.WriteLine("Valuation Total: " + totalValuationRCV.ToString("C"));

            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}
