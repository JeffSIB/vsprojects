﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Net.Mail;

namespace UnassignedAuto
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static string cfg_deliverto;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Range oRange = null;
        static string sReportDate;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_deliverto = colNameVal.Get("deliverto");

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                DateTime dt = new DateTime();
                dt = DateTime.Today;
                sReportDate = dt.ToShortDateString();

                string sDateDisp = dt.Month.ToString() + dt.Day.ToString() + dt.Year.ToString();
                string sEmailSubject = "Unassigned cases report - " + dt.ToShortDateString();
                string sExcelFileNameNoEx = cfg_outputdir + "UnassignedCases_" + sDateDisp;
                string sExcelFileName = sExcelFileNameNoEx + ".xlsx";

                // Delete file if it exists
                FileInfo fi = new FileInfo(sExcelFileName);
                if (fi.Exists)
                    fi.Delete();

                ///////////////////////////////////////////////////////////////


                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                int iAU = 0;
                int iBU = 0;
                int iCU = 0;
                int iDU = 0;
                int iEU = 0;
                int iFU = 0;
                int iAMU = 0;
                int iCGU = 0;

                int iAT = 0;
                int iBT = 0;
                int iCT = 0;
                int iDT = 0;
                int iET = 0;
                int iFT = 0;
                int iAMT = 0;
                int iCGT = 0;

                int iATF = 0;
                int iBTF = 0;
                int iCTF = 0;
                int iDTF = 0;
                int iETF = 0;
                int iFTF = 0;
                int iAMTF = 0;
                int iCGTF = 0;

                int iTotUassigned = 0;
                int iTotActive = 0;
                int iTotField = 0;

                string sDistrict = "";
                int iCount = 0;
                string sErrMsg = "";
                bool bErr = false;
                double dPct = 0;

                int iRow = 0;

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);

                // Get unassigned cases by district
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_UnassignedCasesByDistrictExclude";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();
                sqlReader = sqlCmd1.ExecuteReader();
                if (sqlReader.HasRows)
                {

                    sqlReader.Read();

                    // loop through rows
                    do
                    {
                        // count
                        if (sqlReader.IsDBNull(0))
                        {
                            iCount = 0;
                        }
                        else
                        {
                            iCount = (int)sqlReader.GetSqlInt32(0);
                        }
                        // District
                        if (sqlReader.IsDBNull(1))
                        {
                            sDistrict = "";
                        }
                        else
                        {
                            sDistrict = (string)sqlReader.GetSqlString(1);
                        }

                        sDistrict = sDistrict.ToUpper();
                        if (sDistrict == "A")
                        {
                            iAU = iCount;
                        }
                        else if (sDistrict == "B")
                        {
                            iBU = iCount;
                        }
                        else if (sDistrict == "C")
                        {
                            iCU = iCount;
                        }
                        else if (sDistrict == "D")
                        {
                            iDU = iCount;
                        }
                        else if (sDistrict == "E")
                        {
                            iEU = iCount;
                        }
                        else if (sDistrict == "F")
                        {
                            iFU = iCount;
                        }

                        else
                        {
                            sErrMsg += "Unrecognized District (Unassigned): " + sDistrict;
                            bErr = true;
                        }

                    } while (sqlReader.Read());     // Unassigned

                }   // has rows

                sqlReader.Close();

                // Get active cases by district
                sqlCmd1.CommandText = "sp_ActiveCasesByDistrictExclude";   
                sqlReader = sqlCmd1.ExecuteReader();
                if (sqlReader.HasRows)
                {

                    sqlReader.Read();

                    // loop through rows
                    do
                    {
                        // count
                        if (sqlReader.IsDBNull(0))
                        {
                            iCount = 0;
                        }
                        else
                        {
                            iCount = (int)sqlReader.GetSqlInt32(0);
                        }
                        // District
                        if (sqlReader.IsDBNull(1))
                        {
                            sDistrict = "";
                        }
                        else
                        {
                            sDistrict = (string)sqlReader.GetSqlString(1);
                        }

                        sDistrict = sDistrict.ToUpper();
                        if (sDistrict == "A")
                        {
                            iAT = iCount;
                        }
                        else if (sDistrict == "B")
                        {
                            iBT = iCount;
                        }
                        else if (sDistrict == "C")
                        {
                            iCT = iCount;
                        }
                        else if (sDistrict == "D")
                        {
                            iDT = iCount;
                        }
                        else if (sDistrict == "E")
                        {
                            iET = iCount;
                        }
                        else if (sDistrict == "F")
                        {
                            iFT = iCount;
                        }
                        else
                        {
                            sErrMsg += "Unrecognized District (Active): " + sDistrict;
                            bErr = true;
                        }

                    } while (sqlReader.Read());     // Active

                }   // has rows
                sqlReader.Close();

                // Get total in field by district
                sqlCmd1.CommandText = "sp_InFieldByDistrictExclude";
                sqlReader = sqlCmd1.ExecuteReader();
                if (sqlReader.HasRows)
                {

                    sqlReader.Read();

                    // loop through rows
                    do
                    {
                        // count
                        if (sqlReader.IsDBNull(0))
                        {
                            iCount = 0;
                        }
                        else
                        {
                            iCount = (int)sqlReader.GetSqlInt32(0);
                        }
                        // District
                        if (sqlReader.IsDBNull(1))
                        {
                            sDistrict = "";
                        }
                        else
                        {
                            sDistrict = (string)sqlReader.GetSqlString(1);
                        }

                        sDistrict = sDistrict.ToUpper();
                        if (sDistrict == "A")
                        {
                            iATF = iCount;
                        }
                        else if (sDistrict == "B")
                        {
                            iBTF = iCount;
                        }
                        else if (sDistrict == "C")
                        {
                            iCTF = iCount;
                        }
                        else if (sDistrict == "D")
                        {
                            iDTF = iCount;
                        }
                        else if (sDistrict == "E")
                        {
                            iETF = iCount;
                        }
                        else if (sDistrict == "F")
                        {
                            iFTF = iCount;
                        }
                        else
                        {
                            sErrMsg += "Unrecognized District (Active): " + sDistrict;
                            bErr = true;
                        }

                    } while (sqlReader.Read());     // Active

                }   // has rows
                sqlReader.Close();

                /////////////////////////////
                // AMRisc
                /////////////////////////////
                sqlCmd1.CommandText = "sp_Count_UnassignedInDistrictAMRisc";
                sqlCmd1.Parameters.Clear();
                iAMU = (int)sqlCmd1.ExecuteScalar();

                sqlCmd1.CommandText = "sp_Count_ActiveCasesInDistrictAMRisc";
                iAMT = (int)sqlCmd1.ExecuteScalar();

                sqlCmd1.CommandText = "sp_Count_InFieldByDistrictAMRisc";
                iAMTF = (int)sqlCmd1.ExecuteScalar();

                /////////////////////////////
                // Citizens
                /////////////////////////////
                sqlCmd1.CommandText = "sp_Count_UnassignedInDistrictCGCU";
                sqlCmd1.Parameters.Clear();
                iCGU = (int)sqlCmd1.ExecuteScalar();

                sqlCmd1.CommandText = "sp_Count_ActiveCasesInDistrictCGCU";
                iCGT = (int)sqlCmd1.ExecuteScalar();

                sqlCmd1.CommandText = "sp_Count_InFieldByDistrictCGCU";
                iCGTF = (int)sqlCmd1.ExecuteScalar();

                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;
                
                oExcel = new Excel.Application();
                oExcel.Visible = true;
                oWorkbook = oExcel.Workbooks.Add(1);
                o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                //o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing,oSIBWorksheet,oMissing,oMissing);
                o360Worksheet.Name = "Unassigned Cases";
                createHeader(o360Worksheet);

                iRow = 4;

                if (iAT > 0 && iAU > 0)
                {
                    dPct = ((double)iAU / iATF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "A", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iAU.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iAT.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iATF.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, dPct.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "###0.00", "C");
                iRow++;

                if (iBT > 0 && iBU > 0)
                {
                    dPct = ((double)iBU / iBTF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "B", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iBU.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iBT.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iBTF.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, dPct.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "###0.00", "C");
                iRow++;

                if (iCT > 0 && iCU > 0)
                {
                    dPct = ((double)iCU / iCTF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "C", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iCU.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iCT.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iCTF.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, dPct.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "###0.00", "C");
                iRow++;

                if (iDT > 0 && iDU > 0)
                {
                    dPct = ((double)iDU / iDTF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "D", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iDU.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iDT.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iDTF.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, dPct.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "###0.00", "C");
                iRow++;

                if (iET > 0 && iEU > 0)
                {
                    dPct = ((double)iEU / iETF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "E", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iEU.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iET.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iETF.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, dPct.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "###0.00", "C");
                iRow++;


                if (iFT > 0 && iFU > 0)
                {
                    dPct = ((double)iFU / iFTF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "F", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iFU.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iFT.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iFTF.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, dPct.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "###0.00", "C");
                iRow++;

                // AMRisc
                if (iAMT > 0 && iAMU > 0)
                {
                    dPct = ((double)iAMU / iAMTF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "AMRisc", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iAMU.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iAMT.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iAMTF.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, dPct.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "###0.00", "C");
                iRow++;

                // CGCU
                if (iCGT > 0 && iCGU > 0)
                {
                    dPct = ((double)iCGU / iCGTF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "AMRisc", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iCGU.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iCGT.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iCGTF.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, dPct.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "###0.00", "C");
                iRow++;

                //totals
                iTotUassigned = iAU + iBU + iCU + iDU + iEU + iFU + iAMU + iCGU; 
                iTotActive = iAT + iBT + iCT + iDT + iET + iFT + iAMT + iCGT;
                iTotField = iATF + iBTF + iCTF + iDTF + iETF + iFTF + iAMTF + iCGTF;

                if (iTotField > 0 && iTotUassigned > 0)
                {
                    dPct = ((double)iTotUassigned / iTotField) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "Total", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iTotUassigned.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iTotActive.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iTotField.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, dPct.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "###0.00", "C");

                iRow = 15;
                string sText = "Unassigned: Case status = Ordered";
                addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
                iRow++;

                sText = "Total Active: Case status = Hold, Ordered, Assigned, In Progress, QA, QA Rejected, Customer Rejected, QA Hold";
                addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
                iRow++;

                sText = "Total in Field: Case Status = Ordered, Assigned, In Progress, QA Rejected";
                addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
                iRow++;

                sText = "Percentage: Unassigned / Total in Field";
                addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
                iRow++;
                iRow++;

                sText = "District assignment based on location zip code being assgned to district";
                addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");


                //oWorkbook.SaveAs(cfg_outputdir + "ReviewerStats360_" + sDateDisp, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.Close(true, oMissing, oMissing);
                oExcel.Quit();
                msMsgBody += "Processing complete" + System.Environment.NewLine + cfg_outputdir + sExcelFileNameNoEx + System.Environment.NewLine + System.Environment.NewLine;

                releaseObject(oExcel);
                releaseObject(oWorkbook);
                releaseObject(o360Worksheet);
                sendExcelFile(sEmailSubject, sExcelFileName);
                msMsgBody += "File sent " + sExcelFileName;
            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(msMsgBody);
                }

            }
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeader(Excel._Worksheet oWorkSheet)
        {

            Excel.Range oRange;


            oWorkSheet.get_Range("A1", "E1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "E1");
            oRange.FormulaR1C1 = "Unassigned Cases as of " + sReportDate;

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "District";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 20;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Unassigned";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "Total Active";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Total In Field";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Percent";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);          

        }

        //static void createFooter(Excel._Worksheet oWorkSheet)
        //{

        //    oWorkSheet.get_Range("A1", "E1").Merge(false);
        //    oRange = oWorkSheet.get_Range("A1", "E1");
        //    oRange.FormulaR1C1 = "Unassigned Cases as of " + sReportDate;

        //    string sText = "Unassigned: Case status = Ordered\r\n";
        //    addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
        //    iRow++;

        //    sText = "Total Active: Case status = Hold, Ordered, Assigned, In Progress, QA, QA Rejected, Customer Rejected, QA Hold\r\n";
        //    addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
        //    iRow++;

        //    sText = "Total in Field: Case Status = Ordered, Assigned, In Progress, QA\r\n";
        //    addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
        //    iRow++;

        //    sText = "Percentage: Unassigned / Total in Field";
        //    addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
        //    iRow++;



        //}


        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2, string format, string sHorizAlign)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            oRange.WrapText = true; 
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
        }

        static void addMultiCellData(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2, string format, string sHorizAlign)
        {
            oWorkSheet.get_Range(cell1, cell2).Merge(false);
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.FormulaR1C1 = data;
        }


        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }



        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "Unassigned Cases";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Unassigned Cases Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = cfg_deliverto;
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;

            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }


    }
}
