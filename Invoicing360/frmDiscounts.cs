using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Invoicing360
{
    public partial class frmDiscount : Form
    {
        private string cfg_SQL360ConnStr;


        private decimal decDiscountAmount = 0;

        public frmDiscount()
        {
            InitializeComponent();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPreferences_Load(object sender, EventArgs e)
        {
            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_SQL360ConnStr = colNameVal.Get("360UtilConnStr");

        }
        
        private void bSave_Click(object sender, EventArgs e)
        {

            if (tbInvNum.Text.Length == 0)
            {
                MessageBox.Show("The invoice number entered is not valid.", "Invalid invoice number", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbAmount.Text.Length == 0)
            {
                MessageBox.Show("Please enter an amount.", "Invalid discount amount", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbDesc.Text.Length == 0)
            {
                MessageBox.Show("Please enter a description.", "Invalid discount description", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            try
            {
                // init SQL connection
                string sSQLCmd = "";
                int iRetVal = 0;
                decimal decTotInvAmount = 0;
                SqlConnection sqlConn = new SqlConnection(cfg_SQL360ConnStr);
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConn;
                sqlConn.Open();
                sqlCommand.CommandType = CommandType.Text;

                // verify invoice number
                string sInvoiceNum = tbInvNum.Text;

                sSQLCmd = "SELECT TotalInvoiceAmount FROM CustomerInvoices WHERE CustomerInvoiceNumber = " + sInvoiceNum + ";";

                sqlCommand.CommandText = sSQLCmd;

                object oRet = sqlCommand.ExecuteScalar();
                if (oRet == null)
                {
                    MessageBox.Show("The invoice number entered does not appear to be valid.", "Invalid invoice number", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    decDiscountAmount = 0;
                    return;
                }
                else
                {
                    decTotInvAmount = Convert.ToDecimal(oRet);
                }

                decTotInvAmount = decTotInvAmount - decDiscountAmount;

                if (decTotInvAmount < 0)
                {
                    MessageBox.Show("The total invoice would be less than zero if this discount is applied.", "Invalid discount", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    decDiscountAmount = 0;
                    return;
                }

                // Update invoice
                sSQLCmd = "UPDATE CustomerInvoices SET discountamount = " + decDiscountAmount.ToString() + ", DiscountDescription = '" + tbDesc.Text + "', TotalInvoiceAmount =  " + decTotInvAmount + " WHERE CustomerInvoiceNumber = " + sInvoiceNum + ";";

                sqlCommand.CommandText = sSQLCmd;

                iRetVal = sqlCommand.ExecuteNonQuery();

                sqlConn.Close();

                this.Cursor = Cursors.Default;

                MessageBox.Show("Discount applied successfully.", "Customer discount", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Discount was not applied due to an error.\r\n\r\n" + ex.Message + "\r\n" + ex.Message.ToString(), "Error entering customer discount", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                this.Cursor = Cursors.Default;
                decDiscountAmount = 0;
            }
            bSave.Enabled = false;
            bCancel.Text = "Close";
        }

        private void tbAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            string keyInput = e.KeyChar.ToString();

            if (Char.IsDigit(e.KeyChar))
            {
                // Digits are OK
            }
            else if (keyInput.Equals(".") || keyInput.Equals("-"))
            {
                // Decimal separator is OK
            }
            else if (e.KeyChar == '\b')
            {
                // Backspace key is OK
            }
            else if (e.KeyChar == (char)Keys.Return)
            {

                string sTemp = tbAmount.Text;
                if (sTemp.Contains("$"))
                {
                    sTemp = sTemp.Replace("$", "0");
                }
                decDiscountAmount = Convert.ToDecimal(sTemp);

                tbAmount.Text = string.Format("{0:c}", decDiscountAmount);
            }

            else
            {
                e.Handled = true;
            }
        }

        private void tbAmount_TextChanged(object sender, EventArgs e)
        {
        }

        private void tbAmount_Leave(object sender, EventArgs e)
        {
            if (tbAmount.Text.Length > 0)
            {
                string sTemp = tbAmount.Text;
                if (sTemp.Contains("$"))
                {
                    sTemp = sTemp.Replace("$", "0");
                }
                decDiscountAmount = Convert.ToDecimal(sTemp);

                tbAmount.Text = string.Format("{0:c}", decDiscountAmount);
            }
        }


    }
}