﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Xceed.Grid;
using Xceed.Grid.Reporting;
using Xceed.Grid.Editors;
using Xceed.Grid.Viewers;
using Xceed.Grid.Collections;
using Xceed.Editors;
using System.Data.SqlClient;
using Xceed.SmartUI;
using Xceed.SmartUI.Controls.ToolBar;
using Xceed.SmartUI.UIStyle;
using System.IO;
using System.Diagnostics;
using System.Threading;



namespace Invoicing360
{
    public partial class frmMain : System.Windows.Forms.Form
    {

        private string cfg_SQL360ConnStr;
        private string cfg_SQLMainSIBIConnStr;
        private string cfg_SQLMainUtilConnStr;
        static string cfg_ExportDir;
        static string cfg_ExportFile;

        private string rptTitle = "";
        private Xceed.Grid.GridControl xNewGrid = new GridControl();
        ColumnManagerRow xNewGridColumnManagerRow;
        static SortedColumnList sclxNewGrid;
        private int miRowCount;
        private decimal mdecTotalInvAmt;
        private DateTime mdDueDate;
       

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_SQL360ConnStr = colNameVal.Get("360UtilConnStr");
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");
            cfg_SQLMainUtilConnStr = colNameVal.Get("SQLMainUtilConnStr");
            cfg_ExportDir = colNameVal.Get("ExportDir");
            cfg_ExportFile = colNameVal.Get("ExportFile");

            // place grid on panel
            this.panelMain.Controls.Add(xNewGrid);

            xNewGrid.Clear();
            this.PrepareGrid(0);
            this.Enabled = true;
            this.Visible = true;

            mdDueDate = calcDueDate();
            dtpDueDate.Value = mdDueDate;
        }

        private DateTime calcDueDate()
        {
            DateTime dCurDate = DateTime.Now;
            DateTime dDueDate = DateTime.Now;

            int iDay = dCurDate.Day;

            if (iDay > 15)
            {
                dDueDate = dDueDate.AddMonths(1);
                dDueDate = FirstDayOfMonth(dDueDate).AddDays(14);
            }
            else
            {
                dDueDate = LastDayOfMonth(dDueDate);
            }

            return dDueDate;
            
        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }

        private void PrepareGrid(int iMode)
        {
            // mode
            //  0 = normal - used by invoicing staff
            //  1 = uses invoiced date for printing pulling report 

            try
            {

                tSSlblStatus.Text = "Status: Loading...";
                tSSlblStatus.BackColor = System.Drawing.Color.Yellow;
                tSSlblAmount.Text = "Total amount invoiced: ";
                tSSlblCount.Text = "Invoices: ";
                sStrip1.Refresh();
                this.Cursor = Cursors.WaitCursor;
                this.Refresh();
                Application.DoEvents();

                // header row
                xNewGridColumnManagerRow = new ColumnManagerRow();
                xNewGrid.FixedHeaderRows.Add(xNewGridColumnManagerRow);

                //Columns
                xNewGrid.Columns.Add(new Column("InvoiceNumber", typeof(int)));
                xNewGrid.Columns["InvoiceNumber"].Title = "Invoice";
                xNewGrid.Columns["InvoiceNumber"].Width = 90;
                xNewGrid.Columns["InvoiceNumber"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["InvoiceNumber"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;

                xNewGrid.Columns.Add(new Column("Acnt", typeof(string)));
                xNewGrid.Columns["Acnt"].Title = "Account";
                xNewGrid.Columns["Acnt"].Width = 60;
                xNewGrid.Columns["Acnt"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["Acnt"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;

                xNewGrid.Columns.Add(new Column("CustName", typeof(string)));
                xNewGrid.Columns["CustName"].Title = "Customer name";
                xNewGrid.Columns["CustName"].Width = 200;
                xNewGrid.Columns["CustName"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Left;
                xNewGrid.Columns["CustName"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Left;

                xNewGrid.Columns.Add(new Column("InvoiceDate", typeof(DateTime)));
                xNewGrid.Columns["InvoiceDate"].Title = "Invoice date";
                xNewGrid.Columns["InvoiceDate"].Width = 120;
                xNewGrid.Columns["InvoiceDate"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["InvoiceDate"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;

                xNewGrid.Columns.Add(new Column("Amount", typeof(decimal)));
                xNewGrid.Columns["Amount"].Title = "Invoice amount";
                xNewGrid.Columns["Amount"].Width = 120;
                xNewGrid.Columns["Amount"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["Amount"].FormatSpecifier = "c2";
                xNewGrid.Columns["Amount"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                
                xNewGrid.Columns.Add(new Column("InvoiceID", typeof(string)));
                xNewGrid.Columns["InvoiceID"].Visible = false;

                //grid wide settings
                xNewGrid.ReadOnly = true;
                xNewGrid.Dock = DockStyle.Fill;
                xNewGrid.FixedColumnSplitter.Visible = false;
                xNewGrid.RowSelectorPane.Visible = false;

                // prevent cell navigation
                xNewGrid.AllowCellNavigation = false;

                // resize
                xNewGrid.Resize += new System.EventHandler(xNewGrid_Resize);

                
                // declare vars
                miRowCount = 0;
                mdecTotalInvAmt = 0;
                int iInvNumber = 0;
                string sAcnt = "";
                string sCustName = "";
                string sInvoiceID = "";
                DateTime dInvDate = DateTime.Now;
                decimal decInvAmt = 0;
                DateTime dGenDate = dInvDate;

                // init SQL connection
                string sSQLCmd = "";
                SqlDataReader sqlDR;
                SqlConnection sqlConn = new SqlConnection(cfg_SQL360ConnStr);
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConn;
                sqlConn.Open();

                // set SQL based on user preferences
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Clear();
                sqlCommand.Parameters.AddWithValue("@gendate", dGenDate);
                //sSQLCmd = "SELECT ci.CustomerInvoiceNumber as InvoiceNumber, cu.LookupID as CustomerAcnt, cu.Name as Customer, ci.GenerationDate as DateInvoiced,ci.TotalInvoiceAmount as Amount, ci.CustomerInvoiceID as InvoiceID " +
                //    "FROM CustomerInvoices ci INNER JOIN customers cu on cu.customerID = ci.customerID " +
                //    "WHERE ci.Syncdate IS NULL " +
                //    "ORDER BY ci.GenerationDate, ci.CustomerInvoiceID;";

                sqlCommand.CommandText = "sp_Invoice_LoadGrid";

                rptTitle = "All invoices";

                sqlDR = sqlCommand.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {
                    // add row to grid
                    xNewGrid.BeginInit();


                    // loop through records
                    while (sqlDR.Read())
                    {

                        try
                        {

                            // invoice number
                            if (sqlDR.IsDBNull(0))
                            {
                                iInvNumber = 0;
                            }
                            else
                            {
                                iInvNumber = (int)sqlDR.GetSqlInt32(0);
                            }

                            // acnt 
                            if (sqlDR.IsDBNull(1))
                            {
                                sAcnt = "";
                            }
                            else
                            {
                                sAcnt = sqlDR.GetSqlString(1).ToString();
                            }

                            // customer name
                            if (sqlDR.IsDBNull(2))
                            {
                                sCustName = "";
                            }
                            else
                            {
                                sCustName = sqlDR.GetSqlString(2).ToString();
                            }
                            
                            // invoice date 
                            dInvDate = sqlDR.GetDateTime(3);

                            decInvAmt = (decimal)sqlDR.GetSqlMoney(4);

                            // 360 invoice ID
                            sInvoiceID = sqlDR.GetGuid(5).ToString();
                            
                            // Load data into grid row
                            Xceed.Grid.DataRow dataRow = xNewGrid.DataRows.AddNew();
                            dataRow.Cells["InvoiceNumber"].Value = iInvNumber;
                            dataRow.Cells["Acnt"].Value = sAcnt;
                            dataRow.Cells["CustName"].Value =sCustName;
                            dataRow.Cells["InvoiceDate"].Value = dInvDate;
                            dataRow.Cells["Amount"].Value = decInvAmt;
                            dataRow.Cells["InvoiceID"].Value = sInvoiceID;


                            dataRow.EndEdit();
                            miRowCount++;
                            mdecTotalInvAmt += decInvAmt;

                        }
                        finally
                        {
                            xNewGrid.EndInit();

                        }	//try


                    }	// while


                }	// has rows
                else
                {

                    MessageBox.Show("There were no requests found matching your current filter criteria.\r\nPlease check your settings.", "No data found", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }


                sqlConn.Close();

                if (sclxNewGrid == null)
                {
                    sclxNewGrid = xNewGrid.SortedColumns;
                }


                if (sclxNewGrid.Count == 0)
                {
                    xNewGrid.Columns["InvoiceNumber"].SortDirection = SortDirection.Ascending;
                    sclxNewGrid = xNewGrid.SortedColumns;
                }

                xNewGrid_Resize(null, null);

                tSSlblCount.Text = "Invoices: " + miRowCount.ToString("#,#");
                tSSlblAmount.Text = "Total amount invoiced: " + string.Format("{0:c}", mdecTotalInvAmt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                tSSlblStatus.Text = "Status: Ready";
                tSSlblStatus.BackColor = System.Drawing.Color.LightGray;
                this.Cursor = Cursors.Default;
            }
        }

         
 
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void designExportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GenerateReportForm reportForm = new GenerateReportForm(xNewGrid);
            reportForm.ShowDialog();
        }

        private void quickReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Report report = new Report(xNewGrid);
            report.PrintPreview();
        }

 

        // Grid resize
        private void xNewGrid_Resize(object sender, System.EventArgs e)
        {
            int iAvailWidth = xNewGrid.DisplayRectangle.Width;
            int iTotColWidth = 0;
            int iResizeCol = 2;
            int iAdjustment = 0;

            if (iAvailWidth == 0)
                return;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xNewGrid.Columns)
            {
                if (column.Index != iResizeCol)
                {
                    Console.WriteLine(column.FieldName);
                    iTotColWidth += column.Width;
                }

            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xNewGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }
            
            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xNewGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);
            
            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.Width = iWidth;
            }
            else
            {
                xResizeColumn.Width = iWidth;
            }
        }

        // resize grid for print
        private void xNewGrid_ResizeForPrint(int iAvailWidth)
        {
            int iTotColWidth = 0;
            int iResizeCol = 2;
            int iAdjustment = 0;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xNewGrid.Columns)
            {
                if (column.Index != iResizeCol)
                    iTotColWidth += column.Width;
            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xNewGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xNewGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
            else
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
        }

        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

 
        private void customToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GenerateReportForm reportForm = new GenerateReportForm(xNewGrid);
            reportForm.ShowDialog();

        }

        private void standardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.xNewGrid.ReportSettings.Title = rptTitle;
            this.xNewGrid.ReportSettings.Landscape = true;

            xNewGrid_ResizeForPrint(850);

            // base font for the report
            Font font = new Font("Arial", 9);
            this.xNewGrid.ReportStyle.Font = font;
            
            Report report = new Report(xNewGrid);
            ReportStyleSheet reportStyleSheet = report.ReportStyleSheet; 


            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            reportStyleSheet.Grid.DataRows[0].BackColor = Color.FromArgb(205,255,205);
            reportStyleSheet.Grid.DataRows[1].BackColor = Color.Transparent;

            report.ReportStyleSheet.PageHeader.BottomBorder.Thickness = ReportLineThickness.SingleThin;
            report.ReportStyleSheet.PageHeader.BottomMargin = 5;
            report.ReportStyleSheet.PageHeader.Font = new Font(xNewGrid.ReportStyle.Font.FontFamily, 10, FontStyle.Bold);

            this.xNewGridColumnManagerRow.ReportStyle.RepeatOnEachPage = true;
            this.xNewGridColumnManagerRow.ReportStyle.BackColor = Color.DarkGray;
            this.xNewGridColumnManagerRow.ReportStyle.ForeColor = Color.White;
            this.xNewGridColumnManagerRow.ReportStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            this.xNewGridColumnManagerRow.ReportStyle.WordWrap = true;
            this.xNewGridColumnManagerRow.ReportStyle.RowHeightExpansion = RowHeightExpansion.ExpandWithoutLimits;
            this.xNewGridColumnManagerRow.ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            this.xNewGridColumnManagerRow.ReportStyle.VerticalAlignment = VerticalAlignment.Bottom;
            
            report.ReportStyleSheet.PageHeader.LeftElement.TextFormat = "Invoiced Items Report" + Environment.NewLine + "%Title%";
            report.ReportStyleSheet.PageHeader.RightElement.TextFormat =
              "Page : %Page format=000%" + Environment.NewLine +
              "Date : %DateTime format=g%";

            report.ReportStyleSheet.PageFooter.Font = new Font(xNewGrid.ReportStyle.Font.FontFamily, 10, FontStyle.Bold);
            report.ReportStyleSheet.PageFooter.TopBorder.Thickness = ReportLineThickness.SingleThin;
            report.ReportStyleSheet.PageFooter.LeftElement.TextFormat = "Total number of invoices: " + miRowCount.ToString("#,#");
            report.ReportStyleSheet.PageFooter.CenterElement.TextFormat = "Total amount invoiced: " + string.Format("{0:c}", mdecTotalInvAmt);

            report.PrintPreview();
        }
  
        private void tsmiRefresh_Click(object sender, EventArgs e)
        {
            xNewGrid.Clear();
            PrepareGrid(0);
        }

        private string getUserID()
        {
            string sUserID = "";

            try
            {
                sUserID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return sUserID;
        }

        private void tsmiExport_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Current due date is: " + mdDueDate.ToShortDateString() + "\r\n\r\nPlease verify due date is correct.", "Export to Certiflex", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
            {
                return;
            }

            if (MessageBox.Show("Export 360 invoice data to Certiflex now?", "Export 360 invoice data to Certiflex", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                if (buildExportFile())
                {
                    MessageBox.Show("The export was successfull.", "Export Invoice Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Invoice data from 360 was not exported.", "Export Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }
            else
            {
                MessageBox.Show("Export cancelled.", "Export 360 invoice data to Certiflex", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        private bool buildExportFile()
        {
            
            // ******
            // TODO: 
            // Export file must be erased after import into CertiFLEX (once SIBOffice invoicing stops)
            // ** NO LONGER USED Set sync date in CustomerInvoices
            // ******
            // If export file does not exist - create it

            // Append 360 invoice data to SIBOffice export 
            // Set sync date in CustomerInvoices

            // Export file from SIBOffice "j:\cfx9\xconnect\arimport.TXT"
            // Also copied to "j:\cfx9\xconnect\SI" & Format(Date, "yymmdd") & ".TXT"

            bool bRet = false;

           
            string sAcnt = "";
            string sInvoiceNum = "";
            DateTime dtInvoiceDate;
            decimal decInvAmt = 0;
            string sInvoiceAmount = "";
            string sLine = "";
            string sFilePath = "";

            DateTime dt = new DateTime();
            dt = DateTime.Today;
            string sDateString = dt.Month.ToString() + dt.Day.ToString() + dt.Year.ToString();
            
            ReadOnlyDataRowList dataRows = xNewGrid.GetSortedDataRows(true);

            // Stop if nothing in grid
            if (dataRows.Count == 0)
            {
                MessageBox.Show("There are no items to export", "Create Export File", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            
            this.Cursor = Cursors.WaitCursor;

            try
            {

                // Create certiflex export file
                StreamWriter fs;

                // if export file exists, rename and delete it
                FileInfo fiExportFile = new FileInfo(cfg_ExportDir + cfg_ExportFile);
                if (fiExportFile.Exists)
                {

                    fiExportFile.CopyTo(cfg_ExportDir + "SI" + sDateString + ".TXT");
                    fiExportFile.Delete();
                }
                fiExportFile = null;

                // Create export file 
                fs = File.CreateText(cfg_ExportDir + cfg_ExportFile);
                fs.Flush();
                fs.Close(); 
                fs = null;

                // Open for append
                fs = File.AppendText(cfg_ExportDir + cfg_ExportFile);

                foreach (Xceed.Grid.DataRow dr in dataRows)
                {

                    sInvoiceNum = dr.Cells["InvoiceNumber"].Value.ToString();
                    sAcnt = dr.Cells["Acnt"].Value.ToString();
                    dtInvoiceDate = (DateTime)dr.Cells["InvoiceDate"].Value;
                    decInvAmt = (decimal)dr.Cells["Amount"].Value;
                    sInvoiceAmount = string.Format("{0:,#.00}", decInvAmt);

                    // write to file
                    sLine = "TRNS\t" + sAcnt + "\t0\t\"" + sInvoiceNum + "\"\t" + string.Format("{0:yyyyMMdd}",mdDueDate) + "\t0\t1";
                    fs.WriteLine(sLine);
                    sLine = "";
                    sLine = "SPL\t0011\t1\t" + sInvoiceAmount + "\t" + sInvoiceAmount + "\t" + sInvoiceAmount;
                    fs.WriteLine(sLine);

                }

                fs.Flush();
                fs.Close();

                this.Cursor = Cursors.Default;
                bRet = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + sFilePath + "\r\n" + ex.Message.ToString(), "Error creating export file", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

            return bRet;
        }

        private bool setSnycDate()
        {
            ////////////////////////////
            // No longer used 3/31/20 - can't write to 360
            // No longer using sync date as criteria to load grid
            ////////////////////////////

            bool bRet = false;

            ReadOnlyDataRowList dataRows = xNewGrid.GetSortedDataRows(true);
            string sInvoiceID = "";
            DateTime dSyncDate = DateTime.Now;

            this.Cursor = Cursors.WaitCursor;

            try
            {


                ////// TODO //////
                // Append to ARImport.Txt (create if it does not exist?)
                // Copy Arimport.Txt to SI...Txt
                // Set SyncDate


                // init SQL connection
                string sSQLCmd = "";
                int iRetVal = 0;
                SqlConnection sqlConn = new SqlConnection(cfg_SQL360ConnStr);
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConn;
                sqlConn.Open();
                sqlCommand.CommandType = CommandType.Text;


                foreach (Xceed.Grid.DataRow dr in dataRows)
                {

                    sInvoiceID = dr.Cells["InvoiceID"].Value.ToString();

                    sSQLCmd = "UPDATE CustomerInvoices SET syncdate = '" + dSyncDate.ToString() + "' WHERE CustomerInvoiceID = '" + sInvoiceID + "';";

                    sqlCommand.CommandText = sSQLCmd;

                    iRetVal = sqlCommand.ExecuteNonQuery();
                    
                }

                sqlConn.Close();

                this.Cursor = Cursors.Default;
                bRet = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.Message.ToString(), "Error finalizing 360 invoices", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

            return bRet;        
        
        
        
        }

        private void dtpDueDate_ValueChanged(object sender, EventArgs e)
        {
            mdDueDate = dtpDueDate.Value;

        }

        private void tsmiFinalize_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("This step should be run after the invoice data has been successfully imported into CertiFlex.\r\n\r\nRun this procedure now?", "Finalize Invoicing", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                if (setSnycDate())
                {
                    MessageBox.Show("The process was successful.", "Finalize Invoices", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("The process was not successful.", "Finalize Invoices", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Process cancelled.", "Finalize Invoices", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void discountsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // show form
            frmDiscount frmDsc = new frmDiscount();
            frmDsc.ShowDialog();
            frmDsc.TopMost = true;

            // refresh on return
            xNewGrid.Clear();
            PrepareGrid(0);


        }

      

     
      }
}