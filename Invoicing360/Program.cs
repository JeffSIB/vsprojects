﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using Xceed.Grid;
using Xceed.Grid.Collections;
using Xceed.Grid.Editors;
using Xceed.SmartUI;
using Xceed.SmartUI.Controls.ToolBar;
using Xceed.SmartUI.UIStyle;

namespace Invoicing360
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Xceed.Grid.Licenser.LicenseKey = "GRD38KM45BRABWYYN5A";
                Xceed.SmartUI.Licenser.LicenseKey = "SUN359EWYTJ4HBP5NJA";

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmMain());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "System Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
