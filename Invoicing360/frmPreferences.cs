using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Invoicing360
{
    public partial class frmPreferences : Form
    {
        int miStatus;

        public int retStatus
        {
            get
            {
                return miStatus;
            }
        }

        public frmPreferences()
        {
            InitializeComponent();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPreferences_Load(object sender, EventArgs e)
        {
            miStatus = 0;   // cancelled

            // load user settings
           // tbMinAge.Text = Properties.Settings.Default.MinAge;
            tbAcnt.Text = Properties.Settings.Default.Acnt;
            tbInsp.Text = Properties.Settings.Default.Insp;
            cbPaper.Checked = Properties.Settings.Default.Paper;
        }
        
        private void bSave_Click(object sender, EventArgs e)
        {

           
            tbAcnt.Text = tbAcnt.Text.Trim();
            if (tbAcnt.Text.Length > 0 && Convert.ToInt32(tbAcnt.Text) == 0)
                tbAcnt.Text = "";

            tbInsp.Text = tbInsp.Text.Trim();

            miStatus = 1;

            // Save user preferences
            //Properties.Settings.Default.MinAge = tbMinAge.Text.Trim();
            Properties.Settings.Default.Acnt = tbAcnt.Text.Trim();
            Properties.Settings.Default.Insp = tbInsp.Text.Trim();
            Properties.Settings.Default.Paper = cbPaper.Checked;

            Properties.Settings.Default.Save();

            this.Visible = false;
        }


    }
}