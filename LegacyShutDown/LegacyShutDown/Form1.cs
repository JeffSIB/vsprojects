﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LegacyShutDown
{
    public partial class frmMain : Form
    {
        
        private System.Windows.Forms.Timer timer1;
        private int counter = 10;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            lblCountDown.Visible = false;
            lblCount.Visible = false;
            lblTermComp.Visible = false;
        }

        private void btnGo_Click(object sender, EventArgs e)
        {

            DialogResult dialogResult = MessageBox.Show("Are you sure?", "Confirm Shutdown", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                DialogResult dialogResult2 = MessageBox.Show("Are you really sure?", "Confirm Shutdown", MessageBoxButtons.YesNo);
                if (dialogResult2 == DialogResult.Yes)
                {
                    lblCountDown.Visible = true;
                    lblCount.Visible = true;
                    btnGo.Visible = false;
                    this.Refresh();

                    timer1 = new System.Windows.Forms.Timer();
                    timer1.Tick += new EventHandler(timer1_Tick);
                    timer1.Interval = 1000; // 1 second
                    timer1.Start();
                    lblCountDown.Text = counter.ToString();
                }

            }


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            counter--;
            if (counter == 5)
            {
                timer1.Stop();
                DialogResult dialogResult3 = MessageBox.Show("WAIT! THIS IS YOUR LAST CHANCE!" + System.Environment.NewLine + System.Environment.NewLine +  "ARE YOU REALLY SURE?", "Confirm Shutdown", MessageBoxButtons.YesNo);
                if (dialogResult3 == DialogResult.Yes)
                {

                    timer1.Start();
                    lblCountDown.Text = counter.ToString();
                }
                else
                {
                    lblCountDown.Visible = false;
                    lblCount.Visible = false;
                    lblTermComp.Visible = false;
                    btnGo.Visible = true;
                }
            }
            else if (counter == 0)
            {
                timer1.Stop();
                lblCountDown.Visible = false;
                lblCount.Visible = false;
                lblTermComp.Visible = true;
                this.Refresh();
            }


            lblCountDown.Text = counter.ToString();
        }

    }
}
