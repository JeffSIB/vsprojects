﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Linq;
using System.Net;


namespace importBass
{


    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyNumber { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string UnderwriterEmail { get; set; }
            public string UnderwriterPhone { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string AgencyAgentEmail { get; set; }
            public string BrokerName { get; set; }
            public string BrokerPhone { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            //public string BusinessOperations { get; set; }
            public string LocationFullAddress { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string CoverageB { get; set; }
            public string CoverageC { get; set; }
            public string CoverageD { get; set; }
            public string CoverageE { get; set; }
            public string CoverageF { get; set; }
            public string ISOClass { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }

            public string ImageRightDrawer { get; set; }
            public string Coverage { get; set; }
            public string RatingClass { get; set; }

            public string InspectionLink { get; set; }
            public string LocationNumber { get; set; }
            public string LocationName { get; set; }
            public string SpecialInstructions { get; set; }


            public string Construction { get; set; }
            public string YearBuilt { get; set; }
            public string DwellingType { get; set; }
            public string NumberOfFloors { get; set; }
            public string Occupancy { get; set; }
            public string SquareFootage { get; set; }
            public string DistanceToCoast { get; set; }
            public string RoofYear { get; set; }
            public string RoofMaterials { get; set; }
            public string RoofGeometry { get; set; }
            public string Usage { get; set; }
            public string WindMitigation { get; set; }
            public string PCClass { get; set; }
            public string RoofCladding { get; set; }
            public string RoofCondition { get; set; }
            public string BuildingCladding { get; set; }
            public string CompanyPolicyForm { get; set; }
            public string CentralFireAlarm { get; set; }
            public string CentralBurglarAlarm { get; set; }
            public string CentralAlarm { get; set; }
            public string PolicyStatus { get; set; }
            public string PoolFenced { get; set; }
            public string Heating { get; set; }
            public string HeatingFullPartial { get; set; }
            public string HeatingUpdate { get; set; }
            public string HeatingYear { get; set; }
            public string PoolAboveGround { get; set; }
            public string Farming { get; set; }
            public string NumberOfFamilies { get; set; }
            public string Pets { get; set; }
            public string Plumbing { get; set; }
            public string PlumbingFullPartial { get; set; }
            public string PlumbingUpdate { get; set; }
            public string PlumbingYear { get; set; }
            public string PoolSlide { get; set; }
            public string PoolDivingBoard { get; set; }
            public string PoolScreenEncl { get; set; }
            public string Pool { get; set; }
            public string Updates { get; set; }
            public string Wiring { get; set; }
            public string WiringFullPartial { get; set; }
            public string WiringUpdate { get; set; }
            public string WiringYear { get; set; }
            public string Photos { get; set; }
            public string Account { get; set; }
            public string InspTypeKey { get; set; }
        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];
        static string cfg_NewRequestsURI = ConfigurationManager.AppSettings["NewRequestsURI"];


        static LogUtils.LogUtils oLU;
        static bool bErr = false;
        static int iNodes = 0;

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {

            string sXMLFileName = "";
            DateTime dToday = new DateTime();
            dToday = DateTime.Now;

            bool bImportSuccess = false;

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                //// load XML from Bass website
                //oLU.WritetoLog("Requesting XML from website");
                //HttpWebRequest request = WebRequest.Create(cfg_NewRequestsURI) as HttpWebRequest;
                //request.ContentType = "application/xml";
                //HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(response.GetResponseStream());

                //// build file name
                //sXMLFileName = "Bass" + dToday.Month.ToString() + dToday.Day.ToString() + dToday.Hour.ToString() + dToday.Minute.ToString() + dToday.Second.ToString() + ".xml";

                //// save file to tmp folder
                //xmlDoc.Save(cfg_sourcedir + sXMLFileName);

                //oLU.WritetoLog("Saved XML as: " + sXMLFileName);


                if (args.Length == 0)
                {
                    throw new ApplicationException("No arguments passed");
                }

                // get excel file name from command line
                sXMLFileName = args[0];

                // does file exist?
                FileInfo fi = new FileInfo(cfg_sourcedir + sXMLFileName);
                if (!fi.Exists)
                {
                    // send email
                    throw new ApplicationException("XML file does not exist: " + sXMLFileName);
                }

            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                oLU.closeLog();
                sendErrEmail("Error initializing importBass\r\n\r\n" + ex.Message);
                return;
            }

            iNodes = 0;

            try
            {


                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(cfg_sourcedir + sXMLFileName);

                XmlNodeList requests = xmlDoc.GetElementsByTagName("batch");

                // process each inspection in XML file
                foreach (XmlNode node in requests)
                {

                    ImportRecord IR = new ImportRecord();

                    XmlElement reqElem = (XmlElement)node;

                    iNodes++;

                    IR.InsuredName = reqElem.GetElementsByTagName("name")[0].InnerText;
                    IR.ContactName = reqElem.GetElementsByTagName("contact")[0].ChildNodes[0].InnerText;
                    IR.ContactPhoneWork = reqElem.GetElementsByTagName("contact")[0].ChildNodes[1].InnerText;
                    IR.RushHandling = reqElem.GetElementsByTagName("priorityLevel")[0].InnerText;

                    XmlNodeList policies = xmlDoc.GetElementsByTagName("policies");
                    foreach (XmlNode policy in policies)
                    {
                        IR.PolicyNumber = reqElem.GetElementsByTagName("number")[0].InnerText;
                        IR.Occupancy = reqElem.GetElementsByTagName("occupancy")[0].InnerText;
                        IR.Underwriter = reqElem.GetElementsByTagName("underwriter")[0].ChildNodes[0].InnerText;
                        IR.UnderwriterEmail = reqElem.GetElementsByTagName("underwriter")[0].ChildNodes[1].InnerText;
                        IR.AgencyAgentName = reqElem.GetElementsByTagName("agency")[0].ChildNodes[0].InnerText;
                        IR.AgencyAgentPhone = reqElem.GetElementsByTagName("agency")[0].ChildNodes[1].InnerText;
                        IR.AgencyAgentEmail = reqElem.GetElementsByTagName("agency")[0].ChildNodes[2].InnerText;
                        IR.InsuranceCompany = reqElem.GetElementsByTagName("carrier")[0].ChildNodes[0].InnerText;
                        IR.ImageRightDrawer = reqElem.GetElementsByTagName("index")[0].ChildNodes[0].InnerText;
                        IR.InspectionType = reqElem.GetElementsByTagName("inspectionTypeCode")[0].ChildNodes[0].InnerText;
                        IR.InspTypeKey = reqElem.GetElementsByTagName("inspectionType")[0].ChildNodes[0].InnerText;

                    }

                    XmlNodeList locations = xmlDoc.GetElementsByTagName("locations");
                    foreach (XmlNode location in locations)
                    {
                        IR.LocationAddress1 = reqElem.GetElementsByTagName("address")[0].ChildNodes[0].InnerText;
                        IR.LocationAddress2 = reqElem.GetElementsByTagName("address")[0].ChildNodes[1].InnerText;
                        IR.LocationCity = reqElem.GetElementsByTagName("address")[0].ChildNodes[2].InnerText;
                        IR.LocationState = reqElem.GetElementsByTagName("address")[0].ChildNodes[3].InnerText;
                        IR.LocationZip = reqElem.GetElementsByTagName("address")[0].ChildNodes[4].InnerText;
                    }

                    XmlNodeList coverages = xmlDoc.GetElementsByTagName("ratingClass");
                    for (int i = 0; i < coverages.Count; i++)
                    {
                        IR.Coverage += coverages[i].InnerText + " | ";
                    }

                    //XmlNodeList coverages = xmlDoc.GetElementsByTagName("coverages");
                    //foreach (XmlNode coverage in coverages)
                    //{
                    //    IR.Coverage = reqElem.GetElementsByTagName("coverage")[0].InnerText;
                    //    //IR.RatingClass = reqElem.GetElementsByTagName("coverage")[0].ChildNodes[0].InnerText;
                    //    //IR.Photos = reqElem.GetElementsByTagName("coverage")[0].ChildNodes[1].InnerText;
                    //}

                    XmlNodeList instructions = xmlDoc.GetElementsByTagName("instructions");
                    foreach (XmlNode coverage in instructions)
                    {
                        IR.SpecialInstructions = reqElem.GetElementsByTagName("specialinstructions")[0].InnerText;
                        IR.Comments = reqElem.GetElementsByTagName("comments")[0].InnerText + "\r\n";
                    }

                    ImportRequests oAPI = new ImportRequests();
                    oAPI.CustomerUserName = "APIProd";
                    oAPI.CustomerPassword = "Sutton2012";
                    //oAPI.CustomerAccount = "7192";

                    // trim IRDrawer to max of 4 characters
                    if (IR.ImageRightDrawer.Length > 4)
                    {
                        IR.ImageRightDrawer = IR.ImageRightDrawer.Substring(0, 4);
                    }

                    // set customer account based on IR Drawer
                    if (IR.ImageRightDrawer == "USTP")
                        oAPI.CustomerAccount = "7192";
                    else if (IR.ImageRightDrawer == "UGLF")
                        oAPI.CustomerAccount = "7372";
                    else if (IR.ImageRightDrawer == "UORL")
                        oAPI.CustomerAccount = "7341";
                    else if (IR.ImageRightDrawer == "UFTL")
                        oAPI.CustomerAccount = "7030";
                    else if (IR.ImageRightDrawer == "UATL")
                        oAPI.CustomerAccount = "7370";
                    else if (IR.ImageRightDrawer == "UGAI")
                        oAPI.CustomerAccount = "7342";
                    else if (IR.ImageRightDrawer == "UHOU")
                        oAPI.CustomerAccount = "7371";
                    else if (IR.ImageRightDrawer == "ULVN")
                        oAPI.CustomerAccount = "7377";
                    else if (IR.ImageRightDrawer == "USAC")
                        oAPI.CustomerAccount = "7378";
                    else if (IR.ImageRightDrawer == "UNOL")
                        oAPI.CustomerAccount = "7376";
                    else if (IR.ImageRightDrawer == "UNYL")
                        oAPI.CustomerAccount = "7380";
                    else if (IR.ImageRightDrawer == "USNJ")
                        oAPI.CustomerAccount = "7382";
                    else if (IR.ImageRightDrawer == "UCHS")
                        oAPI.CustomerAccount = "7383";
                    else if (IR.ImageRightDrawer == "UNYC")
                        oAPI.CustomerAccount = "7387";
                    else if (IR.ImageRightDrawer == "USDG")
                        oAPI.CustomerAccount = "7391";
                    else if (IR.ImageRightDrawer == "ULAX")
                        oAPI.CustomerAccount = "7419";
                    else if (IR.ImageRightDrawer == "UNPL")
                        oAPI.CustomerAccount = "7428";
                    else if (IR.ImageRightDrawer == "URVA")
                        oAPI.CustomerAccount = "7436";
                    else
                        throw new Exception("Unable to determine account: " + IR.ImageRightDrawer);

                    // standard values provided
                    oAPI.InsuredName = IR.InsuredName;
                    oAPI.LocationContactName = IR.ContactName;
                    oAPI.ContactPhoneWork = IR.ContactPhoneWork;
                    oAPI.ContactPhoneCell = "";
                    oAPI.ContactPhoneHome = "";
                    oAPI.PolicyNumber = IR.PolicyNumber;
                    //oAPI.EffectiveDate = IR.EffectiveDate;
                    oAPI.CoverageA = "";
                    oAPI.Occupancy = IR.Occupancy;
                    oAPI.LocationAddress1 = IR.LocationAddress1;
                    oAPI.LocationAddress2 = IR.LocationAddress2;
                    oAPI.LocationCity = IR.LocationCity;
                    oAPI.LocationState = IR.LocationState;
                    oAPI.LocationZip = IR.LocationZip;
                    oAPI.MailAddress1 = IR.LocationAddress1;
                    oAPI.MailAddress2 = IR.LocationAddress2;
                    oAPI.MailCity = IR.LocationCity;
                    oAPI.MailState = IR.LocationState;
                    oAPI.MailZip = IR.LocationZip;
                    oAPI.InsuranceCompany = IR.InsuranceCompany;
                    oAPI.AgencyAgentName = IR.AgencyAgentName;
                    oAPI.AgencyAgentPhone = IR.AgencyAgentPhone;

                    oAPI.AgentAddress1 = "5656 Central Ave";
                    oAPI.AgentAddress2 = "";
                    oAPI.AgentCity = "St. Petersburg";
                    oAPI.AgentState = "FL";
                    oAPI.AgentZip = "33707";
                    //oAPI.AgentEmail = "CustomerServices@sibfla.com";


                    oAPI.Producer = "";
                    oAPI.Underwriter = IR.Underwriter;
                    oAPI.UnderwriterFirstName = "";
                    oAPI.UnderwriterLastName = "";
                    oAPI.UnderwriterPhone = IR.UnderwriterPhone;
                    oAPI.UnderwriterCorrEmail = IR.UnderwriterEmail;
                    oAPI.ImageRightDrawer = IR.ImageRightDrawer;

                    // standard values not provided
                    oAPI.EmailConfirmation = "";

                    if (IR.RushHandling.ToUpper() == "RUSH")
                        oAPI.RushHandling = "Y";
                    else
                        oAPI.RushHandling = "N";

                    oAPI.ContactName = "";

                    // Convert CoverageA to numeric
                    //double dCovA = Convert.ToDouble(row.CoverageA);

                    IR.Comments += "Agency email: " + IR.AgencyAgentEmail + "\r\n" + "Coverage: " + IR.Coverage + "\r\n" + "Rating class: " + IR.RatingClass + "\r\n";

                    // inspection type
                    string sInspType = IR.InspectionType.ToUpper();
                    string sInspTypeKey = IR.InspTypeKey.ToUpper();

                    if (sInspType.Length < 1)
                    {
                        throw new Exception("No Inspection Type - File name: " + sXMLFileName);
                    }

                    if (sInspTypeKey == "PHONE")
                    {
                        sInspType = oAPI.CustomerAccount + "-TC";
                    }
                    else
                    {
                        sInspType = oAPI.CustomerAccount + "-" + sInspType;
                    }

                    oAPI.InspectionType = sInspType;


                    //bool bHO = false;
                    //bool bVal = false;
                    //bool bPhoto = false;

                    //IR.Coverage = IR.Coverage.ToUpper();

                    //if (IR.Coverage.Contains("HOMEOWNER"))
                    //        bHO = true;

                    //if (IR.Coverage.Contains("VALUATION"))
                    //    bVal = true;

                    //if (IR.Coverage.Contains("PHOTO"))
                    //    bPhoto = true;

                    //if (bHO && bVal && bPhoto)
                    //    sInspType = oAPI.CustomerAccount + "-R-IE-MSB-U";
                    //else if (bHO && bVal)
                    //    sInspType = oAPI.CustomerAccount + "-R-E-MSB-U";
                    //else
                    //{
                    //    sInspType = oAPI.CustomerAccount + "-R-E-MSB-U";
                    //    IR.Comments = "**** UNABLE TO DETERMINE CORRECT INSPECTION TYPE ****\r\n\r\n" + IR.Comments;
                    //}


                    // comments
                    oAPI.Comments = IR.Comments + "\r\n" + IR.SpecialInstructions;

                    oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + IR.PolicyNumber);
                    sbEmail.Append("Importing Policy# " + IR.PolicyNumber + System.Environment.NewLine);

                    string sRet = oAPI.ImportBass();

                    oLU.WritetoLog("oAPI.Import return for for Policy# " + IR.PolicyNumber + "\r\n\r\n" + sRet);

                    var importResults = sRet.FromJSON<List<ImportResult>>();

                    foreach (var importResult in importResults)
                    {

                        if (importResult.Successful)
                        {
                            oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                            sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + System.Environment.NewLine);
                        }
                        else
                        {
                            oLU.WritetoLog("**** Import failed **** ");
                            sbEmail.Append("**** Import failed ****" + System.Environment.NewLine);

                            if (importResult.Errors != null)
                            {
                                foreach (var error in importResult.Errors)
                                {
                                    oLU.WritetoLog("Error: " + error.ErrorText);
                                    sbEmail.Append("Error: " + error.ErrorText + System.Environment.NewLine);
                                }
                            }
                        }
                    }

                }   // foreach node

                bImportSuccess = true;


            }   //try

            catch (Exception ex)
            {
                bImportSuccess = false;
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail("Exception logged" + System.Environment.NewLine + ex.Message);
            }

            finally
            {


                // Copy source file to holding area and delete temp
                string sSourceName = cfg_sourcedir + sXMLFileName;
                string sDestName = "";

                //if successful - copy to sibidata\Bass\Processed
                //if failed - copy to sibidata\Bass\fail
                if (bImportSuccess)
                {
                    sDestName = cfg_compdir + sXMLFileName;
                }
                else
                {
                    sDestName = cfg_faildir + sXMLFileName;
                }

                // only copy if data present
                if (iNodes == 0)
                {
                    oLU.WritetoLog("Nothing to import");
                }
                else
                {

                    sendLogEmail(sbEmail.ToString());

                    File.Copy(sSourceName, sDestName);

                    if (!File.Exists(sDestName))
                    {
                        throw new ApplicationException("Copy failed for: " + sXMLFileName);
                    }
                }

                File.Delete(sSourceName);

                oLU.closeLog();
            }

        }

        /// <summary>
        /// createKey360
        /// </summary>
        /// <returns></returns>
        //public string createKey360()
        //{

        //    string sXMLFile = msXMLFilePath + msXMLFile;
        //    msReturnStr = "";
        //    string sErrText = "";

        //    XmlTextReader reader = null;

        //    try
        //    {

        //        // verify xml file exists
        //        if (!File.Exists(sXMLFile))
        //        {
        //            throw new ApplicationException("File does not exist: " + sXMLFile);
        //        }



        //        // Load the reader with the data file and ignore all white space nodes.         
        //        reader = new XmlTextReader(sXMLFile);
        //        reader.WhitespaceHandling = WhitespaceHandling.None;
        //        XmlReaderSettings readersettings = new XmlReaderSettings();
        //        readersettings.CheckCharacters = false;

        //        // Parse the file and store each of the elements in a structure.
        //        while (reader.Read())
        //        {

        //            if (reader.MoveToContent() == XmlNodeType.Element && reader.Name != "Request")
        //            {

        //                switch (reader.Name)
        //                {
        //                    case "CustomerAccount":
        //                        sKeyData.sMasterAcnt = reader.ReadString();
        //                        break;
        //                    case "InspectionCategory":
        //                        sKeyData.sRequestType = reader.ReadString();
        //                        break;
        //                    case "RequestedBy":
        //                        sKeyData.sRequestedBy = reader.ReadString();
        //                        break;
        //                    case "EmailConfirmation":
        //                        sKeyData.sEmailConfirmation = reader.ReadString();
        //                        break;
        //                    case "AgencyAgentName":
        //                        sKeyData.sAgencyAgentName = reader.ReadString();
        //                        break;
        //                    case "AgencyAgentCode":
        //                        sKeyData.sAgentCode = reader.ReadString();
        //                        break;
        //                    case "AgencyAgentPhone":
        //                        sKeyData.sAgencyAgentPhone = reader.ReadString();
        //                        break;
        //                    case "AgencyAgentFax":
        //                        sKeyData.sAgentFax = reader.ReadString();
        //                        break;
        //                    case "AgencyAgentContactName":
        //                        sKeyData.sAgencyAgentContactName = reader.ReadString();
        //                        break;
        //                    case "AgencyAgentAddress1":
        //                        sKeyData.sAgentAddress1 = reader.ReadString();
        //                        break;
        //                    case "AgencyAgentAddress2":
        //                        sKeyData.sAgentAddress2 = reader.ReadString();
        //                        break;
        //                    case "AgencyAgentCity":
        //                        sKeyData.sAgentCity = reader.ReadString();
        //                        break;
        //                    case "AgencyAgentState":
        //                        sKeyData.sAgentState = reader.ReadString();
        //                        break;
        //                    case "AgencyAgentZip":
        //                        sKeyData.sAgentZip = stripLastFour(reader.ReadString());
        //                        break;
        //                    case "PolicyNumber":
        //                        sKeyData.sPolicyNumber = reader.ReadString();
        //                        break;
        //                    case "InsuranceCompany":
        //                        sKeyData.sInsuranceCo = reader.ReadString();
        //                        break;
        //                    case "Producer":
        //                        sKeyData.sProducer = reader.ReadString();
        //                        break;
        //                    case "RushHandling":
        //                        sKeyData.sRushHandling = reader.ReadString();
        //                        break;
        //                    case "InsuredName":
        //                        sKeyData.sInsuredName = reader.ReadString();
        //                        break;
        //                    case "MailAddress1":
        //                        sKeyData.sMailAddress1 = reader.ReadString();
        //                        break;
        //                    case "MailAddress2":
        //                        sKeyData.sMailAddress2 = reader.ReadString();
        //                        break;
        //                    case "MailCity":
        //                        sKeyData.sMailCity = reader.ReadString();
        //                        break;
        //                    case "MailState":
        //                        sKeyData.sMailState = reader.ReadString();
        //                        break;
        //                    case "MailZip":
        //                        sKeyData.sMailZip = stripLastFour(reader.ReadString());
        //                        break;
        //                    case "ContactName":
        //                        sKeyData.sContactName = reader.ReadString();
        //                        break;
        //                    case "ContactPhoneHome":
        //                        sKeyData.sContactPhoneHome = reader.ReadString();
        //                        break;
        //                    case "ContactPhoneWork":
        //                        sKeyData.sContactPhoneWork = reader.ReadString();
        //                        break;
        //                    case "ContactPhoneCell":
        //                        sKeyData.sContactPhoneCell = reader.ReadString();
        //                        break;
        //                    case "BusinessOperations":
        //                        sKeyData.sBusinessOperations = reader.ReadString();
        //                        break;
        //                    case "LocationAddress1":
        //                        sKeyData.sLocationAddress1 = reader.ReadString();
        //                        break;
        //                    case "LocationAddress2":
        //                        sKeyData.sLocationAddress2 = reader.ReadString();
        //                        break;
        //                    case "LocationCity":
        //                        sKeyData.sLocationCity = reader.ReadString();
        //                        break;
        //                    case "LocationState":
        //                        sKeyData.sLocationState = reader.ReadString();
        //                        break;
        //                    case "LocationZip":
        //                        sKeyData.sLocationZip = stripLastFour(reader.ReadString());
        //                        break;
        //                    case "LocationContactName":
        //                        sKeyData.sLocationContactName = reader.ReadString();
        //                        break;
        //                    case "LocationContactPhone":
        //                        sKeyData.sLocationContactPhone = reader.ReadString();
        //                        break;
        //                    case "Comments":
        //                        sKeyData.sComments = reader.ReadString();
        //                        break;
        //                    case "PhotoCode":
        //                        sKeyData.sPhotoCode = reader.ReadString();
        //                        break;
        //                    case "PhotoOther":
        //                        sKeyData.sPhotoOther = reader.ReadString();
        //                        break;
        //                    case "InspectionType":
        //                        sKeyData.sInspectionType = reader.ReadString();
        //                        break;
        //                    case "EffectiveDate":
        //                        sKeyData.sEffectiveDate = reader.ReadString();
        //                        break;
        //                    case "Underwriter":
        //                        sKeyData.sUnderwriter = reader.ReadString();
        //                        break;
        //                    case "UnderwriterFirstName":
        //                        sKeyData.sUnderwriterFirst = reader.ReadString();
        //                        break;
        //                    case "UnderwriterLastName":
        //                        sKeyData.sUnderwriterLast = reader.ReadString();
        //                        break;
        //                    case "UnderwriterPhone":
        //                        sKeyData.sUnderwriterPhone = reader.ReadString();
        //                        break;
        //                    case "UnderwriterContactEmail":
        //                        sKeyData.sUnderwriterCorrEmail = reader.ReadString();
        //                        break;
        //                    case "UnderwriterReportEmail":
        //                        sKeyData.sUnderwriterRptEmail = reader.ReadString();
        //                        break;
        //                    case "BuildingCost":
        //                        sKeyData.sBuildingCost = reader.ReadString();
        //                        break;
        //                    case "BuildingCosts":
        //                        sKeyData.sBuildingCost = reader.ReadString();
        //                        break;
        //                    case "BusinessTotalRevenue":
        //                        sKeyData.sBusinessTotalRevenue = reader.ReadString();
        //                        break;
        //                    case "ContentsCost":
        //                        sKeyData.sContentsCost = reader.ReadString();
        //                        break;
        //                    case "CoverageA":
        //                        sKeyData.sCoverageA = reader.ReadString();
        //                        break;
        //                    case "ISOClass":
        //                        sKeyData.sISOClass = reader.ReadString();
        //                        break;
        //                    case "Occupancy":
        //                        sKeyData.sOccupancy = reader.ReadString();
        //                        break;
        //                    case "YearBuilt":
        //                        sKeyData.sYearBuilt = reader.ReadString();
        //                        break;
        //                    case "GenericField1Name":
        //                        sKeyData.sGenericFieldName = reader.ReadString();
        //                        break;
        //                    case "GenericField1Value":
        //                        sKeyData.sGenericFieldValue = reader.ReadString();
        //                        break;
        //                    case "GenericFieldName2":
        //                        sKeyData.sGenericFieldName2 = reader.ReadString();
        //                        break;
        //                    case "GenericFieldValue2":
        //                        sKeyData.sGenericFieldValue2 = reader.ReadString();
        //                        break;
        //                    case "GenericFieldName3":
        //                        sKeyData.sGenericFieldName3 = reader.ReadString();
        //                        break;
        //                    case "GenericFieldValue3":
        //                        sKeyData.sGenericFieldValue3 = reader.ReadString();
        //                        break;
        //                }   // switch

        //            }   //if (reader.MoveToContent()

        //        }   //while

        //        // Validate required fields
        //        string sValidation = validateXML();

        //        // Check inspection type
        //        string sITRet = parseInspectionType(sKeyData.sMasterAcnt, sKeyData.sInspectionType);

        //        // Return exception if necessary
        //        if (sValidation.Length > 0 || sITRet.Length == 0)
        //        {
        //            if (sValidation.Length > 0)
        //            {
        //                msReturnStr = buildResponse(msUserID, "Exception", sValidation);
        //            }
        //            if (sITRet.Length == 0)
        //            {
        //                msReturnStr = buildResponse(msUserID, "Exception", "Invalid inspection type: " + sKeyData.sInspectionType);
        //            }
        //        }
        //        else    // create and return key
        //        {
        //            //**************************************************************

        //            string sKey = "";

        //            ImportRequests oAPI = new ImportRequests();
        //            oAPI.CustomerUserName = "APIProd";
        //            oAPI.CustomerPassword = "Sutton2012";
        //            oAPI.CustomerAccount = sKeyData.sMasterAcnt;

        //            // Underwriter
        //            // If underwriter first and last name are blank and Underwriter has value, try to split it on space
        //            if (sKeyData.sUnderwriterFirst.Length == 0 && sKeyData.sUnderwriterLast.Length == 0)
        //            {

        //                if (sKeyData.sUnderwriter.Length > 0)
        //                {
        //                    if (sKeyData.sUnderwriter.Contains(" "))
        //                    {
        //                        int iSpace = sKeyData.sUnderwriter.IndexOf(" ");
        //                        sKeyData.sUnderwriterFirst = sKeyData.sUnderwriter.Substring(0, iSpace);
        //                        sKeyData.sUnderwriterLast = sKeyData.sUnderwriter.Substring(iSpace, sKeyData.sUnderwriter.Length - iSpace);
        //                    }
        //                }
        //            }

        //            // Set inspection type
        //            oAPI.InspectionType = sITRet;
        //            sKeyData.sInspectionType = sITRet;

        //            // Load values into API
        //            oAPI.EmailConfirmation = "";
        //            oAPI.RequestedBy = sKeyData.sRequestedBy;
        //            oAPI.PolicyNumber = sKeyData.sPolicyNumber;
        //            oAPI.EffectiveDate = sKeyData.sEffectiveDate;

        //            oAPI.Underwriter = sKeyData.sUnderwriter;
        //            oAPI.UnderwriterFirstName = sKeyData.sUnderwriterFirst;
        //            oAPI.UnderwriterLastName = sKeyData.sUnderwriterLast;
        //            oAPI.UnderwriterCorrEmail = sKeyData.sUnderwriterCorrEmail;
        //            oAPI.UnderwriterPhone = sKeyData.sUnderwriterPhone;
        //            oAPI.UnderwriterRptEmail = sKeyData.sUnderwriterRptEmail;

        //            oAPI.AgencyAgentName = sKeyData.sAgencyAgentName;
        //            oAPI.AgentCode = sKeyData.sAgentCode;
        //            oAPI.AgencyAgentPhone = sKeyData.sAgencyAgentPhone;
        //            oAPI.AgentFax = sKeyData.sAgentFax;
        //            oAPI.AgencyAgentContact = sKeyData.sAgencyAgentContactName.Length > 0 ? sKeyData.sAgencyAgentContactName : sKeyData.sProducer;
        //            oAPI.AgentAddress1 = sKeyData.sAgentAddress1;
        //            oAPI.AgentAddress2 = sKeyData.sAgentAddress2;
        //            oAPI.AgentCity = sKeyData.sAgentCity;
        //            oAPI.AgentState = sKeyData.sAgentState;
        //            oAPI.AgentZip = sKeyData.sAgentZip;
        //            oAPI.AgentCode = sKeyData.sAgentCode;

        //            oAPI.InsuranceCompany = sKeyData.sInsuranceCo;
        //            oAPI.Producer = sKeyData.sProducer;
        //            oAPI.RushHandling = sKeyData.sRushHandling;
        //            oAPI.InsuredName = sKeyData.sInsuredName;
        //            oAPI.ContactName = sKeyData.sContactName;
        //            oAPI.ContactPhoneHome = sKeyData.sContactPhoneHome;
        //            oAPI.ContactPhoneWork = sKeyData.sContactPhoneWork;
        //            oAPI.ContactPhoneCell = sKeyData.sContactPhoneCell;
        //            oAPI.MailAddress1 = sKeyData.sMailAddress1;
        //            oAPI.MailAddress2 = sKeyData.sMailAddress2;
        //            oAPI.MailCity = sKeyData.sMailCity;
        //            oAPI.MailState = sKeyData.sMailState;
        //            oAPI.MailZip = sKeyData.sMailZip;
        //            oAPI.LocationAddress1 = sKeyData.sLocationAddress1;
        //            oAPI.LocationAddress2 = sKeyData.sLocationAddress2;
        //            oAPI.LocationCity = sKeyData.sLocationCity;
        //            oAPI.LocationState = sKeyData.sLocationState;
        //            oAPI.LocationZip = sKeyData.sLocationZip;
        //            oAPI.LocationContactName = sKeyData.sLocationContactName;
        //            oAPI.LocationContactPhone = sKeyData.sLocationContactPhone;
        //            oAPI.CoverageA = sKeyData.sCoverageA;
        //            oAPI.Comments = sKeyData.sComments;
        //            oAPI.GenericField1Name = sKeyData.sGenericFieldName;
        //            oAPI.GenericField1Value = sKeyData.sGenericFieldValue;
        //            oAPI.BuildingCost = sKeyData.sBuildingCost;
        //            oAPI.Occupancy = sKeyData.sOccupancy;
        //            oAPI.YearBuilt = sKeyData.sYearBuilt;


        //            //**********************************
        //            // UNCOMMENT FOR TEST 
        //            //oAPI.CustomerUserName = "APITest";
        //            //oAPI.CustomerAccount = "9998";
        //            //oAPI.InspectionType = "9998RE";

        //            //oAPI.AgentAddress1 = "Agent address 1";
        //            //oAPI.AgentAddress2 = "Agent address 2";
        //            //oAPI.AgentCity = "Agent city";
        //            //oAPI.AgentState = "FL";
        //            //oAPI.AgentZip = "33601";
        //            //oAPI.AgentCode = "AGNT1";
        //            //oAPI.AgentFax = "222-222-2222";

        //            //oAPI.UnderwriterCorrEmail = "uwcorremail@sibfla.com";
        //            //oAPI.UnderwriterRptEmail = "uwreportemailsibfla.com";
        //            //oAPI.UnderwriterPhone = "999-999-9999";

        //            //**********************************

        //            // Call import method
        //            string sRet = oAPI.Import();

        //            // Process return
        //            var importResults = sRet.FromJSON<List<ImportResult>>();

        //            foreach (var importResult in importResults)
        //            {

        //                if (importResult.Successful)
        //                {

        //                    sKey = importResult.CaseNumber.ToString();
        //                }
        //                else
        //                {
        //                    if (importResult.Errors != null)
        //                        foreach (var error in importResult.Errors)
        //                        {
        //                            sErrText += error.ErrorText + System.Environment.NewLine;
        //                        }
        //                }

        //                if ((bool)importResult.Duplicate)
        //                {

        //                    sErrText += "Duplicate case: " + System.Environment.NewLine;

        //                }
        //            }


        //            // key created
        //            if (sKey.Length > 0)
        //            {
        //                msReturnStr = buildResponse(msUserID, "Key", sKey);

        //                // send email conf if necessary
        //                if (sKeyData.sEmailConfirmation.Length > 0)
        //                {
        //                    sendEmailConf(sKey, sKeyData);
        //                }

        //            }
        //            else
        //            {
        //                msReturnStr = buildResponse(msUserID, "Exception", "Request creation failed");
        //                sErrText += "sKey.Length = 0" + System.Environment.NewLine;
        //            }
        //        }

        //    }   //try

        //    catch (Exception ex)
        //    {
        //        if (sErrText.Length > 0)
        //        {
        //            throw new ApplicationException("Error processing request: " + sErrText);
        //        }
        //        else
        //        {
        //            throw new ApplicationException("INTERNAL [createKey]: " + ex.Message);
        //        }
        //    }

        //    finally
        //    {
        //        //if (sErrText.Length > 0)
        //        //{
        //        //    sendErrEmail(sErrText);
        //        //}


        //        if (reader != null)
        //            reader.Close();
        //    }

        //    return msReturnStr;
        //}


        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import Bass Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import Bass Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static XmlDocument MakeRequest(string requestUrl)
        {
            try
            {

                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                request.ContentType = "application/xml";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(response.GetResponseStream());
                return (xmlDoc);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                Console.Read();
                return null;
            }
        }
    }
}
