﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Word = Microsoft.Office.Interop.Word;
using WinSCP;


namespace SCUIDelivery
{
    class Program
    {

        static void Main(string[] args)
        {

            Setup setup = new Setup();

        }


        /// <summary>
        /// Instantiate properties from the AppSettings located in the App.config.
        /// </summary>
        public class Setup
        {

            private string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
            private string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
            private string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            private string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
            private string cfg_recroot = ConfigurationManager.AppSettings["RecRoot"];
            private string cfg_FTPRoot = ConfigurationManager.AppSettings["FTPRoot"];
            private string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
            private string cfg_exportapp = ConfigurationManager.AppSettings["ExportApp"];
            private string cfg_recapp = ConfigurationManager.AppSettings["RecApp"];
            private string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];

            private LogUtils.LogUtils oLU;
            private bool bErr = false;
            private string sPolicy = "";
            private string sAddLine1 = "";
            private string sInsured = "";
            private string sCaseNum = "";
            private Guid guCaseID;
            private string sIRFileNum = "";
            private string sIRUserID = "";
            private string sCustNum = "";
            private string sCustName = "";
            private string sAmount = "";
            private string sOutputFileName = "";
            private string sDeliveryDate = "";
            private string sBatchNum = "";

            //Used for email message body.  
            private StringBuilder sbEmail = new StringBuilder();

            //Used for transmittal message body.  
            private StringBuilder sbmsgBody = new StringBuilder();

            public Setup()
            {
                int iCasesExported = 0;

                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                bool bExport = true;
                bool bCreateRecs = true;
                bool bFTPFiles = true;

                // build list of accounts that if ordered by, set IRUserID to "IR_Offshore"
                List<string> lOffshore = new List<string>();
                lOffshore.Add("DCANTEROS");
                lOffshore.Add("RCAJONTOY");
                lOffshore.Add("RMARFIL");
                lOffshore.Add("EALVAREZ");
                lOffshore.Add("DLAYGO");
                lOffshore.Add("KDELEON");
                lOffshore.Add("KMARTINEZ");
                lOffshore.Add("CBARBADO");
                lOffshore.Add("MFERRER");
                lOffshore.Add("EESTILLORE");
                lOffshore.Add("KAVENA");
                lOffshore.Add("MFUGIAO");
                lOffshore.Add("BCALUPIG");
                lOffshore.Add("BBANATE");
                lOffshore.Add("GNAVARRO");
                lOffshore.Add("JMAGLAYA");
                lOffshore.Add("DSAQUISAME");
                lOffshore.Add("MMALOMA");
                lOffshore.Add("MTANZUACO");
                lOffshore.Add("ODONILA");
                lOffshore.Add("JCABITON");
                lOffshore.Add("DIGNACIO");
                lOffshore.Add("DDAZA");

                DirectoryInfo diPDF;
                DirectoryInfo diRec;
                DirectoryInfo diCaseFiles;

                FileInfo[] fiPDFFiles;
                FileInfo[] fiRECFiles;
                FileInfo fiRec;

                oLU.WritetoLog("Begin Processing  " + DateTime.Now.ToString());
                sbEmail.Append("Begin Processing  " + DateTime.Now.ToString() + "\r\n\r\n");

                //***********************************************************
                // Export PDF's from 360
                if (bExport)
                {
                    try
                    {

                        // Delete all files in PDF Export directory.
                        string[] sFiles = Directory.GetFiles(cfg_pdfroot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);

                        // Export PDF's from 360
                        oLU.WritetoLog(" Begin exporting completed cases from 360");
                        sbEmail.Append("---- Begin exporting completed cases from 360\r\n\r\n");
                        oLU.closeLog();

                        oLU.OpenLog();
                        oLU.WritetoLog(cfg_exportapp);

                        var proc = Process.Start(cfg_exportapp);
                        proc.WaitForExit();
                        var exitCode = proc.ExitCode;
                        sbEmail.Append("Exporter returned: " + exitCode.ToString() + "\r\n");

                        // exit code should be the number of cases exported
                        iCasesExported = (int)exitCode;

                        Console.WriteLine(exitCode.ToString());

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR IN EXPORT:\r\n" + ex.Message);
                        oLU.WritetoLog("**** ERROR IN EXPORT:\r\n" + ex.Message);
                        bErr = true;
                        oLU.closeLog();
                        oLU.OpenLog();
                    }

                    diPDF = new DirectoryInfo(cfg_pdfroot);
                    fiPDFFiles = diPDF.GetFiles("*.pdf");

                    if (fiPDFFiles.Count() != iCasesExported)
                    {
                        sbEmail.Append("**** PDF count does not match number exported ****\r\n");
                        sbEmail.Append("Exported: " + iCasesExported.ToString() + " - In folder: " + fiPDFFiles.Count().ToString() + "\r\n\r\n");
                    }

                }

                if (bCreateRecs && !bErr)
                {

                    //***********************************************************
                    // Build Rec docs for all PDF files that were extracted


                    //************************************************
                    // Test for connection to casefiles
                    //************************************************
                    try
                    {
                        diCaseFiles = new DirectoryInfo(cfg_casefilesroot);
                        if (!diCaseFiles.Exists)
                            throw new ApplicationException("Unable to access case files root: " + cfg_casefilesroot + "\r\n");

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR ACCESSING CASEFILES FOLDER:\r\n" + ex.Message);
                        oLU.WritetoLog("**** ERROR ACCESSING CASEFILES FOLDER:\r\n" + ex.Message);
                        bErr = true;
                    }

                    if (!bErr)
                    {

                        // Delete all files in Rec folder.
                        string[] sFiles = Directory.GetFiles(cfg_recroot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);

                        sbEmail.Append("\r\n\r\n---- Begin creating REC Docs\r\n");

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        foreach (FileInfo file in fiPDFFiles)
                        {

                            sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));

                            ProcessStartInfo psi = new ProcessStartInfo();
                            psi.FileName = cfg_recapp;
                            psi.Arguments = sCaseNum;

                            var proc1 = Process.Start(psi);
                            proc1.WaitForExit();
                            var exitCode1 = proc1.ExitCode;
                            Console.WriteLine(exitCode1.ToString());

                            // verify rec doc was created
                            fiRec = new FileInfo(cfg_recroot + sCaseNum + ".docx");
                            if (fiRec.Exists)
                            {
                                // success
                                sbEmail.Append("Rec doc created for: " + sCaseNum + "\r\n");
                            }
                            else
                            {
                                // fail
                                sbEmail.Append("No rec doc for : " + sCaseNum + "\r\n");
                            }
                            fiRec = null;
                        }
                    }
                }

                //***********************************************************
                // rename PDF's & Recs to ImageRight file spec
                // **** REMOVE create transmittal email and send if FTP is successful
                // copy to FTP folder
                if (bFTPFiles && !bErr)
                {

                    // Delete all files in FTP folder.
                    string[] sFiles = Directory.GetFiles(cfg_FTPRoot);
                    foreach (string sFile in sFiles)
                        File.Delete(sFile);

                    sbEmail.Append("\r\n\r\n---- Begin renaming reports\r\n");

                    int iUnique = 1;

                    // Create email header
                    DateTime dNow = DateTime.Today;
                    sDeliveryDate = dNow.ToShortDateString();
                    sBatchNum = dNow.Year.ToString() + dNow.Month.ToString() + dNow.Day.ToString();
                    buildHeader();

                    ///////////////////////////////////
                    // PDF's
                    ///////////////////////////////////
                    try
                    {

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        // for each PDF 
                        foreach (FileInfo pdfFile in fiPDFFiles)
                        {

                            // Get case # from file name
                            sCaseNum = pdfFile.Name.Substring(0, pdfFile.Name.IndexOf("."));
                            sbEmail.Append("Case #: " + sCaseNum + "\r\n");

                            // Get CaseID & customer number
                            if (!GetCaseInfo(sCaseNum))
                            {
                                sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("GetInfo failed for case: " + sCaseNum);
                            }

                            if (sIRFileNum == "")
                            {
                                sIRFileNum = "99999999";
                            }

                            sbEmail.Append("    Original sIRUSerid: " + sIRUserID + "\r\n");


                            // If sIRUserID (left of person ordering email) is in list, change to "IR_Offshore"
                            if (lOffshore.IndexOf(sIRUserID.ToUpper()) != -1)
                            {
                                sIRUserID = "IR_Offshore";
                            }

                            sbEmail.Append("    Translated sIRUSerid: " + sIRUserID + "\r\n");

                            // replace any illegal characters in address & file #  with "-"
                            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                            {
                                sAddLine1 = sAddLine1.Replace(c, '-');
                                sIRFileNum = sIRFileNum.Replace(c, '-');
                            }


                            // Create email line item
                            buildLineItem(sPolicy,sIRFileNum,sInsured,sCaseNum,sAmount);

                            // Build Report file name
                            //sOutputFileName = sIRDrawer & "_" & sIRFile & "_" & msIRFolderType & "_" & msIRDocType & "__" & "_______" & msIRPageDesc & "_" & msIRFlowID & "_" & msIRStepID & "_" & sIRUID & "_" & msIRPriority & "_" & msIRTaskDesc & "__" & msIRConvFlag & "_" & msIRFileType & "_" & iUnique.ToString & ".pdf
                            if (sCustNum == "7062")   //TAMPA - COMMERCIAL
                            {
                                //sOutputFileName = cfg_FTPRoot + "SCTP_" + sIRFileNum + "_" + "Policy Information 0" + "_" + "INSP" + "__" + "_______" + "Insp Rcvd" + "_" + "612" + "_" + "614" + "_" + "CHDAVIS" + "_" + "5" + "_" + "Inspection Received" + "__" + "C" + "_" + "SCTP" + "_";
                                //sOutputFileName = cfg_FTPRoot + @"SCU Tampa_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + "CHDAVIS" + "_@_";

                                if (sIRUserID != "IR_Offshore")
                                    sIRUserID = "CHDAVIS";

                                sOutputFileName = cfg_FTPRoot + @"SCU Tampa_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + sIRUserID + "_@_";

                            }
                            else if (sCustNum == "7213")    //TAMPA - GARAGE
                            {
                                //sOutputFileName = cfg_FTPRoot + "SCTP_" + sIRFileNum + "_" + "Policy Information 0" + "_" + "INSP" + "__" + "_______" + "Insp Rcvd" + "_" + "612" + "_" + "614" + "_" + "LBLAIR" + "_" + "5" + "_" + "Inspection Received" + "__" + "C" + "_" + "SCTP" + "_";
                                //sOutputFileName = cfg_FTPRoot + @"SCU Tampa_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + "LBLAIR" + "_@_";

                                if (sIRUserID != "IR_Offshore")
                                    sIRUserID = "LBLAIR";

                                sOutputFileName = cfg_FTPRoot + @"SCU Tampa_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + sIRUserID + "_@_";

                            }

                            // If it exists - add digit to end
                            if (File.Exists(sOutputFileName + iUnique.ToString() + ".pdf"))
                            {

                                iUnique = 2;
                                while (true)
                                {

                                    if (File.Exists(sOutputFileName + iUnique.ToString() + ".pdf"))
                                    {
                                        iUnique++;
                                        if (iUnique > 10)
                                        {
                                            throw new ApplicationException(sOutputFileName + iUnique.ToString() + ".pdf" + " could not be saved - exists.");
                                        }

                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }

                            // Copy PDF to FTP folder under new name
                            pdfFile.CopyTo(sOutputFileName + iUnique.ToString() + ".pdf");
                            sbEmail.Append("    PDF Copied: " + sOutputFileName + iUnique.ToString() + ".pdf" + "\r\n");

                        }

                        // build email footer
                        buildFooter();

                        ////////////////////////////////////////
                        //Recs
                        ///////////////////////////////////////
                        iUnique = 1;
                        diRec = new DirectoryInfo(cfg_recroot);
                        fiRECFiles = diRec.GetFiles("*.docx");

                        // for each docx 
                        foreach (FileInfo fiRecDoc in fiRECFiles)
                        {

                            // Get case # from file name
                            sCaseNum = fiRecDoc.Name.Substring(0, fiRecDoc.Name.IndexOf("."));

                            // Get CaseID & customer number
                            if (!GetCaseInfo(sCaseNum))
                            {
                                sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("GetInfo failed for case: " + sCaseNum);
                            }

                            if (sIRFileNum == "")
                            {
                                sIRFileNum = "99999999";
                            }

                            sbEmail.Append("    Original (rec) sIRUSerid: " + sIRUserID + "\r\n");

                            // If sIRUserID (left of person ordering email) is in list, change to "IR_Offshore"
                            if (lOffshore.IndexOf(sIRUserID.ToUpper()) != -1)
                            {
                                sIRUserID = "IR_Offshore";
                            }

                            sbEmail.Append("    Translated sIRUSerid: " + sIRUserID + "\r\n");


                            // replace any illegal characters in address & file #  with "-"
                            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                            {
                                sAddLine1 = sAddLine1.Replace(c, '-');
                                sIRFileNum = sIRFileNum.Replace(c, '-');
                            }

                            // Build Rec file name
                            //sOutputFileName = sIRDrawer & "_" & sIRFile & "_" & msIRFolderType & "_" & msIRDocType & "__" & "_______" & msIRPageDesc & "_" & msIRFlowID & "_" & msIRStepID & "_" & sIRUID & "_" & msIRPriority & "_" & msIRTaskDesc & "__" & msIRConvFlag & "_" & msIRFileType & "_" & iUnique.ToString & ".pdf
                            if (sCustNum == "7062")   //TAMPA - COMMERCIAL
                            {
                                //sOutputFileName = cfg_FTPRoot + "SCTP_" + sIRFileNum + "_" + "Policy Information 0" + "_" + "INSP" + "__" + "_______" + "Recommendation Memo" + "_" + "" + "_" + "" + "_" + "CHDAVIS" + "_" + "5" + "_" + "Inspection Received" + "__" + "C" + "_" + "SCTP" + "_";
                                //sOutputFileName = cfg_FTPRoot + @"SCU Tampa_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + "CHDAVIS" + "_@_";

                                if (sIRUserID != "IR_Offshore")
                                    sIRUserID = "CHDAVIS";

                                sOutputFileName = cfg_FTPRoot + @"SCU Tampa_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + sIRUserID + "_@_";
                            }
                            else if (sCustNum == "7213")    //TAMPA - GARAGE
                            {
                                //sOutputFileName = cfg_FTPRoot + "SCTP_" + sIRFileNum + "_" + "Policy Information 0" + "_" + "INSP" + "__" + "_______" + "Recommendation Memo" + "_" + "" + "_" + "" + "_" + "LBLAIR" + "_" + "5" + "_" + "Inspection Received" + "__" + "C" + "_" + "SCTP" + "_";
                                //sOutputFileName = cfg_FTPRoot + @"SCU Tampa_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + "LBLAIR" + "_@_";

                                if (sIRUserID != "IR_Offshore")
                                    sIRUserID = "LBLAIR";

                                sOutputFileName = cfg_FTPRoot + @"SCU Tampa_@_Underwriting_@_" + sIRFileNum + "_@_" + sAddLine1 + "_@_" + sIRUserID + "_@_";
                            }

                            // If it exists - add digit to end
                            if (File.Exists(sOutputFileName + iUnique.ToString() + ".docx"))
                            {

                                iUnique = 2;
                                while (true)
                                {

                                    if (File.Exists(sOutputFileName + iUnique.ToString() + ".docx"))
                                    {
                                        iUnique++;
                                        if (iUnique > 10)
                                        {
                                            throw new ApplicationException(sOutputFileName + iUnique.ToString() + ".docx" + " could not be saved - exists.");
                                        }

                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }

                            // Copy to FTP folder under new name
                            fiRecDoc.CopyTo(sOutputFileName + iUnique.ToString() + ".docx");
                            sbEmail.Append("    Rec Copied: " + sOutputFileName + iUnique.ToString() + ".docx" + "\r\n");

                        }   // for each rec

                    }
                    catch (Exception ex)
                    {
                        bErr = true;
                        sbEmail.Append("**** Error processing files for case:" + sCaseNum + "\r\n" + ex.Message + "\r\n\r\n");
                    }

                    //////////////////////////////
                    // FTP all files in PDF folder
                    //////////////////////////////
                    int iNumUploaded = 0;
                    int iNumToUpload = 0;

                    diPDF = new DirectoryInfo(cfg_FTPRoot);
                    fiPDFFiles = diPDF.GetFiles("*.*");

                    iNumToUpload = fiPDFFiles.Count();
                    if (iNumToUpload == 0)
                    {
                        sbEmail.Append("\r\n\r\n---- No Files to FTP\r\n");
                    }
                    else
                    {
                        sbEmail.Append("\r\n\r\n---- Begin FTP transfer\r\n");
                        sbEmail.Append("\r\n\r\n" + iNumToUpload.ToString() + " files to upload\r\n");
                        try
                        {
                            // Setup session options
                            SessionOptions sessionOptions = new SessionOptions
                            {
                                Protocol = Protocol.Sftp,
                                HostName = "Sftp.scui.com",
                                UserName = "SibFla",
                                Password = "W36CkHwA",
                                SshHostKeyFingerprint = "ssh-rsa 2048 5e:02:ae:fd:e3:cb:2e:1e:af:a0:8d:13:f4:1c:b4:a6",
                            };
                            
                            //SshHostKeyFingerprint = "ssh-dss 1024 80:83:cd:a3:b2:eb:90:c1:b7:8d:81:62:db:a3:79:21",

                            using (Session session = new Session())
                            {

                                session.SessionLogPath = @"c:\automationlogs\SCUI\FTPLogs.txt";

                                // Connect
                                session.Open(sessionOptions);

                                // Upload files
                                TransferOptions transferOptions = new TransferOptions();
                                transferOptions.TransferMode = TransferMode.Binary;
                                transferOptions.ResumeSupport.State = TransferResumeSupportState.Off;
                                transferOptions.PreserveTimestamp = false;

                                TransferOperationResult transferResult;
                                // 1/16/19 //transferResult = session.PutFiles(cfg_FTPRoot + "*", "/Usr/SibFla/Tampa/", false, transferOptions);
                                transferResult = session.PutFiles(cfg_FTPRoot + "*", "/Underwriting/", false, transferOptions);

                                // Throw on any error
                                transferResult.Check();

                                // Print results
                                foreach (TransferEventArgs transfer in transferResult.Transfers)
                                {
                                    oLU.WritetoLog(transfer.FileName + " uploaded");
                                    sbEmail.Append(transfer.FileName + " uploaded");
                                    iNumUploaded++;
                                }
                            }
                            oLU.WritetoLog("\r\n" + iNumUploaded.ToString() + " files uploaded\r\n\r\n---- End FTP transfer\r\n");
                            sbEmail.Append("\r\n" + iNumUploaded.ToString() + " files uploaded\r\n\r\n---- End FTP transfer\r\n");
                        }
                        catch (Exception ex)
                        {
                            bErr = true;
                            oLU.WritetoLog("**** Error in FTP Transfer\r\n\r\n" + ex.Message + "\r\n\r\n");
                            sbEmail.Append("**** Error in FTP Transfer\r\n\r\n" + ex.Message + "\r\n\r\n");
                        }

                    }
                    // Done - write to log, send email  and clean up
                    sbEmail.Append("\r\nEnd Processing  " + DateTime.Now.ToString() + "\r\n\r\n");
                    oLU.WritetoLog("\r\nEnd Processing  " + DateTime.Now.ToString() + "\r\n\r\n");
                    oLU.closeLog();

                    if (iNumUploaded > 0)
                    {
                        sendTransmittal();
                    }

                    if (bErr)
                    {
                        sendLogEmail("**** ERROR RECORDED - SCUI Delivery Processing ****", sbEmail.ToString());
                    }
                    else
                    {
                        sendLogEmail("SCUI Delivery Processing", sbEmail.ToString());
                    }
                }

            }   //if FTP

            private void sendLogEmail(string sSubject, string sBodyText)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "noreply@sibfla.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;
                }
            }

            private void sendTransmittal()
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "sibflamail@sibfla.com";
                    oMail.MailTo = "chdavis@scui.com";
                    //oMail.MailTo = "jeff@sibfla.com";
                    oMail.MailBCC = "jeff@sibfla.com";
                    oMail.MsgSubject = "Transmittal - " + DateTime.Today.ToShortDateString();
                    oMail.MsgBody = sbmsgBody.ToString();
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = true;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;
                }
            }
            private bool GetCaseInfoOLD(string sCaseNum)
            {

                bool bRetVal = false;

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseInfo_CaseNum";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {
                            guCaseID = sqlReader.GetGuid(1);
                            sPolicy = sqlReader.GetSqlString(3).ToString();
                            sInsured = sqlReader.GetSqlString(2).ToString();
                            sCustName = sqlReader.GetSqlString(10).ToString().Trim();
                            sCustNum = sqlReader.GetSqlString(11).ToString().Trim();
                            sAmount = sqlReader.GetSqlDecimal(12).ToString();
                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }

                    // IR FileNum
                    sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                    sqlCmd1.Parameters.AddWithValue("@fieldname", "ImageRight File Number");

                    sIRFileNum = "";
                    object oRetVal = sqlCmd1.ExecuteScalar();

                    if (oRetVal != null)
                    {
                        sIRFileNum = (string)oRetVal;
                    }

                    if (sIRFileNum == "")
                    {
                        sbEmail.Append("\r\n" + "No IR File Num for " + sCaseNum + " \r\n");
                    }

                    bRetVal = true;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();



                }

                return bRetVal;
            }

            private void buildHeader()
            {

                // <head>
                sbmsgBody = sbmsgBody.Append("<html><head><title>Sutton Inspection Bureau, Inc.of Florida</title> " + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<style type='text/css'>.stdText {font-size: 8pt;COLOR: black;font-family: Verdana,Tahoma,Arial;}" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append(".stdTextBold {font-size: 8pt;color: black;font-family: Verdana,Tahoma,Arial;font-weight: bold;}" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append(".largeText {font-size: 12pt;color: black;font-family: Verdana,Tahoma,Arial;font-weight: bold;}</style></head>" + System.Environment.NewLine);

                // <body>
                sbmsgBody = sbmsgBody.Append("<body><table cellSpacing = '0' cellPadding = '2' width = '760' border = '0'><tr><td width = '10'> &nbsp;</td><td align = 'left' width = '475'><span class='stdText'>Sutton Inspection Bureau, Inc.of Florida</span></td><td width = '275'> &nbsp;</td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td> &nbsp;</td><td align = 'left'><span class='stdText'>5656 Central Avenue</span></td><td align = 'right'><span class='largeText'>Delivery Transmittal</span></td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td> &nbsp;</td><td align = 'left'><span class='stdText'>St.Petersburg,  FL 33707-1718</span></td><td>&nbsp;</td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='0' width='760' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='750'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='760' border='0'><tr><td width = '10'>&nbsp;</td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align= right' width= 60'><span Class='stdText'>Account:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='left' width='340'><span Class='stdText'>SCU - Tampa</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right' width='240'><span Class='stdText'>Delivery date:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right' width='110'><span Class='stdText'>" + sDeliveryDate + "</span></td></tr>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right'><span Class='stdText'>Batch Number:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right'><span Class='stdText'>" + sBatchNum.Trim() + "</span></td></tr></table>" + System.Environment.NewLine);

                //sbmsgBody = sbmsgBody.Append("</table>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='0' width='760' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='750'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='760' style='border - bottom:solid; '><tr><td align = 'center' width = '120'><span Class='stdTextBold'> Policy </span></td>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> IR File</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='360'><span Class='stdTextBold'> Insured </span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> Case</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> Amount </span></td></tr>" + System.Environment.NewLine);

            }

            private void buildFooter()
            {
                sbmsgBody = sbmsgBody.Append("</table></body></html>");

            }

            private void buildLineItem(string sPolicy, string sIR, string sInsured, string sCaseNum, string sAmount)
            {

                sbmsgBody = sbmsgBody.Append("<tr><td align='center'><span Class='stdText'>" + sPolicy + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'> " + sIR + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='left'><span Class='stdText'>" + sInsured + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'>" + sCaseNum + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'>" + sAmount + "</span></td></tr>");
                
            }
            private bool GetCaseInfo(string sCaseNum)
            {

                bool bRetVal = false;

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseInfo_CaseNum";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {
                            guCaseID = sqlReader.GetGuid(1);
                            sInsured = sqlReader.GetSqlString(2).ToString();
                            sPolicy = sqlReader.GetSqlString(3).ToString();
                            sAddLine1 = sqlReader.GetSqlString(4).ToString();
                            sCustName = sqlReader.GetSqlString(10).ToString().Trim();
                            sCustNum = sqlReader.GetSqlString(11).ToString().Trim();
                            sAmount = sqlReader.GetSqlDecimal(12).ToString();
                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }

                    // IR FileNum
                    sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                    sqlCmd1.Parameters.AddWithValue("@fieldname", "ImageRight File Number");

                    sIRFileNum = "";
                    object oRetVal = sqlCmd1.ExecuteScalar();

                    if (oRetVal != null)
                    {
                        sIRFileNum = (string)oRetVal;
                    }

                    if (sIRFileNum == "")
                    {
                        sbEmail.Append("\r\n" + "No IR File Num for " + sCaseNum + " \r\n");
                    }

                    //// Underwriter email (IRUSerID)
                    //sqlCmd1.CommandText = "sp_GetUnderwriterEmail";
                    //sqlCmd1.Parameters.Clear();
                    //sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);

                    //sIRUserID = "";
                    //oRetVal = sqlCmd1.ExecuteScalar();

                    //if (oRetVal != null)
                    //{
                    //    sIRUserID = (string)oRetVal;
                    //}

                    //if (sIRUserID == "")
                    //{
                    //    sbEmail.Append("\r\n" + "No Underwriter email (IRUserID) for " + sCaseNum + " \r\n");
                    //}
                    //else
                    //{
                    //    // Get the left part of the email for the IRUserID
                    //    int iPos = sIRUserID.IndexOf("@");
                    //    if (iPos > 0)
                    //    {
                    //        sIRUserID = sIRUserID.Substring(0, iPos);
                    //    }
                    //    else
                    //    {
                    //        sbEmail.Append("\r\n" + "Invalid Underwriter email (IRUserID) for " + sCaseNum + " \r\n");
                    //        sIRUserID = "UNKNOWN";
                    //    }
                    //}

                    // email pf person ordering (IRUSerID)
                    sqlCmd1.CommandText = "sp_GetCaseOrderedBy";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);

                    sIRUserID = "";
                    oRetVal = sqlCmd1.ExecuteScalar();

                    if (oRetVal != null)
                    {
                        sIRUserID = (string)oRetVal;
                    }

                    if (sIRUserID == "")
                    {
                        sbEmail.Append("\r\n" + "No email for person ordering (IRUserID) for " + sCaseNum + " \r\n");
                    }
                    else
                    {
                        // Get the left part of the email for the IRUserID
                        int iPos = sIRUserID.IndexOf("@");
                        if (iPos > 0)
                        {
                            sIRUserID = sIRUserID.Substring(0, iPos);
                        }
                        else
                        {
                            sbEmail.Append("\r\n" + "Invalid person ordering email (IRUserID) for " + sCaseNum + " \r\n");
                            sIRUserID = "UNKNOWN";
                        }
                    }

                    bRetVal = true;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();
                }

                return bRetVal;
            }

        }
    }
}
