﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Globalization;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;

namespace AnnualSalesReport
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static int cfg_TimeZoneOffset;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Range oRange = null;
        static int iReportYear;
        static string sMode = "";
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;
        static int[] aBGColor = new int[14];
        
        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                //cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));
                cfg_TimeZoneOffset = getTimeZoneOffset();

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Year for report
                if (args.Length < 1)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                //if (iReportYear < 2010 || iReportYear > 2025)
                //{
                //    throw new SystemException("Invalid reort year supplied");
                //}

                // F = full month / month end
                // M = mid month
                // Y = year end
                sMode = args[0].ToUpper();
                if (sMode != "F" && sMode != "M" && sMode != "Y")
                {
                    throw new SystemException("Invalid reort mode supplied - must be F, M or Y");
                }
                oLU.WritetoLog("Report mode: " + sMode);

                string sRunDate = DateTime.Now.ToShortDateString();
                string sFileDate = DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Year.ToString();
                string sEmailSubject = "";
                string sExcelFileName = "";
                string sExcelFileNameNoEx = "";

                // set report year
                // current year unless year end - set for current -1
                if (sMode == "F" || sMode == "M")
                    iReportYear = DateTime.Now.Year;
                else
                    iReportYear = DateTime.Now.Year-1;

                // Title / Subject
                if (sMode == "F")
                    sEmailSubject = "Annual Sales" + iReportYear.ToString() + " as of " + sRunDate;
                else if (sMode == "M")
                    sEmailSubject = "Annual Sales - Mid Month" + iReportYear.ToString() + " as of " + sRunDate;
                else if (sMode == "Y")
                    sEmailSubject = "Annual Sales - Year Ending " + iReportYear.ToString() + " - run date: " + sRunDate;
                oLU.WritetoLog("Title: " + sEmailSubject);
                
                // File name
                if (sMode == "F")
                    sExcelFileNameNoEx = cfg_outputdir + "AnnualSales_" + sFileDate;
                else if (sMode == "M")
                    sExcelFileNameNoEx = cfg_outputdir + "AnnualSales_MidMonth_" + sFileDate;
                else if (sMode == "Y")
                    sExcelFileNameNoEx = cfg_outputdir + "AnnualSales_YearEnd_" + iReportYear.ToString();

                sExcelFileName = sExcelFileNameNoEx + ".xlsx";

                oLU.WritetoLog("File name: " + sExcelFileName);

                ///////////////////////////////////////////////////
                // 360 
                ///////////////////////////////////////////////////

                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                int iRow = 0;
                string sAccount = "";
                int iM1 = 0;
                int iM2 = 0;
                int iM3 = 0;
                int iM4 = 0;
                int iM5 = 0;
                int iM6 = 0;
                int iM7 = 0;
                int iM8 = 0;
                int iM9 = 0;
                int iM10 = 0;
                int iM11 = 0;
                int iM12 = 0;
                int iMTotal = 0;

                int iT1 = 0;
                int iT2 = 0;
                int iT3 = 0;
                int iT4 = 0;
                int iT5 = 0;
                int iT6 = 0;
                int iT7 = 0;
                int iT8 = 0;
                int iT9 = 0;
                int iT10 = 0;
                int iT11 = 0;
                int iT12 = 0;
                int iYTDTotal = 0;


                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandTimeout = 120;

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                if (sMode == "F")
                    sqlCmd1.CommandText = "sp_NumOrderedByCustByMonth";
                else if (sMode == "M")
                    sqlCmd1.CommandText = "sp_NumOrderedByCustByMidMonth";
                else if (sMode == "Y")
                    sqlCmd1.CommandText = "sp_NumOrderedByCustByMonth";
                    
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@reportyear", iReportYear);
                sqlConn1.Open();

                // initiate Excel
                oExcel = new Excel.Application();
                oExcel.Visible = true;
                oWorkbook = oExcel.Workbooks.Add(1);
                o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                o360Worksheet.Name = "Cases ordered in " + iReportYear.ToString();

                if (sMode == "F")
                    createHeader360(o360Worksheet, sRunDate);
                else if (sMode == "M")
                    createHeader360MidMonth(o360Worksheet, sRunDate);
                else if (sMode == "Y")
                    createHeaderYE(o360Worksheet, "12/31/" + iReportYear.ToString());

                iRow = 4;

                oLU.WritetoLog("Running SQL");

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {


                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        if (sqlReader.IsDBNull(0))
                        {
                            sAccount = "**** NO ACCOUNT ****";
                        }
                        else
                        {
                           sAccount = (string)sqlReader.GetSqlString(0);
                        }

                        iM1 = (int)sqlReader.GetSqlInt32(1);
                        iM2 = (int)sqlReader.GetSqlInt32(2);
                        iM3 = (int)sqlReader.GetSqlInt32(3);
                        iM4 = (int)sqlReader.GetSqlInt32(4);
                        iM5 = (int)sqlReader.GetSqlInt32(5);
                        iM6 = (int)sqlReader.GetSqlInt32(6);
                        iM7 = (int)sqlReader.GetSqlInt32(7);
                        iM8 = (int)sqlReader.GetSqlInt32(8);
                        iM9 = (int)sqlReader.GetSqlInt32(9);
                        iM10 = (int)sqlReader.GetSqlInt32(10);
                        iM11 = (int)sqlReader.GetSqlInt32(11);
                        iM12 = (int)sqlReader.GetSqlInt32(12);
                        iMTotal = (int)sqlReader.GetSqlInt32(13);

                        iT1 = iT1 + iM1;
                        iT2 = iT2 + iM2;
                        iT3 = iT3 + iM3;
                        iT4 = iT4 + iM4;
                        iT5 = iT5 + iM5;
                        iT6 = iT6 + iM6;
                        iT7 = iT7 + iM7;
                        iT8 = iT8 + iM8;
                        iT9 = iT9 + iM9;
                        iT10 = iT10 + iM10;
                        iT11 = iT11 + iM11;
                        iT12 = iT12 + iM12;
                        iYTDTotal = iYTDTotal + iMTotal;

                        aBGColor[2] = SetCellColor(2,iM1, iM2);
                        aBGColor[3] = SetCellColor(3,iM2, iM3);
                        aBGColor[4] = SetCellColor(4,iM3, iM4);
                        aBGColor[5] = SetCellColor(5,iM4, iM5);
                        aBGColor[6] = SetCellColor(6,iM5, iM6);
                        aBGColor[7] = SetCellColor(7,iM6, iM7);
                        aBGColor[8] = SetCellColor(8,iM7, iM8);
                        aBGColor[9] = SetCellColor(9,iM8, iM9);
                        aBGColor[10] = SetCellColor(10,iM9, iM10);
                        aBGColor[11] = SetCellColor(11,iM10, iM11);
                        aBGColor[12] = SetCellColor(12,iM11, iM12);

                        //SetCellColorAll();

                        addData(o360Worksheet, iRow, 1, sAccount, "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                        addIntData(o360Worksheet, iRow, 2, iM1, "B" + iRow.ToString(), "B" + iRow.ToString(), "#,0", "C",4);
                        addIntData(o360Worksheet, iRow, 3, iM2, "C" + iRow.ToString(), "C" + iRow.ToString(), "#,0", "C",aBGColor[2]);
                        addIntData(o360Worksheet, iRow, 4, iM3, "D" + iRow.ToString(), "D" + iRow.ToString(), "#,0", "C", aBGColor[3]);
                        addIntData(o360Worksheet, iRow, 5, iM4, "E" + iRow.ToString(), "E" + iRow.ToString(), "#,0", "C", aBGColor[4]);
                        addIntData(o360Worksheet, iRow, 6, iM5, "F" + iRow.ToString(), "F" + iRow.ToString(), "#,0", "C", aBGColor[5]);
                        addIntData(o360Worksheet, iRow, 7, iM6, "G" + iRow.ToString(), "G" + iRow.ToString(), "#,0", "C", aBGColor[6]);
                        addIntData(o360Worksheet, iRow, 8, iM7, "H" + iRow.ToString(), "H" + iRow.ToString(), "#,0", "C", aBGColor[7]);
                        addIntData(o360Worksheet, iRow, 9, iM8, "I" + iRow.ToString(), "I" + iRow.ToString(), "#,0", "C", aBGColor[8]);
                        addIntData(o360Worksheet, iRow, 10, iM9, "J" + iRow.ToString(), "J" + iRow.ToString(), "#,0", "C", aBGColor[9]);
                        addIntData(o360Worksheet, iRow, 11, iM10, "K" + iRow.ToString(), "K" + iRow.ToString(), "#,0", "C", aBGColor[10]);
                        addIntData(o360Worksheet, iRow, 12, iM11, "L" + iRow.ToString(), "L" + iRow.ToString(), "#,0", "C", aBGColor[11]);
                        addIntData(o360Worksheet, iRow, 13, iM12, "M" + iRow.ToString(), "M" + iRow.ToString(), "#,0", "C", aBGColor[12]);
                        if (sMode == "F" || sMode == "Y")
                            addIntData(o360Worksheet, iRow, 14, iMTotal, "N" + iRow.ToString(), "N" + iRow.ToString(), "#,0", "C", 4);

                        iRow++;

                    } while (sqlReader.Read());     // 360

                    // Totals

                    aBGColor[2] = SetCellColor(2,iT1, iT2);
                    aBGColor[3] = SetCellColor(3,iT2, iT3);
                    aBGColor[4] = SetCellColor(4,iT3, iT4);
                    aBGColor[5] = SetCellColor(5,iT4, iT5);
                    aBGColor[6] = SetCellColor(6,iT5, iT6);
                    aBGColor[7] = SetCellColor(7,iT6, iT7);
                    aBGColor[8] = SetCellColor(8,iT7, iT8);
                    aBGColor[9] = SetCellColor(9,iT8, iT9);
                    aBGColor[10] = SetCellColor(10,iT9, iT10);
                    aBGColor[11] = SetCellColor(11,iT10, iT11);
                    aBGColor[12] = SetCellColor(12,iT11, iT12);


                    addData(o360Worksheet, iRow, 1, "Totals", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                    addIntData(o360Worksheet, iRow, 2, iT1, "B" + iRow.ToString(), "B" + iRow.ToString(), "#,0", "C", 4);
                    addIntData(o360Worksheet, iRow, 3, iT2, "C" + iRow.ToString(), "C" + iRow.ToString(), "#,0", "C", aBGColor[2]);
                    addIntData(o360Worksheet, iRow, 4, iT3, "D" + iRow.ToString(), "D" + iRow.ToString(), "#,0", "C", aBGColor[3]);
                    addIntData(o360Worksheet, iRow, 5, iT4, "E" + iRow.ToString(), "E" + iRow.ToString(), "#,0", "C", aBGColor[4]);
                    addIntData(o360Worksheet, iRow, 6, iT5, "F" + iRow.ToString(), "F" + iRow.ToString(), "#,0", "C", aBGColor[5]);
                    addIntData(o360Worksheet, iRow, 7, iT6, "G" + iRow.ToString(), "G" + iRow.ToString(), "#,0", "C", aBGColor[6]);
                    addIntData(o360Worksheet, iRow, 8, iT7, "H" + iRow.ToString(), "H" + iRow.ToString(), "#,0", "C", aBGColor[7]);
                    addIntData(o360Worksheet, iRow, 9, iT8, "I" + iRow.ToString(), "I" + iRow.ToString(), "#,0", "C", aBGColor[8]);
                    addIntData(o360Worksheet, iRow, 10, iT9, "J" + iRow.ToString(), "J" + iRow.ToString(), "#,0", "C", aBGColor[9]);
                    addIntData(o360Worksheet, iRow, 11, iT10, "K" + iRow.ToString(), "K" + iRow.ToString(), "#,0", "C", aBGColor[10]);
                    addIntData(o360Worksheet, iRow, 12, iT11, "L" + iRow.ToString(), "L" + iRow.ToString(), "#,0", "C", aBGColor[11]);
                    addIntData(o360Worksheet, iRow, 13, iT12, "M" + iRow.ToString(), "M" + iRow.ToString(), "#,0", "C", aBGColor[12]);

                    if (sMode == "F" || sMode == "Y")
                        addIntData(o360Worksheet, iRow, 14, iYTDTotal, "N" + iRow.ToString(), "N" + iRow.ToString(), "#,0", "C", 4);


                }   // has rows
                else
                {
                    addDataNoFormat(o360Worksheet, iRow, 1, "No items found", "A" + iRow.ToString(), "A" + iRow.ToString());
                    oLU.WritetoLog("No data");
                }

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;
                 

                //oWorkbook.SaveAs(cfg_outputdir + "ReviewerStats360_" + sDateDisp, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.Close(true, oMissing, oMissing);
                oExcel.Quit();
                oLU.WritetoLog("File saved");
                msMsgBody += "Processing completed for 360" + System.Environment.NewLine + cfg_outputdir + "AnnualSales_" + sRunDate + System.Environment.NewLine + System.Environment.NewLine;
                
                releaseObject(oExcel);
                releaseObject(oWorkbook);
                releaseObject(o360Worksheet);
                oLU.WritetoLog("Sending file");
                sendExcelFile(sEmailSubject, sExcelFileName);
                msMsgBody += "File sent " + sExcelFileName;
            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(msMsgBody);                
                }
            
            }
        }

        static int SetCellColor(int iMonth, int iM1, int iM2)
        {

            int iRetVal = 0;
            int iCurMonth = DateTime.Now.Month;

            // no color for current month (unless YTD mode)
            if (sMode !="Y" && (iMonth == iCurMonth))
                return iRetVal;

            // no color for months > run date (unless YTD mode)
            if (sMode != "Y" && (iMonth > iCurMonth ))
                return iRetVal;

            if (iM2 > iM1)
                iRetVal = 1;
            else if (iM2 < iM1)
                iRetVal = 2;
            else if (iM2 == 0)
                iRetVal = 3;

            return iRetVal;

        }

        static void SetCellColorAll()
        {
            int iCurMonth = DateTime.Now.Month;

            if (iCurMonth < 12)
            {
                for (int i = iCurMonth + 1; i < 13; i++)
                {
                    aBGColor[i] = 0;
                }
            }

        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


 

        static void createHeader360(Excel._Worksheet oWorkSheet, string sRunDate)
        {

            Excel.Range oRange;

            oWorkSheet.get_Range("A1", "N1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "N1");
            oRange.FormulaR1C1 = "Cases ordered by customer as of " + sRunDate;

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 14;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Account";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 55;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Jan";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "Feb";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Mar";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Apr";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 6] = "May";
            oRange = oWorkSheet.get_Range("F3", "F3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 7] = "Jun";
            oRange = oWorkSheet.get_Range("G3", "G3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 8] = "Jul";
            oRange = oWorkSheet.get_Range("H3", "H3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 9] = "Aug";
            oRange = oWorkSheet.get_Range("I3", "I3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 10] = "Sep";
            oRange = oWorkSheet.get_Range("J3", "J3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 11] = "Oct";
            oRange = oWorkSheet.get_Range("K3", "K3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 12] = "Nov";
            oRange = oWorkSheet.get_Range("L3", "L3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 13] = "Dec";
            oRange = oWorkSheet.get_Range("M3", "M3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 14] = "YTD";
            oRange = oWorkSheet.get_Range("N3", "N3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        }


        static void createHeaderYE(Excel._Worksheet oWorkSheet, string sRunDate)
        {

            Excel.Range oRange;

            oWorkSheet.get_Range("A1", "N1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "N1");
            oRange.FormulaR1C1 = "Cases ordered by customer for the year ending " + sRunDate ;

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 14;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Account";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 55;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Jan";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "Feb";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Mar";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Apr";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 6] = "May";
            oRange = oWorkSheet.get_Range("F3", "F3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 7] = "Jun";
            oRange = oWorkSheet.get_Range("G3", "G3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 8] = "Jul";
            oRange = oWorkSheet.get_Range("H3", "H3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 9] = "Aug";
            oRange = oWorkSheet.get_Range("I3", "I3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 10] = "Sep";
            oRange = oWorkSheet.get_Range("J3", "J3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 11] = "Oct";
            oRange = oWorkSheet.get_Range("K3", "K3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 12] = "Nov";
            oRange = oWorkSheet.get_Range("L3", "L3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 13] = "Dec";
            oRange = oWorkSheet.get_Range("M3", "M3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 14] = "YTD";
            oRange = oWorkSheet.get_Range("N3", "N3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        }

        static void createHeader360MidMonth(Excel._Worksheet oWorkSheet, string sRunDate)
        {

            Excel.Range oRange;

            oWorkSheet.get_Range("A1", "M1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "M1");
            oRange.FormulaR1C1 = "Cases ordered by customer Mid Month as of " + sRunDate;

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 14;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Account";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 55;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Jan 15";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "Feb 15";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Mar 15";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Apr 15";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 6] = "May 15";
            oRange = oWorkSheet.get_Range("F3", "F3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 7] = "Jun 15";
            oRange = oWorkSheet.get_Range("G3", "G3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 8] = "Jul 15";
            oRange = oWorkSheet.get_Range("H3", "H3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 9] = "Aug 15";
            oRange = oWorkSheet.get_Range("I3", "I3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 10] = "Sep 15";
            oRange = oWorkSheet.get_Range("J3", "J3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 11] = "Oct 15";
            oRange = oWorkSheet.get_Range("K3", "K3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 12] = "Nov 15";
            oRange = oWorkSheet.get_Range("L3", "L3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 13] = "Dec 15";
            oRange = oWorkSheet.get_Range("M3", "M3");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        }


        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data, 
			string cell1, string cell2,string format,string sHorizAlign)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
        }

        static void addIntData(Excel._Worksheet oWorkSheet, int row, int col, int data,
            string cell1, string cell2, string format, string sHorizAlign, int ibgColor)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
            if (ibgColor == 1)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.PaleGreen);
            }
            else if (ibgColor == 2)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Orange);
            }
            else if (ibgColor == 3)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            }
            else if (ibgColor == 4)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.AliceBlue);
            }
            else
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            }


        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }



        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "Annual Sales";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Annual Sales Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = "axel@sibfla.com;thomas@sibfla.com;jordan@sibfla.com;michael@sibfla.com;andrea@sibfla.com";
                //oMail.MailTo = "jeff@sibfla.com";
                oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

        static int getTimeZoneOffset()
        {
            SqlConnection sqlConn = null;
            SqlCommand sqlCmd = null;
            int iRetVal = 0;

            try
            {
                //string sQuery = @"SELECT dbo.TimeZoneOffset AS TZOffset;";
                sqlConn = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd = new SqlCommand("SELECT dbo.TimeZoneOffset()", sqlConn);
                sqlConn.Open();
                Int32 functionResult = (Int32)sqlCmd.ExecuteScalar();

                iRetVal = functionResult;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }
            finally
            {

                // close objects
                if (sqlConn != null)
                    sqlConn.Close();
            }

            return iRetVal;
        }
    }
}
