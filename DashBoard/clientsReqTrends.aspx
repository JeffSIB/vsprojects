﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="clientsReqTrends.aspx.cs"
    Inherits="DashBoard.clientsReqTrends" %>

<%@ Register Assembly="Xceed.Chart.Server, Version=4.2.100.0, Culture=neutral, PublicKeyToken=ba83ff368b7563c6"
    Namespace="Xceed.Chart.Server" TagPrefix="xceedchart" %>
<%@ Register TagPrefix="uc1" TagName="SIBMiniHeader" Src="SIBMiniHeader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Dashboard</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        function launchCurrent() {
            location.href = "Default.aspx";
        }
        function launchRevenue() {
            location.href = "revenueMain.aspx";
        }
        function inactive() {
            alert("This function is not currently active.");
        }
    </script>

</head>
<body style="margin-top: 0; background-color: #f7f3f7;">
    <form id="frmDefault" runat="server">
    <div>
        <center>
            <table style="width: 800px; background-color: #fff">
                <tr>
                    <td style="width: 100%;">
                        <uc1:SIBMiniHeader ID="SIBHeader1" runat="server"></uc1:SIBMiniHeader>
                        <table id="tblTabHdr" cellspacing="0" cellpadding="2" width="700">
                            <tr style="height: 20px">
                                <td width="50">
                                    &nbsp;
                                </td>
                                <td class="unsel_menuitem" id="tabCurrent" title="Current stats" onclick="launchCurrent()"
                                    width="150">
                                    Current
                                </td>
                                <td class="unsel_menuitem" id="tabRevenue" title="Revenue" onclick="launchRevenue()"
                                    width="150">
                                    Revenue
                                </td>
                                <td class="sel_menuitem" id="tabClients" title="Clients" width="150">
                                    Clients
                                </td>
                                <td class="unsel_menuitem" id="tabInspectors" title="Inspectors" onclick="inactive()"
                                    width="150">
                                    Inspectors
                                </td>
                                <td width="50">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table id="Table2" cellspacing="0" cellpadding="2" width="700">
                            <tr>
                                <td width="100%" align="center" class="StdText" valign="middle">
                                    Select an item to view:&nbsp;
                                    <asp:ListBox ID="lbClientRptType" runat="server" Rows="1">
                                        <asp:ListItem>Client request trends</asp:ListItem>
                                        <asp:ListItem>Client margin</asp:ListItem>
                                    </asp:ListBox>
                                    &nbsp;
                                    <asp:LinkButton ID="lbGo" runat="server" CssClass="StdText" OnClick="lbGo_Click">Go</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <table width="760">
                <tr>
                    <td align="center">
                        <table id="Table1" style="background-color: #EFF3F7; border-right: #d3d3d3 thin solid;
                            border-top: #d3d3d3 thin solid; border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid"
                            cellspacing="4" cellpadding="4" width="600" border="0" runat="server">
                            <tr style="background-color: #EFF3F7">
                                <td align="center" class="StdTextSmallCap">
                                    Client Request Trends
                                </td>
                            </tr>
                            <tr style="background-color: #EFF3F7">
                                <td align="center" class="StdTextSmall">
                                    This chart contrasts new requests for the week against average weekly requests over
                                    the past year.
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table width="560">
                                        <tr>
                                            <td style="width: 300px" align="left" class="StdTextSmallGrey">
                                                <asp:Label ID="lblGraphPd" runat="server" CssClass="StdTextSmallGrey"> </asp:Label>
                                            </td>
                                            <td style="width: 300px" align="right" class="StdTextSmallGrey">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div style="width: 15px; height: 15px; background: #85B581;">
                                                                &nbsp;</div>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label1" runat="server" CssClass="StdTextSmallGrey">Current week</asp:Label>&nbsp;/&nbsp;
                                                        </td>
                                                        <td>
                                                            <div style="width: 15px; height: 15px; background: #705DEC;">
                                                                &nbsp;</div>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label2" runat="server" CssClass="StdTextSmallGrey">Average</asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="760">
                <tr>
                    <td style="width: 760px;" align="center">
                        <xceedchart:ChartServerControl ID="xcClientComp" runat="server" Height="4000px" Width="600px">
                        </xceedchart:ChartServerControl>
                    </td>
                </tr>
            </table>
            </td> </tr> </table>
        </center>
    </div>
    </form>
</body>
</html>
