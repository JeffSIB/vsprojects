﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="clientMargin.aspx.cs" Inherits="DashBoard.clientMargin" %>

<%@ Register TagPrefix="uc1" TagName="SIBMiniHeader" Src="SIBMiniHeader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Dashboard</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        function launchCurrent() {
            location.href = "Default.aspx";
        }
        function launchRevenue() {
            location.href = "revenueMain.aspx";
        }
        function inactive() {
            alert("This function is not currently active.");
        }
    </script>

</head>
<body style="margin-top: 0; background-color: #f7f3f7;">
    <form id="frmDefault" runat="server">
    <div>
        <center>
            <table style="width: 800px; background-color: #fff">
                <tr>
                    <td style="width: 100%;">
                        <uc1:SIBMiniHeader ID="SIBHeader1" runat="server"></uc1:SIBMiniHeader>
                        <table id="tblTabHdr" cellspacing="0" cellpadding="2" width="700">
                            <tr style="height: 20px">
                                <td width="50">
                                    &nbsp;
                                </td>
                                <td class="unsel_menuitem" id="tabCurrent" title="Current stats" onclick="launchCurrent()"
                                    width="150">
                                    Current
                                </td>
                                <td class="unsel_menuitem" id="tabRevenue" title="Revenue" onclick="launchRevenue()"
                                    width="150">
                                    Revenue
                                </td>
                                <td class="sel_menuitem" id="tabClients" title="Clients" width="150">
                                    Clients
                                </td>
                                <td class="unsel_menuitem" id="tabInspectors" title="Inspectors" onclick="inactive()"
                                    width="150">
                                    Inspectors
                                </td>
                                <td width="50">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table id="Table2" cellspacing="0" cellpadding="2" width="700">
                            <tr>
                                <td width="100%" align="center" class="StdText" valign="middle">
                                    Select an item to view:&nbsp;
                                    <asp:ListBox ID="lbClientRptType" runat="server" Rows="1">
                                        <asp:ListItem>Client request trends</asp:ListItem>
                                        <asp:ListItem>Client margin</asp:ListItem>
                                    </asp:ListBox>
                                    &nbsp;
                                    <asp:LinkButton ID="lbGo" runat="server" CssClass="StdText" OnClick="lbGo_Click">Go</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <table id="Table1" style="border-right: #c0c0c0 thin solid; border-top: #c0c0c0 thin solid;
                border-left: #c0c0c0 thin solid; border-bottom: #c0c0c0 thin solid" cellspacing="0"
                cellpadding="0" width="700" border="0" runat="server">
                <tr bgcolor="white" style="height: 25px">
                    <td width="700" class="StdTextSmall">
                        <asp:Label ID="Label1" runat="server" CssClass="StdTextSmall">View report for week beginning: </asp:Label>
                        <asp:ListBox ID="lbWeekEnding" runat="server" Rows="1" OnSelectedIndexChanged="lbWeekEnding_SelectedIndexChanged"
                            AutoPostBack="true"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table cellspacing="2" cellpadding="2" width="680" border="0">
                            <tr>
                                <td align="center" width="680">
                                    <asp:DataGrid ID="Grid1" runat="server" CssClass="StdText" BorderColor="#666666"
                                        BorderWidth="1px" CellPadding="2" AutoGenerateColumns="False" 
                                        BorderStyle="Solid" ShowFooter="true" onitemdatabound="Grid1_ItemDataBound">
                                        <AlternatingItemStyle BorderStyle="None" BorderColor="#666666" BackColor="#E4FFE4">
                                        </AlternatingItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="StdTextSmall" BackColor="#FFFFDD"
                                            Width="680px"></HeaderStyle>
                                            <FooterStyle HorizontalAlign="Center" CssClass="StdTextSmall" BackColor="#FFFFDD"
                                            Width="680px" />
                                        <Columns>
                                            <asp:BoundColumn DataField="acnt" ReadOnly="True" HeaderText="Account">
                                                <ItemStyle Width="80px" HorizontalAlign="center" CssClass="StdTextSmall"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="requested" DataFormatString="{0:N0}" ReadOnly="True"
                                                HeaderText="Requested">
                                                <ItemStyle Width="100px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="completed" ReadOnly="True" DataFormatString="{0:N0}"
                                                HeaderText="Completed">
                                                <ItemStyle Width="100px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="invamt" ReadOnly="True" DataFormatString="{0:c}" HeaderText="Invoiced Amt">
                                                <ItemStyle Width="100px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="inspchg" ReadOnly="True" DataFormatString="{0:c}" HeaderText="Insp Cost">
                                                <ItemStyle Width="100px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="margin" ReadOnly="True" DataFormatString="{0:F2}" HeaderText="Margin">
                                                <ItemStyle Width="100px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="average" ReadOnly="True" DataFormatString="{0:c}" HeaderText="Average">
                                                <ItemStyle Width="100px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                            </asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table cellspacing="2" cellpadding="2" width="680" border="0">
                                        <tr>
                                            <td align="center" width="680">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>
