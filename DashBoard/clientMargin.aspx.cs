﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Principal;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;

namespace DashBoard
{
    public partial class clientMargin : System.Web.UI.Page
    {
        static string cfg_SQLMainSIBUtilConnStr;
        static string cfg_SQLMainSIBIConnStr;
        static LogUtils.LogUtils oLU;
        static string cfg_logfilename;
        
        private int miTRequested = 0;
        private int miTComp = 0;
        private decimal mdTInvAmt = 0;
        private decimal mdTInspChg = 0;


        protected void Page_Load(object sender, EventArgs e)
        {


            //SqlConnection sqlConn1 = null;
            //SqlCommand sqlCmd1 = null;
            //SqlDataReader sqlReader = null;


            //SqlConnection sqlConn2 = null;
            //SqlCommand sqlCmd2 = null;

            try
            {

                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;

                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_SQLMainSIBUtilConnStr = colNameVal.Get("SQLMainSIBUtilConnStr");
                cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");

                                
                if (!IsPostBack)
                {
                    DateTime dBegDate = new DateTime();
                    dBegDate = DateTime.Today;
                    dBegDate = FirstDayofWeek(dBegDate);
                    DateTime dEndDate = dBegDate.AddDays(-7);
                    dBegDate = dBegDate.AddDays(-361);
                    dBegDate = FirstDayofWeek(dBegDate);
                    DateTime dCurDate = dEndDate;
                    lbClientRptType.SelectedIndex = 1;

                    do
                    {
                        lbWeekEnding.Items.Add(dCurDate.ToShortDateString());
                        dCurDate = dCurDate.AddDays(-7);

                    } while (dCurDate >= dBegDate);

                    lbWeekEnding.SelectedIndex = 0;
                    loadData(lbWeekEnding.SelectedValue);
                }

            }
            catch (Exception ex)
            {

                logError(ex.Message);

            }

            finally
            {
                //if (!(sqlConn1 == null))
                //    sqlConn1.Close();
            }

        }

        protected int loadData(string sWeekBeg)
        {
            int iCount = 0;

            DataTable dt = new DataTable();
            dt.Columns.Add("acnt");
            dt.Columns.Add("requested", typeof(int));
            dt.Columns.Add("completed", typeof(int));
            dt.Columns.Add("invamt", typeof(decimal));
            dt.Columns.Add("inspchg", typeof(decimal));
            dt.Columns.Add("margin", typeof(decimal));
            dt.Columns.Add("average", typeof(decimal));

            string sAcnt = "";
            int iRequested = 0;
            int iComp = 0;
            decimal dInvAmt = 0;
            decimal dInspChg = 0;
            decimal dMargin = 0;
            decimal dAvgAmt = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection
                sqlConn1 = new SqlConnection(cfg_SQLMainSIBIConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT acnt_num,requested,completed,invoicedamount,inspcharges FROM clienthistory WHERE weekbeginning = '" + sWeekBeg + "' ORDER BY acnt_num";
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {

                    sqlReader.Read();

                    do
                    {

                        sAcnt = sqlReader.GetSqlInt32(0).ToString();
                        iRequested = (int)sqlReader.GetSqlInt32(1);
                        iComp = (int)sqlReader.GetSqlInt32(2);
                        dInvAmt = (decimal)sqlReader.GetDecimal(3);
                        dInspChg = (decimal)sqlReader.GetDecimal(4);
                        if (dInspChg > 0 && dInvAmt > 0)
                            dMargin = (dInspChg / dInvAmt) * 100;
                        else
                            dMargin = 0;

                        if (iComp > 0 && dInvAmt > 0)
                            dAvgAmt = dInvAmt / iComp;
                        else
                            dAvgAmt = 0;

                        DataRow dr1 = dt.NewRow();

                        dr1["acnt"] = sAcnt;
                        dr1["requested"] = iRequested;
                        dr1["completed"] = iComp;
                        dr1["invamt"] = dInvAmt;
                        dr1["inspchg"] = dInspChg;
                        dr1["margin"] = dMargin;
                        dr1["average"] = dAvgAmt;

                        // Add row
                        dt.Rows.Add(dr1);
                        iCount += 1;

                    } while (sqlReader.Read());


                }


                sqlConn1.Close();
            }

            catch (Exception ex)
            {

                logError("clientMargin - loadData: " + ex.Message);
                Response.Redirect("Util/SystemErr.aspx");

            }

            Grid1.DataSource = dt;
            Grid1.DataBind();

            if (iCount == 0)
            {
                Grid1.Visible = false;
            }

            return iCount;

        }

        protected void lbGo_Click(object sender, EventArgs e)
        {
            if (lbClientRptType.SelectedIndex == 0)
            {
                Response.Redirect("clientsReqTrends.aspx");
            }
            else if (lbClientRptType.SelectedIndex == 1)
            {
                // current page
            }

        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        private void logError(string sErrMsg)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();

            //record exception  
            oLU.WritetoLog("Default.aspx - " + sErrMsg);

            // close objects
            oLU.closeLog();

        }

        protected void lbWeekEnding_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadData(lbWeekEnding.SelectedValue);
        }

        protected void Grid1_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

            switch ((int)(e.Item.ItemType))
            {
                case (int)ListItemType.Item:
                case (int)ListItemType.AlternatingItem:


                    //decimal mdTMargin = 0;
                    //decimal mdTAvgAmt = 0;


                    //Calculate total for the field of each row and alternating row.
                    miTRequested += Convert.ToInt32(e.Item.Cells[1].Text);
                    miTComp += Convert.ToInt32(e.Item.Cells[2].Text);
                    mdTInvAmt += toDecimal(e.Item.Cells[3].Text);
                    mdTInspChg += toDecimal(e.Item.Cells[4].Text);
                    
                    break;
                case (int)ListItemType.Footer:
                    //Use the footer to display the summary row.
                    e.Item.Cells[0].Text = "Total";
                    e.Item.Cells[1].Text = miTRequested.ToString("N0");
                    e.Item.Cells[2].Text = miTComp.ToString("N0");
                    e.Item.Cells[3].Text = mdTInvAmt.ToString("c");
                    e.Item.Cells[4].Text = mdTInspChg.ToString("c");
                    e.Item.Cells[5].Text = (mdTInspChg / mdTInvAmt).ToString("p");
                    e.Item.Cells[6].Text = (mdTInvAmt/miTComp).ToString("c");
                    break;
            }
        }

        protected decimal toDecimal(string sTemp)
        { 
        
            if (sTemp.Length == 0)
                return 0;

            if (sTemp.Contains("$"))
            {
                sTemp = sTemp.Replace("$", "0");
            }
            return Convert.ToDecimal(sTemp);
        
        }

    }
}
