using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Drawing;
using Xceed.Chart;
using Xceed.Chart.GraphicsCore;
using Xceed.Chart.Standard;
using Xceed.Chart.Core;
using Xceed.Chart.Server;


namespace DashBoard
{
    public partial class _Default : System.Web.UI.Page
    {
        static string cfg_SQLMainSIBUtilConnStr;
        static string cfg_360ConnStr;
        static string cfg_360UtilConnStr;

        static LogUtils.LogUtils oLU;
        static string cfg_logfilename;
        static string cfg_level1users;

        static int miCurYear;
        static int miCurYear1;
        static int miCurYear2;
        static int miCurYear3;
        static int miCurYear4;
        static int miCurYear5;
        static int miCurYear6;

        static int miCurYearCount;
        static int miCurYear1Count;
        static int miCurYear2Count;
        static int miCurYear3Count;
        static int miCurYear4Count;
        static int miCurYear5Count;
        static int miCurYear6Count;

        static decimal mdecCurYearAmount;
        static decimal mdecCurYear1Amount;
        static decimal mdecCurYear2Amount;
        static decimal mdecCurYear3Amount;
        static decimal mdecCurYear4Amount;
        static decimal mdecCurYear5Amount;
        static decimal mdecCurYear6Amount;

        static string msUserID;
        static bool mbLevel1User;

        protected void Page_Load(object sender, EventArgs e)
        {

            string sStatusTime = "";
            int iOver30 = 0;
            int iOpen = 0;
            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            DateTime dNow = DateTime.Today;
            DateTime dYesterday = dNow.AddDays(-1);
            DateTime dBegOfWeek = FirstDayofWeek(dNow);
            DateTime dBegOfMonth = FirstDayOfMonth(dNow);
            DateTime dEndOfMonth = LastDayOfMonth(dNow);

            int iTotOpen = 0;
            int iTotPReview = 0;
            int iTotPAssign = 0;
            int iTotU30 = 0;
            int iTot30_59 = 0;
            int iTot60_89 = 0;
            int iTot90_120 = 0;
            int iTotOver120 = 0;

            int iTotDNew = 0;
            int iTotDComp = 0;
            decimal dTotDBilled = 0;
            decimal dTotDPaid = 0;
            int iTotDReturned = 0;
            int iTotDTraced = 0;

            int iTotWNew = 0;
            int iTotWComp = 0;
            decimal dTotWBilled = 0;
            decimal dTotWPaid = 0;
            int iTotWReturned = 0;
            int iTotWTraced = 0;

            int iTotMNew = 0;
            int iTotMComp = 0;
            decimal dTotMBilled = 0;
            decimal dTotMPaid = 0;
            int iTotMReturned = 0;
            int iTotMTraced = 0;

            double dMonthComp = 0;
            int iTemp = 0;
            decimal dTemp = 0;

            try
            {

                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;

                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_SQLMainSIBUtilConnStr = colNameVal.Get("SQLMainSIBUtilConnStr");
                cfg_360ConnStr = colNameVal.Get("360ConnStr");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_level1users = colNameVal.Get("level1users");

                // get User ID
                msUserID = getUser();
                msUserID = msUserID.ToLower();

                // Show/Hide menus based on user id
                mbLevel1User = cfg_level1users.Contains(msUserID.ToLower());

                if (mbLevel1User)
                {
                    pnlMenu.Visible = true;
                }
                else
                {
                    pnlMenu.Visible = false;
                }

                // set up SQL connection
                sqlConn1 = new SqlConnection(cfg_SQLMainSIBUtilConnStr);
                sqlCmd1 = new SqlCommand();
                SqlDataReader sqlDR = null;

                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT statustime, opencases, pendingreview, pendingassign, openunder30, open30to59, open60to89, open90to120, openover120, oldestopen";
                sqlCmd1.CommandText += " FROM Dashboard WHERE statusdate = '" + DateTime.Today.ToString() + "'";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    sqlDR.Read();

                    if (sqlDR.IsDBNull(0))
                    {
                        sStatusTime = "N/A";
                    }
                    else
                    {
                        lblCurrStatProcTime.Text = DateTime.Today.ToShortDateString() + " " + sqlDR.GetString(0);
                    }

                    if (sqlDR.IsDBNull(1))
                    {
                        lblOpenCases.Text = "?";
                    }
                    else
                    {
                        iOpen = (int)sqlDR.GetInt32(1);
                        lblOpenCases.Text = iOpen.ToString("#,0");
                        iTotOpen = iOpen;
                    }

                    if (sqlDR.IsDBNull(2))
                    {
                        lblPendReview.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(2);
                        lblPendReview.Text = iTemp.ToString("#,0");
                        iTotPReview = iTemp;
                    }

                    if (sqlDR.IsDBNull(3))
                    {
                        lblPendAssign.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(3);
                        lblPendAssign.Text = iTemp.ToString("#,0");
                        iTotPAssign = iTemp;
                    }

                    if (sqlDR.IsDBNull(4))
                    {
                        lblUnder30.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(4);
                        lblUnder30.Text = iTemp.ToString("#,0");
                        iTotU30 = iTemp;
                    }

                    if (sqlDR.IsDBNull(5))
                    {
                        lbl3059.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(5);
                        lbl3059.Text = iTemp.ToString("#,0");
                        iOver30 = iTemp;
                        iTot30_59 = iTemp;

                    }

                    if (sqlDR.IsDBNull(6))
                    {
                        lbl6089.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(6);
                        lbl6089.Text = iTemp.ToString("#,0");
                        iOver30 += iTemp;
                        iTot60_89 = iTemp;
                    }

                    if (sqlDR.IsDBNull(7))
                    {
                        lbl90120.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(7);
                        lbl90120.Text = iTemp.ToString("#,0");
                        iOver30 += iTemp;
                        iTot90_120 = iTemp;
                    }

                    if (sqlDR.IsDBNull(8))
                    {
                        lblOver120.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(8);
                        lblOver120.Text = iTemp.ToString("#,0");
                        iOver30 += iTemp;
                        iTotOver120 = iTemp;
                    }

                    if (sqlDR.IsDBNull(9))
                    {
                        lblOldest.Text = "?";
                    }
                    else
                    {
                        lblOldest.Text = sqlDR.GetInt32(9).ToString();
                    }


                    sqlDR.Close();

                }

                // no data 
                else
                {
                    sqlConn1.Close();
                    throw new SystemException("No data returned by Dashboard query");
                }

                // Daily
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT statusdate, statustime, newcases, completed, returneditems, traceditems, amtinvoiced, insppaid";
                sqlCmd1.CommandText += " FROM Dashboard_Stats WHERE stattype='D' AND statusperiod = '" + DateTime.Today.ToString() + "'";
                sqlCmd1.Connection = sqlConn1;

                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    DateTime dProcDate;


                    sqlDR.Read();

                    if (sqlDR.IsDBNull(0))
                    {
                        lblTodayProcTime.Text = "??";
                    }
                    else
                    {
                        dProcDate = sqlDR.GetDateTime(0);
                        lblTodayProcTime.Text = dProcDate.ToShortDateString();
                    }

                    if (sqlDR.IsDBNull(1))
                    {
                        sStatusTime = "??";
                    }
                    else
                    {
                        lblTodayProcTime.Text += " " + sqlDR.GetString(1);
                    }

                    if (sqlDR.IsDBNull(2))
                    {
                        lblDailyNew.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(2);
                        lblDailyNew.Text = iTemp.ToString("#,0");
                        iTotDNew = iTemp;
                    }
                    if (sqlDR.IsDBNull(3))
                    {
                        lblDailyComp.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(3);
                        lblDailyComp.Text = iTemp.ToString("#,0");
                        iTotDComp = iTemp;
                    }
                    if (sqlDR.IsDBNull(4))
                    {
                        lblDailyReturned.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(4);
                        lblDailyReturned.Text = iTemp.ToString("#,0");
                        iTotDReturned = iTemp;
                    }
                    if (sqlDR.IsDBNull(5))
                    {
                        lblDailyTraced.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(5);
                        lblDailyTraced.Text = iTemp.ToString("#,0");
                        iTotDTraced = iTemp;
                    }
                    if (sqlDR.IsDBNull(6))
                    {
                        lblDailyBilled.Text = "?";
                    }
                    else
                    {
                        lblDailyBilled.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(6));
                        dTotDBilled = (decimal)sqlDR.GetSqlMoney(6);
                    }
                    if (sqlDR.IsDBNull(7))
                    {
                        lblDailyPaid.Text = "?";
                    }
                    else
                    {
                        lblDailyPaid.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(7));
                        dTotDPaid = (decimal)sqlDR.GetSqlMoney(7);
                    }


                    sqlDR.Close();



                }

                // no data 
                else
                {
                    sqlConn1.Close();
                    throw new SystemException("No data returned from Dashboard_Stats - Daily");
                }

                // Week
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT statusperiod, statustime, newcases, completed, returneditems, traceditems, amtinvoiced, insppaid";
                sqlCmd1.CommandText += " FROM Dashboard_Stats WHERE stattype='W' AND statusperiod = '" + dBegOfWeek.ToString() + "'";
                sqlCmd1.Connection = sqlConn1;

                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    DateTime dProcDate;


                    sqlDR.Read();

                    if (sqlDR.IsDBNull(0))
                    {
                        lblWeekProcTime.Text = "??";
                    }
                    else
                    {
                        dProcDate = sqlDR.GetDateTime(0);
                        lblWeekProcTime.Text = "Beg: " + dProcDate.ToShortDateString();
                    }

                    if (sqlDR.IsDBNull(2))
                    {
                        lblWeekNew.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(2);
                        lblWeekNew.Text = iTemp.ToString("#,0");
                        iTotWNew = iTemp;
                    }
                    if (sqlDR.IsDBNull(3))
                    {
                        lblWeekComp.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(3);
                        lblWeekComp.Text = iTemp.ToString("#,0");
                        iTotWComp = iTemp;
                    }
                    if (sqlDR.IsDBNull(4))
                    {
                        lblWeekReturned.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(4);
                        lblWeekReturned.Text = iTemp.ToString("#,0");
                        iTotWReturned = iTemp;
                    }
                    if (sqlDR.IsDBNull(5))
                    {
                        lblWeekTraced.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(5);
                        lblWeekTraced.Text = iTemp.ToString("#,0");
                        iTotWTraced = iTemp;
                    }
                    if (sqlDR.IsDBNull(6))
                    {
                        lblWeekBilled.Text = "?";
                    }
                    else
                    {
                        lblWeekBilled.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(6));
                        dTotWBilled = (decimal)sqlDR.GetSqlMoney(6);
                    }
                    if (sqlDR.IsDBNull(7))
                    {
                        lblWeekPaid.Text = "?";
                    }
                    else
                    {
                        lblWeekPaid.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(7));
                        dTotWPaid = (decimal)sqlDR.GetSqlMoney(7);
                    }
                    sqlDR.Close();

                }

                // no data 
                else
                {
                    sqlConn1.Close();
                    throw new SystemException("No data returned from Dashboard_Stats - Weekly");
                }

                // Month
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT statusperiod, statustime, newcases, completed, returneditems, traceditems, amtinvoiced, insppaid";
                sqlCmd1.CommandText += " FROM Dashboard_Stats WHERE stattype='M' AND statusperiod = '" + dBegOfMonth.ToString() + "'";
                sqlCmd1.Connection = sqlConn1;

                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    DateTime dProcDate;


                    sqlDR.Read();

                    if (sqlDR.IsDBNull(0))
                    {
                        lblMonthProcTime.Text = "??";
                    }
                    else
                    {
                        dProcDate = sqlDR.GetDateTime(0);
                        lblMonthProcTime.Text = "Beg: " + dProcDate.ToShortDateString();
                    }

                    if (sqlDR.IsDBNull(2))
                    {
                        lblMonthNew.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(2);
                        lblMonthNew.Text = iTemp.ToString("#,0");
                        iTotMNew = iTemp;
                    }
                    if (sqlDR.IsDBNull(3))
                    {
                        lblMonthComp.Text = "?";
                    }
                    else
                    {
                        dMonthComp = Convert.ToDouble(sqlDR.GetInt32(3));
                        iTemp = sqlDR.GetInt32(3);
                        lblMonthComp.Text = iTemp.ToString("#,0");
                        iTotMComp = iTemp;
                    }
                    if (sqlDR.IsDBNull(4))
                    {
                        lblMonthReturned.Text = "?";
                    }
                    else
                    {
                        iTemp =  sqlDR.GetInt32(4);
                        lblMonthReturned.Text = iTemp.ToString("#,0");
                        iTotMReturned = iTemp;
                    }
                    if (sqlDR.IsDBNull(5))
                    {
                        lblMonthTraced.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(5);
                        lblMonthTraced.Text = iTemp.ToString("#,0");
                        iTotMTraced = iTemp;
                    }
                    if (sqlDR.IsDBNull(6))
                    {
                        lblMonthBilled.Text = "?";
                    }
                    else
                    {
                        lblMonthBilled.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(6));
                        dTotMBilled = (decimal)sqlDR.GetSqlMoney(6);
                    }
                    if (sqlDR.IsDBNull(7))
                    {
                        lblMonthPaid.Text = "?";
                    }
                    else
                    {
                        lblMonthPaid.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(7));
                        dTotMPaid = (decimal)sqlDR.GetSqlMoney(7);
                    }


                    sqlDR.Close();


                }

                // no data 
                else
                {
                    sqlConn1.Close();
                    throw new SystemException("No data returned from Dashboard_Stats - Monthly");
                }

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_Count_Completed_LT_30";
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);

                double dComp29SIB = Convert.ToDouble(sqlCmd1.ExecuteScalar());
                lblMonthComp290rLess.Text = dComp29SIB.ToString("#,0");

                if (dMonthComp > 0)
                {
                    double dPct = (dComp29SIB / dMonthComp);
                    lblMonthComp290rLessPct.Text = dPct.ToString("##0.00%");
                }

                sqlCmd1.Parameters.Clear();
                sqlCmd1.CommandText = "sp_Count_AVG_DaysToComp";
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);

                int iAvgToCompSIB = (int)(sqlCmd1.ExecuteScalar());
                lblMonthAvgTimeSvc.Text = iAvgToCompSIB.ToString("#,0");

                sqlCmd1.Parameters.Clear();
                sqlCmd1.CommandText = "sp_Dashboard_CountHold";

                int iOnHold = (int)(sqlCmd1.ExecuteScalar());
                lblOnHold.Text = iOnHold.ToString("#,0");
                
                loadSalesHist();
                setupRequestHistChart();
                setupWeeklyNewCompChart();

                /////////
                // 360 //
                /////////

                // set up SQL connection
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT statustime, opencases, pendingreview, pendingassign, openunder30, open30to59, open60to89, open90to120, openover120, oldestopen";
                sqlCmd1.CommandText += " FROM Dashboard WHERE statusdate = '" + DateTime.Today.ToString() + "'";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    sqlDR.Read();

                    if (sqlDR.IsDBNull(1))
                    {
                        lbl360OpenCases.Text = "?";
                    }
                    else
                    {
                        iTemp = (int)sqlDR.GetInt32(1);
                        lbl360OpenCases.Text = iTemp.ToString("#,0");
                        iOpen += iTemp;
                        iTotOpen += iTemp;
                    }

                    if (sqlDR.IsDBNull(2))
                    {
                        lbl360PendReview.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(2);
                        lbl360PendReview.Text = iTemp.ToString("#,0");
                        iTotPReview += iTemp;
                    }

                    if (sqlDR.IsDBNull(3))
                    {
                        lbl360PendAssign.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(3);
                        lbl360PendAssign.Text = iTemp.ToString("#,0");
                        iTotPAssign += iTemp;
                    }

                    if (sqlDR.IsDBNull(4))
                    {
                        lbl360Under30.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(4);
                        lbl360Under30.Text = iTemp.ToString("#,0");
                        iTotU30 += iTemp;
                    }

                    if (sqlDR.IsDBNull(5))
                    {
                        lbl3603059.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(5);
                        lbl3603059.Text = iTemp.ToString("#,0");
                        iOver30 += iTemp;
                        iTot30_59 += iTemp;

                    }

                    if (sqlDR.IsDBNull(6))
                    {
                        lbl3606089.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(6);
                        lbl3606089.Text = iTemp.ToString("#,0");
                        iOver30 += iTemp;
                        iTot60_89 += iTemp;
                    }

                    if (sqlDR.IsDBNull(7))
                    {
                        lbl36090120.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(7);
                        lbl36090120.Text = iTemp.ToString("#,0");
                        iOver30 += iTemp;
                        iTot90_120 += iTemp;
                    }

                    if (sqlDR.IsDBNull(8))
                    {
                        lbl360Over120.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(8);
                        lbl360Over120.Text = iTemp.ToString("#,0");
                        iOver30 += iTemp;
                        iTotOver120 += iTemp;
                    }

                    if (sqlDR.IsDBNull(9))
                    {
                        lbl360Oldest.Text = "?";
                    }
                    else
                    {
                       lbl360Oldest.Text = sqlDR.GetInt32(9).ToString();
                    }


                    sqlDR.Close();

                    lblTotOpenCases.Text = iTotOpen.ToString("#,0");
                    lblTotPendReview.Text = iTotPReview.ToString("#,0");
                    lblTotPendAssign.Text = iTotPAssign.ToString("#,0");
                    lblTotUnder30.Text = iTotU30.ToString("#,0");
                    lblTot3059.Text = iTot30_59.ToString("#,0");
                    lblTot6089.Text = iTot60_89.ToString("#,0");
                    lblTot90120.Text = iTot90_120.ToString("#,0");
                    lblTotOver120.Text = iTotOver120.ToString("#,0");
                    
                    if (iOver30 > 0 && iOpen > 0)
                    {
                        float iPctOld = ((float)iOver30 / (float)iOpen);
                        lblPctOld.Text = iPctOld.ToString("#.##%");

                    }


                }

                // no data 
                else
                {
                    sqlConn1.Close();
                    throw new SystemException("No data returned by Dashboard query");
                }

                // Daily
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT statusdate, statustime, newcases, completed, returneditems, traceditems, amtinvoiced, insppaid";
                sqlCmd1.CommandText += " FROM Dashboard_Stats WHERE stattype='D' AND statusperiod = '" + DateTime.Today.ToString() + "'";
                sqlCmd1.Connection = sqlConn1;

                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    //DateTime dProcDate;


                    sqlDR.Read();

                    //if (sqlDR.IsDBNull(0))
                    //{
                    //    lblTodayProcTime.Text = "??";
                    //}
                    //else
                    //{
                    //    dProcDate = sqlDR.GetDateTime(0);
                    //    lblTodayProcTime.Text = dProcDate.ToShortDateString();
                    //}

                    //if (sqlDR.IsDBNull(1))
                    //{
                    //    sStatusTime = "??";
                    //}
                    //else
                    //{
                    //    lblTodayProcTime.Text += " " + sqlDR.GetString(1);
                    //}

                    if (sqlDR.IsDBNull(2))
                    {
                        lbl360DailyNew.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(2);
                        lbl360DailyNew.Text = iTemp.ToString("#,0");
                        iTotDNew += iTemp;
                    }
                    if (sqlDR.IsDBNull(3))
                    {
                        lbl360DailyComp.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(3);
                        lbl360DailyComp.Text = iTemp.ToString("#,0");
                        iTotDComp += iTemp;
                    }
                    if (sqlDR.IsDBNull(4))
                    {
                        lbl360DailyReturned.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(4);
                        lbl360DailyReturned.Text = iTemp.ToString("#,0");
                        iTotDReturned += iTemp;
                    }
                    if (sqlDR.IsDBNull(5))
                    {
                        lbl360DailyTraced.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(5);
                        lbl360DailyTraced.Text = "NA";
                        iTotDTraced += iTemp;
                    }
                    if (sqlDR.IsDBNull(6))
                    {
                        lbl360DailyBilled.Text = "?";
                    }
                    else
                    {
                        lbl360DailyBilled.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(6));
                        dTotDBilled += (decimal)sqlDR.GetSqlMoney(6);
                    }

                    if (sqlDR.IsDBNull(7))
                    {
                        lbl360DailyPaid.Text = "?";
                    }
                    else
                    {
                        lbl360DailyPaid.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(7));
                        dTotDPaid += (decimal)sqlDR.GetSqlMoney(7);
                    }


                    sqlDR.Close();

                    lblTotDailyNew.Text = iTotDNew.ToString("#,0");
                    lblTotDailyComp.Text = iTotDComp.ToString("#,0");
                    lblTotDailyBilled.Text = String.Format("{0:C}", dTotDBilled);
                    lblTotDailyReturned.Text = iTotDReturned.ToString("#,0");
                    lblTotDailyTraced.Text = iTotDTraced.ToString("#,0");
                    lblTotDailyPaid.Text = String.Format("{0:C}", dTotDPaid);

                    if (iTotDReturned > 0 && iTotDComp > 0)
                    { 
                        dTemp = (decimal)iTotDReturned / iTotDComp;
                        lblDailyRejectPct.Text = String.Format("{0:P1}", dTemp);
                        dTemp = dTemp * 100;
                        if (dTemp > 10 && dTemp < 15)
                        {
                            lblDailyRejectPct.BackColor = System.Drawing.Color.Yellow;
                        }
                        else if (dTemp > 15)
                        {
                            lblDailyRejectPct.BackColor = System.Drawing.Color.Red;
                            lblDailyRejectPct.ForeColor = System.Drawing.Color.White;
                        }

                    }

                }

                // no data 
                else
                {
                    sqlConn1.Close();
                    throw new SystemException("No data returned from Dashboard_Stats - Daily");
                }

                // Week
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT statusperiod, statustime, newcases, completed, returneditems, traceditems, amtinvoiced, insppaid";
                sqlCmd1.CommandText += " FROM Dashboard_Stats WHERE stattype='W' AND statusperiod = '" + dBegOfWeek.ToString() + "'";
                sqlCmd1.Connection = sqlConn1;

                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    //DateTime dProcDate;


                    sqlDR.Read();

                    //if (sqlDR.IsDBNull(0))
                    //{
                    //    lblWeekProcTime.Text = "??";
                    //}
                    //else
                    //{
                    //    dProcDate = sqlDR.GetDateTime(0);
                    //    lblWeekProcTime.Text = "Beg: " + dProcDate.ToShortDateString();
                    //}

                    if (sqlDR.IsDBNull(2))
                    {
                        lbl360WeekNew.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(2);
                        lbl360WeekNew.Text = iTemp.ToString("#,0");
                        iTotWNew += iTemp;
                    }
                    if (sqlDR.IsDBNull(3))
                    {
                        lbl360WeekComp.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(3);
                        lbl360WeekComp.Text = iTemp.ToString("#,0");
                        iTotWComp += iTemp;
                    }
                    if (sqlDR.IsDBNull(4))
                    {
                        lbl360WeekReturned.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(4);
                        lbl360WeekReturned.Text = iTemp.ToString("#,0");
                        iTotWReturned += iTemp;
                    }
                    if (sqlDR.IsDBNull(5))
                    {
                        lbl360WeekTraced.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(5);
                        lbl360WeekTraced.Text = "NA";
                        iTotWTraced += iTemp;
                    }
                    if (sqlDR.IsDBNull(6))
                    {
                        lbl360WeekBilled.Text = "?";
                    }
                    else
                    {
                        lbl360WeekBilled.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(6));
                        dTotWBilled += (decimal)sqlDR.GetSqlMoney(6);
                    }
                    if (sqlDR.IsDBNull(7))
                    {
                        lbl360WeekPaid.Text = "?";
                    }
                    else
                    {
                        lbl360WeekPaid.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(7));
                        dTotWPaid += (decimal)sqlDR.GetSqlMoney(7);
                    }

                    sqlDR.Close();

                    lblTotWeekNew.Text = iTotWNew.ToString("#,0");
                    lblTotWeekComp.Text = iTotWComp.ToString("#,0");
                    lblTotWeekBilled.Text = String.Format("{0:C}", dTotWBilled);
                    lblTotWeekPaid.Text = String.Format("{0:C}", dTotWPaid);
                    lblTotWeekReturned.Text = iTotWReturned.ToString("#,0");
                    lblTotWeekTraced.Text = iTotWTraced.ToString("#,0");
                    
                    if (iTotWReturned > 0 && iTotWComp > 0)
                    {
                        dTemp = (decimal)iTotWReturned / iTotWComp;
                        lblWeeklyRejectPct.Text = String.Format("{0:P1}", dTemp);
                        dTemp = dTemp * 100;
                        if (dTemp > 10 && dTemp < 15)
                        {
                            lblWeeklyRejectPct.BackColor = System.Drawing.Color.Yellow;
                        }
                        else if (dTemp > 15)
                        {
                            lblWeeklyRejectPct.BackColor = System.Drawing.Color.Red;
                            lblWeeklyRejectPct.ForeColor = System.Drawing.Color.White;
                        }

                    }


                }

                // no data 
                else
                {
                    sqlConn1.Close();
                    throw new SystemException("No data returned from Dashboard_Stats - Weekly");
                }

                // Month
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT statusperiod, statustime, newcases, completed, returneditems, traceditems, amtinvoiced, insppaid";
                sqlCmd1.CommandText += " FROM Dashboard_Stats WHERE stattype='M' AND statusperiod = '" + dBegOfMonth.ToString() + "'";
                sqlCmd1.Connection = sqlConn1;

                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    //DateTime dProcDate;


                    sqlDR.Read();

                    //if (sqlDR.IsDBNull(0))
                    //{
                    //    lblMonthProcTime.Text = "??";
                    //}
                    //else
                    //{
                    //    dProcDate = sqlDR.GetDateTime(0);
                    //    lblMonthProcTime.Text = "Beg: " + dProcDate.ToShortDateString();
                    //}

                    if (sqlDR.IsDBNull(2))
                    {
                        lbl360MonthNew.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(2);
                        lbl360MonthNew.Text = iTemp.ToString("#,0");
                        iTotMNew += iTemp;
                    }
                    if (sqlDR.IsDBNull(3))
                    {
                        lbl360MonthComp.Text = "?";
                    }
                    else
                    {
                        dMonthComp = Convert.ToDouble(sqlDR.GetInt32(3));
                        iTemp = sqlDR.GetInt32(3);
                        lbl360MonthComp.Text = iTemp.ToString("#,0");
                        iTotMComp += iTemp;
                    }
                    if (sqlDR.IsDBNull(4))
                    {
                        lbl360MonthReturned.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(4);
                        lbl360MonthReturned.Text = iTemp.ToString("#,0");
                        iTotMReturned += iTemp;
                    }
                    if (sqlDR.IsDBNull(5))
                    {
                        lbl360MonthTraced.Text = "?";
                    }
                    else
                    {
                        iTemp = sqlDR.GetInt32(5);
                        lbl360MonthTraced.Text = "NA";
                        iTotMTraced += iTemp;
                    }
                    if (sqlDR.IsDBNull(6))
                    {
                        lbl360MonthBilled.Text = "?";
                    }
                    else
                    {
                        lbl360MonthBilled.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(6));
                        dTotMBilled += (decimal)sqlDR.GetSqlMoney(6);
                    }
                    if (sqlDR.IsDBNull(7))
                    {
                        lbl360MonthPaid.Text = "?";
                    }
                    else
                    {
                        lbl360MonthPaid.Text = String.Format("{0:C}", (decimal)sqlDR.GetSqlMoney(7));
                        dTotMPaid += (decimal)sqlDR.GetSqlMoney(7);
                    }


                    sqlDR.Close();

                    lblTotMonthNew.Text = iTotMNew.ToString("#,0");
                    lblTotMonthComp.Text = iTotMComp.ToString("#,0");
                    lblTotMonthBilled.Text = String.Format("{0:C}", dTotMBilled);
                    lblTotMonthPaid.Text = String.Format("{0:C}", dTotMPaid);
                    lblTotMonthReturned.Text = iTotMReturned.ToString("#,0");
                    lblTotMonthTraced.Text = iTotMTraced.ToString("#,0");

                    if (iTotMReturned > 0 && iTotMComp > 0)
                    {
                        dTemp = (decimal)iTotMReturned / iTotMComp;
                        lblMonthRejectPct.Text = String.Format("{0:P1}", dTemp);
                        dTemp = dTemp * 100;
                        if (dTemp > 10 && dTemp < 15)
                        {
                            lblMonthRejectPct.BackColor = System.Drawing.Color.Yellow;
                        }
                        else if (dTemp > 15)
                        {
                            lblMonthRejectPct.BackColor = System.Drawing.Color.Red;
                            lblMonthRejectPct.ForeColor = System.Drawing.Color.White;
                        }

                    }

                }

                // no data 
                else
                {
                    sqlConn1.Close();
                    throw new SystemException("No data returned from Dashboard_Stats - Monthly");
                }


                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_Count_CompLT30";
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);

                double dComp29360 = Convert.ToDouble(sqlCmd1.ExecuteScalar());
                lblMonthComp290rLess360.Text = dComp29360.ToString("#,0");

                if (dMonthComp > 0)
                {
                    double dPct = (dComp29360 / dMonthComp);
                    lblMonthComp290rLessPct360.Text = dPct.ToString("##0.00%");
                }

                sqlCmd1.Parameters.Clear();
                sqlCmd1.CommandText = "sp_Count_AVG_DaysToComp";
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);

                int iAvgToCompSIB360 = (int)(sqlCmd1.ExecuteScalar());
                lblMonthAvgTimeSvc360.Text = iAvgToCompSIB360.ToString("#,0");

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.CommandText = "sp_Count_CustReject";
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);

                int i360CustRejects = Convert.ToInt32(sqlCmd1.ExecuteScalar());
                lbl360CustRejects.Text = i360CustRejects.ToString("#,0");



            }
            catch (Exception ex)
            {

                logError(ex.Message);

            }

            finally
            {
                sqlConn1.Close();
            
            }
            
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }




        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        private void setupRequestHistChart()
        {

            Xceed.Chart.Core.Chart chart;
            BarSeries bar;
            chart = (Chart)xcReqHist.Charts[0];
            chart.MarginMode = MarginMode.Stretch;
            chart.Margins = new RectangleF(12, 10, 80, 80);
            //chart.Width = 800;
            //chart.Height = 600;

            bar = (BarSeries)chart.Series.Add(SeriesType.Bar);
            bar.Add(miCurYear6Count, miCurYear6.ToString(), new FillEffect(Color.Azure));
            bar.Add(miCurYear5Count, miCurYear5.ToString(), new FillEffect(Color.Cyan));
            bar.Add(miCurYear4Count, miCurYear4.ToString(), new FillEffect(Color.Yellow));
            bar.Add(miCurYear3Count, miCurYear3.ToString(), new FillEffect(Color.Red));
            bar.Add(miCurYear2Count, miCurYear2.ToString(), new FillEffect(Color.Orange));
            bar.Add(miCurYear1Count, miCurYear1.ToString(), new FillEffect(Color.Blue));
            bar.Add(miCurYearCount,miCurYear.ToString(), new FillEffect(Color.Green));

            bar.BarStyle = BarStyle.SmoothEdgeBar;
            bar.Appearance.FillMode = AppearanceFillMode.DataPoints;
            bar.Legend.Mode = SeriesLegendMode.None;
            bar.DataLabels.Format = "<value>";
            bar.DataLabels.Text.Backplane.Visible = false;
            bar.DataLabels.ArrowLength = 5;
            bar.DataLabels.ArrowLine.Color = Color.White;

            // disable automatic labels
            chart.Axis(StandardAxis.PrimaryX).DimensionScale.AutoLabels = false;

            // add custom labels to be displayed on major tick values
            chart.Axis(StandardAxis.PrimaryX).Labels.Clear();
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear6.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear5.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear4.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear3.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear2.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear1.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear.ToString());

            // enable the antialiasing of the whole scene
            xcReqHist.Settings.EnableJittering = true;

            // add an image border
            xcReqHist.Background.FrameType = FrameType.Image;
            ImageFrame imageFrame = xcReqHist.Background.ImageFrame;

            imageFrame.SetPredefinedFrameStyle(PredefinedImageFrame.Thin);
            imageFrame.FillEffect.SetSolidColor(Color.SlateGray);
            imageFrame.BackgroundColor = Color.White;

            // set gradient filling for the background
            xcReqHist.Background.FillEffect.SetGradient(GradientStyle.Horizontal, GradientVariant.Variant1, Color.White, Color.LightSteelBlue);
            xcReqHist.UpdateFromDataSources();
            
        }
  
        private void setupWeeklyNewCompChart()
        {

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlDR1 = null;

            SqlConnection sqlConn360 = null;
            SqlCommand sqlCmd360 = null;
            SqlDataReader sqlDR360 = null;


            try
            {



                xcWeeklyNewComp.Settings.RenderDevice = RenderDevice.GDI;
                xcWeeklyNewComp.Background.FillEffect.SetGradient(GradientStyle.Horizontal, GradientVariant.Variant2, Color.White, Color.FromArgb(234, 234, 244));

                // image border
                xcWeeklyNewComp.Background.FrameType = FrameType.Image;
                ImageFrame imageFrame = xcWeeklyNewComp.Background.ImageFrame;

                imageFrame.SetPredefinedFrameStyle(PredefinedImageFrame.Thin);
                imageFrame.FillEffect.SetSolidColor(Color.SlateGray);
                imageFrame.BackgroundColor = Color.White;

                // set gradient filling for the background
                xcWeeklyNewComp.Background.FillEffect.SetGradient(GradientStyle.Horizontal, GradientVariant.Variant1, Color.White, Color.LightSteelBlue);
                
                // setup chart
                Xceed.Chart.Core.Chart chart = xcWeeklyNewComp.Charts[0];
                chart.MarginMode = MarginMode.Stretch;
                chart.Margins = new RectangleF(10, 10, 85, 70);

                chart.Axis(StandardAxis.PrimaryY).ScaleMode = AxisScaleMode.Numeric;
                chart.Axis(StandardAxis.PrimaryY).MajorGridLine.Pattern = LinePattern.Dot;
                chart.Axis(StandardAxis.PrimaryY).OuterTickLength = 1.5f;
                chart.Axis(StandardAxis.PrimaryY).MinorTickLength = 1;

                chart.Axis(StandardAxis.PrimaryX).ScaleMode = AxisScaleMode.DateTime;
                chart.Axis(StandardAxis.PrimaryX).MajorGridLine.Pattern = LinePattern.Dot;
                chart.Axis(StandardAxis.PrimaryX).OuterTickLength = 1.5f;
                chart.Axis(StandardAxis.PrimaryX).MinorTickLength = 1;

                LineSeries lsComp = (LineSeries)chart.Series.Add(SeriesType.Line);
                lsComp.Legend.Mode = SeriesLegendMode.None;
                lsComp.InflateMargins = false;
                lsComp.LineBorder.Color = Color.Navy;
                lsComp.Markers.Visible = true;
                lsComp.Markers.Border.Color = Color.Navy;
                lsComp.Markers.Style = PointStyle.Cylinder;
                lsComp.Markers.Width = 0.7f;
                lsComp.Markers.Height = 0.7f;
                lsComp.Markers.AutoDepth = true;
                lsComp.DataLabels.Mode = DataLabelsMode.None;

                LineSeries lsNew = (LineSeries)chart.Series.Add(SeriesType.Line);
                lsNew.Legend.Mode = SeriesLegendMode.None;
                lsNew.InflateMargins = false;
                lsNew.LineBorder.Color = Color.Maroon;
                lsNew.Markers.Visible = true;
                lsNew.Markers.Border.Color = Color.Maroon;
                lsNew.Markers.Style = PointStyle.Sphere;
                lsNew.Markers.Width = 0.7f;
                lsNew.Markers.Height = 0.7f;
                lsNew.Markers.AutoDepth = true;
                lsNew.DataLabels.Mode = DataLabelsMode.None;

                //StandardFrame fp = line.DataLabels.Text.Backplane.StandardFrame;
                //fp.Border.Color = Color.CornflowerBlue;

                Axis axis = chart.Axis(StandardAxis.PrimaryX);

                axis.StaggerTexts = true;

                axis.ValueFormatting.Format = Xceed.Chart.Utilities.ValueFormat.Date;
                axis.DateTimeScale.MajorTickMode = MajorTickModeDateTime.Months;
                axis.DateTimeScale.MonthsStep = 1;

                lsComp.UseXValues = true;
                lsNew.UseXValues = true;

                // Date range
                DateTime dBegDate = new DateTime();
                dBegDate = DateTime.Today;
                dBegDate = FirstDayofWeek(dBegDate);
                DateTime dEndDate = dBegDate.AddDays(-7);
                dBegDate = dBegDate.AddDays(-361);
                dBegDate = FirstDayofWeek(dBegDate);

                int iRow = 0;
                int iNew = 0;
                int iComp = 0;
                DateTime dWeek;

                int[] aiNew = new int[52];
                int[] aiComp = new int[52];
                DateTime[] adtWeek = new DateTime[52];
                
                // SIBOffice
                sqlConn1 = new SqlConnection(cfg_SQLMainSIBUtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT newcases, completed, statusperiod FROM Dashboard_Stats WHERE statusperiod BETWEEN '" + dBegDate + "' AND '" + dEndDate + "' AND stattype = 'W' ORDER BY statusperiod";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlDR1 = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR1.HasRows)
                {

                    sqlDR1.Read();

                    do
                    {

                        iNew = (int)sqlDR1.GetInt32(0);
                        iComp = (int)sqlDR1.GetInt32(1);
                        dWeek = sqlDR1.GetDateTime(2);
                        aiNew[iRow] = iNew;
                        aiComp[iRow] = iComp;
                        adtWeek[iRow] = dWeek;
                        iRow++;

                        //lsNew.Values.Add(iNew);
                        //lsComp.Values.Add(iComp);
                        //lsComp.XValues.Add(dWeek.ToOADate());
                        //lsNew.XValues.Add(dWeek.ToOADate());

                    } while (sqlDR1.Read());
                }

                sqlConn1.Close();

                // 360
                sqlConn360 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd360 = new SqlCommand();

                sqlCmd360.CommandType = CommandType.Text;
                sqlCmd360.CommandText = "SELECT newcases, completed, statusperiod FROM Dashboard_Stats WHERE statusperiod BETWEEN '" + dBegDate + "' AND '" + dEndDate + "' AND stattype = 'W' ORDER BY statusperiod";
                sqlCmd360.Connection = sqlConn360;
                sqlConn360.Open();

                sqlDR360 = sqlCmd360.ExecuteReader();

                // anythign returned?
                if (sqlDR360.HasRows)
                {
                    iRow = 0;
                    sqlDR360.Read();

                    do
                    {

                        iNew = (int)sqlDR360.GetInt32(0);
                        iComp = (int)sqlDR360.GetInt32(1);
                        dWeek = sqlDR360.GetDateTime(2);
                        aiNew[iRow] = aiNew[iRow] + iNew;
                        aiComp[iRow] = aiComp[iRow] + iComp;
    
                        if (adtWeek[iRow].Date != dWeek.Date)
                        {
                            throw new SystemException("Date mismatch loading NewComp chart");
                        }
                        iRow++;

                    } while (sqlDR360.Read());
                }

                sqlConn360.Close();

                for (int iWeek = 0; iWeek < 52; iWeek++)
                {

                    lsNew.Values.Add(aiNew[iWeek]);
                    lsComp.Values.Add(aiComp[iWeek]);
                    lsComp.XValues.Add(adtWeek[iWeek].ToOADate());
                    lsNew.XValues.Add(adtWeek[iWeek].ToOADate());                
                }


                //xcWeeklyNewComp.UpdateFromDataSources();

            }


            catch (Exception ex)
            {

                logError(ex.Message);
                Response.Redirect("SystemErr.aspx");

            }


        }

        private void loadSalesHist()
        {

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlDR1 = null;
            SqlConnection sqlConn360 = null;
            SqlCommand sqlCmd360 = null;
            SqlDataReader sqlDR360 = null;


            miCurYear = DateTime.Now.Year;
            miCurYear1 = miCurYear - 1;
            miCurYear2 = miCurYear - 2;
            miCurYear3 = miCurYear - 3;
            miCurYear4 = miCurYear - 4;
            miCurYear5 = miCurYear - 5;
            miCurYear6 = miCurYear - 6;

            miCurYear1Count = 0;
            miCurYear2Count = 0;
            miCurYear3Count = 0;
            miCurYear4Count = 0;
            miCurYear5Count = 0;
            miCurYear6Count = 0;

            mdecCurYear1Amount = 0;
            mdecCurYear2Amount = 0;
            mdecCurYear3Amount = 0;
            mdecCurYear4Amount = 0;
            mdecCurYear5Amount = 0;
            mdecCurYear6Amount = 0;

            DateTime dPeriodBeg;
            DateTime dPeriodEnd;
            DateTime dUpdated;
            
            try
            {

                sqlConn1 = new SqlConnection(cfg_SQLMainSIBUtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT * FROM Dashboard_Sales_Hist";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlDR1 = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR1.HasRows)
                {

                    sqlDR1.Read();

                    miCurYearCount = (int)sqlDR1.GetInt32(0);
                    mdecCurYearAmount = Math.Round(((decimal)sqlDR1.GetDecimal(1)* (decimal).001),2);
                    miCurYear1Count = (int)sqlDR1.GetInt32(2);
                    mdecCurYear1Amount = Math.Round(((decimal)sqlDR1.GetDecimal(3) * (decimal).001), 2);
                    miCurYear2Count = (int)sqlDR1.GetInt32(4);
                    mdecCurYear2Amount = Math.Round(((decimal)sqlDR1.GetDecimal(5) * (decimal).001), 2);
                    miCurYear3Count = (int)sqlDR1.GetInt32(6);
                    mdecCurYear3Amount = Math.Round(((decimal)sqlDR1.GetDecimal(7)*(decimal).001),2);
                    miCurYear4Count = (int)sqlDR1.GetInt32(8);
                    mdecCurYear4Amount = Math.Round(((decimal)sqlDR1.GetDecimal(9)*(decimal).001),2);
                    miCurYear5Count = (int)sqlDR1.GetInt32(10);
                    mdecCurYear5Amount = Math.Round(((decimal)sqlDR1.GetDecimal(11) * (decimal).001), 2);
                    miCurYear6Count = (int)sqlDR1.GetInt32(12);
                    mdecCurYear6Amount = Math.Round(((decimal)sqlDR1.GetDecimal(13) * (decimal).001), 2);
                    dUpdated = (DateTime)sqlDR1.GetSqlDateTime(14);
                    dPeriodBeg = (DateTime)sqlDR1.GetSqlDateTime(15);
                    dPeriodEnd = (DateTime)sqlDR1.GetSqlDateTime(16);
                    lblMTDReqHistGraphUpdate.Text = "Updated: " + dUpdated.ToString();
                    lblMTDReqHistGraphPd.Text = "Period: " + dPeriodBeg.ToShortDateString() +" - " + dPeriodEnd.ToShortDateString();
          
                                        
                }   // has rows

                sqlConn1.Close();
                
                // 360
                sqlConn360 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd360 = new SqlCommand();

                sqlCmd360.CommandType = CommandType.Text;
                sqlCmd360.CommandText = "SELECT * FROM Dashboard_Sales_Hist";
                sqlCmd360.Connection = sqlConn360;
                sqlConn360.Open();

                sqlDR360 = sqlCmd360.ExecuteReader();

                // anythign returned?
                if (sqlDR360.HasRows)
                {

                    sqlDR360.Read();

                    miCurYearCount = miCurYearCount + (int)sqlDR360.GetInt32(0);
                    mdecCurYearAmount = mdecCurYearAmount + Math.Round(((decimal)sqlDR360.GetDecimal(1) * (decimal).001), 2);
                    miCurYear1Count = miCurYear1Count + (int)sqlDR360.GetInt32(2);
                    mdecCurYear1Amount = mdecCurYear1Amount + Math.Round(((decimal)sqlDR360.GetDecimal(3) * (decimal).001), 2);
                    miCurYear2Count = miCurYear2Count + (int)sqlDR360.GetInt32(4);
                    mdecCurYear2Amount = mdecCurYear2Amount + Math.Round(((decimal)sqlDR360.GetDecimal(5) * (decimal).001), 2);
                    miCurYear3Count =  miCurYear3Count + (int)sqlDR360.GetInt32(6);
                    mdecCurYear3Amount = mdecCurYear3Amount + Math.Round(((decimal)sqlDR360.GetDecimal(7) * (decimal).001), 2);
                    miCurYear4Count = miCurYear4Count + (int)sqlDR360.GetInt32(8);
                    mdecCurYear4Amount = mdecCurYear4Amount + Math.Round(((decimal)sqlDR360.GetDecimal(9) * (decimal).001), 2);
                    miCurYear5Count = miCurYear5Count + (int)sqlDR360.GetInt32(10);
                    mdecCurYear5Amount = mdecCurYear5Amount + Math.Round(((decimal)sqlDR360.GetDecimal(11) * (decimal).001), 2);
                    mdecCurYear5Amount = mdecCurYear5Amount + (int)sqlDR360.GetInt32(12);
                    mdecCurYear6Amount = mdecCurYear6Amount + Math.Round(((decimal)sqlDR360.GetDecimal(13) * (decimal).001), 2);

                }   // has rows

                sqlConn360.Close();
                
            }

            catch (Exception ex)
            {

                logError(ex.Message);
                Response.Redirect("SystemErr.aspx");

            }

        }



        private void logError(string sErrMsg)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();

            //record exception  
            oLU.WritetoLog("Default.aspx - " + sErrMsg);

            // close objects
            oLU.closeLog();

        }

        private string getUser()
        {
            string sUserID = "";
            int iPos = 0;

            try
            {
                IPrincipal ip = HttpContext.Current.User;
                sUserID = ip.Identity.Name;
                if (sUserID.Length > 0 && sUserID.Contains("\\"))
                { 
                    iPos = sUserID.IndexOf("\\");
                    sUserID = sUserID.Substring(iPos + 1, sUserID.Length - (iPos+1));                
                }
            }
            catch (Exception ex)
            {
                logError(ex.Message);
                Response.Redirect("SystemErr.aspx");
            }

            return sUserID;
                    
        }

     }
}


