<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DashBoard._Default" %>

<%@ Register Assembly="Xceed.Chart.Server, Version=4.2.100.0, Culture=neutral, PublicKeyToken=ba83ff368b7563c6"
    Namespace="Xceed.Chart.Server" TagPrefix="xceedchart" %>
<%@ Register TagPrefix="uc1" TagName="SIBMiniHeader" Src="SIBMiniHeader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dashboard</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">

        function launchRevenue() {
            location.href = "revenueMain.aspx";
        }
        function launchClients() {
            location.href = "clientsMain.aspx";
        }
        function inactive() {
            alert("This function is not currently active.");
        }
        
    </script>
    <style type="text/css">
        .style1
        {
            width: 90%;
        }
    </style>
</head>
<body style="margin-top: 0; background-color: #fbfbfb;">
    <form id="frmDefault" runat="server">
    <div>
        <center>
            <table style="width: 900px; background-color: #fff">
                <tr align="center">
                    <td>
                        <uc1:SIBMiniHeader ID="SIBHeader1" runat="server"></uc1:SIBMiniHeader>
                        <asp:Panel ID="pnlMenu" runat="server" Style="position: relative; width: 80%">
                            <table id="tblTabHdr" cellspacing="0" cellpadding="0" width="100%">
                                <tr align="left" style="height: 25px">
                                    <td class="sel_menuitem" id="tabCurrent" title="Current stats" width="25%">
                                        Current
                                    </td>
                                    <td class="unsel_menuitem" id="tabRevenue" title="Revenue" onclick="launchRevenue()"
                                        width="25%">
                                        Revenue
                                    </td>
                                    <td class="unsel_menuitem" id="tabClients" title="Clients" onclick="launchClients()"
                                        width="25%">
                                        Clients
                                    </td>
                                    <td class="unsel_menuitem" id="tabInspectors" title="Inspectors" onclick="inactive()"
                                        width="25%">
                                        Inspectors
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <table id="Table1" style="background-color: #73929a; border-right: #d3d3d3 thin solid;
                            border-top: #d3d3d3 thin solid; border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid"
                            cellspacing="0" cellpadding="0" width="900" border="1" runat="server">
                            <tr style="background-color: #73929a">
                                <td style="width: 800" align="left">
                                    <table width="800">
                                        <tr>
                                            <td style="width: 80%; height: 25">
                                                <asp:Label ID="Label4" runat="server" CssClass="StdTextSmallWhiteSmallCap">
							        &nbsp;Current case load
                                                </asp:Label>
                                            </td>
                                            <td style="width: 20%; height: 25" align="right">
                                                <asp:Label ID="lblCurrStatProcTime" runat="server" CssClass="StdTextSmallWhite">
							        &nbsp;As of
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="background-color: #fbfbfb">
                                <td>
                                    <table cellspacing="0" cellpadding="2" width="100%" border="0">
                                        <tr>
                                            <td style="width: 10;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 90;" align="center">
                                                <asp:Label ID="Label2" runat="server" CssClass="StdTextSmallGrey">
							        Open cases
                                                </asp:Label>
                                            </td>
                                            <td style="width: 80;" align="center">
                                                <asp:Label ID="Label3" runat="server" CssClass="StdTextSmallGrey">
							        Pending<br />review
                                                </asp:Label>
                                            </td>
                                            <td style="width: 80;" align="center">
                                                <asp:Label ID="Label5" runat="server" CssClass="StdTextSmallGrey">
							        Pending<br />assign
                                                </asp:Label>
                                            </td>
                                            <td style="width: 80;" align="center">
                                                <asp:Label ID="Label6" runat="server" CssClass="StdTextSmallGrey">
							        Under 30
                                                </asp:Label>
                                            </td>
                                            <td style="width: 80;" align="center">
                                                <asp:Label ID="Label7" runat="server" CssClass="StdTextSmallGrey">
							        30 - 59
                                                </asp:Label>
                                            </td>
                                            <td style="width: 80;" align="center">
                                                <asp:Label ID="Label8" runat="server" CssClass="StdTextSmallGrey">
							        60 - 89
                                                </asp:Label>
                                            </td>
                                            <td style="width: 80;" align="center">
                                                <asp:Label ID="Label9" runat="server" CssClass="StdTextSmallGrey">
							        90 - 120
                                                </asp:Label>
                                            </td>
                                            <td style="width: 80;" align="center">
                                                <asp:Label ID="Label10" runat="server" CssClass="StdTextSmallGrey">
							        Over 120
                                                </asp:Label>
                                            </td>
                                            <td style="width: 80;" align="center">
                                                <asp:Label ID="Label11" runat="server" CssClass="StdTextSmallGrey">
							        Oldest<br />case
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                SIB
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="80px" BorderStyle="Solid" ID="lblOpenCases" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblPendReview" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblPendAssign" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblUnder30" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lbl3059" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lbl6089" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lbl90120" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblOver120" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblOldest" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                        </tr>
                                        <!--   -->
                                        <!--360-->
                                        <!--   -->
                                        <tr>
                                            <td align="center">
                                                360
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="80px" BorderStyle="Solid" ID="lbl360OpenCases" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lbl360PendReview" runat="server"
                                                    CssClass="StdTextSmallGrey" BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lbl360PendAssign" runat="server"
                                                    CssClass="StdTextSmallGrey" BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lbl360Under30" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lbl3603059" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lbl3606089" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lbl36090120" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lbl360Over120" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lbl360Oldest" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                        </tr>
                                        <!--   -->
                                        <!--Totals-->
                                        <!--   -->
                                        <tr>
                                            <td align="center">
                                                Total
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="80px" BorderStyle="Solid" ID="lblTotOpenCases" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblTotPendReview" runat="server"
                                                    CssClass="StdTextSmallGreyBold" BorderWidth="1px" BorderColor="LightSteelBlue"
                                                    Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblTotPendAssign" runat="server"
                                                    CssClass="StdTextSmallGreyBold" BorderWidth="1px" BorderColor="LightSteelBlue"
                                                    Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblTotUnder30" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblTot3059" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblTot6089" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblTot90120" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblTotOver120" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="right">
                                                <asp:Label ID="Label1" runat="server" CssClass="StdTextSmallGrey">Percent of cases over 29 days old:</asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblPctOld" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td colspan="2">
                                                &nbsp;
                                            </td>
                                            <td colspan="2" align="right">
                                                <asp:Label ID="Label38" runat="server" CssClass="StdTextSmallGrey">SIB items on hold:</asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="70px" BorderStyle="Solid" ID="lblOnHold" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <%-- /////////////////////// TODAY--%>
                        <table id="Table2" style="background-color: #73929a; border-right: #d3d3d3 thin solid;
                            border-top: #d3d3d3 thin solid; border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid"
                            cellspacing="0" cellpadding="0" width="900" border="1" runat="server">
                            <tr style="background-color: #73929a">
                                <td style="width: 900" align="left">
                                    <table width="900">
                                        <tr>
                                            <td style="width: 08%; height: 25">
                                                <asp:Label ID="Label16" runat="server" CssClass="StdTextSmallWhiteSmallCap">
							        &nbsp;Today
                                                </asp:Label>
                                            </td>
                                            <td style="width: 20%; height: 25" align="right">
                                                <asp:Label ID="lblTodayProcTime" runat="server" CssClass="StdTextSmallWhite">
							        &nbsp;As of
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="background-color: #fbfbfb">
                                <td>
                                    <table width="900" border="0">
                                        <tr>
                                            <td style="width: 10;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 128;" align="center">
                                                <asp:Label ID="Label14" runat="server" CssClass="StdTextSmallGrey">
							        New cases
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label15" runat="server" CssClass="StdTextSmallGrey">
							        Completed
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label30" runat="server" CssClass="StdTextSmallGrey">
							        Billed
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label17" runat="server" CssClass="StdTextSmallGrey">
							        Paid
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label19" runat="server" CssClass="StdTextSmallGrey">
							        Returned/QA Reject
                                                </asp:Label>
                                            </td>
                                            <td style="width: 128;" align="center">
                                                <asp:Label ID="Label20" runat="server" CssClass="StdTextSmallGrey">
							        Traced
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                SIB
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90px" BorderStyle="Solid" ID="lblDailyNew" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblDailyComp" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblDailyBilled" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblDailyPaid" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblDailyReturned" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblDailyTraced" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                        </tr>
                                        <!--   -->
                                        <!--360-->
                                        <!--   -->
                                        <tr>
                                            <td align="center">
                                                360
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90px" BorderStyle="Solid" ID="lbl360DailyNew" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360DailyComp" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360DailyBilled" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360DailyPaid" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360DailyReturned" runat="server"
                                                    CssClass="StdTextSmallGrey" BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360DailyTraced" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                        </tr>
                                        <!--      -->
                                        <!--Totals-->
                                        <!--      -->
                                        <tr>
                                            <td align="center">
                                                Total
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90px" BorderStyle="Solid" ID="lblTotDailyNew" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotDailyComp" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotDailyBilled" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotDailyPaid" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotDailyReturned" runat="server"
                                                    CssClass="StdTextSmallGreyBold" BorderWidth="1px" BorderColor="LightSteelBlue"
                                                    Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotDailyTraced" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                        </tr>
                                        <!-- Return/Reject % -->
                                        <tr>
                                            <td align="right" colspan="5">
                                                <asp:Label ID="lblDRetPct" runat="server" CssClass="StdTextSmallGrey">
							        Returned/QA Reject %:
                                                </asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblDailyRejectPct" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <%-- /////////////////////// WEEK --%>
                        <table id="Table3" style="background-color: #73929a; border-right: #d3d3d3 thin solid;
                            border-top: #d3d3d3 thin solid; border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid"
                            cellspacing="0" cellpadding="0" width="900" border="1" runat="server">
                            <tr style="background-color: #73929a">
                                <td style="width: 900" align="left">
                                    <table width="900">
                                        <tr>
                                            <td style="width: 560; height: 25">
                                                <asp:Label ID="Label21" runat="server" CssClass="StdTextSmallWhiteSmallCap">
							        &nbsp;This Week
                                                </asp:Label>
                                            </td>
                                            <td style="width: 200; height: 25" align="right">
                                                <asp:Label ID="lblWeekProcTime" runat="server" CssClass="StdTextSmallWhite">
							        &nbsp;As of
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="background-color: #fbfbfb">
                                <td>
                                    <table width="900" border="0">
                                        <tr>
                                            <td style="width: 10;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 128;" align="center">
                                                <asp:Label ID="Label23" runat="server" CssClass="StdTextSmallGrey">
							        New cases
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label24" runat="server" CssClass="StdTextSmallGrey">
							        Completed
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label31" runat="server" CssClass="StdTextSmallGrey">
							        Billed
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label34" runat="server" CssClass="StdTextSmallGrey">
							        Paid
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label25" runat="server" CssClass="StdTextSmallGrey">
							        Returned/QA Reject
                                                </asp:Label>
                                            </td>
                                            <td style="width: 128;" align="center">
                                                <asp:Label ID="Label26" runat="server" CssClass="StdTextSmallGrey">
							        Traced
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                SIB
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90px" BorderStyle="Solid" ID="lblWeekNew" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblWeekComp" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblWeekBilled" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblWeekPaid" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblWeekReturned" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblWeekTraced" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                        </tr>
                                        <!--   -->
                                        <!--360-->
                                        <!--   -->
                                        <tr>
                                            <td align="center">
                                                360
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90px" BorderStyle="Solid" ID="lbl360WeekNew" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360WeekComp" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360WeekBilled" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360WeekPaid" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360WeekReturned" runat="server"
                                                    CssClass="StdTextSmallGrey" BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360WeekTraced" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                        </tr>
                                        <!--      -->
                                        <!--Totals-->
                                        <!--      -->
                                        <tr>
                                            <td align="center">
                                                Total
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90px" BorderStyle="Solid" ID="lblTotWeekNew" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotWeekComp" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotWeekBilled" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotWeekPaid" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotWeekReturned" runat="server"
                                                    CssClass="StdTextSmallGreyBold" BorderWidth="1px" BorderColor="LightSteelBlue"
                                                    Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotWeekTraced" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                        </tr>
                                        <!-- Return/Reject % -->
                                        <tr>
                                            <td align="right" colspan="5">
                                                <asp:Label ID="Label36" runat="server" CssClass="StdTextSmallGrey">
							        Returned/QA Reject %:
                                                </asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblWeeklyRejectPct" runat="server"
                                                    CssClass="StdTextSmallGreyBold" BorderWidth="1px" BorderColor="LightSteelBlue"
                                                    Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <%-- /////////////////////// MONTH --%>
                        <table id="Table4" style="background-color: #73929a; border-right: #d3d3d3 thin solid;
                            border-top: #d3d3d3 thin solid; border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid"
                            cellspacing="0" cellpadding="0" width="900" border="1" runat="server">
                            <tr style="background-color: #73929a">
                                <td style="width: 900" align="left">
                                    <table width="900">
                                        <tr>
                                            <td style="width: 560; height: 25">
                                                <asp:Label ID="Label12" runat="server" CssClass="StdTextSmallWhiteSmallCap">
							        &nbsp;This Month
                                                </asp:Label>
                                            </td>
                                            <td style="width: 200; height: 25" align="right">
                                                <asp:Label ID="lblMonthProcTime" runat="server" CssClass="StdTextSmallWhite">
							        &nbsp;As of
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="background-color: #fbfbfb">
                                <td>
                                    <table width="900" border="0">
                                        <tr>
                                            <td style="width: 10;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 128;" align="center">
                                                <asp:Label ID="Label18" runat="server" CssClass="StdTextSmallGrey">
							        New cases
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label22" runat="server" CssClass="StdTextSmallGrey">
							        Completed
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label27" runat="server" CssClass="StdTextSmallGrey">Billed
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label35" runat="server" CssClass="StdTextSmallGrey">Paid
                                                </asp:Label>
                                            </td>
                                            <td style="width: 126;" align="center">
                                                <asp:Label ID="Label32" runat="server" CssClass="StdTextSmallGrey">
							        Returned/QA Reject
                                                </asp:Label>
                                            </td>
                                            <td style="width: 128;" align="center">
                                                <asp:Label ID="Label33" runat="server" CssClass="StdTextSmallGrey">
							        Traced
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                SIB
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90px" BorderStyle="Solid" ID="lblMonthNew" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="80" BorderStyle="Solid" ID="lblMonthComp" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="100" BorderStyle="Solid" ID="lblMonthBilled" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" ToolTip="Amount invoiced based on items completed this month.">
                                                </asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="100" BorderStyle="Solid" ID="lblMonthPaid" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" ToolTip="Amount invoiced based on items completed this month.">
                                                </asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblMonthReturned" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblMonthTraced" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                        </tr>
                                        <!--   -->
                                        <!--360-->
                                        <!--   -->
                                        <tr>
                                            <td align="center">
                                                360
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90px" BorderStyle="Solid" ID="lbl360MonthNew" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="80" BorderStyle="Solid" ID="lbl360MonthComp" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="100" BorderStyle="Solid" ID="lbl360MonthBilled" runat="server"
                                                    CssClass="StdTextSmallGrey" BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px">
                                                </asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="100" BorderStyle="Solid" ID="lbl360MonthPaid" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px">
                                                </asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360MonthReturned" runat="server"
                                                    CssClass="StdTextSmallGrey" BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lbl360MonthTraced" runat="server" CssClass="StdTextSmallGrey"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px"></asp:Label>
                                            </td>
                                        </tr>
                                        <!--     -->
                                        <!--Total-->
                                        <!--     -->
                                        <tr>
                                            <td align="center">
                                                Total
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90px" BorderStyle="Solid" ID="lblTotMonthNew" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="80" BorderStyle="Solid" ID="lblTotMonthComp" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="100" BorderStyle="Solid" ID="lblTotMonthBilled" runat="server"
                                                    CssClass="StdTextSmallGreyBold" BorderWidth="1px" BorderColor="LightSteelBlue"
                                                    Height="15px" BackColor="White">
                                                </asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="100" BorderStyle="Solid" ID="lblTotMonthPaid" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White">
                                                </asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotMonthReturned" runat="server"
                                                    CssClass="StdTextSmallGreyBold" BorderWidth="1px" BorderColor="LightSteelBlue"
                                                    Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblTotMonthTraced" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                        </tr>
                                        <!-- Return/Reject % -->
                                        <tr>
                                            <td align="right" colspan="5">
                                                <asp:Label ID="Label37" runat="server" CssClass="StdTextSmallGrey">
							        Returned/QA Reject %:
                                                </asp:Label>
                                            </td>
                                            <td align="center">
                                                <asp:Label Width="90" BorderStyle="Solid" ID="lblMonthRejectPct" runat="server" CssClass="StdTextSmallGreyBold"
                                                    BorderWidth="1px" BorderColor="LightSteelBlue" Height="15px" BackColor="White"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="StdTextSmallGrey" colspan='7'>
                                                360 Customer rejects:&nbsp;
                                                <asp:Label ID="lbl360CustRejects" runat="server" CssClass="StdTextSmallGrey"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="StdTextSmallGrey" colspan='4'>
                                                SIB: Reports completed in 29 days or less &nbsp;
                                                <asp:Label ID="lblMonthComp290rLess" runat="server" CssClass="StdTextSmallGrey"></asp:Label>
                                                &nbsp;/&nbsp;
                                                <asp:Label ID="lblMonthComp290rLessPct" runat="server" CssClass="StdTextSmallGrey"></asp:Label>
                                            </td>
                                            <td colspan='3' align="right" class="StdTextSmallGrey">
                                                Average time service -&nbsp;
                                                <asp:Label ID="lblMonthAvgTimeSvc" runat="server" CssClass="StdTextSmallGrey"></asp:Label>
                                                &nbsp;days&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="StdTextSmallGrey" colspan='4'>
                                                360: Reports completed in 29 days or less &nbsp;
                                                <asp:Label ID="lblMonthComp290rLess360" runat="server" CssClass="StdTextSmallGrey"></asp:Label>
                                                &nbsp;/&nbsp;
                                                <asp:Label ID="lblMonthComp290rLessPct360" runat="server" CssClass="StdTextSmallGrey"></asp:Label>
                                            </td>
                                            <td colspan='3' align="right" class="StdTextSmallGrey">
                                                Average time service -&nbsp;
                                                <asp:Label ID="lblMonthAvgTimeSvc360" runat="server" CssClass="StdTextSmallGrey"></asp:Label>
                                                &nbsp;days&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="760">
                            <tr>
                                <td width="400" align="left">
                                    <asp:Label ID="Label29" runat="server" CssClass="StdTextSmallGreySmallCap">Weekly New and Completed Requests</asp:Label>
                                </td>
                                <td width="360" align="right" class="StdTextSmall">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="width: 15px; height: 15px; background: maroon">
                                                    &nbsp;</div>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label13" runat="server" CssClass="StdTextSmallGrey">New</asp:Label>&nbsp;/&nbsp;
                                            </td>
                                            <td>
                                                <div style="width: 15px; height: 15px; background: navy;">
                                                    &nbsp;</div>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label28" runat="server" CssClass="StdTextSmallGrey">Completed</asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <xceedchart:ChartServerControl ID="xcWeeklyNewComp" runat="server" Width="750px">
                                    </xceedchart:ChartServerControl>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="760">
                            <tr>
                                <td width="460" align="center">
                                    <xceedchart:ChartServerControl ID="xcReqHist" runat="server" Width="400px">
                                    </xceedchart:ChartServerControl>
                                </td>
                                <td width="300" align="center" valign="middle">
                                    <table style="background-color: #EFF3F7; border-right: #d3d3d3 thin solid; border-top: #d3d3d3 thin solid;
                                        border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid" cellspacing="4"
                                        cellpadding="4" width="260" border="0" runat="server">
                                        <tr style="background-color: #EFF3F7">
                                            <td align="left">
                                                <center>
                                                    <span class="StdTextSmallCap">Month to date request history</span>
                                                </center>
                                                <p class="StdTextSmallGrey">
                                                    Number of new requests, month to date, for each year.</p>
                                                <asp:Label ID="lblMTDReqHistGraphPd" runat="server" CssClass="StdTextSmallGrey">
                                                </asp:Label><br />
                                                <asp:Label ID="lblMTDReqHistGraphUpdate" runat="server" CssClass="StdTextSmallGrey">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>
