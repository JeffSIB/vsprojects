﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="clientsMain.aspx.cs" Inherits="DashBoard.clientsMain" %>

<%@ Register Assembly="Xceed.Chart.Server, Version=4.2.100.0, Culture=neutral, PublicKeyToken=ba83ff368b7563c6"
    Namespace="Xceed.Chart.Server" TagPrefix="xceedchart" %>
<%@ Register TagPrefix="uc1" TagName="SIBMiniHeader" Src="SIBMiniHeader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Dashboard</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        function launchCurrent() {
            location.href = "Default.aspx";
        }
        function launchRevenue() {
            location.href = "revenueMain.aspx";
        }
        function inactive() {
            alert("This function is not currently active.");
        }
    </script>

</head>
<body style="margin-top: 0; background-color: #f7f3f7;">
    <form id="frmDefault" runat="server">
    <div>
        <center>
            <table style="width: 800px; background-color: #fff">
                <tr>
                    <td style="width: 100%;">
                        <uc1:SIBMiniHeader ID="SIBHeader1" runat="server"></uc1:SIBMiniHeader>
                        <table id="tblTabHdr" cellspacing="0" cellpadding="2" width="700">
                            <tr style="height: 20px">
                                <td width="50">
                                    &nbsp;
                                </td>
                                <td class="unsel_menuitem" id="tabCurrent" title="Current stats" onclick="launchCurrent()"
                                    width="150">
                                    Current
                                </td>
                                <td class="unsel_menuitem" id="tabRevenue" title="Revenue" onclick="launchRevenue()"
                                    width="150">
                                    Revenue
                                </td>
                                <td class="sel_menuitem" id="tabClients" title="Clients" width="150">
                                    Clients
                                </td>
                                <td class="unsel_menuitem" id="tabInspectors" title="Inspectors" onclick="inactive()"
                                    width="150">
                                    Inspectors
                                </td>
                                <td width="50">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table id="Table2" cellspacing="0" cellpadding="2" width="700">
                            <tr>
                                <td width="100%" align="center" class="StdText" valign="middle">
                                    Select an item to view:&nbsp;
                                    <asp:ListBox ID="lbClientRptType" runat="server" Rows="1">
                                        <asp:ListItem>Client request trends</asp:ListItem>
                                        <asp:ListItem>Client margin</asp:ListItem>
                                    </asp:ListBox>
                                    &nbsp;
                                    <asp:LinkButton ID="lbGo" runat="server" CssClass="StdText" 
                                        onclick="lbGo_Click">Go</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>
