﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Principal;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Drawing;
using Xceed.Chart;
using Xceed.Chart.GraphicsCore;
using Xceed.Chart.Standard;
using Xceed.Chart.Core;
using Xceed.Chart.Server;


namespace DashBoard
{
    public partial class clientsReqTrends : System.Web.UI.Page
    {
        static string cfg_SQLMainSIBUtilConnStr;
        static string cfg_SQLMainSIBIConnStr;
        static LogUtils.LogUtils oLU;
        static string cfg_logfilename;

        protected BarSeries m_Bar1;
        protected BarSeries m_Bar2;
        protected Xceed.Chart.Core.Chart m_chart;
        protected Xceed.Chart.Core.Chart m_chartControl;

        protected void Page_Load(object sender, EventArgs e)
        {


            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;


            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;


            try
            {

                if (!IsPostBack)
                {

                    // load configuration values from app.config
                    System.Collections.Specialized.NameValueCollection colNameVal;
                    colNameVal = System.Configuration.ConfigurationManager.AppSettings;

                    cfg_logfilename = colNameVal.Get("logfilename");
                    cfg_SQLMainSIBUtilConnStr = colNameVal.Get("SQLMainSIBUtilConnStr");
                    cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");

                    // setup horizontal chart
                    //m_chart = (Xceed.Chart.Core.Chart)xcClientComp.Charts[0];
                    //m_chart.PredefinedChartStyle = PredefinedChartStyle.HorizontalLeft;
                    //m_chart.LightModel.SetPredefinedScheme(LightScheme.SoftTopRight);
                    //m_chart.MarginMode = MarginMode.Stretch;
                    //m_chart.Margins = new RectangleF(15, 5, 70, 70);
                    ////m_chart.Height = 70;
                    ////m_chart.Width = 70;
                    ////m_chart.Axis(StandardAxis.Depth).Visible = false;


                    //m_chart.Axis(StandardAxis.PrimaryY).DimensionScale.AutoLabels = false;

                    //xcClientComp.Settings.RenderDevice = RenderDevice.OpenGL;
                    xcClientComp.Background.FillEffect.SetSolidColor(Color.FromArgb(232, 237, 245));


                    // setup the chart
                    Xceed.Chart.Core.Chart m_chart = xcClientComp.Charts[0];
                    m_chart.PredefinedChartStyle = PredefinedChartStyle.HorizontalLeft;
                    m_chart.Axis(StandardAxis.Depth).Visible = false;
                    m_chart.LightModel.SetPredefinedScheme(LightScheme.SoftTopRight);
                    m_chart.Margins = new RectangleF(12, 2, 80, 92);
                    m_chart.MarginMode = MarginMode.Stretch;
                    m_chart.Width = 600.0f;
                    m_chart.Height = 50.0f;
                    m_chart.Axis(StandardAxis.PrimaryX).DimensionScale.AutoLabels = false;

                    // add an image border
                    xcClientComp.Background.FrameType = FrameType.Image;
                    ImageFrame imageFrame = xcClientComp.Background.ImageFrame;

                    imageFrame.SetPredefinedFrameStyle(PredefinedImageFrame.Thin);
                    imageFrame.FillEffect.SetSolidColor(Color.SlateGray);
                    imageFrame.BackgroundColor = Color.White;

                    // add the first bar
                    BarSeries m_Bar1 = (BarSeries)m_chart.Series.Add(SeriesType.Bar);
                    m_Bar1.Name = "Current week requests";
                    m_Bar1.MultiBarMode = MultiBarMode.Series;
                    m_Bar1.Legend.Mode = SeriesLegendMode.None;

                    // add the second bar
                    BarSeries m_Bar2 = (BarSeries)m_chart.Series.Add(SeriesType.Bar);
                    m_Bar2.Name = "Average weekly requests";
                    m_Bar2.MultiBarMode = MultiBarMode.Clustered;
                    m_Bar2.Legend.Mode = SeriesLegendMode.None;

                    // data shown will be from previous week
                    DateTime dBegOfWeek = FirstDayofWeek(DateTime.Now);
                    dBegOfWeek = dBegOfWeek.AddDays(-7);

                    lblGraphPd.Text = "Period: " + dBegOfWeek.ToShortDateString() + " - " + dBegOfWeek.AddDays(6).ToShortDateString();

                    // show clients with history over the past year
                    DateTime dBegDate = DateTime.Now.AddYears(-1);

                    int iAcnt = 0;
                    int iRequested = 0;
                    int iAvgReq = 0;
                    object oRet;


                    sqlConn2 = new SqlConnection(cfg_SQLMainSIBIConnStr);
                    sqlCmd2 = new SqlCommand();
                    sqlCmd2.CommandType = CommandType.Text;
                    sqlCmd2.Connection = sqlConn2;
                    sqlConn2.Open();


                    sqlConn1 = new SqlConnection(cfg_SQLMainSIBIConnStr);
                    sqlCmd1 = new SqlCommand();
                    sqlCmd1.CommandType = CommandType.Text;
                    sqlCmd1.CommandText = "SELECT DISTINCT acnt_num FROM ClientHistory WHERE weekbeginning > '" + dBegDate.ToShortDateString() + "' ORDER BY acnt_num";
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        // for each account that has ordered in the past year
                        do
                        {

                            if (!sqlReader.IsDBNull(0))
                            {
                                // look for data from last week
                                iAcnt = (int)sqlReader.GetSqlInt32(0);
                                sqlCmd2.CommandText = "SELECT ISNULL(requested,0) FROM ClientHistory WHERE acnt_num = " + iAcnt.ToString() + " AND weekbeginning = '" + dBegOfWeek.ToShortDateString() + "'";
                                oRet = sqlCmd2.ExecuteScalar();
                                if (oRet == null)
                                    iRequested = 0;
                                else
                                    iRequested = Convert.ToInt32(oRet);

                                if (iAcnt < 9000 && iAcnt > 5999 && iAcnt != 6011 && iAcnt != 7097 && iAcnt != 7242)
                                {
                                    sqlCmd2.CommandText = "SELECT AVG(requested) FROM ClientHistory WHERE acnt_num = " + iAcnt.ToString() + " AND weekbeginning > '" + dBegDate.ToShortDateString() + "'";
                                    iAvgReq = (int)sqlCmd2.ExecuteScalar();

                                    //if (iAvgReq > 1)
                                    //{
                                    m_Bar1.Values.Add(iRequested);
                                    //m_Bar1.Labels.Add(iAcnt.ToString());
                                    m_Bar2.Values.Add(iAvgReq);
                                    //m_chart.Axis(StandardAxis.PrimaryX).Labels.Add("account number " + i.ToString());
                                    m_chart.Axis(StandardAxis.PrimaryX).Labels.Add(iAcnt.ToString());

                                    //}
                                }
                            }

                        } while (sqlReader.Read());

                        sqlReader.Close();
                    }

                    // change the color of the second bar
                    m_Bar1.BarFillEffect.SetSolidColor(Color.DarkSeaGreen);
                    m_Bar2.BarFillEffect.SetSolidColor(Color.MediumSlateBlue);
                }
            }
            catch (Exception ex)
            {

                logError(ex.Message);

            }

            finally
            {
                if (! (sqlConn1 == null))
                    sqlConn1.Close();
            }
 
        }
        //private void setupSalesRevenueChart()
        //{

        //    Xceed.Chart.Core.Chart chart;
        //    BarSeries bar;
        //    chart = (Chart)xcRevenueHist.Charts[0];
        //    chart.MarginMode = MarginMode.Stretch;
        //    chart.Margins = new RectangleF(12, 10, 80, 80);
        //    //chart.Width = 800;
        //    //chart.Height = 600;

        //    bar = (BarSeries)chart.Series.Add(SeriesType.Bar);
        //    bar.Add(Convert.ToDouble(mdecCurYear6Amount), miCurYear6.ToString(), new FillEffect(Color.Azure));
        //    bar.Add(Convert.ToDouble(mdecCurYear5Amount), miCurYear5.ToString(), new FillEffect(Color.Cyan));
        //    bar.Add(Convert.ToDouble(mdecCurYear4Amount), miCurYear4.ToString(), new FillEffect(Color.Yellow));
        //    bar.Add(Convert.ToDouble(mdecCurYear3Amount), miCurYear3.ToString(), new FillEffect(Color.Red));
        //    bar.Add(Convert.ToDouble(mdecCurYear2Amount), miCurYear2.ToString(), new FillEffect(Color.Orange));
        //    bar.Add(Convert.ToDouble(mdecCurYear1Amount), miCurYear1.ToString(), new FillEffect(Color.Blue));
        //    bar.Add(Convert.ToDouble(mdecCurYearAmount), miCurYear.ToString(), new FillEffect(Color.Green));

        //    bar.BarStyle = BarStyle.SmoothEdgeBar;
        //    bar.Appearance.FillMode = AppearanceFillMode.DataPoints;
        //    bar.Legend.Mode = SeriesLegendMode.None;
        //    bar.DataLabels.Format = "<value>";
        //    bar.DataLabels.Text.Backplane.Visible = false;
        //    bar.DataLabels.ArrowLength = 5;
        //    bar.DataLabels.ArrowLine.Color = Color.White;

        //    // disable automatic labels
        //    chart.Axis(StandardAxis.PrimaryX).DimensionScale.AutoLabels = false;

        //    // add custom labels to be displayed on major tick values
        //    chart.Axis(StandardAxis.PrimaryX).Labels.Clear();
        //    chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear6.ToString());
        //    chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear5.ToString());
        //    chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear4.ToString());
        //    chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear3.ToString());
        //    chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear2.ToString());
        //    chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear1.ToString());
        //    chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear.ToString());

        //    // enable the antialiasing of the whole scene
        //    xcRevenueHist.Settings.EnableJittering = true;

        //    // add an image border
        //    xcRevenueHist.Background.FrameType = FrameType.Image;
        //    ImageFrame imageFrame = xcRevenueHist.Background.ImageFrame;

        //    imageFrame.SetPredefinedFrameStyle(PredefinedImageFrame.Thin);
        //    imageFrame.FillEffect.SetSolidColor(Color.SlateGray);
        //    imageFrame.BackgroundColor = Color.White;

        //    // set gradient filling for the background
        //    xcRevenueHist.Background.FillEffect.SetGradient(GradientStyle.Horizontal, GradientVariant.Variant1, Color.White, Color.LightSteelBlue);
        //    xcRevenueHist.UpdateFromDataSources();
        //}


        private void logError(string sErrMsg)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();

            //record exception  
            oLU.WritetoLog("Default.aspx - " + sErrMsg);

            // close objects
            oLU.closeLog();

        }

        private string getUser()
        {
            string sUserID = "";
            int iPos = 0;

            try
            {
                IPrincipal ip = HttpContext.Current.User;
                sUserID = ip.Identity.Name;
                if (sUserID.Length > 0 && sUserID.Contains("\\"))
                {
                    iPos = sUserID.IndexOf("\\");
                    sUserID = sUserID.Substring(iPos + 1, sUserID.Length - (iPos + 1));
                }
            }
            catch (Exception ex)
            {
                logError(ex.Message);
                Response.Redirect("Util/SystemErr.aspx");
            }

            return sUserID;

        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        private void HorizontalBarUC_Load(object sender, System.EventArgs e)
        {

            xcClientComp.Settings.RenderDevice = RenderDevice.OpenGL;
            xcClientComp.Background.FillEffect.SetSolidColor(Color.FromArgb(232, 237, 245));

            // add a header label
            ChartLabel header = xcClientComp.Labels.AddHeader("Horizontal Bar Chart");
            header.TextProps.Backplane.Visible = false;
            header.TextProps.Shadow.Type = ShadowType.Solid;
            header.TextProps.HorzAlign = HorzAlign.Left;
            header.TextProps.VertAlign = VertAlign.Top;
            header.HorizontalMargin = 2;
            header.VerticalMargin = 2;

            // setup the chart
            Xceed.Chart.Core.Chart chart = xcClientComp.Charts[0];
            chart.PredefinedChartStyle = PredefinedChartStyle.HorizontalLeft;
            chart.Axis(StandardAxis.Depth).Visible = false;
            chart.LightModel.SetPredefinedScheme(LightScheme.SoftTopRight);
            chart.Width = 55.0f;
            chart.Height = 70.0f;

            // add manual labels to the chart

            chart.Axis(StandardAxis.PrimaryX).DimensionScale.AutoLabels = false;
            for (int i = 0; i < 5; ++i)
            {
                chart.Axis(StandardAxis.PrimaryX).Labels.Add("account number " + i.ToString());
            }

            // add a bar series
            BarSeries b1 = (BarSeries)chart.Series.Add(SeriesType.Bar);
            b1.MultiBarMode = MultiBarMode.Series;
            b1.Name = "Bar 1";
            b1.DataLabels.Format = "<value>";
            b1.Values.FillRandomRange(5, 10, 100);
            b1.BarFillEffect.SetSolidColor(Color.Violet);

        }

  
       protected void lbGo_Click(object sender, EventArgs e)
        {
            if (lbClientRptType.SelectedIndex == 0)
            {
                // current page
            }
            else if (lbClientRptType.SelectedIndex == 1)
            {
                Response.Redirect("clientMargin.aspx");
            }

        }
 

    }
}
