﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Principal;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Drawing;
using Xceed.Chart;
using Xceed.Chart.GraphicsCore;
using Xceed.Chart.Standard;
using Xceed.Chart.Core;
using Xceed.Chart.Server;

namespace DashBoard
{
    public partial class revenueMain : System.Web.UI.Page
    {
        static string cfg_SQLMainSIBUtilConnStr;
        static string cfg_360UtilConnStr;

        static LogUtils.LogUtils oLU;
        static string cfg_logfilename;
        static string cfg_level1users;

        static string msUserID;
        static bool mbLevel1User;

        static int miCurYear;
        static int miCurYear1;
        static int miCurYear2;
        static int miCurYear3;
        static int miCurYear4;
        static int miCurYear5;
        static int miCurYear6;

        static int miCurYearCount;
        static int miCurYear1Count;
        static int miCurYear2Count;
        static int miCurYear3Count;
        static int miCurYear4Count;
        static int miCurYear5Count;
        static int miCurYear6Count;

        static decimal mdecCurYearAmount;
        static decimal mdecCurYear1Amount;
        static decimal mdecCurYear2Amount;
        static decimal mdecCurYear3Amount;
        static decimal mdecCurYear4Amount;
        static decimal mdecCurYear5Amount;
        static decimal mdecCurYear6Amount;

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;

                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_SQLMainSIBUtilConnStr = colNameVal.Get("SQLMainSIBUtilConnStr");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_level1users = colNameVal.Get("level1users");

                // get User ID
                msUserID = getUser();

                // Show/Hide menus based on user id
                mbLevel1User = cfg_level1users.Contains(msUserID.ToLower());

                if (mbLevel1User)
                {
                    pnlMenu.Visible = true;
                }
                else
                {
                    pnlMenu.Visible = false;
                }


                loadSalesData();
                loadSalesHist();
                setupSalesRevenueChart();
                setupWeeklyRevenueChart();


            }
            catch (Exception ex)
            {

                logError(ex.Message);

            }

            finally
            {

            }
 

        }

        private void setupSalesRevenueChart()
        {

            Xceed.Chart.Core.Chart chart;
            BarSeries bar;
            chart = (Chart)xcRevenueHist.Charts[0];
            chart.MarginMode = MarginMode.Stretch;
            chart.Margins = new RectangleF(12, 10, 80, 80);
            //chart.Width = 800;
            //chart.Height = 600;

            bar = (BarSeries)chart.Series.Add(SeriesType.Bar);
            bar.Add(Convert.ToDouble(mdecCurYear6Amount), miCurYear6.ToString(), new FillEffect(Color.Azure));
            bar.Add(Convert.ToDouble(mdecCurYear5Amount), miCurYear5.ToString(), new FillEffect(Color.Cyan));
            bar.Add(Convert.ToDouble(mdecCurYear4Amount), miCurYear4.ToString(), new FillEffect(Color.Yellow));
            bar.Add(Convert.ToDouble(mdecCurYear3Amount), miCurYear3.ToString(), new FillEffect(Color.Red));
            bar.Add(Convert.ToDouble(mdecCurYear2Amount), miCurYear2.ToString(), new FillEffect(Color.Orange));
            bar.Add(Convert.ToDouble(mdecCurYear1Amount), miCurYear1.ToString(), new FillEffect(Color.Blue));
            bar.Add(Convert.ToDouble(mdecCurYearAmount), miCurYear.ToString(), new FillEffect(Color.Green));

            bar.BarStyle = BarStyle.SmoothEdgeBar;
            bar.Appearance.FillMode = AppearanceFillMode.DataPoints;
            bar.Legend.Mode = SeriesLegendMode.None;
            bar.DataLabels.Format = "<value>";
            bar.DataLabels.Text.Backplane.Visible = false;
            bar.DataLabels.ArrowLength = 5;
            bar.DataLabels.ArrowLine.Color = Color.White;

            // disable automatic labels
            chart.Axis(StandardAxis.PrimaryX).DimensionScale.AutoLabels = false;

            // add custom labels to be displayed on major tick values
            chart.Axis(StandardAxis.PrimaryX).Labels.Clear();
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear6.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear5.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear4.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear3.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear2.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear1.ToString());
            chart.Axis(StandardAxis.PrimaryX).Labels.Add(miCurYear.ToString());

            // enable the antialiasing of the whole scene
            xcRevenueHist.Settings.EnableJittering = true;

            // add an image border
            xcRevenueHist.Background.FrameType = FrameType.Image;
            ImageFrame imageFrame = xcRevenueHist.Background.ImageFrame;

            imageFrame.SetPredefinedFrameStyle(PredefinedImageFrame.Thin);
            imageFrame.FillEffect.SetSolidColor(Color.SlateGray);
            imageFrame.BackgroundColor = Color.White;

            // set gradient filling for the background
            xcRevenueHist.Background.FillEffect.SetGradient(GradientStyle.Horizontal, GradientVariant.Variant1, Color.White, Color.LightSteelBlue);
            xcRevenueHist.UpdateFromDataSources();
        }

        private void logError(string sErrMsg)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();

            //record exception  
            oLU.WritetoLog("Default.aspx - " + sErrMsg);

            // close objects
            oLU.closeLog();

        }

        private string getUser()
        {
            string sUserID = "";
            int iPos = 0;

            try
            {
                IPrincipal ip = HttpContext.Current.User;
                sUserID = ip.Identity.Name;
                if (sUserID.Length > 0 && sUserID.Contains("\\"))
                {
                    iPos = sUserID.IndexOf("\\");
                    sUserID = sUserID.Substring(iPos + 1, sUserID.Length - (iPos + 1));
                }
            }
            catch (Exception ex)
            {
                logError(ex.Message);
                Response.Redirect("Util/SystemErr.aspx");
            }

            return sUserID;

        }

        private void loadSalesHist()
        {

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlDR1 = null;
            SqlConnection sqlConn360 = null;
            SqlCommand sqlCmd360 = null;
            SqlDataReader sqlDR360 = null;


            miCurYear = DateTime.Now.Year;
            miCurYear1 = miCurYear - 1;
            miCurYear2 = miCurYear - 2;
            miCurYear3 = miCurYear - 3;
            miCurYear4 = miCurYear - 4;
            miCurYear5 = miCurYear - 5;
            miCurYear6 = miCurYear - 6;

            miCurYear1Count = 0;
            miCurYear2Count = 0;
            miCurYear3Count = 0;
            miCurYear4Count = 0;
            miCurYear5Count = 0;
            miCurYear6Count = 0;

            mdecCurYear1Amount = 0;
            mdecCurYear2Amount = 0;
            mdecCurYear3Amount = 0;
            mdecCurYear4Amount = 0;
            mdecCurYear5Amount = 0;
            mdecCurYear6Amount = 0;

            DateTime dPeriodBeg;
            DateTime dPeriodEnd;
            DateTime dUpdated;
            
            try
            {

                sqlConn1 = new SqlConnection(cfg_SQLMainSIBUtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT * FROM Dashboard_Sales_Hist";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlDR1 = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR1.HasRows)
                {

                    sqlDR1.Read();

                    miCurYearCount = (int)sqlDR1.GetInt32(0);
                    mdecCurYearAmount = Math.Round(((decimal)sqlDR1.GetDecimal(1)* (decimal).001),2);
                    miCurYear1Count = (int)sqlDR1.GetInt32(2);
                    mdecCurYear1Amount = Math.Round(((decimal)sqlDR1.GetDecimal(3) * (decimal).001), 2);
                    miCurYear2Count = (int)sqlDR1.GetInt32(4);
                    mdecCurYear2Amount = Math.Round(((decimal)sqlDR1.GetDecimal(5) * (decimal).001), 2);
                    miCurYear3Count = (int)sqlDR1.GetInt32(6);
                    mdecCurYear3Amount = Math.Round(((decimal)sqlDR1.GetDecimal(7)*(decimal).001),2);
                    miCurYear4Count = (int)sqlDR1.GetInt32(8);
                    mdecCurYear4Amount = Math.Round(((decimal)sqlDR1.GetDecimal(9)*(decimal).001),2);
                    miCurYear5Count = (int)sqlDR1.GetInt32(10);
                    mdecCurYear5Amount = Math.Round(((decimal)sqlDR1.GetDecimal(11) * (decimal).001), 2);
                    miCurYear6Count = (int)sqlDR1.GetInt32(12);
                    mdecCurYear6Amount = Math.Round(((decimal)sqlDR1.GetDecimal(13) * (decimal).001), 2);
                    dUpdated = (DateTime)sqlDR1.GetSqlDateTime(14);
                    dPeriodBeg = (DateTime)sqlDR1.GetSqlDateTime(15);
                    dPeriodEnd = (DateTime)sqlDR1.GetSqlDateTime(16);
                    lblMTDRevHistGraphUpdate.Text = "Updated: " + dUpdated.ToString();
                    lblMTDRevHistGraphPd.Text = "Period: " + dPeriodBeg.ToShortDateString() + " - " + dPeriodEnd.ToShortDateString();
          
                } 

                 sqlConn1.Close();


                // 360
                 sqlConn360 = new SqlConnection(cfg_360UtilConnStr);
                 sqlCmd360 = new SqlCommand();

                 sqlCmd360.CommandType = CommandType.Text;
                 sqlCmd360.CommandText = "SELECT * FROM Dashboard_Sales_Hist";
                 sqlCmd360.Connection = sqlConn360;
                 sqlConn360.Open();

                 sqlDR360 = sqlCmd360.ExecuteReader();

                 // anythign returned?
                 if (sqlDR360.HasRows)
                 {

                     sqlDR360.Read();

                     miCurYearCount = miCurYearCount + (int)sqlDR360.GetInt32(0);
                     mdecCurYearAmount = mdecCurYearAmount + Math.Round(((decimal)sqlDR360.GetDecimal(1) * (decimal).001), 2);
                     miCurYear1Count = miCurYear1Count + (int)sqlDR360.GetInt32(2);
                     mdecCurYear1Amount = mdecCurYear1Amount + Math.Round(((decimal)sqlDR360.GetDecimal(3) * (decimal).001), 2);
                     miCurYear2Count = miCurYear2Count + (int)sqlDR360.GetInt32(4);
                     mdecCurYear2Amount = mdecCurYear2Amount + Math.Round(((decimal)sqlDR360.GetDecimal(5) * (decimal).001), 2);
                     miCurYear3Count = miCurYear3Count + (int)sqlDR360.GetInt32(6);
                     mdecCurYear3Amount = mdecCurYear3Amount + Math.Round(((decimal)sqlDR360.GetDecimal(7) * (decimal).001), 2);
                     miCurYear4Count = miCurYear4Count + (int)sqlDR360.GetInt32(8);
                     mdecCurYear4Amount = mdecCurYear4Amount + Math.Round(((decimal)sqlDR360.GetDecimal(9) * (decimal).001), 2);
                     miCurYear5Count = miCurYear5Count + (int)sqlDR360.GetInt32(10);
                     mdecCurYear5Amount = mdecCurYear5Amount + Math.Round(((decimal)sqlDR360.GetDecimal(11) * (decimal).001), 2);
                     miCurYear6Count = miCurYear6Count + (int)sqlDR360.GetInt32(12);
                     mdecCurYear6Amount = mdecCurYear6Amount + Math.Round(((decimal)sqlDR360.GetDecimal(13) * (decimal).001), 2);

                 }

                 sqlConn360.Close();


            }

            catch (Exception ex)
            {

                logError(ex.Message);
                Response.Redirect("Util/SystemErr.aspx");

            }

        }

        private void setupWeeklyRevenueChart()
        {

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlDR1 = null;

            SqlConnection sqlConn360 = null;
            SqlCommand sqlCmd360 = null;
            SqlDataReader sqlDR360 = null;

            try
            {



                xcWeeklyRevenue.Settings.RenderDevice = RenderDevice.GDI;
                xcWeeklyRevenue.Background.FillEffect.SetGradient(GradientStyle.Horizontal, GradientVariant.Variant2, Color.White, Color.FromArgb(234, 234, 244));

                // image border
                xcWeeklyRevenue.Background.FrameType = FrameType.Image;
                ImageFrame imageFrame = xcWeeklyRevenue.Background.ImageFrame;

                imageFrame.SetPredefinedFrameStyle(PredefinedImageFrame.Thin);
                imageFrame.FillEffect.SetSolidColor(Color.SlateGray);
                imageFrame.BackgroundColor = Color.White;

                // set gradient filling for the background
                xcWeeklyRevenue.Background.FillEffect.SetGradient(GradientStyle.Horizontal, GradientVariant.Variant1, Color.White, Color.LightSteelBlue);

                // setup chart
                Xceed.Chart.Core.Chart chart = xcWeeklyRevenue.Charts[0];
                chart.MarginMode = MarginMode.Stretch;
                chart.Margins = new RectangleF(10, 10, 85, 70);

                chart.Axis(StandardAxis.PrimaryY).ScaleMode = AxisScaleMode.Numeric;
                chart.Axis(StandardAxis.PrimaryY).MajorGridLine.Pattern = LinePattern.Dot;
                chart.Axis(StandardAxis.PrimaryY).OuterTickLength = 1.5f;
                chart.Axis(StandardAxis.PrimaryY).MinorTickLength = 1;

                chart.Axis(StandardAxis.PrimaryX).ScaleMode = AxisScaleMode.DateTime;
                chart.Axis(StandardAxis.PrimaryX).MajorGridLine.Pattern = LinePattern.Dot;
                chart.Axis(StandardAxis.PrimaryX).OuterTickLength = 1.5f;
                chart.Axis(StandardAxis.PrimaryX).MinorTickLength = 1;

                LineSeries lsInv = (LineSeries)chart.Series.Add(SeriesType.Line);
                lsInv.Legend.Mode = SeriesLegendMode.None;
                lsInv.InflateMargins = false;
                lsInv.LineBorder.Color = Color.Navy;
                lsInv.Markers.Visible = true;
                lsInv.Markers.Border.Color = Color.Navy;
                lsInv.Markers.Style = PointStyle.Cylinder;
                lsInv.Markers.Width = 0.7f;
                lsInv.Markers.Height = 0.7f;
                lsInv.Markers.AutoDepth = true;
                lsInv.DataLabels.Mode = DataLabelsMode.None;

                LineSeries lsInsp = (LineSeries)chart.Series.Add(SeriesType.Line);
                lsInsp.Legend.Mode = SeriesLegendMode.None;
                lsInsp.InflateMargins = false;
                lsInsp.LineBorder.Color = Color.Maroon;
                lsInsp.Markers.Visible = true;
                lsInsp.Markers.Border.Color = Color.Maroon;
                lsInsp.Markers.Style = PointStyle.Sphere;
                lsInsp.Markers.Width = 0.7f;
                lsInsp.Markers.Height = 0.7f;
                lsInsp.Markers.AutoDepth = true;
                lsInsp.DataLabels.Mode = DataLabelsMode.None;

                //StandardFrame fp = line.DataLabels.Text.Backplane.StandardFrame;
                //fp.Border.Color = Color.CornflowerBlue;

                Axis axis = chart.Axis(StandardAxis.PrimaryX);

                axis.StaggerTexts = true;

                axis.ValueFormatting.Format = Xceed.Chart.Utilities.ValueFormat.Date;
                axis.DateTimeScale.MajorTickMode = MajorTickModeDateTime.Months;
                axis.DateTimeScale.MonthsStep = 1;

                lsInv.UseXValues = true;
                lsInsp.UseXValues = true;


                // Date range
                DateTime dBegDate = new DateTime();
                dBegDate = DateTime.Today;
                dBegDate = FirstDayofWeek(dBegDate);
                DateTime dEndDate = dBegDate.AddDays(-7);
                dBegDate = dBegDate.AddDays(-361);
                dBegDate = FirstDayofWeek(dBegDate);

                decimal dInv = 0;
                decimal dInsp = 0;
                DateTime dWeek;
                int iRow = 0;

                decimal[] adInv = new decimal[52];
                decimal[] adInsp = new decimal[52];
                DateTime[] adtWeek = new DateTime[52];


                sqlConn1 = new SqlConnection(cfg_SQLMainSIBUtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT amtinvoiced, insppaid, statusperiod FROM Dashboard_Stats WHERE statusperiod BETWEEN '" + dBegDate + "' AND '" + dEndDate + "' AND stattype = 'W' ORDER BY statusperiod";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlDR1 = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR1.HasRows)
                {

                    sqlDR1.Read();

                    do
                    {

                        dInv = (decimal)sqlDR1.GetDecimal(0);
                        dInsp = (decimal)sqlDR1.GetDecimal(1);
                        dWeek = sqlDR1.GetDateTime(2);

                        adInv[iRow] = dInv;
                        adInsp[iRow] = dInsp;
                        adtWeek[iRow] = dWeek;
                        iRow++;

                    } while (sqlDR1.Read());
                }

                sqlConn1.Close();

                // 360
                iRow = 0;
                sqlConn360 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd360 = new SqlCommand();

                sqlCmd360.CommandType = CommandType.Text;
                sqlCmd360.CommandText = "SELECT amtinvoiced, insppaid, statusperiod FROM Dashboard_Stats WHERE statusperiod BETWEEN '" + dBegDate + "' AND '" + dEndDate + "' AND stattype = 'W' ORDER BY statusperiod";
                sqlCmd360.Connection = sqlConn360;
                sqlConn360.Open();

                sqlDR360 = sqlCmd360.ExecuteReader();

                // anythign returned?
                if (sqlDR360.HasRows)
                {

                    sqlDR360.Read();

                    do
                    {

                        dInv = (decimal)sqlDR360.GetDecimal(0);
                        dInsp = (decimal)sqlDR360.GetDecimal(1);
                        dWeek = sqlDR360.GetDateTime(2);

                        adInv[iRow] = adInv[iRow] + dInv;
                        adInsp[iRow] = adInsp[iRow] + dInsp;

                        if (adtWeek[iRow] != dWeek)
                        {
                            throw new SystemException("Date mismatch loading WeeklyRevenue chart");
                        }
                        iRow++;

                    } while (sqlDR360.Read());
                }

                sqlConn360.Close();

                for (int iWeek = 0; iWeek < 52; iWeek++)
                {

                    lsInsp.Values.Add(adInv[iWeek]);
                    lsInv.Values.Add(adInsp[iWeek]);
                    lsInv.XValues.Add(adtWeek[iWeek].ToOADate());
                    lsInsp.XValues.Add(adtWeek[iWeek].ToOADate());
                }
                
            }


            catch (Exception ex)
            {

                logError(ex.Message);
                Response.Redirect("Util/SystemErr.aspx");

            }


        }

        protected int loadSalesData()
        {
            int iCount = 0;

            DataTable dt = new DataTable();
            dt.Columns.Add("month", typeof(string)).Unique = true;
            dt.Columns.Add("requested", typeof(int));
            dt.Columns.Add("completed", typeof(int));
            dt.Columns.Add("invamt", typeof(decimal));
            dt.Columns.Add("inspchg", typeof(decimal));
            dt.Columns.Add("margin", typeof(decimal));
            dt.Columns.Add("average", typeof(decimal));
            dt.Columns.Add("time", typeof(decimal));
            dt.Columns.Add("CompLT30", typeof(int));


            DataColumn[] pKey = new DataColumn[1];
            pKey[0] = dt.Columns["month"];
            dt.PrimaryKey = pKey;


            DataTable dt360 = new DataTable();
            dt360.Columns.Add("month", typeof(string)).Unique = true;
            dt360.Columns.Add("requested", typeof(int));
            dt360.Columns.Add("completed", typeof(int));
            dt360.Columns.Add("invamt", typeof(decimal));
            dt360.Columns.Add("inspchg", typeof(decimal));
            dt360.Columns.Add("margin", typeof(decimal));
            dt360.Columns.Add("average", typeof(decimal));
            dt360.Columns.Add("time", typeof(decimal));
            dt360.Columns.Add("CompLT30", typeof(int));

            DataColumn[] pKey360 = new DataColumn[1];
            pKey360[0] = dt360.Columns["month"];
            dt360.PrimaryKey = pKey360;

            //DataRow[] test;


            DateTime dFirstMonth = DateTime.Today;
            dFirstMonth = FirstDayOfMonth(dFirstMonth);
            dFirstMonth = dFirstMonth.AddMonths(-13);
            //dFirstMonth = dFirstMonth.AddMonths(1);

            string sMonth = "";
            int iRequested = 0;
            int iComp = 0;
            decimal dInvAmt = 0;
            decimal dInspChg = 0;
            decimal dMargin = 0;
            decimal dAvgAmt = 0;
            int iCompLT30 = 0;
            decimal dComp = 0;
            decimal dCompLT30 = 0;
            decimal dTime = 0;
            DateTime dStatDate;
            string sStatTime;
            bool bCurMonth = true;

            DateTime dMonth;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            SqlConnection sqlConn360 = null;
            SqlCommand sqlCmd360 = null;
            SqlDataReader sqlDR360 = null;

            try
            {

                // set up SQL connection
                sqlConn1 = new SqlConnection(cfg_SQLMainSIBUtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.CommandType = CommandType.Text;
                //sqlCmd1.CommandText = "SELECT statusperiod, newcases, completed, amtinvoiced, insppaid, complt30, statusdate, statustime FROM dashboard_stats WHERE stattype = 'M' AND statusperiod > '" + dFirstMonth.ToShortDateString() + "' ORDER BY statusperiod DESC";
                sqlCmd1.CommandText = "SELECT period, requested, completed, invamt, paidamt, complt30,statusdate FROM dashboard_monthlysaleshist ORDER BY period DESC";
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {

                    sqlReader.Read();

                    do
                    {

                        dMonth = (DateTime)sqlReader.GetSqlDateTime(0);
                        sMonth = String.Format("{0:MMM yy}", dMonth);
                        iRequested = (int)sqlReader.GetSqlInt32(1);
                        iComp = (int)sqlReader.GetSqlInt32(2);
                        dInvAmt = (decimal)sqlReader.GetDecimal(3);
                        dInspChg = (decimal)sqlReader.GetDecimal(4);
                        iCompLT30 = (int)sqlReader.GetSqlInt32(5);
                        dStatDate = (DateTime)sqlReader.GetSqlDateTime(6);
                        //sStatTime = sqlReader.GetSqlString(7).ToString();

                        if (bCurMonth)
                        {
                            lblAsOf.Text = "As of: " + dStatDate.ToShortDateString() + " " + dStatDate.ToShortTimeString();
                            bCurMonth = false;
                        }
                        
                        //if (dInspChg > 0 && dInvAmt > 0)
                        //    dMargin = (dInspChg / dInvAmt);
                        //else
                        //    dMargin = 0;

                        //if (iComp > 0 && dInvAmt > 0)
                        //    dAvgAmt = dInvAmt / iComp;
                        //else
                        //    dAvgAmt = 0;

                        //dComp = Convert.ToDecimal(iComp);
                        //dCompLT30 = Convert.ToDecimal(iCompLT30);

                        //if (dCompLT30 > 0 && dComp > 0)
                        //    dTime = dCompLT30 / dComp;
                        //else
                        //    dTime = 0;

                        DataRow dr1 = dt.NewRow();

                        dr1["month"] = sMonth;
                        dr1["requested"] = iRequested;
                        dr1["completed"] = iComp;
                        dr1["invamt"] = dInvAmt;
                        dr1["inspchg"] = dInspChg;
                        dr1["CompLT30"] = iCompLT30;
                        //dr1["margin"] = dMargin;
                        //dr1["average"] = dAvgAmt;
                        //dr1["time"] = dTime;


                        // Add row
                        dt.Rows.Add(dr1);
                        iCount += 1;

                    } while (sqlReader.Read());


                }

                sqlConn1.Close();
                                
               //foreach(DataRow myRow in  dt.Rows)
               //{
                                     
               //     myRow["invamt"] = "99.99";
    
               //}           


                /////////
                // 360 //
                /////////

                //// set up SQL connection
                sqlConn360 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd360 = new SqlCommand();
                sqlCmd360.Connection = sqlConn360;
                sqlCmd360.CommandType = CommandType.Text;
                //sqlCmd360.CommandText = "SELECT statusperiod, newcases, completed, amtinvoiced, insppaid, complt30, statusdate, statustime FROM dashboard_stats WHERE stattype = 'M' AND statusperiod > '" + dFirstMonth.ToShortDateString() + "' ORDER BY statusperiod DESC";
                sqlCmd360.CommandText = "SELECT period, requested, completed, invamt, paidamt, complt30 FROM dashboard_monthlysaleshist ORDER BY period DESC";
                sqlConn360.Open();

                sqlDR360 = sqlCmd360.ExecuteReader();

                if (sqlDR360.HasRows)
                {

                    sqlDR360.Read();

                    do
                    {

                        dMonth = (DateTime)sqlDR360.GetSqlDateTime(0);
                        sMonth = String.Format("{0:MMM yy}", dMonth);
                        iRequested = (int)sqlDR360.GetSqlInt32(1);
                        iComp = (int)sqlDR360.GetSqlInt32(2);
                        dInvAmt = (decimal)sqlDR360.GetDecimal(3);
                        dInspChg = (decimal)sqlDR360.GetDecimal(4);
                        iCompLT30 = (int)sqlDR360.GetSqlInt32(5);
                        //dStatDate = (DateTime)sqlDR360.GetSqlDateTime(6);
                        //sStatTime = sqlDR360.GetSqlString(7).ToString();


                        DataRow dr360 = dt360.NewRow();

                        dr360["month"] = sMonth;
                        dr360["requested"] = iRequested;
                        dr360["completed"] = iComp;
                        dr360["invamt"] = dInvAmt;
                        dr360["inspchg"] = dInspChg;
                        dr360["CompLT30"] = iCompLT30;

                        // Add row
                        dt360.Rows.Add(dr360);
                        iCount += 1;

                    } while (sqlDR360.Read());

                }


                sqlConn360.Close();

                ///////////////////////////////////////////////////
                // Combine tables
                ///////////////////////////////////////////////////

                int iCurRow = 0;
                DataRow dRow360;
                                
                foreach (DataRow dRow in dt.Rows)
                {

                    iCurRow = dt.Rows.IndexOf(dRow);
                    dRow360 = dt360.Rows[iCurRow];
                    dRow["requested"] = (int)dRow["requested"] + (int)dRow360["requested"];
                    dRow["completed"] = (int)dRow["completed"] + (int)dRow360["completed"];
                    dRow["invamt"] = (decimal)dRow["invamt"] + (decimal)dRow360["invamt"];
                    dRow["inspchg"] = (decimal)dRow["inspchg"] + (decimal)dRow360["inspchg"];
                    dRow["CompLT30"] = (int)dRow["CompLT30"] + (int)dRow360["CompLT30"];

                    iComp = (int)dRow["completed"];
                    dInvAmt = (decimal)dRow["invamt"];
                    dInspChg = (decimal)dRow["inspchg"];
                    iCompLT30 = (int)dRow["CompLT30"];

                    if (dInspChg > 0 && dInvAmt > 0)
                        dMargin = (dInspChg / dInvAmt);
                    else
                        dMargin = 0;

                    if (iComp > 0 && dInvAmt > 0)
                        dAvgAmt = dInvAmt / iComp;
                    else
                        dAvgAmt = 0;

                    dComp = Convert.ToDecimal(iComp);
                    dCompLT30 = Convert.ToDecimal(iCompLT30);

                    if (dCompLT30 > 0 && dComp > 0)
                        dTime = dCompLT30 / dComp;
                    else
                        dTime = 0;

                    dRow["margin"] = dMargin;
                    dRow["average"] = dAvgAmt;
                    dRow["time"] = dTime;

                }           






                //string sTMonth = "Jan 11";
                //test = dt.Select("month = '" + sTMonth + "'");
                //Response.Write(test.Count());
                //DataTable temptable = test.CopyToDataTable();
                //foreach (DataRow row in temptable.Rows)
                //{
                //    Response.Write(row["month"]);
                //}

            }

            catch (Exception ex)
            {

                logError("clientMargin - loadData: " + ex.Message);
                Response.Redirect("Util/SystemErr.aspx");

            }

            Grid1.DataSource = dt;
            Grid1.DataBind();

            if (iCount == 0)
            {
                Grid1.Visible = false;
            }

            return iCount;

        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

    }
}
