﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Principal;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Drawing;
using Xceed.Chart;
using Xceed.Chart.GraphicsCore;
using Xceed.Chart.Standard;
using Xceed.Chart.Core;
using Xceed.Chart.Server;


namespace DashBoard
{
    public partial class clientsMain : System.Web.UI.Page
    {
        static string cfg_SQLMainSIBUtilConnStr;
        static string cfg_SQLMainSIBIConnStr;
        static LogUtils.LogUtils oLU;
        static string cfg_logfilename;

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;

                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_SQLMainSIBUtilConnStr = colNameVal.Get("SQLMainSIBUtilConnStr");
                cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");

            }
            catch (Exception ex)
            {

                logError(ex.Message);

            }

            finally
            {
            }
 
        }


        private void logError(string sErrMsg)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();

            //record exception  
            oLU.WritetoLog("Default.aspx - " + sErrMsg);

            // close objects
            oLU.closeLog();

        }
                
        protected void lbGo_Click(object sender, EventArgs e)
        {
            if (lbClientRptType.SelectedIndex == 0)
            {
                Response.Redirect("clientsReqTrends.aspx");
            }
            else if (lbClientRptType.SelectedIndex == 1)
            {
                Response.Redirect("clientMargin.aspx");
            }

        }
            
   
    }
}
