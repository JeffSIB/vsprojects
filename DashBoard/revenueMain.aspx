﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="revenueMain.aspx.cs" Inherits="DashBoard.revenueMain" %>

<%@ Register Assembly="Xceed.Chart.Server, Version=4.2.100.0, Culture=neutral, PublicKeyToken=ba83ff368b7563c6"
    Namespace="Xceed.Chart.Server" TagPrefix="xceedchart" %>
<%@ Register TagPrefix="uc1" TagName="SIBMiniHeader" Src="SIBMiniHeader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Dashboard</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        function launchCurrent() {
            location.href = "Default.aspx";
        }
        function launchClients() {
            location.href = "clientsMain.aspx";
        }
        function inactive() {
            alert("This function is not currently active.");
        }

        
    </script>

</head>
<body style="margin-top: 0; background-color: #f7f3f7;">
    <form id="frmDefault" runat="server">
    <div>
        <center>
            <table style="width: 800px; background-color: #fff">
                <tr>
                    <td style="width: 100%">
                        <uc1:SIBMiniHeader ID="SIBHeader1" runat="server"></uc1:SIBMiniHeader>
                        <div id="pnlMenu" runat="server" style="position: relative;">
                            <table id="tblTabHdr" cellspacing="0" cellpadding="2" width="700">
                                <tr style="height: 20px">
                                    <td width="50">
                                        &nbsp;
                                    </td>
                                    <td class="unsel_menuitem" id="tabCurrent" title="Current stats" onclick="launchCurrent()"
                                        width="150">
                                        Current
                                    </td>
                                    <td class="sel_menuitem" id="tabRevenue" title="Revenue" width="150">
                                        Revenue
                                    </td>
                                    <td class="unsel_menuitem" id="tabClients" title="Clients" onclick="launchClients()"
                                        width="150">
                                        Clients
                                    </td>
                                    <td class="unsel_menuitem" id="tabInspectors" title="Inspectors" onclick="inactive()"
                                        width="150">
                                        Inspectors
                                    </td>
                                    <td width="50">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <table id="Table1" style="border-right: #c0c0c0 thin solid; border-top: #c0c0c0 thin solid;
                            border-left: #c0c0c0 thin solid; border-bottom: #c0c0c0 thin solid" cellspacing="0"
                            cellpadding="0" width="700" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <table cellspacing="2" cellpadding="2" width="680" border="0">
                                        <tr>
                                            <td align="left" width="380">
                                                <asp:Label ID="Label1" runat="server" CssClass="StdTextSmallGreySmallCap">12 Month Summary</asp:Label>
                                            </td>
                                            <td align="right" width="380">
                                                <asp:Label ID="lblAsOf" runat="server" CssClass="StdTextSmallGrey"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table cellspacing="2" cellpadding="2" width="680" border="0">
                                        <tr>
                                            <td align="center" width="680">
                                                <asp:DataGrid ID="Grid1" runat="server" CssClass="StdText" BorderColor="#666666"
                                                    BorderWidth="1px" CellPadding="2" AutoGenerateColumns="False" BorderStyle="Solid">
                                                    <AlternatingItemStyle BorderStyle="None" BorderColor="#666666" BackColor="#E4FFE4">
                                                    </AlternatingItemStyle>
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="StdTextSmall" BackColor="#FFFFDD"
                                                        Width="680px"></HeaderStyle>
                                                    <FooterStyle HorizontalAlign="Center" CssClass="StdTextSmall" BackColor="#FFFFDD"
                                                        Width="680px" />
                                                    <Columns>
                                                        <asp:BoundColumn DataField="month" ReadOnly="True" HeaderText="Month">
                                                            <ItemStyle Width="80px" HorizontalAlign="center" CssClass="StdTextSmall"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="requested" DataFormatString="{0:N0}" ReadOnly="True"
                                                            HeaderText="Requested">
                                                            <ItemStyle Width="80px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="completed" ReadOnly="True" DataFormatString="{0:N0}"
                                                            HeaderText="Completed">
                                                            <ItemStyle Width="80px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="invamt" ReadOnly="True" DataFormatString="{0:c}" HeaderText="Invoiced Amt">
                                                            <ItemStyle Width="100px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="inspchg" ReadOnly="True" DataFormatString="{0:c}" HeaderText="Insp Cost">
                                                            <ItemStyle Width="100px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="margin" ReadOnly="True" DataFormatString="{0:P2}" HeaderText="Margin">
                                                            <ItemStyle Width="80px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="time" ReadOnly="True" DataFormatString="{0:P2}" HeaderText="Comp < 30">
                                                            <ItemStyle Width="80px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="average" ReadOnly="True" DataFormatString="{0:c}" HeaderText="Average">
                                                            <ItemStyle Width="80px" HorizontalAlign="Center" CssClass="StdTextSmall"></ItemStyle>
                                                        </asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="760">
                            <tr>
                                <td width="460" align="center">
                                    <xceedchart:ChartServerControl ID="xcRevenueHist" runat="server" Width="400px">
                                    </xceedchart:ChartServerControl>
                                </td>
                                <td width="300" align="center" valign="middle">
                                    <table id="Table6" style="background-color: #EFF3F7; border-right: #d3d3d3 thin solid;
                                        border-top: #d3d3d3 thin solid; border-left: #d3d3d3 thin solid; border-bottom: #d3d3d3 thin solid"
                                        cellspacing="4" cellpadding="4" width="260" border="0" runat="server">
                                        <tr style="background-color: #EFF3F7">
                                            <td align="left">
                                                <center>
                                                    <span class="StdTextSmallCap">Month to Date Revenue History</span>
                                                </center>
                                                <p class="StdTextSmallGrey">
                                                    Invoiced amount (thousands), month to date, for each year.</p>
                                                <asp:Label ID="lblMTDRevHistGraphPd" runat="server" CssClass="StdTextSmallGrey">
                                                </asp:Label><br />
                                                <asp:Label ID="lblMTDRevHistGraphUpdate" runat="server" CssClass="StdTextSmallGrey">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="760">
                            <tr>
                                <td width="400" align="left">
                                    <asp:Label ID="Label29" runat="server" CssClass="StdTextSmallGreySmallCap">Weekly Billing / Inspector Fees </asp:Label>
                                </td>
                                <td width="360" align="right" class="StdTextSmall">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="width: 15px; height: 15px; background: maroon">
                                                    &nbsp;</div>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label13" runat="server" CssClass="StdTextSmallGrey">Invoiced</asp:Label>&nbsp;/&nbsp;
                                            </td>
                                            <td>
                                                <div style="width: 15px; height: 15px; background: navy;">
                                                    &nbsp;</div>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label28" runat="server" CssClass="StdTextSmallGrey">Insp Chgs</asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <xceedchart:ChartServerControl ID="xcWeeklyRevenue" runat="server" Width="750px">
                                    </xceedchart:ChartServerControl>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>
