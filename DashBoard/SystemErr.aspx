<%@ Page Language="C#" AutoEventWireup="true" Inherits="Util_SystemErr" Codebehind="SystemErr.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="SIBHeader" Src="SIBHeader.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Sutton Inspection Bureau</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />
</head>
<body style="background:Images/bluebkg.jpg; margin:0; ">
    <form id="frmSystemErr" runat="server">
        <uc1:SIBHeader ID="SIBHeader1" runat="server"></uc1:SIBHeader>
        <br/>
        <table cellspacing="0" cellpadding="5" width="500">
            <tr>
                <td width="500" bgcolor="Maroon" height="40">
                    <asp:Label ID="Label2" runat="server" CssClass="StdTextLargeWhiteSmallCap">
							&nbsp;Your request could not be completed
                    </asp:Label>
                </td>
            </tr>
        </table>
        <br/>
        <table width="500">
            <tr>
                <td width="10">
                    &nbsp;</td>
                <td width="490">
                    <asp:Label ID="lblDesc" runat="server" CssClass="StdTextSmall">
							An error occured while processing your request - you may want to try again.
                    </asp:Label>
                </td>
            </tr>
   
        </table>
        <br/>
        <table width="500">
            <tr>
                <td align="center">
                    <asp:LinkButton ID="lbSIBHome" CssClass="LinkSmall" TabIndex="15" CausesValidation="False"
                        OnClick="launchSIBHome" runat="server">
							Sutton Inspection Bureau - Home
                    </asp:LinkButton>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
