using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Globalization;
using System.Threading;


namespace DashboardStats360
{
    public class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        //static string cfg_360ConnStr;
        static string cfg_360UtilConnStr;
        static int cfg_TimeZoneOffset;

        static void Main()
        {
            //string sErr = "None";

            try
            {

                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //  C = weekly client update
                //  E = end of day
                //  F = full
                //if (args.Length < 1)
                //{
                //    foreach (string s in args)
                //    {
                //        sErr += s + " - ";
                //    }
                //    throw new SystemException("Invalid number of arguments supplied: " + sErr);
                //}

                //////////////////////////////////////////////////////////

                DateTime dt = new DateTime(); 
                dt = DateTime.Today;

                ///////////////////////////////////////////////////////////////


                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                //cfg_360ConnStr = colNameVal.Get("360ConnStr");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));

                //*****************************//
                // Called from DashboardStats  //
                //*****************************//
                
                //buildHistoricalData();

                //if (args[0].ToUpper() == "C")
                //{

                //    DateTime dCHDate = DateTime.Today;
                //    buildClientHist(dCHDate);
                //}
                //else if (args[0].ToUpper() == "E")
                //{
                //    /////////////////////////////////////
                //    // CALL AFTER MIDNIGHT
                //    DateTime dDate = DateTime.Today.AddDays(-1);

                //    //Day(dDate);
                //    //Week(dDate);
                //    //Month(dDate);
                //    //SalesHist(dDate);
                //}
                //else if (args[0].ToUpper() == "F")
                //{
                //    DateTime dDate = DateTime.Today;
                    
                //    ////Full();
                //    //Day(dDate);
                //    //Week(dDate);
                //    //Month(dDate);
                //}
                //else
                //{
                //    throw new SystemException("Invalid arguments supplied: " + args[0].ToUpper());
                //}
 
            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

   
        public void Full()
        {

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");
            //cfg_360ConnStr = colNameVal.Get("360ConnStr");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");

            String sStatDate = DateTime.Now.ToShortDateString();
            DateTime dStatDate = DateTime.Parse(sStatDate);
            String sStatTime = DateTime.Now.ToShortTimeString();
            DateTime dStatDatePlus1 = dStatDate.AddDays(1);


            int iKeyNumber = 0;
            //int iAcnt = 0;
            //string sInsp = "";
            int iRet = 0;
            int idaysold = 0;
            int iNotAssigned = 0;

            int itotalopen = 0;
            int iunder30 = 0;
            int i30to59 = 0;
            int i60to89 = 0;
            int i90to120 = 0;
            int iover120 = 0;
            int iInReview = 0;
            int iPendingAssign = 0;
            int iOldestCaseDays = 0;
            int iOldestCaseKey = 0;
            int iNewCases = 0;
            int iCompleted = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Full ++++");

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "dbo.sp_DBStats_GetOpenCases";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // Get all open keys 
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through open keys
                    do
                    {

                        //case number
                        if (sqlReader.IsDBNull(0))
                        {
                            throw new SystemException("SQL returned empty Case#");
                        }
                        else
                        {
                            iKeyNumber = (int)sqlReader.GetSqlInt32(0);
                        }

                        //daysold
                        if (sqlReader.IsDBNull(2))
                        {
                            throw new SystemException("SQL returned empty DaysOld");
                        }
                        else
                        {
                            idaysold = (int)sqlReader.GetSqlInt32(2);
                        }

                        // Hold 0r Ordered = not assigned
                        iNotAssigned = (int)sqlReader.GetSqlInt32(1);

                        // Pending assign
                        if (iNotAssigned == 1)
                            iPendingAssign += 1;

                        // total open items
                        itotalopen += 1;

                        // oldest case
                        if (idaysold > iOldestCaseDays)
                        {
                            iOldestCaseDays = idaysold;
                            iOldestCaseKey = iKeyNumber;
                        }
                        // increment aging counters
                        if (idaysold < 30)
                        {
                            iunder30 += 1;
                        }
                        else if (idaysold > 29 && idaysold < 60)
                        {
                            i30to59 += 1;
                        }
                        else if (idaysold > 59 && idaysold < 90)
                        {
                            i60to89 += 1;
                        }
                        else if (idaysold > 89 && idaysold < 121)
                        {
                            i90to120 += 1;
                        }
                        else if (idaysold > 120)
                        {
                            iover120 += 1;
                        }

                    } while (sqlReader.Read());     // open keys

                    sqlReader.Close();

                    // get new requests
                    sqlCmd1.CommandText = "sp_Count_Ordered";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@begdate", dStatDate);
                    sqlCmd1.Parameters.AddWithValue("@enddate", dStatDatePlus1);
                    iNewCases = (int)sqlCmd1.ExecuteScalar();

                    // get completed
                    sqlCmd1.CommandText = "sp_Count_Completed";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@begdate", dStatDate);
                    sqlCmd1.Parameters.AddWithValue("@enddate", dStatDatePlus1);
                    iCompleted = (int)sqlCmd1.ExecuteScalar();

                    // get in Review
                    sqlCmd1.CommandText = "sp_Count_IN_QA";
                    sqlCmd1.Parameters.Clear();
                    iInReview = (int)sqlCmd1.ExecuteScalar();

                    // Insert into Dashboard
                    sqlCmd1.CommandText = "sp_Dashboard_Insert";
                    sqlCmd1.Parameters.Clear();

                    sqlCmd1.Parameters.AddWithValue("@statusdate", dStatDate);
                    sqlCmd1.Parameters.AddWithValue("@statustime", sStatTime);
                    sqlCmd1.Parameters.AddWithValue("@opencases", itotalopen);
                    sqlCmd1.Parameters.AddWithValue("@pendingreview", iInReview);
                    sqlCmd1.Parameters.AddWithValue("@pendingassign", iPendingAssign);
                    sqlCmd1.Parameters.AddWithValue("@completed", iCompleted);
                    sqlCmd1.Parameters.AddWithValue("@newcases", iNewCases);
                    sqlCmd1.Parameters.AddWithValue("@oldestopen", iOldestCaseDays);
                    sqlCmd1.Parameters.AddWithValue("@openunder30", iunder30);
                    sqlCmd1.Parameters.AddWithValue("@open30to59", i30to59);
                    sqlCmd1.Parameters.AddWithValue("@open60to89", i60to89);
                    sqlCmd1.Parameters.AddWithValue("@open90to120", i90to120);
                    sqlCmd1.Parameters.AddWithValue("@openover120", iover120);

                    iRet = sqlCmd1.ExecuteNonQuery();

                    if (iRet == 0)
                        throw new SystemException("spDBStat_Insert returned 0 for " + iKeyNumber.ToString());

                }   // has rows

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End Full ----");
                oLU.closeLog();

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();


            }

        
        }




        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;
        
        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;
            
            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }

      
        public void Day(DateTime dDate)
        {

            Main();

            // Creates day row for date passed
            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dDatePlus1 = dDate.AddDays(1);

            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();
            DateTime dStatDatePlus1 = dStatDate.AddDays(1);

            int iNewCases = 0;
            int iCompleted = 0;
            int iReturned = 0;
            int iTraced = 0;
            decimal dAmountInv = 0;
            decimal dInspPaid = 0;
            int iCompLT30 = 0;

            int iRet = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Day ++++");

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get new requests
                sqlCmd1.CommandText = "sp_Count_Ordered";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                iNewCases = (int)sqlCmd1.ExecuteScalar();

                // get completed
                sqlCmd1.CommandText = "sp_Count_Completed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                iCompleted = (int)sqlCmd1.ExecuteScalar();

                // get returned
                sqlCmd1.CommandText = "sp_Count_QAReject";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                iReturned = (int)sqlCmd1.ExecuteScalar();

                //// get traced
                //sqlCmd1.CommandText = "sp_Count_Traced";
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                //iTraced = (int)sqlCmd1.ExecuteScalar();

                // get amount invoiced
                sqlCmd1.CommandText = "sp_Total_Billed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                // get amount paid to the inspectors
                sqlCmd1.CommandText = "sp_Total_Paid";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                //// get compLT30
                sqlCmd1.CommandText = "sp_Count_CompLT30";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dDatePlus1);
                iCompLT30 = (int)sqlCmd1.ExecuteScalar();
                                
                sqlCmd1.CommandText = "sp_Dashboard_Stats_Overwrite";
                sqlCmd1.Parameters.Clear();

                sqlCmd1.Parameters.AddWithValue("@stattype", "D");
                sqlCmd1.Parameters.AddWithValue("@statusdate", ChangeTime(dStatDate, 0, 0, 0, 0));
                sqlCmd1.Parameters.AddWithValue("@statusperiod", ChangeTime(dDate, 0, 0, 0, 0));
                sqlCmd1.Parameters.AddWithValue("@statustime", sStatTime);
                sqlCmd1.Parameters.AddWithValue("@newcases", iNewCases);
                sqlCmd1.Parameters.AddWithValue("@completed", iCompleted);
                sqlCmd1.Parameters.AddWithValue("@returneditems", iReturned);
                sqlCmd1.Parameters.AddWithValue("@traceditems", iTraced);
                sqlCmd1.Parameters.AddWithValue("@amtinvoiced", dAmountInv);
                sqlCmd1.Parameters.AddWithValue("@insppaid", dInspPaid);
                sqlCmd1.Parameters.AddWithValue("@complt30", iCompLT30);

                iRet = sqlCmd1.ExecuteNonQuery();

                if (iRet == 0)
                    throw new SystemException("sp_Dashboard_Stats_Overwrite returned 0");


            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End Day ----");
                oLU.closeLog();

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }
        }


        public void Week(DateTime dDate)
        {
            Main();

            // Creates week row for week containing date passed
            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dBegOfWeek = FirstDayofWeek(dDate);
            DateTime dEndOfWeek = dBegOfWeek.AddDays(6);

            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iNewCases = 0;
            int iCompleted = 0;
            int iReturned = 0;
            int iTraced = 0;
            decimal dAmountInv = 0;
            decimal dInspPaid = 0;
            int iCompLT30 = 0;

            int iRet = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Week ++++");

                // set up SQL connection (SIBTBL)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get new requests
                sqlCmd1.CommandText = "sp_Count_Ordered";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                iNewCases = (int)sqlCmd1.ExecuteScalar();

                // get completed
                sqlCmd1.CommandText = "sp_Count_Completed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                iCompleted = (int)sqlCmd1.ExecuteScalar();

                // get returned
                sqlCmd1.CommandText = "sp_Count_QAReject";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                iReturned = (int)sqlCmd1.ExecuteScalar();

                //// get traced
                //sqlCmd1.CommandText = "sp_Count_Traced";
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                //iTraced = (int)sqlCmd1.ExecuteScalar();

                // get amount invoiced
                sqlCmd1.CommandText = "sp_Total_Billed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                // get amount paid to the inspectors
                sqlCmd1.CommandText = "sp_Total_Paid";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                //// get compLT30
                sqlCmd1.CommandText = "sp_Count_CompLT30";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                iCompLT30 = (int)sqlCmd1.ExecuteScalar();


                sqlCmd1.CommandText = "sp_Dashboard_Stats_Overwrite";
                sqlCmd1.Parameters.Clear();

                sqlCmd1.Parameters.AddWithValue("@stattype", "W");
                sqlCmd1.Parameters.AddWithValue("@statusdate", ChangeTime(dStatDate, 0, 0, 0, 0));
                sqlCmd1.Parameters.AddWithValue("@statusperiod", ChangeTime(dBegOfWeek, 0, 0, 0, 0));
                sqlCmd1.Parameters.AddWithValue("@statustime", sStatTime);
                sqlCmd1.Parameters.AddWithValue("@newcases", iNewCases);
                sqlCmd1.Parameters.AddWithValue("@completed", iCompleted);
                sqlCmd1.Parameters.AddWithValue("@returneditems", iReturned);
                sqlCmd1.Parameters.AddWithValue("@traceditems", iTraced);
                sqlCmd1.Parameters.AddWithValue("@amtinvoiced", dAmountInv);
                sqlCmd1.Parameters.AddWithValue("@insppaid", dInspPaid);
                sqlCmd1.Parameters.AddWithValue("@complt30", iCompLT30);

                iRet = sqlCmd1.ExecuteNonQuery();

                if (iRet == 0)
                    throw new SystemException("sp_Dashboard_Stats_Overwrite returned 0");


            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End Week ----");
                oLU.closeLog();

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }
                      

        }


        public void Month(DateTime dDate)
        {

            Main();

            // Creates month row for month containing date passed
            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dBegOfMonth = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth = LastDayOfMonth(dDate);

            // set time to 12:00am
            dBegOfMonth = ChangeTime(dBegOfMonth, 0, 0, 0, 0);

            // set time to midnight
            dEndOfMonth = ChangeTime(dEndOfMonth, 23,59, 59, 0);


            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iNewCases = 0;
            int iCompleted = 0;
            int iReturned = 0;
            int iTraced = 0;
            decimal dAmountInv = 0;
            decimal dInspPaid = 0;
            int iCompLT30 = 0;

            int iRet = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Month ++++");

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get new requests
                sqlCmd1.CommandText = "sp_Count_Ordered";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                iNewCases = (int)sqlCmd1.ExecuteScalar();

                // get completed
                sqlCmd1.CommandText = "sp_Count_Completed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                iCompleted = (int)sqlCmd1.ExecuteScalar();

                // get returned
                sqlCmd1.CommandText = "sp_Count_QAReject";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                iReturned = (int)sqlCmd1.ExecuteScalar();

                //// get traced
                //sqlCmd1.CommandText = "sp_Count_Traced";
                //sqlCmd1.Parameters.Clear();
                //sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                //sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                //iTraced = (int)sqlCmd1.ExecuteScalar();

                // get amount invoiced
                sqlCmd1.CommandText = "sp_Total_Billed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                // get amount paid to the inspectors
                sqlCmd1.CommandText = "sp_Total_Paid";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                //// get compLT30
                sqlCmd1.CommandText = "sp_Count_CompLT30";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                iCompLT30 = (int)sqlCmd1.ExecuteScalar();
                
                sqlCmd1.CommandText = "sp_Dashboard_Stats_Overwrite";
                sqlCmd1.Parameters.Clear();

                sqlCmd1.Parameters.AddWithValue("@stattype", "M");
                sqlCmd1.Parameters.AddWithValue("@statusdate", ChangeTime(dStatDate, 0, 0, 0, 0));
                sqlCmd1.Parameters.AddWithValue("@statusperiod", ChangeTime(dBegOfMonth, 0, 0, 0, 0));
                sqlCmd1.Parameters.AddWithValue("@statustime", sStatTime);
                sqlCmd1.Parameters.AddWithValue("@newcases", iNewCases);
                sqlCmd1.Parameters.AddWithValue("@completed", iCompleted);
                sqlCmd1.Parameters.AddWithValue("@returneditems", iReturned);
                sqlCmd1.Parameters.AddWithValue("@traceditems", iTraced);
                sqlCmd1.Parameters.AddWithValue("@amtinvoiced", dAmountInv);
                sqlCmd1.Parameters.AddWithValue("@insppaid", dInspPaid);
                sqlCmd1.Parameters.AddWithValue("@complt30", iCompLT30);


                iRet = sqlCmd1.ExecuteNonQuery();

                if (iRet == 0)
                    throw new SystemException("sp_Dashboard_Stats_Overwrite returned 0");

                oLU.WritetoLog("Date range: " + dBegOfMonth.ToString() + " - " + dEndOfMonth.ToString());

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End Month ----");
                oLU.closeLog();

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

        }

        public decimal TotalBilledFirstPd(DateTime dBegDate, DateTime dEndDate, bool bIncludeUnbilled)
        {

            Main();

            // Add 1 day to end date to get items through midnight.
            //dEndDate = dEndDate.AddDays(1);

            decimal decTotBilled = 0;
            decimal decTotBilledNotInv = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get amount invoiced
                sqlCmd1.CommandText = "sp_Total_Invoiced";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                decTotBilled = (decimal)sqlCmd1.ExecuteScalar();

                if (bIncludeUnbilled)
                {
                    // get amount waiting to be invoiced
                    sqlCmd1.CommandText = "sp_Total_Billed_NotInv";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                    sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                    decTotBilledNotInv = (decimal)sqlCmd1.ExecuteScalar();

                    decTotBilled += decTotBilledNotInv;
                }

            }

            catch (Exception ex)
            {

                decTotBilled = 0;
            }

            finally
            {

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }


            return decTotBilled;
        }


        public decimal TotalBilledSecondPd(DateTime dBegDate, DateTime dEndDate, bool bIncludeUnbilled)
        {

            // Sum total items INVOICED for 16 - EOM
            // sum total non invoiced items for entire month (-3 days) to account
            // for clients that are only billed once per month.

            Main();

            // Add 1 day to end date to get items through midnight.
            //dEndDate = dEndDate.AddDays(1);

            decimal decTotBilled = 0;
            decimal decTotBilledNotInv = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get amount invoiced
                sqlCmd1.CommandText = "sp_Total_Invoiced";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                decTotBilled = (decimal)sqlCmd1.ExecuteScalar();

                if (bIncludeUnbilled)
                {
                    //get amount waiting to be invoiced
                    //dBegDate = FirstDayOfMonth(dBegDate);
                    sqlCmd1.CommandText = "sp_Total_Billed_NotInv";
                    sqlCmd1.Parameters.Clear();
                    //sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate.AddDays(-3));
                    sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                    sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                    decTotBilledNotInv = (decimal)sqlCmd1.ExecuteScalar();

                    decTotBilled += decTotBilledNotInv;
                }
            }

            catch (Exception ex)
            {

                decTotBilled = 0;
            }

            finally
            {

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }


            return decTotBilled;
        }

        public int CountBilledSecondPd(DateTime dBegDate, DateTime dEndDate, bool bIncludeUnbilled)
        {

            // Count items INVOICED for 16 - EOM
            // Count total non invoiced items for entire month to account for clients that are only billed once per month.

            Main();

            // Add 1 day to end date to get items through midnight.
            dEndDate = dEndDate.AddDays(1);

            int iTotBilled = 0;
            int iTotBilledNotInv = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get number invoiced
                sqlCmd1.CommandText = "sp_Count_Invoiced";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                iTotBilled = (int)sqlCmd1.ExecuteScalar();

                if (bIncludeUnbilled)
                {
                    //get amount waiting to be invoiced
                    //dBegDate = FirstDayOfMonth(dBegDate);
                    sqlCmd1.CommandText = "sp_Count_Billed_NotInv";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                    sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                    iTotBilledNotInv = (int)sqlCmd1.ExecuteScalar();

                    iTotBilled += iTotBilledNotInv;
                }
            }

            catch (Exception ex)
            {
                iTotBilled = 0;
            }

            finally
            {

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }


            return iTotBilled;
        }


        public void Period(DateTime dDate)
        {

            Main();

            // Creates period row for period (1/15 - 16-EOM) containing date passed
            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dBegOfMonth = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth = LastDayOfMonth(dDate);
            DateTime dBegOfPeriod;
            DateTime dEndOfPeriod;

            // set period dates
            if (dDate.Day < 16)
                dBegOfPeriod = dBegOfMonth;
            else
                dBegOfPeriod = dBegOfMonth.AddDays(15);

            // End of period = actual end date plus 1 to account for all items through midnight on the last day
            if (dDate.Day < 16)
                dEndOfPeriod = dBegOfMonth.AddDays(15);
            else
                dEndOfPeriod = dEndOfMonth.AddDays(1);


            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iNewCases = 0;
            int iCompleted = 0;
            int iReturned = 0;
            int iTraced = 0;
            decimal dAmountInv = 0;
            decimal dInspPaid = 0;
            int iCompLT30 = 0;

            int iRet = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Period ++++");

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();


                // get amount invoiced
                sqlCmd1.CommandText = "sp_Total_Invoiced";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfPeriod);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfPeriod);
                dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                // if invoiced = 0 get amount waiting to be invoiced
                if (dAmountInv == 0)
                {
                    sqlCmd1.CommandText = "sp_Total_Billed";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfPeriod);
                    sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfPeriod);
                    dAmountInv = (decimal)sqlCmd1.ExecuteScalar();
                
                }

                // get amount paid to the inspectors
                sqlCmd1.CommandText = "sp_Total_Paid";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfPeriod);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfPeriod);
                dInspPaid = (decimal)sqlCmd1.ExecuteScalar();


                sqlCmd1.CommandText = "sp_Dashboard_Stats_Overwrite";
                sqlCmd1.Parameters.Clear();

                sqlCmd1.Parameters.AddWithValue("@stattype", "P");
                sqlCmd1.Parameters.AddWithValue("@statusdate", ChangeTime(dStatDate, 0, 0, 0, 0));
                sqlCmd1.Parameters.AddWithValue("@statusperiod", ChangeTime(dBegOfPeriod, 0, 0, 0, 0));
                sqlCmd1.Parameters.AddWithValue("@statustime", sStatTime);
                sqlCmd1.Parameters.AddWithValue("@newcases", iNewCases);
                sqlCmd1.Parameters.AddWithValue("@completed", iCompleted);
                sqlCmd1.Parameters.AddWithValue("@returneditems", iReturned);
                sqlCmd1.Parameters.AddWithValue("@traceditems", iTraced);
                sqlCmd1.Parameters.AddWithValue("@amtinvoiced", dAmountInv);
                sqlCmd1.Parameters.AddWithValue("@insppaid", dInspPaid);
                sqlCmd1.Parameters.AddWithValue("@complt30", iCompLT30);


                iRet = sqlCmd1.ExecuteNonQuery();

                if (iRet == 0)
                    throw new SystemException("sp_Dashboard_Stats_Overwrite returned 0");

                oLU.WritetoLog("Date range: " + dBegOfMonth.ToString() + " - " + dEndOfMonth.ToString());

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End Period ----");
                oLU.closeLog();

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

        }


         public void buildHistoricalData()
         {

             try
             {

                 DateTime dCurDate = new DateTime(2012, 1, 1);
                 DateTime dEndDate = new DateTime(2013, 7, 2);
                 do
                 {

                     updateCompLT30(dCurDate);
                     dCurDate = dCurDate.AddMonths(1);

                 } while (dCurDate < dEndDate);
             }
             catch (Exception ex)
             {

                 //record exception  

             }

          }

         static void updateCompLT30(DateTime dDate)
         {

             // update compLT30 in DBStat table

             Main();

             CultureInfo info = Thread.CurrentThread.CurrentCulture;
             DateTime dBegOfMonth = FirstDayOfMonth(dDate);
             DateTime dEndOfMonth = LastDayOfMonth(dDate);

             // set time to 12:00am
             dBegOfMonth = ChangeTime(dBegOfMonth, 0, 0, 0, 0);

             // set time to midnight
             dEndOfMonth = ChangeTime(dEndOfMonth, 23, 59, 59, 0);


             int iCompLT30 = 0;
              int iRet = 0;

             SqlConnection sqlConn1 = null;
             SqlCommand sqlCmd1 = null;
             SqlConnection sqlConn2 = null;
             SqlCommand sqlCmd2 = null;

             try
             {

                 // set up SQL connection 
                 sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                 sqlCmd1 = new SqlCommand();
                 sqlCmd1.CommandType = CommandType.StoredProcedure;
                 sqlCmd1.Connection = sqlConn1;
                 sqlConn1.Open();

                 //// get new
                 //sqlCmd1.CommandText = "sp_Count_NewReq_Range";
                 //sqlCmd1.Parameters.Clear();
                 //sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                 //sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                 //iNew = (int)sqlCmd1.ExecuteScalar();

                 //// get comp
                 //sqlCmd1.CommandText = "sp_Count_Completed_Range";
                 //sqlCmd1.Parameters.Clear();
                 //sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                 //sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                 //iComp = (int)sqlCmd1.ExecuteScalar();


                 // get compLT30
                 sqlCmd1.CommandText = "sp_Count_CompLT30";
                 sqlCmd1.Parameters.Clear();
                 sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                 sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                 iCompLT30 = (int)sqlCmd1.ExecuteScalar();


                 //// get amount invoiced
                 //sqlCmd1.CommandText = "sp_Sum_InvAmt_Range";
                 //sqlCmd1.Parameters.Clear();
                 //sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                 //sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                 //dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                 //// get amount paid to the inspectors
                 //sqlCmd1.CommandText = "sp_Sum_InspChgs_Range";
                 //sqlCmd1.Parameters.Clear();
                 //sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                 //sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);
                 //dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                 sqlConn1.Close();

                 // set up SQL connection 
                 sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                 sqlCmd2 = new SqlCommand();

                 sqlCmd2.CommandType = CommandType.Text;
                 sqlCmd2.CommandText = "UPDATE Dashboard_Stats SET complt30 = " + iCompLT30 + " WHERE (stattype = 'M' AND statusperiod = '" + dBegOfMonth + "')";
                 sqlCmd2.Connection = sqlConn2;
                 sqlConn2.Open();
                 iRet = sqlCmd2.ExecuteNonQuery();

             }

             catch (Exception ex)
             {

                 //record exception  
                 Console.WriteLine(ex.Message);

             }

             finally
             {

                 if (sqlConn1 != null)
                     sqlConn1.Close();

                 if (sqlConn2 != null)
                     sqlConn2.Close();

             }


         }


         static void buildRevenueHist(DateTime dDate)
         {

             // update invamt and insppaid in DBStat table

             // Get first day (Sunday) of current week
             CultureInfo info = Thread.CurrentThread.CurrentCulture;
             DateTime dBegOfWeek = FirstDayofWeek(dDate);

             // First day of previous week
             dBegOfWeek = dBegOfWeek.AddDays(-7);

             DateTime dEndOfWeek = dBegOfWeek.AddDays(6);

             decimal dAmountInv = 0;
             decimal dInspPaid = 0;
             int iRet = 0;

             SqlConnection sqlConn1 = null;
             SqlCommand sqlCmd1 = null;
             SqlConnection sqlConn2 = null;
             SqlCommand sqlCmd2 = null;

             try
             {

                 // set up SQL connection 
                 sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                 sqlCmd1 = new SqlCommand();
                 sqlCmd1.CommandType = CommandType.StoredProcedure;
                 sqlCmd1.Connection = sqlConn1;
                 sqlConn1.Open();

                 // get amount invoiced
                 sqlCmd1.CommandText = "sp_Sum_InvAmt_Range";
                 sqlCmd1.Parameters.Clear();
                 sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                 sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                 dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                 // get amount paid to the inspectors
                 sqlCmd1.CommandText = "sp_Sum_InspChgs_Range";
                 sqlCmd1.Parameters.Clear();
                 sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                 sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                 dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                 sqlConn1.Close();

                 // set up SQL connection 
                 sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                 sqlCmd2 = new SqlCommand();

                 sqlCmd2.CommandType = CommandType.Text;
                 sqlCmd2.CommandText = "UPDATE Dashboard_Stats SET amtinvoiced = " + dAmountInv + ", insppaid = " + dInspPaid + " WHERE (stattype = 'M' AND statusperiod = '" + dBegOfWeek + "')";
                 sqlCmd2.Connection = sqlConn2;
                 sqlConn2.Open();
                 iRet = sqlCmd2.ExecuteNonQuery();

             }

             catch (Exception ex)
             {

                 //record exception  
                 Console.WriteLine(ex.Message);

             }

             finally
             {

                 if (sqlConn1 != null)
                     sqlConn1.Close();

                 if (sqlConn2 != null)
                     sqlConn2.Close();

             }         
         
         
         }

         public void SalesHist(DateTime dDate)
         {

             Main();

             // Cauculate sales history and insert into Dashboard_Sales_Hist
             CultureInfo info = Thread.CurrentThread.CurrentCulture;
             DateTime dBegDate = FirstDayOfMonth(dDate);
             DateTime dEndDate = dDate.AddDays(1);

             int iRet = 0;

             SqlConnection sqlConn2 = null;
             SqlCommand sqlCmd2 = null;

             try
             {

                 // initialize log file class
                 oLU = new LogUtils.LogUtils();

                 // set log file name
                 oLU.logFileName = cfg_logfilename;

                 // open log file
                 oLU.OpenLog();
                 oLU.WritetoLog("++++ Begin SalesHist360 ++++");

                 // set up SQL connection 
                 sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                 sqlCmd2 = new SqlCommand();
                 sqlCmd2.Connection = sqlConn2;
                 sqlConn2.Open();

                 sqlCmd2.CommandType = CommandType.StoredProcedure;
                 sqlCmd2.CommandText = "sp_Dashboard_SalesHist_Update";
                 sqlCmd2.Connection = sqlConn2;
                 sqlCmd2.CommandType = CommandType.StoredProcedure;
                 sqlCmd2.Parameters.Clear();

                 sqlCmd2.Parameters.AddWithValue("@begdate", dBegDate);
                 sqlCmd2.Parameters.AddWithValue("@enddate", dEndDate);

                 iRet = sqlCmd2.ExecuteNonQuery();

                 if (iRet == 0)
                     throw new SystemException("sp_Dashboard_SalesHist_Update");

                 oLU.WritetoLog("Date range: " + dBegDate.ToString() + " - " + dEndDate.ToString());

             }

             catch (Exception ex)
             {

                 //record exception  
                 oLU.WritetoLog(ex.Message);

             }

             finally
             {

                 // close objects
                 oLU.WritetoLog("---- End SalesHist360 ----");
                 oLU.closeLog();

                 if (sqlConn2 != null)
                     sqlConn2.Close();

             }


         }


        public void MonthlySalesHist(DateTime dDate)
        {

            Main();

            // Cauculate sales history and insert into Dashboard_Sales_Hist
            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            int iRet = 0;

            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            DateTime dBegDate = FirstDayOfMonth(dDate);
            DateTime dEndDate = dDate.AddDays(1);

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin MonthlySalesHist360 ++++");
                
                // set up SQL connection 
                sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd2 = new SqlCommand();
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                // Clear table
                sqlCmd2.CommandType = CommandType.Text;
                sqlCmd2.CommandText = "DELETE FROM dbo.Dashboard_MonthlySalesHist";
                sqlCmd2.Connection = sqlConn2;
                iRet = sqlCmd2.ExecuteNonQuery();

                // set up for sp
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "sp_Dashboard_MonthlySalesHist_Update";
                sqlCmd2.Connection = sqlConn2;

                // set session end date 12 months prior
                DateTime dEndOfRun = dDate.AddMonths(-13);

                int iCurMonth = 1;
                
                do
                {
                    sqlCmd2.Parameters.Clear();
                    sqlCmd2.Parameters.AddWithValue("@begdate", dBegDate);
                    sqlCmd2.Parameters.AddWithValue("@enddate", dEndDate);
                    sqlCmd2.Parameters.AddWithValue("@curmonth", iCurMonth);
                    iCurMonth = 0;
                    
                    iRet = sqlCmd2.ExecuteNonQuery();

                    if (iRet == 0)
                        throw new SystemException("sp_Dashboard_MonthlySalesHist_Update");

                    oLU.WritetoLog("Date range: " + dBegDate.ToString() + " - " + dEndDate.ToString());

                    dBegDate = dBegDate.AddMonths(-1);
                    dEndDate = LastDayOfMonth(dBegDate);
                    dEndDate = dEndDate.AddHours(cfg_TimeZoneOffset);  // handle 360 GMT
                    dEndDate = dEndDate.AddDays(1);  // handle 360 GMT

                } while (dBegDate > dEndOfRun);

   
            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End MonthlySalesHist360 ----");
                oLU.closeLog();

                if (sqlConn2 != null)
                    sqlConn2.Close();

            }


        }
        static void buildClientHist(DateTime dDate)
        {
            // Calculate weekly data and insert into ClientHistory table
            // Creates data for week prior to date passed
            // Should be called on Sunday to calc data for previous week

            // Runs query returning all accounts with completed reports during the period
            // Calls processClient to build row in clienthstory table

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            int iAcnt = 0;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin ClientHist ++++");

                CultureInfo info = Thread.CurrentThread.CurrentCulture;

                // Get first day (Sunday) of current week
                DateTime dBegOfWeek = FirstDayofWeek(dDate);

                // First day of previous week
                dBegOfWeek = dBegOfWeek.AddDays(-7);

                DateTime dEndOfWeek = dBegOfWeek.AddDays(6);

                oLU.WritetoLog("Period: " + dBegOfWeek.ToShortDateString() + " - " + dEndOfWeek.ToShortDateString());

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // set up SQL connection 
                sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd2 = new SqlCommand();
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.Connection = sqlConn2;
                sqlCmd2.CommandText = "sp_ClientHistBuild_Get";
                sqlCmd2.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd2.Parameters.AddWithValue("@enddate", dEndOfWeek);
                sqlConn2.Open();

                SqlDataReader sqlReader = null;
                // Get all open keys from SibTBL
                sqlReader = sqlCmd2.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through open keys
                    do
                    {

                        //acnt
                        if (!sqlReader.IsDBNull(0))
                        {
                            iAcnt = (int)sqlReader.GetSqlInt32(0);
                            processClient(dBegOfWeek, dEndOfWeek, iAcnt, sqlConn1, sqlCmd1);
                        }

                    } while (sqlReader.Read());      

                    sqlReader.Close();
                }
            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End Client Hist ----");
                oLU.closeLog();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn2.Close();

            }
        }

        static void processClient(DateTime dBegOfWeek, DateTime dEndOfWeek, int iAcnt, SqlConnection sqlConn1, SqlCommand sqlCmd1)
        {

            int iNewCases = 0;
            int iCompleted = 0;
            decimal dAmountInv = 0;
            decimal dInspPaid = 0;
            int iRet = 0;

            try
            {

                // get new requests
                sqlCmd1.CommandText = "sp_Count_NewReq_Acnt_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                sqlCmd1.Parameters.AddWithValue("@acnt", iAcnt);
                iNewCases = (int)sqlCmd1.ExecuteScalar();

                // get completed
                sqlCmd1.CommandText = "sp_Count_Completed_Acnt_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                sqlCmd1.Parameters.AddWithValue("@acnt", iAcnt);
                iCompleted = (int)sqlCmd1.ExecuteScalar();

                // get amount invoiced
                sqlCmd1.CommandText = "sp_Sum_InvAmt_Acnt_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                sqlCmd1.Parameters.AddWithValue("@acnt", iAcnt);
                dAmountInv = (decimal)sqlCmd1.ExecuteScalar();

                // get amount paid to the inspectors
                sqlCmd1.CommandText = "sp_Sum_InspChgs_Acnt_Range";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfWeek);
                sqlCmd1.Parameters.AddWithValue("@acnt", iAcnt);
                dInspPaid = (decimal)sqlCmd1.ExecuteScalar();

                // Insert record into ClientHistory
                sqlCmd1.CommandText = "sp_ClientHistory_Insert";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@acntnum", iAcnt);
                sqlCmd1.Parameters.AddWithValue("@weekbeginning", dBegOfWeek);
                sqlCmd1.Parameters.AddWithValue("@requested", iNewCases);
                sqlCmd1.Parameters.AddWithValue("@completed", iCompleted);
                sqlCmd1.Parameters.AddWithValue("@invoicedamount", dAmountInv);
                sqlCmd1.Parameters.AddWithValue("@inspcharges", dInspPaid);

                iRet = sqlCmd1.ExecuteNonQuery();

                if (iRet == 0)
                    throw new SystemException("sp_ClientHistory_Insert returned 0 - acnt#: " + iAcnt.ToString());


            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }
        
        }

        public int CountCompleted(DateTime dBegDate)
        {

            // Count number completed for given day

            Main();

            // Add 1 day to end date to get items through midnight.
            DateTime dEndDate = dBegDate.AddDays(1);

            int iNumComp = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get number completed
                sqlCmd1.CommandText = "sp_Count_Completed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                iNumComp = (int)sqlCmd1.ExecuteScalar();

            }

            catch (Exception ex)
            {

                iNumComp = 0;
            }

            finally
            {

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }
            
            return iNumComp;
        }

        public decimal TotalBilled(DateTime dBegDate)
        {

            // Total amount billed for given day

            Main();

            // Add 1 day to end date to get items through midnight.
            DateTime dEndDate = dBegDate.AddDays(1);

            decimal decTotBilled = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get number completed
                sqlCmd1.CommandText = "sp_Total_Billed";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                decTotBilled = (decimal)sqlCmd1.ExecuteScalar();

            }

            catch (Exception ex)
            {

                decTotBilled = 0;
            }

            finally
            {

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

            return decTotBilled;
        }

        public decimal OutstandingPR()
        {

            // Total outstanding payroll

            Main();


            decimal dPR= 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get number completed
                sqlCmd1.CommandText = "sp_Total_Outstanding_Payroll";
                sqlCmd1.Parameters.Clear();
                dPR = (decimal)sqlCmd1.ExecuteScalar();

            }

            catch (Exception ex)
            {

                dPR = 0;
            }

            finally
            {

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

            return dPR;
        }

        public int OpenItems()
        {

            // Total open cases

            Main();


            int iOpen = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // set up SQL connection 
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // get number completed
                sqlCmd1.CommandText = "sp_DBStats_CountOpenCases";
                sqlCmd1.Parameters.Clear();
                iOpen = (int)sqlCmd1.ExecuteScalar();

            }

            catch (Exception ex)
            {

                iOpen = 0;
            }

            finally
            {

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

            return iOpen;
        }
    }
            
    }



