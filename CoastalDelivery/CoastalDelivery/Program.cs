﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Word = Microsoft.Office.Interop.Word;
using WinSCP;


namespace CoastalDelivery
{
    class Program
    {

        static void Main(string[] args)
        {

            Setup setup = new Setup();

        }


        /// <summary>
        /// Instantiate properties from the AppSettings located in the App.config.
        /// </summary>
        public class Setup
        {

            private string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
            private string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
            private string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            private string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
            private string cfg_recroot = ConfigurationManager.AppSettings["RecRoot"];
            private string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
            private string cfg_exportapp = ConfigurationManager.AppSettings["ExportApp"];
            private string cfg_recapp = ConfigurationManager.AppSettings["RecApp"];
            private string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];

            private LogUtils.LogUtils oLU;
            private bool bErr = false;
            private string sPolicy = "";
            private string sInsured = "";
            private string sCaseNum = "";

            //Used for email message body.  
            private StringBuilder sbEmail = new StringBuilder();

            public Setup()
            {
                int iCasesExported = 0;

                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                bool bExport = true;
                bool bCreateRecs = true;
                bool bRenameFiles = true;
                bool bFTPFiles = true;

                DirectoryInfo diPDF;
                DirectoryInfo diCaseFiles;

                FileInfo[] fiPDFFiles;
                FileInfo fiRec;
                string sNewRecDoc = "";
                string sNewPDF = "";

                sbEmail.Append("Begin Processing  " + DateTime.Now.ToString() + "\r\n\r\n");

                //***********************************************************
                // Export PDF's from 360
                if (bExport)
                {
                    try
                    {

                        // Delete all files in PDF Export directory.
                        string[] sFiles = Directory.GetFiles(cfg_pdfroot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);

                        // Export PDF's from 360
                        sbEmail.Append("---- Begin exporting completed cases from 360\r\n\r\n");

                        var proc = Process.Start(cfg_exportapp);
                        proc.WaitForExit();
                        var exitCode = proc.ExitCode;
                        sbEmail.Append("Exporter returned: " + exitCode.ToString() + "\r\n");

                        // exit code should be the number of cases exported
                        iCasesExported = (int)exitCode;

                        Console.WriteLine(exitCode.ToString());

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR IN EXPORT:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                    }

                    diPDF = new DirectoryInfo(cfg_pdfroot);
                    fiPDFFiles = diPDF.GetFiles("*.pdf");

                    if (fiPDFFiles.Count() != iCasesExported)
                    {
                        sbEmail.Append("**** PDF count does not match number exported ****\r\n");
                        sbEmail.Append("Exported: " + iCasesExported.ToString() + " - In folder: " + fiPDFFiles.Count().ToString() + "\r\n\r\n");
                    }

                }


                if (bCreateRecs && !bErr)
                {

                    //***********************************************************
                    // Build Rec docs for all PDF files that were extracted


                    //************************************************
                    // Test for connection to casefiles
                    //************************************************
                    try
                    {
                        diCaseFiles = new DirectoryInfo(cfg_casefilesroot);
                        if (!diCaseFiles.Exists)
                            throw new ApplicationException("Unable to access case files root: " + cfg_casefilesroot + "\r\n");

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR ACCESSING CASEFILES FOLDER:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                    }

                    if (!bErr)
                    {

                        try
                        {
                            sbEmail.Append("\r\n\r\n---- Begin creating REC Docs\r\n");

                            // Delete all files in Rec folder.
                            string[] sFiles = Directory.GetFiles(cfg_recroot);
                            foreach (string sFile in sFiles)
                                File.Delete(sFile);


                            diPDF = new DirectoryInfo(cfg_pdfroot);
                            fiPDFFiles = diPDF.GetFiles("*.pdf");

                            foreach (FileInfo file in fiPDFFiles)
                            {

                                sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));

                                ProcessStartInfo psi = new ProcessStartInfo();
                                psi.FileName = cfg_recapp;
                                psi.Arguments = sCaseNum;

                                var proc1 = Process.Start(psi);
                                proc1.WaitForExit();
                                var exitCode1 = proc1.ExitCode;
                                Console.WriteLine(exitCode1.ToString());

                                // verify rec doc was created
                                fiRec = new FileInfo(cfg_recroot + sCaseNum + "Recs.pdf");
                                if (fiRec.Exists)
                                {
                                    // success
                                    sbEmail.Append("Rec doc created for: " + sCaseNum + "\r\n");
                                }
                                else
                                {
                                    // fail
                                    sbEmail.Append("No rec doc for : " + sCaseNum + "\r\n");
                                }
                                fiRec = null;
                            }

                        }
                        catch (Exception ex)
                        {
                            sbEmail.Append("**** ERROR CREATING REC DOC:\r\n" + ex.Message);
                            oLU.WritetoLog(sbEmail.ToString());
                            bErr = true;
                        }

                    }
                }



                //***********************************************************
                // Rename PDF's to policy
                // Copy recs to pdf folder & rename to policy
                // 
                if (bRenameFiles && !bErr)
                {

                    sbEmail.Append("\r\n\r\n---- Begin renaming reports\r\n");

                    try
                    {

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        // for each PDF 
                        foreach (FileInfo pdfFile in fiPDFFiles)
                        {
                            sCaseNum = pdfFile.Name.Substring(0, pdfFile.Name.IndexOf("."));

                            // get policy #
                            if (!GetCaseInfo(sCaseNum))
                            {
                                sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("GetInfo failed for case: " + sCaseNum);
                            }

                            // Rename case#.pdf to policy.pdf
                            sNewPDF = cfg_pdfroot + sPolicy + ".pdf";

                            // If it exists - add digit to end
                            if (File.Exists(sNewPDF))
                            {

                                int i = 1;
                                while (true)
                                {
                                    sNewPDF = cfg_pdfroot + sPolicy + "~" + i.ToString() + ".pdf";
                                    if (File.Exists(sNewPDF))
                                    {
                                        i++;
                                        if (i > 10)
                                        {
                                            throw new ApplicationException(sNewPDF + " could not be saved - exists.");
                                        }

                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                            }   // file.Exists               

                            // rename PDF
                            pdfFile.MoveTo(sNewPDF);

                            // Copy Rec pdf to pdf folder if present
                            // Rename to policy.pdf
                            fiRec = new FileInfo(cfg_recroot + sCaseNum + "Recs.pdf");
                            if (fiRec.Exists)
                            {
                                sNewRecDoc = cfg_pdfroot + sPolicy + "Recs.pdf";

                                // If it exists - add digit to end
                                if (File.Exists(sNewRecDoc))
                                {

                                    int i = 1;
                                    while (true)
                                    {
                                        sNewRecDoc = cfg_pdfroot + sPolicy + "~" + i.ToString() + "Recs.pdf";
                                        if (File.Exists(sNewRecDoc))
                                        {
                                            i++;
                                            if (i > 10)
                                            {
                                                throw new ApplicationException(sNewRecDoc + " could not be saved - exists.");
                                            }

                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }

                                fiRec.MoveTo(sNewRecDoc);
                            }   // fiRec.Exists               

                        }   // for each pdf

                    }
                    catch (Exception ex)
                    {
                        bErr = true;
                        sbEmail.Append("**** Error processing report for case:" + sCaseNum + "\r\n" + ex.Message + "\r\n\r\n");
                    }
                }   //if (bMailFiles && !bErr)

                if (bFTPFiles)
                {
                    // FTP all files in PDF folder
                    int iNumUploaded = 0;
                    int iNumToUpload = 0;

                    diPDF = new DirectoryInfo(cfg_pdfroot);
                    fiPDFFiles = diPDF.GetFiles("*.*");

                    iNumToUpload = fiPDFFiles.Count();
                    if (iNumToUpload == 0)
                    {
                        sbEmail.Append("\r\n\r\n---- No Files to FTP\r\n");
                    }
                    else
                    {
                        sbEmail.Append("\r\n\r\n---- Begin FTP transfer\r\n");
                        sbEmail.Append("\r\n\r\n" + iNumToUpload.ToString() + " files to upload\r\n");
                        try
                        {
                            // Setup session options
                            SessionOptions sessionOptions = new SessionOptions
                            {
                                Protocol = Protocol.Sftp,
                                HostName = "sftp.ciuins.com",
                                UserName = "sutton",
                                Password = "cQ9D2QFG",
                                SshHostKeyFingerprint = "ssh-rsa 2048 e4:ba:36:df:a6:e6:ba:6c:57:52:44:3a:2a:2c:ba:f3"
                            };

                            using (Session session = new Session())
                            {

                                session.SessionLogPath = @"c:\automationlogs\Coastal\FTPLogs.txt";

                                // Connect
                                session.Open(sessionOptions);

                                // Upload files
                                TransferOptions transferOptions = new TransferOptions();
                                transferOptions.TransferMode = TransferMode.Binary;

                                TransferOperationResult transferResult;
                                transferResult = session.PutFiles(cfg_pdfroot + "*", "/", false, transferOptions);

                                // Throw on any error
                                transferResult.Check();

                                // Print results
                                foreach (TransferEventArgs transfer in transferResult.Transfers)
                                {
                                    sbEmail.Append(transfer.FileName + " uploaded");
                                    iNumUploaded++;
                                }

                            }

                            sbEmail.Append("\r\n" + iNumUploaded.ToString() + " files uploaded\r\n");
                            if (iNumToUpload != iNumUploaded)
                            {
                                throw new ApplicationException("FTP Upload discrepancy\r\n\r\n# to Upload" + iNumToUpload.ToString() + "  # uploaded: " + iNumUploaded.ToString());
                            }
                            sbEmail.Append("\r\n---- End FTP transfer\r\n");

                        }
                        catch (Exception ex)
                        {
                            bErr = true;
                            sbEmail.Append("**** Error in FTP Transfer\r\n\r\n" + ex.Message + "\r\n\r\n");
                        }

                    }   // files to upload

                }   //bFTPFiles


                // Done - write to log, send email  and clean up
                sbEmail.Append("\r\nEnd Processing  " + DateTime.Now.ToString() + "\r\n\r\n");
                oLU.WritetoLog(sbEmail.ToString());
                oLU.closeLog();

                if (bErr)
                {
                    sendLogEmail("**** ERROR RECORDED - Coastal Delivery Processing", sbEmail.ToString());
                }
                else
                {
                    sendLogEmail("Coastal Delivery Processing", sbEmail.ToString());
                }
            }

            private void sendCaseEmail(string sSubject, string sPDF, string sRec)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
                string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "sibflamail@sibfla.com";
                    oMail.MailTo = "submission@ciuins.com";
                    //oMail.MailBCC = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    oMail.Attachment = sPDF;
                    oMail.Attachment2 = sRec;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

            }



            private void sendLogEmail(string sSubject, string sBodyText)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "sibflamail@sibfla.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }


            }

            private bool GetCaseInfo(string sCaseNum)
            {

                bool bRetVal = false;

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseInfo_CaseNum";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {

                            sInsured = sqlReader.GetSqlString(2).ToString();
                            sPolicy = sqlReader.GetSqlString(3).ToString();

                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }
                    else // nothing returned by sp_GetCaseInfo_CaseNum
                    {
                        throw new ApplicationException("sp_GetCaseInfo_CaseNum returned nothing for  " + sCaseNum + "\r\n");
                    }

                    bRetVal = true;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();


                }

                return bRetVal;
            }

            private bool AppendRecs(string sReportDoc, string sRecDoc, string sFinalDoc)
            {
                // Append rec doc to report doc
                // Save as DOCX

                bool bRet = false;

                object oMissing = System.Reflection.Missing.Value;

                //Start Word and open report doc
                Word._Application oWord;
                Word._Document oDoc;
                oWord = new Word.Application();
                oWord.Visible = false;
                oDoc = oWord.Documents.Open(sReportDoc, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                try
                {

                    // Move to end of file
                    Word.Selection selection = oWord.Selection;
                    selection.EndKey(Word.WdUnits.wdStory);

                    // Insert blank page
                    selection.InsertNewPage();

                    // Insert rec doc
                    selection.InsertFile(sRecDoc);

                    // Set page margins
                    selection.PageSetup.TopMargin = 18;
                    selection.PageSetup.BottomMargin = 18;
                    selection.PageSetup.LeftMargin = 36;
                    selection.PageSetup.RightMargin = 36;

                    // Save doc
                    oWord.ActiveDocument.SaveAs2(sFinalDoc, Word.WdSaveFormat.wdFormatXMLDocument);

                    bRet = true;

                }
                catch (Exception ex)
                {

                    //pass exception to calling proc
                    throw ex;

                }

                finally
                {
                    if (oDoc != null)
                    {
                        oDoc.Close();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                        oDoc = null;
                    }
                    if (oWord != null)
                    {
                        oWord.Quit();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oWord);
                        oWord = null;
                    }
                    GC.Collect();

                }

                return bRet;

            }

            private bool SaveAsDocx(string sReportDoc, string sFinalDoc)
            {
                // Save report doc as DOCX - used when there are no recs

                bool bRet = false;

                object oMissing = System.Reflection.Missing.Value;

                //Start Word and open report doc
                Word._Application oWord;
                Word._Document oDoc;
                oWord = new Word.Application();
                oWord.Visible = false;
                oDoc = oWord.Documents.Open(sReportDoc, ref oMissing, ref oMissing,
                    ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                try
                {

                    // Save as doc
                    oWord.ActiveDocument.SaveAs2(sFinalDoc, Word.WdSaveFormat.wdFormatXMLDocument);
                    bRet = true;

                }
                catch (Exception ex)
                {

                    //pass exception to calling proc
                    throw ex;

                }

                finally
                {
                    if (oDoc != null)
                    {
                        oDoc.Close();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oDoc);
                        oDoc = null;
                    }
                    if (oWord != null)
                    {
                        oWord.Quit();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oWord);
                        oWord = null;
                    }
                    GC.Collect();

                }

                return bRet;

            }
        }
    }
}
