﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.SqlClient;



namespace Statements
{
    public partial class frmImportCust : Form
    {
        private string cfg_SQLMainSIBIConnStr;
 
        public frmImportCust()
        {
            InitializeComponent();
        }

        private void bImport_Click(object sender, EventArgs e)
        {

            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");
            
            SqlConnection sqlConn = new SqlConnection(cfg_SQLMainSIBIConnStr);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConn;
            sqlConn.Open();

            var xlApp = new Excel.Application();
            xlApp.Visible = false;

            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open("c:\\temp\\CustStmtData1.xlsx");
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;

            int rowCount = xlRange.Rows.Count;
            int colCount = xlRange.Columns.Count;
            
            string sAcnt = "";
            string sEmail = "";
            string sSendEmail = "";
            string sFreq = "";
            bool bImport = false;
            object oRetVal;

            for (int i = 2; i <= rowCount; i++)
            {
                sAcnt = xlRange.Cells[i, "A"].Value2 == null ? "" : xlRange.Cells[i, "A"].Value2.ToString();
                sAcnt = sAcnt.Trim();
                sEmail = xlRange.Cells[i, "C"].Value2 == null ? "" : xlRange.Cells[i, "C"].Value2.ToString();
                sEmail = sEmail.Trim();
                sFreq = xlRange.Cells[i, "D"].Value2 == null ? "" : xlRange.Cells[i, "D"].Value2.ToString();
                sFreq = sFreq.Trim();

                bImport = true;

                if (sAcnt.Length == 0)
                    bImport = false;

                if (sEmail.Length > 0)
                {
                    sEmail = sEmail.Replace(',', ';');
                }
                
                if (sEmail.Length > 0)
                    sSendEmail = "1";
                else
                {
                    sSendEmail = "0";
                    bImport = false;
                }

                if (sFreq.ToUpper() == "YES")
                {
                    sFreq = "M";
                }
                else
                {
                    sFreq = "B";
                }


                if (bImport)
                {
                    tbList.Text += sAcnt + " ~ " + sFreq + " ~ " + sEmail + System.Environment.NewLine;

                    sqlCommand.CommandText = "INSERT INTO StatementEmail " +
                        "([acnt_num],[StmtEmail],[SendEmail],[Frequency]) " +
                        "VALUES (" + sAcnt + ",'" + sEmail + "'," + sSendEmail + ",'" + sFreq + "')";
                    oRetVal = sqlCommand.ExecuteNonQuery();
                }

            }

            xlApp.Workbooks.Close();
            xlApp = null;

        }
    }
}
