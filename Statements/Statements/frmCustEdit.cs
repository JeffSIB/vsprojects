﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace Statements
{
    public partial class frmCustomerEdit : Form
    {

        private string cfg_SQLMainSIBIConnStr;
        public string AccountNum { get; set; }
        public string CustomerName { get; set; }

        public frmCustomerEdit()
        {
            InitializeComponent();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCustomerEdit_Load(object sender, EventArgs e)
        {

            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");
            
            string sRecip = "";
            string sFreq = "";
            bool bEmail = false;

            try{

                
                // init SQL connection
                SqlDataReader sqlDR;
                SqlConnection sqlConn = new SqlConnection(cfg_SQLMainSIBIConnStr);
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConn;
                sqlConn.Open();

                // get Customer info from StatementEmail table
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "SELECT se.StmtEmail, se.SendEmail, se.Frequency ";
                sqlCommand.CommandText += "FROM dbo.StatementEmail se ";
                sqlCommand.CommandText += "WHERE se.acnt_num = " + AccountNum.ToString();
                             
                sqlDR = sqlCommand.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {
                    // add row to grid

                    // loop through active Customers
                    while (sqlDR.Read())
                    {

                        sRecip = sqlDR.IsDBNull(0) ? "" : sqlDR.GetSqlString(0).ToString().Trim();
                        bEmail = sqlDR.IsDBNull(1) ? false : sqlDR.GetBoolean(1); 
                        sFreq = sqlDR.IsDBNull(2) ? "" : sqlDR.GetSqlString(2).ToString().Trim();

                        tbAccountNum.Text = AccountNum;
                        tbCustName.Text = CustomerName;
                        tbRecip.Text = sRecip;
                            
                        if (bEmail)
                            rbEmailYes.Checked = true;
                        else
                            rbEmailNo.Checked = true;

                        if (sFreq == "M")
                            rbMonthly.Checked = true;
                        else
                            rbBiMonthly.Checked = true;
 
                    }	// while


                }	// has rows
                else
                {
                    try
                    {

                        sqlDR.Close();

                        sqlCommand.CommandText = "INSERT INTO StatementEmail " +
                            "([acnt_num],[StmtEmail],[SendEmail],[Frequency]) " +
                            "VALUES (" + AccountNum + ",'',0,'B')";
                        object oRetVal = sqlCommand.ExecuteNonQuery();

                        tbAccountNum.Text = AccountNum;
                        tbCustName.Text = CustomerName;
                        rbEmailNo.Checked = true;
                        rbBiMonthly.Checked = true;
                        tbRecip.Text = "";

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Customer account update failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            
                sqlConn.Close();
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void bSubmit_Click(object sender, EventArgs e)
        {

            string sAcntName = tbCustName.Text.Trim();
            string sAcntNum = tbAccountNum.Text.Trim();
            string sRecip = tbRecip.Text.Trim();
            string sFreq = rbBiMonthly.Checked ? "B" : "M";
            bool bEmail = rbEmailYes.Checked;
            int iEmail = 0;

            if (bEmail)
                iEmail = 1;

            object oRetVal;

            // init SQL connection
            SqlConnection sqlConn = new SqlConnection(cfg_SQLMainSIBIConnStr);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConn;

            if (bEmail && sRecip.Length == 0)
            {
                MessageBox.Show("If send statements via email is checked you must enter at least one email address.","Customer Information",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }

            if (sRecip.IndexOf(" ") != -1)
            {
                MessageBox.Show("Email address may not contain spaces.", "Customer Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // validate email addresses
            char[] splitchar = { ';' };
            string[] sEmailAdd = sRecip.Split(splitchar);
            bool bEmailValid = true;

            foreach (string s in sEmailAdd)
            {
                if (s.Trim() != "")
                    if (!isValidEmail(s))
                        bEmailValid = false;
            }

            if (!bEmailValid)
            {
                MessageBox.Show("The email address entered is not in a valid format.\r\n\r\nIf there are multiple addrsses, they must be seperated by a ';'", "Customer Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;          
            
            }
            

            try
            {

                sqlConn.Open();

                // update FieldRepPayroll table
                sqlCommand.CommandText = "UPDATE dbo.StatementEmail ";
                sqlCommand.CommandText += "Set StmtEmail = '" + sRecip + "', Frequency = '" + sFreq + "', SendEmail = " + iEmail.ToString();
                sqlCommand.CommandText += " WHERE acnt_num = " + AccountNum.ToString();
                
                oRetVal = sqlCommand.ExecuteNonQuery();

                bSubmit.Enabled = false;
                bCancel.Text = "Close";
                MessageBox.Show("Customer account updated successfully.", "Update successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Customer account update failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
               sqlConn.Close();
            }
        }

        private bool isValidEmail(string eMail)
        {

            bool bRet = false;

            Match match = Regex.Match(eMail, @"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@(([0-9a-zA-Z])+([-\w]*[0-9a-zA-Z])*\.)+[a-zA-Z]{2,9})$",
                RegexOptions.IgnoreCase);

            if (match.Success)
            {
                bRet = true;
            }

            return bRet;

        }
    }
}
