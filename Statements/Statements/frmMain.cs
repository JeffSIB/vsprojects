﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Statements
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void tsmiExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void llblCustMaint_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmCustomerList frm = new frmCustomerList();
            frm.ShowDialog();
        }

  
        private void importToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmImportCust frm = new frmImportCust();
            frm.ShowDialog();

        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string sPDFFileName = "J:\\StatementProcessing\\AutomatedStatementEmail.pdf";
            System.Diagnostics.Process.Start(sPDFFileName);
        }

        private void llblStatements_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //MessageBox.Show("This feature is not currently active", "Process statements", MessageBoxButtons.OK, MessageBoxIcon.Information);
            frmStatements frm = new frmStatements();
            frm.ShowDialog();
        }

        private void tsmiViewLog_Click(object sender, EventArgs e)
        {
            string sFileName = "J:\\StatementProcessing\\Logs\\StatementLog.txt";
            System.Diagnostics.Process.Start(sFileName);
        }

 
    }
}
