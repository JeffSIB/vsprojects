﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Xceed.Grid;
using Xceed.Grid.Reporting;
using Xceed.Grid.Editors;
using Xceed.Grid.Viewers;
using Xceed.Grid.Collections;
using System.Data.SqlClient;

namespace Statements
{
    public partial class frmCustomerList : Form
    {

        private string cfg_SQL360UtilConnStr;
        private string cfg_SQLMainSIBIConnStr;
        private string rptTitle = "";
        private Xceed.Grid.GridControl xGrid = new GridControl();
        ColumnManagerRow xGridColumnManagerRow;
        private string msGridSortCol;
        private bool mbGridSortDir;


        public frmCustomerList()
        {
            InitializeComponent();
        }

        private void FieldRepMaint_Load(object sender, EventArgs e)
        {
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_SQL360UtilConnStr = colNameVal.Get("360UtilConnStr");
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");


            // place grid on panel
            this.pnlMain.Controls.Add(xGrid);

            // grid sort order
            msGridSortCol = "";
            mbGridSortDir = true;

            xGrid.Clear();
            this.PrepareGrid();

        }

        private void PrepareGrid()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xGrid.Clear();

                // header row
                xGridColumnManagerRow = new ColumnManagerRow();
                xGrid.FixedHeaderRows.Add(xGridColumnManagerRow);

                //COLUMNS
                xGrid.Columns.Add(new Column("CustNum", typeof(int)));
                xGrid.Columns["CustNum"].Title = "Cust #";
                xGrid.Columns["CustNum"].Width = 60;
                xGrid.Columns["CustNum"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["CustNum"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["CustNum"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("CustName", typeof(string)));
                xGrid.Columns["CustName"].Title = "Customer name";
                xGrid.Columns["CustName"].Width = 200;
                xGrid.Columns["CustName"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Left;
                xGrid.Columns["CustName"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Left;
                xGrid.DataRowTemplate.Cells["CustName"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("Freq", typeof(string)));
                xGrid.Columns["Freq"].Title = "Period";
                xGrid.Columns["Freq"].Width = 50;
                xGrid.Columns["Freq"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["Freq"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["Freq"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("Mailed", typeof(bool)));
                xGrid.Columns["Mailed"].Title = "Email";
                xGrid.Columns["Mailed"].Width = 50;
                xGrid.Columns["Mailed"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.Columns["Mailed"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xGrid.DataRowTemplate.Cells["Mailed"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

                xGrid.Columns.Add(new Column("Recip", typeof(string)));
                xGrid.Columns["Recip"].Title = "Email addresses";
                xGrid.Columns["Recip"].Width = 220;
                xGrid.Columns["Recip"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Left;
                xGrid.Columns["Recip"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Left;
                xGrid.DataRowTemplate.Cells["Recip"].MouseDown += new MouseEventHandler(xGrid_cell_MouseDown);

               
                //grid wide settings
                xGrid.ReadOnly = true;
                xGrid.Dock = DockStyle.Fill;
                xGrid.FixedColumnSplitter.Visible = false;
                xGrid.RowSelectorPane.Visible = false;
                xGrid.KeyDown += new KeyEventHandler(xGrid_KeyDown);

                // prevent cell navigation
                xGrid.AllowCellNavigation = false;

                // resize
                xGrid.Resize += new System.EventHandler(xGrid_Resize);

                // Sort
                xGrid.SortedColumnsChanged += new EventHandler(xGrid_SortedColumnsChanged);

                //rptTitle = "Pay period ending " + dPayDate.Month.ToString() + "/" + dPayDate.Day.ToString() + "/" + dPayDate.Year.ToString();
                //this.Text = "Inspector payroll - Pay period ending " + dPayDate.Month.ToString() + "/" + dPayDate.Day.ToString() + "/" + dPayDate.Year.ToString();

                int iCustNumber = 0;
                string sCustName = "";
                string sRecip = "";
                bool bEmail = false;
                string sFreq = "";
                
                int iCustCount = 0;

                // init SQL connection
                SqlDataReader sqlDR;
                SqlConnection sqlConn = new SqlConnection(cfg_SQLMainSIBIConnStr);
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConn;
                sqlConn.Open();

                // get all customers
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "SELECT acnt.acnt_num, acnt.acnt_name, se.StmtEmail, se.SendEmail, se.Frequency ";
                sqlCommand.CommandText += "FROM dbo.account acnt LEFT OUTER JOIN dbo.StatementEmail se on se.acnt_num = acnt.acnt_num ";
                sqlCommand.CommandText += "ORDER BY acnt.acnt_num";

                sqlDR = sqlCommand.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {
                    // add row to grid
                    xGrid.BeginInit();

                    // loop through customers
                    while (sqlDR.Read())
                    {

                        try
                        {

                            iCustNumber = (int)sqlDR.GetSqlInt32(0);
                            sCustName = sqlDR.IsDBNull(1) ? "" : sqlDR.GetSqlString(1).ToString();
                            sRecip = sqlDR.IsDBNull(2) ? "" : sqlDR.GetSqlString(2).ToString();
                            bEmail = sqlDR.IsDBNull(3) ? false : (bool)sqlDR.GetSqlBoolean(3);
                            sFreq = sqlDR.IsDBNull(4) ? "" : sqlDR.GetSqlString(4).ToString();
 
                            iCustCount += 1;

                            Xceed.Grid.DataRow dataRow = xGrid.DataRows.AddNew();
                            dataRow.Cells["CustNum"].Value = iCustNumber;
                            dataRow.Cells["CustName"].Value = sCustName;
                            dataRow.Cells["Recip"].Value = sRecip;
                            dataRow.Cells["Mailed"].Value = bEmail;
                            dataRow.Cells["Freq"].Value = sFreq;
                            
                            dataRow.EndEdit();

                        }
                        finally
                        {
                            xGrid.EndInit();

                        }	//try


                    }	// while

                    xGrid_Resize(null, null);

                }	// has rows

                sqlConn.Close();

                // if there is no sorted column - sort by Insp num
                if (msGridSortCol == "")
                {
                    xGrid.Columns["CustNum"].SortDirection = SortDirection.Ascending;
                    msGridSortCol = "CustNum";
                    mbGridSortDir = true;
                }
                else
                {
                    if (mbGridSortDir)
                        xGrid.Columns[msGridSortCol].SortDirection = SortDirection.Ascending;
                    else
                        xGrid.Columns[msGridSortCol].SortDirection = SortDirection.Descending;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void xGrid_cell_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Left) && (e.Clicks >= 2))
                this.xGrid_editRow(((Cell)sender).ParentRow);
        }

        private void xGrid_editRow(CellRow folderRow)
        {

            frmCustomerEdit frm = new frmCustomerEdit();
            frm.AccountNum = folderRow.Cells["CustNum"].Value.ToString();
            frm.CustomerName = folderRow.Cells["CustName"].Value.ToString();
            frm.ShowDialog();

        }
        private void xGrid_KeyDown(object sender, KeyEventArgs e)
        ///<summary>
        ///
        {
            switch (e.KeyData)
            {
                case Keys.Enter:
                    {
                        CellRow currentRow = xGrid.CurrentRow as CellRow;

                        if (currentRow != null)
                            this.xGrid_editRow(currentRow);
                        break;
                    }

                case Keys.Back:
                    //this.NavigateUp();
                    break;
            }
        }

        private void xGrid_SortedColumnsChanged(object sender, System.EventArgs e)
        {

            // preserve existing sort order
            SortedColumnList sclxTemp = xGrid.SortedColumns;

            foreach (Xceed.Grid.Column column in sclxTemp)
            {
                if (column.SortIndex == 0)
                {
                    msGridSortCol = column.FieldName.ToString();
                    mbGridSortDir = (column.SortDirection == SortDirection.Ascending);
                }

            }
        }

        // Grid resize
        private void xGrid_Resize(object sender, System.EventArgs e)
        {
            int iAvailWidth = xGrid.DisplayRectangle.Width;
            int iTotColWidth = 0;
            int iResizeCol = 4;
            int iAdjustment = 0;

            // minimized
            if (iAvailWidth == 0)
                return;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xGrid.Columns)
            {
                if (column.Index != iResizeCol)
                {
                    Console.WriteLine(column.FieldName);
                    iTotColWidth += column.Width;
                }

            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.Width = iWidth;
            }
            else
            {
                xResizeColumn.Width = iWidth;
            }
        }

        // resize grid for print
        private void xGrid_ResizeForPrint(int iAvailWidth)
        {
            int iTotColWidth = 0;
            int iResizeCol = 4;
            int iAdjustment = 10;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xGrid.Columns)
            {
                if (column.Index != iResizeCol)
                    iTotColWidth += column.Width;
            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
            else
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
        }

        private void tsmiFile_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsmiRefreshGrid_Click(object sender, EventArgs e)
        {
            xGrid.Clear();
            this.PrepareGrid();
        }

        private void tsmiPrintInspectorList_Click(object sender, EventArgs e)
        {
            this.xGrid.ReportSettings.Title = rptTitle;
            this.xGrid.ReportSettings.Landscape = true;

            xGrid_ResizeForPrint(850);
            
            // base font for the report
            Font font = new Font("Arial", 9);
            this.xGrid.ReportStyle.Font = font;

            Report report = new Report(xGrid);
            ReportStyleSheet reportStyleSheet = report.ReportStyleSheet;
            
            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            reportStyleSheet.Grid.DataRows[0].BackColor = Color.FromArgb(205, 255, 205);
            reportStyleSheet.Grid.DataRows[1].BackColor = Color.Transparent;

            report.ReportStyleSheet.PageHeader.BottomBorder.Thickness = ReportLineThickness.SingleThin;
            report.ReportStyleSheet.PageHeader.BottomMargin = 5;
            report.ReportStyleSheet.PageHeader.Font = new Font(xGrid.ReportStyle.Font.FontFamily, 10, FontStyle.Bold);

            this.xGridColumnManagerRow.ReportStyle.RepeatOnEachPage = true;
            this.xGridColumnManagerRow.ReportStyle.BackColor = Color.DarkGray;
            this.xGridColumnManagerRow.ReportStyle.ForeColor = Color.White;
            this.xGridColumnManagerRow.ReportStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            this.xGridColumnManagerRow.ReportStyle.WordWrap = true;
            this.xGridColumnManagerRow.ReportStyle.RowHeightExpansion = RowHeightExpansion.ExpandWithoutLimits;
            this.xGridColumnManagerRow.ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            this.xGridColumnManagerRow.ReportStyle.VerticalAlignment = VerticalAlignment.Bottom;

            report.ReportStyleSheet.PageHeader.LeftElement.TextFormat = "Statement Email Information" + Environment.NewLine;
            report.ReportStyleSheet.PageHeader.RightElement.TextFormat =
              "Page : %Page format=000%" + Environment.NewLine +
              "Date : %DateTime format=g%";

            report.ReportStyleSheet.PageFooter.Font = new Font(xGrid.ReportStyle.Font.FontFamily, 10, FontStyle.Bold);
            report.ReportStyleSheet.PageFooter.TopBorder.Thickness = ReportLineThickness.SingleThin;
            //report.ReportStyleSheet.PageFooter.LeftElement.TextFormat = miInspCount.ToString() + " inspectors.";
            //report.ReportStyleSheet.PageFooter.RightElement.TextFormat = "Total payroll: " + mdTotalPRAmt.ToString("c2");
            report.PrintPreview();
        }

    }
}
