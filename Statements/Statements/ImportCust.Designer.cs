﻿namespace Statements
{
    partial class frmImportCust
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bImport = new System.Windows.Forms.Button();
            this.tbList = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // bImport
            // 
            this.bImport.Location = new System.Drawing.Point(12, 12);
            this.bImport.Name = "bImport";
            this.bImport.Size = new System.Drawing.Size(75, 23);
            this.bImport.TabIndex = 0;
            this.bImport.Text = "Import";
            this.bImport.UseVisualStyleBackColor = true;
            this.bImport.Click += new System.EventHandler(this.bImport_Click);
            // 
            // tbList
            // 
            this.tbList.Location = new System.Drawing.Point(12, 41);
            this.tbList.Multiline = true;
            this.tbList.Name = "tbList";
            this.tbList.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbList.Size = new System.Drawing.Size(842, 429);
            this.tbList.TabIndex = 1;
            // 
            // frmImportCust
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 482);
            this.Controls.Add(this.tbList);
            this.Controls.Add(this.bImport);
            this.Name = "frmImportCust";
            this.Text = "ImportCust";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bImport;
        private System.Windows.Forms.TextBox tbList;
    }
}