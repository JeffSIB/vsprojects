﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Net.Mail;

namespace Statements
{
    public partial class frmStatements : Form
    {

        private LogUtils.LogUtils oLU;

        private string cfg_SQLMainSIBIConnStr;
        private string cfg_StatementDir;
        private string cfg_SMTPServer;
        private string cfg_logfilename;
        private bool bErr;

        public frmStatements()
        {
            InitializeComponent();
        }

        private void frmStatements_Load(object sender, EventArgs e)
        {
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");
            cfg_StatementDir = colNameVal.Get("StatementDir");
            cfg_SMTPServer = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");


            bSend.Enabled = false;

            //TestEmail();

        }

        private void bSend_Click(object sender, EventArgs e)
        {

            // disable select
            bSelect.Enabled = false;
            bSend.Enabled = false;

            this.Refresh();

            // Build statement date
            string sStmtDate = "";
            DateTime dStmtDate = dtpStmtDate.Value;
            sStmtDate = dStmtDate.ToString("_MM_dd_yy");
            sStmtDate += ".pdf";

            string sAcntNum = "";
            string sRecip = "";
            string sFreq = "";
            bool bEmail = false;

            this.Cursor = Cursors.WaitCursor;
            tbStmtMsg.Text = "Processing statements - please wait...";
            tbStmtMsg.ForeColor = System.Drawing.Color.Maroon;
            this.Refresh();

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();
            oLU.WritetoLog("++++ Begin procressing for statement date = " + dStmtDate.ToString(@"MM/dd/yy") + " ++++");

            try
            {

                // init SQL connection
                SqlDataReader sqlDR;
                SqlConnection sqlConn = new SqlConnection(cfg_SQLMainSIBIConnStr);
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConn;
                sqlConn.Open();
                               
                // Set up statement folder
                DirectoryInfo diStmtDir = new DirectoryInfo(cfg_StatementDir);
                FileInfo[] fiStmt = diStmtDir.GetFiles("*" + sStmtDate);

                int iFiles = fiStmt.GetUpperBound(0) + 1;
                bErr = false;

                 // Process each statement in folder for period selected
                foreach (FileInfo fiStatement in fiStmt)
                {
                    //fiStatement.CopyTo(sTargetDir + @"\\" + fiStatement.Name);

                    // get cust acnt from file name
                    sAcntNum = fiStatement.Name.Substring(0, 4);

                    // Update log
                    tbResults.Text += "Account: " + sAcntNum + System.Environment.NewLine;
                    oLU.WritetoLog(System.Environment.NewLine + "Account: " + sAcntNum);

                    // get Customer info from SQL
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "SELECT se.StmtEmail, se.SendEmail, se.Frequency ";
                    sqlCommand.CommandText += "FROM dbo.StatementEmail se ";
                    sqlCommand.CommandText += "WHERE se.acnt_num = " + sAcntNum;

                    sqlDR = sqlCommand.ExecuteReader();

                    // anythign returned?
                    if (sqlDR.HasRows)
                    {

                        while (sqlDR.Read())
                        {
                            sRecip = sqlDR.IsDBNull(0) ? "" : sqlDR.GetSqlString(0).ToString().Trim();
                            bEmail = sqlDR.IsDBNull(1) ? false : sqlDR.GetBoolean(1);
                            sFreq = sqlDR.IsDBNull(2) ? "" : sqlDR.GetSqlString(2).ToString().Trim();

                            if (bEmail)
                            {
                                tbResults.Text += "\tSending statement " + fiStatement.Name + System.Environment.NewLine;
                                oLU.WritetoLog("Sending statement " + fiStatement.Name);

                                SendStmt(sRecip, fiStatement.Name, sAcntNum);
                                this.Refresh();
                                Application.DoEvents();
                            }
                            
                        }	// while


                    }	// has rows
                    else
                    {
                        // no data for customer - must not have been imported from CertiFlex
                        tbResults.Text += "\t***** No data found - customer must not have been imported from CertiFlex" + System.Environment.NewLine;
                        oLU.WritetoLog("***** No data found - customer must not have been imported from CertiFlex");
                    }

                    sqlDR.Close();
                      
                }                

                this.Refresh();

                diStmtDir = null;
                fiStmt = null;
                sqlConn.Close();
                this.Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                oLU.WritetoLog(ex.Message);
                bErr = true;
            }

            oLU.closeLog();
            this.Cursor = Cursors.Default;
            tbStmtMsg.Text = "Processing complete";
            tbStmtMsg.ForeColor = System.Drawing.Color.Black;
            this.Refresh();

            if (bErr)
            {
                MessageBox.Show("One or more errors occurred while sending statements\r\nReview the log for details.", "Error Sending Statements", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Process completed successfully", "Process Statements", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void bSelect_Click(object sender, EventArgs e)
        {

            // Build statement date
            string sStmtDate = "";
            DateTime dStmtDate = dtpStmtDate.Value;
            sStmtDate = dStmtDate.ToString("_MM_dd_yy");
            sStmtDate += ".pdf";

            try
            {
                DirectoryInfo diStmtDir = new DirectoryInfo(cfg_StatementDir);
                FileInfo[] fiSource = diStmtDir.GetFiles("*" + sStmtDate);

                int iFiles = fiSource.GetUpperBound(0) + 1;

                bSend.Enabled = false;

                if (iFiles > 0)
                {
                    tbStmtMsg.ForeColor = System.Drawing.Color.Black;
                    tbStmtMsg.Text = iFiles.ToString() + " statements found for the period specified.";
                    bSend.Enabled = true;
                }
                else
                {
                    tbStmtMsg.Text = "There were no statements found for the specified period.";
                    tbStmtMsg.ForeColor = System.Drawing.Color.Maroon;
                    MessageBox.Show("There were no statements found for the date you selected.\r\n\r\nThe date given must be the date the statement date - typically 1st or 16th.", "No Statements Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                this.Refresh();

                diStmtDir = null;
                fiSource = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SendStmt(string sTo, string sAttFile, string sAcntNum)
        {

            //sTo = "jeff@sibfla.com";

            try
            {

                MailMessage msg = new MailMessage();
                SmtpClient mailClient = new SmtpClient(cfg_SMTPServer);

                mailClient.UseDefaultCredentials = true;

                // split multiple addresses
                char[] splitchar = { ';' };
                string[] sRecipients = sTo.Split(splitchar);

                // add recipients
                foreach (string sRecip in sRecipients)
                {

                    if (sRecip.Length > 0)
                    {
                        msg.To.Add(new MailAddress(sRecip));
                    }
                }

                msg.Bcc.Add(new MailAddress("AutoStmt@sibfla.com"));

                msg.From = new MailAddress("accounting@sibfla.com");
                msg.Subject = "Sutton Inspection Bureau - Customer statement - " + sAcntNum;

                string sBody = "A copy of your statement is attached for your convenience. If you have any questions about your statement, please contact us." + System.Environment.NewLine + System.Environment.NewLine +
                    "Please note our updated mailing address:" + System.Environment.NewLine +
                    "\tSutton Inspection Bureau, Inc." + System.Environment.NewLine +
                    "\t5656 Central Avenue" + System.Environment.NewLine +
                    "\tSt. Petersburg, FL  33707-1718" + System.Environment.NewLine + System.Environment.NewLine +
                    "Thank you for your business." + System.Environment.NewLine + System.Environment.NewLine +
                    "Tiana Courchaine" + System.Environment.NewLine +
                    "Sutton Inspection Bureau" + System.Environment.NewLine +
                    "accounting@sibfla.com" + System.Environment.NewLine +
                    "727-384-5454 x112" + System.Environment.NewLine;

                msg.Body = sBody;
                msg.IsBodyHtml = false;

                msg.Attachments.Add(new Attachment(cfg_StatementDir + "\\" + sAttFile));

                mailClient.Send(msg);

                tbResults.Text += "\tMessage sent to " + sTo + System.Environment.NewLine;
                oLU.WritetoLog("Message sent to " + sTo);

                msg = null;
                mailClient = null;
            }

            catch (Exception ex)
            {
                tbResults.Text += "\t***** Error sending email ****" + System.Environment.NewLine +
                    "\t" + ex.Message + System.Environment.NewLine;
                oLU.WritetoLog("***** Error sending email ****");
                oLU.WritetoLog("ex.Message");

                bErr = true;
            }
         
        }

        private void TestEmail()
        {

            string sTo = "jeff@sibfla.com";
            string sAcntNum = "9999";
            
            try
            {

                MailMessage msg = new MailMessage();
                SmtpClient mailClient = new SmtpClient(cfg_SMTPServer);

                mailClient.UseDefaultCredentials = true;

                // split multiple addresses
                char[] splitchar = { ';' };
                string[] sRecipients = sTo.Split(splitchar);

                // add recipients
                foreach (string sRecip in sRecipients)
                {

                    if (sRecip.Length > 0)
                    {
                        msg.To.Add(new MailAddress(sRecip));
                    }
                }

                msg.Bcc.Add(new MailAddress("AutoStmt@sibfla.com"));

                msg.From = new MailAddress("accounting@sibfla.com");
                msg.Subject = "Sutton Inspection Bureau - Customer statement - " + sAcntNum;

                string sBody = "A copy of your statement is attached for your convenience. If you have any questions about your statement, please contact us." + System.Environment.NewLine + System.Environment.NewLine +
                    "Please note our updated mailing address:" + System.Environment.NewLine +
                    "\tSutton Inspection Bureau, Inc." + System.Environment.NewLine +
                    "\t5656 Central Avenue" + System.Environment.NewLine +
                    "\tSt. Petersburg, FL  33707-1718" + System.Environment.NewLine + System.Environment.NewLine +
                    "Thank you for your business." + System.Environment.NewLine + System.Environment.NewLine +
                    "Tiana Courchaine" + System.Environment.NewLine +
                    "Sutton Inspection Bureau" + System.Environment.NewLine +
                    "accounting@sibfla.com" + System.Environment.NewLine +
                    "727-384-5454 x101" + System.Environment.NewLine;

                msg.Body = sBody;
                msg.IsBodyHtml = false;

                //msg.Attachments.Add(new Attachment(cfg_StatementDir + "\\" + sAttFile));

                mailClient.Send(msg);

                tbResults.Text += "\tMessage sent to " + sTo + System.Environment.NewLine;
                oLU.WritetoLog("Message sent to " + sTo);

                msg = null;
                mailClient = null;
            }

            catch (Exception ex)
            {
                tbResults.Text += "\t***** Error sending email ****" + System.Environment.NewLine +
                    "\t" + ex.Message + System.Environment.NewLine;
                oLU.WritetoLog("***** Error sending email ****");
                oLU.WritetoLog("ex.Message");

                bErr = true;
            }

        }
    }
}
