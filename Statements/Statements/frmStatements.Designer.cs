﻿namespace Statements
{
    partial class frmStatements
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.bSelect = new System.Windows.Forms.Button();
            this.dtpStmtDate = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbResults = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bSend = new System.Windows.Forms.Button();
            this.tbStmtMsg = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.MaximumSize = new System.Drawing.Size(500, 20);
            this.textBox1.MinimumSize = new System.Drawing.Size(0, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(366, 15);
            this.textBox1.TabIndex = 1;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "Email statements to clients for the selected period.";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.bSelect);
            this.groupBox1.Controls.Add(this.dtpStmtDate);
            this.groupBox1.Location = new System.Drawing.Point(154, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 139);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Select statement period ";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(6, 19);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(271, 22);
            this.textBox2.TabIndex = 9;
            this.textBox2.TabStop = false;
            this.textBox2.Text = "Select the date the statements were created";
            // 
            // bSelect
            // 
            this.bSelect.Location = new System.Drawing.Point(75, 93);
            this.bSelect.Name = "bSelect";
            this.bSelect.Size = new System.Drawing.Size(132, 23);
            this.bSelect.TabIndex = 8;
            this.bSelect.Text = "Select Statements";
            this.bSelect.UseVisualStyleBackColor = true;
            this.bSelect.Click += new System.EventHandler(this.bSelect_Click);
            // 
            // dtpStmtDate
            // 
            this.dtpStmtDate.CalendarForeColor = System.Drawing.Color.Black;
            this.dtpStmtDate.CalendarTitleBackColor = System.Drawing.Color.DarkGray;
            this.dtpStmtDate.CalendarTitleForeColor = System.Drawing.Color.Black;
            this.dtpStmtDate.CalendarTrailingForeColor = System.Drawing.Color.DarkGray;
            this.dtpStmtDate.Location = new System.Drawing.Point(41, 47);
            this.dtpStmtDate.Name = "dtpStmtDate";
            this.dtpStmtDate.Size = new System.Drawing.Size(200, 20);
            this.dtpStmtDate.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(75, 103);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(8, 8);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tbResults);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(12, 348);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(568, 295);
            this.panel1.TabIndex = 6;
            // 
            // tbResults
            // 
            this.tbResults.BackColor = System.Drawing.Color.White;
            this.tbResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbResults.ForeColor = System.Drawing.Color.Black;
            this.tbResults.Location = new System.Drawing.Point(0, 0);
            this.tbResults.Multiline = true;
            this.tbResults.Name = "tbResults";
            this.tbResults.ReadOnly = true;
            this.tbResults.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbResults.Size = new System.Drawing.Size(568, 295);
            this.tbResults.TabIndex = 6;
            this.tbResults.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(12, 329);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Session log";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bSend);
            this.groupBox3.Controls.Add(this.tbStmtMsg);
            this.groupBox3.Location = new System.Drawing.Point(154, 194);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(283, 127);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Process Statements ";
            // 
            // bSend
            // 
            this.bSend.Location = new System.Drawing.Point(78, 75);
            this.bSend.Name = "bSend";
            this.bSend.Size = new System.Drawing.Size(132, 23);
            this.bSend.TabIndex = 7;
            this.bSend.Text = "Send Statements";
            this.bSend.UseVisualStyleBackColor = true;
            this.bSend.Click += new System.EventHandler(this.bSend_Click);
            // 
            // tbStmtMsg
            // 
            this.tbStmtMsg.BackColor = System.Drawing.Color.White;
            this.tbStmtMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbStmtMsg.Location = new System.Drawing.Point(13, 19);
            this.tbStmtMsg.Multiline = true;
            this.tbStmtMsg.Name = "tbStmtMsg";
            this.tbStmtMsg.ReadOnly = true;
            this.tbStmtMsg.Size = new System.Drawing.Size(264, 50);
            this.tbStmtMsg.TabIndex = 0;
            this.tbStmtMsg.TabStop = false;
            // 
            // frmStatements
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(592, 658);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmStatements";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Send Statements";
            this.Load += new System.EventHandler(this.frmStatements_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtpStmtDate;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbResults;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button bSelect;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bSend;
        private System.Windows.Forms.TextBox tbStmtMsg;
    }
}