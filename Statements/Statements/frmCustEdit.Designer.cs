﻿namespace Statements
{
    partial class frmCustomerEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbAccountNum = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCustName = new System.Windows.Forms.TextBox();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.bSubmit = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tbRecip = new System.Windows.Forms.TextBox();
            this.rbEmailYes = new System.Windows.Forms.RadioButton();
            this.rbEmailNo = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlFreq = new System.Windows.Forms.Panel();
            this.rbBiMonthly = new System.Windows.Forms.RadioButton();
            this.rbMonthly = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlFreq.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(61, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Account number";
            // 
            // tbAccountNum
            // 
            this.tbAccountNum.BackColor = System.Drawing.Color.White;
            this.tbAccountNum.ForeColor = System.Drawing.Color.DimGray;
            this.tbAccountNum.Location = new System.Drawing.Point(156, 65);
            this.tbAccountNum.Name = "tbAccountNum";
            this.tbAccountNum.ReadOnly = true;
            this.tbAccountNum.Size = new System.Drawing.Size(59, 20);
            this.tbAccountNum.TabIndex = 1;
            this.tbAccountNum.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(66, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Customer name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(21, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Send statement via email";
            // 
            // tbCustName
            // 
            this.tbCustName.BackColor = System.Drawing.Color.White;
            this.tbCustName.ForeColor = System.Drawing.Color.DimGray;
            this.tbCustName.Location = new System.Drawing.Point(156, 91);
            this.tbCustName.Name = "tbCustName";
            this.tbCustName.ReadOnly = true;
            this.tbCustName.Size = new System.Drawing.Size(325, 20);
            this.tbCustName.TabIndex = 7;
            this.tbCustName.TabStop = false;
            // 
            // tbTitle
            // 
            this.tbTitle.BackColor = System.Drawing.Color.White;
            this.tbTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbTitle.ForeColor = System.Drawing.Color.DimGray;
            this.tbTitle.Location = new System.Drawing.Point(0, 0);
            this.tbTitle.Margin = new System.Windows.Forms.Padding(5, 15, 5, 5);
            this.tbTitle.Multiline = true;
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.ReadOnly = true;
            this.tbTitle.Size = new System.Drawing.Size(511, 25);
            this.tbTitle.TabIndex = 12;
            this.tbTitle.TabStop = false;
            this.tbTitle.Text = "Customer account number and name must be changed in Cerfitlex.";
            // 
            // bSubmit
            // 
            this.bSubmit.Location = new System.Drawing.Point(167, 325);
            this.bSubmit.Name = "bSubmit";
            this.bSubmit.Size = new System.Drawing.Size(75, 23);
            this.bSubmit.TabIndex = 5;
            this.bSubmit.Text = "Submit";
            this.bSubmit.UseVisualStyleBackColor = true;
            this.bSubmit.Click += new System.EventHandler(this.bSubmit_Click);
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(268, 325);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 6;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 165);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(211, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Send statements to the following addresses";
            // 
            // tbRecip
            // 
            this.tbRecip.BackColor = System.Drawing.Color.White;
            this.tbRecip.ForeColor = System.Drawing.Color.Black;
            this.tbRecip.Location = new System.Drawing.Point(26, 181);
            this.tbRecip.MaxLength = 2048;
            this.tbRecip.Multiline = true;
            this.tbRecip.Name = "tbRecip";
            this.tbRecip.Size = new System.Drawing.Size(458, 63);
            this.tbRecip.TabIndex = 4;
            // 
            // rbEmailYes
            // 
            this.rbEmailYes.AutoSize = true;
            this.rbEmailYes.Location = new System.Drawing.Point(157, 140);
            this.rbEmailYes.Name = "rbEmailYes";
            this.rbEmailYes.Size = new System.Drawing.Size(43, 17);
            this.rbEmailYes.TabIndex = 2;
            this.rbEmailYes.TabStop = true;
            this.rbEmailYes.Text = "Yes";
            this.rbEmailYes.UseVisualStyleBackColor = true;
            // 
            // rbEmailNo
            // 
            this.rbEmailNo.AutoSize = true;
            this.rbEmailNo.Location = new System.Drawing.Point(247, 140);
            this.rbEmailNo.Name = "rbEmailNo";
            this.rbEmailNo.Size = new System.Drawing.Size(39, 17);
            this.rbEmailNo.TabIndex = 3;
            this.rbEmailNo.TabStop = true;
            this.rbEmailNo.Text = "No";
            this.rbEmailNo.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Location = new System.Drawing.Point(16, 247);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(479, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Seperate multiple addresses with a semicolon . There should be no spaces in or be" +
                "tween addresses.";
            // 
            // pnlFreq
            // 
            this.pnlFreq.Controls.Add(this.rbBiMonthly);
            this.pnlFreq.Controls.Add(this.rbMonthly);
            this.pnlFreq.Controls.Add(this.label4);
            this.pnlFreq.Location = new System.Drawing.Point(12, 117);
            this.pnlFreq.Name = "pnlFreq";
            this.pnlFreq.Size = new System.Drawing.Size(487, 20);
            this.pnlFreq.TabIndex = 20;
            // 
            // rbBiMonthly
            // 
            this.rbBiMonthly.AutoSize = true;
            this.rbBiMonthly.Location = new System.Drawing.Point(235, 1);
            this.rbBiMonthly.Name = "rbBiMonthly";
            this.rbBiMonthly.Size = new System.Drawing.Size(70, 17);
            this.rbBiMonthly.TabIndex = 4;
            this.rbBiMonthly.TabStop = true;
            this.rbBiMonthly.Text = "Bimonthly";
            this.rbBiMonthly.UseVisualStyleBackColor = true;
            // 
            // rbMonthly
            // 
            this.rbMonthly.AutoSize = true;
            this.rbMonthly.Location = new System.Drawing.Point(145, 1);
            this.rbMonthly.Name = "rbMonthly";
            this.rbMonthly.Size = new System.Drawing.Size(62, 17);
            this.rbMonthly.TabIndex = 3;
            this.rbMonthly.TabStop = true;
            this.rbMonthly.Text = "Monthly";
            this.rbMonthly.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(29, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Statement frequency";
            // 
            // frmCustomerEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(511, 360);
            this.ControlBox = false;
            this.Controls.Add(this.pnlFreq);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rbEmailNo);
            this.Controls.Add(this.rbEmailYes);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bSubmit);
            this.Controls.Add(this.tbTitle);
            this.Controls.Add(this.tbRecip);
            this.Controls.Add(this.tbCustName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbAccountNum);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCustomerEdit";
            this.Text = "Edit Customer Account Information";
            this.Load += new System.EventHandler(this.frmCustomerEdit_Load);
            this.pnlFreq.ResumeLayout(false);
            this.pnlFreq.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbAccountNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCustName;
        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.Button bSubmit;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbRecip;
        private System.Windows.Forms.RadioButton rbEmailYes;
        private System.Windows.Forms.RadioButton rbEmailNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnlFreq;
        private System.Windows.Forms.RadioButton rbBiMonthly;
        private System.Windows.Forms.RadioButton rbMonthly;
        private System.Windows.Forms.Label label4;
    }
}