﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Microsoft.Office;
namespace RPSInvoicing
{
    public static class Program
    {
        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static string cfg_deliverto;
        static string cfg_CustName;
        static string cfg_CustAddress;
        static string cfg_CustCSZ;

        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Range oRange = null;
        static string sReportDate;
        static string sReportTitle;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;
        static DataTable dt360;
        static DataSet dsReportData;
        static string sCustNumber;
        static int cfg_TimeZoneOffset;
        static string sMode;
        static DateTime dInvoiceDate;
        static string sInvoiceDate;
        static DateTime dInvoiceDispDate;
        static string sInvoiceDispDate;
        static string sInvoiceNum;
        static decimal decTotalInvAmt;
        static int iInvoiceNumber;
        static Guid guInvoiceID;


        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";
            bool bRetVal = false;

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_deliverto = colNameVal.Get("deliverto");
                cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));
                cfg_CustAddress = colNameVal.Get("CustAddress");
                cfg_CustName = colNameVal.Get("CustName");
                cfg_CustCSZ = colNameVal.Get("CustCSZ");

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                // Run for 7328-GA, 7349-KY & 7374-OK
                // Call once with N and once with A for each account


                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //      A = Admitted cases only
                //      N = All cases except admitted
                //
                //  Customer Number
                //      Customer number to process
                //
                //  Invoice Date
                //      Invoice date in 360
                //
                //  Invoice display date
                //      Date displayed on invoice
                //
                //  Example:
                //  N 06/30/2021 7/1/2021 7328
                //  A 06/30/2021 7/1/2021 7328

                sMode = "";
                if (args.Length < 4)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                // set report period based on parammeter passed
                DateTime dt = new DateTime();

                //Mode - admitted or non admitted
                if (args[0].ToUpper() == "A")
                {
                    sMode = "A";
                }
                else if (args[0].ToUpper() == "N")
                {
                    sMode = "N";
                }
                else
                {
                    throw new SystemException("Invalid mode supplied");
                }

                // Invoice date (date invoice was produced in 360)
                // Validate and store in dInvoiceDate
                sInvoiceDate = args[1];
                if (!DateTime.TryParse(sInvoiceDate, out dInvoiceDate))
                {
                    throw new SystemException("Invalid invoice date supplied");
                }

                // Invoice display date (date that appears on invoice)
                // Validate and store in sInvoiceDate
                sInvoiceDispDate = args[2];
                if (DateTime.TryParse(sInvoiceDispDate, out dInvoiceDispDate))
                {
                    sInvoiceDispDate = dInvoiceDispDate.ToShortDateString();
                }
                else
                {
                    throw new SystemException("Invalid invoice display date supplied");
                }

                // Customer number
                // Validate and store in sCustNumber
                sCustNumber = args[3];
                if (sCustNumber == "7328")
                {
                    cfg_CustName = "Risk Placement Services, Inc. 7328";
                    cfg_CustAddress = "2400 Lakeview Parkway, Suite 675";
                    cfg_CustCSZ = "Alpharetta, GA 30009";
                }
                else if (sCustNumber == "7349")
                {
                    cfg_CustName = "Risk Placement Services, Inc. 7349";
                    cfg_CustAddress = "527 Wellington Way, Suite 300";
                    cfg_CustCSZ = "Lexington, KY 40503";
                }
                else if (sCustNumber == "7374")
                {
                    cfg_CustName = "Risk Placement Services, Inc. 7374";
                    cfg_CustAddress = "2448 E 81 Street, Suite 4400";
                    cfg_CustCSZ = "Tulsa, OK 74137";
                }
                else
                {
                    throw new SystemException("Invalid customer number supplied");
                }

                // set report run date
                dt = dInvoiceDispDate;
                sReportDate = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                // Set report title
                string sDateDisp = dt.Month.ToString() + "/" + dt.Day.ToString() + "/" + dt.Year.ToString();
                string sEmailSubject = "";
                string sExcelFileNameNoEx = "";
                string sExcelFileName = "";

                if (sMode == "A")
                {
                    sExcelFileNameNoEx = cfg_outputdir + "RPS" + sCustNumber + "Invoice_A_" + dt.Month.ToString() + dt.Day.ToString() + dt.Year.ToString();
                    sExcelFileName = sExcelFileNameNoEx + ".xlsx";
                    sReportTitle = "RPS " + sCustNumber + " Invoice - Admitted " + sInvoiceDispDate;

                }
                else
                {
                    sExcelFileNameNoEx = cfg_outputdir + "RPS" + sCustNumber + "Invoice_" + dt.Month.ToString() + dt.Day.ToString() + dt.Year.ToString();
                    sExcelFileName = sExcelFileNameNoEx + ".xlsx";
                    sReportTitle = "RPS " + sCustNumber + " Invoice " + sInvoiceDispDate;

                }
                sEmailSubject = sReportTitle;
                int iRow = 0;
                string sAddress = "";


                // Get invoice info if it exists
                // Loads GUID InvoiceID into guInvoiceID, Invoice Number into iInvoiceNumber, amount into decTotalInvAmt
                if (!getInvoice(dInvoiceDate, sCustNumber))
                {
                    throw new Exception("getInvoice returned false");
                }

                if (iInvoiceNumber == 0)
                {
                    throw new Exception("No Invoice for period");
                }

                sInvoiceNum = iInvoiceNumber.ToString();

                // Create a DataSet.
                dsReportData = new DataSet("reportdata");
                dt360 = new DataTable("360");
                dsReportData.Tables.Add(dt360);

                try
                {
                    dt360.Columns.Add("CaseNum", typeof(int));
                    dt360.Columns.Add("Policy", typeof(string));
                    dt360.Columns.Add("Insured", typeof(string));
                    dt360.Columns.Add("Address1", typeof(string));
                    dt360.Columns.Add("Address2", typeof(string));
                    dt360.Columns.Add("City", typeof(string));
                    dt360.Columns.Add("State", typeof(string));
                    dt360.Columns.Add("Zip", typeof(string));
                    dt360.Columns.Add("Completed", typeof(string));
                    dt360.Columns.Add("UWFirst", typeof(string));
                    dt360.Columns.Add("UWLast", typeof(string));
                    dt360.Columns.Add("AmtBilled", typeof(decimal));
                    dt360.Columns.Add("CaseType", typeof(string));

                    // Load invoice line items into data table
                    if (!getInvoicedItems(guInvoiceID,false))
                    {
                        throw new Exception("Error loading invoice line items");
                    }

                    // sort table
                    //dt360.DefaultView.Sort = "acnt ASC";
                    dt360 = dt360.DefaultView.ToTable();

                    bool bPrintLine = false;

                    if (dt360.Rows.Count > 0)
                    {

                        oExcel = new Excel.Application();
                        oExcel.Visible = true;
                        oWorkbook = oExcel.Workbooks.Add(1);
                        o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                        o360Worksheet.Name = "Invoice";
                        createHeader(o360Worksheet);

                        iRow = 9;
                        decTotalInvAmt = 0;

                        // loop through invoice line items
                        foreach (DataRow row in dt360.Rows)
                        {

                            // If in "Admitted" mode, look for "Admitted: in case type 
                            if (sMode == "A") 
                            {
                                bPrintLine = false;
                                //Console.WriteLine(row[12].ToString().ToUpper());
                                if (row[12].ToString().ToUpper().Contains("ADMITTED"))
                                    bPrintLine = true;
                            }
                            else   // Non Admitted mode
                            {
                                bPrintLine = true;
                                if (row[12].ToString().ToUpper().Contains("ADMITTED"))
                                    bPrintLine = false;
                            }

                            if (bPrintLine)
                            {
                                sAddress = row[3].ToString() + " " + row[5].ToString() + ", " + row[6].ToString() + " " + row[7].ToString();
                                addData(o360Worksheet, iRow, 1, row[1].ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                                addData(o360Worksheet, iRow, 2, row[2].ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                                addData(o360Worksheet, iRow, 3, sAddress, "C" + iRow.ToString(), "C" + iRow.ToString(), "", "");
                                addData(o360Worksheet, iRow, 4, row[8].ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "MM/DD/YYYY", "C");
                                addData(o360Worksheet, iRow, 5, row[11].ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "$#,##0.00", "C");
                                iRow++;
                                decTotalInvAmt += Convert.ToDecimal(row[11]);
                            }

                        } // foreach


                        // Total
                        addData(o360Worksheet, iRow, 4, "Total", "D" + iRow.ToString(), "D" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 5, decTotalInvAmt.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "$#,##0.00", "C");

                        // Save Excel
                        oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                        string sPDFFile = sExcelFileNameNoEx + ".pdf";
                        oWorkbook.ExportAsFixedFormat2(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, sPDFFile);
                        oWorkbook.Close(true, oMissing, oMissing);
                        oExcel.Quit();
                        msMsgBody += "Processing completed" + System.Environment.NewLine + cfg_outputdir + sExcelFileNameNoEx + System.Environment.NewLine + System.Environment.NewLine;

                        releaseObject(oExcel);
                        releaseObject(oWorkbook);
                        releaseObject(o360Worksheet);
                        sendExcelFile(sEmailSubject, sPDFFile);
                        msMsgBody += "File sent " + sExcelFileName;

                    }   //(dtFinal.Rows.Count > 0)

                }   // try

                catch (Exception ex)
                {
                    //record exception   
                    oLU.WritetoLog(ex.Message);
                    mbErr = true;
                    msErrMsg = ex.Message;
                }

            }

            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    //sendEmail(msMsgBody);
                }

            }
        }


        static bool getInvoicedItems(Guid guInvoiceID, bool bAdmitted)
        {

            bool bRet = false;
            int iCaseNum = 0;
            string sPolicy = "";

            string sInsured = "";
            string sAddress1 = "";
            string sAddress2 = "";
            string sCity = "";
            string sState = "";
            string sZip = "";
            DateTime dCompleted;
            string sUWFirst = "";
            string sUWLast = "";
            decimal dcAmtBilled = 0;
            string sCaseType = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // init SQL connection for read query
                SqlDataReader sqlDR;
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;


                // get items on invoice
                sqlCmd1.CommandText = "sp_GetCasesOnInvoice";
                sqlCmd1.Parameters.AddWithValue("@InvoiceID", guInvoiceID);
                sqlConn1.Open();
                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        iCaseNum = (int)sqlDR.GetSqlInt32(0);

                        sPolicy = getSQLString(sqlDR, 1).ToString();
                        sInsured = getSQLString(sqlDR, 2).ToString();
                        sAddress1 = getSQLString(sqlDR, 3).ToString();
                        sAddress2 = getSQLString(sqlDR, 4).ToString();
                        sCity = getSQLString(sqlDR, 5).ToString();
                        sState = getSQLString(sqlDR, 6).ToString();
                        sZip = getSQLString(sqlDR, 7).ToString();
                        dCompleted = (DateTime)sqlDR.GetSqlDateTime(8);
                        sUWFirst = getSQLString(sqlDR, 9).ToString();
                        sUWLast = getSQLString(sqlDR, 10).ToString();
                        dcAmtBilled = (decimal)(sqlDR.GetSqlDecimal(11));
                        sCaseType = getSQLString(sqlDR, 12).ToString();


                        dt360.Rows.Add(iCaseNum, sPolicy, sInsured, sAddress1, sAddress2, sCity, sState, sZip, dCompleted, sUWFirst, sUWLast,dcAmtBilled,sCaseType);

                    }	// while


                }   // has rows
                sqlDR.Close();

                bRet = true;

            }
            catch (Exception ex)
            {

                oLU.WritetoLog(ex.Message);

            }

            finally
            {
                if (sqlConn1 != null)

                    sqlConn1.Close();
            }

            return bRet;
        }

        public static string getSQLString(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return string.Empty;
        }

        static bool getInvoice(DateTime dInvoiceDate, string sCustNum)
        {

            // Get invoice info for customer / date

            bool bRet = false;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // init SQL connection for read query
                SqlDataReader sqlDR;
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;

                sqlCmd1.CommandText = "sp_GetInvoiceForCust";
                sqlCmd1.Parameters.AddWithValue("@invdate", dInvoiceDate);
                sqlCmd1.Parameters.AddWithValue("@CustNum", sCustNum);
                sqlConn1.Open();
                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        if (sqlDR.IsDBNull(0))
                        {
                            decTotalInvAmt = 0;
                        }
                        else
                        {
                            decTotalInvAmt = (decimal)(sqlDR.GetSqlMoney(0));
                        }

                        iInvoiceNumber = (int)sqlDR.GetSqlInt32(1);

                        guInvoiceID = (Guid)sqlDR.GetSqlGuid(2);

                    }	// while


                }   // has rows
                sqlDR.Close();

                bRet = true;

            }
            catch (Exception ex)
            {

                oLU.WritetoLog(ex.Message);

            }

            finally
            {
                if (sqlConn1 != null)

                    sqlConn1.Close();
            }

            return bRet;
        }

       

    

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeader(Excel._Worksheet oWorkSheet)
        {

            string sCustName = cfg_CustName;
            if (sMode == "A")
                sCustName = cfg_CustName + "  -  ADMITTED ONLY";

            oWorkSheet.Shapes.AddPicture("C:\\temp\\SuttonLogo.jpg", Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoCTrue, 0, 0, 117, 56);

            Excel.Range oRange;

            oWorkSheet.get_Range("B1", "E1").Merge(false);
            oRange = oWorkSheet.get_Range("B1", "E1");
            oRange.FormulaR1C1 = "Sutton Inspection Bureau, Inc.";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 14;
            oRange.Font.Bold = true;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            //oWorkSheet.get_Range("D1", "E1").Merge(false);
            //oRange = oWorkSheet.get_Range("D1", "E1");
            //oRange.FormulaR1C1 = "INVOICE ";
            //oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            //oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            //oRange.Font.Size = 18;
            //oRange.Font.Bold = false;
            //oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.get_Range("B2", "E2").Merge(false);
            oRange = oWorkSheet.get_Range("B2", "E2");
            oRange.FormulaR1C1 = "5656 Central Avenue";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 10;
            oRange.Font.Bold = false;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            //oWorkSheet.get_Range("D2", "E2").Merge(false);
            //oRange = oWorkSheet.get_Range("D2", "E2");
            //oRange.FormulaR1C1 = "Invoice date: 6/1/2021";
            //oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            //oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            //oRange.Font.Size = 10;
            //oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);


            oWorkSheet.get_Range("B3", "E3").Merge(false);
            oRange = oWorkSheet.get_Range("B3", "E3");
            oRange.FormulaR1C1 = "St. Petersburg, FL 33707";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 10;
            oRange.Font.Bold = false;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            //oWorkSheet.get_Range("D3", "E3").Merge(false);
            //oRange = oWorkSheet.get_Range("D3", "E3");
            //oRange.FormulaR1C1 = "Invoice Number: 100212";
            //oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            //oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            //oRange.Font.Size = 10;
            //oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);


            //oRange = oWorkSheet.get_Range("A2", "E2");
            //oRange.FormulaR1C1 = " ";
            //oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            //oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            //oRange.Font.Size = 9;
            //oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGray);

            oRange = oWorkSheet.get_Range("A4", "E4");
            oRange.FormulaR1C1 = " ";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 9;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGray);

 
            oWorkSheet.get_Range("A5", "C5").Merge(false);
            oRange = oWorkSheet.get_Range("A5", "C5");
            oRange.FormulaR1C1 = sCustName;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 12;
            oRange.Font.Bold = true;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            oWorkSheet.get_Range("D5", "E5").Merge(false);
            oRange = oWorkSheet.get_Range("D5", "E5");
            oRange.FormulaR1C1 = "Invoice #: " + sInvoiceNum;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 12;
            oRange.Font.Bold = true;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);


            oWorkSheet.get_Range("A6", "C6").Merge(false);
            oRange = oWorkSheet.get_Range("A6", "C6");
            oRange.FormulaR1C1 = cfg_CustAddress + " " + cfg_CustCSZ;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 10;
            oRange.Font.Bold = false;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            oWorkSheet.get_Range("D6", "E6").Merge(false);
            oRange = oWorkSheet.get_Range("D6", "E6");
            oRange.FormulaR1C1 = "Invoice date: " + sInvoiceDispDate;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 10;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            oRange = oWorkSheet.get_Range("A7", "E7");
            oRange.FormulaR1C1 = " ";
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 9;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGray);
            
            oWorkSheet.Cells[8, 1] = "Policy";
            oRange = oWorkSheet.get_Range("A8", "A8");
            oRange.ColumnWidth = 12;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[8, 2] = "Insured";
            oRange = oWorkSheet.get_Range("B8", "B8");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[8, 3] = "Address";
            oRange = oWorkSheet.get_Range("C8", "C8");
            oRange.ColumnWidth = 30;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[8, 4] = "Completed";
            oRange = oWorkSheet.get_Range("D8", "D8");
            oRange.ColumnWidth = 10;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            //oWorkSheet.Cells[8, 5] = "Underwriter";
            //oRange = oWorkSheet.get_Range("E8", "E8");
            //oRange.ColumnWidth = 15;
            //oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            //oRange.Font.Bold = true;
            //oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[8, 5] = "Amount";
            oRange = oWorkSheet.get_Range("E8", "E8");
            oRange.ColumnWidth = 8;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        }




        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2, string format, string sHorizAlign)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            oRange.Font.Size = 8;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else if (sHorizAlign == "R")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
        }

        static void addDataWColor(Excel._Worksheet oWorkSheet, int row, int col, string data,
           string cell1, string cell2, string format, string sHorizAlign, int ibgColor)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }

            if (ibgColor == 1)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.PaleGreen);
            }
            else if (ibgColor == 2)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightPink);
            }
            else if (ibgColor == 3)
            {
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            }

        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Font.Size = 8;
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }

        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "RPS Monthly Invoice";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by RPSInvoicing Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = cfg_deliverto;

                //oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }
    }
}
