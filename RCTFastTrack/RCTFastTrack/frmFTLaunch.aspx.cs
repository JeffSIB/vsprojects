﻿using System;
using System.Web.UI;
using System.Configuration;
using System.ServiceModel;
using RCTFastTrack.RCT_ValuationService;

namespace RCTFastTrack
{
    public partial class frmFTLaunch : System.Web.UI.Page
    {
        static string sVal = "";
        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;

        protected void Page_Load(object sender, EventArgs e)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();
            string sVal = "";
            try
            {
                if (!Page.IsPostBack)
                {
                    sVal = Request.QueryString["val"];
                    sVal = sVal.Trim();

                    // load configuration values from app.config
                    System.Collections.Specialized.NameValueCollection colNameVal;
                    colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                    cfg_smtpserver = colNameVal.Get("smtpserver");
                    cfg_logfilename = colNameVal.Get("logfilename");

                    // set log file name
                    oLU.logFileName = cfg_logfilename;

                    // open log file
                    oLU.OpenLog();
                    oLU.WritetoLog("Preparing link for case#: " + sVal);

                    if (sVal.Length < 1)
                    {
                        lblStatus.Text = "Unable to load valuation - please contact Sutton Support.";
                        oLU.WritetoLog("**** NO VALUATION PASSED ****");
                        throw new ApplicationException("**** NO VALUATION PASSED ****");
                    }

                    ChannelFactory<ValuationService> channelFactory = new ChannelFactory<ValuationService>("ValuationServiceEndpoint");
                    channelFactory.Credentials.UserName.UserName = ConfigurationManager.AppSettings["UserName"];
                    channelFactory.Credentials.UserName.Password = ConfigurationManager.AppSettings["Password"];
                    ValuationService valuationService = channelFactory.CreateChannel();

                    // Establish FastTrackRequest
                    FastTrackRequest getFastTrackRequest = new FastTrackRequest();
                    getFastTrackRequest.UserLogin = ConfigurationManager.AppSettings["UserName"];  // Specify UserLogin of user to be logged into RCT Express
                    getFastTrackRequest.ValuationIdentifier = new ValuationIdentifier();
                    getFastTrackRequest.ValuationIdentifier.PolicyNumber = sVal;                   //Specify the valuation's unique identifier
                    //getFastTrackRequest.FirstName = "New";   //optional
                    //getFastTrackRequest.LastName = "User";  //optional
                    //getFastTrackRequest.AgencyName = "NewTestAgency";   //only required when creating a new user or agency
                    getFastTrackRequest.Operation = FastTrackOperationCode.Edit;
                    //getFastTrackRequest.Role = "Agent";     //only required when creating a new user             
                    getFastTrackRequest.RedirectURL = "http://www.sibfla.com";

                    // ExpressLync4 ValutionService web service call to web method GetFastTrackURL:
                    FastTrackResult getFastTrackResult = valuationService.GetFastTrackURL(getFastTrackRequest, null, null);

                    // Display URL on writeline
                    Console.WriteLine("FastTrackURL: {0}", getFastTrackResult.FastTrackURL);

                    // Open url in browser
                    Response.Redirect(getFastTrackResult.FastTrackURL, false);

                    oLU.WritetoLog("Valuation launched.");
                    lblStatus.Text = "Valuation launched.";

                }
            }
            catch (Exception ex)
            {

                //catch exception  
                lblStatus.Text = "An error occurred accessing the valuation. Please contact Sutton support.";

                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message + "\r\n\r\n" + sVal);

            }

            finally
            {
                // close log
                if (oLU != null)
                    oLU.closeLog();
            }

        }

 
        static void sendErrEmail(string bodytext)
        {

            string sRet;

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            string smtpserver = colNameVal.Get("smtpserver");

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com; kim@sibfla.com";
            oMail.MsgSubject = "** Errors logged by RCTFastTrack **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

    }
}