﻿using System;
using System.Web.UI;
using System.Configuration;
using System.ServiceModel;

namespace RCTFastTrack
{
    public partial class frmFT : System.Web.UI.Page
    {
        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;


        protected void Page_Load(object sender, EventArgs e)
        {

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");

            //if (!Page.IsPostBack)
            //{
            string sVal = Request.QueryString["val"];
            hiVal.Value = sVal;
            trMode1.Visible = false;
            trMode2.Visible = false;

            if (sVal == null)
            {
                trMode1.Visible = true;
                lblModeMsg.Text = "This process will launch you into the valuation for the selected inspection.";
            }
            else
            {
                trMode2.Visible = true;
                lblModeMsg.Text = "This process will launch you into the valuation for the current inspection.";
            }
            //}
        }

        protected void bSubmit_Click(object sender, EventArgs e)
        {

            string sVal = hiVal.Value;
            hiRedir.Value = "frmFTLaunch.aspx?val=" + sVal;

            bSubmit.Visible = false;
            lblStatus.Text = "Please wait.....";

            //// load configuration values from app.config
            //System.Collections.Specialized.NameValueCollection colNameVal;
            //colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            //cfg_smtpserver = colNameVal.Get("smtpserver");
            //cfg_logfilename = colNameVal.Get("logfilename");

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();
            oLU.WritetoLog("Initiating call for case#: " + sVal);
            oLU.closeLog();
        }

        protected void bSubmit2_Click(object sender, EventArgs e)
        {
            if (tbCaseNum.Text.Length < 7)
            {
                lblStatus.Text = "Please enter a valid case number.";
                lblStatus.ForeColor = System.Drawing.Color.Maroon;
            }
            else
            {
                string sVal = hiVal.Value;
                hiRedir.Value = "frmFTLaunch.aspx?val=" + tbCaseNum.Text;

                bSubmit.Visible = false;
                lblStatus.Text = "Please wait.....";
                lblStatus.ForeColor = System.Drawing.Color.Black;

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("Initiating call for case#: " + tbCaseNum.Text);
                oLU.closeLog();

            }
        }
    }
}