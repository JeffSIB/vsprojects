﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmFT.aspx.cs" Inherits="RCTFastTrack.frmFT" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sutton Inspection Bureau</title>
    <link href="Site.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript">

        function setfocus() {
            var ref = document.getElementById('hiRedir').value;
            if (ref.length > 0) {
                location.href = ref;

            }
            else {
                document.getElementById('rbOpen').focus();
            }
        }
        function cancelForm() {
            window.opener = top;
            redirectTimerId = window.setTimeout('redirect()', 2000);
            window.close();
        }

 

    </script>
</head>
<body onload="setfocus()">
    <form id="frmMain" runat="server">
        <main>
            <div>
                <div id="div1" runat="server" style="width: 100%;">
                    <table style="width: 100%; border-bottom: gray thin solid">
                        <tr>
                            <td style="width: 80%">
                                <asp:Label ID="lblTitle" runat="server" CssClass="StdTextGreySmallCap">
								&nbsp;Sutton Inspection Bureau Valuation Portal
                                </asp:Label>
                            </td>
                            <td style="width: 20%; text-align: center;">
                                <input class="StdText" onclick="cancelForm()" tabindex="500" type="button" value="Cancel"
                                    name="bCancel" />
                            </td>
                        </tr>
                    </table>
                    <table id="tbl1" style="width: 100%; border-right: #c0c0c0 thin solid; border-top: #c0c0c0 thin solid; border-left: #c0c0c0 thin solid; border-bottom: #c0c0c0 thin solid; border: 0; border-spacing: 0; padding: 2px"
                        runat="server">
                        <tr style="height: 25px; background-color: #e4ffe4">
                            <td style="width: 700px" class="StdTextSmall">
                                <asp:Label ID="lblTitle2" runat="server" CssClass="StdText">
								&nbsp;Sutton Valuation Login
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width: 100%; border: 0; border-spacing: 0; padding: 2px">
                                    <tr>
                                        <td style="width: 10%"></td>
                                        <td style="width: 90%"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" colspan="2">
                                            <asp:Label ID="Label2" runat="server" CssClass="StdTextSmallGrey">
				                        <p>This process will log you into the valuation for the current inspection.</p>
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                        <td style="text-align: left; vertical-align: middle" class="StdText">
                                            <asp:Button ID="bSubmit" runat="server" Text="Launch valuation" OnClick="bSubmit_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblStatus" runat="server" CssClass="StdText">&nbsp;
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </main>
        <input id="hiVal" type="hidden" runat="server" name="hiVal" />
    	<input id="hiRedir" type="hidden" runat="server"/>

    </form>

</body>
</html>
