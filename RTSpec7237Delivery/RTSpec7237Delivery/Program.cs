﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Word = Microsoft.Office.Interop.Word;


namespace RTSpec7237Delivery
{
    class Program
    {

        static void Main(string[] args)
        {

            Setup setup = new Setup();

        }


        /// <summary>
        /// Instantiate properties from the AppSettings located in the App.config.
        /// </summary>
        public class Setup
        {

            private string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
            private string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
            private string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            private string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
            private string cfg_recroot = ConfigurationManager.AppSettings["RecRoot"];
            private string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
            private string cfg_exportapp = ConfigurationManager.AppSettings["ExportApp"];
            private string cfg_recapp = ConfigurationManager.AppSettings["RecApp"];
            private string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];


            private LogUtils.LogUtils oLU;
            private bool bErr = false;
            private string sPolicy = "";
            private string sInsured = "";
            private string sCaseNum = "";
            private Guid guCaseID;
            private string sCustNum = "";
            private string sCustName = "";
            private string sAmount = "";
            private string sDeliveryDate = "";
            private string sBatchNum = "";


            //Used for email message body.  
            private StringBuilder sbEmail = new StringBuilder();

            //Used for transmittal message body.  
            private StringBuilder sbmsgBody = new StringBuilder();

            public Setup()
            {
                int iCasesExported = 0;

                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                bool bExport = true;
                bool bCreateRecs = true;
                bool bEmailFiles = true;

                DirectoryInfo diPDF;
                DirectoryInfo diCaseFiles;

                FileInfo[] fiPDFFiles;
                FileInfo fiRec;
                oLU.WritetoLog("Begin Processing  " + DateTime.Now.ToString());
                sbEmail.Append("Begin Processing  " + DateTime.Now.ToString() + "\r\n\r\n");

                try
                {

                    //***********************************************************
                    // Export PDF's from 360
                    if (bExport)
                    {
                        try
                        {

                            // Delete all files in PDF Export directory.
                            string[] sFiles = Directory.GetFiles(cfg_pdfroot);
                            foreach (string sFile in sFiles)
                                File.Delete(sFile);

                            // Export PDF's from 360
                            oLU.WritetoLog(" Begin exporting completed cases from 360");
                            sbEmail.Append("---- Begin exporting completed cases from 360\r\n\r\n");
                            //oLU.closeLog();

                            //oLU.OpenLog();
                            //oLU.WritetoLog(cfg_exportapp);

                            var proc = Process.Start(cfg_exportapp);
                            proc.WaitForExit();
                            var exitCode = proc.ExitCode;
                            sbEmail.Append("Exporter returned: " + exitCode.ToString() + "\r\n");

                            // exit code should be the number of cases exported
                            iCasesExported = (int)exitCode;

                            Console.WriteLine(exitCode.ToString());

                        }
                        catch (Exception ex)
                        {
                            sbEmail.Append("**** ERROR IN EXPORT:\r\n" + ex.Message);
                            oLU.WritetoLog(sbEmail.ToString());
                            bErr = true;
                            //oLU.closeLog();

                            //oLU.OpenLog();

                        }

                        oLU.WritetoLog(" Export complete. Exported: " + iCasesExported.ToString());
                        sbEmail.Append("---- Export complete. \r\n\r\n");


                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        if (fiPDFFiles.Count() != iCasesExported)
                        {
                            sbEmail.Append("**** PDF count does not match number exported ****\r\n");
                            sbEmail.Append("Exported: " + iCasesExported.ToString() + " - In folder: " + fiPDFFiles.Count().ToString() + "\r\n\r\n");
                        }

                    }

                    if (bCreateRecs && !bErr)
                    {

                        oLU.WritetoLog(" Begin processing recommendations");
                        sbEmail.Append("---- Begin processing recommendations\r\n\r\n");

                        //***********************************************************
                        // Build Rec docs for all PDF files that were extracted


                        //************************************************
                        // Test for connection to casefiles
                        //************************************************
                        try
                        {
                            diCaseFiles = new DirectoryInfo(cfg_casefilesroot);
                            if (!diCaseFiles.Exists)
                                throw new ApplicationException("Unable to access case files root: " + cfg_casefilesroot + "\r\n");

                        }
                        catch (Exception ex)
                        {
                            sbEmail.Append("**** ERROR ACCESSING CASEFILES FOLDER:\r\n" + ex.Message);
                            oLU.WritetoLog(sbEmail.ToString());
                            bErr = true;
                        }

                        if (!bErr)
                        {

                            // Delete all files in Rec folder.
                            string[] sFiles = Directory.GetFiles(cfg_recroot);
                            foreach (string sFile in sFiles)
                                File.Delete(sFile);

                            sbEmail.Append("\r\n\r\n---- Begin creating REC Docs\r\n");

                            diPDF = new DirectoryInfo(cfg_pdfroot);
                            fiPDFFiles = diPDF.GetFiles("*.pdf");

                            foreach (FileInfo file in fiPDFFiles)
                            {

                                sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));

                                ProcessStartInfo psi = new ProcessStartInfo();
                                psi.FileName = cfg_recapp;
                                psi.Arguments = sCaseNum;

                                var proc1 = Process.Start(psi);
                                proc1.WaitForExit();
                                var exitCode1 = proc1.ExitCode;
                                Console.WriteLine(exitCode1.ToString());

                                // verify rec doc was created
                                fiRec = new FileInfo(cfg_recroot + sCaseNum + ".docx");
                                if (fiRec.Exists)
                                {
                                    // success
                                    sbEmail.Append("Rec doc created for: " + sCaseNum + "\r\n");
                                }
                                else
                                {
                                    // fail
                                    sbEmail.Append("No rec doc for : " + sCaseNum + "\r\n");
                                }
                                fiRec = null;
                            }
                        }
                    }

                    //***********************************************************
                    // Email PDF's & Recs 
                    // create transmittal email and send if successful
                    if (bEmailFiles && !bErr)
                    {

                        //////////////////////////////
                        // Email all files in PDF folder
                        // attach rec if it exists
                        //////////////////////////////
                        int iNumEmailed = 0;
                        int iNumToEmail = 0;
                        string sRecDoc = "";
                        string sRecip = "";

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.*");

                        iNumToEmail = fiPDFFiles.Count();
                        if (iNumToEmail == 0)
                        {
                            sbEmail.Append("\r\n\r\n---- No Files to send\r\n");
                        }
                        else
                        {
                            sbEmail.Append("\r\n\r\n---- Begin emailing files\r\n");
                            sbEmail.Append("\r\n\r\n" + iNumToEmail.ToString() + " files to send\r\n");
                        }
                        try
                        {

                            foreach (FileInfo file in fiPDFFiles)
                            {

                                // Get case # from file name
                                sCaseNum = file.Name.Substring(0, file.Name.IndexOf("."));

                                // Get Case info
                                if (!GetCaseInfo(sCaseNum))
                                {
                                    sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                    throw new ApplicationException("GetInfo failed for case: " + sCaseNum);
                                }

                                // if rec doc exists, send it
                                sRecDoc = cfg_recroot + "\\" + sCaseNum + ".docx";
                                if (!File.Exists(sRecDoc))
                                {
                                    sRecDoc = "";
                                }

                                sbEmail.Append("Processing case:" + sCaseNum + "\r\n\r\n");

                                if (sRecDoc == "")
                                {
                                    sbEmail.Append("No Rec for : " + sCaseNum + "\r\n\r\n");
                                }
                                else
                                {
                                    sbEmail.Append("Sending Rec for: " + sCaseNum + "\r\n\r\n");
                                }


                                // Set Recip based on policy prefix
                                sPolicy = sPolicy.ToUpper();
                                if (sPolicy.StartsWith("PLNMB") || sPolicy.StartsWith("NMB") || sPolicy.StartsWith("HIX") || sPolicy.StartsWith("AML"))
                                {
                                    sRecip = "Vesna.Djurdjevic@rtspecialty.com";
                                }
                                else
                                {
                                    sRecip = "rthinspections@solartisglobal.com";
                                }

                                sbEmail.Append("Sending : " + sCaseNum + " - " + sPolicy + " to " + sRecip + "\r\n");

                                sendCaseEmail(sInsured, sPolicy, sRecip, file.FullName, sRecDoc);

                                iNumEmailed++;

                                System.Threading.Thread.Sleep(5000);

                            }
                            sbEmail.Append("\r\n" + iNumEmailed.ToString() + " files sent\r\n\r\n");
                        }
                        catch (Exception ex)
                        {
                            bErr = true;
                            sbEmail.Append("**** Error emailing files\r\n\r\n" + ex.Message + "\r\n\r\n");
                        }

                    }   // if email

                    oLU.WritetoLog(sbEmail.ToString());

                    if (bErr)
                    {
                        sendLogEmail("**** ERROR RECORDED - RTSpec7237 Delivery Processing ****", sbEmail.ToString());
                    }
                    else
                    {
                        sendLogEmail("RTSpec7237 Delivery Processing", sbEmail.ToString());
                    }

                }
                catch (Exception ex)
                {
                    bErr = true;
                    sbEmail.Append("**** Error ****\r\n\r\n" + ex.Message + "\r\n\r\n");
                    oLU.WritetoLog(sbEmail.ToString());
                    oLU.closeLog();
                }

                finally
                {
                    oLU.closeLog();
                }

            }

            private void sendLogEmail(string sSubject, string sBodyText)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "noreply@sibfla.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;
                }
            }

            private void sendCaseEmail(string sInsured, string sPolicy, string sTO, string sPDF, string sRec)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
                string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "inspections@sibfla.com";
                    oMail.MailTo = sTO;
                    //oMail.MailTo = "jeff@sibfla.com";
                    //oMail.MailBCC = "jeff@sibfla.com";
                    oMail.MsgSubject = "Completed inspection: " + sInsured + " - " + sPolicy;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    oMail.Attachment = sPDF;
                    oMail.Attachment2 = sRec;
                    sRet = oMail.Send();
                    if (sRet != "")
                    {
                        throw new Exception("SendMail returned: " + sRet);
                    }
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

            }

            private void sendTransmittal()
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "sibflamail@sibfla.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    //oMail.MailBCC = "jeff@sibfla.com";
                    oMail.MsgSubject = "Transmittal - " + DateTime.Today.ToShortDateString();
                    oMail.MsgBody = sbmsgBody.ToString();
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = true;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;
                }
            }
            private bool GetCaseInfo(string sCaseNum)
            {

                bool bRetVal = false;

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;
                string sFirst = "";
                string sLast = "";

                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseInfo_CaseNum";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {
                            guCaseID = sqlReader.GetGuid(1);
                            sPolicy = sqlReader.GetSqlString(3).ToString();
                            sInsured = sqlReader.GetSqlString(2).ToString();
                            sCustName = sqlReader.GetSqlString(10).ToString().Trim();
                            sCustNum = sqlReader.GetSqlString(11).ToString().Trim();
                            sAmount = sqlReader.GetSqlDecimal(12).ToString();
                            sFirst = sqlReader.GetSqlString(16).ToString();
                            sLast = sqlReader.GetSqlString(17).ToString();

                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }

                    //// IR FileNum
                    //sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                    //sqlCmd1.Parameters.Clear();
                    //sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                    //sqlCmd1.Parameters.AddWithValue("@fieldname", "ImageRight File Number");

                    //sIRFileNum = "";
                    //object oRetVal = sqlCmd1.ExecuteScalar();

                    //if (oRetVal != null)
                    //{
                    //    sIRFileNum = (string)oRetVal;
                    //}

                    //if (sIRFileNum == "")
                    //{
                    //    sbEmail.Append("\r\n" + "No IR File Num for " + sCaseNum + " \r\n");
                    //}

                    //sUnderwriter = sFirst + "." + sLast;

                    //// Underwriter code
                    //sqlCmd1.CommandText = "sp_GetUnderwriterCode";
                    //sqlCmd1.Parameters.Clear();
                    //sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    //object oRetVal = sqlCmd1.ExecuteScalar();

                    //sIRUID = "";

                    //if (oRetVal != null)
                    //{
                    //    sIRUID = (string)oRetVal;
                    //}

                    //if (sIRUID == "")
                    //{
                    //    sbEmail.Append("\r\n" + "No Underwriter code for " + sCaseNum + " \r\n");
                    //}

                    bRetVal = true;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();

                }

                return bRetVal;
            }

            private void buildHeader()
            {

                // <head>
                sbmsgBody = sbmsgBody.Append("<html><head><title>Sutton Inspection Bureau, Inc.of Florida</title> " + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<style type='text/css'>.stdText {font-size: 8pt;COLOR: black;font-family: Verdana,Tahoma,Arial;}" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append(".stdTextBold {font-size: 8pt;color: black;font-family: Verdana,Tahoma,Arial;font-weight: bold;}" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append(".largeText {font-size: 12pt;color: black;font-family: Verdana,Tahoma,Arial;font-weight: bold;}</style></head>" + System.Environment.NewLine);

                // <body>
                sbmsgBody = sbmsgBody.Append("<body><table cellSpacing = '0' cellPadding = '2' width = '760' border = '0'><tr><td width = '10'> &nbsp;</td><td align = 'left' width = '475'><span class='stdText'>Sutton Inspection Bureau, Inc.of Florida</span></td><td width = '275'> &nbsp;</td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td> &nbsp;</td><td align = 'left'><span class='stdText'>5656 Central Avenue</span></td><td align = 'right'><span class='largeText'>Delivery Transmittal</span></td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td> &nbsp;</td><td align = 'left'><span class='stdText'>St.Petersburg,  FL 33707-1718</span></td><td>&nbsp;</td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='0' width='760' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='750'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='760' border='0'><tr><td width = '10'>&nbsp;</td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align= right' width= 60'><span Class='stdText'>Account:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='left' width='340'><span Class='stdText'>SCU - Boca</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right' width='240'><span Class='stdText'>Delivery date:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right' width='110'><span Class='stdText'>" + sDeliveryDate + "</span></td></tr>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right'><span Class='stdText'>Batch Number:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right'><span Class='stdText'>" + sBatchNum.Trim() + "</span></td></tr></table>" + System.Environment.NewLine);

                //sbmsgBody = sbmsgBody.Append("</table>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='0' width='760' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='750'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='760' style='border - bottom:solid; '><tr><td align = 'center' width = '120'><span Class='stdTextBold'> Policy </span></td>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> IR File</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='360'><span Class='stdTextBold'> Insured </span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> Case</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> Amount </span></td></tr>" + System.Environment.NewLine);

            }

            private void buildFooter()
            {
                sbmsgBody = sbmsgBody.Append("</table></body></html>");

            }

            private void buildLineItem(string sPolicy, string sIR, string sInsured, string sCaseNum, string sAmount)
            {

                sbmsgBody = sbmsgBody.Append("<tr><td align='center'><span Class='stdText'>" + sPolicy + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'> " + sIR + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='left'><span Class='stdText'>" + sInsured + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'>" + sCaseNum + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'>" + sAmount + "</span></td></tr>");
                
            }
        }
    }
}
