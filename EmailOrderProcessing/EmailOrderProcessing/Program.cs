﻿using System;
using Microsoft.Exchange.WebServices.Data;
using System.Configuration;
using System.Diagnostics;
using System.Xml;


namespace EmailOrderProcessing
{
    //class TraceListener : ITraceListener
    //{
    //    #region ITraceListener Members
    //    public void Trace(string traceType, string traceMessage)
    //    {
    //        CreateXMLTextFile(traceType, traceMessage.ToString());
    //    }
    //    #endregion
    //    //private void CreateXMLTextFile(string fileName, string traceContent)
    //    //{
    //    //    // Create a new XML file for the trace information.
    //    //    try
    //    //    {
    //    //        // If the trace data is valid XML, create an XmlDocument object and save.
    //    //        XmlDocument xmlDoc = new XmlDocument();
    //    //        xmlDoc.Load(traceContent);
    //    //        xmlDoc.Save(fileName + ".xml");
    //    //    }
    //    //    catch
    //    //    {
    //    //        // If the trace data is not valid XML, save it as a text document.
    //    //        System.IO.File.WriteAllText(fileName + ".txt", traceContent);
    //    //    }
    //    //}
    //}
    class Program
    {
        static LogUtils.LogUtils oLU;
        static string cfg_attdir = ConfigurationManager.AppSettings["attdir"].ToString();
        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_amwinsimportapp = ConfigurationManager.AppSettings["amwinsimportapp"];
        static string cfg_towerhillimportapp = ConfigurationManager.AppSettings["towerhillimportapp"];
        static string cfg_bankersimportapp = ConfigurationManager.AppSettings["bankersimportapp"];
        static string cfg_orchidimportapp = ConfigurationManager.AppSettings["orchidimportapp"];
        static string cfg_bassimportapp = ConfigurationManager.AppSettings["bassimportapp"];
        static string cfg_siuimportapp = ConfigurationManager.AppSettings["siuimportapp"];
        static string cfg_macneillimportapp = ConfigurationManager.AppSettings["macneillimportapp"];
        static string cfg_rtspecimportapp = ConfigurationManager.AppSettings["rtspecimportapp"];

        static void Main(string[] args)
        {

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                //TraceListener oTL = new TraceListener();
                //oTL.Trace()

                // connect to Exchange Online
                ExchangeService serviceInstance = new ExchangeService(ExchangeVersion.Exchange2013_SP1);
                serviceInstance.Credentials = new WebCredentials(ConfigurationManager.AppSettings["User"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
                serviceInstance.TraceEnabled = true;
                serviceInstance.TraceFlags = TraceFlags.All;
                serviceInstance.AutodiscoverUrl(ConfigurationManager.AppSettings["MailBox"].ToString(), RedirectionUrlValidationCallback);
                //serviceInstance.TraceListener = oTL;
                //serviceInstance.TraceFlags = TraceFlags.EwsRequest | TraceFlags.EwsResponse;
                //serviceInstance.TraceEnabled = true;

                //// The search filter to get unread email.
                //SearchFilter sf = new SearchFilter.SearchFilterCollection(LogicalOperator.And, new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false));
                //ItemView view = new ItemView(1);

                // Return a single item.
                //ItemView view = new ItemView(1);

                //string querystring = "HasAttachments:true Kind:email Sent:yesterday";
                //string querystring = "Kind:email HasAttachments:true Received:02/22/2017";

                // Find the first email message in the Inbox that has attachments. This results in a FindItem operation call to EWS.
                //FindItemsResults<Item> results = serviceInstance.FindItems(WellKnownFolderName.Inbox, querystring, view);

                // Get all email in Inbox (Max 50)
                string querystring = "Kind:email";

                FindItemsResults<Item> results = serviceInstance.FindItems(WellKnownFolderName.Inbox, querystring, new ItemView(20));

                string sSender = "";
                string sSenderCode = "";
                string sBodyPrefix = "Auto forwarded from sibflamail@sibfla.com";

                // if anything to process
                if (results.TotalCount > 0)
                {

                    oLU.WritetoLog("Total items count: " + results.TotalCount.ToString());

                    // step through messages
                    foreach (EmailMessage email in results)
                    {
                        oLU.WritetoLog("Processing: " + email.Sender.Address + " / " + email.Subject + " / " + email.DateTimeReceived.ToString());

                        //Identify sender
                        sSenderCode = "";
                        sSender = email.Sender.Address.ToUpper();

                        if (sSender.ToUpper().Contains("THIG.COM"))
                            processTowerHill(email);

                        else if (sSender.ToUpper().Contains("BANKERS"))
                            processBankers(email);

                        else if (sSender.ToUpper().Contains("SIUINS"))
                            processSIU(email);

                        else if (sSender.ToUpper().Contains("MACNEILL") || sSender.ToUpper().Contains("TEAMFOCUSINS"))
                            processMacNeill(email);

                        else if (sSender.ToUpper().Contains("ORCHID"))
                            processOrchid(email);

                        else if (sSender.ToUpper().Contains("OCEANWIDEBRIDGE"))
                        {
                            processOrchidConnect(email);
                        }

                        //else if (sSender.ToUpper().Contains("PROMONT"))
                        //    sSenderCode = "PRO";

                        else if (sSender.ToUpper().Contains("ASCENDANT") || sSender.ToUpper().Contains("ACICOMPANIES"))
                            processAscendant(email);

                        else if (sSender.ToUpper().Contains("INSPECTIONS@BASSUW") || sSender.ToUpper().Contains("INSTECHNOLOGIES.NET"))
                            processBass(email);

                        else if (sSender.ToUpper().Contains("AMWINS") && email.Subject.ToUpper().Contains("INSPECTION REQUEST"))
                        {
                            processAMWins(email);
                        }
                        else if (sSender.ToUpper().Contains("RTSPECIALTY"))
                        {
                             processRTSpecialty(email);
                        }


                        else
                        {
                            // if email was not recognized, forward to customer service
                            EmailAddress[] addresses = new EmailAddress[1];
                            addresses[0] = new EmailAddress("customerservice@sibfla.com");
                            oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to customerservice@sibfla.com");

                            email.Forward(sBodyPrefix, addresses);
                            email.Delete(DeleteMode.MoveToDeletedItems, true);
                        }

                    }   // foreach message


                }   // messages to process
            }
            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail("Main: " + ex.Message);
            }
            finally
            {
                oLU.closeLog();
            }

        }

        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;

            Uri redirectionUri = new Uri(redirectionUrl);

            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }

        private static void processTowerHill(EmailMessage email)
        {
            // Tower Hill
            // Save Excel attachments to target folder and process


            string sAttName = "";
            string sAttType = "";
            int iPos = 0;
            string sFileName = "";
            DateTime dNow;

            try
            {

                // if email is from comminspec#thig.com - delete it
                if (email.Sender.Address.ToUpper() == "COMMINSPEC@THIG.COM")
                {
                    email.Delete(DeleteMode.MoveToDeletedItems, true);
                }
                else
                {
                    // if there are attachments
                    if (email.HasAttachments)
                    {
                        email.Load(new PropertySet(EmailMessageSchema.Attachments));

                        foreach (Attachment attachment in email.Attachments)
                        {
                            // If there are attachments
                            if (attachment is FileAttachment)
                            {
                                FileAttachment fileAttachment = attachment as FileAttachment;

                                oLU.WritetoLog("    Attachment: " + fileAttachment.Name);

                                // get file extension of attachment
                                sAttName = fileAttachment.Name.ToUpper();
                                iPos = sAttName.LastIndexOf('.');
                                if (iPos > 0)
                                {

                                    // get attachment type
                                    sAttType = getFileType(sAttName);

                                    // If Excel
                                    if (sAttType == "XLS" || sAttType == "XLSX")
                                    {
                                        // Build file name in the format YYYYHHMMSS
                                        dNow = DateTime.Now;
                                        sFileName = "THL" + dNow.Year.ToString() + dNow.Hour.ToString() + dNow.Minute.ToString() + dNow.Second.ToString() + "." + sAttType;

                                        // Save attachment to SIBI\SibiData\Tmp
                                        fileAttachment.Load(cfg_attdir + sFileName);
                                        oLU.WritetoLog("    Saved as: " + sFileName);

                                        ProcessStartInfo psi = new ProcessStartInfo();
                                        psi.FileName = cfg_towerhillimportapp;
                                        psi.Arguments = sFileName;
                                        oLU.WritetoLog("    Importing: " + sFileName);
                                        var proc1 = Process.Start(psi);
                                        proc1.WaitForExit();
                                        int iExitCode = proc1.ExitCode;
                                        if (iExitCode == 0)
                                        {
                                            oLU.WritetoLog("    Import returned: " + iExitCode.ToString());
                                        }
                                        else
                                        {
                                            // Log error 
                                            throw new Exception("****TowerHill import returned: " + iExitCode.ToString());
                                        }

                                    }   // Excel file

                                }   // iPos > 0

                            }   // File attachment

                        }   // for each atachment

                        email.Delete(DeleteMode.MoveToDeletedItems, true);

                    }   // has attachments
                    else
                    {
                        // no attachments, forward to customer service
                        EmailAddress[] addresses = new EmailAddress[1];
                        addresses[0] = new EmailAddress("customerservice@sibfla.com");
                        oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to customerservice@sibfla.com");

                        email.Forward("Auto forwarded from sibflamail@sibfla.com", addresses);
                        email.Delete(DeleteMode.MoveToDeletedItems, true);
                    }


                }   // from comminspec
            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);
            }
        }

        private static void processAMWins(EmailMessage email)
        {
            // AMWins 7301
            // Save Excel attachment to target folder and process


            string sAttName = "";
            string sAttType = "";
            int iPos = 0;
            int iBegPos = 0;
            int iEndPos = 0;
            string sFileName = "";
            string sSubject = email.Subject;
            string sPolicy = "";
            DateTime dNow;

            try
            {

                // if there are attachments
                if (email.HasAttachments)
                {
                    email.Load(new PropertySet(EmailMessageSchema.Attachments));

                    foreach (Attachment attachment in email.Attachments)
                    {
                        // If there are attachments
                        if (attachment is FileAttachment)
                        {
                            FileAttachment fileAttachment = attachment as FileAttachment;

                            oLU.WritetoLog("    Attachment: " + fileAttachment.Name);

                            // get file extension of attachment
                            sAttName = fileAttachment.Name.ToUpper();
                            iPos = sAttName.LastIndexOf('.');
                            if (iPos > 0)
                            {

                                // get attachment type
                                sAttType = getFileType(sAttName);

                                // If Excel
                                if (sAttType == "XLS" || sAttType == "XLSX")
                                {

                                    // Get policy from subject
                                    iBegPos = sSubject.IndexOf('(');
                                    iEndPos = sSubject.IndexOf(')');
                                    if (iBegPos > 0 && iEndPos > 0)
                                    {
                                        sPolicy = sSubject.Substring(iBegPos + 1, (iEndPos - iBegPos) - 1);
                                    }
                                    else
                                    {
                                        throw new Exception("****AMWins import error - No Policy Number: " + email.Subject);
                                    }

                                    // Build file name in the format YYYYHHMMSS
                                    dNow = DateTime.Now;
                                    sFileName = sPolicy + "." + sAttType;

                                    // Save attachment to SIBI\SibiData\Tmp
                                    fileAttachment.Load(cfg_attdir + sFileName);
                                    oLU.WritetoLog("    Saved as: " + sFileName);

                                    ProcessStartInfo psi = new ProcessStartInfo();
                                    psi.FileName = cfg_amwinsimportapp;
                                    psi.Arguments = sFileName;
                                    oLU.WritetoLog("    Importing: " + sFileName);
                                    var proc1 = Process.Start(psi);
                                    proc1.WaitForExit();
                                    int iExitCode = proc1.ExitCode;
                                    if (iExitCode == 0)
                                    {
                                        oLU.WritetoLog("    Import returned: " + iExitCode.ToString());
                                    }
                                    else
                                    {
                                        // Log error 
                                        throw new Exception("****AMWins import returned: " + iExitCode.ToString());
                                    }
                                }   // Excel file

                            }   // iPos > 0

                        }   // File attachment

                    }   // for each atachment

                    email.Delete(DeleteMode.MoveToDeletedItems, true);

                }   // has attachments
                else
                {
                    // no attachments - forward to customerservice@sibfla.com
                    // where it will be fowarded to Lealyn
                    EmailAddress[] addresses = new EmailAddress[1];
                    addresses[0] = new EmailAddress("customerservice@sibfla.com");
                    oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to customerservice@sibfla.com");

                    email.Forward("Auto forwarded from sibflamail@sibfla.com", addresses);
                    email.Delete(DeleteMode.MoveToDeletedItems, true);
                }

            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);
            }
        }


        private static void processBankers(EmailMessage email)
        {
            // Bankers
            // Save Excel attachments to target folder and process


            string sAttName = "";
            string sAttType = "";
            int iPos = 0;
            string sFileName = "";
            DateTime dNow;

            try
            {

                //if there are attachments
                if (email.HasAttachments)
                {
                    email.Load(new PropertySet(EmailMessageSchema.Attachments));

                    foreach (Attachment attachment in email.Attachments)
                    {
                        // If there are attachments
                        if (attachment is FileAttachment)
                        {
                            FileAttachment fileAttachment = attachment as FileAttachment;

                            oLU.WritetoLog("    Attachment: " + fileAttachment.Name);

                            // get file extension of attachment
                            sAttName = fileAttachment.Name.ToUpper();
                            iPos = sAttName.LastIndexOf('.');
                            if (iPos > 0)
                            {

                                // get attachment type
                                sAttType = getFileType(sAttName);

                                // If Excel
                                if (sAttType == "XLS" || sAttType == "XLSX")
                                {
                                    // remove spaces from file name
                                    sFileName = sAttName.Replace(" ", String.Empty);

                                    // Build file name in the format YYYYHHMMSS + original file name with spaces removed
                                    dNow = DateTime.Now;
                                    sFileName = "BNK" + dNow.Year.ToString() + dNow.Hour.ToString() + dNow.Minute.ToString() + dNow.Second.ToString() + "-" + sFileName;

                                    // Save attachment to SIBI\SibiData\Tmp
                                    fileAttachment.Load(cfg_attdir + sFileName);
                                    oLU.WritetoLog("    Saved as: " + sFileName);

                                    ProcessStartInfo psi = new ProcessStartInfo();
                                    psi.FileName = cfg_bankersimportapp;
                                    psi.Arguments = sFileName;
                                    oLU.WritetoLog("    Importing: " + sFileName);
                                    var proc1 = Process.Start(psi);
                                    proc1.WaitForExit();
                                    int iExitCode = proc1.ExitCode;
                                    if (iExitCode == 0)
                                    {
                                        oLU.WritetoLog("    Import returned: " + iExitCode.ToString());
                                    }
                                    else
                                    {
                                        // Log error 
                                        throw new Exception("****Bankers import returned: " + iExitCode.ToString());
                                    }

                                }   // Excel file

                            }   // iPos > 0

                        }   // File attachment

                    }   // for each atachment

                    email.Delete(DeleteMode.MoveToDeletedItems, true);

                }   // has attachments
                else
                {
                    // no attachments, forward to customer service
                    EmailAddress[] addresses = new EmailAddress[1];
                    addresses[0] = new EmailAddress("jeff@sibfla.com");
                    oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to customerservice@sibfla.com");

                    email.Forward("Auto forwarded from sibflamail@sibfla.com", addresses);
                    email.Delete(DeleteMode.MoveToDeletedItems, true);
                }

            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);
            }
        }

        private static void processOrchid(EmailMessage email)
        {
            // Orchid

            // Forward to Michael to verify against API orders
            string sBodyPrefix = "Auto forwarded from sibflamail@sibfla.com";
            EmailAddress[] addresses = new EmailAddress[1];
            addresses[0] = new EmailAddress("michael@sibfla.com");
            oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to michael@sibfla.com");

            try
            {
                email.Forward(sBodyPrefix, addresses);
                email.Delete(DeleteMode.MoveToDeletedItems, true);
            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);
            }


            //string sAttName = "";
            //string sAttType = "";
            //int iPos = 0;
            //string sFileName = "";
            //DateTime dNow;

            //try
            //{

            //    // if there are attachments
            //    if (email.HasAttachments)
            //    {
            //        email.Load(new PropertySet(EmailMessageSchema.Attachments));

            //        foreach (Attachment attachment in email.Attachments)
            //        {
            //            // If there are attachments
            //            if (attachment is FileAttachment)
            //            {
            //                FileAttachment fileAttachment = attachment as FileAttachment;

            //                oLU.WritetoLog("    Attachment: " + fileAttachment.Name);

            //                // get file extension of attachment
            //                sAttName = fileAttachment.Name.ToUpper();
            //                iPos = sAttName.LastIndexOf('.');
            //                if (iPos > 0)
            //                {

            //                    // get attachment type
            //                    sAttType = getFileType(sAttName);

            //                    // If Excel
            //                    if (sAttType == "CSV")
            //                    {
            //                        // remove spaces from file name
            //                        sFileName = sAttName.Replace(" ", String.Empty);

            //                        // Build file name in the format YYYYHHMMSS + original file name with spaces removed
            //                        dNow = DateTime.Now;
            //                        sFileName = "ORC" + dNow.Year.ToString() + dNow.Hour.ToString() + dNow.Minute.ToString() + dNow.Second.ToString() + "-" + sFileName;

            //                        // Save attachment to SIBI\SibiData\Tmp
            //                        fileAttachment.Load(cfg_attdir + sFileName);
            //                        oLU.WritetoLog("    Saved as: " + sFileName);

            //                        ProcessStartInfo psi = new ProcessStartInfo();
            //                        psi.FileName = cfg_orchidimportapp;
            //                        psi.Arguments = sFileName;
            //                        oLU.WritetoLog("    Importing: " + sFileName);
            //                        var proc1 = Process.Start(psi);
            //                        proc1.WaitForExit();
            //                        int iExitCode = proc1.ExitCode;
            //                        if (iExitCode == 0)
            //                        {
            //                            oLU.WritetoLog("    Import returned: " + iExitCode.ToString());
            //                        }
            //                        else
            //                        {
            //                            // Log error 
            //                            throw new Exception("****Orchid import returned: " + iExitCode.ToString());
            //                        }


            //                    }   // CSV file

            //                }   // iPos > 0

            //            }   // File attachment

            //        }   // for each atachment

            //        email.Delete(DeleteMode.MoveToDeletedItems, true);

            //    }   // has attachments
            //    else
            //    {
            //        // no attachments, forward to customer service
            //        EmailAddress[] addresses = new EmailAddress[1];
            //        addresses[0] = new EmailAddress("customerservice@sibfla.com");
            //        oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to customerservice@sibfla.com");

            //        email.Forward("Auto forwarded from sibflamail@sibfla.com", addresses);
            //        email.Delete(DeleteMode.MoveToDeletedItems, true);
            //    }
            //}

            //catch (Exception ex)
            //{
            //    oLU.WritetoLog(ex.Message);
            //    sendErrEmail(ex.Message);
            //}
        }

        private static void processOrchidConnect(EmailMessage email)
        {
            // Orchid Connect
            // Save CSV attachments to target folder and process


            string sAttName = "";
            string sAttType = "";
            int iPos = 0;
            string sFileName = "";
            DateTime dNow;

            try
            {

                // if there are attachments
                if (email.HasAttachments)
                {
                    email.Load(new PropertySet(EmailMessageSchema.Attachments));

                    foreach (Attachment attachment in email.Attachments)
                    {
                        // If there are attachments
                        if (attachment is FileAttachment)
                        {
                            FileAttachment fileAttachment = attachment as FileAttachment;

                            oLU.WritetoLog("    Attachment: " + fileAttachment.Name);

                            // get file extension of attachment
                            sAttName = fileAttachment.Name.ToUpper();
                            iPos = sAttName.LastIndexOf('.');
                            if (iPos > 0)
                            {

                                // get attachment type
                                sAttType = getFileType(sAttName);

                                // If Excel
                                if (sAttType == "CSV")
                                {
                                    // remove spaces from file name
                                    sFileName = sAttName.Replace(" ", String.Empty);

                                    // Build file name in the format YYYYHHMMSS + original file name with spaces removed
                                    dNow = DateTime.Now;
                                    sFileName = "ORCCON" + dNow.Year.ToString() + dNow.Hour.ToString() + dNow.Minute.ToString() + dNow.Second.ToString() + "-" + sFileName;

                                    // Save attachment to SIBI\SibiData\Tmp
                                    fileAttachment.Load(cfg_attdir + sFileName);
                                    oLU.WritetoLog("    Saved as: " + sFileName);

                                    //ProcessStartInfo psi = new ProcessStartInfo();
                                    //psi.FileName = cfg_orchidimportapp;
                                    //psi.Arguments = sFileName;
                                    //oLU.WritetoLog("    Importing: " + sFileName);
                                    //var proc1 = Process.Start(psi);
                                    //proc1.WaitForExit();
                                    //int iExitCode = proc1.ExitCode;
                                    //if (iExitCode == 0)
                                    //{
                                    //    oLU.WritetoLog("    Import returned: " + iExitCode.ToString());
                                    //}
                                    //else
                                    //{
                                    //    // Log error 
                                    //    throw new Exception("****Orchid import returned: " + iExitCode.ToString());
                                    //}


                                }   // CSV file

                            }   // iPos > 0

                        }   // File attachment

                    }   // for each atachment

                    email.Delete(DeleteMode.MoveToDeletedItems, true);

                }   // has attachments
                else
                {
                    // no attachments, forward to customer service
                    EmailAddress[] addresses = new EmailAddress[1];
                    addresses[0] = new EmailAddress("customerservice@sibfla.com");
                    oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to customerservice@sibfla.com");

                    email.Forward("Auto forwarded from sibflamail@sibfla.com", addresses);
                    email.Delete(DeleteMode.MoveToDeletedItems, true);
                }

            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);
            }
        }

        private static void processSIU(EmailMessage email)
        {
            // SIU
            // Save TXT attachments to target folder and process


            string sAttName = "";
            string sAttType = "";
            int iPos = 0;
            string sFileName = "";
            DateTime dNow;

            try
            {

                // if there are attachments
                if (email.HasAttachments)
                {
                    email.Load(new PropertySet(EmailMessageSchema.Attachments));

                    foreach (Attachment attachment in email.Attachments)
                    {
                        // If there are attachments
                        if (attachment is FileAttachment)
                        {
                            FileAttachment fileAttachment = attachment as FileAttachment;

                            oLU.WritetoLog("    Attachment: " + fileAttachment.Name);

                            // get file extension of attachment
                            sAttName = fileAttachment.Name.ToUpper();
                            iPos = sAttName.LastIndexOf('.');
                            if (iPos > 0)
                            {

                                // get attachment type
                                sAttType = getFileType(sAttName);

                                // If Excel
                                if (sAttType == "TXT")
                                {
                                    // remove spaces from file name
                                    sFileName = sAttName.Replace(" ", String.Empty);

                                    // Build file name in the format YYYYHHMMSS + original file name with spaces removed
                                    dNow = DateTime.Now;
                                    sFileName = "SIU" + dNow.Year.ToString() + dNow.Hour.ToString() + dNow.Minute.ToString() + dNow.Second.ToString() + "-" + sFileName;

                                    // Save attachment to SIBI\SibiData\Tmp
                                    fileAttachment.Load(cfg_attdir + sFileName);
                                    oLU.WritetoLog("    Saved as: " + sFileName);

                                    ProcessStartInfo psi = new ProcessStartInfo();
                                    psi.FileName = cfg_siuimportapp;
                                    psi.Arguments = sFileName;
                                    oLU.WritetoLog("    Importing: " + sFileName);
                                    var proc1 = Process.Start(psi);
                                    proc1.WaitForExit();
                                    int iExitCode = proc1.ExitCode;
                                    if (iExitCode == 0)
                                    {
                                        oLU.WritetoLog("    Import returned: " + iExitCode.ToString());
                                    }
                                    else
                                    {
                                        // Log error 
                                        throw new Exception("****SIU import returned: " + iExitCode.ToString());
                                    }

                                }   // file

                            }   // iPos > 0

                        }   // File attachment

                    }   // for each atachment

                    email.Delete(DeleteMode.MoveToDeletedItems, true);

                }   // has attachments
                else
                {
                    // no attachments, forward to customer service
                    EmailAddress[] addresses = new EmailAddress[1];
                    addresses[0] = new EmailAddress("customerservice@sibfla.com");
                    oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to customerservice@sibfla.com");

                    email.Forward("Auto forwarded from sibflamail@sibfla.com", addresses);
                    email.Delete(DeleteMode.MoveToDeletedItems, true);
                }

            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);
            }
        }

        private static void processMacNeill(EmailMessage email)
        {
            // MacNeill
            // Save XLS attachments to target folder and process


            string sAttName = "";
            string sAttType = "";
            int iPos = 0;
            string sFileName = "";
            DateTime dNow;

            try
            {

                // if there are attachments
                if (email.HasAttachments)
                {
                    email.Load(new PropertySet(EmailMessageSchema.Attachments));

                    foreach (Attachment attachment in email.Attachments)
                    {
                        // If there are attachments
                        if (attachment is FileAttachment)
                        {
                            FileAttachment fileAttachment = attachment as FileAttachment;

                            oLU.WritetoLog("    Attachment: " + fileAttachment.Name);

                            // get file extension of attachment
                            sAttName = fileAttachment.Name.ToUpper();
                            iPos = sAttName.LastIndexOf('.');
                            if (iPos > 0)
                            {

                                // get attachment type
                                sAttType = getFileType(sAttName);

                                // If Excel
                                if (sAttType == "XLS" || sAttType == "XLSX")
                                {
                                    // remove spaces from file name
                                    sFileName = sAttName.Replace(" ", String.Empty);

                                    // Build file name in the format YYYYHHMMSS + original file name with spaces removed
                                    dNow = DateTime.Now;
                                    sFileName = "MAC" + dNow.Year.ToString() + dNow.Hour.ToString() + dNow.Minute.ToString() + dNow.Second.ToString() + "-" + sFileName;

                                    // Save attachment to c:\automationdata\temp\
                                    fileAttachment.Load(cfg_attdir + sFileName);
                                    oLU.WritetoLog("    Saved as: " + sFileName);

                                    ProcessStartInfo psi = new ProcessStartInfo();
                                    psi.FileName = cfg_macneillimportapp;
                                    psi.Arguments = sFileName;
                                    oLU.WritetoLog("    Importing: " + sFileName);
                                    var proc1 = Process.Start(psi);
                                    proc1.WaitForExit();
                                    int iExitCode = proc1.ExitCode;
                                    if (iExitCode == 0)
                                    {
                                        oLU.WritetoLog("    Import returned: " + iExitCode.ToString());
                                    }
                                    else
                                    {
                                        // Log error 
                                        throw new Exception("****MacNeill import returned: " + iExitCode.ToString());
                                    }

                                }   // Excel file

                            }   // iPos > 0

                        }   // File attachment

                    }   // for each atachment

                    email.Delete(DeleteMode.MoveToDeletedItems, true);

                }   // has attachments
                else
                {
                    // no attachments, forward to customer service
                    EmailAddress[] addresses = new EmailAddress[1];
                    addresses[0] = new EmailAddress("customerservice@sibfla.com");
                    oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to customerservice@sibfla.com");

                    email.Forward("Auto forwarded from sibflamail@sibfla.com", addresses);
                    email.Delete(DeleteMode.MoveToDeletedItems, true);
                }

            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);
            }
        }

        private static void processBass(EmailMessage email)
        {
            // Bass
            // Save XML attachments to target folder and process


            string sAttName = "";
            string sAttType = "";
            int iPos = 0;
            string sFileName = "";
            DateTime dNow;

            try
            {

                // if there are attachments
                if (email.HasAttachments)
                {
                    email.Load(new PropertySet(EmailMessageSchema.Attachments));

                    foreach (Attachment attachment in email.Attachments)
                    {
                        // If there are attachments
                        if (attachment is FileAttachment)
                        {
                            FileAttachment fileAttachment = attachment as FileAttachment;

                            oLU.WritetoLog("    Attachment: " + fileAttachment.Name);

                            // get file extension of attachment
                            sAttName = fileAttachment.Name.ToUpper();
                            iPos = sAttName.LastIndexOf('.');
                            if (iPos > 0)
                            {

                                // get attachment type
                                sAttType = getFileType(sAttName);

                                // If Excel
                                if (sAttType == "XML")
                                {
                                    // remove spaces from file name
                                    sFileName = sAttName.Replace(" ", String.Empty);

                                    // Build file name in the format YYYYHHMMSS + original file name with spaces removed
                                    dNow = DateTime.Now;
                                    sFileName = "BAS" + dNow.Year.ToString() + dNow.Hour.ToString() + dNow.Minute.ToString() + dNow.Second.ToString() + "-" + sFileName;

                                    // Save attachment to SIBI\SibiData\Tmp
                                    fileAttachment.Load(cfg_attdir + sFileName);
                                    oLU.WritetoLog("    Saved as: " + sFileName);

                                    ProcessStartInfo psi = new ProcessStartInfo();
                                    psi.FileName = cfg_bassimportapp;
                                    psi.Arguments = sFileName;
                                    oLU.WritetoLog("    Importing: " + sFileName);
                                    var proc1 = Process.Start(psi);
                                    proc1.WaitForExit();
                                    int iExitCode = proc1.ExitCode;
                                    if (iExitCode == 0)
                                    {
                                        oLU.WritetoLog("    Import returned: " + iExitCode.ToString());
                                    }
                                    else
                                    {
                                        // Log error 
                                        throw new Exception("****Bass import returned: " + iExitCode.ToString());
                                    }


                                }   // Excel file

                            }   // iPos > 0

                        }   // File attachment

                    }   // for each atachment

                    email.Delete(DeleteMode.MoveToDeletedItems, true);

                }   // has attachments
                else
                {
                    // no attachments, forward to customer service
                    EmailAddress[] addresses = new EmailAddress[1];
                    addresses[0] = new EmailAddress("customerservice@sibfla.com");
                    oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to customerservice@sibfla.com");

                    email.Forward("Auto forwarded from sibflamail@sibfla.com", addresses);
                    email.Delete(DeleteMode.MoveToDeletedItems, true);
                }

            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);
            }
        }

        private static void processAscendant(EmailMessage email)
        {
            // Ascendant
            // Forward to Customer Service shared folder - then forward to Lealyn and save copy
            string sBodyPrefix = "Auto forwarded from sibflamail@sibfla.com";
            EmailAddress[] addresses = new EmailAddress[1];
            //addresses[0] = new EmailAddress("manualordering@sibfla.com"); 3/26/21
            addresses[0] = new EmailAddress("customerservice@sibfla.com");            
            oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to customerservice@sibfla.com");

            try
            {
                email.Forward(sBodyPrefix, addresses);
                email.Delete(DeleteMode.MoveToDeletedItems, true);
            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);
            }
        }

        private static void processRTSpecialtyOLD(EmailMessage email)
        {
            string sBodyPrefix = "Auto forwarded from sibflamail@sibfla.com";
            EmailAddress[] addresses = new EmailAddress[1];
            addresses[0] = new EmailAddress("jeff@sibfla.com");
            oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to jeff@sibfla.com");

            try
            {
                email.Forward(sBodyPrefix, addresses);
                email.Delete(DeleteMode.MoveToDeletedItems, true);
            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);
            }
        }

        private static string getFileType(string sFileName)
        {
            string sRetVal = "";
            int iPos = 0;

            iPos = sFileName.LastIndexOf('.');
            if (iPos > 0)
            {
                // Build file name in the format YYYYHHMMSS
                sRetVal = sFileName.Substring(iPos + 1);

            }

            return sRetVal;
        }

        private static void processRTSpecialty(EmailMessage email)
        {
            // RTSpecialty / Titan
            // Save XLS attachments to target folder and process


            string sAttName = "";
            string sAttType = "";
            int iPos = 0;
            string sFileName = "";
            DateTime dNow;

            try
            {

                // if there are attachments
                if (email.HasAttachments)
                {
                    email.Load(new PropertySet(EmailMessageSchema.Attachments));

                    foreach (Attachment attachment in email.Attachments)
                    {
                        // If there are attachments
                        if (attachment is FileAttachment)
                        {
                            FileAttachment fileAttachment = attachment as FileAttachment;

                            oLU.WritetoLog("    Attachment: " + fileAttachment.Name);

                            // get file extension of attachment
                            sAttName = fileAttachment.Name.ToUpper();
                            iPos = sAttName.LastIndexOf('.');
                            if (iPos > 0)
                            {

                                // get attachment type
                                sAttType = getFileType(sAttName);

                                // If Excel
                                if (sAttType == "XLS" || sAttType == "XLSX")
                                {
                                    // remove spaces from file name
                                    sFileName = sAttName.Replace(" ", String.Empty);

                                    // Build file name in the format YYYYHHMMSS + original file name with spaces removed
                                    dNow = DateTime.Now;
                                    sFileName = "RTSpec" + dNow.Year.ToString() + dNow.Hour.ToString() + dNow.Minute.ToString() + dNow.Second.ToString() + "-" + sFileName;

                                    // Save attachment to c:\automationdata\temp\
                                    fileAttachment.Load(cfg_attdir + sFileName);
                                    oLU.WritetoLog("    Saved as: " + sFileName);

                                    ProcessStartInfo psi = new ProcessStartInfo();
                                    psi.FileName = cfg_rtspecimportapp; ;
                                    psi.Arguments = sFileName;
                                    oLU.WritetoLog("    Importing: " + sFileName);
                                    var proc1 = Process.Start(psi);
                                    proc1.WaitForExit();
                                    int iExitCode = proc1.ExitCode;
                                    if (iExitCode == 0)
                                    {
                                        oLU.WritetoLog("    Import returned: " + iExitCode.ToString());
                                    }
                                    else
                                    {
                                        // Log error 
                                        throw new Exception("****RTSpecialty import returned: " + iExitCode.ToString());
                                    }

                                }   // Excel file

                            }   // iPos > 0

                        }   // File attachment

                    }   // for each atachment

                    email.Delete(DeleteMode.MoveToDeletedItems, true);

                }   // has attachments
                else
                {
                    // no attachments, forward to customer service
                    EmailAddress[] addresses = new EmailAddress[1];
                    addresses[0] = new EmailAddress("customerservice@sibfla.com");
                    oLU.WritetoLog("Forwarding : " + email.Sender.Address + " / " + email.Subject + " to customerservice@sibfla.com");

                    email.Forward("Auto forwarded from sibflamail@sibfla.com", addresses);
                    email.Delete(DeleteMode.MoveToDeletedItems, true);
                }

            }

            catch (Exception ex)
            {
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);
            }
        }

        private static void sendEmail()
        {



            //EmailMessage email = new EmailMessage(serviceInstance);


            //email.ToRecipients.Add("jeff@jkensinger.com");

            //email.Subject = "HelloWorld";
            //email.Body = new MessageBody("This is the first email I've sent by using the EWS Managed API");

            //email.Send();

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Email Order Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

    }


}
