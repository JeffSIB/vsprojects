﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;


namespace EmailOrderProcessing
{


    public class ExchangeRepository
    {

        ExchangeService serviceInstance;
        public string ExceptionMessage { get; }
        public ExchangeRepository()
        {
            serviceInstance = new ExchangeService(ExchangeVersion.Exchange2013_SP1);

            //Provide the account user name in format vibhu.kuchhal@contoso.com
            serviceInstance.Credentials = new WebCredentials(ConfigurationManager.AppSettings["User"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());

            try
            {
                // Use Autodiscover to set the URL endpoint.
                // and using a AutodiscoverRedirectionUrlValidationCallback in case of https enabled clod account
                serviceInstance.AutodiscoverUrl(ConfigurationManager.AppSettings["MailBox"].ToString(), SslRedirectionCallback);
            }
            catch (Exception ex)
            {
                serviceInstance = null;
                ExceptionMessage = ex.Message;
            }

        }

        bool SslRedirectionCallback(string serviceUrl)
        {
            // Return true if the URL is an HTTPS URL.
            return serviceUrl.ToLower().StartsWith("https:");
        }
    }
}

