﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Globalization;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;

namespace DailyOrders
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static int cfg_TimeZoneOffset;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Range oRange = null;
        static DateTime dBegDate;
        static DateTime dEndDate;
        static DateTime dBegDate360;
        static DateTime dEndDate360;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;
        static string sMode;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                //cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));
                cfg_TimeZoneOffset = getTimeZoneOffset();

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //  D = daily
                if (args.Length < 1)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                if (args[0].ToUpper() == "D")
                {

                    ///////////////////////////////////////////
                    // **Run for previous day and email to Andrea**
                    sMode = "D";
                    dBegDate = DateTime.Today;
                    dBegDate = dBegDate.AddDays(-1);
                    dEndDate = dBegDate;
                }
                else if (args[0].ToUpper() == "C")
                {

                    ///////////////////////////////////////////
                    // Custom dates
                    sMode = "C";
                    dBegDate = Convert.ToDateTime("4/4/2020");
                    dEndDate = Convert.ToDateTime("4/6/2020");
                }

                else
                {
                    throw new SystemException("Invalid period arguments supplied");
                }

                string sDateDisp = dBegDate.Month + "-" + dBegDate.Day;
                string sEmailSubject = "Residential Orders for - " + dBegDate.Month + "/" + dBegDate.Day + ", " + dBegDate.Year.ToString();
                string sExcelFileNameNoEx = cfg_outputdir + "ResOrders_" + sDateDisp;
                string sExcelFileName = sExcelFileNameNoEx + ".xlsx";

                // set time to 12:00am
                // adjust for 360 GMT
                dBegDate = ChangeTime(dBegDate, 0, 0, 0, 0);
                dBegDate360 = dBegDate.AddHours(cfg_TimeZoneOffset);

                // set time to midnight
                dEndDate = ChangeTime(dEndDate, 23, 59, 59, 0);
                dEndDate360 = dEndDate.AddHours(cfg_TimeZoneOffset);

                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                int iCaseNum = 0;
                string sCustomer = "";
                string sInspType = "";
                string sInsCo = "";
                int iRow = 0;

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandTimeout = 360;
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetResOrdersByDate";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate360);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate360);
                sqlConn1.Open();

                // initiate Excel
                oExcel = new Excel.Application();
                oExcel.Visible = false;
                oWorkbook = oExcel.Workbooks.Add(1);
                o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                o360Worksheet.Name = "Residential Orders";
                createHeader360(o360Worksheet);
                iRow = 4;


                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {


                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        // case#
                        if (sqlReader.IsDBNull(0))
                        {
                            iCaseNum = 0;
                        }
                        else
                        {
                            iCaseNum = (int)sqlReader.GetSqlInt32(0);
                        }

                        //Customer
                        if (sqlReader.IsDBNull(1))
                        {
                            sCustomer = "";
                        }
                        else
                        {
                            sCustomer = (string)sqlReader.GetSqlString(1);
                        }

                        //Insp type
                        if (sqlReader.IsDBNull(2))
                        {
                            sInspType= "";
                        }
                        else
                        {
                            sInspType = (string)sqlReader.GetSqlString(2);
                        }
                        
                        //Ins co
                        if (sqlReader.IsDBNull(3))
                        {
                            sInsCo = "";
                        }
                        else
                        {
                            sInsCo = (string)sqlReader.GetSqlString(3);
                        }

                        addData(o360Worksheet, iRow, 1, iCaseNum.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 2, sCustomer.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                        addData(o360Worksheet, iRow, 3, sInspType.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "", "");
                        addData(o360Worksheet, iRow, 4, sInsCo.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "", "");

                        iRow++;

                    } while (sqlReader.Read());     // 360

                }   // has rows
                else
                {
                    addDataNoFormat(o360Worksheet, iRow, 1, "No items found", "A" + iRow.ToString(), "A" + iRow.ToString());
                }
                
                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;
                 

                //oWorkbook.SaveAs(cfg_outputdir + "ReviewerStats360_" + sDateDisp, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.Close(true, oMissing, oMissing);
                oExcel.Quit();
                msMsgBody += "Processing completed for 360" + System.Environment.NewLine + cfg_outputdir + "DailyOrders_" + sDateDisp + System.Environment.NewLine + System.Environment.NewLine;
                
                releaseObject(oExcel);
                releaseObject(oWorkbook);
                releaseObject(o360Worksheet);
                sendExcelFile(sEmailSubject, sExcelFileName);
                msMsgBody += "File sent " + sExcelFileName;
            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(msMsgBody);                
                }
            
            }
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeader360(Excel._Worksheet oWorkSheet)
        {

            Excel.Range oRange;


            oWorkSheet.get_Range("A1", "D1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "D1");
            oRange.FormulaR1C1 = "Residential Orders " + dBegDate.ToShortDateString();

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Case #";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Customer";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 55;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "Inspection Type";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 75;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Insurance Company";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 40;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        }      
   

        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data, 
			string cell1, string cell2,string format,string sHorizAlign)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }



        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "Residential Orders";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Residential Orders Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                if (sMode == "D")
                {
                    oMail.MailTo = "Andrea@sibfla.com;Michael@sibfla.com";
                    //oMail.MailTo = "jeff@sibfla.com";

                }
                if (sMode == "C")
                {
                    oMail.MailTo = "jeff@sibfla.com";
                }
                
                oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

        static int getTimeZoneOffset()
        {
            SqlConnection sqlConn = null;
            SqlCommand sqlCmd = null;
            int iRetVal = 0;

            try
            {
                //string sQuery = @"SELECT dbo.TimeZoneOffset AS TZOffset;";
                sqlConn = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd = new SqlCommand("SELECT dbo.TimeZoneOffset()", sqlConn);
                sqlConn.Open();
                Int32 functionResult = (Int32)sqlCmd.ExecuteScalar();

                iRetVal = functionResult;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }
            finally
            {

                // close objects
                if (sqlConn != null)
                    sqlConn.Close();
            }

            return iRetVal;
        }
    }
}
