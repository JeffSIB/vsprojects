﻿using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Globalization;
using System.Threading;

namespace OpenInspectionsBuild
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360ConnStr;
        static string cfg_360UtilConnStr;
        static string cfg_SQLMainSIBIConnStr;
        static string cfg_SQLMainUTILConnStr;

        static void Main(string[] args)
        {
            
            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");
            cfg_360ConnStr = colNameVal.Get("360ConnStr");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");
            cfg_SQLMainUTILConnStr = colNameVal.Get("SQLMainUTILConnStr");

            // 360 - Current period - clear table first
            Process360(true,true);

            // SIB - Current period  
            ProcessSIB(true,false);

            // Combine
            Finish();

        }


        static void Process360(bool bCurPd, bool bClearTable)
        {

            // Populate table ZipCodeDistrict2Month
            // Current vs Previous month determine by bCurPD
            // CurPD = true gets data 1 month prior to run date 
            // CurPD = false gets data 2 months prior to run date             

            DateTime dDate = DateTime.Today;
            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin 360 ++++");

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd2 = new SqlCommand();
                sqlConn1.Open();

                // clear table first time through
                if (bClearTable)
                {
                    sqlCmd1.CommandType = CommandType.Text;
                    sqlCmd1.CommandText = "DELETE FROM OpenInspetions";
                    sqlCmd1.Connection = sqlConn1;
                    sqlCmd1.ExecuteNonQuery();                                
                }
                
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "dbo.sp_BuildOpenInspections";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();

                // Get all completed cases
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through districts
                    do
                    {

                        //[1CM], [2CM], [3CM], [1RES], [2RES], [3RES], [NP], [TEL], [WC]

                        //SubDistrict code
                        if (sqlReader.IsDBNull(0))
                        {
                            sSubDistrict = "*NA*";
                        }
                        else
                        {
                            sSubDistrict = sqlReader.GetSqlString(0).ToString();
                        }

                        sDistrict = sSubDistrict.Substring(0, 1);

                        // num ordered by skill set
                        i1CM = (int)sqlReader.GetSqlInt32(1);
                        i2CM = (int)sqlReader.GetSqlInt32(2);
                        i3CM = (int)sqlReader.GetSqlInt32(3);
                        i1RES = (int)sqlReader.GetSqlInt32(4);
                        i2RES = (int)sqlReader.GetSqlInt32(5);
                        i3RES = (int)sqlReader.GetSqlInt32(6);
                        iNP = (int)sqlReader.GetSqlInt32(7);
                        iTEL = (int)sqlReader.GetSqlInt32(8);
                        iWC = (int)sqlReader.GetSqlInt32(9);
                        
                        // Insert into table
                        sqlCmd2.Parameters.Clear();

                        if (bCurPd)
                        {
                            sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                            sqlCmd2.Parameters.AddWithValue("@subdistrict", sSubDistrict);
                            sqlCmd2.Parameters.AddWithValue("@1CMCur", i1CM);
                            sqlCmd2.Parameters.AddWithValue("@1CMPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@2CMCur", i2CM);
                            sqlCmd2.Parameters.AddWithValue("@2CMPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@3CMCur", i3CM);
                            sqlCmd2.Parameters.AddWithValue("@3CMPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@1RESCur", i1RES);
                            sqlCmd2.Parameters.AddWithValue("@1RESPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@2RESCur", i2RES);
                            sqlCmd2.Parameters.AddWithValue("@2RESPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@3RESCur", i3RES);
                            sqlCmd2.Parameters.AddWithValue("@3RESPrev",0);
                            sqlCmd2.Parameters.AddWithValue("@NPCur", iNP);
                            sqlCmd2.Parameters.AddWithValue("@NPPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@TELCur", iTEL);
                            sqlCmd2.Parameters.AddWithValue("@TELPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@WCCur", iWC);
                            sqlCmd2.Parameters.AddWithValue("@WCPrev", 0);
                        }
                        else
                        {
                            sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                            sqlCmd2.Parameters.AddWithValue("@subdistrict", sSubDistrict);
                            sqlCmd2.Parameters.AddWithValue("@1CMCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@1CMPrev", i1CM);
                            sqlCmd2.Parameters.AddWithValue("@2CMCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@2CMPrev", i2CM);
                            sqlCmd2.Parameters.AddWithValue("@3CMCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@3CMPrev", i3CM);
                            sqlCmd2.Parameters.AddWithValue("@1RESCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@1RESPrev", i1RES);
                            sqlCmd2.Parameters.AddWithValue("@2RESCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@2RESPrev", i2RES);
                            sqlCmd2.Parameters.AddWithValue("@3RESCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@3RESPrev", i3RES);
                            sqlCmd2.Parameters.AddWithValue("@NPCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@NPPrev", iNP);
                            sqlCmd2.Parameters.AddWithValue("@TELCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@TELPrev", iTEL);
                            sqlCmd2.Parameters.AddWithValue("@WCCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@WCPrev", iWC);
                        }

                        iRet = sqlCmd2.ExecuteNonQuery();

                        if (iRet == 0)
                            throw new SystemException("sp_ZipCodeDist2Month_Update returned 0 for " + sDistrict);

                    } while (sqlReader.Read());     // districts

                    sqlReader.Close();

                }   // has rows

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End TwoMonth ----");
                oLU.closeLog();

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn1.Close();


            }


        }

        static void ProcessSIB(bool bCurPd, bool bClearTable)
        {

            // Populate table ZipCodeDistrict2Month
            // Current vs Previous month determine by bCurPD
            // CurPD = true gets data 1 month prior to run date 
            // CurPD = false gets data 2 months prior to run date             

            DateTime dDate = DateTime.Today;

            if (bCurPd)
            {
                dDate = dDate.AddMonths(-1);
            }
            else
            {
                dDate = dDate.AddMonths(-2);
            }

            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dBegOfMonth = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth = LastDayOfMonth(dDate);

            // set time to 12:00am
            dBegOfMonth = ChangeTime(dBegOfMonth, 0, 0, 0, 0);

            // set time to midnight
            dEndOfMonth = ChangeTime(dEndOfMonth, 23, 59, 59, 0);

            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iRet = 0;
            string sDistrict = "";
            string sSubDistrict = "";
            int i1CM = 0;
            int i2CM = 0;
            int i3CM = 0;
            int i1RES = 0;
            int i2RES = 0;
            int i3RES = 0;
            int iTEL = 0;
            int iNP = 0;
            int iWC = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin TwoMonthSIB ++++");

                // set up SQL connection (SIBUtil)
                sqlConn1 = new SqlConnection(cfg_SQLMainUTILConnStr);
                sqlCmd1 = new SqlCommand();
                sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd2 = new SqlCommand();
                sqlConn1.Open();

                // clear table first time through
                if (bClearTable)
                {
                    oLU.WritetoLog("Clearing table");
                    sqlCmd1.CommandType = CommandType.Text;
                    sqlCmd1.CommandText = "DELETE FROM ZipCodeDistrict2Month";
                    sqlCmd1.Connection = sqlConn1;
                    sqlCmd1.ExecuteNonQuery();
                }

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "dbo.sp_ZipCodeDistrict_Pivot";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);

                // Second SQL conn for updating table
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "dbo.sp_ZipCodeDist2Month_Update";
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                // Get all completed cases
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through districts
                    do
                    {

                        //[1CM], [2CM], [3CM], [1RES], [2RES], [3RES], [NP], [TEL], [WC]

                        //District code
                        if (sqlReader.IsDBNull(0))
                        {
                            sSubDistrict = "*NA*";
                        }
                        else
                        {
                            sSubDistrict = sqlReader.GetSqlString(0).ToString();
                        }

                        sDistrict = sSubDistrict.Substring(0, 1);

                        // num ordered by skill set
                        i1CM = (int)sqlReader.GetSqlInt32(1);
                        i2CM = (int)sqlReader.GetSqlInt32(2);
                        i3CM = (int)sqlReader.GetSqlInt32(3);
                        i1RES = (int)sqlReader.GetSqlInt32(4);
                        i2RES = (int)sqlReader.GetSqlInt32(5);
                        i3RES = (int)sqlReader.GetSqlInt32(6);
                        iNP = (int)sqlReader.GetSqlInt32(7);
                        iTEL = (int)sqlReader.GetSqlInt32(8);
                        iWC = (int)sqlReader.GetSqlInt32(9);

                        // Insert into table
                        sqlCmd2.Parameters.Clear();

                        if (bCurPd)
                        {
                            sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                            sqlCmd2.Parameters.AddWithValue("@subdistrict", sSubDistrict);
                            sqlCmd2.Parameters.AddWithValue("@1CMCur", i1CM);
                            sqlCmd2.Parameters.AddWithValue("@1CMPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@2CMCur", i2CM);
                            sqlCmd2.Parameters.AddWithValue("@2CMPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@3CMCur", i3CM);
                            sqlCmd2.Parameters.AddWithValue("@3CMPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@1RESCur", i1RES);
                            sqlCmd2.Parameters.AddWithValue("@1RESPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@2RESCur", i2RES);
                            sqlCmd2.Parameters.AddWithValue("@2RESPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@3RESCur", i3RES);
                            sqlCmd2.Parameters.AddWithValue("@3RESPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@NPCur", iNP);
                            sqlCmd2.Parameters.AddWithValue("@NPPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@TELCur", iTEL);
                            sqlCmd2.Parameters.AddWithValue("@TELPrev", 0);
                            sqlCmd2.Parameters.AddWithValue("@WCCur", iWC);
                            sqlCmd2.Parameters.AddWithValue("@WCPrev", 0);
                        }
                        else
                        {
                            sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                            sqlCmd2.Parameters.AddWithValue("@subdistrict", sSubDistrict);
                            sqlCmd2.Parameters.AddWithValue("@1CMCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@1CMPrev", i1CM);
                            sqlCmd2.Parameters.AddWithValue("@2CMCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@2CMPrev", i2CM);
                            sqlCmd2.Parameters.AddWithValue("@3CMCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@3CMPrev", i3CM);
                            sqlCmd2.Parameters.AddWithValue("@1RESCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@1RESPrev", i1RES);
                            sqlCmd2.Parameters.AddWithValue("@2RESCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@2RESPrev", i2RES);
                            sqlCmd2.Parameters.AddWithValue("@3RESCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@3RESPrev", i3RES);
                            sqlCmd2.Parameters.AddWithValue("@NPCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@NPPrev", iNP);
                            sqlCmd2.Parameters.AddWithValue("@TELCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@TELPrev", iTEL);
                            sqlCmd2.Parameters.AddWithValue("@WCCur", 0);
                            sqlCmd2.Parameters.AddWithValue("@WCPrev", iWC);
                        }

                        iRet = sqlCmd2.ExecuteNonQuery();

                        if (iRet == 0)
                            throw new SystemException("sp_ZipCodeDist2Month_Update returned 0 for " + sDistrict);

                    } while (sqlReader.Read());     // districts

                    sqlReader.Close();

                }   // has rows

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End TwoMonth ----");
                oLU.closeLog();

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn1.Close();


            }


        }

        static void Finish()
        {

            // Populate table ZipCodeDistrict2Month
            // Current vs Previous month determine by bCurPD
            // CurPD = true gets data 1 month prior to run date 
            // CurPD = false gets data 2 months prior to run date             

             int iRet = 0;
            string sDistrict = "";
            string sSubDistrict = "";
            int i1CM = 0;
            int i2CM = 0;
            int i3CM = 0;
            int i1RES = 0;
            int i2RES = 0;
            int i3RES = 0;
            int iTEL = 0;
            int iNP = 0;
            int iWC = 0;
            int iTotal = 0;
            int iTotalP = 0;
            int i1CMP = 0;
            int i2CMP = 0;
            int i3CMP = 0;
            int i1RESP = 0;
            int i2RESP = 0;
            int i3RESP = 0;
            int iTELP = 0;
            int iNPP = 0;
            int iWCP = 0;

            decimal dTotPct = 0;
            decimal d1CMPct = 0;
            decimal d2CMPct = 0;
            decimal d3CMPct = 0;
            decimal d1RESPct = 0;
            decimal d2RESPct = 0;
            decimal d3RESPct = 0;
            decimal dTELPct = 0;
            decimal dNPPct = 0;
            decimal dWCPct = 0;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin Finalize ++++");

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd2 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT * FROM ZipCodeDistrict2Month";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // Second SQL conn for updating table
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "dbo.sp_ZipCodeDist2Month_Finalize";
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                // process all rows
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        sSubDistrict = sqlReader.GetSqlString(1).ToString();

                        sDistrict = sSubDistrict.Substring(0, 1);

                        // get current & prev month numbers
                        i1CM = (int)sqlReader.GetSqlInt32(5);
                        i1CMP = (int)sqlReader.GetSqlInt32(6);
                        i2CM = (int)sqlReader.GetSqlInt32(8);
                        i2CMP = (int)sqlReader.GetSqlInt32(9);
                        i3CM = (int)sqlReader.GetSqlInt32(11);
                        i3CMP = (int)sqlReader.GetSqlInt32(12);
                        i1RES = (int)sqlReader.GetSqlInt32(14);
                        i1RESP = (int)sqlReader.GetSqlInt32(15);
                        i2RES = (int)sqlReader.GetSqlInt32(17);
                        i2RESP = (int)sqlReader.GetSqlInt32(18);
                        i3RES = (int)sqlReader.GetSqlInt32(20);
                        i3RESP = (int)sqlReader.GetSqlInt32(21);
                        iNP = (int)sqlReader.GetSqlInt32(23);
                        iNPP = (int)sqlReader.GetSqlInt32(24);
                        iTEL = (int)sqlReader.GetSqlInt32(26);
                        iTELP = (int)sqlReader.GetSqlInt32(27);
                        iWC = (int)sqlReader.GetSqlInt32(29);
                        iWCP = (int)sqlReader.GetSqlInt32(30);
                        
                        
                        // Update table
                        sqlCmd2.Parameters.Clear();

                        iTotal = i1CM + i2CM + i3CM + i1RES + i2RES + i3RES + iNP + iTEL + iWC;
                        iTotalP = i1CMP + i2CMP + i3CMP + i1RESP + i2RESP + i3RESP + iNPP + iTELP + iWCP;

                        dTotPct = calcPct(iTotal, iTotalP);
                        d1CMPct = calcPct(i1CM, i1CMP);
                        d2CMPct = calcPct(i2CM, i2CMP);
                        d3CMPct = calcPct(i3CM, i3CMP);
                        d1RESPct = calcPct(i1RES, i1RESP);
                        d2RESPct = calcPct(i2RES, i2RESP);
                        d3RESPct = calcPct(i3RES, i3RESP);
                        dNPPct = calcPct(iNP, iNPP);
                        dTELPct = calcPct(iTEL, iTELP);
                        dWCPct = calcPct(iWC, iWCP);

                        sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                        sqlCmd2.Parameters.AddWithValue("@SubDistrict", sSubDistrict);
                        sqlCmd2.Parameters.AddWithValue("@TotPctChange", dTotPct);
                        sqlCmd2.Parameters.AddWithValue("@1CMPctChange", d1CMPct);
                        sqlCmd2.Parameters.AddWithValue("@2CMPctChange", d2CMPct);
                        sqlCmd2.Parameters.AddWithValue("@3CMPctChange", d3CMPct);
                        sqlCmd2.Parameters.AddWithValue("@1RESPctChange", d1RESPct);
                        sqlCmd2.Parameters.AddWithValue("@2RESPctChange", d2RESPct);
                        sqlCmd2.Parameters.AddWithValue("@3RESPctChange", d3RESPct);
                        sqlCmd2.Parameters.AddWithValue("@NPPctChange", dNPPct);
                        sqlCmd2.Parameters.AddWithValue("@TELPctChange", dTELPct);
                        sqlCmd2.Parameters.AddWithValue("@WCPctChange", dWCPct);

                        //sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                        //sqlCmd2.Parameters.AddWithValue("@1CMPctChange", 0);
                        //sqlCmd2.Parameters.AddWithValue("@2CMPctChange", 0);
                        //sqlCmd2.Parameters.AddWithValue("@3CMPctChange", 0);
                        //sqlCmd2.Parameters.AddWithValue("@1RESPctChange", .1);
                        //sqlCmd2.Parameters.AddWithValue("@2RESPctChange", .2);
                        //sqlCmd2.Parameters.AddWithValue("@3RESPctChange", .3);
                        //sqlCmd2.Parameters.AddWithValue("@NPPctChange", 0);
                        //sqlCmd2.Parameters.AddWithValue("@TELPctChange", 0);
                        //sqlCmd2.Parameters.AddWithValue("@WCPctChange", 0);

                        iRet = sqlCmd2.ExecuteNonQuery();

                        //if (iRet != 1)
                        //    throw new SystemException("sp_sp_ZipCodeDist2Month_Finalize returned " + iRet.ToString() + " for " + sDistrict);

                    } while (sqlReader.Read());     // districts

                    sqlReader.Close();

                }   // has rows


                // Totals

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "dbo.sp_ZipCodeDist2Month_Totals";
                sqlCmd1.Connection = sqlConn1;
                //sqlConn1.Open();

                // Second SQL conn for updating table
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "dbo.sp_ZipCodeDist2Month_TotRow";
                sqlCmd2.Connection = sqlConn2;
                //sqlConn2.Open();

                // process all rows
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {
                           
                        sDistrict = sqlReader.GetSqlString(0).ToString();
                        sSubDistrict = sDistrict + "Total";

                        // get current & prev month totals
                        iTotal = (int)sqlReader.GetSqlInt32(1);
                        iTotalP = (int)sqlReader.GetSqlInt32(2);

                        i1CM = (int)sqlReader.GetSqlInt32(3);
                        i1CMP = (int)sqlReader.GetSqlInt32(4);
                        i2CM = (int)sqlReader.GetSqlInt32(5);
                        i2CMP = (int)sqlReader.GetSqlInt32(6);
                        i3CM = (int)sqlReader.GetSqlInt32(7);
                        i3CMP = (int)sqlReader.GetSqlInt32(8);
                        i1RES = (int)sqlReader.GetSqlInt32(9);
                        i1RESP = (int)sqlReader.GetSqlInt32(10);
                        i2RES = (int)sqlReader.GetSqlInt32(11);
                        i2RESP = (int)sqlReader.GetSqlInt32(12);
                        i3RES = (int)sqlReader.GetSqlInt32(13);
                        i3RESP = (int)sqlReader.GetSqlInt32(14);
                        iNP = (int)sqlReader.GetSqlInt32(15);
                        iNPP = (int)sqlReader.GetSqlInt32(16);
                        iTEL = (int)sqlReader.GetSqlInt32(17);
                        iTELP = (int)sqlReader.GetSqlInt32(18);
                        iWC = (int)sqlReader.GetSqlInt32(19);
                        iWCP = (int)sqlReader.GetSqlInt32(20);


                        // Update table - write total row
                        sqlCmd2.Parameters.Clear();

                        dTotPct = calcPct(iTotal, iTotalP);
                        d1CMPct = calcPct(i1CM, i1CMP);
                        d2CMPct = calcPct(i2CM, i2CMP);
                        d3CMPct = calcPct(i3CM, i3CMP);
                        d1RESPct = calcPct(i1RES, i1RESP);
                        d2RESPct = calcPct(i2RES, i2RESP);
                        d3RESPct = calcPct(i3RES, i3RESP);
                        dNPPct = calcPct(iNP, iNPP);
                        dTELPct = calcPct(iTEL, iTELP);
                        dWCPct = calcPct(iWC, iWCP);

                        sqlCmd2.Parameters.AddWithValue("@district", sDistrict);
                        sqlCmd2.Parameters.AddWithValue("@SubDistrict", sSubDistrict);
                        sqlCmd2.Parameters.AddWithValue("@Total",iTotal );
                        sqlCmd2.Parameters.AddWithValue("@TotalPrev",iTotalP );
                        sqlCmd2.Parameters.AddWithValue("@TotalPctChange", dTotPct);
                        sqlCmd2.Parameters.AddWithValue("@1CMCur",i1CM);
                        sqlCmd2.Parameters.AddWithValue("@1CMPrev",i1CMP );
                        sqlCmd2.Parameters.AddWithValue("@1CMPctChange", d1CMPct);
                        sqlCmd2.Parameters.AddWithValue("@2CMCur",i2CM);
                        sqlCmd2.Parameters.AddWithValue("@2CMPrev",i2CMP);
                        sqlCmd2.Parameters.AddWithValue("@2CMPctChange", d2CMPct);
                        sqlCmd2.Parameters.AddWithValue("@3CMCur",i3CM);
                        sqlCmd2.Parameters.AddWithValue("@3CMPrev",i3CMP);                      
                        sqlCmd2.Parameters.AddWithValue("@3CMPctChange", d3CMPct);
                        sqlCmd2.Parameters.AddWithValue("@1RESCur", i1RES);
                        sqlCmd2.Parameters.AddWithValue("@1RESPrev", i1RESP);
                        sqlCmd2.Parameters.AddWithValue("@1RESPctChange", d1RESPct);
                        sqlCmd2.Parameters.AddWithValue("@2RESCur", i2RES);
                        sqlCmd2.Parameters.AddWithValue("@2RESPrev", i2RESP);
                        sqlCmd2.Parameters.AddWithValue("@2RESPctChange", d2RESPct);
                        sqlCmd2.Parameters.AddWithValue("@3RESCur", i3RES);
                        sqlCmd2.Parameters.AddWithValue("@3RESPrev", i3RESP);
                        sqlCmd2.Parameters.AddWithValue("@3RESPctChange", d3RESPct);
                        sqlCmd2.Parameters.AddWithValue("@NPCur", iNP);
                        sqlCmd2.Parameters.AddWithValue("@NPPrev", iNPP);
                        sqlCmd2.Parameters.AddWithValue("@NPPctChange", dNPPct);
                        sqlCmd2.Parameters.AddWithValue("@TELCur", iTEL);
                        sqlCmd2.Parameters.AddWithValue("@TELPrev", iTELP);
                        sqlCmd2.Parameters.AddWithValue("@TELPctChange", dTELPct);
                        sqlCmd2.Parameters.AddWithValue("@WCCur", iWC);
                        sqlCmd2.Parameters.AddWithValue("@WCPrev", iWCP);
                        sqlCmd2.Parameters.AddWithValue("@WCPctChange", dWCPct);  

                        iRet = sqlCmd2.ExecuteNonQuery();

                        //if (iRet != 1)
                        //    throw new SystemException("sp_sp_ZipCodeDist2Month_Finalize returned " + iRet.ToString() + " for " + sDistrict);

                    } while (sqlReader.Read());     // districts

                    sqlReader.Close();

                }   // has rows
            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End TwoMonth ----");
                oLU.closeLog();

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn1.Close();


            }


        }


        static void BuildSIBZipCodeSkillSet(bool bCurPd, bool bClearTable)
        {

            // Populate table ZipCodeSkillSetWork
            // Current vs Previous month determine by bCurPD
            // CurPD = true gets data 1 month prior to run date 
            // CurPD = false gets data 2 months prior to run date             

            DateTime dDate = DateTime.Today;

            if (bCurPd)
            {
                dDate = dDate.AddMonths(-1);
            }
            else
            {
                dDate = dDate.AddMonths(-2);
            }

            CultureInfo info = Thread.CurrentThread.CurrentCulture;
            DateTime dBegOfMonth = FirstDayOfMonth(dDate);
            DateTime dEndOfMonth = LastDayOfMonth(dDate);

            // set time to 12:00am
            dBegOfMonth = ChangeTime(dBegOfMonth, 0, 0, 0, 0);

            // set time to midnight
            dEndOfMonth = ChangeTime(dEndOfMonth, 23, 59, 59, 0);

            DateTime dStatDate = DateTime.Now;
            String sStatTime = DateTime.Now.ToShortTimeString();

            int iRet = 0;
            int iKeynumber = 0;
            string sZipCode = "";
            string sOptions = "";
            string sSkillSet = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;
            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin BuildSIBZipCodeSkillSet ++++");

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_SQLMainSIBIConnStr);
                sqlCmd1 = new SqlCommand();
                sqlConn2 = new SqlConnection(cfg_SQLMainUTILConnStr);
                sqlCmd2 = new SqlCommand();
                sqlConn1.Open();

                // Second SQL conn for updating table
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                // clear table first time through
                if (bClearTable)
                {
                    sqlCmd2.CommandType = CommandType.Text;
                    sqlCmd2.CommandText = "DELETE FROM ZipCodeSkillSetWork";
                    sqlCmd2.Connection = sqlConn2;
                    sqlCmd2.ExecuteNonQuery();
                }

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "dbo.sp_ZipCodeSkillSet_GetKeys";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegOfMonth);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndOfMonth);

                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "dbo.sp_ZipCodeSkillSetWork_Insert";

                // Get all completed cases
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through cases
                    do
                    {


                        iKeynumber = (int)sqlReader.GetSqlInt32(0);

                        //Location zip
                        if (sqlReader.IsDBNull(1))
                        {
                            sZipCode = "";
                        }
                        else
                        {
                            sZipCode = sqlReader.GetSqlString(1).ToString();

                            // if zip is < 5 ignore it
                            if (sZipCode.Length < 5)
                                sZipCode = "";
 
                            // trim to 5 chars max
                            else if (sZipCode.Length > 5)
                                sZipCode = sZipCode.Remove(5);

                            if (sZipCode.Length == 5 && sZipCode.Substring(0,1) != "3")
                                sZipCode = "";

                            if (!IsNumeric(sZipCode))
                                sZipCode = "";
                        }

                        //Options
                        if (sqlReader.IsDBNull(2))
                        {
                            sOptions = "";
                        }
                        else
                        {
                            sOptions = sqlReader.GetSqlString(2).ToString();
                            sOptions = sOptions.ToUpper();
                        }

                        // If zipcode is valid
                        if (sZipCode != "" && sOptions != "")
                        {

                            // set SkillSet based on form in Options
                            if (sOptions.IndexOf("ORM 1010") > 0)
                            {
                                sSkillSet = "1CM";
                            }
                            else if (sOptions.IndexOf("ORM 1016") > 0)
                            {
                                sSkillSet = "1CM";
                            }
                            else if (sOptions.IndexOf("ORM 1028") > 0)
                            {
                                sSkillSet = "1CM";
                            }
                            else if (sOptions.IndexOf("ORM 1080L") > 0)
                            {
                                sSkillSet = "1RES";
                            }
                            else if (sOptions.IndexOf("ORM 1081") > 0)
                            {
                                sSkillSet = "1RES";
                            }
                            else if (sOptions.IndexOf("ORM 1061") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1091") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1007") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1008") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1008P") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1015") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1024") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1039") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1054") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1057") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1091L") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1095") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1097") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1098") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 3005") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 3007") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 3008") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6010") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6020") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6030") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 6040") > 0)
                            {
                                sSkillSet = "2CM";
                            }
                            else if (sOptions.IndexOf("ORM 1052") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 1080AB") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 4000") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 4010") > 0)
                            {
                                sSkillSet = "2RES";
                            }
                            else if (sOptions.IndexOf("ORM 1003") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1004") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1005") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1012") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1014") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1099") > 0)
                            {
                                sSkillSet = "3CM";
                            }
                            else if (sOptions.IndexOf("ORM 1037") > 0)
                            {
                                sSkillSet = "3RES";
                            }
                            else if (sOptions.IndexOf("ORM 1080A") > 0)
                            {
                                sSkillSet = "3RES";
                            }
                            else if (sOptions.IndexOf("ORM 7131") > 0)
                            {
                                sSkillSet = "NP";
                            }
                            else if (sOptions.IndexOf("ORM 1011") > 0)
                            {
                                sSkillSet = "TEL";
                            }
                            else if (sOptions.IndexOf("ORM 1045") > 0)
                            {
                                sSkillSet = "TEL";
                            }
                            else if (sOptions.IndexOf("ORM 1056") > 0)
                            {
                                if (sOptions.IndexOf("ORM 1056C") > 0)
                                {
                                    sSkillSet = "TEL";
                                }
                                else if (sOptions.IndexOf("ORM 1056T") > 0)
                                {
                                    sSkillSet = "TEL";
                                }
                                else if (sOptions.IndexOf("ORM 1056R") > 0)
                                {
                                    sSkillSet = "TEL";
                                }
                                else
                                {
                                sSkillSet = "2CM";
                                }
                            }
                            else if (sOptions.IndexOf("ORM 1023") > 0)
                            {
                                sSkillSet = "WC";
                            }

                            // log no skillset
                            if (sSkillSet == "")
                            {
                                oLU.WritetoLog("Keynumber: " + iKeynumber.ToString() + " - undetermined skill set");
                            }
                            else
                            {

                                // Insert into table
                                sqlCmd2.Parameters.Clear();

                                sqlCmd2.Parameters.AddWithValue("@keynumber", iKeynumber);
                                sqlCmd2.Parameters.AddWithValue("@ZipCode", sZipCode);
                                sqlCmd2.Parameters.AddWithValue("@SkillSetCode", sSkillSet);

                                iRet = sqlCmd2.ExecuteNonQuery();

                                if (iRet == 0)
                                    throw new SystemException("dbo.sp_ZipCodeSkillSetWork_Insert returned 0 for " + iKeynumber.ToString());

                            }
                        }
                    } while (sqlReader.Read());     // cases

                    sqlReader.Close();

                }   // has rows

            }

            catch (Exception ex)
            {

                //record exception  
                oLU.WritetoLog(ex.Message);

            }

            finally
            {

                // close objects
                oLU.WritetoLog("---- End BuildSIBZipCodeSkillSet ----");
                oLU.closeLog();

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

                if (sqlConn2 != null)
                    sqlConn1.Close();


            }


        }








        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

        static decimal calcPct(int iCur, int iPrev)
        {
            
            decimal dRetVal = 0;

            if (iCur == 0 && iPrev == 0)
                return dRetVal;

            decimal dCur = Convert.ToDecimal(iCur);
            decimal dPrev = Convert.ToDecimal(iPrev);

            if (dCur == 0)
            {
                return 0 - dPrev;
            }
            if (dPrev == 0)
            {
                return dCur;
            }

            if (dCur - dPrev > 0)
            {
                dRetVal = 1 - (dPrev / dCur);  
            }
            else
            {
                dRetVal = 0 - (1-(dCur / dPrev));            
            }

            return dRetVal;
        
        }

        // IsNumeric Function
        static bool IsNumeric(object Expression)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum;

            // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
            double retNum;

            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }		
    }
}
