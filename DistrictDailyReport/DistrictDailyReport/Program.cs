﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace DistrictDailyReport
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static int cfg_TimeZoneOffset;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet oWorksheet = null;
        static Excel.Range oRange = null;
        static DateTime dBegDate;
        static DateTime dEndDate;
        static DateTime dBegDate360;
        static DateTime dEndDate360;
        static DateTime dReportDate;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            // Runs for the current month to date
            DateTime dt = DateTime.Today;
            dReportDate = dt;
            dBegDate = FirstDayOfMonth(dt);
            dEndDate = LastDayOfMonth(dt);

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_TimeZoneOffset = getTimeZoneOffset();

                string sDateDisp = dReportDate.Month + "-" + dReportDate.Day + "-" + dReportDate.Year ;
                string sEmailSubject = "";
                string sExcelFileNameNoEx = "";
                string sExcelFileName = "";

                // adjust time to GMT
                dBegDate = ChangeTime(dBegDate, 0, 0, 0, 0);
                dBegDate360 = dBegDate.AddHours(cfg_TimeZoneOffset);
                dEndDate = ChangeTime(dEndDate, 23, 59, 59, 0);
                dEndDate360 = dEndDate.AddHours(cfg_TimeZoneOffset);

                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                SqlConnection sqlConn2 = null;
                SqlCommand sqlCmd2 = null;

                string sDistrict = "";
                Guid guDistID;
                string sDistEmail = "";
                int iTotComp = 0;
                int iCompLT30 = 0;
                double dCompPct = 0;
                int iUnassigned = 0;
                int iTotActive = 0;
                double dAssignPct = 0;
                int iRow = 0;

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                // Set up SQL for Districts
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetDistricts";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // Set up SQL for data
                sqlConn2 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd2 = new SqlCommand();
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "sp_Count_CasesCompInDistrict";
                sqlCmd2.Connection = sqlConn2;
                sqlConn2.Open();

                // Get list of districts from SQL
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through districts
                    do
                    {
                        // district id
                        guDistID = (Guid)sqlReader.GetSqlGuid(0);

                        // district name
                        sDistrict = (string)sqlReader.GetSqlString(1);

                        // email
                        sDistEmail = (string)sqlReader.GetSqlString(2);

                        if (sDistrict != "Z")
                        {

                            sEmailSubject = "District " + sDistrict + " report for " + dReportDate.Month + "/" + dReportDate.Day + "/" + dReportDate.Year.ToString();
                            sExcelFileNameNoEx = cfg_outputdir + "District" + sDistrict + "Report" + sDateDisp;
                            sExcelFileName = sExcelFileNameNoEx + ".xlsx";

                            // Init Excel sheet
                            oExcel = new Excel.Application();
                            oExcel.Visible = true;
                            oWorkbook = oExcel.Workbooks.Add(1);
                            oWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                            oWorksheet.Name = "District " + sDistrict;
                            createHeader(oWorksheet, sDistrict, true);
                            iRow = 4;

                            // **** Unassigned section ****
                            // Unassigned cases
                            // Based on location zip code assigned to DAR
                            sqlCmd2.CommandText = "sp_Count_UnassignedInDistrictNOAMRisc";
                            sqlCmd2.Parameters.Clear();
                            sqlCmd2.Parameters.AddWithValue("@district", guDistID);
                            iUnassigned = (int)sqlCmd2.ExecuteScalar();

                            //Total active
                            //Based on district of inspector assigned to case 
                            //Does not include Hold, Unassigned, completed or reviewed
                            sqlCmd2.CommandText = "sp_Count_ActiveCasesInDistrictNOAMRisc";
                            iTotActive = (int)sqlCmd2.ExecuteScalar();

                            //*** ADD UNASSIGNED TO GET TOTAL 
                            iTotActive += iUnassigned;

                            // percent
                            if (iUnassigned == 0 || iTotActive == 0)
                                dAssignPct = 0;
                            else
                                dAssignPct = ((double)iUnassigned / iTotActive) * 100;

                            addData(oWorksheet, iRow, 1, iUnassigned.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "#,##0");
                            addData(oWorksheet, iRow, 2, iTotActive.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0");
                            addData(oWorksheet, iRow, 3, dAssignPct.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "##0.00");

                            // **** Completed section ****
                            createHeader(oWorksheet, sDistrict, false);
                            iRow = 8;

                            // Compelted cases
                            sqlCmd2.CommandText = "sp_Count_CasesCompInDistrictNOAMRisc";
                            sqlCmd2.Parameters.Clear();
                            sqlCmd2.Parameters.AddWithValue("@district", guDistID);
                            sqlCmd2.Parameters.AddWithValue("@begdate", dBegDate);
                            sqlCmd2.Parameters.AddWithValue("@enddate", dEndDate);
                            iTotComp = (int)sqlCmd2.ExecuteScalar();

                            //Completed < 30
                            sqlCmd2.CommandText = "sp_Count_CasesCompU30InDistrictNOAMRisc";
                            iCompLT30 = (int)sqlCmd2.ExecuteScalar();

                            // Comp < 30 percent
                            if (iCompLT30 == 0 || iTotComp == 0)
                                dCompPct = 0;
                            else
                                dCompPct = ((double)iCompLT30 / iTotComp) * 100;

                            addData(oWorksheet, iRow, 1, iTotComp.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "#,##0");
                            addData(oWorksheet, iRow, 2, iCompLT30.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0");
                            addData(oWorksheet, iRow, 3, dCompPct.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "##0.00");

                            oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                            oWorkbook.Close(true, oMissing, oMissing);
                            oExcel.Quit();
                            msMsgBody += "Processing completed for District " + sDistrict + System.Environment.NewLine + sExcelFileName + System.Environment.NewLine + System.Environment.NewLine;

                            releaseObject(oExcel);
                            releaseObject(oWorkbook);
                            releaseObject(oWorksheet);
                            sendExcelFile(sDistEmail, sEmailSubject, sExcelFileName);
                            msMsgBody += "File sent to " + sDistEmail + " " + sExcelFileName;

                        }   // skip district Z

                    } while (sqlReader.Read());     // Districts

                    //////////////////////////
                    // AMRISC
                    /////////////////////////
                    sDistEmail = "Karla.Belair@sibfla.com";

                    sEmailSubject = "AMRisc district report for " + dReportDate.Month + "/" + dReportDate.Day + "/" + dReportDate.Year.ToString();
                    sExcelFileNameNoEx = cfg_outputdir + "AMRiscDistrictReport" + sDateDisp;
                    sExcelFileName = sExcelFileNameNoEx + ".xlsx";

                    // Init Excel sheet
                    oExcel = new Excel.Application();
                    oExcel.Visible = true;
                    oWorkbook = oExcel.Workbooks.Add(1);
                    oWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    oWorksheet.Name = "AMRisc District";
                    createHeader(oWorksheet, "AMRisc", true);
                    iRow = 4;

                    // **** Unassigned section ****
                    // Unassigned cases
                    // Based on location zip code assigned to DAR
                    sqlCmd2.CommandText = "sp_Count_UnassignedInDistrictAMRisc";
                    sqlCmd2.Parameters.Clear();
                    iUnassigned = (int)sqlCmd2.ExecuteScalar();

                    //Total active
                    //Based on district of inspector assigned to case 
                    //Does not include Hold, Unassigned, completed or reviewed
                    sqlCmd2.CommandText = "sp_Count_ActiveCasesInDistrictAMRisc";
                    iTotActive = (int)sqlCmd2.ExecuteScalar();

                    //*** ADD UNASSIGNED TO GET TOTAL 
                    iTotActive += iUnassigned;

                    // percent
                    if (iUnassigned == 0 || iTotActive == 0)
                        dAssignPct = 0;
                    else
                        dAssignPct = ((double)iUnassigned / iTotActive) * 100;

                    addData(oWorksheet, iRow, 1, iUnassigned.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "#,##0");
                    addData(oWorksheet, iRow, 2, iTotActive.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0");
                    addData(oWorksheet, iRow, 3, dAssignPct.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "##0.00");

                    // **** Completed section ****
                    createHeader(oWorksheet, "AMRisc", false);
                    iRow = 8;

                    // Completed cases
                    sqlCmd2.CommandText = "sp_Count_CasesCompInDistrictAMRisc";
                    sqlCmd2.Parameters.Clear();
                    sqlCmd2.Parameters.AddWithValue("@begdate", dBegDate);
                    sqlCmd2.Parameters.AddWithValue("@enddate", dEndDate);
                    iTotComp = (int)sqlCmd2.ExecuteScalar();

                    //Completed < 30
                    sqlCmd2.CommandText = "sp_Count_CasesCompU30InDistrictAMRisc";
                    iCompLT30 = (int)sqlCmd2.ExecuteScalar();

                    // Comp < 30 percent
                    if (iCompLT30 == 0 || iTotComp == 0)
                        dCompPct = 0;
                    else
                        dCompPct = ((double)iCompLT30 / iTotComp) * 100;

                    addData(oWorksheet, iRow, 1, iTotComp.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "#,##0");
                    addData(oWorksheet, iRow, 2, iCompLT30.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0");
                    addData(oWorksheet, iRow, 3, dCompPct.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "##0.00");

                    oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                    oWorkbook.Close(true, oMissing, oMissing);
                    oExcel.Quit();
                    msMsgBody += "Processing completed for AMRisc District" + System.Environment.NewLine + sExcelFileName + System.Environment.NewLine + System.Environment.NewLine;

                    //////////////////////////
                    // CGCU
                    /////////////////////////
                    sDistEmail = "CGCU@sibfla.com";

                    sEmailSubject = "CGCU district report for " + dReportDate.Month + "/" + dReportDate.Day + "/" + dReportDate.Year.ToString();
                    sExcelFileNameNoEx = cfg_outputdir + "CGCUDistrictReport" + sDateDisp;
                    sExcelFileName = sExcelFileNameNoEx + ".xlsx";

                    // Init Excel sheet
                    oExcel = new Excel.Application();
                    oExcel.Visible = true;
                    oWorkbook = oExcel.Workbooks.Add(1);
                    oWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    oWorksheet.Name = "CGCU District";
                    createHeader(oWorksheet, "CGCU", true);
                    iRow = 4;

                    // **** Unassigned section ****
                    // Unassigned cases
                    // Based on location zip code assigned to DAR
                    sqlCmd2.CommandText = "sp_Count_UnassignedInDistrictCGCU";
                    sqlCmd2.Parameters.Clear();
                    iUnassigned = (int)sqlCmd2.ExecuteScalar();

                    //Total active
                    //Based on district of inspector assigned to case 
                    //Does not include Hold, Unassigned, completed or reviewed
                    sqlCmd2.CommandText = "sp_Count_ActiveCasesInDistrictCGCU";
                    iTotActive = (int)sqlCmd2.ExecuteScalar();

                    //*** ADD UNASSIGNED TO GET TOTAL 
                    iTotActive += iUnassigned;

                    // percent
                    if (iUnassigned == 0 || iTotActive == 0)
                        dAssignPct = 0;
                    else
                        dAssignPct = ((double)iUnassigned / iTotActive) * 100;

                    addData(oWorksheet, iRow, 1, iUnassigned.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "#,##0");
                    addData(oWorksheet, iRow, 2, iTotActive.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0");
                    addData(oWorksheet, iRow, 3, dAssignPct.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "##0.00");

                    // **** Completed section ****
                    createHeader(oWorksheet, "CGCU", false);
                    iRow = 8;

                    // Completed cases
                    sqlCmd2.CommandText = "sp_Count_CasesCompInDistrictCGCU";
                    sqlCmd2.Parameters.Clear();
                    sqlCmd2.Parameters.AddWithValue("@begdate", dBegDate);
                    sqlCmd2.Parameters.AddWithValue("@enddate", dEndDate);
                    iTotComp = (int)sqlCmd2.ExecuteScalar();

                    //Completed < 30
                    sqlCmd2.CommandText = "sp_Count_CasesCompU30InDistrictCGCU";
                    iCompLT30 = (int)sqlCmd2.ExecuteScalar();

                    // Comp < 30 percent
                    if (iCompLT30 == 0 || iTotComp == 0)
                        dCompPct = 0;
                    else
                        dCompPct = ((double)iCompLT30 / iTotComp) * 100;

                    addData(oWorksheet, iRow, 1, iTotComp.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "#,##0");
                    addData(oWorksheet, iRow, 2, iCompLT30.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0");
                    addData(oWorksheet, iRow, 3, dCompPct.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "##0.00");

                    oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                    oWorkbook.Close(true, oMissing, oMissing);
                    oExcel.Quit();
                    msMsgBody += "Processing completed for CGCU District" + System.Environment.NewLine + sExcelFileName + System.Environment.NewLine + System.Environment.NewLine;

                    releaseObject(oExcel);
                    releaseObject(oWorkbook);
                    releaseObject(oWorksheet);
                    sendExcelFile(sDistEmail, sEmailSubject, sExcelFileName);
                    msMsgBody += "File sent to " + sDistEmail + " " + sExcelFileName;

                }   // has rows
                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;


                //if (bDataFound)
                //{
                //oWorkbook.SaveAs(cfg_outputdir + "ReviewerStats360_" + sDateDisp, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                //oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                //oWorkbook.Close(true, oMissing, oMissing);
                //oExcel.Quit();
                //msMsgBody += "Processing completed for 360" + System.Environment.NewLine + cfg_outputdir + "ReviewerStatsSIB_" + sDateDisp + System.Environment.NewLine + System.Environment.NewLine;

                //releaseObject(oExcel);
                //releaseObject(oWorkbook);
                //releaseObject(oWorksheet);
                //sendExcelFile(sEmailSubject, sExcelFileName);
                //msMsgBody += "File sent " + sExcelFileName;
                //}
                //else
                //{
                //    msMsgBody += "No data found";
                //}

            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(msMsgBody);
                }

            }
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;
        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {
            DateTime dRet = dDate;
            dRet = dRet.AddDays(-(dRet.Day - 1));
            return dRet;
        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {
            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;
        }


        static void createHeader(Excel._Worksheet oWorkSheet, string sDistrict, bool bAssigned)
        {
            Excel.Range oRange;

            if (bAssigned)
            {
                oWorkSheet.get_Range("A1", "C1").Merge(false);
                oRange = oWorkSheet.get_Range("A1", "C1");
                oRange.FormulaR1C1 = "District " + sDistrict + " as of " + dReportDate.ToShortDateString();

                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                //oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.Cells[3, 1] = "Unassigned";
                oRange = oWorkSheet.get_Range("A3", "A3");
                oRange.ColumnWidth = 25;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[3, 2] = "Total Active";
                oRange = oWorkSheet.get_Range("B3", "B3");
                oRange.ColumnWidth = 25;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[3, 3] = "Percent";
                oRange = oWorkSheet.get_Range("C3", "C3");
                oRange.ColumnWidth = 25;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);
            }

            else
            {
                //oWorkSheet.get_Range("A1", "C1").Merge(false);
                //oRange = oWorkSheet.get_Range("A1", "C1");
                //oRange.FormulaR1C1 = "District " + sDistrict + " Report for " + dBegDate.ToShortDateString();

                //oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                //oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                //oRange.Font.Size = 16;
                //oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.Cells[7, 1] = "Total Comp";
                oRange = oWorkSheet.get_Range("A7", "A7");
                oRange.ColumnWidth = 25;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[7, 2] = "Comp < 30";
                oRange = oWorkSheet.get_Range("B7", "B7");
                oRange.ColumnWidth = 25;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[7, 3] = "Percent";
                oRange = oWorkSheet.get_Range("C7", "C7");
                oRange.ColumnWidth = 25;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);


            }
        }

        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2, string format)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }

        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "Daily District Report";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;
            }

        }

        static void sendErrEmail(string sMsgBody)
        {
            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by District Daily Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sRecip, string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = sRecip;
                //oMail.MailBCC = "Andrea@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

        static int getTimeZoneOffset()
        {
            SqlConnection sqlConn = null;
            SqlCommand sqlCmd = null;
            int iRetVal = 0;

            try
            {
                //string sQuery = @"SELECT dbo.TimeZoneOffset AS TZOffset;";
                sqlConn = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd = new SqlCommand("SELECT dbo.TimeZoneOffset()", sqlConn);
                sqlConn.Open();
                Int32 functionResult = (Int32)sqlCmd.ExecuteScalar();

                iRetVal = functionResult;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }
            finally
            {

                // close objects
                if (sqlConn != null)
                    sqlConn.Close();
            }

            return iRetVal;
        }

    }
}
