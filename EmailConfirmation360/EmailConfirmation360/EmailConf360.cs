﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;


namespace EmailConfirmation360
{



    public class EmailConf360
    {

        public string cfg_360ConnStr { get; set; }
        public string cfg_smtpserver { get; set; }

        public string CustNum { get; set; }
        public string PolicyNum { get; set; }
        public string RequestedBy { get; set; }
        public string InsuredName { get; set; }
        public string InsuredContactName{ get; set; }
        public string InsuredContactPhoneHome { get; set; }
        public string InsuredContactPhoneWork { get; set; }
        public string InsuredContactPhoneCell { get; set; }                       
        public string CaseNum { get; set; }
        public string Recip { get; set; }
        public string BCC { get; set; }
        public string DateSubmitted { get; set; }
        public string InsAdd1 { get; set; }
        public string InsAdd2 { get; set; }
        public string InsCity { get; set; }
        public string InsState { get; set; }
        public string InsZip { get; set; }
        public string LocAdd1 { get; set; }
        public string LocAdd2 { get; set; }
        public string LocCity { get; set; }
        public string LocState { get; set; }
        public string LocZip { get; set; }
        public string LocContact { get; set; }
        public string LocContactPhone { get; set; }
        public string Agent { get; set; }
        public string AgentPhone { get; set; }
        public string InsuranceCo { get; set; }
        public string Underwriter { get; set; }
        public string UnderwriterPhone { get; set; }
        public string UnderwriterEmail { get; set; }
        public string InspectionType { get; set; }
        public string Comments { get; set; }
        public string EmailSubject { get; set; }


        //Used for email message body.  
        private StringBuilder sbmsgBody = new StringBuilder();

        public string sendEmailConf360()
        {
            
            string sRet = "";


            if (cfg_360ConnStr.Length == 0 || cfg_smtpserver.Length == 0 || CaseNum.Length == 0 || Recip.Length == 0)
            {
                return "Configuration values missing";
            }

            string sCustomerName = "";
            string sInsAdd = buildInsAdd();
            string sLocAdd = buildLocAdd();
            string sInsContactPhones = buildInsContPhone();
            string sInspectionType = "";

            try

            {

                // get customer name
                sCustomerName = getCustomerName();

                // get inspection type
                sInspectionType = getCaseTypeName(InspectionType);

                // begin header
                sbmsgBody = sbmsgBody.Append("<html><head><title>Sutton Inspection Bureau, Inc. of Florida</title>" + System.Environment.NewLine);

                // Style
                sbmsgBody = sbmsgBody.Append("<style type='text/css'>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append(".stdText { FONT-SIZE: 8pt; COLOR: black;Font-FAMILY: Verdana,Tahoma,Arial}" + System.Environment.NewLine);

                // end header
                sbmsgBody = sbmsgBody.Append("</style></head><body>" + System.Environment.NewLine);


                // begin body
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td width='10'>&nbsp;</td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='left' width='475'><span class='stdText'>Sutton Inspection Bureau, Inc. of Florida</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right' width='175'><span class='stdText'>&nbsp;</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("</tr><tr><td>&nbsp;</td>");
                sbmsgBody = sbmsgBody.Append("<td align='left'><span class='stdText'> Inspection Request Confirmation</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right'><span class='stdText'><b>Case number: " + CaseNum + "</b></span></td></tr></table>" + System.Environment.NewLine);

                // HR
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

                // Ordered by
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='right' width='100'>");
                sbmsgBody = sbmsgBody.Append("<span Class='stdText'>Ordered by:</span></td><td align='left' width='300'><span Class='stdText'>" + sCustomerName + "</span>");
                sbmsgBody = sbmsgBody.Append("</td><td align='right' width='260' class='stdText'><span Class='stdText'>" + RequestedBy + "</span></td></tr></table>");

                // HR
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

                // Insured name
                // Address / Submit time
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right' width='110'><span class='stdText'>Insured name:</span></td><td align='left' width='395'><span class='stdText'>" + InsuredName + "</span></td><td align='center' width='155' class='stdText'>Date submitted</td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td align='right' valign='top' ><span class='stdText'>Address:</span></td><td align='left' valign=top><span Class='stdText'>" + sInsAdd + "</span></td><td align='center' valign=top><span Class='stdText'>" + DateSubmitted + "<br></span></td></tr>" + System.Environment.NewLine);

                // Contact / phone
                sbmsgBody = sbmsgBody.Append("<tr><td align='right'><span class='stdText'>Contact:</span></td><td align='left'><span class='stdText'>" + InsuredContactName + "</span></td><td align='right'><span class='stdText'>" + sInsContactPhones + "</span></td></tr></table>" + System.Environment.NewLine);

                // HR
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);


                // Requested by / policy # / underwriter
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Requested by:</span></td><td width='530' align='left'><span class='stdText'>" + RequestedBy + "</span></td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td align='right'><span class='stdText'>Policy:</span></td><td align='left'><span class='stdText'>" + PolicyNum + "</span></td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td align='right'><span class='stdText'>Underwriter:</span></td><td align='left'><span class='stdText'>" + Underwriter + "</span></td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td align='right'><span class='stdText'>Phone:</span></td><td align='left'><span class='stdText'>" + Underwriter + "</span></td></tr></table>" + System.Environment.NewLine);

                // Agency/Phone
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Agency/Agent:</span></td><td width='530' align='left'><span class='stdText'>" + Agent + "</span></td></tr></table>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Phone:</span></td><td width='530' align='left'><span class='stdText'>" + AgentPhone + "</span></td></tr></table>" + System.Environment.NewLine);

                // InsCo 
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Insurance Co:</span></td><td width='530' align='left'><span class='stdText'>" + InsuranceCo + "</span></td></tr></table>" + System.Environment.NewLine);

                // HR
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

                // Location address
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Location:</span></td><td width='530' align='left'><span class='stdText'>" + sLocAdd + "</span></td></tr></table>" + System.Environment.NewLine);

                // Contact / phone
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Contact:</span></td><td width='530' align='left'><span class='stdText'>" + LocContact + "</span></td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Phone:</span></td><td width='530' align='left'><span class='stdText'>" + LocContactPhone + "</span></td></tr></table>" + System.Environment.NewLine);

                // HR
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

                // Inspection type
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Inspection type:</span></td><td width='530' align='left'><span class='stdText'>" + sInspectionType + "</span></td></tr></table>" + System.Environment.NewLine);

                // HR
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

                // Comments
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650'><span class='stdText'>Comments:</span></td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650'><span class='stdText'>" + Comments + "</td></tr></table>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("</body></html>" + System.Environment.NewLine);

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "sibflamail@sibfla.com";
                    oMail.MailBCC = BCC;
                    oMail.MailTo = Recip;
                    oMail.MsgSubject = "Inspection request confirmation - case # " + CaseNum;
                    oMail.MsgBody = sbmsgBody.ToString();
                    oMail.SMTPServer = cfg_smtpserver;
                    oMail.SendHTML = true;
                    sRet = oMail.Send();
                    oMail = null;
                }

                catch (Exception ex)
                {
                    sRet = ex.Message;
                }

            }

            catch (Exception ex)
            {
                sRet = ex.Message;
            }


            return sRet;
            
        }


        //public string sendEmailConf()
        //{

        //    string sRet = "";


        //    if (cfg_360ConnStr.Length == 0 || cfg_smtpserver.Length == 0 || CaseNum.Length == 0 || Recip.Length == 0)
        //    {
        //        return "Configuration values missing";            
        //    }


        //    string sCustomerName = "";
        //    string sInsAdd = buildInsAdd();
        //    string sLocAdd = buildLocAdd();
        //    string sInsContactPhones = buildInsContPhone();
        //    string sInspectionType = "";

        //    try

        //    {

        //        // get customer name
        //        sCustomerName = getCustomerName();

        //        // get inspection type
        //        sInspectionType = getCaseTypeName(InspectionType);

        //        // begin header
        //        sbmsgBody = sbmsgBody.Append("<html><head><title>Sutton Inspection Bureau, Inc. of Florida</title>" + System.Environment.NewLine);

        //        // Style
        //        sbmsgBody = sbmsgBody.Append("<style type='text/css'>"  + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append(".stdText { FONT-SIZE: 8pt; COLOR: black;Font-FAMILY: Verdana,Tahoma,Arial}"  + System.Environment.NewLine);
                
        //        // end header
        //        sbmsgBody = sbmsgBody.Append("</style></head><body>"  + System.Environment.NewLine);


        //        // begin body
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'>"  + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<tr><td width='10'>&nbsp;</td>"  + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td align='left' width='475'><span class='stdText'>Sutton Inspection Bureau, Inc. of Florida</span></td>"  + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td align='right' width='175'><span class='stdText'>&nbsp;</span></td>"  + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("</tr><tr><td>&nbsp;</td>");
        //        sbmsgBody = sbmsgBody.Append("<td align='left'><span class='stdText'> Inspection Request Confirmation</span></td>"  + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td align='right'><span class='stdText'><b>Case number: " + CaseNum + "</b></span></td></tr></table>"  + System.Environment.NewLine);

        //        // HR
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

        //        // Ordered by
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='right' width='100'>");
        //        sbmsgBody = sbmsgBody.Append("<span Class='stdText'>Ordered by:</span></td><td align='left' width='300'><span Class='stdText'>" + sCustomerName + "</span>");
        //        sbmsgBody = sbmsgBody.Append("</td><td align='right' width='260' class='stdText'><span Class='stdText'>" + RequestedBy + "</span></td></tr></table>");

        //        // HR
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

        //        // Insured name
        //        // Address / Submit time
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td align='right' width='110'><span class='stdText'>Insured name:</span></td><td align='left' width='395'><span class='stdText'>" + InsuredName + "</span></td><td align='center' width='155' class='stdText'>Date submitted</td></tr>" + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<tr><td align='right' valign='top' ><span class='stdText'>Address:</span></td><td align='left' valign=top><span Class='stdText'>" + sInsAdd + "</span></td><td align='right' valign=top><span Class='stdText'>" + DateSubmitted + "<br></span></td></tr>" + System.Environment.NewLine);

        //        // Contact / phone
        //        sbmsgBody = sbmsgBody.Append("<tr><td align='right'><span class='stdText'>Contact:</span></td><td align='left'><span class='stdText'>" + InsuredContactName + "</span></td><td align='right'><span class='stdText'>" + sInsContactPhones + "</span></td></tr></table>" + System.Environment.NewLine);

        //        // HR
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);


        //        // Requested by / policy # / underwriter
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Requested by:</span></td><td width='375' align='left'><span class='stdText'>" + RequestedBy + "</span></td><td width='155'><span class='stdText'>&nbsp;</span></td></tr>" + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<tr><td align='right'><span class='stdText'>Policy:</span></td><td align='left'><span class='stdText'>" + PolicyNum + "</span></td><td><span class='stdText'>&nbsp;</span></td></tr>" + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<tr><td align='right'><span class='stdText'>Underwriter:</span></td><td align='left'><span class='stdText'>" + Underwriter + "</span></td><td align='right'><span class='stdText'>" + UnderwriterEmail + "</span></td></tr>" + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<tr><td align='right'><span class='stdText'>Underwriter:</span></td><td align='left'><span class='stdText'>" + Underwriter + "</span></td><td align='right'><span class='stdText'>&nbsp;</span></td></tr></table>" + System.Environment.NewLine);

        //        // Agency/Phone
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>"  + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Agency/Agent:</span></td><td width='530' align='left'><span class='stdText'>" + Agent + "</span></td></tr></table>"  + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>"  + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Phone:</span></td><td width='530' align='left'><span class='stdText'>" + AgentPhone+ "</span></td></tr></table>"  + System.Environment.NewLine);

        //        // InsCo 
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>"  + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Insurance Co:</span></td><td width='530' align='left'><span class='stdText'>" + InsuranceCo + "</span></td></tr></table>" + System.Environment.NewLine);

        //        // HR
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

        //        // Location address
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Location:</span></td><td width='530' align='left'><span class='stdText'>" + sLocAdd + "</span></td></tr></table>" + System.Environment.NewLine);

        //        // Contact / phone
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Contact:</span></td><td width='530' align='left'><span class='stdText'>" + LocContact + "</span></td></tr>" + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Phone:</span></td><td width='530' align='left'><span class='stdText'>" + LocContactPhone + "</span></td></tr></table>" + System.Environment.NewLine);

        //        // HR
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

        //        // Inspection type
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr>" + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("<td width='130' align='right'><span class='stdText'>Inspection type:</span></td><td width='530' align='left'><span class='stdText'>" + sInspectionType + "</span></td></tr></table>" + System.Environment.NewLine);

        //        // HR
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650%'><hr align='left' width='100%'></td></tr></table>" + System.Environment.NewLine);

        //        // Comments
        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650'><span class='stdText'>Comments:</span></td></tr></table>" + System.Environment.NewLine);

        //        sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650'><span class='stdText'>" + Comments + "</td></tr></table>" + System.Environment.NewLine);
        //        sbmsgBody = sbmsgBody.Append("</body></html>" + System.Environment.NewLine);
                
        //        try
        //        {

        //            SendMail.SendMail oMail;
        //            oMail = new SendMail.SendMail();

        //            oMail.MailFrom = "sibflamail@sibfla.com";
        //            oMail.MailBCC = BCC;
        //            oMail.MailTo = Recip;

        //            if (EmailSubject.Length > 0)
        //                oMail.MsgSubject = EmailSubject;
        //            else
        //                oMail.MsgSubject = "Inspection request confirmation - case # " + CaseNum;

        //            oMail.MsgBody = sbmsgBody.ToString();
        //            oMail.SMTPServer = cfg_smtpserver;
        //            oMail.SendHTML = true;
        //            sRet = oMail.Send();
        //            oMail = null;                    
        //        }

        //        catch (Exception ex)
        //        {
        //            sRet = ex.Message;
        //        }

        //    }

        //    catch (Exception ex)
        //    {
        //        sRet = ex.Message;
        //    }


        //    return sRet;
        //}


        private string getCustomerName()
        {

            string sRet = "";

            SqlConnection sqlConn1 = new SqlConnection(cfg_360ConnStr);
            SqlCommand sqlCmd1 = new SqlCommand();
            SqlDataReader sqlReader = null;

            // SQL 
            //string sSQLText = "SELECT name " +
            //    " FROM [Sutton].[dbo].[Customers]" +
            //    " WHERE [LookupID] = '" + CustNum + "';";

            sqlCmd1.CommandType = CommandType.StoredProcedure;
            sqlCmd1.CommandText = "sp_GetCustomerName";
            sqlCmd1.Parameters.AddWithValue("@lookupid", CustNum);
            sqlCmd1.Connection = sqlConn1;

            sqlConn1.Open();

            // Get case info
            sqlReader = sqlCmd1.ExecuteReader();

            if (sqlReader.HasRows)
            {
                sqlReader.Read();

                // loop through results
                do
                {

                    //key number
                    if (sqlReader.IsDBNull(0))
                    {
                        sRet = "NOT FOUND";
                    }
                    else
                    {
                        sRet = sqlReader.GetSqlString(0).ToString();
                    }

                } while (sqlReader.Read());

            }   // has rows

            if (sqlReader != null)
                sqlReader.Close();

            if (sqlConn1 != null)
                sqlConn1.Close();

            return sRet;
        
        }

        private string getCaseTypeName(string sID)
        {

            string sRet = sID;

            SqlConnection sqlConn1 = new SqlConnection(cfg_360ConnStr);
            SqlCommand sqlCmd1 = new SqlCommand();
            SqlDataReader sqlReader = null;

            // SQL 
            //string sSQLText = "SELECT name " +
            //    " FROM [Sutton].[dbo].[CaseTypes]" +
            //    " WHERE [LookupID] = '" + sID + "';";

            sqlCmd1.CommandType = CommandType.StoredProcedure;
            sqlCmd1.CommandText = "sp_GetCaseTypeName";
            sqlCmd1.Parameters.AddWithValue("@lookupid", sID);
            sqlCmd1.Connection = sqlConn1;

            sqlConn1.Open();

            // Get case info
            sqlReader = sqlCmd1.ExecuteReader();

            if (sqlReader.HasRows)
            {
                sqlReader.Read();

                // loop through results
                do
                {

                    //key number
                    if (sqlReader.IsDBNull(0))
                    {
                        sRet = sID;
                    }
                    else
                    {
                        sRet = sqlReader.GetSqlString(0).ToString();
                    }

                } while (sqlReader.Read());

            }   // has rows

            if (sqlReader != null)
                sqlReader.Close();

            if (sqlConn1 != null)
                sqlConn1.Close();

            return sRet;

        }

        private string buildInsAdd()
        {
            string sRet = InsAdd1;
            if (InsAdd2 != null)
            {
                if (InsAdd2.Length > 0)
                {
                    sRet += "<br />" + InsAdd2;
                }
            }
            sRet += "<br />" + InsCity + ", " + InsState + " " + InsZip;

            return sRet;
        
        }

        private string buildLocAdd()
        {
            string sRet = LocAdd1;
            if (LocAdd2 != null)
            {
                if (LocAdd2.Length > 0)
                {
                    sRet += "<br />" + LocAdd2;
                }
            }
            sRet += "<br />" + LocCity + ", " + LocState + " " + LocZip;

            return sRet;

        }
        private string buildInsContPhone()
        {

            string sRet = "";

            if (InsuredContactPhoneHome.Length > 0)
                sRet = "Home: " + InsuredContactPhoneHome;

            if (InsuredContactPhoneWork.Length > 0)
            { 
                if (sRet.Length > 0)
                    sRet += "<br/>";

                sRet +="Work: " + InsuredContactPhoneWork;
            }

            if (InsuredContactPhoneCell.Length > 0)
            {
            
                if (sRet.Length > 0)
                    sRet += "<br/>";
                sRet += "Cell: " + InsuredContactPhoneCell;
            
            }
        
            return sRet;
        }
    
    }

    


}

