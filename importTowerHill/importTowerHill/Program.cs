﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;


namespace importTowerHill
{

    /// <summary>
    /// Called directly from imptweb on RudyPC
    /// Must reside on H:\VS2010\importTowerHill\ImportTowerHill\bin\Release
    /// </summary>
    /// 

    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyNumber { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string UnderwriterEmail { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string ContactEmail { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            //public string BusinessOperations { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string ISOClass { get; set; }
            public string YearBuilt { get; set; }
            public string Occupancy { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }
        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
        static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string cfg_SQLMainSIBIConnStr = ConfigurationManager.AppSettings["SIBIConnStr"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];


        static LogUtils.LogUtils oLU;

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {

            string sExcelFileName = "";
            string sUWFirst = "";
            string sUWLast = "";
            string sPolicyNum = "";
            string sCaseNum = "";
            bool bErr = false;


            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                if (args.Length == 0)
                {
                    throw new ApplicationException("No arguments passed");
                }

                // get excel file name from command line
                sExcelFileName = args[0];

                ////////////////////////////////////////////////
                // Do Not Process
                bool bAbort = false;
                if (bAbort)
                {
                    throw new ApplicationException("PROCESSING MANUALLY ABORTED: " + sExcelFileName);
                }
                ////////////////////////////////////////////////

                // does file exist?
                FileInfo fi = new FileInfo(cfg_sourcedir + sExcelFileName);
                if (fi.Exists)
                {
                    oLU.WritetoLog("Processing file: " + sExcelFileName);
                }
                else
                {
                    // send email
                    throw new ApplicationException("File does not exist: " + sExcelFileName);
                }

            }

            catch (Exception ex)
            {
                oLU.closeLog();
                sendErrEmail("Error initializing imptTowerHill\r\n\r\n" + ex.Message);
                return;
            }

            try
            {
                ImportRequests oAPI = new ImportRequests();
                oAPI.CustomerUserName = "APIProd";
                oAPI.CustomerPassword = "Sutton2012";
                oAPI.CustomerAccount = "7116";
                oAPI.Comments = "";

                // Underwriter
                sUWFirst = "";
                sUWLast = "";

                Microsoft.Office.Interop.Excel.Application xlApp;
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                Microsoft.Office.Interop.Excel.Range range;

                string sData = "";
                string sTag = "";
                string sTagWork = "";
                object oValue = null;
                object oValue2 = null;

                int rCnt = 0;
                int cCnt = 0;
                string sProduct = "";
                string sProgram = "";
                string sType = "";
                oAPI.UnderwriterCorrEmail = "emailnotprovided@sibfla.com";
                string sContactEmail = "";

                xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlApp.Visible = true;
                //xlWorkBook = xlApp.Workbooks.Open(sExcelFileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                xlWorkBook = xlApp.Workbooks.Open(cfg_sourcedir + sExcelFileName, 0, false);

                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                range = xlWorkSheet.UsedRange;

                for (rCnt = 5; rCnt <= range.Rows.Count; rCnt++)
                {
                    //for (cCnt = 1; cCnt <= range.Columns.Count; cCnt++)
                    for (cCnt = 1; cCnt <= 1; cCnt++)
                    {

                        sTag = (string)(range.Cells[rCnt, cCnt] as Microsoft.Office.Interop.Excel.Range).Value2;

                        if (sTag == null && sTagWork == null)
                        {
                            sTagWork = sTag;
                        }
                        else if (sTag == null && sTagWork.Length > 0)
                        {
                            sTag = sTagWork;
                        }
                        else
                        {
                            if (sTagWork != "Sub Locations to be Inspected:")
                                sTagWork = sTag;
                        }
                        if (sTag != null)
                        {
                            oValue = (range.Cells[rCnt, 2] as Microsoft.Office.Interop.Excel.Range).Value2;
                            oValue2 = (range.Cells[rCnt, 3] as Microsoft.Office.Interop.Excel.Range).Value2;    // Col C

                            if (oValue == null)
                            {
                                if (oValue2 == null)
                                    sData = null;
                                else
                                    sData = "";
                            }
                            else
                            {
                                sData = oValue.ToString();
                            }

                            //sData = (string)(range.Cells[rCnt, 2] as Microsoft.Office.Interop.Excel.Range).Value2;
                            if (sData != null)
                            {
                                switch (sTag)
                                {
                                    case "Insured:":
                                        oAPI.InsuredName = sData;
                                        break;
                                    case "Vendor Inspection Nbr:":
                                        break;
                                    case "Address:":
                                        oAPI.LocationAddress1 = sData;
                                        oAPI.MailAddress1 = sData;
                                        break;
                                    case "City:":
                                        oAPI.LocationCity = sData;
                                        oAPI.MailCity = sData;
                                        break;
                                    case "State:":
                                        oAPI.LocationState = sData;
                                        oAPI.MailState = sData;
                                        break;
                                    case "Zip:":
                                        oAPI.LocationZip = sData;
                                        oAPI.MailZip = sData;
                                        break;
                                    case "County:":
                                        break;
                                    case "Product:":
                                        sProduct = sData;
                                        break;
                                    case "Program:":
                                        sProgram = sData;
                                        break;
                                    case "Type:":
                                        sType = sData;
                                        break;
                                    case "Policy Nbr:":
                                        sPolicyNum = sData.ToUpper();
                                        oAPI.PolicyNumber = sData.ToUpper();
                                        break;
                                    case "Company:":
                                        oAPI.InsuranceCompany = sData;
                                        break;
                                    case "Underwriter Name:":
                                        sUWFirst = "";
                                        sUWLast = "";
                                        if (sData.Length > 0)
                                        {
                                            if (sData.Contains(" "))
                                            {
                                                int iSpace = sData.IndexOf(" ");

                                                sUWFirst = sData.Substring(0, iSpace);
                                                sUWLast = sData.Substring(iSpace, sData.Length - iSpace);
                                            }
                                        }
                                        oAPI.UnderwriterFirstName = sUWFirst;
                                        oAPI.UnderwriterLastName = sUWLast;

                                        break;
                                    case "Underwriter Email:":
                                        oAPI.UnderwriterCorrEmail = sData;
                                        break;
                                    case "Underwriter Extension:":
                                        oAPI.UnderwriterPhone = sData;
                                        break;
                                    case "Contact Person:":
                                        oAPI.ContactName = sData;
                                        break;
                                    case "Contact Nbr:":
                                        oAPI.ContactPhoneWork = sData;
                                        break;
                                    case "Contact Email:":
                                        sContactEmail = sData;
                                        break;
                                    case "Due Date:":
                                        break;
                                    case "Agency:":
                                        oAPI.AgencyAgentName = sData;
                                        break;
                                    case "Agent Nbr:":
                                        oAPI.AgencyAgentPhone = sData;
                                        break;
                                    case "Agency Contact:":
                                        oAPI.AgencyAgentContact = sData;
                                        break;
                                    case "Date Inspection Requested:":
                                        break;
                                    case "Special Instructions:":

                                        if (sData.Length > 0)
                                        {
                                            if (!oAPI.Comments.Contains("Special instructions from customer"))
                                            {
                                                oAPI.Comments = "Special instructions from customer:\r\n";
                                            }
                                            oAPI.Comments += sData + "\r\n";
                                           
                                        }

                                        // Add contact email
                                        if (sContactEmail.Length > 0)
                                        {
                                            oAPI.Comments = "Contact email: " + sContactEmail + "\r\n" + oAPI.Comments;
                                        }

                                        break;
                                    case "Location to be Inspected:":
                                        if (sData.Length > 0)
                                        {
                                            if (!oAPI.Comments.Contains("Location to be inspected"))
                                            {
                                                oAPI.Comments += "\r\nLocation to be inspected:\r\n";
                                            }
                                            oAPI.Comments += sData + "\r\n";
                                        }
                                        break;
                                    case "Items to be Inspected:":
                                        if (sData.Length > 0)
                                        {
                                            if (!oAPI.Comments.Contains("Items to be inspected"))
                                            {
                                                oAPI.Comments += "\r\nItems to be inspected:\r\n";
                                            }
                                            oAPI.Comments += sData + "\r\n";
                                        }
                                        break;
                                    case "Sub Locations to be Inspected:":
                                        if (sData.Length > 0)
                                        {
                                            if (!oAPI.Comments.Contains("Sub locations to be inspected"))
                                            {
                                                oAPI.Comments += "\r\nSub locations to be inspected:\r\n";
                                            }
                                        }
                                        // Get any value from col C
                                        if (oValue2 != null)
                                        {
                                            sData += "    " + oValue2.ToString();
                                        }

                                        oAPI.Comments += sData + "\r\n";
                                        break;

                                    default:
                                        break;
                                }   // switch
                            }   // sData != null
                        }   // not null
                    }   // cols
                }   // rows

                // parse inspection type
                oAPI.InspectionType = parseInspType(sType, oAPI.PolicyNumber, sProduct, sProgram);

                xlWorkBook.Close(true, null, null);
                xlApp.Quit();


                // Agency / Agent address
                oAPI.AgentAddress1 = "5656 Central Ave";
                oAPI.AgentAddress2 = "";
                oAPI.AgentCity = "St. Petersburg";
                oAPI.AgentState = "FL";
                oAPI.AgentZip = "33707";

                // No inspection type
                if (oAPI.InspectionType == "")
                {
                    throw new ApplicationException("UNRECOGNIZED INSPECTION TYPE");
                }

                oAPI.RushHandling = "N";
                oAPI.EffectiveDate = "";
                oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + oAPI.PolicyNumber);
                sbEmail.Append("Importing Policy# " + oAPI.PolicyNumber + System.Environment.NewLine);

                string sRet = oAPI.Import();

                oLU.WritetoLog("oAPI.Import return for for Policy# " + oAPI.PolicyNumber + "\r\n\r\n" + sRet);

                var importResults = sRet.FromJSON<List<ImportResult>>();

                foreach (var importResult in importResults)
                {

                    if (importResult.Successful)
                    {
                        sCaseNum = importResult.CaseNumber.ToString();

                        oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                        sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + System.Environment.NewLine);
                    }
                    else
                    {
                        oLU.WritetoLog("**** Import failed **** "); if (importResult.Errors != null)
                            sbEmail.Append("**** Import failed ****" + System.Environment.NewLine);
                        bErr = true;

                        foreach (var error in importResult.Errors)
                        {
                            oLU.WritetoLog("Error: " + error.ErrorText);
                            sbEmail.Append("Error: " + error.ErrorText + System.Environment.NewLine);
                        }
                    }

                    if ((bool)importResult.Duplicate)
                    {
                        oLU.WritetoLog("Duplicate case");
                        sbEmail.Append("Duplicate case" + System.Environment.NewLine);
                        bErr = true;
                    }
                }

                string sSourceName = cfg_sourcedir + sExcelFileName;
                string sDestName = "";
                DateTime dNow = DateTime.Now;

                //if successful - copy to sibidata\towerhill\Processed
                //if failed - copy to sibidata\towerhill\fail
                if (sCaseNum.Length > 0)
                {
                    sDestName = cfg_compdir + sCaseNum + ".xls";
                }
                else
                {
                    sDestName = cfg_faildir + dNow.Year.ToString() + dNow.Month.ToString() + dNow.Day.ToString() + dNow.Second.ToString() + ".xls";
                }

                // Copy Excel
                File.Copy(sSourceName, sDestName);
                if (File.Exists(sDestName))
                {
                    File.Delete(sSourceName);
                }
                else
                {
                    throw new ApplicationException("Copy failed for: " + sSourceName + ".xls");
                }

                if (bErr)
                {
                    sendErrEmail("Import failed for: " + sExcelFileName + System.Environment.NewLine + sbEmail.ToString());
                }

            }


            catch (Exception ex)
            {
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail("Exception logged" + System.Environment.NewLine + ex.Message);
            }

            finally
            {
                sendLogEmail(sbEmail.ToString());
                oLU.closeLog();
            }

        }

        static string parseInspType(string sType, string sPolicy, string sProduct, string sProgram)
        {
            string sRet = "";
            sType = sType.Trim();
            sType = sType.ToUpper();
            string sPolPrefix = sPolicy.Substring(0, 3);
            sProduct = sProduct.ToUpper();
            sProgram = sProgram.ToUpper();

            // Drone orders
            // 7116-DRONE-SO
            
            // hard coded inspection type
            if (sType.StartsWith("7116-"))
            {
                sRet = sType;
            }
            else
            {
                // ECF Florida
                if (sPolPrefix == "ECF")
                {
                    if (sProduct == "COMMERCIAL PROPERTY")
                    {
                        if (sProgram == "APARTMENTS")
                        {
                            sRet = "7116-CRCAP";
                        }
                        else if (sProgram == "CONDOMINIUMS")
                        {
                            sRet = "7116-CRCAP";
                        }
                        else if (sProgram == "ASSISTED LIVING FACILITY")
                        {
                            sRet = "7116-ALFP";
                        }
                        else if (sProgram == "DAYCARE")
                        {
                            sRet = "7116-DCP";
                        }
                        else if (sProgram == "HOMEOWNER ASSOCIATIONS / TOWNHOMES")
                        {
                            sRet = "7116-CRHOAP";
                        }
                        else if (sProgram == "HOTEL/MOTEL")
                        {
                            sRet = "7116-HMP";
                        }
                        else if (sProgram.StartsWith("MHP"))
                        {
                            sRet = "7116-MHPP";
                        }
                        else if (sProgram == "SELF-STORAGE")
                        {
                            sRet = "7116-SSFP";
                        }
                        else if (sProgram == "OFFICE/RETAIL")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                        else if (sProgram == "MERCANTILE")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                    }
                }
                //EBP   Florida  
                if (sPolPrefix == "EBP")
                {
                    if (sProduct == "BUSINESS OWNERS POLICY")
                    {
                        if (sProgram == "APARTMENTS")
                        {
                            sRet = "7116-CRCA";
                        }
                        else if (sProgram == "RESIDENTIAL CONDOMINIUMS")
                        {
                            sRet = "7116-CRCA";
                        }
                        else if (sProgram == "HOMEOWNER ASSOCIATIONS")
                        {
                            sRet = "7116-CRHOA";
                        }
                        else if (sProgram == "HOTELMOTEL")
                        {
                            sRet = "7116-HM";
                        }
                        else if (sProgram.StartsWith("MANUFACTURED"))
                        {
                            sRet = "7116-MHP";
                        }
                        else if (sProgram == "SELFSTORAGE")
                        {
                            sRet = "7116-SSF";
                        }
                        else if (sProgram == "OFFICERETAIL")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CT";
                            else
                                sRet = "7116-CLR";
                        }
                        else if (sProgram == "MERCANTILE")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CT";
                            else
                                sRet = "7116-CLR";
                        }
                    }
                }

                // ESF  South Carolina & EAF Alabama
                else if (sPolPrefix == "ESF" || sPolPrefix == "EAF")
                {
                    if (sProduct == "COMMERCIAL PROPERTY")
                    {
                        if (sProgram == "APARTMENTS")
                        {
                            sRet = "7116-CRCAP";
                        }
                        else if (sProgram == "CONDOMINIUMS")
                        {
                            sRet = "7116-CRCAP";
                        }
                        else if (sProgram == "DAYCARE")
                        {
                            sRet = "7116-DCP";
                        }
                        else if (sProgram == "HOMEOWNER ASSOCIATIONS / TOWNHOMES")
                        {
                            sRet = "7116-CRHOAP";
                        }
                        else if (sProgram == "HOTEL/MOTEL")
                        {
                            sRet = "7116-HMP";
                        }
                        else if (sProgram == "SELF-STORAGE")
                        {
                            sRet = "7116-SSFP";
                        }
                        else if (sProgram == "OFFICE/RETAIL")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                        else if (sProgram == "MERCANTILE")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                    }
                }

                // ESB   South Carolina & EAB Alabama
                else if (sPolPrefix == "ESB" || sPolPrefix == "EAB")
                {
                    if (sProduct == "BUSINESS OWNERS POLICY")
                    {
                        if (sProgram == "APARTMENTS")
                        {
                            sRet = "7116-CRCA";
                        }
                        else if (sProgram == "RESIDENTIAL CONDOMINIUMS")
                        {
                            sRet = "7116-CRCA";
                        }
                        else if (sProgram == "HOMEOWNER ASSOCIATIONS")
                        {
                            sRet = "7116-CRHOA";
                        }
                        else if (sProgram == "HOTELMOTEL" || sProgram == "HOTEL/MOTEL")
                        {
                            sRet = "7116-HM";
                        }
                        else if (sProgram.StartsWith("MANUFACTURED"))
                        {
                            sRet = "7116-MHP";
                        }
                        else if (sProgram == "SELFSTORAGE")
                        {
                            sRet = "7116-SSF";
                        }
                        else if (sProgram == "OFFICERETAIL")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CT";
                            else
                                sRet = "7116-CLR";
                        }
                        else if (sProgram == "MERCANTILE")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CT";
                            else
                                sRet = "7116-CLR";
                        }
                    }
                }
                else if (sPolPrefix == "EAF")    //Alabama
                {
                    if (sProduct == "COMMERCIAL PROPERTY")
                    {
                        if (sProgram == "APARTMENTS")
                        {
                            sRet = "7116-CRCAP";
                        }
                        else if (sProgram == "CONDOMINIUMS")
                        {
                            sRet = "7116-CRCAP";
                        }
                        else if (sProgram == "DAYCARE")
                        {
                            sRet = "7116-DCP";
                        }
                        else if (sProgram == "HOMEOWNER ASSOCIATIONS / TOWNHOMES")
                        {
                            sRet = "7116-CRHOAP";
                        }
                        else if (sProgram == "HOTEL/MOTEL")
                        {
                            sRet = "7116-HMP";
                        }
                        else if (sProgram == "SELF-STORAGE")
                        {
                            sRet = "7116-SSFP";
                        }
                        else if (sProgram == "OFFICE/RETAIL")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                        else if (sProgram == "MERCANTILE")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                    }
                }
                // UCF  Indian Harbor  
                else if (sPolPrefix == "UCF")
                {
                    if (sProduct == "COMMERCIAL PROPERTY")
                    {
                        if (sProgram == "COMMERCIAL-RESIDENTIAL")
                        {
                            if (sType == "RENTAL DWELLINGS")
                            {
                                sRet = "7116-CRRDP";
                            }
                            else
                            {
                                sRet = "7116-CRCAP";
                            }
                        }
                        else if (sProgram == "ASSISTEDLIVINGFACILITY")
                        {
                            sRet = "7116-ALFP";
                        }
                        else if (sProgram == "DAYCARE")
                        {
                            sRet = "7116-DCP";
                        }
                        else if (sProgram == "HOA")
                        {
                            sRet = "7116-CRHOAP";
                        }
                        else if (sProgram == "HOTELMOTEL" || sProgram == "HOTEL/MOTEL")
                        {
                            sRet = "7116-HMP";
                        }
                        else if (sProgram == "PARK")
                        {
                            sRet = "7116-MHPP";
                        }
                        else if (sProgram == "SELF-STORAGE" || sProgram == "SELFSTORAGE")
                        {
                            sRet = "7116-SSFP";
                        }
                        else if (sProgram == "OFFICERETAIL" || sProgram == "OFFICE/RETAIL")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                        else if (sProgram == "MAINSTREET" || sProgram == "MERCANTILE")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                    }
                }

                // UBP  Indian Harbor  
                else if (sPolPrefix == "UBP")
                {
                    if (sProduct == "BUSINESS OWNERS POLICY")
                    {
                        if (sProgram == "COMMERCIAL-RESIDENTIAL")
                        {
                            if (sType == "RENTAL DWELLINGS")
                            {
                                sRet = "7116-CRRD";
                            }
                            else
                            {
                                sRet = "7116-CRCA";
                            }
                        }
                        else if (sProgram == "HOA")
                        {
                            sRet = "7116-CRHOA";
                        }
                        else if (sProgram == "HOTELMOTEL")
                        {
                            sRet = "7116-HM";
                        }
                        else if (sProgram == "PARK")
                        {
                            sRet = "7116-MHP";
                        }
                        else if (sProgram == "SELFSTORAGE")
                        {
                            sRet = "7116-SSF";
                        }
                        else if (sProgram == "OFFICERETAIL")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CT";
                            else
                                sRet = "7116-CLR";
                        }
                        else if (sProgram == "SELFSTORAGE")
                        {
                            sRet = "7116-SSF";
                        }
                        else if (sProgram == "MAINSTREET")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                    }
                }

                // UXF /USF Texas
                else if (sPolPrefix == "UXF" || sPolPrefix == "USF")
                {
                    if (sProduct == "COMMERCIAL PROPERTY")
                    {
                        if (sProgram == "COMMERCIAL-RESIDENTIAL")
                        {
                            if (sType == "RENTAL DWELLINGS")
                            {
                                sRet = "7116-CRRDP";
                            }
                            else
                            {
                                sRet = "7116-CRCAP";
                            }
                        }
                        else if (sProgram == "ASSISTEDLIVINGFACILITY")
                        {
                            sRet = "7116-ALFP";
                        }
                        else if (sProgram == "DAYCARE")
                        {
                            sRet = "7116-DCP";
                        }
                        else if (sProgram == "HOA")
                        {
                            sRet = "7116-CRHOAP";
                        }
                        else if (sProgram == "HOTELMOTEL")
                        {
                            sRet = "7116-HMP";
                        }
                        else if (sProgram == "PARK")
                        {
                            sRet = "7116-MHPP";
                        }
                        else if (sProgram == "SELF-STORAGE" || sProgram == "SELFSTORAGE")
                        {
                            sRet = "7116-SSFP";
                        }
                        else if (sProgram == "OFFICERETAIL" || sProgram == "OFFICE/RETAIL")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                        else if (sProgram == "MAINSTREET")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                    }
                }

                // UXB / USB  Texas
                else if (sPolPrefix == "UXB" || sPolPrefix == "USB")
                {
                    if (sProduct == "BUSINESS OWNERS POLICY")
                    {
                        if (sProgram == "COMMERCIAL-RESIDENTIAL")
                        {
                            if (sType == "RENTAL DWELLINGS")
                            {
                                sRet = "7116-CRRD";
                            }
                            else
                            {
                                sRet = "7116-CRCA";
                            }
                        }
                        else if (sProgram == "HOA")
                        {
                            sRet = "7116-CRHOA";
                        }
                        else if (sProgram == "HOTELMOTEL")
                        {
                            sRet = "7116-HM";
                        }
                        else if (sProgram == "PARK")
                        {
                            sRet = "7116-MHP";
                        }
                        else if (sProgram == "SELFSTORAGE")
                        {
                            sRet = "7116-SSF";
                        }
                        else if (sProgram == "OFFICERETAIL")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CT";
                            else
                                sRet = "7116-CLR";
                        }
                        else if (sProgram == "SELFSTORAGE")
                        {
                            sRet = "7116-SSF";
                        }
                        else if (sProgram == "MAINSTREET")
                        {
                            if (sType.Contains("TENANT"))
                                sRet = "7116-CTP";
                            else
                                sRet = "7116-CLRP";
                        }
                    }
                }


            }

            return sRet;
        }

        //static string parseInspTypeOLD(string sInspType, string sPolicy, string sInspTypeDesc)
        //{
        //    string sRet = "";
        //    sInspType = sInspType.Trim();
        //    sInspType = sInspType.ToUpper();
        //    string sPolPrefix = sPolicy.Substring(0, 3);

        //    // hard coded inspection type
        //    if (sInspType.StartsWith("7116-"))
        //    {
        //        sRet = sInspType;
        //    }
        //    // ECF   Florida  
        //    else
        //    {
        //        if (sPolPrefix == "ECF")
        //        {
        //            if (sInspType == "HOMEOWNER ASSOCIATIONS / TOWNHOMES")
        //            {
        //                sRet = "7116-CRHOAP";
        //            }
        //            else if (sInspType == "CONDOMINIUMS")
        //            {
        //                sRet = "7116-CRCAP";
        //            }
        //            else if (sInspType == "MHP (RESIDENT OWNED COMMUNITIES)")
        //            {
        //                sRet = "7116-MHPP";
        //            }

        //            else if (sInspType.Contains("LRO"))
        //            {
        //                if (sInspTypeDesc.Contains("TENANT"))
        //                {
        //                    sRet = "7116-CTP";
        //                }
        //                else
        //                {
        //                    sRet = "7116-CLRP";
        //                }
        //            }
        //            else if (sInspType == "OCCUPIED BY TENANT")
        //            {
        //                sRet = "7116-CTP";
        //            }
        //            else if (sInspType == "HOTEL/MOTEL")
        //            {
        //                sRet = "7116-HMP";
        //            }
        //            else if (sInspType == "DAYCARE")
        //            {
        //                sRet = "7116-DCP";
        //            }


        //        }   // ECF   Florida

        //        //EBP   Florida  
        //        else if (sPolPrefix == "EBP")
        //        {
        //            if (sInspType.Contains("LRO"))
        //            {
        //                if (sInspTypeDesc.Contains("OCCUPIED BY INSURED AS TENANT"))
        //                {
        //                    sRet = "7116-CT";
        //                }
        //                else
        //                {
        //                    sRet = "7116-CLR";
        //                }
        //            }
        //            else if (sInspType.Contains("HOMEOWNER ASSOCIATIONS"))
        //            {
        //                sRet = "7116-CRHOA";
        //            }
        //            else if (sInspType.Contains("RESIDENTIAL CONDO"))
        //            {
        //                sRet = "7116-CRCA";
        //            }
        //            else if (sInspType.Contains("HOTEL"))
        //            {
        //                sRet = "7116-HM";
        //            }
        //            else if (sInspType.Contains("SSF"))
        //            {
        //                sRet = "7116-SSF";
        //            }
        //            else if (sInspType.Contains("MANUFACTURED HOUSING"))
        //            {
        //                sRet = "7116-MHP";
        //            }
        //            else if (sInspType == "OCCUPIED BY TENANT")
        //            {
        //                sRet = "7116-CT";
        //            }
        //            else if (sInspType == "OCCUPIED BY OWNER")
        //            {
        //                sRet = "7116-CLR";
        //            }
        //            else if (sInspType == "APARTMENTS")
        //            {
        //                sRet = "7116-CRCAP";
        //            }


        //        }   //EBP   Florida

        //        // ESF  South Carolina & EAF Alabama
        //        if (sPolPrefix == "ESF" || sPolPrefix == "EAF")
        //        {
        //            if (sInspType.Contains("LRO"))
        //            {
        //                if (sInspTypeDesc.Contains("TENANT"))
        //                {
        //                    sRet = "7116-CTP";
        //                }
        //                else
        //                {
        //                    sRet = "7116-CLR";
        //                }
        //            }
        //            else if (sInspType.Contains("OCCUPIED BY"))
        //            {
        //                if (sInspType.Contains("TENANT"))
        //                {
        //                    sRet = "7116-CTP";
        //                }
        //                else
        //                {
        //                    sRet = "7116-CLRP";
        //                }
        //            }

        //            else if (sInspType.Contains("DAYCARE"))
        //            {
        //                sRet = "7116-DCP";
        //            }

        //        }   //ESF

        //        // ESB   South Carolina & EAB Alabama
        //        if (sPolPrefix == "ESB" || sPolPrefix == "EAB")
        //        {
        //            if (sInspType.Contains("LRO"))
        //            {
        //                if (sInspTypeDesc.Contains("TENANT"))
        //                {
        //                    sRet = "7116-CT";
        //                }
        //                else
        //                {
        //                    sRet = "7116-CLR";
        //                }
        //            }
        //            else if (sInspType.Contains("OCCUPIED BY INSURED AS TENANT"))
        //            {
        //                sRet = "7116-CT";
        //            }
        //            else if (sInspType == "RESIDENTIAL CONDOMINIUMS")
        //            {
        //                sRet = "7116-CRCA";
        //            }
        //            else if (sInspType.Contains("HOMEOWNER ASSOCIATIONS"))
        //            {
        //                sRet = "7116-CRHOA";
        //            }
        //            else if (sInspType == "SSF")
        //            {
        //                sRet = "7116-SSF";
        //            }
        //            else if (sInspType == "APARTMENTS")
        //            {
        //                sRet = "7116-CRCAP";
        //            }
        //            else if (sInspType == "HOTEL/MOTEL")
        //            {
        //                sRet = "7116-HM";
        //            }



        //        }   //ESB

        //        // UCF  Indian Harbor  
        //        if (sPolPrefix == "UCF")
        //        {
        //            if (sInspType == "COMMERCIAL PROPERTY - HOA")
        //            {
        //                sRet = "7116-CRHOAP";
        //            }
        //            else if (sInspType == "COMMERCIAL PROPERTY - DAYCARE")
        //            {
        //                sRet = "7116-DCP";
        //            }
        //            else if (sInspType == "COMMERCIAL PROPERTY - HOTELMOTEL")
        //            {
        //                sRet = "7116-HMP";
        //            }
        //            else if (sInspType == "COMMERCIAL PROPERTY - COMMERCIAL-RESIDENTIAL")
        //            {
        //                sRet = "7116-CRCAP";
        //            }
        //            else if (sInspType == "COMMERCIAL PROPERTY - COMMERCIAL-RESIDENTIAL")
        //            {
        //                sRet = "7116-CRCAP";
        //            }
        //            else if (sInspType == "COMMERCIAL PROPERTY - SELFSTORAGE")
        //            {
        //                sRet = "7116-SSFP";
        //            }

        //            else if (sInspType == "COMMERCIAL PROPERTY - PARK")
        //            {
        //                sRet = "7116-MHPP";
        //            }



        //        }   // UCF  Indian Harbor  

        //        // UBP  Indian Harbor  
        //        if (sPolPrefix == "UBP")
        //        {
        //            if (sInspType == "HOA")
        //            {
        //                sRet = "7116-CRHOA";
        //            }
        //            else if (sInspType == "MHP")
        //            {
        //                sRet = "7116-MHP";
        //            }
        //            else if (sInspType == "HOTEL/MOTEL")
        //            {
        //                sRet = "7116-HM";
        //            }
        //            else if (sInspType == "SSF")
        //            {
        //                sRet = "7116-SSF";
        //            }
        //            else if (sInspType == "COMMERCIAL RESIDENTIAL")
        //            {
        //                sRet = "7116-CRCA";
        //            }

        //            else if (sInspType.Contains("OCCUPIED BY"))
        //            {
        //                if (sInspType.Contains("TENANT"))
        //                {
        //                    sRet = "7116-CT";
        //                }
        //                else
        //                {
        //                    sRet = "7116-CLR";
        //                }
        //            }
        //        }   // UBP  Indian Harbor  

        //        // THB Florida
        //        if (sPolPrefix == "THB")
        //        {

        //            if (sInspType == "COMMERCIAL RESIDENTIAL")
        //            {
        //                sRet = "7116-CRCA";
        //            }
        //            else if (sInspType.Contains("LRO"))
        //            {
        //                if (sInspTypeDesc.Contains("TENANT"))
        //                {
        //                    sRet = "7116-CT";
        //                }
        //                else
        //                {
        //                    sRet = "7116-CLR";
        //                }
        //            }
        //            else if (sInspType.Contains("OCCUPIED BY"))
        //            {
        //                if (sInspType.Contains("TENANT"))
        //                {
        //                    sRet = "7116-CT";
        //                }
        //                else
        //                {
        //                    sRet = "7116-CLR";
        //                }
        //            }




        //        }   // THB Florida

        //    }

        //    return sRet;
        //}



        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import TowerHill Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import TowerHill Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }
    }
}
