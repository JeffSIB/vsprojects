﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace CL_ValuationUtils
{
    public partial class ResCreate : System.Web.UI.Page
    {
        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        //static string cfg_360ConnStr;
        static string cfg_360UtilConnStr;

        static int iCaseNum = 0;
        static string sPolicyNum = "";
        static string sInsuredName = "";
        static string sStreetAdd = "";
        static string sCity = "";
        static string sState = "";
        static string sZip = "";
        static decimal dCoverageA;
        static int iCoverageA;
        static int iStatus = 0;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");
            //cfg_360ConnStr = colNameVal.Get("360ConnStr");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");

            string sVal = Request.QueryString["val"];
            hiVal.Value = sVal;
            tblMode1.Visible = false;
            tblMode2.Visible = false;
            tblMode3.Visible = false;
            trError3.Visible = false;

            if (sVal == null)
            {
                tblMode1.Visible = true;
                lblMode1Msg.Text = "Enter the case number for the valuation and click lookup:";
            }
            else if (sVal == "1")
            {
                tblMode2.Visible = true;
                lblMode2Msg.Text = "Creating valuation for the following case:";
            }
            else if (sVal == "2")
            {
                tblMode3.Visible = true;
                lblMode3Msg.Text = "Valuation created successfully";
            }

        }

        protected void bSubmit1_Click(object sender, EventArgs e)
        {
            string sErrMsg = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {
                if (tbCaseNum.Text.Length < 7)
                {
                    sErrMsg = "Please enter a valid case number.";
                    throw new SystemException("");
                }

                iCaseNum = Convert.ToInt32(tbCaseNum.Text);

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetCaseInfoValuation";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", iCaseNum);

                sqlConn1.Open();

                // Get case info
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through results
                    do
                    {
                        //case number
                        if (sqlReader.IsDBNull(0))
                        {
                            throw new SystemException("SQL returned empty Case#");
                        }
                        else
                        {
                            iCaseNum = (int)sqlReader.GetSqlInt32(0);
                        }

                        // policy #
                        sPolicyNum = sqlReader.GetSqlString(1).ToString();

                        // name
                        sInsuredName = sqlReader.GetSqlString(2).ToString();
                        if (sInsuredName.Length > 49)   // 50 char max insured name
                        {
                            sInsuredName = sInsuredName.Substring(0, 49);
                        }

                        // Address
                        sStreetAdd = sqlReader.GetSqlString(3).ToString();

                        // City
                        sCity = sqlReader.GetSqlString(4).ToString();

                        // State
                        sState = sqlReader.GetSqlString(5).ToString();

                        // Zip
                        sZip = sqlReader.GetSqlString(6).ToString();

                        // Coverage A
                        dCoverageA = (decimal)sqlReader.GetSqlMoney(7);
                        iCoverageA = Convert.ToInt32(dCoverageA);

                        // Case status
                        iStatus = (int)sqlReader.GetSqlInt32(8);


                    } while (sqlReader.Read());

                    sqlReader.Close();
                    sqlConn1.Close();

                }   // has rows
                else
                {
                    sErrMsg = "Unable to locate case number. Please verify you have input the correct value.";
                    throw new SystemException("No rows returned for: " + tbCaseNum.Text);
                }

                // break if case has been completed
                if (iStatus == 6 || iStatus == 7)
                {
                    sErrMsg = "This case has already been completed.";
                    throw new SystemException("Inspection completed: " + tbCaseNum.Text);
                }


                hiVal.Value = "2";
                tblMode1.Visible = false;
                tblMode2.Visible = true;
                tbCaseNum2.Text = iCaseNum.ToString();
                tbInsured2.Text = sInsuredName;
                lblMode2Msg.Text = "Create valuation for the following case:";
                
            }

            catch (Exception ex)
            {

                if (sErrMsg.Length > 0)
                {
                    lbl1Status.Text = "&nbsp; &nbsp;" + sErrMsg + "&nbsp;&nbsp;";
                }
                else
                {
                    lbl1Status.Text = "&nbsp;&nbsp;An unexpected error has occurred - please contact Sutton support.&nbsp;&nbsp;";
                }

                //record exception  
                string sErr = "Error attempting to access case: " + tbCaseNum.Text + System.Environment.NewLine;

                WriteToLog(sErr + ex.Message);

            }

            finally
            {

                if (sqlReader != null)
                    sqlReader.Close();
                if (sqlConn1 != null)
                    sqlConn1.Close();

            }
        }

        protected void bSubmit2_Click(object sender, EventArgs e)
        {

            try
            {
                // Create valuation
                RCT4Utils.RCT4 oRCT4 = new RCT4Utils.RCT4();

                oRCT4.APIUserName = "Sutton_GFT";
                oRCT4.APIPassword = "Welcome18";
                oRCT4.UserLogin = "MLSutton";
                oRCT4.InsuredName = sInsuredName;
                oRCT4.Policy = sPolicyNum;
                oRCT4.StreetAddress = sStreetAdd;
                oRCT4.City = sCity;
                oRCT4.State = sState;
                oRCT4.PostalCode = sZip;
                oRCT4.ValuationUID = iCaseNum.ToString();
                oRCT4.CoverageAIn = iCoverageA;

                string sRetVal = oRCT4.CreateValuation();

                hiVal.Value = "3";
                tblMode1.Visible = false;
                tblMode2.Visible = false;
                tblMode3.Visible = true;
                tbCaseNum3.Text = iCaseNum.ToString();
                tbInsured3.Text = sInsuredName;
                lblMode3Msg.Text = "Creating valuation for the following case:";


                if (sRetVal.Contains("ERROR"))
                {
                    lbl3Status.BackColor = System.Drawing.Color.Maroon;
                    lbl3Status.ForeColor = System.Drawing.Color.White;
                    lbl3Status.Text = "  Valuation could not be created  ";
                    trError3.Visible = true;
                    tbErr3.Text = sRetVal;
                    WriteToLog("Error creating valuation for: " + tbCaseNum.Text + System.Environment.NewLine +  sRetVal);

                }
                else
                {
                    lbl3Status.BackColor = System.Drawing.Color.DarkGreen;
                    lbl3Status.ForeColor = System.Drawing.Color.White;
                    lbl3Status.Text = "Valuation created successfully";
                    WriteToLog("Valuation created for: " + tbCaseNum.Text);
                }
            }
            catch (Exception ex)
            {

                lbl3Status.BackColor = System.Drawing.Color.Maroon;
                lbl3Status.ForeColor = System.Drawing.Color.White;
                lbl3Status.Text = "&nbsp;&nbsp;An unexpected error has occurred - please contact Sutton support.&nbsp;&nbsp;";
               
                //record exception  
                string sErr = "Error creating case: " + tbCaseNum.Text + System.Environment.NewLine;

                WriteToLog(sErr + ex.Message);

            }
        }

        private void WriteToLog(string sMessage)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();
            oLU.WritetoLog(sMessage);
            oLU.closeLog();
            oLU = null;
        }

        protected void bClose_Click(object sender, EventArgs e)
        {
            Server.Transfer("Main.aspx");
        }
    }
}