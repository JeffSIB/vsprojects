﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="CommCreate.aspx.cs" Inherits="CL_ValuationUtils.CommCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        function setfocus() {
            var ref = document.getElementById('hiRedir').value;
            if (ref.length > 0) {
                location.href = ref;

            }
            else {
                document.getElementById('tbCaseNum').focus();
            }
        }
        function cancelForm() {
            location.href = 'Main.aspx';
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="divHead" style="padding: 10px; margin: 20px; border: 1px solid #808080;">
        <table style="width: 100%;">
            <tr>
                <td style="width: 80%">
                    <asp:Label ID="Label3" runat="server" Text="Create a base CoreLogic commercial valuation" Font-Size="14pt" ForeColor="Black" Font-Bold="False" Font-Names="Arial"></asp:Label>
                </td>
                <td style="width: 20%; text-align: center;">
                    <input onclick="cancelForm()" tabindex="500" type="button" value="Cancel"
                        name="bCancel" />
                </td>

            </tr>
        </table>
    </div>
    <div id="divBody" style="padding: 10px; margin: 20px;">

        <table id="tblMode1" runat="server" style="width: 100%; border: 0; border-spacing: 5px; padding: 2px">
            <tr>
                <td style="width: 10%"></td>
                <td style="width: 90%"></td>
            </tr>
            <tr>
                <td style="text-align: left" colspan="2">
                    <asp:Label ID="lblMode1Msg" runat="server" CssClass="StdTextSmallGrey">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
                <td style="vertical-align: central">
                    <asp:Label ID="Label1" runat="server">Case Number:</asp:Label>&nbsp;&nbsp;
                <asp:TextBox ID="tbCaseNum" runat="server" Width="100px"></asp:TextBox>&nbsp;&nbsp;
                <asp:Button ID="bSubmit1" runat="server" Text="Lookup" OnClick="bSubmit1_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Label ID="lbl1Status" runat="server" ForeColor="White" BackColor="#CC0000">
                    </asp:Label>
                </td>
            </tr>
        </table>

        <table id="tblMode2"  runat="server" style="width: 100%; border: 0; border-spacing: 5px; padding: 2px">
           <tr>
                <td style="width: 10%"></td>
                <td style="width: 90%"></td>
            </tr>
            <tr>
                <td style="text-align: left" colspan="2">
                    <asp:Label ID="lblMode2Msg" runat="server" CssClass="StdTextSmallGrey">
                    </asp:Label>
                </td>
            </tr>            
            <tr>
                <td style="width: 100%;">
                    <table style="width: 100%; border: 0; border-spacing: 5px; padding: 2px">
                        <tr>
                            <td style="width: 20%">
                                <asp:Label ID="Label2" runat="server">Case Number:</asp:Label>
                            </td>
                            <td style="width: 80%">
                                <asp:TextBox ID="tbCaseNum2" runat="server" ReadOnly="true" Width="90%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server">Insured:</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="tbInsured2" runat="server" ReadOnly="true" Width="90%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; background-color: #CCCCCC;">
                    <asp:Label ID="lbl2Status" runat="server" BackColor="#CCCCCC">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; vertical-align: middle" class="StdText">
                    <asp:Button ID="bSubmit2" runat="server" Text="Create Valuation" OnClick="bSubmit2_Click" />
                </td>
            </tr>
        </table>

       <table id="tblMode3"  runat="server" style="width: 100%; border: 0; border-spacing: 5px; padding: 2px">
           <tr>
                <td style="width: 10%"></td>
                <td style="width: 90%"></td>
            </tr>
            <tr>
                <td style="text-align: left" colspan="2">
                    <asp:Label ID="lblMode3Msg" runat="server" CssClass="StdTextSmallGrey">
                    </asp:Label>
                </td>
            </tr>            
            <tr>
                <td style="width: 100%;">
                    <table style="width: 100%; border: 0; border-spacing: 5px; padding: 2px">
                        <tr>
                            <td style="width: 20%">
                                <asp:Label ID="Label6" runat="server">Case Number:</asp:Label>
                            </td>
                            <td style="width: 80%">
                                <asp:TextBox ID="tbCaseNum3" runat="server" ReadOnly="true" Width="90%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server">Insured:</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="tbInsured3" runat="server" ReadOnly="true" Width="90%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; background-color: #CCCCCC;">
                    <asp:Label ID="lbl3Status" runat="server" BackColor="#CCCCCC">
                    </asp:Label>
                </td>
            </tr>
            <tr id="trError3" runat="server" style="height:100px">
                <td colspan="2" style="text-align: center; background-color: #FFCC00;">
                    <asp:TextBox ID="tbErr3" runat="server" BorderStyle="None" BackColor="#FFCC00" Height="95px" Width="95%" ReadOnly="true" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; vertical-align: middle" class="StdText">
                    <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="bClose_Click" />
                </td>
            </tr>
        </table>
        <input id="hiVal" type="hidden" runat="server" name="hiVal" />
        <input id="hiRedir" type="hidden" runat="server" />
    </div>
</asp:Content>
