﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="CL_ValuationUtils.Main" %>

<%@ MasterType VirtualPath="~/SiteMaster.master" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
    </script>
    <title>Sutton Inspection Bureau - Home</title>
</asp:Content>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="divCreate" style="padding: 10px; margin: 20px; border: 1px solid #808080;">
       <table style="width: 100%;">
            <tr>
                <td style="width: 100%">
                    <asp:Label ID="Label3" runat="server" Text="Create a base valuation in CoreLogic" Font-Size="14pt" ForeColor="Black" Font-Bold="False" Font-Names="Arial"></asp:Label>
                </td>
            </tr>
        </table>

        <table style="width: 100%;">
            <tr>
                <td style="width: 20%">&nbsp;</td>
                <td style="width: 80%">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="text-align: left;">
                    <asp:Label ID="Label1" runat="server" Text="Select a valuation type:" Font-Size="14pt"></asp:Label>
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td style="width: 40%">&nbsp;</td>
                <td style="text-align: left; width: 60%">
                    <asp:LinkButton ID="lbRes" CssClass="menubutton" runat="server" OnClick="lbRes_Click">Residential</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:LinkButton ID="lbComm" CssClass="menubutton" runat="server" Width="200px" OnClick="lbComm_Click">Commercial</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
