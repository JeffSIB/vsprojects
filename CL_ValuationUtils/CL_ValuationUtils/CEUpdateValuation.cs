﻿using CL_ValuationUtils.IValuationService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CL_ValuationUtils
{
    class CEUpdateValuation
    {
        private string _contextUserName = ConfigurationManager.AppSettings["ContextUserName"];
        private string _contextUserPassword = ConfigurationManager.AppSettings["ContextUserPassword"];
        private string _contextAlias = ConfigurationManager.AppSettings["ContextAlias"];
        private Guid _contextCompanyId = new Guid(ConfigurationManager.AppSettings["CompanyId"]);
        private string _contextLogonUser = ConfigurationManager.AppSettings["LogonUser"];

        private ChannelFactory<IValuation> _channelFactory = new ChannelFactory<IValuation>("ClearUsernameBinding_IValuation");
        private StandardResponse _response;

        public CEUpdateValuation()
        {
            _channelFactory.Credentials.UserName.UserName = _contextUserName + "@" + _contextAlias;
            _channelFactory.Credentials.UserName.Password = _contextUserPassword;
        }

        public void UpdateValuation(string newPolicyNumber, ResponseGetValuation getValResp)
        {
            string sErr = "";

            IValuation ivaluation = _channelFactory.CreateChannel();

            UpdateValuationRequest request = new UpdateValuationRequest();

            request.CompanyID = _contextCompanyId;
            request.Logon = _contextLogonUser;

            request.ValuationID = getValResp.Valuation.ID.Value;

            request.Valuation = getValResp.Valuation;

            request.Valuation.ValuationNumber = newPolicyNumber;

            try
            {
                _response = ivaluation.UpdateValuation(request);

                if ((_response.Errors.Length > 0))
                {
                    foreach (ErrorDetail error in _response.Errors)
                    {
                        sErr += error.Description + System.Environment.NewLine;
                    }
                }
                else
                {
                    if (_response.Status == ResponseStatusType.Success)
                    {
                        sErr = "";
                    }
                }

                if (sErr.Length > 0)
                {
                    throw new Exception(sErr);
                }

            }
            catch (Exception ex)
            {
                // pass error back to calling procedure
                throw ex;
            }
        }
    }
}
