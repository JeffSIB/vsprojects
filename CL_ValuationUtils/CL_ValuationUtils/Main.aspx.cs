﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CL_ValuationUtils
{
    public partial class Main : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void lbRes_Click(object sender, EventArgs e)
        {
            //Server.Transfer("ResCreate.aspx");
            Response.Redirect("ResCreate.aspx");
        }

        protected void lbComm_Click(object sender, EventArgs e)
        {
            Server.Transfer("CommCreate.aspx");
        }

        protected void lbCommFull_Click(object sender, EventArgs e)
        {
            Server.Transfer("CommCreateFull.aspx");
        }

    }
}