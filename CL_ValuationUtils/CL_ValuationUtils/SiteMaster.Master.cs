﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CL_ValuationUtils
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string thispage = this.Page.AppRelativeVirtualPath;
            int pos = thispage.LastIndexOf('/');
            string pagename = thispage.Substring(pos + 1);
            pos = thispage.LastIndexOf('.');
            pagename = pagename.Substring(0, pos - 2);
            pagename = pagename.ToLower();
            hicurPage.Value = pagename;
        }

        protected void rescreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("ResCreate.aspx");
        }

        protected void commcreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("CommCreate.aspx");
        }
    }
}