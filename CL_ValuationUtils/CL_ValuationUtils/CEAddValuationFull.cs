﻿using CL_ValuationUtils.IValuationService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CL_ValuationUtils
{
    class CEAddValuationFull
    {
        private string _contextUserName = ConfigurationManager.AppSettings["ContextUserName"];
        private string _contextUserPassword = ConfigurationManager.AppSettings["ContextUserPassword"];
        private string _contextAlias = ConfigurationManager.AppSettings["ContextAlias"];
        private Guid _contextCompanyId = new Guid(ConfigurationManager.AppSettings["CompanyId"]);
        private string _contextLogonUser = ConfigurationManager.AppSettings["LogonUser"];

        //private ChannelFactory<IValuation> _channelFactory = new ChannelFactory<IValuation>("ClearUsernameBinding_IValuation");

        public string CaseNumber { get; set; }
        public string BusinessName { get; set; }
        public string LocationAddress1 { get; set; }
        public string LocationAddress2 { get; set; }
        public string LocationCity { get; set; }
        public string LocationState { get; set; }
        public string LocationZip { get; set; }
        public int YearBuilt { get; set; }
        public int Stories { get; set; }
        public int GrossFloorArea { get; set; }
        public string ConstCode { get; set; }
        public int ConstPct { get; set; }
        public int OccupancyCode { get; set; }
        public int OccupancyPct { get; set; }


        public CEAddValuationFull()
        {
            //_channelFactory.Credentials.UserName.UserName = _contextUserName + "@" + _contextAlias;
            //_channelFactory.Credentials.UserName.Password = _contextUserPassword;
        }

        public long CEAddFullValuation()
        {
            string sErr = "";

            ChannelFactory<IValuation> channelFactory = new ChannelFactory<IValuation>("ClearUsernameBinding_IValuation");

            //channelFactory.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AdminUser"]
            //        + "@" + ConfigurationManager.AppSettings["CompanyAlias"];
            //channelFactory.Credentials.UserName.Password = ConfigurationManager.AppSettings["Password"];

            channelFactory.Credentials.UserName.UserName = _contextUserName + "@" + _contextAlias;
            channelFactory.Credentials.UserName.Password = _contextUserPassword;

            IValuation iValuation = channelFactory.CreateChannel();

            AddValuationRequest request = new AddValuationRequest();
            //request.CompanyID = new Guid(ConfigurationManager.AppSettings["CompanyId"]);
            //request.Logon = ConfigurationManager.AppSettings["AdminUser"];

            request.CompanyID = _contextCompanyId;
            request.Logon = _contextLogonUser;

            request.CalculateValuation = false;
            request.ReturnAddedValuation = true;

            request.Valuation = new Valuation();
            request.Valuation.AssignedUser = _contextLogonUser;
            request.Valuation.ValuationNumber = CaseNumber;
            //request.Valuation.ValuationNumber = "Test10062090-1";


            //Business Address is not required, but optional.
            request.Valuation.Business = new Business();
            request.Valuation.Business.Name = BusinessName;
            request.Valuation.Business.Address = new Address();
            request.Valuation.Business.Address.Line1 = LocationAddress1;
            request.Valuation.Business.Address.City = LocationCity;
            request.Valuation.Business.Address.PostalCode = LocationZip;
            request.Valuation.Business.Address.RegionCode = LocationState;

            request.Valuation.EffectiveDate = DateTime.Now;

            //Single location.
            request.Valuation.Locations = new Location[1];
            request.Valuation.Locations[0] = new Location();
            request.Valuation.Locations[0].Address = new Address();
            request.Valuation.Locations[0].Address.Line1 = LocationAddress1;
            request.Valuation.Locations[0].Address.City = LocationCity;
            request.Valuation.Locations[0].Address.PostalCode = LocationZip;
            request.Valuation.Locations[0].Address.RegionCode = LocationState;

            request.Valuation.Locations[0].IsHeadquarters = true;

            //Single building.
            request.Valuation.Locations[0].Buildings = new Building[1];
            request.Valuation.Locations[0].Buildings[0] = new Building();
            request.Valuation.Locations[0].Buildings[0].Name = "Building 1"; 
            request.Valuation.Locations[0].Buildings[0].Type = BuildingType.Commercial;

            //Single section.
            request.Valuation.Locations[0].Buildings[0].Sections = new BuildingSection[1];
            request.Valuation.Locations[0].Buildings[0].Sections[0] = new BuildingSection();
            request.Valuation.Locations[0].Buildings[0].Sections[0].Name = "Section 1"; //Optional
            request.Valuation.Locations[0].Buildings[0].Sections[0].YearBuilt = YearBuilt;
            request.Valuation.Locations[0].Buildings[0].Sections[0].Stories = Stories;
            request.Valuation.Locations[0].Buildings[0].Sections[0].GrossFloorArea = Convert.ToDecimal(GrossFloorArea);

            // Construction quality left at default (Mary)
            //request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionQualityCode = "2.0";

            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes = new ConstructionTypeCollection();

            // Everythign defaults to Commercial
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes.BuildingType = BuildingType.Commercial;
        
            //Single construction type.
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes.ConstructionTypes = new ConstructionType[1];
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes.ConstructionTypes[0] = new ConstructionType();
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes.ConstructionTypes[0].ConstructionCode = ConstCode; 
            request.Valuation.Locations[0].Buildings[0].Sections[0].ConstructionTypes.ConstructionTypes[0].Percent = ConstPct;

            //Single occupancy
            request.Valuation.Locations[0].Buildings[0].Sections[0].Occupancies = new OccupancyTypeCollection();
            request.Valuation.Locations[0].Buildings[0].Sections[0].Occupancies.Occupancies = new OccupancyType[1];
            request.Valuation.Locations[0].Buildings[0].Sections[0].Occupancies.Occupancies[0] = new OccupancyType();
            request.Valuation.Locations[0].Buildings[0].Sections[0].Occupancies.Occupancies[0].Code = OccupancyCode;
            request.Valuation.Locations[0].Buildings[0].Sections[0].Occupancies.Occupancies[0].Percent = OccupancyPct;

            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials = new SectionMaterial[1];
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0] = new SectionMaterial();
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0].GroupID = 46;
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0].ItemID = 283;
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0].Value = 3;
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0].Type = 2;
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0].ValueType = UnitOfMeasure.Count;

            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials = new SectionMaterial[1];
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0] = new SectionMaterial();
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0].GroupID = 17;
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0].ItemID = 199;
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0].Value = 100;
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0].Type = 1;
            //request.Valuation.Locations[0].Buildings[0].Sections[0].Materials[0].ValueType = UnitOfMeasure.Percentage;

             

            try
            {
                ResponseNewID response = iValuation.AddValuation(request);

                if ((response.Errors.Length > 0))
                {
                    foreach (ErrorDetail error in response.Errors)
                    {
                        sErr += error.Description + System.Environment.NewLine;
                    }
                }
                else
                {
                    return response.NewID;
                }

                if (sErr.Length > 0)
                {
                    throw new Exception(sErr);
                }
            }
            catch (Exception ex)
            {
                // pass error back to calling procedure
                throw ex;
            }

            return 0; //0 = unsuccessful response

        }

        //public long CEAddFullValuation()
        //{
        //    // create Full valuation
        //    // returns internal valuation number - 0 on failure
        //    // throws exception to calling proc


        //    //Locations
        //    //Location

        //    //Address
        //    //Address

        //    //Buildings
        //    //Building
        //    //Sections
        //    //BuildingSection
        //    //ConstructionTypes (collection)
        //    //BuildingType
        //    //ConstructionTypes (collection)
        //    //ConstructionType
        //    //ConstructionType
        //    //ConstructionTypes
        //    //ConstructionTypes
        //    //BuildingSection
        //    //Sections
        //    //Building
        //    //Buildings


        //    //Location
        //    //Locations

        //    //IReference iReference = new IReference();
 
        //    string sErr = "";

        //   // IValuation ivaluation = _channelFactory.CreateChannel();
            
        //    AddValuationRequest request = new AddValuationRequest();

        //    request.CompanyID = _contextCompanyId;
        //    request.Logon = _contextLogonUser;

        //    request.Valuation = new Valuation();

        //    request.Valuation.Business = new Business();
        //    request.Valuation.Business.Address = new Address();

        //    request.Valuation.Business.Address.Line1 = LocationAddress1;
        //    request.Valuation.Business.Address.City = LocationCity;
        //    request.Valuation.Business.Address.RegionCode = LocationState;
        //    request.Valuation.Business.Address.PostalCode = LocationZip;
        //    request.Valuation.Business.Name = BusinessName;

        //    request.Valuation.EffectiveDate = DateTime.Now;

        //    // Location
        //    Location location1 = new Location();
        //    location1.Address = new Address();
        //    location1.Address.Line1 = LocationAddress1;
        //    location1.Address.City = LocationCity;
        //    location1.Address.RegionCode = LocationState;
        //    location1.Address.PostalCode = LocationZip;

        //    //Locations
        //    Location[] locations = new Location[1];
        //    locations[0] = location1;

        //    // Building
        //    Building building1 = new Building();
        //    BuildingSection buildingSection1 = new BuildingSection();

        //    // Sq Ft, Year built, Stories
        //    buildingSection1.GrossFloorArea = Convert.ToDecimal(GrossFloorArea);
        //    buildingSection1.YearBuilt = YearBuilt;
        //    buildingSection1.Stories = Stories;

        //    // Inner Construction Type
        //    ConstructionType constType1 = new ConstructionType();
        //    constType1.ConstructionCode = ConstCode;
        //    constType1.Percent = ConstPct;

        //    // Inner Construction Types
        //    ConstructionType[] ConstTypeCollInner = new ConstructionType[1];
        //    ConstTypeCollInner[0] = constType1;

        //    //Outer construction type
        //    ConstructionTypeCollection constTypeCollOuter = new ConstructionTypeCollection();
        //    BuildingType buildingType1 = BuildingType.Commercial;
        //    constTypeCollOuter.BuildingType = buildingType1;
        //    constTypeCollOuter.ConstructionTypes = ConstTypeCollInner;
            
        //    // Occupancies            
        //    OccupancyType OccType = new OccupancyType();
        //    OccType.Code = OccupancyCode;
        //    OccType.Percent = OccupancyPct;

        //    OccupancyType[] occTypes = new OccupancyType[1];
        //    occTypes[0] = OccType;

        //    OccupancyTypeCollection occupanciesColl = new OccupancyTypeCollection();
        //    occupanciesColl.Occupancies = occTypes;

        //    buildingSection1.Occupancies = occupanciesColl;
            

        //    // Add Construction types to Building Section
        //    buildingSection1.ConstructionTypes = constTypeCollOuter;

        //    //Add Building Section to Sections
        //    BuildingSection[] buildingSections = new BuildingSection[1];
        //    buildingSections[0] = buildingSection1;

        //    // Add sections to building
        //    building1.Sections = buildingSections;

        //    // Add building to Buildings
        //    Building[] buildings = new Building[1];
        //    buildings[0] = building1;
        //    location1.Buildings = buildings;
        //    location1.IsHeadquarters = true;

        //    request.Valuation.Locations = locations;
        //    request.Valuation.ValuationNumber = CaseNumber; 

        //    try
        //    {
        //        ResponseNewID response = ivaluation.AddValuation(request);

        //        if ((response.Errors.Length > 0))
        //        {
        //            foreach (ErrorDetail error in response.Errors)
        //            {
        //                sErr += error.Description + System.Environment.NewLine;
        //            }
        //        }
        //        else
        //        {
        //            return response.NewID;
        //        }

        //        if (sErr.Length > 0)
        //        {
        //            throw new Exception(sErr);
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        // pass error back to calling procedure
        //        throw ex;
        //    }

        //    return 0; //0 = unsuccessful response
        //}
       

    }
}
