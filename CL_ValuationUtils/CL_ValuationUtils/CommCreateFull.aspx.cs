﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace CL_ValuationUtils
{
    public partial class CommCreateFull : System.Web.UI.Page
    {
        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;

        static int iCaseNum = 0;
        static Guid guCaseID;
        static string sPolicyNum = "";
        static string sInsuredName = "";
        static string sStreetAdd = "";
        static string sCity = "";
        static string sState = "";
        static string sZip = "";
        static decimal dCoverageA;
        static int iCoverageA;
        static int iStatus = 0;
        static int iNumStoriesOwner = 0;
        static int iNumStoriesTenant = 0;
        static int iYearBuilt = 0;
        static int iStories = 0;
        static int iGrossFloorArea = 0;
        static string sConstruction = "";
        static string sConstCode = "";  //ISO
        static int iConstPct = 0;
        static string sBusinessType = "";
        static string sCommSubType = "";
        static string sAgSubType = "";
        static int iOccupancyCode = 0;
        static int iOccupancyPct = 100;
        static string sOCIndustrial = "";
        static string sOCFamily = "";
        static string sOCLodging = "";
        static string sOCMercantile = "";
        static string sOCPublic = "";
        static string sOCProfessional = "";
        static string sOCOffice = "";
        static string sOCRestaurant = "";
        static string sOCService = "";
        static string sFoundation = "";
        static string sElectrical = "";
        static string sSprinkler = "";
        static string sFireAlarm = "";
        static string sElevator = "";
        static string sHVAC = "";
        static string sWallConst = "";
        static string sRoofCover = "";



        //static string sFieldRepNum = "";
        //static string sFieldRepFName = "";
        //static string sFieldRepLName = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");

            string sVal = Request.QueryString["val"];
            hiVal.Value = sVal;
            tblMode1.Visible = false;
            tblMode2.Visible = false;
            tblMode3.Visible = false;
            trError3.Visible = false;

            if (sVal == null)
            {
                tblMode1.Visible = true;
                lblMode1Msg.Text = "Enter the inspection number for the valuation and click lookup:";
            }
            else if (sVal == "1")
            {
                tblMode2.Visible = true;
                lblMode2Msg.Text = "Creating valuation for the following inspection:";
            }
            else if (sVal == "2")
            {
                tblMode3.Visible = true;
                lblMode3Msg.Text = "Valuation created successfully";
            }

        }

        protected void bSubmit1_Click(object sender, EventArgs e)
        {
            string sErrMsg = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {
                if (tbCaseNum.Text.Length < 7)
                {
                    sErrMsg = "Please enter a valid inspection number.";
                    throw new SystemException("");
                }

                iCaseNum = Convert.ToInt32(tbCaseNum.Text);

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetCaseInfoValuation";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", iCaseNum);

                sqlConn1.Open();

                // Get case info
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through results
                    do
                    {
                        //case number
                        if (sqlReader.IsDBNull(0))
                        {
                            throw new SystemException("SQL returned empty Case#");
                        }
                        else
                        {
                            iCaseNum = (int)sqlReader.GetSqlInt32(0);
                        }

                        // policy #
                        sPolicyNum = sqlReader.GetSqlString(1).ToString();

                        // name
                        sInsuredName = sqlReader.GetSqlString(2).ToString();
                        if (sInsuredName.Length > 49)   // 50 char max insured name
                        {
                            sInsuredName = sInsuredName.Substring(0, 49);
                        }

                        // Address
                        sStreetAdd = sqlReader.GetSqlString(3).ToString();

                        // City
                        sCity = sqlReader.GetSqlString(4).ToString();

                        // State
                        sState = sqlReader.GetSqlString(5).ToString();

                        // Zip
                        sZip = sqlReader.GetSqlString(6).ToString();

                        // Coverage A
                        dCoverageA = (decimal)sqlReader.GetSqlMoney(7);
                        iCoverageA = Convert.ToInt32(dCoverageA);

                        // Case status
                        iStatus = (int)sqlReader.GetSqlInt32(8);

                        //// Insp Number
                        //sFieldRepNum = sqlReader.GetSqlString(9).ToString();

                        //// Insp first name
                        //sFieldRepFName = sqlReader.GetSqlString(10).ToString();

                        ////Insp last name
                        //sFieldRepLName = sqlReader.GetSqlString(11).ToString();

                        // CaseID
                        guCaseID = (Guid)sqlReader.GetGuid(12);


                    } while (sqlReader.Read());

                    sqlReader.Close();
                    sqlConn1.Close();

                }   // has rows
                else
                {
                    sErrMsg = "Unable to locate inspection. Please verify you have entered the correct case number.";
                    throw new SystemException("No rows returned for: " + tbCaseNum.Text);
                }

                // break if case has been completed
                if (iStatus == 6 || iStatus == 7)
                {
                    sErrMsg = "This inspection has already been completed.";
                    throw new SystemException("Inspection completed: " + tbCaseNum.Text);
                }



                //// break if inspector information incomplete
                //if (sFieldRepNum.Length < 1 || sFieldRepFName.Length < 1 || sFieldRepLName.Length < 1)
                //{
                //    sErrMsg = "Incomplete inspector information.";
                //    throw new SystemException("Incomplete insp info: " + tbCaseNum.Text + " Insp#: " + sFieldRepNum + " FName: " + sFieldRepFName + " LName: " + sFieldRepLName);
                //}

                hiVal.Value = "2";
                tblMode1.Visible = false;
                tblMode2.Visible = true;
                tbCaseNum2.Text = iCaseNum.ToString();
                tbInsured2.Text = sInsuredName;
                lblMode2Msg.Text = "Create valuation for the following inspection:";

            }

            catch (Exception ex)
            {

                if (sErrMsg.Length > 0)
                {
                    lbl1Status.Text = "&nbsp; &nbsp;" + sErrMsg + "&nbsp;&nbsp;";
                }
                else
                {
                    lbl1Status.Text = "&nbsp;&nbsp;An unexpected error has occurred - please contact Sutton support.&nbsp;&nbsp;";
                }

                //record exception  
                string sErr = "Error attempting to access inspection: " + tbCaseNum.Text + System.Environment.NewLine;

                WriteToLog(sErr + ex.Message);

            }

            finally
            {

                if (sqlReader != null)
                    sqlReader.Close();
                if (sqlConn1 != null)
                    sqlConn1.Close();

            }
        }

        protected void bSubmit2_Click(object sender, EventArgs e)
        {
            string sErrMsg = "";

            hiVal.Value = "3";
            tblMode1.Visible = false;
            tblMode2.Visible = false;
            tblMode3.Visible = true;
            tbCaseNum3.Text = iCaseNum.ToString();
            tbInsured3.Text = sInsuredName;
            lblMode3Msg.Text = "Creating valuation for the following inspection:";

            WriteToLog("Creating valuation for: " + tbCaseNum.Text);

            try
            {

                // Get FormInstance ID for BVS Shared Fields v11.23.20 
                // If nothing is returned there is no BVS form on case
                //sp_GetBVSForm
                string sFormInstance = getBVSFormInstance(iCaseNum);
                if (sFormInstance == "")
                {
                    sErrMsg = "Unable to locate the BVS form on this case./r/n/r/nThe BVS Form must be saved prior to running this procedure.";
                    throw new Exception("No BVS form on case: " + iCaseNum.ToString());
                }

                // pull values from BVS form
                string sRetVal = getBVSFormData(sFormInstance);
                if (sRetVal != "")
                {
                    sErrMsg = sRetVal;
                    throw new Exception("Error retrieving BVS form data: " + iCaseNum.ToString());
                }

                // Validate data
                int iCurYear = DateTime.Now.Year;
                if (iYearBuilt < 1800 || iYearBuilt > iCurYear)
                {
                    sErrMsg = "Year built is missing or invalid";
                    throw new Exception("Year built is missing or invalid: " + iCaseNum.ToString());
                }

                // Get number of stories
                // **** PUT IN LOGIC TO HANDLE BOTH HAVING VALUE
                if (iNumStoriesOwner > 0)
                    iStories = iNumStoriesOwner;
                if (iNumStoriesTenant > 0)
                    iStories = iNumStoriesTenant;

                if (iStories < 1 || iStories > 100)
                {
                    sErrMsg = "Number of stories is missing or invalid";
                    throw new Exception("Number of stories is missing or invalid: " + iCaseNum.ToString());
                }

                if (iGrossFloorArea < 1)
                {
                    sErrMsg = "Gross floor area is required";
                    throw new Exception("Gross floor area is required: " + iCaseNum.ToString());
                }

                if (sBusinessType == "")
                {
                    sErrMsg = "Business type is required";
                    throw new Exception("Business type is required: " + iCaseNum.ToString());
                }

                // Commercial business type
                if (sBusinessType == "Commercial")
                {

                    if (sCommSubType == "")
                    {
                        sErrMsg = "Occupancy type is required";
                        throw new Exception("Occupancy code is required: " + iCaseNum.ToString());
                    }

                    if (sCommSubType.Contains("Industrial"))
                    {
                        switch (sOCIndustrial)
                        {
                            case "ACHANG":
                                iOccupancyCode = 7150;
                                break;
                            case "BOATSTOR":
                                iOccupancyCode = 7131;
                                break;
                            case "MANUFH":
                                iOccupancyCode = 8210;
                                break;
                            case "MANUFL":
                                iOccupancyCode = 8100;
                                break;
                            case "MPP":
                                iOccupancyCode = 9060;
                                break;
                            case "STH":
                                iOccupancyCode = 7151;
                                break;
                            case "WH":
                                iOccupancyCode = 8200;
                                break;
                            case "WHL":
                                iOccupancyCode = 8200;
                                break;
                            case "WHMS":
                                iOccupancyCode = 7310;
                                break;
                            case "WHMSHR":
                                iOccupancyCode = 7315;
                                break;
                            case "WHSS":
                                iOccupancyCode = 7300;
                                break;
                            default:
                                iOccupancyCode = 0;
                                break;

                        }
                    }
                    else if (sCommSubType.Contains("Lodging"))
                    {
                        switch (sOCLodging)
                        {
                            case "APT":
                                iOccupancyCode = 1230;
                                break;
                            case "APTHR":
                                iOccupancyCode = 1200;
                                break;
                            case "APTLR":
                                iOccupancyCode = 1100;
                                break;
                            case "BANDB":
                                iOccupancyCode = 1460;
                                break;
                            case "BOARDH":
                                iOccupancyCode = 1640;
                                break;
                            case "CTASSOC":
                                iOccupancyCode = 1335;
                                break;
                            case "COOP":
                                iOccupancyCode = 1335;
                                break;
                            case "FRAT":
                                iOccupancyCode = 1445;
                                break;
                            case "HOA":
                                iOccupancyCode = 4210;
                                break;
                            case "HOTLFS":
                                iOccupancyCode = 1500;
                                break;
                            case "HOTLLS":
                                iOccupancyCode = 1550;
                                break;
                            case "MHP":
                                iOccupancyCode = 4210;
                                break;
                            case "TSHARE":
                                iOccupancyCode = 1335;
                                break;
                            default:
                                iOccupancyCode = 0;
                                break;

                        }
                    }
                    else if (sCommSubType.Contains("Mercantile"))
                    {
                        switch (sOCMercantile)
                        {
                            case "BARB":
                                iOccupancyCode = 3105;
                                break;
                            case "BEAUT":
                                iOccupancyCode = 3100;
                                break;
                            case "CONV":
                                iOccupancyCode = 3500;
                                break;
                            case "DSPA":
                                iOccupancyCode = 3100;
                                break;
                            case "FURN":
                                iOccupancyCode = 3600;
                                break;
                            case "LAUND":
                                iOccupancyCode = 7180;
                                break;
                            case "NAIL":
                                iOccupancyCode = 3100;
                                break;
                            case "SHOP":
                                iOccupancyCode = 3300;
                                break;
                            case "STORG":
                                iOccupancyCode = 3100;
                                break;
                            case "STOROLD":
                                iOccupancyCode = 3102;
                                break;
                            case "STORA":
                                iOccupancyCode = 3401;
                                break;
                            case "STORO":
                                iOccupancyCode = 3400;
                                break;
                            case "STRIP":
                                iOccupancyCode = 3300;
                                break;
                            case "STUDIO":
                                iOccupancyCode = 3100;
                                break;
                            case "GROC":
                                iOccupancyCode = 3505;
                                break;
                            case "TAN":
                                iOccupancyCode = 3100;
                                break;
                            case "TATTO":
                                iOccupancyCode =3105 ;
                                break;
                            case "WHSL":
                                iOccupancyCode = 8500;
                                break;
                            default:
                                iOccupancyCode = 0;
                                break;
                        }
                    }
                    else if (sCommSubType.Contains("Office"))
                    {
                        switch (sOCOffice)
                        {
                            case "OFFH":
                                iOccupancyCode = 2100;
                                break;
                            case "OFFHR":
                                iOccupancyCode = 2300;
                                break;
                            case "OFFLR":
                                iOccupancyCode = 2100;
                                break;
                            case "OFFMR":
                                iOccupancyCode = 2200;
                                break;
                            default:
                                iOccupancyCode = 0;
                                break;
                        }
                    }
                    else if (sCommSubType.Contains("Professional"))
                    {
                        switch (sOCProfessional)
                        {
                            case "ADULTDC":
                                iOccupancyCode = 6500;
                                break;
                            case "ALF":
                                iOccupancyCode = 5220;
                                break;
                            case "DAYCARE":
                                iOccupancyCode = 6540;
                                break;
                            case "DENT":
                                iOccupancyCode = 5130;
                                break;
                            case "DOGKEN":
                                iOccupancyCode = 5305;
                                break;
                            case "FUNL":
                                iOccupancyCode = 5230;
                                break;
                            case "SURG":
                                iOccupancyCode = 5120;
                                break;
                            case "MED":
                                iOccupancyCode = 5100;
                                break;
                            case "VET":
                                iOccupancyCode = 5300;
                                break;
                            default:
                                iOccupancyCode = 0;
                                break;
                        }
                    }
                    else if (sCommSubType.Contains("Public"))
                    {
                        switch (sOCPublic)
                        {
                            case "CHURCHA":
                                iOccupancyCode = 6105;
                                break;
                            case "CHURCHB":
                                iOccupancyCode = 6100;
                                break;
                            case "CHURCHT":
                                iOccupancyCode = 6115;
                                break;
                            case "CHURCHH":
                                iOccupancyCode = 6105;
                                break;
                            case "ESCH":
                                iOccupancyCode = 6300;
                                break;
                            case "FSHALL":
                                iOccupancyCode = 6200;
                                break;
                            case "HSCH":
                                iOccupancyCode = 6314;
                                break;
                            case "MSCH":
                                iOccupancyCode = 6310;
                                break;
                            case "VSCH":
                                iOccupancyCode = 6340;
                                break;
                            case "VISIT":
                                iOccupancyCode = 6560;
                                break;
                            default:
                                iOccupancyCode = 0;
                                break;
                        }
                    }
                    else if (sCommSubType.Contains("Restaurant"))
                    {
                        switch (sOCRestaurant)
                        {
                            case "AMUSE":
                                iOccupancyCode = 3100;
                                break;
                            case "BAKER":
                                iOccupancyCode = 9000;
                                break;
                            case "BAR":
                                iOccupancyCode = 4125;
                                break;
                            case "BOWL":
                                iOccupancyCode = 4200;
                                break;
                            case "CAFET":
                                iOccupancyCode = 4120;
                                break;
                            case "CAMP":
                                iOccupancyCode = 4210;
                                break;
                            case "CATER":
                                iOccupancyCode = 4110;
                                break;
                            case "CONCESS":
                                iOccupancyCode = 4320;
                                break;
                            case "CCLUB":
                                iOccupancyCode = 4215;
                                break;
                            case "FFREST1":
                                iOccupancyCode = 4110;
                                break;
                            case "FFREST2":
                                iOccupancyCode = 4100;
                                break;
                            case "FIT":
                                iOccupancyCode = 4242;
                                break;
                            case "HFCLUB":
                                iOccupancyCode = 4210;
                                break;
                            case "HALL":
                                iOccupancyCode = 4215;
                                break;
                            case "REST":
                                iOccupancyCode = 4115;
                                break;
                            case "RVPK":
                                iOccupancyCode = 4210;
                                break;
                            case "CLUBS":
                                iOccupancyCode = 4215;
                                break;
                            case "THEAT":
                                iOccupancyCode = 4205;
                                break;
                            case "WEDD":
                                iOccupancyCode = 4215;
                                break;
                            default:
                                iOccupancyCode = 0;
                                break;
                        }
                    }
                    else if (sCommSubType.Contains("Services"))
                    {
                        switch (sOCService)
                        {
                            case "AUTOSVC":
                                iOccupancyCode = 7110;
                                break;
                            case "AUTOSLS":
                                iOccupancyCode = 7126;
                                break;
                            case "CARW":
                                iOccupancyCode = 7120;
                                break;
                            case "CARWSS":
                                iOccupancyCode = 7122;
                                break;
                            case "CARWFS":
                                iOccupancyCode = 7121;
                                break;
                            default:
                                iOccupancyCode = 0;
                                break;
                        }
                    }
                    else if (sCommSubType.ToUpper().Contains("FAMILY"))
                    {
                        if (sOCFamily.Contains("VALET"))
                        {
                            sErrMsg = "Valet does not require a valuation.";
                            throw new Exception("Valet does not require a valuation: " + iCaseNum.ToString());
                        }

                        if (sOCFamily.Contains("CONDO1"))
                        {
                            sErrMsg = "Condo/Townhome occupancies require a RCT4 valuation.";
                            throw new Exception("Valet and Condo/Townhome occupancies require a RCT valuation: " + iCaseNum.ToString());
                        }

                        switch (sOCFamily)
                        {
                            case "DUPL":
                                iOccupancyCode = 1230;
                                break;
                            case "SFD":
                                iOccupancyCode = 1250;
                                break;
                            case "VACB":
                                iOccupancyCode = 3100;
                                break;
                            case "VACD":
                                iOccupancyCode = 1250;
                                break;
                            default:
                                iOccupancyCode = 0;
                                break;
                        }
                    }
                    else
                    {
                        // Invalid occupancy type
                        sErrMsg = "Unable to determine occupancy type";
                        throw new Exception("Unable to determine occupancy type: " + iCaseNum.ToString());
                    }

                }   // Commercial business type

                // Construction class (Not ISO) based on Construction question (Construction2)
                int iConstCode = 0;

                switch (sConstruction)
                {
                    case "Frame":
                        iConstCode = 1;     //Frame (ISO 1)
                        break;
                    case "Joisted Masonry":     //Masonry (ISO 2)                    
                        iConstCode = 2;
                        break;
                    case "Non-combustible":     //Pre-Engineered Metal (ISO 3)       
                        iConstCode = 3;
                        break;
                    case "Masonry Non-combustible":     //Steel Frame (ISO 4)               
                        iConstCode = 4;
                        break;
                    case "Fire Resistive":      //Reinforced Concrete Frame (ISO 6) 
                        iConstCode = 6; 
                        break;
                    default:
                        iConstCode = 0;
                        break;
                }

                if (iConstCode < 1 || iConstCode > 6)
                {
                    sErrMsg = "Invalid construction class: value given = " + sConstruction;
                    throw new Exception("Invalid construction class: value given = " + sConstruction + " Case #: " + iCaseNum.ToString());
                }


                // Protection class
                // MSB does not use > 6
                // Have to reverse 5 and 6
                int iPCClass = 0;

                switch (sConstCode)
                {
                    case "PC1":
                        iPCClass = 1;
                        break;
                    case "PC 2":
                        iPCClass = 2;
                        break;
                    case "PC 3":
                        iPCClass = 3;
                        break;
                    case "PC 4":
                        iPCClass = 4;
                        break;
                    case "PC 5":
                        iPCClass = 6; // reverse 5 and 6
                        break;
                    case "PC 6":
                        iPCClass = 5;
                        break;
                    default:
                        iPCClass = 0;
                        break;
                }

                // ****** Not currently used
                //if (iPCClass < 1 || iPCClass > 6)
                //{
                //    sErrMsg = "Invalid protection class: ISO Code given = " + sConstCode;
                //    throw new Exception("Invalid construction class: ISO Code given = " + sConstCode + " Case #: " + iCaseNum.ToString());
                //}


                // create full valuation
                // returns 0 on failure - throws exception if error occurred
                CEAddValuationFull nmAddValuation = new CEAddValuationFull();

                nmAddValuation.CaseNumber = iCaseNum.ToString();
                nmAddValuation.BusinessName = sInsuredName;
                nmAddValuation.LocationAddress1 = sStreetAdd;
                nmAddValuation.LocationCity = sCity;
                nmAddValuation.LocationState = sState;
                nmAddValuation.LocationZip = sZip;
                nmAddValuation.YearBuilt = iYearBuilt;
                nmAddValuation.Stories = iStories;
                nmAddValuation.GrossFloorArea = iGrossFloorArea;
                nmAddValuation.ConstCode = iConstCode.ToString();
                nmAddValuation.ConstPct = 100;
                nmAddValuation.OccupancyCode = iOccupancyCode;
                nmAddValuation.OccupancyPct = 100;

                long newValuationId = nmAddValuation.CEAddFullValuation();

                if (newValuationId == 0)
                //{
                //    // get the valuation just created
                //    CEGetValuation nmGetValuation = new CEGetValuation();
                //    curPolNum = nmGetValuation.CEGetValuationbyId(Convert.ToInt32(newValuationId));

                //    // Update valuation to set the policy # to the case # 
                //    CEUpdateValuation nmUpdateValuation = new CEUpdateValuation();
                //    nmUpdateValuation.UpdateValuation(iCaseNum.ToString(), nmGetValuation.ReturnGetValuationResponse());
                //}
                //else
                {
                    throw new Exception("Create valuation return 0");
                }

                lbl3Status.BackColor = System.Drawing.Color.DarkGreen;
                lbl3Status.ForeColor = System.Drawing.Color.White;
                lbl3Status.Text = "  Valuation created successfully  ";
                WriteToLog("Valuation created for: " + tbCaseNum.Text);

            }
            catch (Exception ex)
            {

                lbl3Status.BackColor = System.Drawing.Color.Maroon;
                lbl3Status.ForeColor = System.Drawing.Color.White;
                trError3.Visible = true;
                tbErr3.Text = "Error creating valuation" + System.Environment.NewLine + ex.Message;

                if (sErrMsg.Length > 0)
                {
                    lbl3Status.Text = "&nbsp; &nbsp;" + sErrMsg + "&nbsp;&nbsp;";
                }
                else if (ex.Message.ToUpper().Contains("MUST BE UNIQUE"))
                {
                    lbl3Status.Text = "  Valuation already exists  ";
                }
                else
                {
                    //record exception  
                    lbl3Status.Text = "  Valuation could not be created  ";
                }

                string sErr = "Error creating valuation: " + tbCaseNum.Text + System.Environment.NewLine;
                WriteToLog(sErr + ex.Message);
            }
        }

        private void WriteToLog(string sMessage)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();
            oLU.WritetoLog(sMessage);
            oLU.closeLog();
            oLU = null;
        }

        protected void bClose_Click(object sender, EventArgs e)
        {
            Server.Transfer("Main.aspx");
        }

        protected string getBVSFormInstance(int iCaseNum)
        {
            // Returns FormInstanceID or "" if not found

            string sRetVal = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                Guid guBVSFormID = new Guid("406ca41a-2bb3-4ad3-98ad-581d9fe0f521");   //BVS Shared Fields v11.23.20

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetBVSForm";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseid", guCaseID);
                sqlCmd1.Parameters.AddWithValue("@caseformid", guBVSFormID);

                sqlConn1.Open();

                // Get case info
                object oRet = sqlCmd1.ExecuteScalar();
                if (oRet != null)
                    sRetVal = oRet.ToString();

                sqlConn1.Close();
            }

            catch (Exception ex)
            {

                //record exception  
                string sErr = "Error locating BVS Form for inspection: " + tbCaseNum.Text + System.Environment.NewLine;

                WriteToLog(sErr + ex.Message);

            }

            finally
            {

                if (sqlConn1 != null)
                    sqlConn1.Close();
            }

            return sRetVal;
        }

        protected string getBVSFormData(string sBVSFormInstance)
        {
            string sErrMsg = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetBVSFormData";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseid", guCaseID);
                sqlCmd1.Parameters.AddWithValue("@formid", sBVSFormInstance);

                sqlConn1.Open();

                // Get case info
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through results
                    do
                    {

                        // Stories
                        iNumStoriesOwner = sqlReader.GetInt32(0);
                        iNumStoriesTenant = sqlReader.GetInt32(1);

                        // Year built
                        iYearBuilt = Convert.ToInt32(sqlReader.GetSqlString(2).ToString());

                        // Bldg Area
                        iGrossFloorArea = sqlReader.GetInt32(3);

                        // Business type
                        sBusinessType = sqlReader.GetSqlString(4).ToString(); // Commercial / AG

                        // Commercial Occupancy sub type
                        sCommSubType = sqlReader.GetSqlString(5).ToString(); // Industrial, Lodging, etc.

                        // Agricultural  Occupancy sub type
                        sAgSubType = sqlReader.GetSqlString(6).ToString(); // Farm, Winery

                        sOCIndustrial = sqlReader.GetSqlString(7).ToString();
                        sOCLodging = sqlReader.GetSqlString(8).ToString();
                        sOCMercantile = sqlReader.GetSqlString(9).ToString();
                        sOCOffice = sqlReader.GetSqlString(10).ToString();
                        sOCProfessional = sqlReader.GetSqlString(11).ToString();
                        sOCPublic = sqlReader.GetSqlString(12).ToString();
                        sOCRestaurant = sqlReader.GetSqlString(13).ToString();
                        sOCService = sqlReader.GetSqlString(14).ToString();
                        sOCFamily = sqlReader.GetSqlString(25).ToString();

                        sConstruction = sqlReader.GetSqlString(15).ToString();
                        sFoundation = sqlReader.GetSqlString(16).ToString();
                        sElectrical = sqlReader.GetSqlString(17).ToString();
                        sSprinkler = sqlReader.GetSqlString(18).ToString();
                        sFireAlarm = sqlReader.GetSqlString(19).ToString();
                        sElevator = sqlReader.GetSqlString(20).ToString();
                        sHVAC = sqlReader.GetSqlString(21).ToString();
                        sWallConst = sqlReader.GetSqlString(22).ToString();
                        sRoofCover = sqlReader.GetSqlString(23).ToString();
                        sConstCode = sqlReader.GetSqlString(24).ToString();

                        

                    } while (sqlReader.Read());

                    sqlReader.Close();
                    sqlConn1.Close();

                }   // has rows
                else
                {
                    sErrMsg = "Unable to locate valuation form data. Please verify the case number." + System.Environment.NewLine + "Note that you must save the Ghost form before creating the valuation";
                    throw new SystemException("No rows returned for: " + tbCaseNum.Text);
                }

            }

            catch (Exception ex)
            {

                //record exception  
                string sErr = "Error getting BVS data: " + tbCaseNum.Text + System.Environment.NewLine;

                WriteToLog(sErr + ex.Message);

            }

            finally
            {

                if (sqlReader != null)
                    sqlReader.Close();
                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

            return sErrMsg;
        }
    }
}