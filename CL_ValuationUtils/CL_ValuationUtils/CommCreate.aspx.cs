﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace CL_ValuationUtils
{
    public partial class CommCreate : System.Web.UI.Page
    {
        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;

        static int iCaseNum = 0;
        static string sPolicyNum = "";
        static string sInsuredName = "";
        static string sStreetAdd = "";
        static string sCity = "";
        static string sState = "";
        static string sZip = "";
        static decimal dCoverageA;
        static int iCoverageA;
        static int iStatus = 0;
        //static string sFieldRepNum = "";
        //static string sFieldRepFName = "";
        //static string sFieldRepLName = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");

            string sVal = Request.QueryString["val"];
            hiVal.Value = sVal;
            tblMode1.Visible = false;
            tblMode2.Visible = false;
            tblMode3.Visible = false;
            trError3.Visible = false;

            if (sVal == null)
            {
                tblMode1.Visible = true;
                lblMode1Msg.Text = "Enter the inspection number for the valuation and click lookup:";
            }
            else if (sVal == "1")
            {
                tblMode2.Visible = true;
                lblMode2Msg.Text = "Creating valuation for the following inspection:";
            }
            else if (sVal == "2")
            {
                tblMode3.Visible = true;
                lblMode3Msg.Text = "Valuation created successfully";
            }

        }

        protected void bSubmit1_Click(object sender, EventArgs e)
        {
            string sErrMsg = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {
                if (tbCaseNum.Text.Length < 7)
                {
                    sErrMsg = "Please enter a valid inspection number.";
                    throw new SystemException("");
                }

                iCaseNum = Convert.ToInt32(tbCaseNum.Text);

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetCaseInfoValuation";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", iCaseNum);

                sqlConn1.Open();

                // Get case info
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through results
                    do
                    {
                        //case number
                        if (sqlReader.IsDBNull(0))
                        {
                            throw new SystemException("SQL returned empty Case#");
                        }
                        else
                        {
                            iCaseNum = (int)sqlReader.GetSqlInt32(0);
                        }

                        // policy #
                        sPolicyNum = sqlReader.GetSqlString(1).ToString();

                        // name
                        sInsuredName = sqlReader.GetSqlString(2).ToString();
                        if (sInsuredName.Length > 49)   // 50 char max insured name
                        {
                            sInsuredName = sInsuredName.Substring(0, 49);
                        }

                        // Address
                        sStreetAdd = sqlReader.GetSqlString(3).ToString();

                        // City
                        sCity = sqlReader.GetSqlString(4).ToString();

                        // State
                        sState = sqlReader.GetSqlString(5).ToString();

                        // Zip
                        sZip = sqlReader.GetSqlString(6).ToString();

                        // Coverage A
                        dCoverageA = (decimal)sqlReader.GetSqlMoney(7);
                        iCoverageA = Convert.ToInt32(dCoverageA);

                        // Case status
                        iStatus = (int)sqlReader.GetSqlInt32(8);

                        //// Insp Number
                        //sFieldRepNum = sqlReader.GetSqlString(9).ToString();

                        //// Insp first name
                        //sFieldRepFName = sqlReader.GetSqlString(10).ToString();

                        ////Insp last name
                        //sFieldRepLName = sqlReader.GetSqlString(11).ToString();
                        

                    } while (sqlReader.Read());

                    sqlReader.Close();
                    sqlConn1.Close();

                }   // has rows
                else
                {
                    sErrMsg = "Unable to locate inspection. Please verify you have entered the correct case number.";
                    throw new SystemException("No rows returned for: " + tbCaseNum.Text);
                }

                // break if case has been completed
                if (iStatus == 6 || iStatus == 7)
                {
                    sErrMsg = "This inspection has already been completed.";
                    throw new SystemException("Inspection completed: " + tbCaseNum.Text);
                }

                //// break if inspector information incomplete
                //if (sFieldRepNum.Length < 1 || sFieldRepFName.Length < 1 || sFieldRepLName.Length < 1)
                //{
                //    sErrMsg = "Incomplete inspector information.";
                //    throw new SystemException("Incomplete insp info: " + tbCaseNum.Text + " Insp#: " + sFieldRepNum + " FName: " + sFieldRepFName + " LName: " + sFieldRepLName);
                //}

                hiVal.Value = "2";
                tblMode1.Visible = false;
                tblMode2.Visible = true;
                tbCaseNum2.Text = iCaseNum.ToString();
                tbInsured2.Text = sInsuredName;
                lblMode2Msg.Text = "Create valuation for the following inspection:";

            }

            catch (Exception ex)
            {

                if (sErrMsg.Length > 0)
                {
                    lbl1Status.Text = "&nbsp; &nbsp;" + sErrMsg + "&nbsp;&nbsp;";
                }
                else
                {
                    lbl1Status.Text = "&nbsp;&nbsp;An unexpected error has occurred - please contact Sutton support.&nbsp;&nbsp;";
                }

                //record exception  
                string sErr = "Error attempting to access inspection: " + tbCaseNum.Text + System.Environment.NewLine;

                WriteToLog(sErr + ex.Message);

            }

            finally
            {

                if (sqlReader != null)
                    sqlReader.Close();
                if (sqlConn1 != null)
                    sqlConn1.Close();

            }
        }

        protected void bSubmit2_Click(object sender, EventArgs e)
        {
            string curPolNum = "";

            hiVal.Value = "3";
            tblMode1.Visible = false;
            tblMode2.Visible = false;
            tblMode3.Visible = true;
            tbCaseNum3.Text = iCaseNum.ToString();
            tbInsured3.Text = sInsuredName;
            lblMode3Msg.Text = "Creating valuation for the following inspection:";

            WriteToLog("Creating valuation for: " + tbCaseNum.Text);

            try
            {

                // create minimal valuation
                // returns 0 on failure - throws exception if error occurred
                CEAddValuationMin nmAddValuationMin = new CEAddValuationMin();
                long newValuationId = nmAddValuationMin.CEAddValuationMinimum(sInsuredName, sStreetAdd, sCity, sState, sZip);

                if (newValuationId != 0)
                {
                    // get the valuation just created
                    CEGetValuation nmGetValuation = new CEGetValuation();
                    curPolNum = nmGetValuation.CEGetValuationbyId(Convert.ToInt32(newValuationId));

                    // Update valuation to set the policy # to the case # 
                    CEUpdateValuation nmUpdateValuation = new CEUpdateValuation();
                    nmUpdateValuation.UpdateValuation(iCaseNum.ToString(), nmGetValuation.ReturnGetValuationResponse());
                }
                else
                {
                    throw new Exception("Create valuation return 0");
                }

                lbl3Status.BackColor = System.Drawing.Color.DarkGreen;
                lbl3Status.ForeColor = System.Drawing.Color.White;
                lbl3Status.Text = "  Valuation created successfully  ";
                WriteToLog("Valuation created for: " + tbCaseNum.Text);

            }
            catch (Exception ex)
            {
                if (ex.Message.ToUpper().Contains("MUST BE UNIQUE"))
                {
                    lbl3Status.Text = "  Valuation already exists  ";
                    lbl3Status.BackColor = System.Drawing.Color.Maroon;
                    lbl3Status.ForeColor = System.Drawing.Color.White;
                    trError3.Visible = true;
                    tbErr3.Text = "Error creating valuation" + System.Environment.NewLine + ex.Message;
                }
                else
                {
                    //record exception  
                    lbl3Status.Text = "  Valuation could not be created  ";
                    lbl3Status.BackColor = System.Drawing.Color.Maroon;
                    lbl3Status.ForeColor = System.Drawing.Color.White;
                    trError3.Visible = true;
                    tbErr3.Text = "Error creating valuation" + System.Environment.NewLine + ex.Message;
                }

                string sErr = "Error creating valuation: " + tbCaseNum.Text + System.Environment.NewLine;
                WriteToLog(sErr + ex.Message);
            }
        }

        private void WriteToLog(string sMessage)
        {

            // initialize log file class
            oLU = new LogUtils.LogUtils();

            // set log file name
            oLU.logFileName = cfg_logfilename;

            // open log file
            oLU.OpenLog();
            oLU.WritetoLog(sMessage);
            oLU.closeLog();
            oLU = null;
        }

        protected void bClose_Click(object sender, EventArgs e)
        {
            Server.Transfer("Main.aspx");
        }
    }
}