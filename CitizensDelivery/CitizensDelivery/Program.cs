﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Word = Microsoft.Office.Interop.Word;
using WinSCP;


namespace CitizensDelivery
{
    class Program
    {

        static void Main(string[] args)
        {

            Setup setup = new Setup();

        }


        /// <summary>
        /// Instantiate properties from the AppSettings located in the App.config.
        /// </summary>
        public class Setup
        {

            private string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
            private string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
            private string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            private string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
            private string cfg_recroot = ConfigurationManager.AppSettings["RecRoot"];
            private string cfg_FTPRoot = ConfigurationManager.AppSettings["FTPRoot"];
            private string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
            private string cfg_exportapp = ConfigurationManager.AppSettings["ExportApp"];
            private string cfg_recapp = ConfigurationManager.AppSettings["RecApp"];
            private string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];

            private LogUtils.LogUtils oLU;
            private bool bErr = false;
            private string sPolicy = "";
            private string sCaseNum = "";
            private Guid guCaseID;
            private string sActivityID = "";
            private string sCaseType = "";
            private string sDeliveryDate = "";
            private string sBatchNum = "";

            //Used for email message body.  
            private StringBuilder sbEmail = new StringBuilder();

            //Used for transmittal message body.  
            private StringBuilder sbmsgBody = new StringBuilder();

            public Setup()
            {
                int iCasesExported = 0;

                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                bool bExport = true;
                // no recs //////////////
                //bool bCreateRecs = false;
                /////////////////////////
                bool bFTPFiles = true;

                DirectoryInfo diPDF;
               // DirectoryInfo diRec;
                DirectoryInfo diCaseFiles;

                FileInfo[] fiPDFFiles;
                //FileInfo[] fiRECFiles;
                FileInfo fiRec;
                oLU.WritetoLog("Begin Processing  " + DateTime.Now.ToString());
                sbEmail.Append("Begin Processing  " + DateTime.Now.ToString() + "\r\n\r\n");

                //***********************************************************
                // Export PDF's from 360
                if (bExport)
                {
                    try
                    {

                        // Delete all files in PDF Export directory.
                        string[] sFiles = Directory.GetFiles(cfg_pdfroot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);

                        // Export PDF's from 360
                        oLU.WritetoLog(" Begin exporting completed cases from 360");
                        sbEmail.Append("---- Begin exporting completed cases from 360\r\n\r\n");
                        oLU.closeLog();

                        oLU.OpenLog();
                        oLU.WritetoLog(cfg_exportapp);

                        var proc = Process.Start(cfg_exportapp);
                        proc.WaitForExit();
                        var exitCode = proc.ExitCode;
                        sbEmail.Append("Exporter returned: " + exitCode.ToString() + "\r\n");

                        // exit code should be the number of cases exported
                        iCasesExported = exitCode;

                        Console.WriteLine(exitCode.ToString());

                    }
                    catch (Exception ex)
                    {
                        sbEmail.Append("**** ERROR IN EXPORT:\r\n" + ex.Message);
                        oLU.WritetoLog(sbEmail.ToString());
                        bErr = true;
                        oLU.closeLog();

                        oLU.OpenLog();

                    }

                    diPDF = new DirectoryInfo(cfg_pdfroot);
                    fiPDFFiles = diPDF.GetFiles("*.pdf");

                    if (fiPDFFiles.Count() != iCasesExported)
                    {
                        sbEmail.Append("**** PDF count does not match number exported ****\r\n");
                        sbEmail.Append("Exported: " + iCasesExported.ToString() + " - In folder: " + fiPDFFiles.Count().ToString() + "\r\n\r\n");
                    }

                }




                //***********************************************************
                // Rename PDF's 
                //  ???? Create packinglist of everything to be sent 
                // FTP contents of FTP folder
                if (bFTPFiles && !bErr)
                {

                    //*******************
                    // Rename files in PDF folder, move to FTP folder

                    // 7/1/2021 file format changes
                    //
                    // File name = VendorName_PolicyNumber_GAMAssignmentNumber_InspectionResultCode
                    // 
                    // GAMAssignmentNumber is generic field "Activity ID"
                    //
                    // Example Sutton_Policy_123456_COMPLETED.PDF or Sutton_Policy_123456_CLOSEOUT.PDF
                    // 
                    // Result code logic
                    // If case type includes "Non-prod" or "Additional" set to Closeout otherwise Completed


                    string sOutputFileName = "";
                    string sOutcome = "";

                    try
                    {

                        // Delete all files in FTP folder.
                        string[] sFiles = Directory.GetFiles(cfg_FTPRoot);
                        foreach (string sFile in sFiles)
                            File.Delete(sFile);

                        // get list of files in PDF folder
                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        // for each PDF 
                        foreach (FileInfo pdfFile in fiPDFFiles)
                        {

                            // Get case # from file name
                            sCaseNum = pdfFile.Name.Substring(0, pdfFile.Name.IndexOf("."));

                            // Get Activity ID
                            if (!GetCaseInfo(sCaseNum))
                            {
                                sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("GetInfo failed for case: " + sCaseNum);
                            }

                            if (sActivityID == "")
                            {
                                sbEmail.Append("No Activity ID for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("No Activity ID for case: " + sCaseNum);
                            }

                            // Case type / outcome
                            sOutcome = "COMPLETED";
                            if (sCaseType.ToUpper().Contains("NON-PROD") || sCaseType.ToUpper().Contains("ADDITIONAL"))
                            {
                                sOutcome = "CLOSEOUT";
                            }

                            // Build Report file name
                            sOutputFileName = @"Sutton_" + sPolicy + "_" + sActivityID + "_" + sOutcome + ".pdf";


                            //// replace any illegal characters with "-"
                            //foreach (char c in sInvalidChars)
                            //{
                            //    sOutputFileName = sOutputFileName.Replace(c, '-');
                            //}

                            // Copy PDF to FTP folder under new name
                            pdfFile.CopyTo(cfg_FTPRoot +  sOutputFileName);

                        }   // Rename PDF's
                    }
                    catch (Exception ex)
                    {
                        bErr = true;
                        sbEmail.Append("**** Error renaming files" + "\r\n" + ex.Message + "\r\n\r\n");
                    }   // Rename PDF's

                    //*******************
                    // Build packing list
                    if (!bErr)
                    {
                        sbEmail.Append("\r\n\r\n---- Build packing list\r\n");
                        string sPackList = "";
                        try
                        {

                            diPDF = new DirectoryInfo(cfg_FTPRoot);
                            fiPDFFiles = diPDF.GetFiles("*.pdf");

                            // for each PDF 
                            foreach (FileInfo pdfFile in fiPDFFiles)
                            {

                                sPackList = sPackList + pdfFile.Name + System.Environment.NewLine;

                            }

                            // create packinglist.txt - overwrite existing
                            using (StreamWriter writer = new StreamWriter(cfg_pdfroot + "packinglst.txt", false))
                            {
                                writer.WriteLine(sPackList);
                            }

                            sbEmail.Append("\r\n\r\n" + sPackList + "\r\n");

                            //sendPackingList(cfg_pdfroot + "packinglst.txt");

                        }
                        catch (Exception ex)
                        {
                            bErr = true;
                            sbEmail.Append("**** Error creating packinglist.txt" + "\r\n" + ex.Message + "\r\n\r\n");
                        }   // Packing list
                    }

                    //////////////////////////////
                    // FTP all files in FTP folder
                    //////////////////////////////
                    if (!bErr)
                    {
                        int iNumUploaded = 0;
                        int iNumToUpload = 0;

                        diPDF = new DirectoryInfo(cfg_FTPRoot);
                        fiPDFFiles = diPDF.GetFiles("*.*");

                        iNumToUpload = fiPDFFiles.Count();
                        if (iNumToUpload == 0)
                        {
                            sbEmail.Append("\r\n\r\n---- No Files to FTP\r\n");
                        }
                        else
                        {
                            sbEmail.Append("\r\n\r\n---- Begin FTP transfer\r\n");
                            sbEmail.Append("\r\n\r\n" + iNumToUpload.ToString() + " files to upload\r\n");
                            try
                            {
                                // Setup session options
                                SessionOptions sessionOptions = new SessionOptions
                                {
                                    Protocol = Protocol.Sftp,
                                    HostName = "sftp1.citizensfla.com",
                                    UserName = "inspections_sutton_svc",
                                    Password = "5PMuT^QUpS&pMZn",
                                    SshHostKeyFingerprint = "ssh-rsa 2048 12:52:0e:d1:16:c0:e1:ba:3d:20:81:e6:3e:3c:0c:d1"
                                };

                                using (Session session = new Session())
                                {

                                    session.SessionLogPath = @"c:\automationlogs\Citizens\FTPLogs.txt";

                                    // Connect
                                    session.Open(sessionOptions);

                                    // Upload files
                                    TransferOptions transferOptions = new TransferOptions();
                                    transferOptions.TransferMode = TransferMode.Binary;
                                    TransferOperationResult transferResult;
                                    transferResult = session.PutFiles(cfg_FTPRoot + "*", "SUTTON_UPLOAD/", true, transferOptions);

                                    // Throw on any error
                                    transferResult.Check();

                                    // Print results
                                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                                    {
                                        oLU.WritetoLog(transfer.FileName + " uploaded");
                                        iNumUploaded++;
                                    }

                                }

                                sbEmail.Append("\r\n" + iNumUploaded.ToString() + " files uploaded\r\n\r\n---- End FTP transfer\r\n");
                            }
                            catch (Exception ex)
                            {
                                bErr = true;
                                sbEmail.Append("**** Error in FTP Transfer\r\n\r\n" + ex.Message + "\r\n\r\n");
                            }

                        }
                    }

                    // Send packing list
                    if (!bErr)
                    {
                        try
                        {
                            sendPackingList(cfg_pdfroot + "packinglst.txt");
                        }
                        catch (Exception ex)
                        {
                            bErr = true;
                            sbEmail.Append("**** Error sending packing list\r\n\r\n" + ex.Message + "\r\n\r\n");
                        }
                    }


                    // Done - write to log, send email  and clean up
                    sbEmail.Append("\r\nEnd Processing  " + DateTime.Now.ToString() + "\r\n\r\n");
                    oLU.WritetoLog(sbEmail.ToString());
                    oLU.closeLog();

                    if (bErr)
                    {
                        sendLogEmail("**** ERROR RECORDED - Citizens Delivery Processing ****", sbEmail.ToString());
                    }
                    else
                    {
                        sendLogEmail("Citizens Delivery Processing", sbEmail.ToString());
                    }
                }

            }   //if FTP

            private void sendPackingList(string sAttachment)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
                string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "sibflamail@sibfla.com";
                    oMail.MailTo = "uw.vendor.admin@citizensfla.com";
                    //oMail.MailTo = "jeff@sibfla.com";
                    oMail.MailBCC = "jeff@sibfla.com";
                    oMail.MsgSubject = "Sutton delivery confirmation for " + DateTime.Now.ToShortDateString();
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    oMail.Attachment = sAttachment;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

            }

            private void sendLogEmail(string sSubject, string sBodyText)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "noreply@sibfla.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;
                }
            }

    
            private bool GetCaseInfo(string sCaseNum)
            {

                bool bRetVal = false;

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseInfo_CaseNum";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {
                            guCaseID = sqlReader.GetGuid(1);
                            sPolicy = sqlReader.GetSqlString(3).ToString();
                            sCaseType = sqlReader.GetSqlString(9).ToString().Trim();
                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }

                    // IR FileNum
                    sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                    sqlCmd1.Parameters.AddWithValue("@fieldname", "Activity ID");

                    sActivityID = "";
                    object oRetVal = sqlCmd1.ExecuteScalar();

                    if (oRetVal != null)
                    {
                        sActivityID = (string)oRetVal;
                    }

                    if (sActivityID == "")
                    {
                        sbEmail.Append("\r\n" + "No IR File Num for " + sCaseNum + " \r\n");
                        bErr = true;
                    }

                    bRetVal = true;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();



                }

                return bRetVal;
            }

            private void buildHeader()
            {

                // <head>
                sbmsgBody = sbmsgBody.Append("<html><head><title>Sutton Inspection Bureau, Inc.of Florida</title> " + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<style type='text/css'>.stdText {font-size: 8pt;COLOR: black;font-family: Verdana,Tahoma,Arial;}" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append(".stdTextBold {font-size: 8pt;color: black;font-family: Verdana,Tahoma,Arial;font-weight: bold;}" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append(".largeText {font-size: 12pt;color: black;font-family: Verdana,Tahoma,Arial;font-weight: bold;}</style></head>" + System.Environment.NewLine);

                // <body>
                sbmsgBody = sbmsgBody.Append("<body><table cellSpacing = '0' cellPadding = '2' width = '760' border = '0'><tr><td width = '10'> &nbsp;</td><td align = 'left' width = '475'><span class='stdText'>Sutton Inspection Bureau, Inc.of Florida</span></td><td width = '275'> &nbsp;</td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td> &nbsp;</td><td align = 'left'><span class='stdText'>5656 Central Avenue</span></td><td align = 'right'><span class='largeText'>Delivery Transmittal</span></td></tr>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<tr><td> &nbsp;</td><td align = 'left'><span class='stdText'>St.Petersburg,  FL 33707-1718</span></td><td>&nbsp;</td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='0' width='760' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='750'><hr align='left' width='750'></td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='760' border='0'><tr><td width = '10'>&nbsp;</td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align= right' width= 60'><span Class='stdText'>Account:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='left' width='340'><span Class='stdText'>Citizens</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right' width='240'><span Class='stdText'>Delivery date:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right' width='110'><span Class='stdText'>" + sDeliveryDate + "</span></td></tr>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right'><span Class='stdText'>Batch Number:</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='right'><span Class='stdText'>" + sBatchNum.Trim() + "</span></td></tr></table>" + System.Environment.NewLine);

                //sbmsgBody = sbmsgBody.Append("</table>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='0' width='760' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='750'><hr align='left' width='750'></td></tr></table>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<table cellSpacing='0' cellPadding='2' width='760' style='border - bottom:solid; '><tr><td align = 'center' width = '120'><span Class='stdTextBold'> Policy </span></td>" + System.Environment.NewLine);

                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> IR File</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='360'><span Class='stdTextBold'> Insured </span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> Case</span></td>" + System.Environment.NewLine);
                sbmsgBody = sbmsgBody.Append("<td align='center' width='90'><span Class='stdTextBold'> Amount </span></td></tr>" + System.Environment.NewLine);

            }

            private void buildFooter()
            {
                sbmsgBody = sbmsgBody.Append("</table></body></html>");

            }

            private void buildLineItem(string sPolicy, string sIR, string sInsured, string sCaseNum, string sAmount)
            {

                sbmsgBody = sbmsgBody.Append("<tr><td align='center'><span Class='stdText'>" + sPolicy + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'> " + sIR + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='left'><span Class='stdText'>" + sInsured + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'>" + sCaseNum + "</span></td>");
                sbmsgBody = sbmsgBody.Append("<td align='center'><span Class='stdText'>" + sAmount + "</span></td></tr>");
                
            }

        }
    }
}
