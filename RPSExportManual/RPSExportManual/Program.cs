﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Word = Microsoft.Office.Interop.Word;
using System.Net.Mail;


namespace RPSExportManual
{
    class Program
    {

        static void Main(string[] args)
        {

            Setup setup = new Setup();

        }


        /// <summary>
        /// Instantiate properties from the AppSettings located in the App.config.
        /// </summary>
        public class Setup
        {

            private string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
            private string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
            private string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            private string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
            private string cfg_recroot = ConfigurationManager.AppSettings["RecRoot"];
            private string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
            private string cfg_exportapp = ConfigurationManager.AppSettings["ExportApp"];
            private string cfg_recapp = ConfigurationManager.AppSettings["RecApp"];
            private string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
            private string cfg_EmailSendTo = ConfigurationManager.AppSettings["EmailSendTo"];
            private string cfg_EmailSendFrom = ConfigurationManager.AppSettings["EmailSendFrom"];


            private LogUtils.LogUtils oLU;
            private bool bErr = false;
            private string sPolicy = "";
            private string sInsured = "";
            private string sCaseNum = "";
            private Guid guCaseID;
            private string sIRFileNum = "";
            private string sEmailSubject = "";
            private string sCustNum = "";
            private string sCustName = "";
            private string sAmount = "";
            private string sOutputFileName = "";
            private string sDeliveryDate = "";
            private string sBatchNum = "";

            //Used for email message body.  
            private StringBuilder sbEmail = new StringBuilder();

            //Used for transmittal message body.  
            private StringBuilder sbmsgBody = new StringBuilder();

            public Setup()
            {

                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                bool bEmailFiles = true;

                DirectoryInfo diPDF;
                FileInfo[] fiPDFFiles;


                oLU.WritetoLog("Begin Processing  " + DateTime.Now.ToString());
                sbEmail.Append("Begin Processing  " + DateTime.Now.ToString() + "\r\n\r\n");

              
                //***********************************************************
                // Send PDF's & Recs
                // Email subject - If IR FileNum - "SYS*REF#" + IRFileNum
                // Else - Policy + " - completed report"
                if (bEmailFiles && !bErr)
                {

                    sbEmail.Append("\r\n\r\n---- Begin renaming reports\r\n");

                    ///////////////////////////////////
                    // Email PDF's & Recs
                    ///////////////////////////////////
                    try
                    {

                        diPDF = new DirectoryInfo(cfg_pdfroot);
                        fiPDFFiles = diPDF.GetFiles("*.pdf");

                        // for each PDF 
                        foreach (FileInfo pdfFile in fiPDFFiles)
                        {

                            // Get case # from file name
                            sCaseNum = pdfFile.Name.Substring(0, pdfFile.Name.IndexOf("."));

                            // Get CaseID & customer number
                            if (!GetCaseInfo(sCaseNum))
                            {
                                sbEmail.Append("GetInfo failed for case: " + sCaseNum + "\r\n\r\n");
                                throw new ApplicationException("GetInfo failed for case: " + sCaseNum);
                            }

                            if (sIRFileNum.Length > 0)
                            {
                                sEmailSubject = "[SYS*REF#" + sIRFileNum + "]";
                            }
                            else
                            {
                                sEmailSubject = sPolicy.Trim() + " - completed report";
                            }

                            /// Remove characters that would make it fail
                            sEmailSubject = sEmailSubject.Replace("\n", " ").Replace("\r", " ");
                            
                            //Instantiate and create Email message
                            using (MailMessage msg = new MailMessage())
                            {
                                msg.From = new MailAddress(cfg_EmailSendFrom);
                                msg.To.Add(new MailAddress(cfg_EmailSendTo));

                                msg.Subject = sEmailSubject;

                                // Attach PDF 
                                msg.Attachments.Add(new Attachment(pdfFile.FullName));

                                if (SendMailMessage(msg))
                                {
                                    sbEmail.Append("Email for case " + sCaseNum + " successfully sent\r\n\r\n");
                                }
                                else
                                {
                                    sbEmail.Append("**** Error sending email for case " + sCaseNum + "\r\n\r\n");
                                }

                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        bErr = true;
                        sbEmail.Append("**** Error processing files for case:" + sCaseNum + "\r\n" + ex.Message + "\r\n\r\n");
                    }

                    // Done - write to log, send email  and clean up
                    sbEmail.Append("\r\nEnd Processing  " + DateTime.Now.ToString() + "\r\n\r\n");
                    oLU.WritetoLog(sbEmail.ToString());
                    oLU.closeLog();

                    if (bErr)
                    {
                        sendLogEmail("**** ERROR RECORDED - RPSManual Delivery Processing ****", sbEmail.ToString());
                    }
                    else
                    {
                        sendLogEmail("RPSManual Delivery Processing", sbEmail.ToString());
                    }
                }

            }   //if email reports

            private void sendLogEmail(string sSubject, string sBodyText)
            {

                string sRet;
                string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

                try
                {

                    SendMail.SendMail oMail;
                    oMail = new SendMail.SendMail();

                    oMail.MailFrom = "sibflamail@sibfla.com";
                    oMail.MailTo = "jeff@sibfla.com";
                    oMail.MsgSubject = sSubject;
                    oMail.MsgBody = sBodyText;
                    oMail.SMTPServer = smtpserver;
                    oMail.SendHTML = false;
                    sRet = oMail.Send();
                    oMail = null;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;
                }
            }

            private bool SendMailMessage(MailMessage message)
            {

                bool bRet = true;
                try
                {
                    SmtpClient client = new SmtpClient();
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    bErr = true;
                    bRet = false;
                    sbEmail.Append("**** Error sending mail ****\r\n\r\n" + ex.Message + "\r\n\r\n");
                }
                return bRet;
            }


            private bool GetCaseInfo(string sCaseNum)
            {

                bool bRetVal = false;

                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                try
                {

                    // set up SQL connection (360)
                    sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                    sqlCmd1 = new SqlCommand();

                    sqlCmd1.CommandType = CommandType.StoredProcedure;
                    sqlCmd1.CommandText = "sp_GetCaseInfo_CaseNum";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                    sqlCmd1.Connection = sqlConn1;
                    sqlConn1.Open();

                    sqlReader = sqlCmd1.ExecuteReader();

                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();

                        do
                        {
                            guCaseID = sqlReader.GetGuid(1);
                            sPolicy = sqlReader.GetSqlString(3).ToString();
                            sInsured = sqlReader.GetSqlString(2).ToString();
                            sCustName = sqlReader.GetSqlString(10).ToString().Trim();
                            sCustNum = sqlReader.GetSqlString(11).ToString().Trim();
                            sAmount = sqlReader.GetSqlDecimal(12).ToString();
                        } while (sqlReader.Read());

                        sqlReader.Close();

                    }

                    // IR FileNum
                    sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                    sqlCmd1.Parameters.AddWithValue("@fieldname", "ImageRight File Number");

                    sIRFileNum = "";
                    object oRetVal = sqlCmd1.ExecuteScalar();

                    if (oRetVal != null)
                    {
                        sIRFileNum = (string)oRetVal;
                    }

                    if (sIRFileNum == "")
                    {
                        sbEmail.Append("\r\n" + "No IR File Num for " + sCaseNum + " \r\n");
                    }

                    bRetVal = true;
                }
                catch (Exception ex)
                {

                    //record exception  
                    throw ex;

                }

                finally
                {

                    // close objects

                    if (sqlReader != null)
                        sqlReader.Close();

                    if (sqlConn1 != null)
                        sqlConn1.Close();
                    
                }

                return bRetVal;
            }

 
        }
    }
}
