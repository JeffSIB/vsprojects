﻿using System;
using System.Data;
using System.Windows.Forms;
using Xceed.SmartUI.Controls.CheckedListBox;
using System.Data.SqlClient;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;

namespace _360RecHaz
{
    public partial class frmMain : Form
    {
        const string HTML_TAG_PATTERN = "<.*?>";

        private SmartCheckedListBox xAcntList1 = new SmartCheckedListBox();
        private string cfg_SQLMainSIBIConnStr;
        private string cfg_SQLMainUtilConnStr;
        private string cfg_360UtilConnStr;
        private Excel.Application oExcel = null;
        private Excel.Workbook oWorkbook = null;
        private Excel.Worksheet oRecWorksheet = null;
        private Excel.Worksheet oHazWorksheet = null;
        private Excel.Worksheet oProWorksheet = null;
        private Excel.Worksheet oShrWorksheet = null;
        private Excel.Worksheet oInjWorksheet = null;
        private Excel.Worksheet oValWorksheet = null;
        private Excel.Range oRange = null;
        private object oMissing = System.Reflection.Missing.Value;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");
            cfg_SQLMainUtilConnStr = colNameVal.Get("SQLMainUtilConnStr");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");

            cbRecs.Checked = true;
            cbHaz.Checked = true;
            cbPromoted.Checked = true;
            cbShared.Checked = true;
            cbInject.Checked = true;
            cbValidations.Checked = true;
            cbQuestions.Checked = true;

            // place grid on panel
            this.pnlAccounts.Controls.Add(xFormList);

            try
            {


                // Get forms
                if (!loadForms())
                {
                    throw new Exception("Error loading form data from 360");
                }

                fbdOutputDir.Description = "Output file location";
                //fbdOutputDir.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                fbdOutputDir.SelectedPath = "C:\\Temp";
                tbOutputDir.Text = fbdOutputDir.SelectedPath;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                //tSSlblStatus.Text = "Status: Ready";
                //tSSlblStatus.BackColor = System.Drawing.Color.LightGray;
                this.Cursor = Cursors.Default;
            }

            bGo.Focus();

        }

        private void bGo_Click(object sender, EventArgs e)
        {

            DateTime dNow = DateTime.Today;
            DateTime dBegDate = dNow.AddDays(-(dNow.Day - 1));
            DateTime dEndDate = new DateTime(dNow.Year, dNow.Month, 1).AddMonths(1).AddDays(-1);
            int iCount = 0;
            string sOutputFileName = "";
            Guid guFormID;
            bGo.Enabled = false;

            // for each selected account in list
            foreach (CheckedListBoxItem clbi in xFormList.Items)
            {

                if (clbi.Checked)
                {
                    iCount++;
                }

            }

            // must be at least 1 form selected
            if (iCount == 0)
            {
                MessageBox.Show("Please select one or more forms for the report", "No form selected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                bGo.Enabled = true;
                return;
            }

            // must be at least 1 report type selected
            if (!cbHaz.Checked && !cbRecs.Checked && !cbInject.Checked && !cbPromoted.Checked && !cbShared.Checked && !cbValidations.Checked && !cbQuestions.Checked)
            {
                MessageBox.Show("Please select at least one report type.", "No report type selected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                bGo.Enabled = true;
                return;
            }

            // Pick output folder
            if (tbOutputDir.Text == "")
            {
                MessageBox.Show("Please select an optout file location", "Output File Location", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                bGo.Enabled = true;
                return;
            }

            tbResults.Text = "";

            foreach (CheckedListBoxItem clbi in xFormList.Items)
            {

                if (clbi.Checked)
                {
                    guFormID = Guid.Parse(clbi.Key);

                    sOutputFileName = buildFileName(clbi.Text);
                    tbResults.Text += "Processing " + clbi.Text + System.Environment.NewLine;

                    buildExcel(guFormID, clbi.Text, tbOutputDir.Text, sOutputFileName);
                }
            }

            bGo.Enabled = true;
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private bool loadForms()
        {

            bool bRet = false;
            string sFormName;
            string sFormID;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // init SQL connection for read query
                SqlDataReader sqlDR;
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;

                // get all forms
                sqlCmd1.CommandText = "sp_GetForms";

                sqlConn1.Open();
                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        sFormName = sqlDR.GetSqlString(0).ToString();
                        sFormID = sqlDR.GetGuid(1).ToString();

                        if (cbIncludeZ.Checked)
                        {
                            CheckedListBoxItem clbi = new CheckedListBoxItem();
                            clbi.Text = sFormName;
                            clbi.Key = sFormID;
                            xFormList.Items.Add(clbi);
                        }
                        else
                        {
                            if (!sFormName.ToUpper().StartsWith("Z"))
                            {
                                CheckedListBoxItem clbi = new CheckedListBoxItem();
                                clbi.Text = sFormName;
                                clbi.Key = sFormID;
                                xFormList.Items.Add(clbi);
                            }
                        }


                    }	// while


                }	// has rows
                else
                {

                    MessageBox.Show("There were no customers found in 360.", "No data found", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

                bRet = true;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {
                if (sqlConn1 != null)
                    sqlConn1.Close();
            }

            return bRet;
        }

        private void bChangeOutputDir_Click(object sender, EventArgs e)
        {
            if (fbdOutputDir.ShowDialog() == DialogResult.OK)
            {
                tbOutputDir.Text = fbdOutputDir.SelectedPath;
            }

        }

        private bool buildExcel(Guid guFormID, string sFormName, string sOutputDir, string sOutputFileName)
        {

            bool bRet = false;
            string sGuid = guFormID.ToString();
            Console.WriteLine(sGuid);

            // SQL
            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;
            int iRow = 0;
            string sQuestion = "";
            string sQuestionToCheck = "";
            string sRecLogic = "";
            string sRecTextRaw = "";
            string sRecText = "";
            string sDescription = "";
            string sDBColName = "";
            string sPromotedField = "";
            string sSharedField = "";
            string sOtherField = "";
            string sRegEx = "";
            string sForm = "";
            string sColor = "";
            string sAnswer = "";
            string sValue = "";
            string sLookupID = "";
            string sRecType = "";
            int iScore = 0;

            bool bFirstSheet = true;

            //string sExcelFileNameNoEx = "C:\\temp\\" + sOutputFileName;
            string sExcelFileNameNoEx = sOutputDir + "\\" + sOutputFileName;
            string sExcelFileName = sOutputDir + sExcelFileNameNoEx + ".xlsx";

            oExcel = new Excel.Application();
            oExcel.Visible = true;
            oWorkbook = oExcel.Workbooks.Add(1);

            if (cbRecs.Checked)
            {

                tbResults.Text += "Building Excel sheet for Recommendations" + System.Environment.NewLine;

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetRecsOnForm";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@formID", guFormID);
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (bFirstSheet)
                {
                    oRecWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    bFirstSheet = false;
                }
                oRecWorksheet.Name = "Recommendations";
                createHeader(oRecWorksheet, sFormName, "REC");

                if (sqlReader.HasRows)
                {

                    iRow = 6;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //Question
                        if (sqlReader.IsDBNull(0))
                        {
                            sQuestion = "";
                        }
                        else
                        {
                            sQuestion = (string)sqlReader.GetSqlString(0);
                        }

                        //RecLogic
                        if (sqlReader.IsDBNull(1))
                        {
                            sRecLogic = "";
                        }
                        else
                        {
                            sRecLogic = (string)sqlReader.GetSqlString(1);
                        }

                        //RecText
                        if (sqlReader.IsDBNull(2))
                        {
                            sRecText = "";
                        }
                        else
                        {
                            sRecTextRaw = (string)sqlReader.GetSqlString(2);
                            sRecTextRaw = sRecTextRaw.Trim();
                            sRecText = StripHTML(sRecTextRaw);
                        }

                        //Lookup id
                        if (sqlReader.IsDBNull(3))
                        {
                            sLookupID = "";
                        }
                        else
                        {
                            sLookupID = (string)sqlReader.GetSqlString(3);
                        }

                        //Rec type
                        if (sqlReader.IsDBNull(4))
                        {
                            sRecType = "";
                        }
                        else
                        {
                            sRecType = (string)sqlReader.GetSqlString(4);
                        }

                        addData(oRecWorksheet, iRow, 1, sQuestion, "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                        addData(oRecWorksheet, iRow, 2, sRecLogic, "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                        addData(oRecWorksheet, iRow, 3, sRecText, "C" + iRow.ToString(), "C" + iRow.ToString(), "", "");
                        addData(oRecWorksheet, iRow, 4, sLookupID, "D" + iRow.ToString(), "E" + iRow.ToString(), "", "");
                        addData(oRecWorksheet, iRow, 5, sRecType, "E" + iRow.ToString(), "E" + iRow.ToString(), "", "");

                        iRow++;

                    } while (sqlReader.Read());     // Recs

                    tbResults.Text += (iRow - 3).ToString() + " recommendations found" + System.Environment.NewLine;

                }   // has rows
                else
                {
                    iRow = 5;
                    addData(oRecWorksheet, iRow, 1, "No Recommendations Found", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                    tbResults.Text += "No recommendations found" + System.Environment.NewLine;
                }

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;
            }


            ///////////////////////////////
            // HAZARDS
            ///////////////////////////////

            if (cbHaz.Checked)
            {

                tbResults.Text += "Creating Hazards Excel Sheet" + System.Environment.NewLine;

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetHazardsOnForm";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@formID", guFormID);
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (bFirstSheet)
                {
                    oHazWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    bFirstSheet = false;
                }
                else
                {
                    oHazWorksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing, oMissing, oMissing, oMissing);
                }

                oHazWorksheet.Name = "Hazards";
                createHeader(oHazWorksheet, sFormName, "HAZ");

                if (sqlReader.HasRows)
                {

                    iRow = 6;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //Question
                        if (sqlReader.IsDBNull(0))
                        {
                            sQuestion = "";
                        }
                        else
                        {
                            sQuestion = (string)sqlReader.GetSqlString(0);
                            sQuestion = sQuestion.Trim();
                        }

                        //Logic
                        if (sqlReader.IsDBNull(1))
                        {
                            sRecLogic = "";
                        }
                        else
                        {
                            sRecLogic = (string)sqlReader.GetSqlString(1);
                            sRecLogic = sRecLogic.Trim();
                        }

                        //Description
                        if (sqlReader.IsDBNull(2))
                        {
                            sDescription = "";
                        }
                        else
                        {
                            sDescription = (string)sqlReader.GetSqlString(2);
                            sDescription = sDescription.Trim();
                        }

                        // Score
                        if (sqlReader.IsDBNull(3))
                        {
                            iScore = 0;
                        }
                        else
                        {
                            iScore = (int)sqlReader.GetSqlInt32(3);
                        }

                        addData(oHazWorksheet, iRow, 1, sQuestion, "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                        addData(oHazWorksheet, iRow, 2, sDescription, "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                        addData(oHazWorksheet, iRow, 3, sRecLogic, "C" + iRow.ToString(), "C" + iRow.ToString(), "", "");
                        addData(oHazWorksheet, iRow, 4, iScore.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "", "");

                        iRow++;

                    } while (sqlReader.Read());     // Recs

                    tbResults.Text += (iRow - 4).ToString() + " hazards found" + System.Environment.NewLine;

                }   // has rows
                else
                {
                    iRow = 5;
                    addData(oHazWorksheet, iRow, 1, "No Hazards Found", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                    tbResults.Text += "No hazards found" + System.Environment.NewLine;
                }
                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;

            }

            ///////////////////////////////
            // PROMOTED FIELDS
            ///////////////////////////////

            if (cbPromoted.Checked)
            {

                tbResults.Text += "Creating Promoted Fields Excel Sheet" + System.Environment.NewLine;

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetPromotedFieldsOnForm";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@formID", guFormID);
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (bFirstSheet)
                {
                    oProWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    bFirstSheet = false;
                }
                else
                {
                    oProWorksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing, oMissing, oMissing, oMissing);
                }
                oProWorksheet.Name = "Promoted";
                createHeader(oProWorksheet, sFormName, "PRO");

                if (sqlReader.HasRows)
                {

                    iRow = 6;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //Question
                        if (sqlReader.IsDBNull(0))
                        {
                            sQuestion = "";
                        }
                        else
                        {
                            sQuestion = (string)sqlReader.GetSqlString(0);
                            sQuestion = sQuestion.Trim();
                        }

                        //db col name
                        if (sqlReader.IsDBNull(1))
                        {
                            sDBColName = "";
                        }
                        else
                        {
                            sDBColName = (string)sqlReader.GetSqlString(1);
                        }

                        //Promoted field
                        if (sqlReader.IsDBNull(2))
                        {
                            sPromotedField = "";
                        }
                        else
                        {
                            sPromotedField = (string)sqlReader.GetSqlString(2);
                        }

                        addData(oProWorksheet, iRow, 1, sQuestion, "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                        addData(oProWorksheet, iRow, 2, sDBColName, "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                        addData(oProWorksheet, iRow, 3, sPromotedField, "C" + iRow.ToString(), "C" + iRow.ToString(), "", "");

                        iRow++;

                    } while (sqlReader.Read());     // SQL

                    tbResults.Text += (iRow - 4).ToString() + " promoted fields found" + System.Environment.NewLine;

                }   // has rows
                else
                {
                    iRow = 5;
                    addData(oProWorksheet, iRow, 1, "No Promoted Fields Found", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                    tbResults.Text += "No Promoted Fields found" + System.Environment.NewLine;
                }
                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;

            }

            ///////////////////////////////
            // SHARED FIELDS
            ///////////////////////////////

            if (cbShared.Checked)
            {

                tbResults.Text += "Creating Hazards Excel Sheet" + System.Environment.NewLine;

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetSharedFieldsOnForm";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@formID", guFormID);
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (bFirstSheet)
                {
                    oShrWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    bFirstSheet = false;
                }
                else
                {
                    oShrWorksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing, oMissing, oMissing, oMissing);
                }

                oShrWorksheet.Name = "Shared Fields";
                createHeader(oShrWorksheet, sFormName, "SHR");

                if (sqlReader.HasRows)
                {

                    iRow = 6;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //Question
                        if (sqlReader.IsDBNull(0))
                        {
                            sQuestion = "";
                        }
                        else
                        {
                            sQuestion = (string)sqlReader.GetSqlString(0);
                            sQuestion = sQuestion.Trim();
                        }

                        //db col name
                        if (sqlReader.IsDBNull(1))
                        {
                            sDBColName = "";
                        }
                        else
                        {
                            sDBColName = (string)sqlReader.GetSqlString(1);
                        }

                        //Shared field
                        if (sqlReader.IsDBNull(2))
                        {
                            sSharedField = "";
                        }
                        else
                        {
                            sSharedField = (string)sqlReader.GetSqlString(2);
                        }

                        addData(oShrWorksheet, iRow, 1, sQuestion, "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                        addData(oShrWorksheet, iRow, 2, sDBColName, "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                        addData(oShrWorksheet, iRow, 3, sSharedField, "C" + iRow.ToString(), "C" + iRow.ToString(), "", "");

                        iRow++;

                    } while (sqlReader.Read());

                    tbResults.Text += (iRow - 4).ToString() + " shared fields found" + System.Environment.NewLine;

                }   // has rows
                else
                {
                    iRow = 5;
                    addData(oShrWorksheet, iRow, 1, "No Shared Fields Found", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                    tbResults.Text += "No Shared Fields Found" + System.Environment.NewLine;
                }
                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;

            }

            ///////////////////////////////
            // FORM INJECTIONS
            ///////////////////////////////

            if (cbInject.Checked)
            {

                tbResults.Text += "Creating Form Injection Excel Sheet" + System.Environment.NewLine;

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetInjectionsOnForm";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@formID", guFormID);
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (bFirstSheet)
                {
                    oInjWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    bFirstSheet = false;
                }
                else
                {
                    oInjWorksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing, oMissing, oMissing, oMissing);
                }

                oInjWorksheet.Name = "Injections";
                createHeader(oInjWorksheet, sFormName, "INJ");

                if (sqlReader.HasRows)
                {

                    iRow = 6;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //Question
                        if (sqlReader.IsDBNull(0))
                        {
                            sQuestion = "";
                        }
                        else
                        {
                            sQuestion = (string)sqlReader.GetSqlString(0);
                            sQuestion = sQuestion.Trim();
                        }

                        //RegEx
                        if (sqlReader.IsDBNull(1))
                        {
                            sRegEx = "";
                        }
                        else
                        {
                            sRegEx = (string)sqlReader.GetSqlString(1);
                        }

                        //Form
                        if (sqlReader.IsDBNull(2))
                        {
                            sForm = "";
                        }
                        else
                        {
                            sForm = (string)sqlReader.GetSqlString(2);
                        }

                        addData(oInjWorksheet, iRow, 1, sQuestion, "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                        addData(oInjWorksheet, iRow, 2, sRegEx, "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                        addData(oInjWorksheet, iRow, 3, sForm, "C" + iRow.ToString(), "C" + iRow.ToString(), "", "");

                        iRow++;

                    } while (sqlReader.Read());     // Recs

                    tbResults.Text += (iRow - 4).ToString() + " injections found" + System.Environment.NewLine;

                }   // has rows
                else
                {
                    iRow = 5;
                    addData(oInjWorksheet, iRow, 1, "No Form Injections Found", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                    tbResults.Text += "No Form Injections found" + System.Environment.NewLine;
                }
                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;
            }

            ///////////////////////////////
            // Validations
            ///////////////////////////////

            if (cbValidations.Checked)
            {

                tbResults.Text += "Creating Form Validation Excel Sheet" + System.Environment.NewLine;

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetValidationsOnForm";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@formID", guFormID);
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (bFirstSheet)
                {
                    oValWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    bFirstSheet = false;
                }
                else
                {
                    oValWorksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing, oMissing, oMissing, oMissing);
                }

                oValWorksheet.Name = "Validations";
                createHeader(oValWorksheet, sFormName, "VAL");

                if (sqlReader.HasRows)
                {

                    iRow = 6;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //dbColName
                        sDBColName = (string)sqlReader.GetSqlString(0);

                        //Question
                        if (sqlReader.IsDBNull(1))
                        {
                            sQuestion = "";
                        }
                        else
                        {
                            sQuestion = (string)sqlReader.GetSqlString(1);
                            sQuestion = sQuestion.Trim();
                        }

                        //Question to check
                        if (sqlReader.IsDBNull(2))
                        {
                            sQuestionToCheck = "";
                        }
                        else
                        {
                            sQuestionToCheck = (string)sqlReader.GetSqlString(2);
                            sQuestionToCheck = sQuestionToCheck.Trim();
                        }

                        //Other field
                        if (sqlReader.IsDBNull(3))
                        {
                            sOtherField = "";
                        }
                        else
                        {
                            sOtherField = (string)sqlReader.GetSqlString(3);
                            sOtherField = sOtherField.Trim();
                        }


                        //RegEx
                        if (sqlReader.IsDBNull(4))
                        {
                            sRegEx = "";
                        }
                        else
                        {
                            sRegEx = (string)sqlReader.GetSqlString(4);
                        }

                        addData(oValWorksheet, iRow, 1, sDBColName, "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                        addData(oValWorksheet, iRow, 2, sQuestion, "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                        addData(oValWorksheet, iRow, 3, sQuestionToCheck, "C" + iRow.ToString(), "C" + iRow.ToString(), "", "");
                        addData(oValWorksheet, iRow, 4, sOtherField, "D" + iRow.ToString(), "D" + iRow.ToString(), "", "");
                        addData(oValWorksheet, iRow, 5, sRegEx, "E" + iRow.ToString(), "E" + iRow.ToString(), "", "");

                        iRow++;

                    } while (sqlReader.Read());     // Validations

                    tbResults.Text += (iRow - 4).ToString() + " validations found" + System.Environment.NewLine;

                }   // has rows
                else
                {
                    iRow = 5;
                    addData(oInjWorksheet, iRow, 1, "No Validations Found", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                    tbResults.Text += "No Validations found" + System.Environment.NewLine;
                }

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;
            }

            ///////////////////////////////
            // All Questions
            ///////////////////////////////

            if (cbQuestions.Checked)
            {

                tbResults.Text += "Creating Form Question Excel Sheet" + System.Environment.NewLine;

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetQuestionsOnForm";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@formID", guFormID);
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (bFirstSheet)
                {
                    oValWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    bFirstSheet = false;
                }
                else
                {
                    oValWorksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing, oMissing, oMissing, oMissing);
                }

                oValWorksheet.Name = "Questions";
                createHeader(oValWorksheet, sFormName, "QUEST");

                if (sqlReader.HasRows)
                {

                    iRow = 6;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //dbColName
                        sDBColName = (string)sqlReader.GetSqlString(0);

                        //Question
                        if (sqlReader.IsDBNull(1))
                        {
                            sQuestion = "";
                        }
                        else
                        {
                            sQuestion = (string)sqlReader.GetSqlString(1);
                            sQuestion = sQuestion.Trim();
                        }

                        //Answer
                        if (sqlReader.IsDBNull(2))
                        {
                            sAnswer = "";
                        }
                        else
                        {
                            sAnswer = (string)sqlReader.GetSqlString(2);
                        }


                        //Value
                        if (sqlReader.IsDBNull(3))
                        {
                            sValue = "";
                        }
                        else
                        {
                            sValue = (string)sqlReader.GetSqlString(3);
                        }

                        //Color
                        if (sqlReader.IsDBNull(4))
                        {
                            sColor = "";
                        }
                        else
                        {
                            sColor = (string)sqlReader.GetSqlString(4);
                        }

                        addData(oValWorksheet, iRow, 1, sDBColName, "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                        addData(oValWorksheet, iRow, 2, sQuestion, "B" + iRow.ToString(), "B" + iRow.ToString(), "", "");
                        addData(oValWorksheet, iRow, 3, sAnswer, "C" + iRow.ToString(), "C" + iRow.ToString(), "", "");
                        addData(oValWorksheet, iRow, 4, sValue, "D" + iRow.ToString(), "D" + iRow.ToString(), "", "");
                        addData(oValWorksheet, iRow, 5, sColor, "E" + iRow.ToString(), "E" + iRow.ToString(), "", "");

                        iRow++;

                    } while (sqlReader.Read());     // Validations

                    tbResults.Text += (iRow - 4).ToString() + " questions found" + System.Environment.NewLine;

                }   // has rows
                else
                {
                    iRow = 5;
                    addData(oInjWorksheet, iRow, 1, "No Questions Found", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "");
                    tbResults.Text += "No Questions found" + System.Environment.NewLine;
                }

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;
            }

            try
            {

                oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.Close(true, oMissing, oMissing);
                oExcel.Quit();

                tbResults.Text += sExcelFileNameNoEx + " created successfully" + System.Environment.NewLine;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            releaseObject(oExcel);
            releaseObject(oWorkbook);
            releaseObject(oInjWorksheet);
            releaseObject(oRecWorksheet);


            return bRet;

        }

        private void createHeader(Excel._Worksheet oWorkSheet, string sFormName, string sMode)
        {

            Excel.Range oRange;

            if (sMode == "REC")
            {

                oWorkSheet.get_Range("A1", "E1").Merge(false);
                oRange = oWorkSheet.get_Range("A1", "E1");
                oRange.FormulaR1C1 = sFormName;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.get_Range("A3", "E3").Merge(false);
                oRange = oWorkSheet.get_Range("A3", "E3");
                oRange.FormulaR1C1 = "Recommendations";
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.Cells[5, 1] = "Question";
                oRange = oWorkSheet.get_Range("A5", "A5");
                oRange.ColumnWidth = 85;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 2] = "Logic";
                oRange = oWorkSheet.get_Range("B5", "B5");
                oRange.ColumnWidth = 25;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 3] = "Rec Text";
                oRange = oWorkSheet.get_Range("C5", "C5");
                oRange.ColumnWidth = 150;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 4] = "Lookup ID";
                oRange = oWorkSheet.get_Range("D5", "D5");
                oRange.ColumnWidth = 30;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 5] = "Rec Type";
                oRange = oWorkSheet.get_Range("E5", "E5");
                oRange.ColumnWidth = 15;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            }
            else if (sMode == "HAZ")
            {
                oWorkSheet.get_Range("A1", "D1").Merge(false);
                oRange = oWorkSheet.get_Range("A1", "D1");
                oRange.FormulaR1C1 = sFormName;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.get_Range("A3", "D3").Merge(false);
                oRange = oWorkSheet.get_Range("A3", "D3");
                oRange.FormulaR1C1 = "Hazards";
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.Cells[5, 1] = "Question";
                oRange = oWorkSheet.get_Range("A5", "A5");
                oRange.ColumnWidth = 55;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 2] = "Description";
                oRange = oWorkSheet.get_Range("B5", "B5");
                oRange.ColumnWidth = 75;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 3] = "Logic";
                oRange = oWorkSheet.get_Range("C5", "C5");
                oRange.ColumnWidth = 50;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 4] = "Hazard Score";
                oRange = oWorkSheet.get_Range("D5", "D5");
                oRange.ColumnWidth = 20;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            }
            else if (sMode == "PRO" || sMode == "SHR")
            {

                oWorkSheet.get_Range("A1", "C1").Merge(false);
                oRange = oWorkSheet.get_Range("A1", "C1");
                oRange.FormulaR1C1 = sFormName;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.get_Range("A3", "C3").Merge(false);
                oRange = oWorkSheet.get_Range("A3", "C3");
                if (sMode == "PRO")
                    oRange.FormulaR1C1 = "Promoted Fields";
                else
                    oRange.FormulaR1C1 = "Shared Fields";
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.Cells[5, 1] = "Question";
                oRange = oWorkSheet.get_Range("A5", "A5");
                oRange.ColumnWidth = 75;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 2] = "DB Col Name";
                oRange = oWorkSheet.get_Range("B5", "B5");
                oRange.ColumnWidth = 25;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                if (sMode == "SHR")
                    oWorkSheet.Cells[5, 3] = "Shared Field";
                else
                    oWorkSheet.Cells[5, 3] = "Promoted Field";
                oRange = oWorkSheet.get_Range("C5", "C5");
                oRange.ColumnWidth = 55;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            }
            else if (sMode == "INJ")
            {

                oWorkSheet.get_Range("A1", "C1").Merge(false);
                oRange = oWorkSheet.get_Range("A1", "C1");
                oRange.FormulaR1C1 = sFormName;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.get_Range("A3", "C3").Merge(false);
                oRange = oWorkSheet.get_Range("A3", "C3");
                oRange.FormulaR1C1 = "Form Injections";
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.Cells[5, 1] = "Question";
                oRange = oWorkSheet.get_Range("A5", "A5");
                oRange.ColumnWidth = 75;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 2] = "RegEx";
                oRange = oWorkSheet.get_Range("B5", "B5");
                oRange.ColumnWidth = 25;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 3] = "Form to be Injected";
                oRange = oWorkSheet.get_Range("C5", "C5");
                oRange.ColumnWidth = 55;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            }
            else if (sMode == "VAL")
            {

                oWorkSheet.get_Range("A1", "E1").Merge(false);
                oRange = oWorkSheet.get_Range("A1", "E1");
                oRange.FormulaR1C1 = sFormName;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.get_Range("A3", "E3").Merge(false);
                oRange = oWorkSheet.get_Range("A3", "E3");
                oRange.FormulaR1C1 = "Form Validations";
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.Cells[5, 1] = "dbColName";
                oRange = oWorkSheet.get_Range("A5", "A5");
                oRange.ColumnWidth = 30;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 2] = "Question";
                oRange = oWorkSheet.get_Range("B5", "B5");
                oRange.ColumnWidth = 60;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.WrapText = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 3] = "Question to check, generic field or token";
                oRange = oWorkSheet.get_Range("C5", "C5");
                oRange.ColumnWidth = 60;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.WrapText = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 4] = "Other field name";
                oRange = oWorkSheet.get_Range("D5", "D5");
                oRange.ColumnWidth = 30;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.WrapText = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 5] = "RegEx";
                oRange = oWorkSheet.get_Range("E5", "E5");
                oRange.ColumnWidth = 25;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.WrapText = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);


            }
            else if (sMode == "QUEST")
            {

                oWorkSheet.get_Range("A1", "E1").Merge(false);
                oRange = oWorkSheet.get_Range("A1", "E1");
                oRange.FormulaR1C1 = sFormName;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.get_Range("A3", "E3").Merge(false);
                oRange = oWorkSheet.get_Range("A3", "E3");
                oRange.FormulaR1C1 = "Form Questions";
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                oRange.Font.Size = 16;
                oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

                oWorkSheet.Cells[5, 1] = "dbColName";
                oRange = oWorkSheet.get_Range("A5", "A5");
                oRange.ColumnWidth = 30;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 2] = "Question";
                oRange = oWorkSheet.get_Range("B5", "B5");
                oRange.ColumnWidth = 30;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.WrapText = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 3] = "Answer";
                oRange = oWorkSheet.get_Range("C5", "C5");
                oRange.ColumnWidth = 30;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.WrapText = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 4] = "Value";
                oRange = oWorkSheet.get_Range("D5", "D5");
                oRange.ColumnWidth = 30;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.WrapText = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

                oWorkSheet.Cells[5, 5] = "Color";
                oRange = oWorkSheet.get_Range("E5", "E5");
                oRange.ColumnWidth = 25;
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                oRange.Font.Bold = true;
                oRange.WrapText = true;
                oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);


            }


        }

        private void addData(Excel._Worksheet oWorkSheet, int row, int col, string data,
        string cell1, string cell2, string format, string sHorizAlign)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            oRange.WrapText = true;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
        }

        private void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.WrapText = true;
        }

        private string buildFileName(string sFormName)
        {
            string sFileName = sFormName;
            string sCleanFileName = "";


            try
            {

                // remove any invalid characters
                char[] invalidChars = Path.GetInvalidPathChars();

                sCleanFileName = new string(sFileName.Where(x => !invalidChars.Contains(x)).ToArray());

                //Replace periods with -
                sCleanFileName = sCleanFileName.Replace('.', '-');
                sCleanFileName = sCleanFileName.Replace('/', '-');


                //!File.Exists(Path.Combine(sourceFolder, fileName));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return sCleanFileName;
        }

        private bool isValidFileName(string sFileName)
        {
            bool bRet = false;

            try
            {
                bRet = !string.IsNullOrEmpty(sFileName) &&
                sFileName.IndexOfAny(Path.GetInvalidFileNameChars()) < 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return bRet;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        private string StripHTML(string inputString)
        {
            string sRet = "";

            sRet = Regex.Replace
              (inputString, HTML_TAG_PATTERN, string.Empty);

            sRet = sRet.Trim();

            sRet = sRet.Replace("&nbsp;", " ");
            sRet = sRet.Replace("&quot;", "'");
            sRet = sRet.Replace("&ldquo", "'");
            sRet = sRet.Replace("&rdquo", "'");
            sRet = sRet.Replace("&#39;", "'");

            return sRet;
        }


    }
}
