﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Xceed.Grid;
using Xceed.Grid.Reporting;
using Xceed.Grid.Editors;
using Xceed.Grid.Viewers;
using Xceed.Grid.Collections;
using Xceed.Editors;
using Xceed.SmartUI;
using Xceed.SmartUI.Controls.ToolBar;
using Xceed.SmartUI.UIStyle;

namespace _360RecHaz
{
    public partial class frmViewData : Form
    {

        public DateTime dBegDate { get; set; }
        public DateTime dEndDate { get; set; }
        public string sAcnts { get; set; }
        private string cfg_SQLMainSIBIConnStr;
        private string cfg_SQLMainUtilConnStr;
        private string cfg_360UtilConnStr;
        
        private string rptTitle = "";
        private Xceed.Grid.GridControl xNewGrid = new GridControl();
        ColumnManagerRow xNewGridColumnManagerRow;

        private int miTotReq;
        private int miTotAcnt;

        private DataTable dtSIBI;
        private DataTable dt360;
        private DataTable dtCombined;
        private DataSet dsReportData;

        public frmViewData()
        {
            InitializeComponent();
        }

        private void frmViewData_Load(object sender, EventArgs e)
        {

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");
            cfg_SQLMainUtilConnStr = colNameVal.Get("SQLMainUtilConnStr");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");

            rptTitle = "Survey count for " + dBegDate.ToShortDateString() + " - " + dEndDate.ToShortDateString();

            // place grid on panel
            this.panelMain.Controls.Add(xNewGrid);
            
            // init SQL connection for main query
            //SqlConnection sqlConn = new SqlConnection(cfg_SQLMainSIBIConnStr);
            //SqlCommand sqlCommand = new SqlCommand();
            //sqlCommand.CommandType = CommandType.StoredProcedure;
            //sqlCommand.Connection = sqlConn;

            dtSIBI = new DataTable("SIBI");
            dtSIBI.Columns.Add("acnt",typeof(int));
            dtSIBI.Columns.Add("name", typeof(string));
            dtSIBI.Columns.Add("numreq",typeof(int));
            dtSIBI.Columns.Add("invoiced", typeof(int));
            dtSIBI.Columns.Add("amount", typeof(decimal));
            dtSIBI.Columns.Add("avgamount", typeof(decimal));

            dt360 = new DataTable("360");
            dt360.Columns.Add("acnt", typeof(int));
            dt360.Columns.Add("name", typeof(string));
            dt360.Columns.Add("req", typeof(int));
            dt360.Columns.Add("inv", typeof(int));
            dt360.Columns.Add("amount", typeof(decimal));
            dt360.Columns.Add("avgamount", typeof(decimal));

            dtCombined = new DataTable("Combined");
            dtCombined.Columns.Add("acnt", typeof(int));
            dtCombined.Columns.Add("name", typeof(string));
            dtCombined.Columns.Add("req", typeof(int));
            dtCombined.Columns.Add("inv", typeof(int));
            dtCombined.Columns.Add("amount", typeof(decimal));
            dtCombined.Columns.Add("avgamount", typeof(decimal));

            // Create a DataSet.
            dsReportData = new DataSet("reportdata");
            dsReportData.Tables.Add(dtSIBI);
            dsReportData.Tables.Add(dt360);
            dsReportData.Tables.Add(dtCombined);
            
            try
            {
                string[] sAcntList = sAcnts.Split(',');

                foreach (string sAcnt in sAcntList)
                {
                    if (sAcnt.Length > 0)
                    {
                        // Get data from SIBOffice
                        if (!procSIBI(sAcnt))
                        {
                            throw new Exception("Error loading data from SIBOffice");
                        }

                        // Get data from 360
                        if (!proc360(sAcnt))
                        {
                            throw new Exception("Error loading data from 360");
                        }
                    }
                }

                if (!combineTables())
                {
                    throw new Exception("Error loading data.");
                }

                xNewGrid.Clear();
                this.PrepareGrid();                          
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                //tSSlblStatus.Text = "Status: Ready";
                //tSSlblStatus.BackColor = System.Drawing.Color.LightGray;
                this.Cursor = Cursors.Default;
            }

        }

        private void PrepareGrid()
        {

            try
            {

                tSSlblStatus.Text = "Status: Loading...";
                tSSlblStatus.BackColor = System.Drawing.Color.Yellow;
                tSSlblCount.Text = "Total Requested: ";
                sStrip1.Refresh();
                this.Cursor = Cursors.WaitCursor;
                this.Refresh();
                Application.DoEvents();
                
                // header row
                xNewGridColumnManagerRow = new ColumnManagerRow();
                xNewGrid.FixedHeaderRows.Add(xNewGridColumnManagerRow);

                //COLUMNS
                xNewGrid.Columns.Add(new Column("Acnt", typeof(int)));
                xNewGrid.Columns["Acnt"].Title = "Account";
                xNewGrid.Columns["Acnt"].Width = 100;
                xNewGrid.Columns["Acnt"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["Acnt"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;

                xNewGrid.Columns.Add(new Column("Name", typeof(string)));
                xNewGrid.Columns["Name"].Title = "Name";
                xNewGrid.Columns["Name"].Width = 165;
                xNewGrid.Columns["Name"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Left;
                xNewGrid.Columns["Name"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;

                xNewGrid.Columns.Add(new Column("Req", typeof(int)));
                xNewGrid.Columns["Req"].Title = "# Ordered";
                xNewGrid.Columns["Req"].Width = 100;
                xNewGrid.Columns["Req"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["Req"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;

                xNewGrid.Columns.Add(new Column("Inv", typeof(int)));
                xNewGrid.Columns["Inv"].Title = "# Invoiced";
                xNewGrid.Columns["Inv"].Width = 100;
                xNewGrid.Columns["Inv"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["Inv"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;

                xNewGrid.Columns.Add(new Column("Amount", typeof(decimal)));
                xNewGrid.Columns["Amount"].Title = "Total Amount";
                xNewGrid.Columns["Amount"].Width = 100;
                xNewGrid.Columns["Amount"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["Amount"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["Amount"].FormatSpecifier = "c2";

                xNewGrid.Columns.Add(new Column("Avg", typeof(decimal)));
                xNewGrid.Columns["Avg"].Title = "Average Cost";
                xNewGrid.Columns["Avg"].Width = 100;
                xNewGrid.Columns["Avg"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["Avg"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["Avg"].FormatSpecifier = "c2";


                //grid wide settings
                xNewGrid.ReadOnly = true;
                xNewGrid.Dock = DockStyle.Fill;
                xNewGrid.FixedColumnSplitter.Visible = false;
                xNewGrid.RowSelectorPane.Visible = false;

                // prevent cell navigation
                xNewGrid.AllowCellNavigation = false;

                // resize
                xNewGrid.Resize += new System.EventHandler(xNewGrid_Resize);

                // declare vars
                miTotReq = 0;
                int iWork = 0;
                int iAcntTotal = 0;
                string sWork = "";
                               
                // load data from dataset
                foreach (System.Data.DataRow row in dtCombined.Rows)
                {

                    Xceed.Grid.DataRow dataRow = xNewGrid.DataRows.AddNew();

                    dataRow.Cells["Acnt"].Value = row["acnt"];
                    dataRow.Cells["Name"].Value = row["name"];
                    dataRow.Cells["Req"].Value = row["req"];
                    dataRow.Cells["Inv"].Value = row["inv"];
                    dataRow.Cells["Amount"].Value = row["amount"];
                    dataRow.Cells["Avg"].Value = row["avgamount"];

                    dataRow.EndEdit();
                    miTotAcnt++;

                }
                xNewGrid_Resize(null, null);



                 tSSlblCount.Text = "Total requested: " + miTotReq.ToString("#,#");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                tSSlblStatus.Text = "Status: Ready";
                tSSlblStatus.BackColor = System.Drawing.Color.LightGray;
                this.Cursor = Cursors.Default;
            }
        }

        // Grid resize
        private void xNewGrid_Resize(object sender, System.EventArgs e)
        {
            int iAvailWidth = xNewGrid.DisplayRectangle.Width;
            int iTotColWidth = 0;
            int iResizeCol = 1;
            int iAdjustment = 0;

            if (iAvailWidth == 0)
                return;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xNewGrid.Columns)
            {
                if (column.Index != iResizeCol)
                {
                    Console.WriteLine(column.FieldName);
                    iTotColWidth += column.Width;
                }

            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xNewGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xNewGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.Width = iWidth;
            }
            else
            {
                //xResizeColumn.Width = iWidth;
            }
        }

        // resize grid for print
        private void xNewGrid_ResizeForPrint(int iAvailWidth)
        {
            int iTotColWidth = 0;
            int iResizeCol = 0;
            int iAdjustment = 0;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xNewGrid.Columns)
            {
                if (column.Index != iResizeCol)
                    iTotColWidth += column.Width;
            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xNewGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xNewGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
            else
            {
                //xResizeColumn.ReportStyle.Width = iWidth;
            }
        }

        private bool procSIBI(string sAcnt)
        {

            bool bRet = false;
            int iAcnt;
            string sAcntName = "";
            int iNumReq = 0;
            int iNumInv = 0;
            decimal decTotInv = 0;
            decimal decAvgInv = 0;

            SqlConnection sqlConn2 = null;
            SqlCommand sqlCmd2 = null;

           
            try
            {

                iAcnt = Convert.ToInt32(sAcnt);

                // init SQL connection for read query
                SqlDataReader sqlDR;
                sqlConn2 = new SqlConnection(cfg_SQLMainSIBIConnStr);
                sqlCmd2 = new SqlCommand();
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.Connection = sqlConn2;
                sqlCmd2.CommandText = "sp_360RecHazReport_GetAcnt";
                sqlConn2.Open();

                sqlCmd2.Parameters.Clear();
                sqlCmd2.Parameters.AddWithValue("@begdate", dBegDate);
                sqlCmd2.Parameters.AddWithValue("@enddate", dEndDate);
                sqlCmd2.Parameters.AddWithValue("@acntnum", iAcnt);
                sqlDR = sqlCmd2.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        iNumReq = (int)sqlDR.GetSqlInt32(0);
                        iNumInv = (int)sqlDR.GetSqlInt32(1);
                        decTotInv = (decimal)sqlDR.GetSqlMoney(2);
                        decAvgInv = (decimal)sqlDR.GetSqlMoney(3);
                        sAcntName = sqlDR.GetSqlString(4).ToString();

                        dtSIBI.Rows.Add(iAcnt,sAcntName,iNumReq,iNumInv,decTotInv,decAvgInv);

                    }	// while


                }	// has rows
                else
                {

                    dtSIBI.Rows.Add(iAcnt, "NO DATA FOUND", 0, 0, 0.00, 0.00);

                }

                bRet = true;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {
                if (sqlConn2 != null)
                    sqlConn2.Close();

            }
            
            return bRet;
        }

        private bool proc360(string sAcnt)
        {

            bool bRet = false;
            int iAcnt;
            DateTime dDateReq;
            int iNumReq;




            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // init SQL connection for read query
                SqlDataReader sqlDR;
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;

                // get items requested for the period
                sqlCmd1.CommandText = "dbo.sp_360RecHazRpt";
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate);
                sqlCmd1.Parameters.AddWithValue("@acntnum", dEndDate);
                sqlConn1.Open();
                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        dDateReq = sqlDR.GetDateTime(0);
                        sAcnt = sqlDR.GetSqlString(1).ToString();
                        iAcnt = Convert.ToInt32(sAcnt);
                        iNumReq = (int)sqlDR.GetSqlInt32(2);

                        dt360.Rows.Add(iAcnt, dDateReq, iNumReq);

                    }	// while


                }	// has rows
                else
                {

                    MessageBox.Show("There were no requests found in 360 for the selected date range.\r\nPlease check your settings.", "No data found", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

                bRet = true;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {
                if (sqlConn1 != null)

                    sqlConn1.Close();


            }

            return bRet;
        }

        private bool combineTables()
        {


            //dtSIBI.Columns.Add("acnt", typeof(int));
            //dtSIBI.Columns.Add("name", typeof(string));
            //dtSIBI.Columns.Add("numreq", typeof(int));
            //dtSIBI.Columns.Add("invoiced", typeof(int));
            //dtSIBI.Columns.Add("amount", typeof(double));
            //dtSIBI.Columns.Add("avgamount", typeof(double));

            bool bRet = false;
            DateTime dCurDate = dBegDate;
            DateTime dLastDate = dEndDate.AddDays(1);
            string sCurDate = "";
            int iAcnt = 0;
            string sAcntName = "";
            int iNumReq = 0;
            int iNumReqWork = 0;
            int iNumInv = 0;
            int iNumInvWork = 0;
            decimal decAmount = 0;
            decimal decAMountWork = 0;
            decimal decAvgAmount = 0;
            decimal decAvgAmountWork = 0;
            int iWork = 0;

            try 
            {
                // SIBI
                foreach (System.Data.DataRow row in dtSIBI.Rows)
                {

                    // get data from current row
                    iAcnt = (int)row["acnt"];
                    sAcntName = (string)row["name"];
                    iNumReq = (int)row["numreq"];
                    iNumInv = (int)row["invoiced"];
                    decAmount = (decimal)row["amount"];
                    decAvgAmount = (decimal)row["avgamount"];

                    // if row for acnt does not exist in Combined table - add it
                    System.Data.DataRow[] CombRow = dtCombined.Select("acnt = " + iAcnt);

                    dtCombined.Rows.Add(iAcnt,sAcntName,iNumReq,iNumInv,decAmount,decAvgAmount);
                    
                    // add count to appropriate date column
                    //CombRow = dtCombined.Select("acnt = " + iAcnt);
                    //iWork = (int)CombRow[0][sCurDate];

                                   
                }

                // 360
                //foreach (System.Data.DataRow row in dt360.Rows)
                //{

                //    // get data from current row
                //    dCurDate = (DateTime)row["datereq"];
                //    iAcnt = (int)row["acnt"];
                //    iNumReq = (int)row["numreq"];

                //    // if row for acnt does not exist in Combined table - add it
                //    System.Data.DataRow[] CombRow = dtCombined.Select("acnt = " + iAcnt);

                //    if (CombRow.Length == 0)
                //    {
                //        dtCombined.Rows.Add(iAcnt,sAcntName,0,0,0.00,0.00);
                //    }

                //    // add count to appropriate date column
                //    sCurDate = dCurDate.Day.ToString();
                //    CombRow = dtCombined.Select("acnt = " + iAcnt);
                //    iWork = (int)CombRow[0][sCurDate];
                //    CombRow[0][sCurDate] = iWork + iNumReq;

                //}


                bRet = true;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


            return bRet;
        
        }

 
        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.xNewGrid.ReportSettings.Title = rptTitle;
            this.xNewGrid.ReportSettings.Landscape = true;
            this.xNewGrid.ReportSettings.ColumnLayout = ColumnLayout.FitToPage;

            //xNewGrid_ResizeForPrint(1000);

            // base font for the report
            Font font = new Font("Arial", 8);
            this.xNewGrid.ReportStyle.Font = font;

            Report report = new Report(xNewGrid);
            ReportStyleSheet reportStyleSheet = report.ReportStyleSheet;

            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            reportStyleSheet.Grid.DataRows[0].BackColor = Color.FromArgb(205, 255, 205);
            reportStyleSheet.Grid.DataRows[1].BackColor = Color.Transparent;

            report.ReportStyleSheet.PageHeader.BottomBorder.Thickness = ReportLineThickness.SingleThin;
            report.ReportStyleSheet.PageHeader.BottomMargin = 5;
            report.ReportStyleSheet.PageHeader.Font = new Font(xNewGrid.ReportStyle.Font.FontFamily, 8, FontStyle.Bold);

            this.xNewGridColumnManagerRow.ReportStyle.RepeatOnEachPage = true;
            this.xNewGridColumnManagerRow.ReportStyle.BackColor = Color.DarkGray;
            this.xNewGridColumnManagerRow.ReportStyle.ForeColor = Color.White;
            this.xNewGridColumnManagerRow.ReportStyle.Font = new Font("Arial", 8, FontStyle.Bold);
            this.xNewGridColumnManagerRow.ReportStyle.WordWrap = true;
            this.xNewGridColumnManagerRow.ReportStyle.RowHeightExpansion = RowHeightExpansion.ExpandWithoutLimits;
            this.xNewGridColumnManagerRow.ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            this.xNewGridColumnManagerRow.ReportStyle.VerticalAlignment = VerticalAlignment.Bottom;

            report.ReportStyleSheet.PageHeader.LeftElement.TextFormat = rptTitle;
            report.ReportStyleSheet.PageHeader.RightElement.TextFormat =
              "Page : %Page format=000%" + Environment.NewLine +
              "Date : %DateTime format=g%";

            report.ReportStyleSheet.PageFooter.Font = new Font(xNewGrid.ReportStyle.Font.FontFamily, 8, FontStyle.Bold);
            report.ReportStyleSheet.PageFooter.TopBorder.Thickness = ReportLineThickness.SingleThin;
            report.ReportStyleSheet.PageFooter.LeftElement.TextFormat = miTotAcnt.ToString() + " accounts";
            report.ReportStyleSheet.PageFooter.RightElement.TextFormat = "Total requested: " + miTotReq.ToString("N0");

            report.PrintPreview();
        }


       
    }
}
