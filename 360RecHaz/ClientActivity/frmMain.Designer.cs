﻿namespace _360RecHaz
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.bGo = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.pnlAccounts = new System.Windows.Forms.Panel();
            this.xFormList = new Xceed.SmartUI.Controls.CheckedListBox.SmartCheckedListBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.cbRecs = new System.Windows.Forms.CheckBox();
            this.cbHaz = new System.Windows.Forms.CheckBox();
            this.fbdOutputDir = new System.Windows.Forms.FolderBrowserDialog();
            this.tbOutputDir = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bChangeOutputDir = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbResults = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbPromoted = new System.Windows.Forms.CheckBox();
            this.cbShared = new System.Windows.Forms.CheckBox();
            this.cbInject = new System.Windows.Forms.CheckBox();
            this.cbValidations = new System.Windows.Forms.CheckBox();
            this.cbIncludeZ = new System.Windows.Forms.CheckBox();
            this.cbQuestions = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.xFormList)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label2.Location = new System.Drawing.Point(21, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(304, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Select the type of output you would like:";
            // 
            // bGo
            // 
            this.bGo.Location = new System.Drawing.Point(143, 588);
            this.bGo.Margin = new System.Windows.Forms.Padding(4);
            this.bGo.Name = "bGo";
            this.bGo.Size = new System.Drawing.Size(127, 46);
            this.bGo.TabIndex = 5;
            this.bGo.Text = "Create Report";
            this.bGo.UseVisualStyleBackColor = true;
            this.bGo.Click += new System.EventHandler(this.bGo_Click);
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(328, 588);
            this.bCancel.Margin = new System.Windows.Forms.Padding(4);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(104, 46);
            this.bCancel.TabIndex = 6;
            this.bCancel.Text = "Close";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // pnlAccounts
            // 
            this.pnlAccounts.Location = new System.Drawing.Point(16, 155);
            this.pnlAccounts.Margin = new System.Windows.Forms.Padding(4);
            this.pnlAccounts.Name = "pnlAccounts";
            this.pnlAccounts.Size = new System.Drawing.Size(560, 289);
            this.pnlAccounts.TabIndex = 13;
            // 
            // xFormList
            // 
            this.xFormList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xFormList.Location = new System.Drawing.Point(0, 0);
            this.xFormList.Name = "xFormList";
            this.xFormList.Size = new System.Drawing.Size(144, 94);
            this.xFormList.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label5.Location = new System.Drawing.Point(21, 132);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(204, 20);
            this.label5.TabIndex = 15;
            this.label5.Text = "Select one or more forms:";
            // 
            // cbRecs
            // 
            this.cbRecs.AutoSize = true;
            this.cbRecs.Location = new System.Drawing.Point(25, 57);
            this.cbRecs.Margin = new System.Windows.Forms.Padding(4);
            this.cbRecs.Name = "cbRecs";
            this.cbRecs.Size = new System.Drawing.Size(147, 21);
            this.cbRecs.TabIndex = 16;
            this.cbRecs.Text = "Recommendations";
            this.cbRecs.UseVisualStyleBackColor = true;
            // 
            // cbHaz
            // 
            this.cbHaz.AutoSize = true;
            this.cbHaz.Location = new System.Drawing.Point(25, 85);
            this.cbHaz.Margin = new System.Windows.Forms.Padding(4);
            this.cbHaz.Name = "cbHaz";
            this.cbHaz.Size = new System.Drawing.Size(83, 21);
            this.cbHaz.TabIndex = 17;
            this.cbHaz.Text = "Hazards";
            this.cbHaz.UseVisualStyleBackColor = true;
            // 
            // fbdOutputDir
            // 
            this.fbdOutputDir.RootFolder = System.Environment.SpecialFolder.MyDocuments;
            // 
            // tbOutputDir
            // 
            this.tbOutputDir.Location = new System.Drawing.Point(21, 495);
            this.tbOutputDir.Margin = new System.Windows.Forms.Padding(4);
            this.tbOutputDir.Name = "tbOutputDir";
            this.tbOutputDir.Size = new System.Drawing.Size(549, 22);
            this.tbOutputDir.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(21, 471);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(506, 20);
            this.label1.TabIndex = 19;
            this.label1.Text = "Data will be written to Excel files and placed in the following folder.";
            // 
            // bChangeOutputDir
            // 
            this.bChangeOutputDir.Location = new System.Drawing.Point(185, 527);
            this.bChangeOutputDir.Margin = new System.Windows.Forms.Padding(4);
            this.bChangeOutputDir.Name = "bChangeOutputDir";
            this.bChangeOutputDir.Size = new System.Drawing.Size(221, 28);
            this.bChangeOutputDir.TabIndex = 20;
            this.bChangeOutputDir.Text = "Change output file location";
            this.bChangeOutputDir.UseVisualStyleBackColor = true;
            this.bChangeOutputDir.Click += new System.EventHandler(this.bChangeOutputDir_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tbResults);
            this.panel1.Location = new System.Drawing.Point(16, 683);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(556, 182);
            this.panel1.TabIndex = 21;
            // 
            // tbResults
            // 
            this.tbResults.CausesValidation = false;
            this.tbResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbResults.Location = new System.Drawing.Point(0, 0);
            this.tbResults.Margin = new System.Windows.Forms.Padding(4);
            this.tbResults.Multiline = true;
            this.tbResults.Name = "tbResults";
            this.tbResults.ReadOnly = true;
            this.tbResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbResults.Size = new System.Drawing.Size(556, 182);
            this.tbResults.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label3.Location = new System.Drawing.Point(12, 660);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 20);
            this.label3.TabIndex = 22;
            this.label3.Text = "Results:";
            // 
            // cbPromoted
            // 
            this.cbPromoted.AutoSize = true;
            this.cbPromoted.Location = new System.Drawing.Point(259, 57);
            this.cbPromoted.Margin = new System.Windows.Forms.Padding(4);
            this.cbPromoted.Name = "cbPromoted";
            this.cbPromoted.Size = new System.Drawing.Size(128, 21);
            this.cbPromoted.TabIndex = 23;
            this.cbPromoted.Text = "Promoted fields";
            this.cbPromoted.UseVisualStyleBackColor = true;
            // 
            // cbShared
            // 
            this.cbShared.AutoSize = true;
            this.cbShared.Location = new System.Drawing.Point(259, 86);
            this.cbShared.Margin = new System.Windows.Forms.Padding(4);
            this.cbShared.Name = "cbShared";
            this.cbShared.Size = new System.Drawing.Size(113, 21);
            this.cbShared.TabIndex = 24;
            this.cbShared.Text = "Shared fields";
            this.cbShared.UseVisualStyleBackColor = true;
            // 
            // cbInject
            // 
            this.cbInject.AutoSize = true;
            this.cbInject.Location = new System.Drawing.Point(423, 57);
            this.cbInject.Margin = new System.Windows.Forms.Padding(4);
            this.cbInject.Name = "cbInject";
            this.cbInject.Size = new System.Drawing.Size(125, 21);
            this.cbInject.TabIndex = 25;
            this.cbInject.Text = "Form injections";
            this.cbInject.UseVisualStyleBackColor = true;
            // 
            // cbValidations
            // 
            this.cbValidations.AutoSize = true;
            this.cbValidations.Location = new System.Drawing.Point(423, 86);
            this.cbValidations.Margin = new System.Windows.Forms.Padding(4);
            this.cbValidations.Name = "cbValidations";
            this.cbValidations.Size = new System.Drawing.Size(99, 21);
            this.cbValidations.TabIndex = 26;
            this.cbValidations.Text = "Validations";
            this.cbValidations.UseVisualStyleBackColor = true;
            // 
            // cbIncludeZ
            // 
            this.cbIncludeZ.AutoSize = true;
            this.cbIncludeZ.Location = new System.Drawing.Point(423, 131);
            this.cbIncludeZ.Margin = new System.Windows.Forms.Padding(4);
            this.cbIncludeZ.Name = "cbIncludeZ";
            this.cbIncludeZ.Size = new System.Drawing.Size(131, 21);
            this.cbIncludeZ.TabIndex = 27;
            this.cbIncludeZ.Text = "Include Z Forms";
            this.cbIncludeZ.UseVisualStyleBackColor = true;
            // 
            // cbQuestions
            // 
            this.cbQuestions.AutoSize = true;
            this.cbQuestions.Location = new System.Drawing.Point(131, 86);
            this.cbQuestions.Margin = new System.Windows.Forms.Padding(4);
            this.cbQuestions.Name = "cbQuestions";
            this.cbQuestions.Size = new System.Drawing.Size(94, 21);
            this.cbQuestions.TabIndex = 28;
            this.cbQuestions.Text = "Questions";
            this.cbQuestions.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(592, 880);
            this.Controls.Add(this.cbQuestions);
            this.Controls.Add(this.cbIncludeZ);
            this.Controls.Add(this.cbValidations);
            this.Controls.Add(this.cbInject);
            this.Controls.Add(this.cbShared);
            this.Controls.Add(this.cbPromoted);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bChangeOutputDir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbOutputDir);
            this.Controls.Add(this.cbHaz);
            this.Controls.Add(this.cbRecs);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pnlAccounts);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bGo);
            this.Controls.Add(this.label2);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.Text = "360 Recomendations & Hazards by Form";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xFormList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bGo;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Panel pnlAccounts;
        private Xceed.SmartUI.Controls.CheckedListBox.SmartCheckedListBox xFormList;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cbRecs;
        private System.Windows.Forms.CheckBox cbHaz;
        private System.Windows.Forms.FolderBrowserDialog fbdOutputDir;
        private System.Windows.Forms.TextBox tbOutputDir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bChangeOutputDir;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbResults;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbPromoted;
        private System.Windows.Forms.CheckBox cbShared;
        private System.Windows.Forms.CheckBox cbInject;
        private System.Windows.Forms.CheckBox cbValidations;
        private System.Windows.Forms.CheckBox cbIncludeZ;
        private System.Windows.Forms.CheckBox cbQuestions;
    }
}

