﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SurveyCountDaily
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            // default to current mont to date
            DateTime dNow = DateTime.Now;
            DateTime dBegDate = dNow.AddDays(-(dNow.Day - 1));
            DateTime dEndDate = new DateTime(dNow.Year, dNow.Month, 1).AddMonths(1).AddDays(-1);

            dtpBegin.Value = dBegDate;
            dtpEnd.Value = dEndDate;

            rbCurMonth.Checked = true;
            gbCustom.Enabled = false;

            bGo.Focus();
            
        }

        private void bGo_Click(object sender, EventArgs e)
        {

            DateTime dNow = DateTime.Today;
            DateTime dBegDate = dNow.AddDays(-(dNow.Day - 1));
            DateTime dEndDate = new DateTime(dNow.Year, dNow.Month, 1).AddMonths(1).AddDays(-1);

            // set dates based on user selection
            if (rbCurMonth.Checked)
            {
                dBegDate = dNow.AddDays(-(dNow.Day - 1));
                dEndDate = new DateTime(dNow.Year, dNow.Month, 1).AddMonths(1).AddDays(-1);
            }
            else if (rbPrevMonth.Checked)
            {
                DateTime month = new DateTime(dNow.Year, dNow.Month, 1);
                dBegDate = month.AddMonths(-1);
                dEndDate = month.AddDays(-1);
            }
            else // custom
            {
                dBegDate = dtpBegin.Value;
                dEndDate = dtpEnd.Value;
            }

            // set times to 12:00am and Midnight to pick up all items
            dBegDate = ChangeTime(dBegDate, 0, 0, 0, 0);
            dEndDate = ChangeTime(dEndDate, 23, 59, 59, 0);

            TimeSpan period = dEndDate-dBegDate;

            // check dates
            if (period.TotalDays < 0)
            {
                MessageBox.Show("Beginning date cannot be greater than end date", "Invalid period", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // 31 days max          
            if (period.TotalDays > 31)
            {
                MessageBox.Show("Report period cannot be greater than 31 days", "Invalid period", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;            
            }

            frmViewData frm = new frmViewData();
            frm.dBegDate = dBegDate;
            frm.dEndDate = dEndDate;
            frm.Text = "Survey count for " + dBegDate.ToShortDateString() + " - " + dEndDate.ToShortDateString();
            frm.ShowDialog();
            frm.Close();
            this.Show();
            

        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rbCustom_CheckedChanged(object sender, EventArgs e)
        {
            gbCustom.Enabled = rbCustom.Checked;
        }

        private DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

       
    }
}
