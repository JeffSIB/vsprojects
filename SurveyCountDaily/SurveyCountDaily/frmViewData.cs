﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Xceed.Grid;
using Xceed.Grid.Reporting;
using Xceed.Grid.Editors;
using Xceed.Grid.Viewers;
using Xceed.Grid.Collections;
using Xceed.Editors;

namespace SurveyCountDaily
{
    public partial class frmViewData : Form
    {

        public DateTime dBegDate { get; set; }
        public DateTime dEndDate { get; set; }
        private DateTime dBegDate360;
        private DateTime dEndDate360;
        private string cfg_SQLMainSIBIConnStr;
        private string cfg_SQLMainUtilConnStr;
        private string cfg_360UtilConnStr;
        
        private string rptTitle = "";
        private Xceed.Grid.GridControl xNewGrid = new GridControl();
        ColumnManagerRow xNewGridColumnManagerRow;

        private int miTotReq;
        private int miTotAcnt;

        private DataTable dtSIBI;
        private DataTable dt360;
        private DataTable dtCombined;
        private DataSet dsReportData;
        private int cfg_TimeZoneOffset;

        public frmViewData()
        {
            InitializeComponent();
        }

        private void frmViewData_Load(object sender, EventArgs e)
        {

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");
            cfg_SQLMainUtilConnStr = colNameVal.Get("SQLMainUtilConnStr");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
            cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));

            rptTitle = "Survey count for " + dBegDate.ToShortDateString() + " - " + dEndDate.ToShortDateString();

            // place grid on panel
            this.panelMain.Controls.Add(xNewGrid);
            
            // init SQL connection for main query
            //SqlConnection sqlConn = new SqlConnection(cfg_SQLMainSIBIConnStr);
            //SqlCommand sqlCommand = new SqlCommand();
            //sqlCommand.CommandType = CommandType.StoredProcedure;
            //sqlCommand.Connection = sqlConn;

            //dtSIBI = new DataTable("SIBI");
            //dtSIBI.Columns.Add("acnt",typeof(int));
            //dtSIBI.Columns.Add("datereq",typeof(DateTime));
            //dtSIBI.Columns.Add("numreq",typeof(int));

            dt360 = new DataTable("360");
            dt360.Columns.Add("acnt",typeof(int));
            dt360.Columns.Add("datereq",typeof(DateTime));
            dt360.Columns.Add("numreq",typeof(int));

            dtCombined = new DataTable("Combined");

            // Create a DataSet.
            dsReportData = new DataSet("reportdata");
            //dsReportData.Tables.Add(dtSIBI);
            dsReportData.Tables.Add(dt360);
            //dsReportData.Tables.Add(dtCombined);

            try
            {

                // Build combined table
                // Add columns titled 1 - 31
                int iCurCol = 1;

                dtCombined.Columns.Add("acnt", typeof(int));

                while (iCurCol < 32)
                {                    
                    dtCombined.Columns.Add(iCurCol.ToString(), typeof(int));
                    iCurCol ++;
                }

                // Get data from SIBOffice
                //if (!procSIBI())
                //{
                //    throw new Exception("Error loading data from SIBOffice");
                //}

                // Get data from 360
                if (!proc360())
                {
                    throw new Exception("Error loading data from 360");
                }

                if (!combineTables())
                {
                    throw new Exception("Error loading data.");
                }

                xNewGrid.Clear();
                this.PrepareGrid();                          
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                //tSSlblStatus.Text = "Status: Ready";
                //tSSlblStatus.BackColor = System.Drawing.Color.LightGray;
                this.Cursor = Cursors.Default;
            }

        }

        private void PrepareGrid()
        {

            try
            {

                tSSlblStatus.Text = "Status: Loading...";
                tSSlblStatus.BackColor = System.Drawing.Color.Yellow;
                tSSlblCount.Text = "Total Requested: ";
                sStrip1.Refresh();
                this.Cursor = Cursors.WaitCursor;
                this.Refresh();
                Application.DoEvents();
                
                // header row
                xNewGridColumnManagerRow = new ColumnManagerRow();
                xNewGrid.FixedHeaderRows.Add(xNewGridColumnManagerRow);

                //COLUMNS
                xNewGrid.Columns.Add(new Column("Acnt", typeof(int)));
                xNewGrid.Columns["Acnt"].Title = "Account";
                xNewGrid.Columns["Acnt"].Width = 65;
                xNewGrid.Columns["Acnt"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["Acnt"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;

                // Add column for each day in date range - max 31
                DateTime dCurDate = dBegDate;
                DateTime dLastDate = dEndDate;
                string sCurDate = "";
                while (dCurDate < dLastDate)
                {
                    sCurDate = dCurDate.Day.ToString();
                    xNewGrid.Columns.Add(new Column(sCurDate, typeof(string)));
                    xNewGrid.Columns[sCurDate].Title = sCurDate;
                    xNewGrid.Columns[sCurDate].Width = 35;
                    xNewGrid.Columns[sCurDate].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                    xNewGrid.Columns[sCurDate].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                    xNewGrid.Columns[sCurDate].CanBeSorted = false;
                    dCurDate = dCurDate.AddDays(1);
                }

                xNewGrid.Columns.Add(new Column("AcntTotal", typeof(int)));
                xNewGrid.Columns["AcntTotal"].Title = "Total";
                xNewGrid.Columns["AcntTotal"].Width = 65;
                xNewGrid.Columns["AcntTotal"].HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
                xNewGrid.Columns["AcntTotal"].ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;

                //grid wide settings
                xNewGrid.ReadOnly = true;
                xNewGrid.Dock = DockStyle.Fill;
                xNewGrid.FixedColumnSplitter.Visible = false;
                xNewGrid.RowSelectorPane.Visible = false;

                // prevent cell navigation
                xNewGrid.AllowCellNavigation = false;

                // resize
                xNewGrid.Resize += new System.EventHandler(xNewGrid_Resize);

                // declare vars
                miTotReq = 0;
                int iWork = 0;
                int iAcntTotal = 0;
                string sWork = "";
                               
                // load data from dataset
                foreach (System.Data.DataRow row in dtCombined.Rows)
                {

                    //dCurDate = (DateTime)row["datereq"];
                    //sCurDate = dCurDate.Day.ToString();
                    Xceed.Grid.DataRow dataRow = xNewGrid.DataRows.AddNew();

                    dataRow.Cells["Acnt"].Value = row["acnt"];

                    iAcntTotal = 0;
                    foreach (Xceed.Grid.Column column in this.xNewGrid.Columns)
                    {
                        sCurDate = column.FieldName;

                        if (sCurDate != "Acnt" && sCurDate != "AcntTotal")
                        {

                            iWork = (int)row[sCurDate];
                            if (iWork == 0)
                            {
                                sWork = " ";
                            }
                            else
                            {
                                miTotReq += iWork;
                                iAcntTotal += iWork;
                                sWork = iWork.ToString();
                            }

                            dataRow.Cells[sCurDate].Value = sWork;
                                
                        }
                    }

                    dataRow.Cells["AcntTotal"].Value = iAcntTotal;

                    dataRow.EndEdit();
                    miTotAcnt++;

                }

                xNewGrid.Columns["Acnt"].SortDirection = SortDirection.Ascending;

                xNewGrid_Resize(null, null);



                 tSSlblCount.Text = "Total requested: " + miTotReq.ToString("#,#");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                tSSlblStatus.Text = "Status: Ready";
                tSSlblStatus.BackColor = System.Drawing.Color.LightGray;
                this.Cursor = Cursors.Default;
            }
        }

        // Grid resize
        private void xNewGrid_Resize(object sender, System.EventArgs e)
        {
            int iAvailWidth = xNewGrid.DisplayRectangle.Width;
            int iTotColWidth = 0;
            int iResizeCol = 0;
            int iAdjustment = 0;

            if (iAvailWidth == 0)
                return;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xNewGrid.Columns)
            {
                if (column.Index != iResizeCol)
                {
                    Console.WriteLine(column.FieldName);
                    iTotColWidth += column.Width;
                }

            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xNewGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xNewGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.Width = iWidth;
            }
            else
            {
                //xResizeColumn.Width = iWidth;
            }
        }

        // resize grid for print
        private void xNewGrid_ResizeForPrint(int iAvailWidth)
        {
            int iTotColWidth = 0;
            int iResizeCol = 0;
            int iAdjustment = 0;

            // get total width of columns
            foreach (Xceed.Grid.Column column in this.xNewGrid.Columns)
            {
                if (column.Index != iResizeCol)
                    iTotColWidth += column.Width;
            }

            // adjust available width
            foreach (Xceed.Grid.Group group in this.xNewGrid.GroupTemplates)
            {
                iAvailWidth -= group.SideMargin.Width;
            }

            iAvailWidth -= iAdjustment;

            Xceed.Grid.Column xResizeColumn = this.xNewGrid.Columns.GetColumnAtDisplayableIndex(iResizeCol);

            // resize specified column
            int iWidth = iAvailWidth - iTotColWidth;
            if (iWidth > 0)
            {
                xResizeColumn.ReportStyle.Width = iWidth;
            }
            else
            {
                //xResizeColumn.ReportStyle.Width = iWidth;
            }
        }

        private bool procSIBI()
        {

            bool bRet = false;
            int iAcnt;
            DateTime dDateReq;
            int iNumReq;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            string sSQL= "SELECT acnt_num, Count(keynumber), datereq"
                 + " FROM sibtbl"
                 + " WHERE sibtbl.datereq Between '" + dBegDate.ToShortDateString() + "' AND '" + dEndDate.ToShortDateString() + "' AND deleted_flag = 0"
                 + " GROUP BY acnt_num, datereq;";
            
            try
            {
                
                // init SQL connection for read query
                SqlDataReader sqlDR;
                sqlConn1 = new SqlConnection(cfg_SQLMainSIBIConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.Connection = sqlConn1;

                // get items requested for the period
                sqlCmd1.CommandText = sSQL;
                sqlConn1.Open();
                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {
                        if (sqlDR.IsDBNull(0))
                        { 
                            iAcnt = 0;
                        }
                        else
                        {
                            iAcnt = (int)sqlDR.GetSqlInt32(0);
                        }

                        iNumReq = (int)sqlDR.GetSqlInt32(1);
                        dDateReq = sqlDR.GetDateTime(2);
                   
                        
                        dtSIBI.Rows.Add(iAcnt,dDateReq,iNumReq);

                    }	// while


                }	// has rows
                else
                {

                    MessageBox.Show("There were no requests found for the selected date range.\r\nPlease check your settings.", "No data found", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

                bRet = true;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {
                if (sqlConn1 != null)

                    sqlConn1.Close();


            }
            
            return bRet;
        }

        private bool proc360()
        {

            bool bRet = false;
            int iAcnt;
            string sAcnt;
            DateTime dDateReq;
            int iNumReq;

            
            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;

            try
            {

                // init SQL connection for read query
                SqlDataReader sqlDR;
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;


                // fix beg / end date for 360 GMT
                dBegDate360 = dBegDate.AddHours(cfg_TimeZoneOffset);
                dEndDate360 = dEndDate.AddHours(cfg_TimeZoneOffset);

                // get items requested for the period
                sqlCmd1.CommandText = "sp_GetOrderedByCust";
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate360);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate360);
                sqlConn1.Open();
                sqlDR = sqlCmd1.ExecuteReader();

                // anythign returned?
                if (sqlDR.HasRows)
                {

                    // loop through records
                    while (sqlDR.Read())
                    {

                        dDateReq = sqlDR.GetDateTime(0);
                        //dDateReq.AddHours(cfg_TimeZoneOffset);

                        if (sqlDR.IsDBNull(1))
                        {
                            sAcnt = "0";
                        }
                        else
                        {
                            sAcnt = sqlDR.GetSqlString(1).ToString();
                            if (sAcnt.Length == 0)
                                sAcnt = "0";
                        }
                        iAcnt = Convert.ToInt32(sAcnt);
                        iNumReq = (int)sqlDR.GetSqlInt32(2);

                        dt360.Rows.Add(iAcnt, dDateReq, iNumReq);

                    }	// while


                }	// has rows
                else
                {

                    MessageBox.Show("There were no requests found in 360 for the selected date range.\r\nPlease check your settings.", "No data found", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

                bRet = true;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            finally
            {
                if (sqlConn1 != null)

                    sqlConn1.Close();


            }

            return bRet;
        }

        private bool combineTables()
        {

            bool bRet = false;
            DateTime dCurDate = dBegDate;
            DateTime dLastDate = dEndDate.AddDays(1);
            string sCurDate = "";
            int iAcnt = 0;
            int iNumReq = 0;
            int iWork = 0;

            try 
            {
                // SIBI
                //foreach (System.Data.DataRow row in dtSIBI.Rows)
                //{

                //    // get data from current row
                //    dCurDate = (DateTime)row["datereq"];
                //    iAcnt = (int)row["acnt"];
                //    iNumReq = (int)row["numreq"];

                //    // if row for acnt does not exist in Combined table - add it
                //    System.Data.DataRow[] CombRow = dtCombined.Select("acnt = " + iAcnt);

                //    if (CombRow.Length == 0)
                //    {
                //        dtCombined.Rows.Add(iAcnt,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
                //    }
                    
                //    // add count to appropriate date column
                //    sCurDate = dCurDate.Day.ToString();
                //    CombRow = dtCombined.Select("acnt = " + iAcnt);
                //    iWork = (int)CombRow[0][sCurDate];
                //    CombRow[0][sCurDate] = iWork + iNumReq;
                                   
                //}

                // 360
                foreach (System.Data.DataRow row in dt360.Rows)
                {

                    // get data from current row
                    dCurDate = (DateTime)row["datereq"];
                    iAcnt = (int)row["acnt"];
                    iNumReq = (int)row["numreq"];

                    // if row for acnt does not exist in Combined table - add it
                    System.Data.DataRow[] CombRow = dtCombined.Select("acnt = " + iAcnt);

                    if (CombRow.Length == 0)
                    {
                        dtCombined.Rows.Add(iAcnt, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                    }

                    // add count to appropriate date column
                    sCurDate = dCurDate.Day.ToString();
                    CombRow = dtCombined.Select("acnt = " + iAcnt);
                    iWork = (int)CombRow[0][sCurDate];
                    CombRow[0][sCurDate] = iWork + iNumReq;

                }


                bRet = true;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


            return bRet;
        
        }

 
        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {

            System.Drawing.Printing.Margins newMargins = new System.Drawing.Printing.Margins(50, 50, 50, 50);

            this.xNewGrid.ReportSettings.Title = rptTitle;
            this.xNewGrid.ReportSettings.Landscape = true;
            this.xNewGrid.ReportSettings.Margins = newMargins;
            this.xNewGrid.ReportSettings.ColumnLayout = ColumnLayout.FitToPage;

            //xNewGrid_ResizeForPrint(1000);

            // base font for the report
            Font font = new Font("Arial", 8);
            this.xNewGrid.ReportStyle.Font = font;

            Report report = new Report(xNewGrid);
            ReportStyleSheet reportStyleSheet = report.ReportStyleSheet;

            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            report.ReportStyleSheet.Grid.DataRows.Add(new RowReportStyle());
            reportStyleSheet.Grid.DataRows[0].BackColor = Color.FromArgb(205, 255, 205);
            reportStyleSheet.Grid.DataRows[1].BackColor = Color.Transparent;

            report.ReportStyleSheet.PageHeader.BottomBorder.Thickness = ReportLineThickness.SingleThin;
            report.ReportStyleSheet.PageHeader.BottomMargin = 5;
            report.ReportStyleSheet.PageHeader.Font = new Font(xNewGrid.ReportStyle.Font.FontFamily, 8, FontStyle.Bold);

            this.xNewGridColumnManagerRow.ReportStyle.RepeatOnEachPage = true;
            this.xNewGridColumnManagerRow.ReportStyle.BackColor = Color.DarkGray;
            this.xNewGridColumnManagerRow.ReportStyle.ForeColor = Color.White;
            this.xNewGridColumnManagerRow.ReportStyle.Font = new Font("Arial", 8, FontStyle.Bold);
            this.xNewGridColumnManagerRow.ReportStyle.WordWrap = true;
            this.xNewGridColumnManagerRow.ReportStyle.RowHeightExpansion = RowHeightExpansion.ExpandWithoutLimits;
            this.xNewGridColumnManagerRow.ReportStyle.HorizontalAlignment = Xceed.Grid.HorizontalAlignment.Center;
            this.xNewGridColumnManagerRow.ReportStyle.VerticalAlignment = VerticalAlignment.Bottom;

            report.ReportStyleSheet.PageHeader.LeftElement.TextFormat = rptTitle;
            report.ReportStyleSheet.PageHeader.RightElement.TextFormat =
              "Page : %Page format=000%" + Environment.NewLine +
              "Date : %DateTime format=g%";

            report.ReportStyleSheet.PageFooter.Font = new Font(xNewGrid.ReportStyle.Font.FontFamily, 8, FontStyle.Bold);
            report.ReportStyleSheet.PageFooter.TopBorder.Thickness = ReportLineThickness.SingleThin;
            report.ReportStyleSheet.PageFooter.LeftElement.TextFormat = miTotAcnt.ToString() + " accounts";
            report.ReportStyleSheet.PageFooter.RightElement.TextFormat = "Total requested: " + miTotReq.ToString("N0");

            report.PrintPreview();
        }


       
    }
}
