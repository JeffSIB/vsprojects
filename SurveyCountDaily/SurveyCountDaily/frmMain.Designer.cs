﻿namespace SurveyCountDaily
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.bGo = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.gbCustom = new System.Windows.Forms.GroupBox();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpBegin = new System.Windows.Forms.DateTimePicker();
            this.rbCurMonth = new System.Windows.Forms.RadioButton();
            this.rbPrevMonth = new System.Windows.Forms.RadioButton();
            this.rbCustom = new System.Windows.Forms.RadioButton();
            this.gbCustom.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(307, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Please select a date range for the report.  (Maximum of 31 days)";
            // 
            // bGo
            // 
            this.bGo.Location = new System.Drawing.Point(91, 222);
            this.bGo.Name = "bGo";
            this.bGo.Size = new System.Drawing.Size(78, 37);
            this.bGo.TabIndex = 5;
            this.bGo.Text = "View Report";
            this.bGo.UseVisualStyleBackColor = true;
            this.bGo.Click += new System.EventHandler(this.bGo_Click);
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(213, 222);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(78, 37);
            this.bCancel.TabIndex = 6;
            this.bCancel.Text = "Close";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // gbCustom
            // 
            this.gbCustom.Controls.Add(this.dtpEnd);
            this.gbCustom.Controls.Add(this.label3);
            this.gbCustom.Controls.Add(this.label1);
            this.gbCustom.Controls.Add(this.dtpBegin);
            this.gbCustom.Location = new System.Drawing.Point(56, 105);
            this.gbCustom.Name = "gbCustom";
            this.gbCustom.Size = new System.Drawing.Size(300, 89);
            this.gbCustom.TabIndex = 7;
            this.gbCustom.TabStop = false;
            this.gbCustom.Text = " Custom ";
            // 
            // dtpEnd
            // 
            this.dtpEnd.Location = new System.Drawing.Point(74, 55);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(200, 20);
            this.dtpEnd.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(37, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "To: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "From:";
            // 
            // dtpBegin
            // 
            this.dtpBegin.Location = new System.Drawing.Point(74, 14);
            this.dtpBegin.Name = "dtpBegin";
            this.dtpBegin.Size = new System.Drawing.Size(200, 20);
            this.dtpBegin.TabIndex = 5;
            // 
            // rbCurMonth
            // 
            this.rbCurMonth.AutoSize = true;
            this.rbCurMonth.Location = new System.Drawing.Point(56, 35);
            this.rbCurMonth.Name = "rbCurMonth";
            this.rbCurMonth.Size = new System.Drawing.Size(91, 17);
            this.rbCurMonth.TabIndex = 9;
            this.rbCurMonth.TabStop = true;
            this.rbCurMonth.Text = "Current month";
            this.rbCurMonth.UseVisualStyleBackColor = true;
            // 
            // rbPrevMonth
            // 
            this.rbPrevMonth.AutoSize = true;
            this.rbPrevMonth.Location = new System.Drawing.Point(56, 58);
            this.rbPrevMonth.Name = "rbPrevMonth";
            this.rbPrevMonth.Size = new System.Drawing.Size(98, 17);
            this.rbPrevMonth.TabIndex = 10;
            this.rbPrevMonth.TabStop = true;
            this.rbPrevMonth.Text = "Previous month";
            this.rbPrevMonth.UseVisualStyleBackColor = true;
            // 
            // rbCustom
            // 
            this.rbCustom.AutoSize = true;
            this.rbCustom.Location = new System.Drawing.Point(56, 81);
            this.rbCustom.Name = "rbCustom";
            this.rbCustom.Size = new System.Drawing.Size(60, 17);
            this.rbCustom.TabIndex = 11;
            this.rbCustom.TabStop = true;
            this.rbCustom.Text = "Custom";
            this.rbCustom.UseVisualStyleBackColor = true;
            this.rbCustom.CheckedChanged += new System.EventHandler(this.rbCustom_CheckedChanged);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(383, 296);
            this.Controls.Add(this.rbCustom);
            this.Controls.Add(this.rbPrevMonth);
            this.Controls.Add(this.rbCurMonth);
            this.Controls.Add(this.gbCustom);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bGo);
            this.Controls.Add(this.label2);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.Text = "Survey Count";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.gbCustom.ResumeLayout(false);
            this.gbCustom.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bGo;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.GroupBox gbCustom;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpBegin;
        private System.Windows.Forms.RadioButton rbCurMonth;
        private System.Windows.Forms.RadioButton rbPrevMonth;
        private System.Windows.Forms.RadioButton rbCustom;
    }
}

