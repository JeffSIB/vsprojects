﻿namespace SurveyCountDaily
{
    partial class frmViewData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmViewData));
            this.panelMain = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.sStrip1 = new System.Windows.Forms.StatusStrip();
            this.tSSlblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tSSlblCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.sStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelMain.BackColor = System.Drawing.Color.White;
            this.panelMain.ForeColor = System.Drawing.Color.Black;
            this.panelMain.Location = new System.Drawing.Point(11, 29);
            this.panelMain.Margin = new System.Windows.Forms.Padding(10, 39, 3, 3);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1072, 721);
            this.panelMain.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.printToolStripMenuItem,
            this.tsmiRefresh});
            this.menuStrip1.Location = new System.Drawing.Point(11, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(400, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.printToolStripMenuItem.Text = "Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // tsmiRefresh
            // 
            this.tsmiRefresh.Name = "tsmiRefresh";
            this.tsmiRefresh.Size = new System.Drawing.Size(57, 20);
            this.tsmiRefresh.Text = "Refresh";
            // 
            // sStrip1
            // 
            this.sStrip1.BackColor = System.Drawing.Color.LightGray;
            this.sStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tSSlblStatus,
            this.tSSlblCount});
            this.sStrip1.Location = new System.Drawing.Point(0, 757);
            this.sStrip1.Name = "sStrip1";
            this.sStrip1.Size = new System.Drawing.Size(1094, 22);
            this.sStrip1.TabIndex = 10;
            // 
            // tSSlblStatus
            // 
            this.tSSlblStatus.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tSSlblStatus.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.tSSlblStatus.Margin = new System.Windows.Forms.Padding(3, 3, 3, 2);
            this.tSSlblStatus.Name = "tSSlblStatus";
            this.tSSlblStatus.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.tSSlblStatus.Size = new System.Drawing.Size(52, 17);
            this.tSSlblStatus.Text = "Status";
            this.tSSlblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tSSlblCount
            // 
            this.tSSlblCount.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tSSlblCount.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.tSSlblCount.Margin = new System.Windows.Forms.Padding(3, 3, 3, 2);
            this.tSSlblCount.Name = "tSSlblCount";
            this.tSSlblCount.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tSSlblCount.Size = new System.Drawing.Size(92, 17);
            this.tSSlblCount.Text = "Total Items: ";
            this.tSSlblCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmViewData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1094, 779);
            this.Controls.Add(this.sStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panelMain);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1100, 811);
            this.Name = "frmViewData";
            this.Text = "Survey count";
            this.Load += new System.EventHandler(this.frmViewData_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.sStrip1.ResumeLayout(false);
            this.sStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiRefresh;
        private System.Windows.Forms.StatusStrip sStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tSSlblStatus;
        private System.Windows.Forms.ToolStripStatusLabel tSSlblCount;
    }
}