﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SurveyCountDaily
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Xceed.Grid.Licenser.LicenseKey = "GRD39GE4PT9AWW5ZN2A";
            //Xceed.SmartUI.Licenser.LicenseKey = "SUN359EWYTJ4HBP5NJA";

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
