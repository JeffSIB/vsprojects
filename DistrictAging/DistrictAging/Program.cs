﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Net.Mail;

namespace DistrictAging
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static string cfg_deliverto;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Range oRange = null;
        static string sReportDate;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_deliverto = colNameVal.Get("deliverto");

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                DateTime dt = new DateTime();
                dt = DateTime.Today;
                sReportDate = dt.ToShortDateString();

                string sDateDisp = dt.Month.ToString() + dt.Day.ToString() + dt.Year.ToString();
                string sEmailSubject = "District Aging report - " + dt.ToShortDateString();
                string sExcelFileNameNoEx = cfg_outputdir + "DistrictAging_" + sDateDisp;
                string sExcelFileName = sExcelFileNameNoEx + ".xlsx";

                // Delete file if it exists
                FileInfo fi = new FileInfo(sExcelFileName);
                if (fi.Exists)
                    fi.Delete();

                ///////////////////////////////////////////////////////////////


                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                int iATF = 0;
                int iBTF = 0;
                int iCTF = 0;
                int iDTF = 0;
                int iETF = 0;
                int iAMTF = 0;
                int iCGTF = 0;
                int iTotF = 0;

                int iATL = 0;
                int iBTL = 0;
                int iCTL = 0;
                int iDTL = 0;
                int iETL = 0;
                int iAMTL = 0;
                int iCGTL = 0;
                int iTotLate = 0;

                int iA30 = 0;
                int iB30 = 0;
                int iC30 = 0;
                int iD30 = 0;
                int iE30 = 0;
                int iAM30 = 0;
                int iCG30 = 0;
                int iTot30 = 0;

                int iA60 = 0;
                int iB60 = 0;
                int iC60 = 0;
                int iD60 = 0;
                int iE60 = 0;
                int iAM60 = 0;
                int iCG60 = 0;
                int iTot60 = 0;

                int iA90 = 0;
                int iB90 = 0;
                int iC90 = 0;
                int iD90 = 0;
                int iE90 = 0;
                int iAM90 = 0;
                int iCG90 = 0;
                int iTot90 = 0;

                int iACustRej = 0;
                int iBCustRej = 0;
                int iCCustRej = 0;
                int iDCustRej = 0;
                int iECustRej = 0;
                int iAMCustRej = 0;
                int iCGCustRej = 0;
                int iTotCustRej = 0;

                //int iTotUassigned = 0;
                //int iTotActive = 0;
                //int iTotField = 0;
                int iAge = 0;
                int iStat = 0;

                string sDistrict = "";
                //int iCount = 0;
                string sErrMsg = "";
                bool bErr = false;
                double dPct = 0;

                int iRow = 0;

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);

                // Get cases by district
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_DistrictAgingExclude";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();
                sqlReader = sqlCmd1.ExecuteReader();
                if (sqlReader.HasRows)
                {

                    sqlReader.Read();

                    // loop through rows
                    do
                    {
                        // District
                        if (sqlReader.IsDBNull(1))
                        {
                            sDistrict = "";
                        }
                        else
                        {
                            sDistrict = (string)sqlReader.GetSqlString(1);
                        }

                        if (sqlReader.IsDBNull(2))
                        {
                            iAge = 0;
                        }
                        else
                        {
                            iAge = (int)sqlReader.GetInt32(2);
                        }

                        if (sqlReader.IsDBNull(3))
                        {
                            iStat = 0;
                        }
                        else
                        {
                            iStat = (int)sqlReader.GetInt32(3);
                        }

                        sDistrict = sDistrict.ToUpper();
                        if (sDistrict == "A")
                        {
                            iATF++;

                            if (iAge > 29 && iAge < 60)
                                iA30++;
                            else if (iAge > 59 && iAge < 90)
                                iA60++;
                            else if (iAge > 89)
                                iA90++;

                            if (iStat == 8)
                                iACustRej++;
                        }
                        else if (sDistrict == "B")
                        {
                            iBTF++;

                            if (iAge > 29 && iAge < 60)
                                iB30++;
                            else if (iAge > 59 && iAge < 90)
                                iB60++;
                            else if (iAge > 89)
                                iB90++;

                            if (iStat == 8)
                                iBCustRej++;

                        }
                        else if (sDistrict == "C")
                        {
                            iCTF++;

                            if (iAge > 29 && iAge < 60)
                                iC30++;
                            else if (iAge > 59 && iAge < 90)
                                iC60++;
                            else if (iAge > 89)
                                iC90++;

                            if (iStat == 8)
                                iCCustRej++;

                        }
                        else if (sDistrict == "D")
                        {
                            iDTF++;

                            if (iAge > 29 && iAge < 60)
                                iD30++;
                            else if (iAge > 59 && iAge < 90)
                                iD60++;
                            else if (iAge > 89)
                                iD90++;

                            if (iStat == 8)
                                iDCustRej++;

                        }
                        else if (sDistrict == "E")
                        {
                            iETF++;

                            if (iAge > 29 && iAge < 60)
                                iE30++;
                            else if (iAge > 59 && iAge < 90)
                                iE60++;
                            else if (iAge > 89)
                                iE90++;

                            if (iStat == 8)
                                iECustRej++;

                        }
                        else
                        {
                            sErrMsg += "Unrecognized District (Aging): " + sDistrict;
                            bErr = true;
                        }

                    } while (sqlReader.Read());     // get cases

                }   // has rows

                sqlReader.Close();

                /////////////////////////////
                // AMRisc
                /////////////////////////////

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_DistrictAgingAMRisc";
                sqlCmd1.Connection = sqlConn1;
                //sqlConn1.Open();
                sqlReader = sqlCmd1.ExecuteReader();
                if (sqlReader.HasRows)
                {

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        if (sqlReader.IsDBNull(2))
                        {
                            iAge = 0;
                        }
                        else
                        {
                            iAge = (int)sqlReader.GetInt32(2);
                        }

                        if (sqlReader.IsDBNull(3))
                        {
                            iStat = 0;
                        }
                        else
                        {
                            iStat = (int)sqlReader.GetInt32(3);
                        }

                        iAMTF++;

                        if (iAge > 29 && iAge < 60)
                            iAM30++;
                        else if (iAge > 59 && iAge < 90)
                            iAM60++;
                        else if (iAge > 89)
                            iAM90++;

                        if (iStat == 8)
                            iAMCustRej++;


                    } while (sqlReader.Read());     

                }   // has rows
                sqlReader.Close();

                /////////////////////////////
                // CGCU
                /////////////////////////////

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_DistrictAgingCGCU";
                sqlCmd1.Connection = sqlConn1;
                //sqlConn1.Open();
                sqlReader = sqlCmd1.ExecuteReader();
                if (sqlReader.HasRows)
                {

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        if (sqlReader.IsDBNull(2))
                        {
                            iAge = 0;
                        }
                        else
                        {
                            iAge = (int)sqlReader.GetInt32(2);
                        }

                        if (sqlReader.IsDBNull(3))
                        {
                            iStat = 0;
                        }
                        else
                        {
                            iStat = (int)sqlReader.GetInt32(3);
                        }

                        iCGTF++;

                        if (iAge > 29 && iAge < 60)
                            iCG30++;
                        else if (iAge > 59 && iAge < 90)
                            iCG60++;
                        else if (iAge > 89)
                            iCG90++;

                        if (iStat == 8)
                            iCGCustRej++;


                    } while (sqlReader.Read());

                }   // has rows

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;
                
                oExcel = new Excel.Application();
                oExcel.Visible = true;
                oWorkbook = oExcel.Workbooks.Add(1);
                o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                //o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing,oSIBWorksheet,oMissing,oMissing);
                o360Worksheet.Name = "District Aging";
                createHeader(o360Worksheet);

                iRow = 4;

                iATL = iA30 + iA60 + iA90 + iACustRej;
                if (iATL > 0 && iATF > 0)
                {
                    dPct = ((double)iATL / iATF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "A", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iA30.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iA60.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iA90.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, iACustRej.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 6, iATL.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 7, iATF.ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 8, dPct.ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "###0.00", "C");
                iRow++;

                iBTL = iB30 + iB60 + iB90 + iBCustRej;
                if (iBTL > 0 && iBTF > 0)
                {
                    dPct = ((double)iBTL / iBTF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "B", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iB30.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iB60.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iB90.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, iBCustRej.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 6, iBTL.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 7, iBTF.ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 8, dPct.ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "###0.00", "C");
                iRow++;

                iCTL = iC30 + iC60 + iC90 + iCCustRej;
                if (iCTL > 0 && iCTF > 0)
                {
                    dPct = ((double)iCTL / iCTF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "C", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iC30.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iC60.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iC90.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, iCCustRej.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 6, iCTL.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 7, iCTF.ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 8, dPct.ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "###0.00", "C");
                iRow++;

                iDTL = iD30 + iD60 + iD90 + iDCustRej;
                if (iDTL > 0 && iDTF > 0)
                {
                    dPct = ((double)iDTL / iDTF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "D", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iD30.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iD60.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iD90.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, iDCustRej.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 6, iDTL.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 7, iDTF.ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 8, dPct.ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "###0.00", "C");
                iRow++;

                iETL = iE30 + iE60 + iE90 + iECustRej;
                if (iETL > 0 && iETF > 0)
                {
                    dPct = ((double)iETL / iETF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "E", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iE30.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iE60.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iE90.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, iECustRej.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 6, iETL.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 7, iETF.ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 8, dPct.ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "###0.00", "C");
                iRow++;

                // AMRisc
                iAMTL = iAM30 + iAM60 + iAM90 + iAMCustRej;
                if (iAMTL > 0 && iAMTF > 0)
                {
                    dPct = ((double)iAMTL / iAMTF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "Amrisc", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iAM30.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iAM60.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iAM90.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, iAMCustRej.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 6, iAMTL.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 7, iAMTF.ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 8, dPct.ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "###0.00", "C");
                iRow++;

                // CGCU
                iCGTL = iCG30 + iCG60 + iCG90 + iCGCustRej;
                if (iCGTL > 0 && iCGTF > 0)
                {
                    dPct = ((double)iCGTL / iCGTF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "CGCU", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iCG30.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iCG60.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iCG90.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, iCGCustRej.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 6, iCGTL.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 7, iCGTF.ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 8, dPct.ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "###0.00", "C");
                iRow++;

                //totals
                iTot30 = iA30 + iB30 + iC30 + iD30 + iE30 + iAM30 + iCG30;
                iTot60 = iA60 + iB60 + iC60 + iD60 + iE60 + iAM60 + iCG60;
                iTot90 = iA90 + iB90 + iC90 + iD90 + iE90 + iAM90 + iCG90;
                iTotCustRej = iACustRej + iBCustRej + iCCustRej + iDCustRej + iECustRej + iAMCustRej + iCGCustRej;
                iTotLate = iATL + iBTL + iCTL + iDTL + iETL + iAMTL + iCGTL;
                iTotF = iATF + iBTF + iCTF + iDTF + iETF + iAMTF + iCGTF;

                if (iTotF > 0 && iTotLate > 0)
                {
                    dPct = ((double)iTotLate / iTotF) * 100;
                }
                else
                    dPct = 0;

                addData(o360Worksheet, iRow, 1, "Totals", "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                addData(o360Worksheet, iRow, 2, iTot30.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 3, iTot60.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 4, iTot90.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 5, iTotCustRej.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 6, iTotLate.ToString(), "F" + iRow.ToString(), "F" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 7, iTotF.ToString(), "G" + iRow.ToString(), "G" + iRow.ToString(), "#,##0", "C");
                addData(o360Worksheet, iRow, 8, dPct.ToString(), "H" + iRow.ToString(), "H" + iRow.ToString(), "###0.00", "C");

                iRow = 15;
                string sText = "";

                sText = "Customer Reject: Case status = Customer Rejected";
                addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
                iRow++;

                sText = "Total in Field: Case Status = Ordered, Assigned, In Progress, QA Rejected, Cust Rejected";
                addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
                iRow++;

                sText = "Percentage: Total Late / Total in Field";
                addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
                iRow++;
                iRow++;

                sText = "District assignment based on location zip code being assgned to district";
                addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
                iRow++;
                iRow++;

                sText = "Report run date:  " + DateTime.Now.ToString();
                addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");


                oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.Close(true, oMissing, oMissing);
                oExcel.Quit();
                msMsgBody += "Processing complete" + System.Environment.NewLine + cfg_outputdir + sExcelFileNameNoEx + System.Environment.NewLine + System.Environment.NewLine;

                releaseObject(oExcel);
                releaseObject(oWorkbook);
                releaseObject(o360Worksheet);
                sendExcelFile(sEmailSubject, sExcelFileName);
                msMsgBody += "File sent " + sExcelFileName;
            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(msMsgBody);
                }

            }
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeader(Excel._Worksheet oWorkSheet)
        {

            Excel.Range oRange;


            oWorkSheet.get_Range("A1", "H1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "H1");
            oRange.FormulaR1C1 = "District Aging as of " + sReportDate;

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "District";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "30-59";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "60-89";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "90 +";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Cust Reject";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 6] = "Total Late";
            oRange = oWorkSheet.get_Range("F3", "F3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 7] = "Total Cases";
            oRange = oWorkSheet.get_Range("G3", "G3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 8] = "Percent Late";
            oRange = oWorkSheet.get_Range("H3", "H3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        }

        //static void createFooter(Excel._Worksheet oWorkSheet)
        //{

        //    oWorkSheet.get_Range("A1", "E1").Merge(false);
        //    oRange = oWorkSheet.get_Range("A1", "E1");
        //    oRange.FormulaR1C1 = "Unassigned Cases as of " + sReportDate;

        //    string sText = "Unassigned: Case status = Ordered\r\n";
        //    addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
        //    iRow++;

        //    sText = "Total Active: Case status = Hold, Ordered, Assigned, In Progress, QA, QA Rejected, Customer Rejected, QA Hold\r\n";
        //    addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
        //    iRow++;

        //    sText = "Total in Field: Case Status = Ordered, Assigned, In Progress, QA\r\n";
        //    addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
        //    iRow++;

        //    sText = "Percentage: Unassigned / Total in Field";
        //    addMultiCellData(o360Worksheet, iRow, 1, sText, "A" + iRow.ToString(), "E" + iRow.ToString(), "", "L");
        //    iRow++;



        //}


        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2, string format, string sHorizAlign)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            oRange.WrapText = true; 
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
        }

        static void addMultiCellData(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2, string format, string sHorizAlign)
        {
            oWorkSheet.get_Range(cell1, cell2).Merge(false);
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.FormulaR1C1 = data;
        }


        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }



        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "District Aging";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by District Aging Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = cfg_deliverto;
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;

            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }


    }
}
