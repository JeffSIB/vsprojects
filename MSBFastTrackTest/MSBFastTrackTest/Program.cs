﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;

namespace MSBFastTrackTest
{
    class Program
    {

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
        static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];
        static string cfg_NewRequestsURI = ConfigurationManager.AppSettings["NewRequestsURI"];


        static LogUtils.LogUtils oLU;
        static bool bErr = false;
        static int iNodes = 0;

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();

        static void Main(string[] args)
        {
            DateTime dToday = new DateTime();
            dToday = DateTime.Now;
            DateTime dLoginExpires = new DateTime();
            dLoginExpires = DateTime.Now.AddDays(7);


            try
            {
                //// initialize log file class
                //oLU = new LogUtils.LogUtils();

                //// set log file name
                //oLU.logFileName = cfg_logfilename;

                //// open log file
                //oLU.OpenLog();

                ReferenceServiceClient.ReferenceServiceClient rsClient = new ReferenceServiceClient.ReferenceServiceClient();

                rsClient.DisplayInitializationUI();
                rsClient.Open();
                ReferenceServiceClient.FieldOptions fops = rsClient.GetFieldOptionNames();

                //SecurityService.SecurityClient ssClient = new SecurityService.SecurityClient();

                //SecurityService.QuickLinkInfo qinfo = new SecurityService.QuickLinkInfo();
                //qinfo.UserLogon = "SIBDemo";
                ////qinfo.ValuationId = 0;
                //qinfo.Role = "AD";
                //qinfo.LogonExpires = dLoginExpires;
                //qinfo.GroupName = "SuttonInspectionParent";
                //qinfo.GroupDescription = "";
                //qinfo.FirstName = "Sutton";
                //qinfo.LastName = "Inspection";
                //qinfo.RecordAccessAction = "";
                //qinfo.AfterRecordAccessAction = "";
                //qinfo.RedirectUrl = "";
                //qinfo.PolicyNumber = "";

                //SecurityService.GetQuickLinkRequest req = new SecurityService.GetQuickLinkRequest();
                //req.Logon = "SIBDemo";
                //req.Password = "b25e059b1396Q";
                //req.CompanyID = Guid.Parse("a9b7adb7-a081-4b96-b259-55e969e059b5");
                //req.QuickLinkInfo = qinfo;

                //SecurityService.ResponseGetQuickLink resp = new SecurityService.ResponseGetQuickLink();

                //resp =  ssClient.GetQuickLink(req);

            }

            catch (Exception ex)
            {
                string sErr = ex.Message;
                //oLU.closeLog();
                //sendErrEmail("Error initializing importAmRisc\r\n\r\n" + ex.Message);                
                return;
            }
        }
    }
}
