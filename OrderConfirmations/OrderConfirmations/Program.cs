﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Globalization;

namespace OrderConfirmations
{

    class InspectionInfo
    {

        public Guid CaseID { get; set; }
        public string CustNum { get; set; }
        public string PolicyNum { get; set; }
        public string RequestedBy { get; set; }
        public string InsuredName { get; set; }
        public string InsuredContactName { get; set; }
        public string InsuredContactPhoneHome { get; set; }
        public string InsuredContactPhoneWork { get; set; }
        public string InsuredContactPhoneCell { get; set; }
        public string CaseNum { get; set; }
        public string Recip { get; set; }
        public string DateSubmitted { get; set; }
        public string InsAdd1 { get; set; }
        public string InsAdd2 { get; set; }
        public string InsCity { get; set; }
        public string InsState { get; set; }
        public string InsZip { get; set; }
        public string LocAdd1 { get; set; }
        public string LocAdd2 { get; set; }
        public string LocCity { get; set; }
        public string LocState { get; set; }
        public string LocZip { get; set; }
        public string LocContact { get; set; }
        public string LocContactPhone { get; set; }
        public string Agent { get; set; }
        public string AgentPhone { get; set; }
        public string InsuranceCo { get; set; }
        public string Underwriter { get; set; }
        public string InspectionType { get; set; }
        public string Comments { get; set; }
    }
        class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360ConnStr;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static string sReportDate;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;


        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360ConnStr = colNameVal.Get("360ConnStr");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");


                InspectionInfo InspInfo = new InspectionInfo();
                string sCaseNum = "9745085";
                if (GetCaseInfo(sCaseNum, InspInfo))
                {

                    EmailConfirmation360.EmailConf360 oEM = new EmailConfirmation360.EmailConf360();

                    oEM.cfg_360ConnStr = cfg_360ConnStr;
                    oEM.cfg_smtpserver = cfg_smtpserver;
                    oEM.CaseNum = sCaseNum;
                    oEM.CustNum = InspInfo.CustNum;
                    oEM.InsuredName = InspInfo.InsuredName;
                    oEM.RequestedBy = InspInfo.RequestedBy;
                    oEM.Recip = "jeff@sibfla.com";
                    oEM.DateSubmitted = InspInfo.DateSubmitted;
                    oEM.InsAdd1 = InspInfo.InsAdd1;
                    //oEM.InsAdd2 = "Insured address line  2";
                    oEM.InsCity = InspInfo.InsCity;
                    oEM.InsState = InspInfo.InsState;
                    oEM.InsZip = InspInfo.InsZip;
                    oEM.InsuredContactName = InspInfo.InsuredContactName;
                    oEM.InsuredContactPhoneHome = InspInfo.InsuredContactPhoneHome;
                    oEM.InsuredContactPhoneWork = InspInfo.InsuredContactPhoneWork;
                    oEM.InsuredContactPhoneCell = InspInfo.InsuredContactPhoneCell;
                    oEM.PolicyNum = InspInfo.PolicyNum;
                    oEM.Agent = InspInfo.Agent;
                    oEM.AgentPhone = InspInfo.AgentPhone;
                    oEM.InsuranceCo = InspInfo.InsuranceCo;
                    oEM.Underwriter = InspInfo.Underwriter;
                    oEM.LocAdd1 = InspInfo.LocAdd1;
                    oEM.LocCity = InspInfo.LocCity;
                    oEM.LocState = InspInfo.LocState;
                    oEM.LocZip = InspInfo.LocState;
                    oEM.LocContact = InspInfo.LocContact;
                    oEM.LocContactPhone = InspInfo.LocContactPhone;
                    oEM.InspectionType = InspInfo.InspectionType;
                    oEM.Comments = InspInfo.Comments;


                    string sRet = oEM.sendEmailConf();
                }

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                DateTime dt = new DateTime();
                dt = DateTime.Today;
                sReportDate = dt.ToShortDateString();

                string sDateDisp = dt.Month.ToString() + dt.Day.ToString() + dt.Year.ToString();
                string sEmailSubject = "Last login report - " + dt.ToShortDateString();
                string sExcelFileNameNoEx = cfg_outputdir + "LastLogin_" + sDateDisp;
                string sExcelFileName = sExcelFileNameNoEx + ".xlsx";


                ///////////////////////////////////////////////////////////////


                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                int iRet = 0;

                ///////////////////////////////////////////////////
                // 360 
                ///////////////////////////////////////////////////

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);

                // Build FieldRepLastLogin table
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_FieldRepLastLogin_Build";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();
                iRet = sqlCmd1.ExecuteNonQuery();


                // Read from FieldRepLastLogin
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT [FieldRep],[FieldRepName],[LastLogin],[LockedOut],[NumberAssigned] FROM[Sutton360Utils].[dbo].[FieldRepLastLogin]";
                sqlReader = sqlCmd1.ExecuteReader();
                if (sqlReader.HasRows)
                {


                    sqlReader.Read();

                    // loop through rows
                    do
                    {


                    } while (sqlReader.Read());     // 360

                }   // has rows

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;


            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    //sendErrEmail(msErrMsg);
                }
                else
                {
                    //sendEmail(msMsgBody);
                }

            }


        }

        static bool GetCaseInfo(string sCaseNum, InspectionInfo InspInfo)
        {

            bool bRetVal = false;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            string sFirstName1 = "";
            string sLastName1 = "";
            string sFirstName2 = "";
            string sLastName2 = "";

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetFullCaseInfo";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {
                        InspInfo.CaseID = sqlReader.GetGuid(0);
                        InspInfo.CustNum = sqlReader.GetSqlString(1).ToString().Trim();
                        InspInfo.InsuredName = sqlReader.GetSqlString(2).ToString();
                        sFirstName1 = sqlReader.GetSqlString(3).ToString();
                        sLastName1 = sqlReader.GetSqlString(4).ToString();
                        InspInfo.RequestedBy = sFirstName1 + " " +  sLastName1;
                        InspInfo.Recip = sqlReader.GetSqlString(5).ToString();
                        InspInfo.DateSubmitted = sqlReader.GetSqlDateTime(6).ToString();
                        InspInfo.InsAdd1 = sqlReader.GetSqlString(7).ToString();
                        InspInfo.InsCity = sqlReader.GetSqlString(8).ToString();
                        InspInfo.InsState = sqlReader.GetSqlString(9).ToString();
                        InspInfo.InsZip = sqlReader.GetSqlString(10).ToString();
                        InspInfo.InsuredContactName = sqlReader.GetSqlString(11).ToString();
                        InspInfo.InsuredContactPhoneCell = sqlReader.GetSqlString(12).ToString();
                        InspInfo.InsuredContactPhoneHome = sqlReader.GetSqlString(13).ToString();
                        InspInfo.InsuredContactPhoneWork = sqlReader.GetSqlString(14).ToString();
                        InspInfo.PolicyNum = sqlReader.GetSqlString(15).ToString();
                        InspInfo.Agent = sqlReader.GetSqlString(16).ToString();
                        InspInfo.AgentPhone = sqlReader.GetSqlString(17).ToString();
                        sFirstName2 = sqlReader.GetSqlString(18).ToString();
                        sLastName2 = sqlReader.GetSqlString(19).ToString();
                        InspInfo.Underwriter = sFirstName2 + " " + sLastName2;
                        InspInfo.InspectionType = sqlReader.GetSqlString(20).ToString();
                        InspInfo.Comments = sqlReader.GetSqlString(21).ToString();

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }

                //Insurance company
                sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseID", InspInfo.CaseID);
                sqlCmd1.Parameters.AddWithValue("@fieldname", "Insurance Company");

                InspInfo.InsuranceCo = "";
                object oRetVal = sqlCmd1.ExecuteScalar();

                if (oRetVal != null)
                {
                    InspInfo.InsuranceCo = (string)oRetVal;
                }
                sqlReader.Close();


                // Location address
                sqlCmd1.CommandText = "sp_GetLocationAddr";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", sCaseNum);

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        InspInfo.LocAdd1 = sqlReader.GetSqlString(0).ToString();
                        InspInfo.LocCity = sqlReader.GetSqlString(1).ToString();
                        InspInfo.LocState = sqlReader.GetSqlString(2).ToString();
                        InspInfo.LocZip = sqlReader.GetSqlString(3).ToString();
                        InspInfo.LocContact = "";
                        InspInfo.LocContactPhone = "";

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }

                bRetVal = true;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();



            }

            return bRetVal;
        }
    }
}
