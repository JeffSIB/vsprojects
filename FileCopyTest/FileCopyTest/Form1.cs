﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FileCopyTest
{


    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string cfg_FTPRoot = @"\\nas\apps\SIBI\sibidata\Seacoast\FTP";
            string sDestDir = @"\\192.168.10.50\FTPRoot\SeacoastFTP";

            NetworkShare.DisconnectFromShare(@"\\192.168.10.50\FTPRoot", true); //Disconnect in case we are currently connected with our credentials;

            NetworkShare.ConnectToShare(@"\\192.168.10.50\FTPRoot", "Kensinja", "Manana24"); //Connect with the new credentials

            string[] sFiles = Directory.GetFiles(cfg_FTPRoot);

            foreach (string sFile in sFiles)
            {
                // Remove path from the file name.
                string fName = sFile.Substring(cfg_FTPRoot.Length + 1);

                File.Copy(Path.Combine(cfg_FTPRoot, fName), Path.Combine(sDestDir, fName));

            }
            NetworkShare.DisconnectFromShare(@"\\192.168.10.50\FTPRoot", false); //Disconnect from the server.
        }
    }
}
