﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;


namespace importBankers0818
{

    /// <summary>
    /// Called directly from import app
    /// Must reside on H:\VS2010\importBankers0818\importBankers0818\bin\Release
    /// </summary>
    /// 

    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyState { get; set; }
            public string PolicyNumber { get; set; }
            public string PolicyCode { get; set; }
            public string PolicyOccurance { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string AgencyAgentCode { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            public string BusinessOperations { get; set; }
            public string LocationFullAddress { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string CoverageB { get; set; }
            public string CoverageC { get; set; }
            public string CoverageD { get; set; }
            public string CoverageE { get; set; }
            public string CoverageF { get; set; }
            public string ISOClass { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }

            public string Construction { get; set; }
            public string YearBuilt { get; set; }
            public string DwellingType { get; set; }
            public string NumberOfFloors { get; set; }
            public string Occupancy { get; set; }
            public string SquareFootage { get; set; }
            public string DistanceToCoast { get; set; }
            public string RoofYear { get; set; }
            public string RoofMaterials { get; set; }
            public string RoofGeometry { get; set; }
            public string Usage { get; set; }
            public string WindMitigation { get; set; }
            public string PCClass { get; set; }
            public string RoofCladding { get; set; }
            public string RoofCondition { get; set; }
            public string BuildingCladding { get; set; }
            public string CompanyPolicyForm { get; set; }
            public string CentralFireAlarm { get; set; }
            public string CentralBurglarAlarm { get; set; }
            public string CentralAlarm { get; set; }
            public string PolicyStatus { get; set; }
            public string PoolFenced { get; set; }
            public string Heating { get; set; }
            public string HeatingFullPartial { get; set; }
            public string HeatingUpdate { get; set; }
            public string HeatingYear { get; set; }
            public string PoolAboveGround { get; set; }
            public string Farming { get; set; }
            public string NumberOfFamilies { get; set; }
            public string Pets { get; set; }
            public string Plumbing { get; set; }
            public string PlumbingFullPartial { get; set; }
            public string PlumbingUpdate { get; set; }
            public string PlumbingYear { get; set; }
            public string PoolSlide { get; set; }
            public string PoolDivingBoard { get; set; }
            public string PoolScreenEncl { get; set; }
            public string Pool { get; set; }
            public string Updates { get; set; }
            public string Wiring { get; set; }
            public string WiringFullPartial { get; set; }
            public string WiringUpdate { get; set; }
            public string WiringYear { get; set; }

            public string InsName1 { get; set; }
            public string InsName1Col2 { get; set; }
            public string InsName2 { get; set; }
            public string InsName2Col2 { get; set; }
            public string LocAdd2 { get; set; }
            public string MailAdd2 { get; set; }
            public string Product { get; set; }
            public string Account { get; set; }
            public string Category { get; set; }
            public string Type { get; set; }
            public string BuildingTIVFlag { get; set; }
            public string ContentsTIVFlag { get; set; }
            public string AgeFlag { get; set; }
            public string AreaFlag { get; set; }
            public string ClassFlag { get; set; }
            public string InterestFlag { get; set; }
            public string ResidentialFlag { get; set; }
            public string UnitsFlag { get; set; }
            public string inspNum { get; set; }

            
        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
        static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string cfg_SQLMainSIBIConnStr = ConfigurationManager.AppSettings["SIBIConnStr"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];


        static LogUtils.LogUtils oLU;
        static bool bErr = false;

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();
        static StringBuilder sbConfEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {

            string sExcelFileName = "";
            string sConfEmail = "";
            string sPolicyNumber = "";
            string sAddress="";
            string sYearBuilt = "";
            string sStories = "";
            string sSqFt = "";
            string sConst = "";
            string sBldgCost = "";
            string sContCost = "";
            string sBusOps = "";

            bool bImportSuccess = false;

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                if (args.Length == 0)
                {
                    throw new ApplicationException("No arguments passed");
                }

                // get excel file name from command line
                sExcelFileName = args[0];

                // does file exist?
                FileInfo fi = new FileInfo(cfg_sourcedir + sExcelFileName);
                if (!fi.Exists)
                {
                    // send email
                    throw new ApplicationException("File does not exist: " + sExcelFileName);
                }

                // confirmation email
                //sConfEmail = args[1];
                //if (sConfEmail.Length == 0)
                sConfEmail = "jeff@sibfla.com ";
            }

            catch (Exception ex)
            {
                oLU.closeLog();
                bErr = true;
                sendErrEmail("Error initializing importBankers0818\r\n\r\n" + ex.Message);
                return;
            }


            try
            {

                oLU.WritetoLog("Processing: " + sExcelFileName);
                sbEmail.Append("<br />" + "Processing: " + sExcelFileName + "<br />");

                var excel = new ExcelQueryFactory(cfg_sourcedir + sExcelFileName);
                var worksheetNames = excel.GetWorksheetNames();
                List<string> columnnames = new List<string>();
                columnnames = excel.GetColumnNames(worksheetNames.First()).ToList();

                excel.AddMapping<ImportRecord>(x => x.inspNum, "Inspection #");
                excel.AddMapping<ImportRecord>(x => x.Product, "Product");
                excel.AddMapping<ImportRecord>(x => x.Account, "Account");
                excel.AddMapping<ImportRecord>(x => x.Category, "Category");
                excel.AddMapping<ImportRecord>(x => x.Type, "Type");
                excel.AddMapping<ImportRecord>(x => x.Underwriter, "CommercialUnderwriter");

                excel.AddMapping<ImportRecord>(x => x.AgencyAgentCode, "Agent#");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentPhone, "AgentPhone");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentName, "Agent Name");
                excel.AddMapping<ImportRecord>(x => x.PolicyNumber, "Policy");
                excel.AddMapping<ImportRecord>(x => x.EffectiveDate, "EffectiveDate");
                excel.AddMapping<ImportRecord>(x => x.InsuredName, "Insured Name");
                excel.AddMapping<ImportRecord>(x => x.ContactPhoneHome, "Phone");

                excel.AddMapping<ImportRecord>(x => x.MailAddress1, "MailingAddress1");
                excel.AddMapping<ImportRecord>(x => x.MailAddress2, "MailingAddress2");
                excel.AddMapping<ImportRecord>(x => x.MailCity, "MailingCity");
                excel.AddMapping<ImportRecord>(x => x.MailState, "MailingState");
                excel.AddMapping<ImportRecord>(x => x.MailZip, "MailingZip");

                excel.AddMapping<ImportRecord>(x => x.InsuredName, "Insured Name");
                excel.AddMapping<ImportRecord>(x => x.ContactPhoneHome, "Phone");

                excel.AddMapping<ImportRecord>(x => x.ContactName, "ContactName");
                excel.AddMapping<ImportRecord>(x => x.ContactPhoneCell, "ContactPhone");

                excel.AddMapping<ImportRecord>(x => x.YearBuilt, "YearBuilt");
                excel.AddMapping<ImportRecord>(x => x.SquareFootage, "SquareFeet");
                excel.AddMapping<ImportRecord>(x => x.Construction, "Construction Code Desc");
                excel.AddMapping<ImportRecord>(x => x.Occupancy, "InterestType");
                excel.AddMapping<ImportRecord>(x => x.NumberOfFloors, "NumberOfStories");

                excel.AddMapping<ImportRecord>(x => x.LocationAddress1, "PropertyAdress1");
                excel.AddMapping<ImportRecord>(x => x.LocationAddress2, "PropertyAdress2");
                excel.AddMapping<ImportRecord>(x => x.LocationCity, "PropertyCity");
                excel.AddMapping<ImportRecord>(x => x.LocationState, "PropertyState");
                excel.AddMapping<ImportRecord>(x => x.LocationZip, "PropertyZip");

                excel.AddMapping<ImportRecord>(x => x.BuildingCost, "BuildingLimit");
                excel.AddMapping<ImportRecord>(x => x.ContentsCost, "ContentsLimit");
                excel.AddMapping<ImportRecord>(x => x.BusinessOperations, "Class Code Desc");
                //excel.AddMapping<ImportRecord>(x => x.InspectionType, columnnames[31]);
                //excel.AddMapping<ImportRecord>(x => x.RushHandling, "RUSH");
                //excel.AddMapping<ImportRecord>(x => x.Comments, "Comments");

                excel.AddMapping<ImportRecord>(x => x.BuildingTIVFlag, "Building TIV Flag");
                excel.AddMapping<ImportRecord>(x => x.ContentsTIVFlag, "Contents TIV Flag");
                //excel.AddMapping<ImportRecord>(x => x.AgeFlag, "AgeFlag");
                //excel.AddMapping<ImportRecord>(x => x.AreaFlag, "AreaFlag");
                //excel.AddMapping<ImportRecord>(x => x.ClassFlag, "ClassFlag");
                //excel.AddMapping<ImportRecord>(x => x.InterestFlag, "InterestFlag");
                //excel.AddMapping<ImportRecord>(x => x.ResidentialFlag, "ResidentialFlag");
                //excel.AddMapping<ImportRecord>(x => x.UnitsFlag, "UnitsFlag");

                var rows = (from row in excel.Worksheet<ImportRecord>(0) select row).ToList();

                //buildConfHeader();

                int iRow = 0;
                int iRowsProcessed = 0;
                int iMaxRows = (rows.Count)-1;
                string sCurRowNum = "";
                string sNextRowNum = "";

                //for each row in worksheet
                while (true)
                {
                    var row = rows[iRow];

                    // skip row if policy number is empty
                    if (row.PolicyNumber == null)
                    {
                        iRow++;
                    }
                    else
                    {

                        iRowsProcessed++;

                        ImportRequests oAPI = new ImportRequests();
                        oAPI.CustomerUserName = "APIProd";
                        oAPI.CustomerPassword = "Sutton2012";

                        string sCustAcnt = "";
                        if (row.Account.Contains("8015"))
                            sCustAcnt = "8015";
                        else if (row.Account.Contains("8016"))
                            sCustAcnt = "8016";
                        else if (row.Account.Contains("8013"))
                            sCustAcnt = "8013";
                        else if (row.Account.Contains("7315"))
                            sCustAcnt = "7315";

                        if (sCustAcnt=="")
                            throw new Exception("Invalid customer avccount - File name: " + sExcelFileName + " - Policy#: " + row.PolicyNumber);

                        sCurRowNum = row.inspNum.Trim();
                        oAPI.CustomerAccount = sCustAcnt;
                        oAPI.Underwriter = row.Underwriter.Trim();
                        oAPI.AgentCode = row.AgencyAgentCode.Trim();
                        oAPI.AgencyAgentName = row.AgencyAgentName.Trim();
                        oAPI.AgencyAgentPhone = row.AgencyAgentPhone.Trim();
                        oAPI.AgencyAgentContact = "";
                        sPolicyNumber = row.PolicyNumber.Trim();
                        oAPI.PolicyNumber = sPolicyNumber;
                        oAPI.InsuredName = row.InsuredName.Trim();
                        oAPI.ContactPhoneHome = row.ContactPhoneHome.Trim();
                        oAPI.MailAddress1 = row.MailAddress1.Trim();
                        oAPI.MailAddress2 = row.MailAddress2.Trim();
                        oAPI.MailCity = row.MailCity.Trim();
                        oAPI.MailState = row.MailState.Trim();
                        oAPI.MailZip = row.MailZip.Trim();
                        oAPI.ContactName = row.ContactName.Trim();
                        oAPI.ContactPhoneCell = row.ContactPhoneCell.Trim();
                        oAPI.YearBuilt = row.YearBuilt ?? ""; 
                        oAPI.SquareFootage = row.SquareFootage.Trim() ?? "";
                        oAPI.Construction = row.Construction.Trim() ?? "";
                        oAPI.Occupancy = row.Occupancy ?? "NA";
                        oAPI.Stories = row.NumberOfFloors.Trim() ?? "";
                        oAPI.LocationAddress1 = row.LocationAddress1.Trim();
                        oAPI.LocationAddress2 = row.LocationAddress2.Trim();
                        oAPI.LocationCity = row.LocationCity.Trim();
                        oAPI.LocationState = row.LocationState.Trim();
                        oAPI.LocationZip = row.LocationZip.Trim();
                        oAPI.BuildingCost = row.BuildingCost.Trim() ?? "";
                        oAPI.ContentsCost = row.ContentsCost.Trim() ?? "";
                        oAPI.BusinessOperations = row.BusinessOperations ?? "NA";
                        oAPI.EffectiveDate = row.EffectiveDate.Trim();

                        oAPI.CoverageA = "";
                        oAPI.InsuranceCompany = "";
                        oAPI.ContactPhoneWork = "";
                        oAPI.EmailConfirmation = "";
                        oAPI.UnderwriterFirstName = "";
                        oAPI.UnderwriterLastName = "";
                        oAPI.LocationContactPhone = "";
                        oAPI.LocationContactName = "";
                        oAPI.LocationContactPhone = "";

                        oAPI.AgentAddress1 = "5656 Central Ave";
                        oAPI.AgentAddress2 = "";
                        oAPI.AgentCity = "St. Petersburg";
                        oAPI.AgentState = "FL";
                        oAPI.AgentZip = "33707";
                        oAPI.AgentEmail = "CustomerServices@sibfla.com";

                        // handle empty inspection type
                        string sInspType = "";
                        string sType = "";

                        if (row.Type == null)
                        {
                            throw new Exception("No Inspection Type - File name: " + sExcelFileName + " - Policy#: " + sPolicyNumber);
                        }

                        sType = row.Type.Trim().ToUpper();  // 8015 Package Bankers Import
                        if (sType.Contains("PACK"))
                            sInspType = "PACK";
                        else if (sType.Contains("RENEWAL - BANKERS") || sType.Contains("RENEWAL BANKERS"))  // 8015 Renewal - Bankers Import
                            sInspType = "PACK-REN";
                        else if (sType.Contains("HOMEOWNERS"))  // 8016 Bankers Homeowners Association Import
                            sInspType = "PACK";
                        else if (sType.Contains("HOA RENEWAL"))  // 8013 Bankers HOA Renewal Import
                            sInspType = "PACK-REN";
                        else if (sType.Contains("MOBILE"))  // 8016 Bankers Mobile Home Park Import
                            sInspType = "PACK";
                        else if (sType.Contains("MHP"))  // 8016 Bankers MHP Renewal Import
                            sInspType = "PACK-REN";

                        if (sInspType == "")
                        {
                            throw new Exception("Invalid Inspection Type - File name: " + sExcelFileName + " - Policy#: " + sPolicyNumber);
                        }
                        
                        sInspType = sCustAcnt + "-" + sInspType;

                        // Remove any spaces
                        sInspType = sInspType.Replace(" ", "");

                        oAPI.InspectionType = sInspType;


                        // comments
                        oAPI.Comments = "";
                        if (oAPI.SquareFootage.Length > 0)
                            oAPI.Comments += "Square footage: " + oAPI.SquareFootage + "<br />";
                        if (oAPI.Construction.Length > 0)
                            oAPI.Comments += "Construction: " + oAPI.Construction + "<br />";
                        if (oAPI.Occupancy.Length > 0 && oAPI.Occupancy != "Null")
                            oAPI.Comments += "Occupancy: " + oAPI.Occupancy + "<br />";
                        if (oAPI.Stories.Length > 0)
                            oAPI.Comments += "Number of stories: " + oAPI.Stories + "<br />";

                        oAPI.RushHandling = "N";

                        // **** handle multiple buildings ****

                        // if the current row is not the last row
                        if (iRow < iMaxRows)
                        {
                            // get the inspection number from the next row
                            iRow++;
                            var newRow = rows[iRow];
                            sNextRowNum = newRow.inspNum.Trim();

                            // if it is the same as the last row, put the details in comments
                            while (sNextRowNum == sCurRowNum && iRow < iMaxRows) 
                            {
                                sAddress = newRow.LocationAddress1.Trim();
                                sAddress += " " + newRow.LocationAddress2.Trim();
                                sYearBuilt = newRow.YearBuilt ?? "";
                                sSqFt = newRow.SquareFootage.Trim() ?? "";
                                sStories = row.NumberOfFloors.Trim() ?? "";
                                sConst = newRow.Construction.Trim() ?? "";
                                sBldgCost = newRow.BuildingCost.Trim() ?? "";
                                sContCost = newRow.ContentsCost.Trim() ?? "";
                                sBusOps = newRow.BusinessOperations ?? "NA";

                                oAPI.Comments += "<br />====== ADDITION LOCATION ======<br />";
                                oAPI.Comments += "Address: " + sAddress + "<br />";
                                oAPI.Comments += "Year built: " + sYearBuilt + "<br />";
                                oAPI.Comments += "Number of stories: " + sStories + "<br />";
                                oAPI.Comments += "Square footage: " + sSqFt + "<br />";
                                oAPI.Comments += "Construction: " + sConst + "<br />";
                                oAPI.Comments += "Building Cost: " + sBldgCost + "<br />";
                                oAPI.Comments += "Contents Cost: " + sContCost + "<br />";
                                oAPI.Comments += "Operations: " + sBusOps + "<br />";

                                iRow++;
                                if (iRow <= iMaxRows)
                                {
                                    newRow = rows[iRow];
                                    sNextRowNum = newRow.inspNum.Trim();
                                }
                            } 

                        }

                        // Custom/Generic field values
                        //oAPI.Occupancy = row.Occupancy ?? "";
                        //oAPI.Usage = row.Usage ?? "";
                        //oAPI.DwellingType = row.DwellingType ?? "";
                        //oAPI.Stories = row.NumberOfFloors ?? "";
                        //oAPI.YearBuilt = row.YearBuilt ?? "";

                        oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + sPolicyNumber);
                        sbEmail.Append("Importing Policy# " + sPolicyNumber + "<br />");

                        string sRet = oAPI.ImportBankers();

                        oLU.WritetoLog("oAPI.Import return for for Policy# " + sPolicyNumber + "\r\n\r\n" + sRet);

                        var importResults = sRet.FromJSON<List<ImportResult>>();

                        foreach (var importResult in importResults)
                        {

                            if (importResult.Successful)
                            {
                                oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                                sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + "<br />");
                                //buildConfLine(importResult.CaseNumber.ToString(), sPolicyNumber, row.PolicyCode, row.PolicyOccurance, row.InsName1 + row.InsName1Col2 + " " + row.InsName2 + row.InsName2Col2);
                                sendEmailConf(importResult.CaseNumber.ToString(), oAPI);
                            }
                            else
                            {
                                oLU.WritetoLog("**** Import failed **** "); if (importResult.Errors != null)
                                    sbEmail.Append("**** Import failed ****" + "<br />");

                                foreach (var error in importResult.Errors)
                                {
                                    oLU.WritetoLog("Error: " + error.ErrorText);
                                    sbEmail.Append("Error: " + error.ErrorText + "<br />");
                                }
                            }

                            if ((bool)importResult.Duplicate)
                            {
                                oLU.WritetoLog("Duplicate case");
                                sbEmail.Append("Duplicate case" + "<br />");
                            }
                        }

                    }   // Policy number not empty

                    if (iRow == iMaxRows)
                        break;

                }   // foreach row in sheet

                if (iMaxRows > 0 && iRowsProcessed > 0)
                {
                    bImportSuccess = true;
                }
                //if (bImportSuccess)
                //{
                //    buildConfFooter();
                //    sendConfEmail(sbConfEmail.ToString(), sConfEmail);
                //}


            }   //try

            catch (Exception ex)
            {
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                bErr = true;
                sbEmail.Append("Exception Logged" + "<br />" + ex.Message + "<br />");
                sendErrEmail("Exception logged" + "<br />" + ex.Message);
            }

            finally
            {

                // Copy source file to holding area and delete temp
                string sSourceName = cfg_sourcedir + sExcelFileName;
                string sDestName = "";
                DateTime dtNow = DateTime.Now;
                string sTimeStamp = dtNow.Minute.ToString() + dtNow.Second.ToString();

                //if successful - copy to \Processed
                //if failed - copy to \fail
                if (bImportSuccess)
                {
                    sDestName = cfg_compdir + sTimeStamp + sExcelFileName;
                }
                else
                {
                    sDestName = cfg_faildir + sTimeStamp + sExcelFileName;
                }

                // Copy Excel
                File.Copy(sSourceName, sDestName, true);
                if (File.Exists(sDestName))
                {
                    File.Delete(sSourceName);
                }
                else
                {
                    oLU.WritetoLog("Import Error: \r\n\r\n" + "Copy failed for: " + sDestName);
                    bErr = true;
                    sbEmail.Append("Copy failed for: " + sDestName + "<br />");
                }

                if (bErr)
                    sendErrEmail(sbEmail.ToString());
                else
                    sendLogEmail(sbEmail.ToString());

                oLU.closeLog();
            }

        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import Bankers Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import Bankers Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendEmailConf(string sCaseNum, ImportRequests oAPI)
        {

            try
            {
                EmailConfirmation360.EmailConf360 oEM = new EmailConfirmation360.EmailConf360();

                oEM.cfg_360ConnStr = cfg_360ConnStr;
                oEM.cfg_smtpserver = cfg_smtpserver;
                oEM.CaseNum = sCaseNum;
                oEM.CustNum = oAPI.CustomerAccount;
                oEM.InsuredName = oAPI.InsuredName;
                oEM.RequestedBy = oAPI.RequestedBy;
                oEM.Recip = "Inspections_CL@bankersfinancialcorp.com";
                oEM.BCC = "";
                oEM.DateSubmitted = DateTime.Now.ToString();
                oEM.InsAdd1 = oAPI.MailAddress1;
                oEM.InsAdd2 = oAPI.MailAddress2;
                oEM.InsCity = oAPI.MailCity;
                oEM.InsState = oAPI.MailState;
                oEM.InsZip = oAPI.MailZip;
                oEM.InsuredContactName = oAPI.ContactName;
                oEM.InsuredContactPhoneHome = oAPI.ContactPhoneHome;
                oEM.InsuredContactPhoneWork = oAPI.ContactPhoneWork;
                oEM.InsuredContactPhoneCell = oAPI.ContactPhoneCell;
                oEM.PolicyNum = oAPI.PolicyNumber;
                oEM.Agent = oAPI.AgencyAgentName;
                oEM.AgentPhone = oAPI.AgencyAgentPhone;
                oEM.InsuranceCo = oAPI.InsuranceCompany;
                oEM.Underwriter = oAPI.Underwriter;
                oEM.LocAdd1 = oAPI.LocationAddress1;
                oEM.LocAdd2 = oAPI.LocationAddress2;
                oEM.LocCity = oAPI.LocationCity;
                oEM.LocState = oAPI.LocationState;
                oEM.LocZip = oAPI.LocationZip;
                oEM.LocContact = oAPI.LocationContactName;
                oEM.LocContactPhone = oAPI.LocationContactPhone;
                oEM.InspectionType = oAPI.InspectionType;
                oEM.Comments = oAPI.Comments;
                oEM.EmailSubject = "Inspection order confirmation: " + oAPI.PolicyNumber;


                string sRet = oEM.sendEmailConf();
                if (sRet.Length > 0)
                    throw new ApplicationException(sRet);

            }
            catch (Exception ex)
            {
                string sErrText = "Error sending confirmation for case#: " + sCaseNum + System.Environment.NewLine + ex.Message;
                sendErrEmail(sErrText);
            }
        }

        static void sendConfEmail(string bodytext, string sConfEmail)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "inspections@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            //oMail.MailCC = "CLAU@bankersinsurance.com";
            //oMail.MailBCC = "jeff@sibfla.com";
            oMail.MsgSubject = "Inspection request confirmation";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void buildConfHeader()
        {
            // begin header
            sbConfEmail.Append("<html><head><title>Sutton Inspection Bureau, Inc. of Florida</title>" + System.Environment.NewLine);

            // Style
            sbConfEmail.Append("<style type='text/css'>" + System.Environment.NewLine);
            sbConfEmail.Append(".stdText { FONT-SIZE: 8pt; COLOR: black;Font-FAMILY: Verdana,Tahoma,Arial}" + System.Environment.NewLine);

            //end header
            sbConfEmail.Append("</style></head><body>" + System.Environment.NewLine);

            // begin body
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'>" + System.Environment.NewLine);
            sbConfEmail.Append("<tr><td width='10'>&nbsp;</td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='left' width='350'><span class='stdText'>Sutton Inspection Bureau, Inc. of Florida</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='right' width='300'><span class='stdText'>&nbsp;</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("</tr><tr><td>&nbsp;</td>");
            sbConfEmail.Append("<td align='left'><span class='stdText'>Inspection Request Confirmation</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='right'><span class='stdText'>" + DateTime.Now.ToString() + "</span></td></tr></table>" + System.Environment.NewLine);

            // HR
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='0' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650'><hr align='left' width='650'></td></tr></table>" + System.Environment.NewLine);

            // Start table
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'>");

            // Header row
            sbConfEmail.Append("<tr><td align='center' width='60' class='stdText'>Key</td>");
            sbConfEmail.Append("<td align='left' width='300' class='stdText'>Policy number</td>");
            sbConfEmail.Append("<td align='center' width='150' class='stdText'>Policy code</td>");
            sbConfEmail.Append("<td align='center' width='150' class='stdText'>Policy occurrence</td></tr>");

        }

        static void buildConfLine(string sCaseNum, string sPolicy, string sPolCode, string sPolOccur, string sInsured)
        {

            sbConfEmail.Append("<tr><td align='center' width='60' class='stdText'>" + sCaseNum + "</td>");
            sbConfEmail.Append("<td align='left' width='300' class='stdText'> &nbsp;" + sPolicy + "</td>");
            sbConfEmail.Append("<td align='center' width='150' class='stdText'> &nbsp;" + sPolCode + "</td>");
            sbConfEmail.Append("<td align='center' width='150' class='stdText'> &nbsp;" + sPolOccur + "</td></tr>");
            sbConfEmail.Append("<tr><td class='stdText'> &nbsp;</td><td align='left' class='stdText' colspan='3'>" + sInsured + "</td></tr>");

        }

        static void buildConfFooter()
        {
            sbConfEmail.Append("</table></form></body><HTML>");
        }

    }
}
