﻿using System;
using System.Windows.Forms;
using Amazon.S3;
using Amazon.S3.Transfer;
using System.Data.SqlClient;
using System.Data;

namespace S3Archive
{
    public partial class Form1 : Form
    {

        //static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;


        private const string bucketName = "LC360Sutton";
        private const string keyName = "m1P8cP7LVkKbpvDkVR2f/EJy0efl+GhzHcVAyV70";
        // Specify your bucket region (an example region is shown).
        //private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USWest2;
        //private static IAmazonS3 client;

        public Form1()
        {
            InitializeComponent();

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_logfilename = colNameVal.Get("logfilename");
            cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
            cfg_outputdir = colNameVal.Get("outputdir");
            cfg_emailnotify = colNameVal.Get("emailnotify");

        }

        private void btnGo_Click(object sender, EventArgs e)
        {

            if (tbCaseNum.Text.Length < 7)
            {
                MessageBox.Show("Case Number Required");
                return;
            }

            // SP
            // Get S3Name where InspectionID = CaseID and BackupItemType = 4
            // 1 = RS
            // 0 = jpg
            // 3 = jpg



            string sS3Name = "";
            string sTargetFileName = "";
            int iFileCount = 0;
            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
            sqlCmd1 = new SqlCommand();
            sqlCmd1.CommandType = CommandType.StoredProcedure;
            sqlCmd1.CommandText = "sp_GetS3FileName";
            sqlCmd1.CommandTimeout = 360;
            sqlCmd1.Connection = sqlConn1;
            sqlCmd1.Parameters.Clear();
            sqlCmd1.Parameters.AddWithValue("@casenum", Convert.ToInt32(tbCaseNum.Text));
            sqlConn1.Open();

            sqlReader = sqlCmd1.ExecuteReader();

            if (sqlReader.HasRows)
            {

                Amazon.RegionEndpoint bucketRegion = Amazon.RegionEndpoint.USEast1;
                AmazonS3Client s3Client = new AmazonS3Client("AKIATINATEZ2ZJRPFP7E", "m1P8cP7LVkKbpvDkVR2f/EJy0efl+GhzHcVAyV70", Amazon.RegionEndpoint.USEast1);
                TransferUtility fileTransferUtility = new TransferUtility(s3Client);

                tbResults.Text = "Locating archive file" + System.Environment.NewLine;

                sqlReader.Read();

                // loop through rows
                do
                {
                    if (sqlReader.IsDBNull(0))
                    {
                        sS3Name = "";
                    }
                    else
                    {
                        sS3Name = (string)sqlReader.GetSqlString(0);
                    }

                    sTargetFileName = cfg_outputdir + tbCaseNum.Text + ".pdf";
                    fileTransferUtility.Download(sTargetFileName, "LC360Sutton", sS3Name);
                    tbResults.Text = tbResults.Text + sTargetFileName + " downloaded." + System.Environment.NewLine;

                    iFileCount++;

                } while (sqlReader.Read());


            }   // has rows
            else
            {
                tbResults.Text = tbResults.Text + "No S3 file name found." + System.Environment.NewLine;
            }


            sqlReader.Close();
            sqlConn1.Close();
            sqlConn1 = null;
            sqlCmd1 = null;


            //string sFilePath = @"c:\temp\9662892.pdf";
            //string sSourceFile = "2b9fcd87-2601-47bd-a19a-4dcbb0d07c25/3e773d18-f722-48af-a923-74c428aa03c8/3e773d18-f722-48af-a923-74c428aa03c8.pdf";
            //"2b9fcd87-2601-47bd-a19a-4dcbb0d07c25\3e773d18-f722-48af-a923-74c428aa03c8.pdf";
            //"2b9fcd87-2601-47bd-a19a-4dcbb0d07c25/3e773d18-f722-48af-a923-74c428aa03c8/3e773d18-f722-48af-a923-74c428aa03c8.pdf";
            //string sSourceFile = "23135e181-b879-494a-8f16-8689839adbc9\727d0b87-e735-4f19-b10a-3e30acff8726.pdf";

            // Note the 'fileName' is the 'key' of the object in S3 (which is usually just the file name)
            //fileTransferUtility.Download(sFilePath, "LC360Sutton", sSourceFile);
            //fileTransferUtility.Download(sFilePath, "LC360Sutton", sSourceFile);

        }
    }
}
