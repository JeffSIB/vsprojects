﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;


namespace importBankers
{

    /// <summary>
    /// Called directly from import app
    /// Must reside on H:\VS2010\importBankers\importBankers\bin\Release
    /// </summary>
    /// 

    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyState { get; set; }
            public string PolicyNumber { get; set; }
            public string PolicyCode { get; set; }
            public string PolicyOccurance { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string AgencyAgentCode { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            public string BusinessOperations { get; set; }
            public string LocationFullAddress { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string CoverageB { get; set; }
            public string CoverageC { get; set; }
            public string CoverageD { get; set; }
            public string CoverageE { get; set; }
            public string CoverageF { get; set; }
            public string ISOClass { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }

            public string Construction { get; set; }
            public string YearBuilt { get; set; }
            public string DwellingType { get; set; }
            public string NumberOfFloors { get; set; }
            public string Occupancy { get; set; }
            public string SquareFootage { get; set; }
            public string DistanceToCoast { get; set; }
            public string RoofYear { get; set; }
            public string RoofMaterials { get; set; }
            public string RoofGeometry { get; set; }
            public string Usage { get; set; }
            public string WindMitigation { get; set; }
            public string PCClass { get; set; }
            public string RoofCladding { get; set; }
            public string RoofCondition { get; set; }
            public string BuildingCladding { get; set; }
            public string CompanyPolicyForm { get; set; }
            public string CentralFireAlarm { get; set; }
            public string CentralBurglarAlarm { get; set; }
            public string CentralAlarm { get; set; }
            public string PolicyStatus { get; set; }
            public string PoolFenced { get; set; }
            public string Heating { get; set; }
            public string HeatingFullPartial { get; set; }
            public string HeatingUpdate { get; set; }
            public string HeatingYear { get; set; }
            public string PoolAboveGround { get; set; }
            public string Farming { get; set; }
            public string NumberOfFamilies { get; set; }
            public string Pets { get; set; }
            public string Plumbing { get; set; }
            public string PlumbingFullPartial { get; set; }
            public string PlumbingUpdate { get; set; }
            public string PlumbingYear { get; set; }
            public string PoolSlide { get; set; }
            public string PoolDivingBoard { get; set; }
            public string PoolScreenEncl { get; set; }
            public string Pool { get; set; }
            public string Updates { get; set; }
            public string Wiring { get; set; }
            public string WiringFullPartial { get; set; }
            public string WiringUpdate { get; set; }
            public string WiringYear { get; set; }

            public string InsName1 { get; set; }
            public string InsName1Col2 { get; set; }
            public string InsName2 { get; set; }
            public string InsName2Col2 { get; set; }
            public string LocAdd2 { get; set; }
            public string MailAdd2 { get; set; }

        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
        static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string cfg_SQLMainSIBIConnStr = ConfigurationManager.AppSettings["SIBIConnStr"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_compdir = ConfigurationManager.AppSettings["CompDir"];
        static string cfg_faildir = ConfigurationManager.AppSettings["FailDir"];


        static LogUtils.LogUtils oLU;
        static bool bErr = false;

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();
        static StringBuilder sbConfEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {

            string sExcelFileName = "";
            string sConfEmail = "";
            string sPolicyNumber = "";

            bool bImportSuccess = false;

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                if (args.Length == 0)
                {
                    throw new ApplicationException("No arguments passed");                
                }

                // get excel file name from command line
                sExcelFileName = args[0];

                // does file exist?
                FileInfo fi = new FileInfo(cfg_sourcedir + sExcelFileName);
                if (!fi.Exists)
                {
                    // send email
                    throw new ApplicationException("File does not exist: " + sExcelFileName);
                }

                // confirmation email
                //sConfEmail = args[1];
                //if (sConfEmail.Length == 0)
                sConfEmail = "vtlopez@bankersinsurance.com ";
            }

            catch (Exception ex)
            {
                oLU.closeLog();
                bErr = true;
                sendErrEmail("Error initializing importBankers\r\n\r\n" + ex.Message);
                return;
            }
            
 
            try
            {

                oLU.WritetoLog("Processing: " + sExcelFileName);
                sbEmail.Append("<br />" + "Processing: " + sExcelFileName + "<br />");

                var excel = new ExcelQueryFactory(cfg_sourcedir + sExcelFileName);
                var worksheetNames = excel.GetWorksheetNames();
                List<string> columnnames = new List<string>();
                columnnames = excel.GetColumnNames( worksheetNames.First()).ToList();

                if (columnnames.Count == 33)
                {

                    /////////////////////////////////////////////////////////////////////
                    ////  No mailing address                                         ////
                    /////////////////////////////////////////////////////////////////////
                    excel.AddMapping<ImportRecord>(x => x.CustomerAccount, columnnames[1]);
                    excel.AddMapping<ImportRecord>(x => x.AgencyAgentCode, "Agent #");
                    excel.AddMapping<ImportRecord>(x => x.AgencyAgentPhone, "Agent Ph#");
                    excel.AddMapping<ImportRecord>(x => x.AgencyAgentName, "Agent Name");
                    excel.AddMapping<ImportRecord>(x => x.PolicyState, "Pol_State");
                    excel.AddMapping<ImportRecord>(x => x.PolicyNumber, "Pol_Number");
                    excel.AddMapping<ImportRecord>(x => x.PolicyCode, "Pol_Code");
                    excel.AddMapping<ImportRecord>(x => x.PolicyOccurance, "Pol_Occur");
                    excel.AddMapping<ImportRecord>(x => x.InsName1, "Insured Name 1");
                    excel.AddMapping<ImportRecord>(x => x.InsName1Col2, columnnames[10]);
                    excel.AddMapping<ImportRecord>(x => x.InsName2, "Insured Name 2");
                    excel.AddMapping<ImportRecord>(x => x.InsName2Col2, columnnames[12]);
                    excel.AddMapping<ImportRecord>(x => x.ContactPhoneHome, "Phone");
                    excel.AddMapping<ImportRecord>(x => x.ContactName, "Contact");
                    excel.AddMapping<ImportRecord>(x => x.YearBuilt, "Year Built");
                    excel.AddMapping<ImportRecord>(x => x.SquareFootage, "Square Feet");
                    excel.AddMapping<ImportRecord>(x => x.Construction, "Const. Code Desc.");
                    excel.AddMapping<ImportRecord>(x => x.Occupancy, "Interest Type");
                    excel.AddMapping<ImportRecord>(x => x.NumberOfFloors, "# of Stry");
                    excel.AddMapping<ImportRecord>(x => x.LocationAddress1, "Property Address");
                    excel.AddMapping<ImportRecord>(x => x.LocAdd2, columnnames[24]);
                    excel.AddMapping<ImportRecord>(x => x.LocationCity, columnnames[25]);
                    excel.AddMapping<ImportRecord>(x => x.LocationState, columnnames[26]);
                    excel.AddMapping<ImportRecord>(x => x.LocationZip, columnnames[27]);
                    excel.AddMapping<ImportRecord>(x => x.BuildingCost, "Building");
                    excel.AddMapping<ImportRecord>(x => x.ContentsCost, "Contents");
                    excel.AddMapping<ImportRecord>(x => x.BusinessOperations, "Class Code Desc#");
                    excel.AddMapping<ImportRecord>(x => x.InspectionType, columnnames[31]);
                    excel.AddMapping<ImportRecord>(x => x.RushHandling, "RUSH");
                    excel.AddMapping<ImportRecord>(x => x.Comments, "Comments");

                    var rows = (from row in excel.Worksheet<ImportRecord>(0) select row).ToList();

                    buildConfHeader();

                    foreach (var row in rows)
                    {

                        // skip row if policy number is empty
                        if (row.PolicyNumber != null)
                        {

                            ImportRequests oAPI = new ImportRequests();
                            oAPI.CustomerUserName = "APIProd";
                            oAPI.CustomerPassword = "Sutton2012";
                            oAPI.CustomerAccount = row.CustomerAccount;

                            // standard values provided
                            oAPI.InsuredName = row.InsName1 + row.InsName1Col2 + " " + row.InsName2 + row.InsName2Col2;
                            oAPI.ContactName = row.ContactName;
                            oAPI.ContactPhoneWork = "";
                            oAPI.ContactPhoneCell = "";
                            oAPI.ContactPhoneHome = row.ContactPhoneHome;
                            sPolicyNumber = row.PolicyState + " " + row.PolicyNumber + " " + row.PolicyCode + " " + row.PolicyOccurance;
                            oAPI.PolicyNumber = sPolicyNumber;
                            oAPI.EffectiveDate = "";
                            oAPI.CoverageA = "";
                            oAPI.LocationAddress1 = row.LocationAddress1;
                            oAPI.LocationAddress2 = "";
                            oAPI.LocationCity = row.LocationCity;
                            oAPI.LocationState = row.LocationState;
                            oAPI.LocationZip = row.LocationZip;
                            oAPI.MailAddress1 = row.LocationAddress1;
                            oAPI.MailAddress2 = "";
                            oAPI.MailCity = row.LocationCity;
                            oAPI.MailState = row.LocationState;
                            oAPI.MailZip = row.LocationZip;
                            oAPI.InsuranceCompany = row.InsuranceCompany;
                            oAPI.AgentCode = row.AgencyAgentCode;
                            oAPI.AgencyAgentName = row.AgencyAgentName;
                            oAPI.AgencyAgentPhone = row.AgencyAgentPhone;

                            // standard values not provided
                            oAPI.EmailConfirmation = "";
                            oAPI.Underwriter = "";
                            oAPI.UnderwriterFirstName = "";
                            oAPI.UnderwriterLastName = "";
                            oAPI.LocationContactPhone = "";
                            oAPI.LocationContactName = "";
                            oAPI.LocationContactPhone = "";

                            // Validate customer account
                            if (row.CustomerAccount != "8013" && row.CustomerAccount != "8015" && row.CustomerAccount != "8016" && row.CustomerAccount != "7315")
                            {
                                throw new Exception("Invalid account code- File name: " + sExcelFileName + " - Policy#: " + sPolicyNumber);
                            }


                            // Convert year built to numeric
                            int iYearBuilt = Convert.ToInt32(row.YearBuilt);
                            int iYearsOld = 0;
                            if (iYearBuilt > 0)
                            {
                                iYearsOld = DateTime.Now.Year - iYearBuilt;
                            }


                            // handle empty inspection type
                            string sInspType = "";
                            if (row.InspectionType == null)
                            {
                                throw new Exception("No Inspection Type - File name: " + sExcelFileName + " - Policy#: " + sPolicyNumber);
                            }

                            sInspType = row.InspectionType.Trim().ToUpper();
                            if (sInspType != "PACK" && sInspType != "PACK-REN")
                            { 
                                if (sInspType.Contains("REN"))
                                    sInspType = "PACK-REN";
                                else
                                    sInspType = "PACK";                          
                            }

                            sInspType = row.CustomerAccount + "-" + sInspType;

                            // Remove any spaces
                            sInspType = sInspType.Replace(" ", "");

                            oAPI.InspectionType = sInspType;

                            // comments - remove Thank  you
                            oAPI.Comments = row.Comments;
                            if (oAPI.Comments.Trim().ToUpper() == "THANK YOU" || oAPI.Comments.Trim().ToUpper() == "THANK YOU.")
                                oAPI.Comments = "";

                            // Rush
                            //if (row.RushHandling == null)
                            //{
                            //    oAPI.RushHandling = "N";
                            //}
                            //else
                            //{
                            //    if (row.RushHandling.Trim().Length > 0)
                            //        oAPI.RushHandling = "Y";
                            //    else
                            //        oAPI.RushHandling = "N";
                            //}
                            oAPI.RushHandling = "N";

                            // Custom/Generic field values
                            oAPI.Occupancy = row.Occupancy ?? "";
                            oAPI.Usage = row.Usage ?? "";
                            oAPI.DwellingType = row.DwellingType ?? "";
                            oAPI.Stories = row.NumberOfFloors ?? "";
                            oAPI.YearBuilt = row.YearBuilt ?? "";
                            oAPI.BusinessOperations = row.BusinessOperations ?? "NA";
                            oAPI.BuildingCost = row.BuildingCost ?? "";
                            oAPI.ContentsCost = row.ContentsCost ?? "";

                            oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + sPolicyNumber);
                            sbEmail.Append("Importing Policy# " + sPolicyNumber + "<br />");

                            string sRet = oAPI.ImportBankers();

                            oLU.WritetoLog("oAPI.Import return for for Policy# " + sPolicyNumber + "\r\n\r\n" + sRet);

                            var importResults = sRet.FromJSON<List<ImportResult>>();

                            foreach (var importResult in importResults)
                            {

                                if (importResult.Successful)
                                {
                                    oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                                    sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + "<br />");
                                    buildConfLine(importResult.CaseNumber.ToString(), sPolicyNumber, row.PolicyCode, row.PolicyOccurance, row.InsName1 + row.InsName1Col2 + " " + row.InsName2 + row.InsName2Col2);
                                }
                                else
                                {
                                    oLU.WritetoLog("**** Import failed **** "); if (importResult.Errors != null)
                                        sbEmail.Append("**** Import failed ****" + "<br />");

                                    foreach (var error in importResult.Errors)
                                    {
                                        oLU.WritetoLog("Error: " + error.ErrorText);
                                        sbEmail.Append("Error: " + error.ErrorText + "<br />");
                                    }
                                }

                                if ((bool)importResult.Duplicate)
                                {
                                    oLU.WritetoLog("Duplicate case");
                                    sbEmail.Append("Duplicate case" + "<br />");
                                }
                            }

                        }   // Policy number not empty

                    }   // foreach row in sheet

                    bImportSuccess = true;

                }   // 3000 BOP

                else if (columnnames.Count > 36 )
                {
                    /////////////////////////////////////////////////////////////////////
                    ////  w/Mailing adress                                           ////
                    /////////////////////////////////////////////////////////////////////
                    excel.AddMapping<ImportRecord>(x => x.CustomerAccount, columnnames[1]);
                    excel.AddMapping<ImportRecord>(x => x.AgencyAgentCode, "Agent #");
                    excel.AddMapping<ImportRecord>(x => x.AgencyAgentPhone, "Agent Ph#");
                    excel.AddMapping<ImportRecord>(x => x.AgencyAgentName, "Agent Name");
                    excel.AddMapping<ImportRecord>(x => x.PolicyState, "Pol_State");
                    excel.AddMapping<ImportRecord>(x => x.PolicyNumber, "Pol_Number");
                    excel.AddMapping<ImportRecord>(x => x.PolicyCode, "Pol_Code");
                    excel.AddMapping<ImportRecord>(x => x.PolicyOccurance, "Pol_Occur");
                    excel.AddMapping<ImportRecord>(x => x.InsName1, "Insured Name 1");
                    excel.AddMapping<ImportRecord>(x => x.InsName1Col2, columnnames[11]);
                    excel.AddMapping<ImportRecord>(x => x.InsName2, "Insured Name 2");
                    excel.AddMapping<ImportRecord>(x => x.InsName2Col2, columnnames[13]);
                    excel.AddMapping<ImportRecord>(x => x.ContactPhoneHome, "Phone");
                    excel.AddMapping<ImportRecord>(x => x.MailAddress1, "Mailing Address");
                    excel.AddMapping<ImportRecord>(x => x.MailAddress2, columnnames[16]);
                    excel.AddMapping<ImportRecord>(x => x.MailCity, columnnames[17]);
                    excel.AddMapping<ImportRecord>(x => x.MailState, columnnames[18]);
                    excel.AddMapping<ImportRecord>(x => x.MailZip, columnnames[19]);
                    excel.AddMapping<ImportRecord>(x => x.ContactName, "Contact");
                    excel.AddMapping<ImportRecord>(x => x.ContactPhoneWork, "Contact #");
                    excel.AddMapping<ImportRecord>(x => x.YearBuilt, "Year Built");
                    excel.AddMapping<ImportRecord>(x => x.SquareFootage, "Square Feet");
                    excel.AddMapping<ImportRecord>(x => x.Construction, "Const. Code Desc.");
                    excel.AddMapping<ImportRecord>(x => x.Occupancy, "Interest Type");
                    excel.AddMapping<ImportRecord>(x => x.NumberOfFloors, "# of Stry");
                    excel.AddMapping<ImportRecord>(x => x.LocationAddress1, "Property Address");
                    excel.AddMapping<ImportRecord>(x => x.LocAdd2, columnnames[29]);
                    excel.AddMapping<ImportRecord>(x => x.LocationCity, columnnames[30]);
                    excel.AddMapping<ImportRecord>(x => x.LocationState, columnnames[31]);
                    excel.AddMapping<ImportRecord>(x => x.LocationZip, columnnames[32]);
                    excel.AddMapping<ImportRecord>(x => x.BuildingCost, "Building");
                    excel.AddMapping<ImportRecord>(x => x.ContentsCost, "Contents");
                    excel.AddMapping<ImportRecord>(x => x.BusinessOperations, "Class Code Desc#");
                    excel.AddMapping<ImportRecord>(x => x.InspectionType, columnnames[36]);
                    excel.AddMapping<ImportRecord>(x => x.RushHandling, "RUSH");
                    excel.AddMapping<ImportRecord>(x => x.Comments, "Comments");

                    var rows = (from row in excel.Worksheet<ImportRecord>(0) select row).ToList();

                    buildConfHeader();

                    foreach (var row in rows)
                    {

                        // skip row if policy number is empty
                        if (row.PolicyNumber != null)
                        {

                            ImportRequests oAPI = new ImportRequests();
                            oAPI.CustomerUserName = "APIProd";
                            oAPI.CustomerPassword = "Sutton2012";
                            oAPI.CustomerAccount = row.CustomerAccount;

                            // standard values provided
                            oAPI.InsuredName = row.InsName1 + row.InsName1Col2 + " " + row.InsName2 + row.InsName2Col2;
                            oAPI.ContactName = row.ContactName;
                            oAPI.ContactPhoneWork = row.ContactPhoneWork;
                            oAPI.ContactPhoneCell = "";
                            oAPI.ContactPhoneHome = row.ContactPhoneHome;
                            sPolicyNumber = row.PolicyState + " " + row.PolicyNumber + " " + row.PolicyCode + " " + row.PolicyOccurance;
                            oAPI.PolicyNumber = sPolicyNumber;
                            oAPI.EffectiveDate = "";
                            oAPI.CoverageA = "";
                            oAPI.LocationAddress1 = row.LocationAddress1;
                            oAPI.LocationAddress2 = "";
                            oAPI.LocationCity = row.LocationCity;
                            oAPI.LocationState = row.LocationState;
                            oAPI.LocationZip = row.LocationZip;
                            oAPI.MailAddress1 = row.MailAddress1 + row.MailAddress2;
                            oAPI.MailAddress2 = "";
                            oAPI.MailCity = row.MailCity;
                            oAPI.MailState = row.MailState;
                            oAPI.MailZip = row.MailZip;
                            oAPI.InsuranceCompany = row.InsuranceCompany ?? "";

                            // Agency / Agent
                            oAPI.AgencyAgentName = row.AgencyAgentName;
                            oAPI.AgentCode = row.AgencyAgentCode;
                            oAPI.AgencyAgentPhone = row.AgencyAgentPhone;
                            oAPI.AgentFax = "";
                            oAPI.AgencyAgentContact = "";
                            oAPI.AgentAddress1 = "5656 Central Ave";
                            oAPI.AgentAddress2 = "";
                            oAPI.AgentCity = "St. Petersburg";
                            oAPI.AgentState = "FL";
                            oAPI.AgentZip = "33707";

                            // standard values not provided
                            oAPI.EmailConfirmation = "";
                            oAPI.Underwriter = "";
                            oAPI.UnderwriterFirstName = "";
                            oAPI.UnderwriterLastName = "";
                            oAPI.LocationContactPhone = "";
                            oAPI.LocationContactName = "";
                            oAPI.LocationContactPhone = "";

                            // Validate customer account
                            if (row.CustomerAccount != "8013" && row.CustomerAccount != "8015" && row.CustomerAccount != "8016" && row.CustomerAccount != "7315")
                            {
                                throw new Exception("Invalid account code- File name: " + sExcelFileName + " - Policy#: " + sPolicyNumber);
                            }


                            // Convert year built to numeric
                            int iYearBuilt = 0;
                            int iYearsOld = 0;

                            if (int.TryParse(row.YearBuilt,out iYearBuilt))
                            {
                                iYearsOld = DateTime.Now.Year - iYearBuilt;
                            }


                            // handle empty inspection type
                            string sInspType = "";
                            if (row.InspectionType == null)
                            {
                                throw new Exception("No Inspection Type - File name: " + sExcelFileName + " - Policy#: " + sPolicyNumber);
                            }

                            sInspType = row.InspectionType.Trim().ToUpper();
                            if (sInspType != "PACK" && sInspType != "PACK-REN")
                            {
                                if (sInspType.Contains("REN"))
                                    sInspType = "PACK-REN";
                                else
                                    sInspType = "PACK";
                            }

                            sInspType = row.CustomerAccount + "-" + sInspType;

                            // Remove any spaces
                            sInspType = sInspType.Replace(" ", "");

                            oAPI.InspectionType = sInspType;

                            // comments - remove Thank  you
                            if (row.Comments == null)
                                oAPI.Comments = "";
                            else
                            {
                                oAPI.Comments = row.Comments;
                                if (oAPI.Comments.Trim().ToUpper() == "THANK YOU" || oAPI.Comments.Trim().ToUpper() == "THANK YOU.")
                                    oAPI.Comments = "";
                            }

                            // Rush
                            //if (row.RushHandling == null)
                            //{
                            //    oAPI.RushHandling = "N";
                            //}
                            //else
                            //{
                            //    if (row.RushHandling.Trim().Length > 0)
                            //        oAPI.RushHandling = "Y";
                            //    else
                            //        oAPI.RushHandling = "N";
                            //}
                            oAPI.RushHandling = "N";

                            // Custom/Generic field values
                            oAPI.Occupancy = row.Occupancy ?? "";
                            oAPI.Usage = row.Usage ?? "";
                            oAPI.DwellingType = row.DwellingType ?? "";
                            oAPI.Stories = row.NumberOfFloors ?? "";
                            oAPI.YearBuilt = row.YearBuilt ?? "";
                            oAPI.BusinessOperations = row.BusinessOperations ?? "NA";
                            oAPI.BuildingCost = row.BuildingCost ?? "";
                            oAPI.ContentsCost = row.ContentsCost ?? "";

                            oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + sPolicyNumber);
                            sbEmail.Append("Importing Policy# " + sPolicyNumber + "<br />");

                            string sRet = oAPI.ImportBankers();

                            oLU.WritetoLog("oAPI.Import return for for Policy# " + sPolicyNumber + "\r\n\r\n" + sRet);

                            var importResults = sRet.FromJSON<List<ImportResult>>();

                            foreach (var importResult in importResults)
                            {

                                if (importResult.Successful)
                                {
                                    oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                                    sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + "<br />");
                                    buildConfLine(importResult.CaseNumber.ToString(), sPolicyNumber, row.PolicyCode, row.PolicyOccurance, row.InsName1 + row.InsName1Col2 + " " + row.InsName2 + row.InsName2Col2);
                                }
                                else
                                {
                                    oLU.WritetoLog("**** Import failed **** "); if (importResult.Errors != null)
                                        sbEmail.Append("**** Import failed ****" + "<br />");

                                    foreach (var error in importResult.Errors)
                                    {
                                        oLU.WritetoLog("Error: " + error.ErrorText);
                                        sbEmail.Append("Error: " + error.ErrorText + "<br />");
                                    }
                                }

                                if ((bool)importResult.Duplicate)
                                {
                                    oLU.WritetoLog("Duplicate case");
                                    sbEmail.Append("Duplicate case" + "<br />");
                                }
                            }

                        }   // Policy number not empty

                    }   // foreach row in sheet

                    bImportSuccess = true;

                }   // 8000 BOP

                else
                {
                    // invalid column count
                    throw new ApplicationException("Invalid column count in file: " + sExcelFileName);
                }

                if (bImportSuccess)
                {
                    buildConfFooter();
                    sendConfEmail(sbConfEmail.ToString(),sConfEmail);                
                }

                
            }   //try

            catch (Exception ex)
            {
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                bErr = true;
                sbEmail.Append("Exception Logged" + "<br />" + ex.Message + "<br />");
                sendErrEmail("Exception logged" + "<br />" + ex.Message);
            }

            finally
            {

                // Copy source file to holding area and delete temp
                string sSourceName = cfg_sourcedir + sExcelFileName;
                string sDestName = "";
                DateTime dtNow = DateTime.Now;
                string sTimeStamp = dtNow.Minute.ToString() + dtNow.Second.ToString();

                //if successful - copy to \Processed
                //if failed - copy to \fail
                if (bImportSuccess)
                {
                    sDestName = cfg_compdir + sTimeStamp + sExcelFileName;
                }
                else
                {
                    sDestName = cfg_faildir + sTimeStamp + sExcelFileName;
                }

                // Copy Excel
                File.Copy(sSourceName, sDestName,true);
                if (File.Exists(sDestName))
                {
                    File.Delete(sSourceName);
                }
                else
                {
                    oLU.WritetoLog("Import Error: \r\n\r\n" + "Copy failed for: " + sDestName);
                    bErr = true;
                    sbEmail.Append("Copy failed for: " + sDestName + "<br />");
                }

                if (bErr)
                    sendErrEmail(sbEmail.ToString());
                else
                    sendLogEmail(sbEmail.ToString());

                oLU.closeLog();
            }
            
        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import Bankers Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import Bankers Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendConfEmail(string bodytext,string sConfEmail)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "inspections@sibfla.com";
            oMail.MailTo = sConfEmail;
            //oMail.MailCC = "CLAU@bankersinsurance.com";
            oMail.MailBCC = "jeff@sibfla.com";
            oMail.MsgSubject = "Inspection request confirmation";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void buildConfHeader()
        { 
            // begin header
            sbConfEmail.Append("<html><head><title>Sutton Inspection Bureau, Inc. of Florida</title>" + System.Environment.NewLine);

            // Style
            sbConfEmail.Append("<style type='text/css'>" + System.Environment.NewLine);
            sbConfEmail.Append(".stdText { FONT-SIZE: 8pt; COLOR: black;Font-FAMILY: Verdana,Tahoma,Arial}" + System.Environment.NewLine);
    
            //end header
            sbConfEmail.Append("</style></head><body>" + System.Environment.NewLine);

            // begin body
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'>" + System.Environment.NewLine);
            sbConfEmail.Append("<tr><td width='10'>&nbsp;</td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='left' width='350'><span class='stdText'>Sutton Inspection Bureau, Inc. of Florida</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='right' width='300'><span class='stdText'>&nbsp;</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("</tr><tr><td>&nbsp;</td>");
            sbConfEmail.Append("<td align='left'><span class='stdText'>Inspection Request Confirmation</span></td>" + System.Environment.NewLine);
            sbConfEmail.Append("<td align='right'><span class='stdText'>" + DateTime.Now.ToString() + "</span></td></tr></table>" + System.Environment.NewLine);

            // HR
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='0' width='660' border='0'><tr><td width='10'>&nbsp;</td><td align='left' width='650'><hr align='left' width='650'></td></tr></table>" + System.Environment.NewLine);

            // Start table
            sbConfEmail.Append("<table cellSpacing='0' cellPadding='2' width='660' border='0'>");
    
            // Header row
            sbConfEmail.Append("<tr><td align='center' width='60' class='stdText'>Key</td>");
            sbConfEmail.Append("<td align='left' width='300' class='stdText'>Policy number</td>");
            sbConfEmail.Append("<td align='center' width='150' class='stdText'>Policy code</td>");
            sbConfEmail.Append("<td align='center' width='150' class='stdText'>Policy occurrence</td></tr>");  
        
        }

        static void buildConfLine(string sCaseNum, string sPolicy, string sPolCode, string sPolOccur, string sInsured)
        {

            sbConfEmail.Append("<tr><td align='center' width='60' class='stdText'>" + sCaseNum + "</td>");
            sbConfEmail.Append("<td align='left' width='300' class='stdText'> &nbsp;" + sPolicy + "</td>");
            sbConfEmail.Append("<td align='center' width='150' class='stdText'> &nbsp;" + sPolCode + "</td>");
            sbConfEmail.Append("<td align='center' width='150' class='stdText'> &nbsp;" + sPolOccur + "</td></tr>");
            sbConfEmail.Append("<tr><td class='stdText'> &nbsp;</td><td align='left' class='stdText' colspan='3'>" + sInsured + "</td></tr>");       
        
        }

        static void buildConfFooter()
        {
            sbConfEmail.Append("</table></form></body><HTML>");
        }
        
    }
}
