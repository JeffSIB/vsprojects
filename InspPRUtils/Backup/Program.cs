﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;


namespace InspPRUtils
{
    class Program
    {
        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_emailnotify;
        static string cfg_logfilename;

        static string cfg_SQLMainSIBIConnStr;

        static bool mbErr;
        static string msErrMsg;
        static string msMode;

        static DateTime dPayDate;

        static void Main(string[] args)
        {
            try
            {

                // set pay date 3 days in advance
                // assumes this is being run on Tuesday night for Friday pay date.
                dPayDate = DateTime.Today.AddDays(3);
                
                mbErr = false;
                msErrMsg = "";
                string sMsgBody;

                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;

                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_logfilename = colNameVal.Get("logfilename");

                cfg_SQLMainSIBIConnStr = colNameVal.Get("SQLMainSIBIConnStr");

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();

                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //  n = notify of pending pay date
                //  s = set pay date
                string sErr = "None";
                if (args.Length < 1)
                {
                    foreach (string s in args)
                    {
                        sErr += s + " - ";
                    }
                    throw new SystemException("Invalid number of arguments supplied: " + sErr);
                }

                if (args[0].ToUpper() == "N")
                {

                    // Send pending notification email 
                    msMode = "N";
                    sMsgBody = "Inspector payroll cutoff will occurr on " + DateTime.Now.ToShortDateString() + " at 10:00PM<br /> <br />";
                    sMsgBody += "If this is an issue please notify Jeff Kensinger immediately. <br /> <br />";
                    sMsgBody += "Automated message sent : " + DateTime.Now.ToString();

                    sendEmail(sMsgBody);

                }
                else if (args[0].ToUpper() == "S")
                {
                    // set pay date
                    msMode = "S";

                    if (setPayDate())
                    {
                        sMsgBody = "Inspector payroll cutoff ran at " + DateTime.Now.ToString() + "<br /> <br />";
                        sMsgBody += "The pay date was set to " + dPayDate.ToShortDateString() + "<br /> <br />";
                        sMsgBody += "Automated message sent : " + DateTime.Now.ToString();
                        sendEmail(sMsgBody);
                    }
                    else
                    {
                        sMsgBody = "**** INSPECTOR PAYROLL CUTOFF FAILED ****<br /> <br />";
                        sMsgBody += "Inspector payroll cutoff FAILED at " + DateTime.Now.ToString() + "<br /> <br />";
                        sMsgBody += "Automated message sent : " + DateTime.Now.ToString();
                        sendEmail(sMsgBody);

                        throw new SystemException("setPayDate Failed");

                    }
                }
                else
                {
                    throw new SystemException("Invalid arguments supplied: " + args[0].ToUpper());
                }


                
            }
            catch (Exception ex)
            {

                //catch exception which may be thrown
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;

                string sMsgBody;
                sMsgBody = "Error in module Main<br /> <br />";
                sMsgBody += ex.Message + "<br /> <br />";
                sMsgBody += "Automated message sent : " + DateTime.Now.ToString();

                sendErrEmail(sMsgBody);

            }

            // close log
            oLU.closeLog();

            if (mbErr)
            {
            }

        }

        static bool setPayDate()
        {

            bool bRet = false;

            try
            {

                oLU.WritetoLog("Setting pay date to " + dPayDate.ToShortDateString());

                // init SQL connection
                SqlConnection sqlCon = new SqlConnection(cfg_SQLMainSIBIConnStr);
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Connection = sqlCon;
                sqlCmd.CommandText = "sp_InspFees_SetPayPeriod";
                sqlCon.Open();

                sqlCmd.Parameters.AddWithValue("@PayDate", dPayDate);
                sqlCmd.ExecuteNonQuery();

                // close SQL
                sqlCon.Close();
                sqlCon = null;

                oLU.WritetoLog("Process completed at " + DateTime.Now.ToString());

                bRet = true;

            }
            catch (Exception ex)
            {

                //catch exception which may be thrown
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            return bRet;
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            
            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "Inspector Payroll";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;
            
            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by InspPRUtils **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }
    }
}
