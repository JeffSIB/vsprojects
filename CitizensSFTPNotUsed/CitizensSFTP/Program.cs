﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Configuration;
using WinSCP;


namespace CitizensSFTP
{
    class Program
    {

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];

        static LogUtils.LogUtils oLU;

        static void Main(string[] args)
        {

            try
            {

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                int iNumUploaded = 0;


                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = "sftp1.citizensfla.com",
                    UserName = "inspections_sutton_svc",
                    //Password = "121F2CLy",
                    Password = "5PMuT^QUpS&pMZn",
                    SshHostKeyFingerprint = "ssh-rsa 2048 12:52:0e:d1:16:c0:e1:ba:3d:20:81:e6:3e:3c:0c:d1"
                };

                using (Session session = new Session())
                {

                    session.SessionLogPath = @"\\nas\apps\automationlogs\CitizensSFTP\SFTPLogs.txt";

                    // Connect
                    session.Open(sessionOptions);

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    transferResult = session.PutFiles(cfg_sourcedir + "*", "/", true, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        oLU.WritetoLog(transfer.FileName + " uploaded");
                        iNumUploaded++;
                    }


                }

                if (iNumUploaded > 0)
                    sendConfEmail(iNumUploaded.ToString() + " files uploaded.", "Jeff@SIBFLA.com");


            }
            catch (Exception ex)
            {
                // Error
                oLU.WritetoLog(ex.Message);
                sendErrEmail(ex.Message);

            }
            finally
            {
                oLU.closeLog();
            }

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Citizens SFTP **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendConfEmail(string bodytext, string sConfEmail)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Citizens SFTP";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }



    }
}
