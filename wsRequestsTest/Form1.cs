﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;
using System.IO;
using RestSharp;


namespace wsRequestsTest
{
    public partial class Form1 : Form
    {

        static string cfg_smtpserver;
        static string cfg_SQLConnStr;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {

            //string sUserID = "SIBGotham";
            //string sPW = "SuttonWS0509";

            //string sUserID = "SIBOrchid";
            //string sPW = "SuttonWS0512";

            //string sUserID = "SIBHull";
            //string sPW = "SuttonWS0511";

            //string sUserID = "SIBTAPCO";
            //string sPW = "SuttonWS0516";

            //string sUserID = "SIBCoastal";
            //string sPW = "SuttonWS0513";

            //string sUserID = "SIBJHA";
            //string sPW = "SuttonWS0915";

            //string sUserID = "SIBLONDONUW";
            //string sPW = "SIb05@18$";

            //string sUserID = "SIBTHIG";
            //string sPW = "SF5RsvKX";

            string sUserID = "CCU7449";
            string sPW = "BsKqKf!#";



            string sXMLFileName = "";
            tbResults.Text = "";



            //< NewDataSet >< Data >< CustomerAccount > 7150 </ CustomerAccount >< InspectionCategory > C </ InspectionCategory >< InspectionType > PACKS </ InspectionType >< RequestedBy > Jeff K </ RequestedBy >< EmailConfirmation > jeff@sibfla.com </ EmailConfirmation >    < PolicyNumber > ThisIsATest </ PolicyNumber >< EffectiveDate > 02 / 13 / 2020 </ EffectiveDate >< UnderwriterFirstName > Natalie </ UnderwriterFirstName >< UnderwriterLastName > Schrepel </ UnderwriterLastName >< UnderwriterPhone > (303)111 - 1111 </ UnderwriterPhone >    < UnderwriterContactEmail > natalie.schrepel@hullden.com </ UnderwriterContactEmail >< UnderwriterReportEmail > natalie.schrepel@hullden.com </ UnderwriterReportEmail >< AgencyAgentName > Megan Barone </ AgencyAgentName >< AgencyAgentPhone > (904)565 - 1952 </ AgencyAgentPhone >< AgencyAgentContactName > Megan Barone </ AgencyAgentContactName >< AgencyAgentCode />< AgencyAgentAddress1 > 10151 DEERWOOD PARK BLVD </ AgencyAgentAddress1 >< AgencyAgentAddress2 > BUILDING 100 SUITE 100 </ AgencyAgentAddress2 >    < AgencyAgentCity > Jacksonville </ AgencyAgentCity >< AgencyAgentState > FL </ AgencyAgentState >< AgencyAgentZip > 32256 </ AgencyAgentZip >< InsuranceCompany > Scottsdale Insurance Company</ InsuranceCompany >< RushHandling > N </ RushHandling >< InsuredName > 360 Testing </ InsuredName >< ContactName > Natalie is Testing</ ContactName >< ContactEmail > natalie.schrepel@hullden.com </ ContactEmail >< ContactPhoneHome > 303 - 218 - 4074 </ ContactPhoneHome >< MailAddress1 > 1815 Griffin Road</ MailAddress1 >< MailCity > Dania </ MailCity > < MailState > FL </ MailState >< MailZip > 33004 </ MailZip >< LocationAddress1 > 1815 Griffin Road</ LocationAddress1 >< LocationCity > Dania </ LocationCity >< LocationState > FL </ LocationState >< LocationZip > 33004 </ LocationZip >< LocationContactName > Natalie is Testing</ LocationContactName >< LocationContactPhone > 303 - 218 - 4074 </ LocationContactPhone >< Comments > This is a test.Please cancel</ Comments >< NumberOfBuildings />< DataQuickSqFt />< InspectionTypeCode > LIABS </ InspectionTypeCode >< Valuation > Y </ Valuation >< ProvideITV > Y </ ProvideITV >< AgencyAgentEmail > mbarone@bbjax.com </ AgencyAgentEmail ></ Data ></ NewDataSet >

            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "XML Files (.xml)|*.xml|All Files (*.*)|*.*";

            openFileDialog1.InitialDirectory = @"\\192.168.10.50\xmlfiles";
            //openFileDialog1.InitialDirectory = @"\\192.168.10.60\xmlfiles";
            //openFileDialog1.InitialDirectory = @"c:\weblogs\xmlfiles";
            //openFileDialog1.InitialDirectory = @"c:\temp\xmlfiles";

            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = true;

            // Call the ShowDialog method to show the dialog box.
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                sXMLFileName = openFileDialog1.FileName;
                tbResults.Text += System.Environment.NewLine + "Importing: " + sXMLFileName + System.Environment.NewLine + System.Environment.NewLine;
                this.Refresh();

                wsRequests.wsRequests oWS = new wsRequests.wsRequests();
                //wsRequestsTest.wsRequestsTest oWS = new wsRequestsTest.wsRequestsTest();

                string sXMLString = XMLtoString(sXMLFileName);

                string sRet = oWS.UploadRequest(sUserID, sPW, sXMLString);

                tbResults.Text += sRet;
                oWS = null;
            }
            else
            {
                tbResults.Text = "Nothing processed";

            }

        }

        private string XMLtoString(string xmlFileName)
        {
            string sRetVal = "";

            try
            {
                if (!File.Exists(xmlFileName))
                {
                    throw new ApplicationException("Exception in XMLtoString: XML file missing: " + xmlFileName);

                }

                using (StreamReader sr = new StreamReader(xmlFileName))
                {
                    sRetVal = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("INTERNAL [XMLtoString]: " + ex.Message);
            }

            return sRetVal;

        }

        private void bTestEmail_Click(object sender, EventArgs e)
        {

            // load configuration values from app.config
            System.Collections.Specialized.NameValueCollection colNameVal;
            colNameVal = System.Configuration.ConfigurationManager.AppSettings;
            cfg_smtpserver = colNameVal.Get("smtpserver");
            cfg_SQLConnStr = colNameVal.Get("SQLConnStr");


            EmailConfirmation360.EmailConf360 oEM = new EmailConfirmation360.EmailConf360();

            oEM.cfg_360ConnStr = "server=SQL36015.sibfla.com;database=Sutton360Utils;Connect Timeout=30;uid=legacywebaccess;pwd=Rigbirfou2;";
            oEM.cfg_smtpserver = "utilityweb.sibfla.com";
            oEM.CaseNum = "9845469";
            oEM.CustNum = "7346";
            oEM.InsuredName = "JMS Investments, LLC";
            oEM.RequestedBy = "Judy Wood";
            oEM.Recip = "jeff@sibfla.com";
            oEM.BCC = "";
            oEM.DateSubmitted = "7/12/2018";
            oEM.InsAdd1 = "3795 Highway 13 North ";
            //oEM.InsAdd2 = "Insured address line  2";
            oEM.InsCity = "Haleyville";
            oEM.InsState = "AL";
            oEM.InsZip = "33565";
            oEM.InsuredContactName = "Joe Mac Smith";
            oEM.InsuredContactPhoneHome = "(205) 485-8104";
            oEM.InsuredContactPhoneWork = "";
            oEM.InsuredContactPhoneCell = "";
            oEM.PolicyNum = "CPS2856809";
            oEM.Agent = "McGriff, Siebels & Williams Agency";
            oEM.AgentPhone = "(205) 583-9571";
            oEM.InsuranceCo = "Insurance company";
            oEM.Underwriter = "Underwriter";
            oEM.LocAdd1 = "3795 Highway 13 North ";
            //oEM.LocAdd2 = "Location address line  2";
            oEM.LocCity = "Haleyville";
            oEM.LocState = "AL";
            oEM.LocZip = "33565";
            oEM.LocContact = "Joe Mac Smith";
            oEM.LocContactPhone = "(205) 485 - 8104";
            oEM.InspectionType = "Package - AmWins-AL ";
            oEM.Comments = "";


            string sRet = oEM.sendEmailConf360();
            tbResults.Text = sRet;
        }

        private void btn360API_Click(object sender, EventArgs e)
        {



        }

        private void bReject_Click(object sender, EventArgs e)
        {

            //string sUserID = "SIBGotham";
            //string sPW = "SuttonWS0509";

            //string sUserID = "SIBOrchid";
            //string sPW = "SuttonWS0512";

            string sUserID = "SIBHull";
            string sPW = "SuttonWS0511";

            //string sUserID = "SIBTAPCO";
            //string sPW = "SuttonWS0516";

            //string sUserID = "SIBCoastal";
            //string sPW = "SuttonWS0513";

            //string sUserID = "SIBJHA";
            //string sPW = "SuttonWS0915";

            string sXMLFileName = "";
            tbResults.Text = "";

            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "XML Files (.xml)|*.xml|All Files (*.*)|*.*";

            //openFileDialog1.InitialDirectory = @"\\192.168.10.30\xmlfiles";
            //openFileDialog1.InitialDirectory = @"\\192.168.10.50\xmlfiles";
            openFileDialog1.InitialDirectory = @"c:\weblogs\xmlfiles";

            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = true;

            // Call the ShowDialog method to show the dialog box.
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                sXMLFileName = openFileDialog1.FileName;
                tbResults.Text += System.Environment.NewLine + "Importing: " + sXMLFileName + System.Environment.NewLine + System.Environment.NewLine;
                this.Refresh();

                wsRequests.wsRequests oWS = new wsRequests.wsRequests();
                //wsRequestsTest.wsRequestsTest oWS = new wsRequestsTest.wsRequestsTest();

                string sXMLString = XMLtoString(sXMLFileName);

                string sRet = oWS.RejectCase(sUserID, sPW, sXMLString);

                tbResults.Text += sRet;
                oWS = null;
            }
            else
            {
                tbResults.Text = "Nothing processed";

            }            
        }

        private void btnGetStatus_Click(object sender, EventArgs e)
        {

            string sUserID = "SIBLONDONUW";
            string sPW = "SIb05@18$";
            string sCase = "9971230";

            tbResults.Text = "";


            tbResults.Text += System.Environment.NewLine + "Requesting status for case #: " + sCase + System.Environment.NewLine + System.Environment.NewLine;
            this.Refresh();

            wsRequests.wsRequests oWS = new wsRequests.wsRequests();
            string sRet = oWS.GetCaseStatus(sUserID, sPW, sCase);

            tbResults.Text += sRet;
            oWS = null;
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            string sUserID = "SIBLONDONUW";
            string sPW = "SIb05@18$";
            //string sCaseNum = "9834862";
            string sCaseNum = "9838392";

            string sXMLFileName = @"c:\weblogs\xmlfiles\downloadtest.pdf";
            tbResults.Text = "";

            wsRequests.wsRequests oWS = new wsRequests.wsRequests();

            string base64String = oWS.DownloadReport(sUserID, sPW, sCaseNum);

            if (base64String.Contains("Exception"))
            {
                tbResults.Text += base64String + System.Environment.NewLine;
            }
            else
            {
                byte[] aByteArray = Convert.FromBase64String(base64String);
                File.WriteAllBytes(sXMLFileName, aByteArray);
            }
            tbResults.Text += "Done";
            oWS = null;
        }

        private void bXMLCharTest_Click(object sender, EventArgs e)
        {

            string sXMLFile = @"c:\temp\xmlfiles\SIBLONDONUW27593.xml";
            string sXMLFileEnc = @"c:\temp\xmlfiles\SIBLONDONUW27593ENC.xml";
            string sXMLString = XMLtoString(sXMLFile);
            string sEncodedXML = sXMLString.Replace("&", "&amp;");
            //tbResults.Text = sEncodedXML;

            StreamWriter sr = new StreamWriter(sXMLFileEnc);
            sr.Write(sEncodedXML);
            sr.Flush();
            sr.Close();
            sr = null;


            // Load the reader with the data file and ignore all white space nodes.      
            XmlTextReader reader = null;
            reader = new XmlTextReader(sXMLFileEnc);
            reader.WhitespaceHandling = WhitespaceHandling.None;
            XmlReaderSettings readersettings = new XmlReaderSettings();
            readersettings.CheckCharacters = false;

            // Parse the file and store each of the elements in a structure.
            while (reader.Read())
            {

                if (reader.MoveToContent() == XmlNodeType.Element && reader.Name != "Request")
                {

                    switch (reader.Name)
                    {
                        case "AgencyAgentName":
                            tbResults.Text = reader.ReadString();
                            break;
                        case "InspectionCategory":
                            tbResults.Text = reader.ReadString();
                            break;
                    }   // switch

                }   //if (reader.MoveToContent()

            }   //while

        }

        private void bDataTest_Click(object sender, EventArgs e)
        {

            var client = new RestClient("https://ecommerce3.sibfla.com/api/carrier/v1/CompletedInspectionFormDataSet");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/xml");
            request.AddParameter("application/xml", "<InspectionInfoRequest xmlns=\"http://schemas.datacontract.org/2004/07/LC360API.Carrier_V1\"><Password>Sutton1980$</Password> <UserName>SuttonAPITest</UserName> <InspectionID>20d6a469-27ec-4cd7-af78-33870c82ee10</InspectionID> </InspectionInfoRequest>", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);


            //           string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            //           var data = 
            //           {
            //               "Password":"String content",
            //"UserName":"String content",
            //"InspectionID":"1627aea5-8e0a-4371-9022-9b504344e724"
            //           }

            //DataSet dsFormData = data.FormData.



        }
    }
}
