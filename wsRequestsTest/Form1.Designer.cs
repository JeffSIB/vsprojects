﻿namespace wsRequestsTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGo = new System.Windows.Forms.Button();
            this.tbResults = new System.Windows.Forms.TextBox();
            this.bTestEmail = new System.Windows.Forms.Button();
            this.btn360API = new System.Windows.Forms.Button();
            this.bReject = new System.Windows.Forms.Button();
            this.btnGetStatus = new System.Windows.Forms.Button();
            this.btnDownload = new System.Windows.Forms.Button();
            this.bXMLCharTest = new System.Windows.Forms.Button();
            this.bDataTest = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(12, 12);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(160, 36);
            this.btnGo.TabIndex = 0;
            this.btnGo.Text = "Manual Import";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // tbResults
            // 
            this.tbResults.Location = new System.Drawing.Point(12, 128);
            this.tbResults.Multiline = true;
            this.tbResults.Name = "tbResults";
            this.tbResults.Size = new System.Drawing.Size(979, 186);
            this.tbResults.TabIndex = 1;
            // 
            // bTestEmail
            // 
            this.bTestEmail.Location = new System.Drawing.Point(665, 12);
            this.bTestEmail.Name = "bTestEmail";
            this.bTestEmail.Size = new System.Drawing.Size(160, 36);
            this.bTestEmail.TabIndex = 2;
            this.bTestEmail.Text = "Email Confirmation";
            this.bTestEmail.UseVisualStyleBackColor = true;
            this.bTestEmail.Click += new System.EventHandler(this.bTestEmail_Click);
            // 
            // btn360API
            // 
            this.btn360API.Location = new System.Drawing.Point(831, 12);
            this.btn360API.Name = "btn360API";
            this.btn360API.Size = new System.Drawing.Size(160, 36);
            this.btn360API.TabIndex = 3;
            this.btn360API.Text = "360 API Test";
            this.btn360API.UseVisualStyleBackColor = true;
            this.btn360API.Click += new System.EventHandler(this.btn360API_Click);
            // 
            // bReject
            // 
            this.bReject.Location = new System.Drawing.Point(178, 12);
            this.bReject.Name = "bReject";
            this.bReject.Size = new System.Drawing.Size(160, 36);
            this.bReject.TabIndex = 4;
            this.bReject.Text = "Reject Case";
            this.bReject.UseVisualStyleBackColor = true;
            this.bReject.Click += new System.EventHandler(this.bReject_Click);
            // 
            // btnGetStatus
            // 
            this.btnGetStatus.Location = new System.Drawing.Point(344, 12);
            this.btnGetStatus.Name = "btnGetStatus";
            this.btnGetStatus.Size = new System.Drawing.Size(160, 36);
            this.btnGetStatus.TabIndex = 5;
            this.btnGetStatus.Text = "Get Status";
            this.btnGetStatus.UseVisualStyleBackColor = true;
            this.btnGetStatus.Click += new System.EventHandler(this.btnGetStatus_Click);
            // 
            // btnDownload
            // 
            this.btnDownload.Location = new System.Drawing.Point(510, 12);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(149, 36);
            this.btnDownload.TabIndex = 6;
            this.btnDownload.Text = "Download PDF";
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // bXMLCharTest
            // 
            this.bXMLCharTest.Location = new System.Drawing.Point(12, 54);
            this.bXMLCharTest.Name = "bXMLCharTest";
            this.bXMLCharTest.Size = new System.Drawing.Size(160, 36);
            this.bXMLCharTest.TabIndex = 7;
            this.bXMLCharTest.Text = "XML Test";
            this.bXMLCharTest.UseVisualStyleBackColor = true;
            this.bXMLCharTest.Click += new System.EventHandler(this.bXMLCharTest_Click);
            // 
            // bDataTest
            // 
            this.bDataTest.Location = new System.Drawing.Point(178, 54);
            this.bDataTest.Name = "bDataTest";
            this.bDataTest.Size = new System.Drawing.Size(160, 36);
            this.bDataTest.TabIndex = 8;
            this.bDataTest.Text = "Data Test";
            this.bDataTest.UseVisualStyleBackColor = true;
            this.bDataTest.Click += new System.EventHandler(this.bDataTest_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 420);
            this.Controls.Add(this.bDataTest);
            this.Controls.Add(this.bXMLCharTest);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.btnGetStatus);
            this.Controls.Add(this.bReject);
            this.Controls.Add(this.btn360API);
            this.Controls.Add(this.bTestEmail);
            this.Controls.Add(this.tbResults);
            this.Controls.Add(this.btnGo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.TextBox tbResults;
        private System.Windows.Forms.Button bTestEmail;
        private System.Windows.Forms.Button btn360API;
        private System.Windows.Forms.Button bReject;
        private System.Windows.Forms.Button btnGetStatus;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Button bXMLCharTest;
        private System.Windows.Forms.Button bDataTest;
    }
}

