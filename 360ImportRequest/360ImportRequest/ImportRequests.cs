﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.Reflection;
using LC360API.Carrier_V1;



namespace ImportRequest360
{
    public class ImportRequests
    {


        // Prod
        //private string WSurl = "";

        // UAT
        //private string s360TestURL = "https://suttonuat.losscontrol360.com/API/Carrier/V3/ImportInspections";

        //***** CHANGE IN Helpers Class also

        public string WSurl { get; set; } = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

        public string CustomerUserName { get; set; }
        public string CustomerPassword { get; set; }

        public string CustomerAccount { get; set; }
        public string InspectionType { get; set; }
        public string RequestedBy { get; set; }
        public string EmailConfirmation { get; set; }
        public string PolicyNumber { get; set; }
        public string EffectiveDate { get; set; }
        public string Underwriter { get; set; }
        public string UnderwriterFirstName { get; set; }
        public string UnderwriterLastName { get; set; }
        public string UnderwriterCorrEmail { get; set; }
        public string UnderwriterRptEmail { get; set; }
        public string UnderwriterPhone { get; set; }
        public string AgencyAgentName { get; set; }
        public string AgencyAgentEmail { get; set; }
        public string AgencyAgentPhone { get; set; }
        public string AgentAddress1 { get; set; }
        public string AgentAddress2 { get; set; }
        public string AgentCity { get; set; }
        public string AgentState { get; set; }
        public string AgentZip { get; set; }
        public string AgentFax { get; set; }
        public string AgentCode { get; set; }
        public string AgencyAgentContact { get; set; }
        public string InsuranceCompany { get; set; }
        public string Producer { get; set; }
        public string RushHandling { get; set; }
        public string InsuredName { get; set; }
        public string ContactName { get; set; }
        public string ContactPhoneHome { get; set; }
        public string ContactPhoneWork { get; set; }
        public string ContactPhoneCell { get; set; }
        public string InsuredEmail { get; set; }
        public string MailAddress1 { get; set; }
        public string MailAddress2 { get; set; }
        public string MailCity { get; set; }
        public string MailState { get; set; }
        public string MailZip { get; set; }
        public string BusinessOperations { get; set; }
        public string LocationAddress1 { get; set; }
        public string LocationAddress2 { get; set; }
        public string LocationCity { get; set; }
        public string LocationState { get; set; }
        public string LocationZip { get; set; }
        public string LocationContactName { get; set; }
        public string LocationContactPhone { get; set; }
        public string Comments { get; set; }
        public string BuildingCost { get; set; }
        public string BusinessTotalRevenue { get; set; }
        public string ContentsCost { get; set; }
        public string CoverageA { get; set; }
        public string CoverageB { get; set; }
        public string SquareFootage { get; set; }

        public string ISOClass { get; set; }
        public string YearBuilt { get; set; }
        public string Occupancy { get; set; }
        public string GenericField1Name { get; set; }
        public string GenericField1Value { get; set; }
        public string GenericField2Name { get; set; }
        public string GenericField2Value { get; set; }
        public string GenericField3Name { get; set; }
        public string GenericField3Value { get; set; }
        public string AdditionalFeeAddOns { get; set; }


        // AmRisc
        public string InspectionURI { get; set; }
        public string LocationName { get; set; }
        public string LocationNumber { get; set; }
        public string BrokerName { get; set; }
        public string BrokerPhone { get; set; }
        public string UWAssistantName { get; set; }
        public string UWAssistantEmail { get; set; }
        public string UWAssistantPhone { get; set; }

        // Orchid generic fields
        public string Usage { get; set; }
        public string DwellingType { get; set; }
        public string Stories { get; set; }
        public string YearBuiltOrchid { get; set; }
        public string ExteriorWallType { get; set; }
        public string ExteriorCoating { get; set; }
        public string RoofGeometry { get; set; }
        public string RoofType { get; set; }
        public string RoofYear { get; set; }
        public string RoofCondition { get; set; }
        public string RoofMaterial { get; set; }
        public string PoolPresent { get; set; }
        public string PoolAboveGround { get; set; }
        public string PoolScreenEnclosurePresent { get; set; }
        public string PoolFencePresent { get; set; }
        public string PoolDivingBoardPresent { get; set; }
        public string PoolSlidePresent { get; set; }
        public string WindstormProtectiveDevices { get; set; }
        public string ProtectionClass { get; set; }
        public string CentralStationFireAlarmPresent { get; set; }
        public string CentralStationBurglarAlarmPresent { get; set; }
        public string CentralAlarmPresent { get; set; }
        public string BurglarlAlarmPresent { get; set; }
        public string FirelAlarmPresent { get; set; }
        public string Dog { get; set; }
        public string OtherPets { get; set; }

        public string Farming { get; set; }
        public string DistanceCoast { get; set; }

        public string ElectricalUpdates { get; set; }
        public string ElectricalFullPartial { get; set; }
        public string ElectricalYear { get; set; }
        public string HvacUpdates { get; set; }
        public string HvacFullPartial { get; set; }
        public string HvacYear { get; set; }
        public string PlumbingUpdates { get; set; }
        public string PlumbingFullPartial { get; set; }
        public string PlumbingYear { get; set; }

        public string AltLocationContact { get; set; }
        public string AgentEmail { get; set; }

        // ImageRight
        public string ImageRightDrawer { get; set; }
        public string ImageRightFileNum { get; set; }

        // Citizens
        public string ActivityID { get; set; }


        // Orchid COnnect
        public string SidingWood { get; set; }
        public string SidingDryVit { get; set; }
        public string SidingEIFS { get; set; }
        public string SidingVinyl { get; set; }
        public string SidingMasonite { get; set; }
        public string SidingOther { get; set; }
        public string FenceToCode { get; set; }
        public string Unfenced { get; set; }
        public string Secured { get; set; }
        public string WiringYear { get; set; }
        public string HeatingYear { get; set; }
        public string Updates { get; set; }
        public string Construction { get; set; }

        // AMWins
        public string AutomaticWaterShutOff { get; set; }
        public string BrushWildfireZone { get; set; }
        public string HurricaneRoofProtectionToCode { get; set; }
        public string HurricaneProtection { get; set; }
        public string Farm { get; set; }
        public string MobileHomes { get; set; }
        public string DivingBoardNotToCode { get; set; }
        public string ExistingDamage { get; set; }
        public string DwellingsNotInsured100PctOfRCV { get; set; }
        public string KnobTubeWiring { get; set; }
        public string NationalRegistry { get; set; }
        public string EIFSOlderThan1998 { get; set; }
        public string WoodStovePrimaryHeat { get; set; }
        public string DaycareOrAssistedLiving { get; set; }
        public string Over10Acres { get; set; }
        public string RisksInForeclosureProceedings { get; set; }
        public string DeveloperSpecHomes { get; set; }
        public string FloodZone { get; set; }
        public string EffectiveYearBuilt { get; set; }
        public string RatingArea { get; set; }
        public string RatingUnits { get; set; }
        public string RatingReceipts { get; set; }
        public string RatingPayroll { get; set; }
        public string RatingSubCost { get; set; }
        public string RatingOther { get; set; }
        public string DeliverToEmail { get; set; }



        /// <summary>
        /// GENERIC IMPORT
        /// </summary>

        public string Import()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;
            if (EffectiveDate.Length > 0)
            {

                try
                {
                    dtEffectiveDate = Convert.ToDateTime(EffectiveDate);
                }
                catch
                {
                }
            }

            // Notes
            if (LocationContactName != null)
            {
                if (LocationContactName.Length > 0 || LocationContactPhone.Length > 0)
                {
                    if (!(LocationContactName == ContactName))
                    {
                        if (Comments.Length > 0)
                        {
                            Comments = Comments + System.Environment.NewLine + System.Environment.NewLine;
                        }
                        Comments = Comments + "Location Contact: " + LocationContactName + " " + LocationContactPhone;
                    }
                }
            }
            if (GenericField1Name != null)
            {
                if (GenericField1Name.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + GenericField1Name + " " + GenericField1Value;
                }
            }
            if (RequestedBy != null)
            {
                if (RequestedBy.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + "Requested by " + RequestedBy;
                }
            }


            // Rush
            bool bRush = false;
            if (RushHandling.ToUpper() == "Y")
                bRush = true;

            // Agent name
            string sAgencyName = "";
            if (AgencyAgentName != null)
            {
                if (AgencyAgentName.Length > 0)
                    sAgencyName = AgencyAgentName;
                if (Producer != null)
                {
                    if (Producer.Length > 0)
                        sAgencyName += " - " + Producer;
                }

                if (sAgencyName.Length > 75)
                {
                    sAgencyName = sAgencyName.Substring(0, 75);
                }
            }

            // Agency contact required
            if (AgencyAgentContact.Length == 0)
                AgencyAgentContact = "NA";

            // Underwriter
            bool bUnderWriter = true;
            if (UnderwriterFirstName.Length == 0 || UnderwriterLastName.Length == 0 || UnderwriterCorrEmail.Length == 0)
                bUnderWriter = false;

            // Attributes
            decimal decCoverageA = 0;
            if (!Decimal.TryParse(CoverageA, out decCoverageA))
                decCoverageA = 0;

            int iYearBuilt = 0;
            if (!Int32.TryParse(YearBuilt, out iYearBuilt))
                iYearBuilt = 0;

            decimal decBuildingCost = 0;
            if (!Decimal.TryParse(BuildingCost, out decBuildingCost))
                decBuildingCost = 0;

            decimal decContentsCost = 0;
            if (!Decimal.TryParse(ContentsCost, out decContentsCost))
                decContentsCost = 0;

            // ImportInspectionsRequest is in LC360API
            string result = "";

            if (bUnderWriter)
            {
                var data = new LC360API.Carrier_V1.ImportInspectionsRequest
                {
                    UserName = CustomerUserName,
                    Password = CustomerPassword,
                    Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = sAgencyName,
                                    AgentCode = AgentCode,
                                    ContactName = AgencyAgentContact,
                                    ContactEmail = AgencyAgentEmail,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip                                        
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy,
                                BuildingCost = decBuildingCost,
                                IsoClass = ISOClass,
                                ContentsCost = decContentsCost
                            },
                            Underwriter = new Underwriter
                            {
                                FirstName = UnderwriterFirstName,
                                LastName = UnderwriterLastName,
                                CorrespondanceEmail = UnderwriterCorrEmail,
                                ReportEmail = UnderwriterRptEmail,
                                Phone = UnderwriterPhone
                            },
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", InsuranceCompany}
                            },
                            IgnoreDuplicates = true
                        }
                    }

                };
                result = data.MakeJSONHTTPPost(WSurl);

            }
            else
            {

                var data = new LC360API.Carrier_V1.ImportInspectionsRequest
                {
                    UserName = CustomerUserName,
                    Password = CustomerPassword,
                    Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = sAgencyName,
                                    AgentCode = AgentCode,
                                    ContactName = AgencyAgentContact,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy,
                                BuildingCost = decBuildingCost,
                                IsoClass = ISOClass,
                                ContentsCost = decContentsCost
                            },
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", InsuranceCompany}
                            },
                            IgnoreDuplicates = true
                        }
                    }

                };
                result = data.MakeJSONHTTPPost(WSurl);
            }

            return result;
        }

        public string ImportCitizens()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;
            if (EffectiveDate.Length > 0)
            {

                try
                {
                    dtEffectiveDate = Convert.ToDateTime(EffectiveDate);
                }
                catch
                {
                }
            }

            // Rush
            bool bRush = false;

            // Agent name
            string sAgencyName = "";
            if (AgencyAgentName != null)
            {
                if (AgencyAgentName.Length > 0)
                    sAgencyName = AgencyAgentName;
            }

            // Agency contact 
            if (AgencyAgentContact.Length == 0)
                AgencyAgentContact = "";

            // Attributes
            decimal decCoverageA = 0;

            int iYearBuilt = 0;

            decimal decBuildingCost = 0;

            decimal decContentsCost = 0;

            if (InsuredEmail == null)
                InsuredEmail = "";

            // ImportInspectionsRequest is in LC360API
            string result = "";

            var data = new LC360API.Carrier_V1.ImportInspectionsRequest
            {
                UserName = CustomerUserName,
                Password = CustomerPassword,
                Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = sAgencyName,
                                    AgentCode = AgentCode,
                                    ContactName = AgencyAgentContact,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy,
                                BuildingCost = decBuildingCost,
                                IsoClass = ISOClass,
                                ContentsCost = decContentsCost
                            },
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Inspection contact email address", InsuredEmail},
                                {"Agent email", AgentEmail},
                                {"Activity ID", ActivityID}
                            },
                            IgnoreDuplicates = true
                        }
                    }

            };
            result = data.MakeJSONHTTPPost(WSurl);

            return result;
        }


        public string ImportRTSpec()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;
            if (EffectiveDate.Length > 0)
            {

                try
                {
                    dtEffectiveDate = Convert.ToDateTime(EffectiveDate);
                }
                catch
                {
                }
            }

            // Rush
            bool bRush = false;
            if (RushHandling.ToUpper() == "Y")
                bRush = true;

            // Agent name
            string sAgencyName = "";
            if (AgencyAgentName != null)
            {
                if (AgencyAgentName.Length > 0)
                    sAgencyName = AgencyAgentName;
            }

            // Agency contact 
            if (AgencyAgentContact.Length == 0)
                AgencyAgentContact = "";

            // Agency phone
            if (AgencyAgentPhone.Length == 0)
                AgencyAgentPhone = "";

            // Agency email
            if (AgencyAgentEmail.Length == 0)
                AgencyAgentEmail = "";


            // Attributes
            decimal decCoverageA = 0;
            if (!Decimal.TryParse(CoverageA, out decCoverageA))
                decCoverageA = 0;

            int iYearBuilt = 0;
            if (!Int32.TryParse(YearBuilt, out iYearBuilt))
                iYearBuilt = 0;

            decimal decBuildingCost = 0;
            if (!Decimal.TryParse(BuildingCost, out decBuildingCost))
                decBuildingCost = 0;

            decimal decContentsCost = 0;
            if (!Decimal.TryParse(ContentsCost, out decContentsCost))
                decContentsCost = 0;

            if (InsuredEmail == null)
                InsuredEmail = "";

            // ImportInspectionsRequest is in LC360API
            string result = "";

            var data = new LC360API.Carrier_V1.ImportInspectionsRequest
            {
                UserName = CustomerUserName,
                Password = CustomerPassword,
                Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = sAgencyName,
                                    AgentCode = AgentCode,
                                    ContactName = AgencyAgentContact,
                                    ContactEmail = AgencyAgentEmail,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy,
                                BuildingCost = decBuildingCost,
                                IsoClass = ISOClass,
                                ContentsCost = decContentsCost
                            },
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                              
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", ""},
                                {"Additional fee add ons", AdditionalFeeAddOns},
                                {"Annual Receipts ($)",RatingReceipts},
                                {"Area (square feet)",RatingArea},
                                {"Cost of Subcontractors ($)",RatingSubCost},
                                {"Payroll ($)",RatingPayroll},
                                {"Units",RatingUnits},
                                {"Occupancy",Occupancy },
                                {"DeliverToEmail",DeliverToEmail }

                            },
                            IgnoreDuplicates = true
                        }
                    }

            };
            result = data.MakeJSONHTTPPost(WSurl);

            return result;
        }


        public string ImportCapacity()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;
            if (EffectiveDate.Length > 0)
            {

                try
                {
                    dtEffectiveDate = Convert.ToDateTime(EffectiveDate);
                }
                catch
                {
                }
            }

            // Rush
            bool bRush = false;

            // Agent name
            string sAgencyName = "";
            if (AgencyAgentName != null)
            {
                if (AgencyAgentName.Length > 0)
                    sAgencyName = AgencyAgentName;
            }

            // Agency contact 
            if (AgencyAgentContact.Length == 0)
                AgencyAgentContact = "";

            // Agency phone
            if (AgencyAgentPhone.Length == 0)
                AgencyAgentPhone = "";

            // Agency email
            if (AgencyAgentEmail.Length == 0)
                AgencyAgentEmail = "";


            // Attributes
            decimal decCoverageA = 0;
            if (!Decimal.TryParse(CoverageA, out decCoverageA))
                decCoverageA = 0;

            int iYearBuilt = 0;
            if (!Int32.TryParse(YearBuilt, out iYearBuilt))
                iYearBuilt = 0;

            decimal decBuildingCost = 0;
            if (!Decimal.TryParse(BuildingCost, out decBuildingCost))
                decBuildingCost = 0;

            decimal decContentsCost = 0;
            if (!Decimal.TryParse(ContentsCost, out decContentsCost))
                decContentsCost = 0;

            if (InsuredEmail == null)
                InsuredEmail = "";

            // ImportInspectionsRequest is in LC360API
            string result = "";

            var data = new LC360API.Carrier_V1.ImportInspectionsRequest
            {
                UserName = CustomerUserName,
                Password = CustomerPassword,
                Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = sAgencyName,
                                    AgentCode = AgentCode,
                                    ContactName = AgencyAgentContact,
                                    ContactEmail = AgencyAgentEmail,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy,
                                BuildingCost = decBuildingCost,
                                IsoClass = ISOClass,
                                ContentsCost = decContentsCost
                            },
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", InsuranceCompany},
                                {"Order Reference Number", ImageRightFileNum},
                                {"Additional fee add ons", AdditionalFeeAddOns}
                            },
                            IgnoreDuplicates = true
                        }
                    }

            };
            result = data.MakeJSONHTTPPost(WSurl);

            return result;
        }

        /// <summary>
        /// SIU
        /// </summary>

        public string ImportSIU()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;

            // Rush
            bool bRush = false;
            if (RushHandling.ToUpper() == "Y")
                bRush = true;

            // Agent name
            string sAgencyName = "";
            if (AgencyAgentName != null)
            {
                if (AgencyAgentName.Length > 0)
                    sAgencyName = AgencyAgentName;
                if (Producer != null)
                {
                    if (Producer.Length > 0)
                        sAgencyName += " - " + Producer;
                }

                if (sAgencyName.Length > 75)
                {
                    sAgencyName = sAgencyName.Substring(0, 75);
                }

            }

            // Attributes
            decimal decCoverageA = 0;
            if (!Decimal.TryParse(CoverageA, out decCoverageA))
                decCoverageA = 0;

            int iYearBuilt = 0;
            if (!Int32.TryParse(YearBuilt, out iYearBuilt))
                iYearBuilt = 0;

            // ImportInspectionsRequest is in LC360API
            var data = new LC360API.Carrier_V1.ImportInspectionsRequest
            {
                UserName = CustomerUserName,
                Password = CustomerPassword,
                Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = sAgencyName,
                                    AgentCode = AgentCode,
                                    ContactName = AgencyAgentContact,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy
                            },
                            //Underwriter = new Underwriter
                            //{
                            //    FirstName = UnderwriterFirstName,
                            //    LastName = UnderwriterLastName,
                            //    CorrespondanceEmail = UnderwriterCorrEmail,
                            //    ReportEmail = UnderwriterRptEmail,
                            //    Phone = UnderwriterPhone
                            //},
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", InsuranceCompany}
                            },
                            IgnoreDuplicates = true
                        }
                    }

            };

            string result = data.MakeJSONHTTPPost(WSurl);


            return result;
        }   // SIU


        /// <summary>
        /// JHA
        /// </summary>

        public string ImportJHA()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;
            if (EffectiveDate.Length > 0)
            {

                try
                {
                    dtEffectiveDate = Convert.ToDateTime(EffectiveDate);
                }
                catch
                {
                }
            }

            // Notes
            if (LocationContactName != null)
            {
                if (LocationContactName.Length > 0 || LocationContactPhone.Length > 0)
                {
                    if (!(LocationContactName == ContactName))
                    {
                        if (Comments.Length > 0)
                        {
                            Comments = Comments + System.Environment.NewLine + System.Environment.NewLine;
                        }
                        Comments = Comments + "Location Contact: " + LocationContactName + " " + LocationContactPhone;
                    }
                }
            }
            if (GenericField1Name != null)
            {
                if (GenericField1Name.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + GenericField1Name + " " + GenericField1Value;
                }
            }
            if (RequestedBy != null)
            {
                if (RequestedBy.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + "Requested by " + RequestedBy;
                }
            }
            if (BusinessOperations != null)
            {
                if (BusinessOperations.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + "Business Operations " + BusinessOperations;
                }
            }


            // Rush
            bool bRush = false;
            if (RushHandling.ToUpper() == "Y")
                bRush = true;

            // Agent name
            string sAgencyName = "";
            if (AgencyAgentName != null)
            {
                if (AgencyAgentName.Length > 0)
                    sAgencyName = AgencyAgentName;
                if (Producer != null)
                {
                    if (Producer.Length > 0)
                        sAgencyName += " - " + Producer;
                }

                if (sAgencyName.Length > 75)
                {
                    sAgencyName = sAgencyName.Substring(0, 75);
                }

            }

            bool bUnderWriter = true;
            if (UnderwriterFirstName.Length == 0 || UnderwriterLastName.Length == 0 || UnderwriterCorrEmail.Length == 0)
                bUnderWriter = false;

            // Attributes
            decimal decCoverageA = 0;
            if (!Decimal.TryParse(CoverageA, out decCoverageA))
                decCoverageA = 0;

            int iYearBuilt = 0;
            if (!Int32.TryParse(YearBuilt, out iYearBuilt))
                iYearBuilt = 0;

            // ImportInspectionsRequest is in LC360API
            string result = "";
            if (bUnderWriter)
            {
                var data = new LC360API.Carrier_V1.ImportInspectionsRequest
                {
                    UserName = CustomerUserName,
                    Password = CustomerPassword,
                    Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = sAgencyName,
                                    AgentCode = AgentCode,
                                    ContactName = AgencyAgentContact,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy
                            },
                            Underwriter = new Underwriter
                            {
                                FirstName = UnderwriterFirstName,
                                LastName = UnderwriterLastName,
                                CorrespondanceEmail = UnderwriterCorrEmail,
                                ReportEmail = UnderwriterRptEmail,
                                Phone = UnderwriterPhone
                            },
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", InsuranceCompany},
                                {"ImageRight File Number", ImageRightFileNum}
                            },
                            IgnoreDuplicates = true
                        }
                    }
                };
                result = data.MakeJSONHTTPPost(WSurl);
            }
            else
            {
                var data = new LC360API.Carrier_V1.ImportInspectionsRequest
                {
                    UserName = CustomerUserName,
                    Password = CustomerPassword,
                    Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = sAgencyName,
                                    AgentCode = AgentCode,
                                    ContactName = AgencyAgentContact,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy
                            },
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", InsuranceCompany},
                                {"ImageRight File Number", ImageRightFileNum}
                            },
                            IgnoreDuplicates = true
                        }
                    }
                };
                result = data.MakeJSONHTTPPost(WSurl);
            }
            return result;
        }   // JHA

        /// <summary>
        /// AMRISC
        /// </summary>

        public string ImportAmRisc()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;

            // Notes
            //if (LocationContactName != null)
            //{
            //    if (LocationContactName.Length > 0 || LocationContactPhone.Length > 0)
            //    {
            //        if (!(LocationContactName == ContactName))
            //        {
            //            if (Comments.Length > 0)
            //            {
            //                Comments = Comments + System.Environment.NewLine + System.Environment.NewLine;
            //            }
            //            Comments = Comments + "Location Contact: " + LocationContactName + " " + LocationContactPhone;
            //        }
            //    }
            //}
            if (GenericField1Name != null)
            {
                if (GenericField1Name.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + GenericField1Name + " " + GenericField1Value;
                }
            }
            if (RequestedBy != null)
            {
                if (RequestedBy.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + "Requested by " + RequestedBy;
                }
            }

            UnderwriterFirstName = "";
            UnderwriterLastName = "";
            if (Underwriter.Length > 0)
            {
                if (Underwriter.Contains(" "))
                {
                    int iSpace = Underwriter.IndexOf(" ");

                    UnderwriterFirstName = Underwriter.Substring(0, iSpace);
                    UnderwriterLastName = Underwriter.Substring(iSpace, Underwriter.Length - iSpace);
                }
            }



            // Rush
            bool bRush = false;
            if (RushHandling.ToUpper() == "Y")
                bRush = true;

            // Agent name
            string sAgencyName = "";
            if (AgencyAgentName != null)
            {
                if (AgencyAgentName.Length > 0)
                    sAgencyName = AgencyAgentName;
                if (Producer != null)
                {
                    if (Producer.Length > 0)
                        sAgencyName += " - " + Producer;
                }

                if (sAgencyName.Length > 75)
                {
                    sAgencyName = sAgencyName.Substring(0, 75);
                }
            }

            // Attributes
            decimal decCoverageA = 0;
            if (!Decimal.TryParse(CoverageA, out decCoverageA))
                decCoverageA = 0;

            int iYearBuilt = 0;
            if (!Int32.TryParse(YearBuilt, out iYearBuilt))
                iYearBuilt = 0;

            // ImportInspectionsRequest is in LC360API
            var data = new LC360API.Carrier_V1.ImportInspectionsRequest
            {
                UserName = CustomerUserName,
                Password = CustomerPassword,
                Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = AgencyAgentName,
                                    AgentCode = AgentCode,
                                    ContactName = Producer,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy
                            },
                            Underwriter = new Underwriter
                            {
                                FirstName = UnderwriterFirstName,
                                LastName = UnderwriterLastName,
                                CorrespondanceEmail = UnderwriterCorrEmail,
                                ReportEmail = UnderwriterRptEmail,
                                Phone = UnderwriterPhone
                            },
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = LocationContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"InspectionURI", InspectionURI},
                                {"LocationName", LocationName},
                                {"LocationNumber",LocationNumber},
                                {"BrokerName",BrokerName},
                                {"BrokerPhone",BrokerPhone},
                                {"UWAssistantName",UWAssistantName},
                                {"UWAssistantEmail",UWAssistantEmail},
                                {"UWAssistantPhone",UWAssistantPhone}
                            },
                            IgnoreDuplicates = true
                        }
                    }

            };

            string result = data.MakeJSONHTTPPost(WSurl);

            return result;
        }

        /// <summary>
        /// BASS
        /// </summary>

        public string ImportBass()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;
            if (GenericField1Name != null)
            {
                if (GenericField1Name.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + GenericField1Name + " " + GenericField1Value;
                }
            }
            if (RequestedBy != null)
            {
                if (RequestedBy.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + "Requested by " + RequestedBy;
                }
            }

            UnderwriterFirstName = "";
            UnderwriterLastName = "";
            if (Underwriter.Length > 0)
            {
                if (Underwriter.Contains(" "))
                {
                    int iSpace = Underwriter.IndexOf(" ");

                    UnderwriterFirstName = Underwriter.Substring(0, iSpace);
                    UnderwriterLastName = Underwriter.Substring(iSpace, Underwriter.Length - iSpace);
                }
                else
                {
                    UnderwriterFirstName = Underwriter;
                    UnderwriterLastName = Underwriter;
                }
            }


            // Rush
            bool bRush = false;
            if (RushHandling.ToUpper() == "Y")
                bRush = true;

            // Agent name
            string sAgencyName = "";
            if (AgencyAgentName != null)
            {
                if (AgencyAgentName.Length > 0)
                    sAgencyName = AgencyAgentName;
                if (Producer != null)
                {
                    if (Producer.Length > 0)
                        sAgencyName += " - " + Producer;
                }

                if (sAgencyName.Length > 75)
                {
                    sAgencyName = sAgencyName.Substring(0, 75);
                }

            }

            // Attributes
            decimal decCoverageA = 0;
            if (!Decimal.TryParse(CoverageA, out decCoverageA))
                decCoverageA = 0;

            int iYearBuilt = 0;
            if (!Int32.TryParse(YearBuilt, out iYearBuilt))
                iYearBuilt = 0;

            // ImportInspectionsRequest is in LC360API
            var data = new LC360API.Carrier_V1.ImportInspectionsRequest
            {
                UserName = CustomerUserName,
                Password = CustomerPassword,
                Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = AgencyAgentName,
                                    AgentCode = AgentCode,
                                    ContactName = Producer,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy
                            },
                            Underwriter = new Underwriter
                            {
                                FirstName = UnderwriterFirstName,
                                LastName = UnderwriterLastName,
                                CorrespondanceEmail = UnderwriterCorrEmail,
                                ReportEmail = UnderwriterRptEmail,
                                Phone = UnderwriterPhone
                            },
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = LocationContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", InsuranceCompany},
                                {"ImageRight Drawer", ImageRightDrawer}
                            },
                            IgnoreDuplicates = true
                        }
                    }

            };

            string result = data.MakeJSONHTTPPost(WSurl);


            return result;
        }

        /// <summary>
        /// Generic commercial
        /// </summary>

        public string ImportBankers()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;
            string result = "";

            // Underwriter
            bool bUnderWriter = true;
            if (Underwriter.Length == 0)
                bUnderWriter = false;

            if (bUnderWriter)
            {
                UnderwriterFirstName = "";
                UnderwriterLastName = "";
                if (Underwriter.Contains(" "))
                {
                    int iSpace = Underwriter.IndexOf(" ");

                    UnderwriterFirstName = Underwriter.Substring(0, iSpace);
                    UnderwriterLastName = Underwriter.Substring(iSpace, Underwriter.Length - iSpace);
                }
            }

            // Rush
            bool bRush = false;
            if (RushHandling.ToUpper() == "Y")
                bRush = true;

            // Agent name
            string sAgencyName = "";
            if (AgencyAgentName != null)
            {
                if (AgencyAgentName.Length > 0)
                    sAgencyName = AgencyAgentName;
            }

            // Agency contact required
            if (AgencyAgentContact.Length == 0)
                AgencyAgentContact = "NA";


            // Attributes
            decimal decCoverageA = 0;
            if (!Decimal.TryParse(CoverageA, out decCoverageA))
                decCoverageA = 0;

            decimal decBuildingCost = 0;
            if (!Decimal.TryParse(BuildingCost, out decBuildingCost))
                decBuildingCost = 0;

            decimal decContentsCost = 0;
            if (!Decimal.TryParse(ContentsCost, out decContentsCost))
                decContentsCost = 0;

            int iYearBuilt = 0;
            if (!Int32.TryParse(YearBuilt, out iYearBuilt))
                iYearBuilt = 0;

            // ImportInspectionsRequest is in LC360API
            if (bUnderWriter)
            {
                var data = new LC360API.Carrier_V1.ImportInspectionsRequest
                {
                    UserName = CustomerUserName,
                    Password = CustomerPassword,
                    Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = AgencyAgentName,
                                    AgentCode = AgentCode,
                                    ContactName = Producer,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy,
                                BuildingCost = decBuildingCost,
                                ContentsCost = decContentsCost
                            },
                            Underwriter = new Underwriter
                            {
                                FirstName = UnderwriterFirstName,
                                LastName = UnderwriterLastName,
                                CorrespondanceEmail = UnderwriterCorrEmail,
                                ReportEmail = UnderwriterRptEmail,
                                Phone = UnderwriterPhone
                            },
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", "Bankers Insurance Group"},
                                {"BusinessOperations", BusinessOperations },
                                {"Building Cost", BuildingCost },
                                {"Occupancy", Occupancy },
                                {"Year Built", YearBuilt }
                            },
                            IgnoreDuplicates = true
                        }
                    }

                };
                result = data.MakeJSONHTTPPost(WSurl);
            }
            else
            {
                // no underwriter
                var data = new LC360API.Carrier_V1.ImportInspectionsRequest
                {
                    UserName = CustomerUserName,
                    Password = CustomerPassword,
                    Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = AgencyAgentName,
                                    AgentCode = AgentCode,
                                    ContactName = Producer,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy,
                                BuildingCost = decBuildingCost,
                                ContentsCost = decContentsCost
                            },
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", "Bankers Insurance Group"},
                                {"BusinessOperations", BusinessOperations },
                                {"Building Cost", BuildingCost },
                                {"Occupancy", Occupancy },
                                {"Year Built", YearBuilt }
                            },
                            IgnoreDuplicates = true
                        }
                    }

                };
                result = data.MakeJSONHTTPPost(WSurl);
            }

            return result;
        }

        public string ImportAMWins()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;
            if (EffectiveDate.Length > 0)
            {

                try
                {
                    dtEffectiveDate = Convert.ToDateTime(EffectiveDate);
                }
                catch
                {
                }
            }

            // Notes
            if (LocationContactName != null)
            {
                if (LocationContactName.Length > 0 || LocationContactPhone.Length > 0)
                {
                    if (!(LocationContactName == ContactName))
                    {
                        if (Comments.Length > 0)
                        {
                            Comments = Comments + System.Environment.NewLine + System.Environment.NewLine;
                        }
                        Comments = Comments + "Location Contact: " + LocationContactName + " " + LocationContactPhone;
                    }
                }
            }
           

            // Rush
            bool bRush = false;
     
            // Agent name
            string sAgencyName = "";
            if (AgencyAgentName != null)
            {
                if (AgencyAgentName.Length > 0)
                    sAgencyName = AgencyAgentName;
            }

            // Agency contact required
            if (AgencyAgentContact.Length == 0)
                AgencyAgentContact = "NA";


            // Attributes
            decimal decCoverageA = 0;
            if (!Decimal.TryParse(CoverageA, out decCoverageA))
                decCoverageA = 0;

            int iYearBuilt = 0;
            if (!Int32.TryParse(YearBuilt, out iYearBuilt))
                iYearBuilt = 0;

            // Underwriter
            bool bUnderWriter = true;
            if (UnderwriterFirstName.Length == 0 || UnderwriterLastName.Length == 0 || UnderwriterCorrEmail.Length == 0)
                bUnderWriter = false;

            string result = "";

            if (bUnderWriter)
            {

                // ImportInspectionsRequest is in LC360API
                var data = new LC360API.Carrier_V1.ImportInspectionsRequest
                {
                    UserName = CustomerUserName,
                    Password = CustomerPassword,
                    Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                             Agent = new Agent
                                {
                                    AgencyName = AgencyAgentName,
                                    AgentCode = AgentCode,
                                    ContactName = AgencyAgentContact,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                //CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy
                            },
                            Underwriter = new Underwriter
                            {
                                FirstName = UnderwriterFirstName,
                                LastName = UnderwriterLastName,
                                CorrespondanceEmail = UnderwriterCorrEmail,
                                ReportEmail = UnderwriterRptEmail,
                                Phone = UnderwriterPhone
                            },
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = LocationContactPhone,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = LocationContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", InsuranceCompany},
                                {"Occupancy",Occupancy},
                                {"NumberOfStories",Stories},
                                {"YearBuilt",YearBuilt},
                                {"Construction",Construction},
                                {"ProtectionClass",ProtectionClass},
                                {"RoofType",RoofType},
                                {"RoofShape",RoofGeometry},
                                {"RoofAge",RoofYear},
                                {"RoofMaterial",RoofMaterial},
                                {"SquareFeet",SquareFootage},
                                {"AutomaticWaterShutOff",AutomaticWaterShutOff},
                                {"BrushWildfireZone",BrushWildfireZone},
                                {"BurglarAlarm",BurglarlAlarmPresent},
                                {"CentralStationFireAlarm",CentralStationFireAlarmPresent},
                                {"HurricaneRoofProtectionToCode",HurricaneRoofProtectionToCode},
                                {"HurricaneProtection",HurricaneProtection},
                                {"MobileHomes",MobileHomes},
                                {"DivingBoardNotToCode",DivingBoardNotToCode},
                                {"ExistingDamage",ExistingDamage},
                                {"DwellingsNotInsured100PctOfRCV",DwellingsNotInsured100PctOfRCV},
                                {"KnobTubeWiring",KnobTubeWiring},
                                {"NationalRegistry",NationalRegistry},
                                {"EIFSOlderThan1998",EIFSOlderThan1998},
                                {"WoodStovePrimaryHeat",WoodStovePrimaryHeat},
                                {"DaycareOrAssistedLiving",DaycareOrAssistedLiving},
                                {"Over10Acres",Over10Acres},
                                {"RisksInForeclosureProceedings",RisksInForeclosureProceedings},
                                {"DeveloperSpecHomes",DeveloperSpecHomes},
                                {"Farm",Farm},
                                {"CoverageA",CoverageA},
                                {"CoverageB",CoverageB},
                                {"Zone",FloodZone},
                                {"EffectiveYearBuilt",EffectiveYearBuilt },
                                {"MilesFromCoast",DistanceCoast }
                            },
                            IgnoreDuplicates = true
                        }
                    }
                };
                result = data.MakeJSONHTTPPost(WSurl);
            }
            else
            {

                // ImportInspectionsRequest is in LC360API
                var data = new LC360API.Carrier_V1.ImportInspectionsRequest
                {
                    UserName = CustomerUserName,
                    Password = CustomerPassword,
                    Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Attributes = new Attributes
                            {
                                //CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy
                            },
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = LocationContactPhone,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = LocationContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                //{"Insurance Company", InsuranceCompany},
                                {"Occupancy",Occupancy},
                                {"NumberOfStories",Stories},
                                {"YearBuilt",YearBuilt},
                                {"Construction",Construction},
                                {"ProtectionClass",ProtectionClass},
                                {"RoofType",RoofType},
                                {"RoofShape",RoofGeometry},
                                {"RoofAge",RoofYear},
                                {"RoofMaterial",RoofMaterial},
                                {"SquareFeet",SquareFootage},
                                {"AutomaticWaterShutOff",AutomaticWaterShutOff},
                                {"BrushWildfireZone",BrushWildfireZone},
                                {"BurglarAlarm",BurglarlAlarmPresent},
                                {"CentralStationFireAlarm",CentralStationFireAlarmPresent},
                                {"HurricaneRoofProtectionToCode",HurricaneRoofProtectionToCode},
                                {"HurricaneProtection",HurricaneProtection},
                                {"MobileHomes",MobileHomes},
                                {"DivingBoardNotToCode",DivingBoardNotToCode},
                                {"ExistingDamage",ExistingDamage},
                                {"DwellingsNotInsured100PctOfRCV",DwellingsNotInsured100PctOfRCV},
                                {"KnobTubeWiring",KnobTubeWiring},
                                {"NationalRegistry",NationalRegistry},
                                {"EIFSOlderThan1998",EIFSOlderThan1998},
                                {"WoodStovePrimaryHeat",WoodStovePrimaryHeat},
                                {"DaycareOrAssistedLiving",DaycareOrAssistedLiving},
                                {"Over10Acres",Over10Acres},
                                {"RisksInForeclosureProceedings",RisksInForeclosureProceedings},
                                {"DeveloperSpecHomes",DeveloperSpecHomes},
                                {"Farm",Farm},
                                {"CoverageA",CoverageA},
                                {"CoverageB",CoverageB},
                                {"Zone",FloodZone},
                                {"EffectiveYearBuilt",EffectiveYearBuilt },
                                {"MilesFromCoast",DistanceCoast }
                            },
                            IgnoreDuplicates = true
                        }
                    }

                };
                result = data.MakeJSONHTTPPost(WSurl);

            }   // if underwriter


            return result;
        }

        /// <summary>
        /// ORCHID
        /// </summary>

        public string ImportOrchid()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;
            if (EffectiveDate.Length > 0)
            {

                try
                {
                    dtEffectiveDate = Convert.ToDateTime(EffectiveDate);
                }
                catch
                {
                }
            }

            // Notes
            if (LocationContactName != null)
            {
                if (LocationContactName.Length > 0 || LocationContactPhone.Length > 0)
                {
                    if (!(LocationContactName == ContactName))
                    {
                        if (Comments.Length > 0)
                        {
                            Comments = Comments + System.Environment.NewLine + System.Environment.NewLine;
                        }
                        Comments = Comments + "Location Contact: " + LocationContactName + " " + LocationContactPhone;
                    }
                }
            }
            if (GenericField1Name != null)
            {
                if (GenericField1Name.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + GenericField1Name + " " + GenericField1Value;
                }
            }
            if (RequestedBy != null)
            {
                if (RequestedBy.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + "Requested by " + RequestedBy;
                }
            }
            if (AgentEmail != null)
            {
                if (AgentEmail.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + "Agent Email " + AgentEmail;
                }
            }

            if (AltLocationContact != null)
            {
                if (AltLocationContact.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine;
                    }
                    Comments = Comments + AltLocationContact;
                }
            }


            // Rush
            bool bRush = false;
            if (RushHandling.ToUpper() == "Y")
                bRush = true;

            // Agent name
            string sAgencyName = "";
            if (AgencyAgentName != null)
            {
                if (AgencyAgentName.Length > 0)
                    sAgencyName = AgencyAgentName;
                if (Producer != null)
                {
                    if (Producer.Length > 0)
                        sAgencyName += " - " + Producer;
                }

                if (sAgencyName.Length > 75)
                {
                    sAgencyName = sAgencyName.Substring(0, 75);
                }

            }

            // Attributes
            decimal decCoverageA = 0;
            if (!Decimal.TryParse(CoverageA, out decCoverageA))
                decCoverageA = 0;

            int iYearBuilt = 0;
            if (!Int32.TryParse(YearBuilt, out iYearBuilt))
                iYearBuilt = 0;

            // ImportInspectionsRequest is in LC360API
            var data = new LC360API.Carrier_V1.ImportInspectionsRequest
            {
                UserName = CustomerUserName,
                Password = CustomerPassword,
                Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = sAgencyName,
                                    AgentCode = AgentCode,
                                    ContactName = Producer,
                                    Fax = AgentFax,
                                    Phone = AgencyAgentPhone,
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy
                            },
                            //Underwriter = new Underwriter
                            //{
                            //    FirstName = UnderwriterFirstName,
                            //    LastName = UnderwriterLastName,
                            //    CorrespondanceEmail = UnderwriterCorrEmail,
                            //    ReportEmail = UnderwriterRptEmail,
                            //    Phone = UnderwriterPhone
                            //},
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Insurance Company", InsuranceCompany},
                                {"Occupancy",Occupancy},
                                {"Usage",Usage},
                                {"DwellingType",DwellingType},
                                {"Stories",Stories},
                                {"YearBuiltOrchid",YearBuilt},
                                {"ExteriorWallType",ExteriorWallType},
                                {"ExteriorCoating",ExteriorCoating},
                                {"RoofGeometry",RoofGeometry},
                                {"RoofType",RoofType},
                                {"RoofYear",RoofYear},
                                {"RoofCondition",RoofCondition},
                                {"PoolPresent",PoolPresent},
                                {"PoolAboveGround",PoolAboveGround},
                                {"PoolScreenEnclosurePresent",PoolScreenEnclosurePresent},
                                {"PoolFencePresent",PoolFencePresent},
                                {"DivingBoardOrPlatformPresent",PoolDivingBoardPresent},
                                {"PoolSlidePresent",PoolSlidePresent},
                                {"WindstormProtectiveDevices",WindstormProtectiveDevices},
                                {"ProtectionClass",ProtectionClass},
                                {"CentralStationFireAlarmPresent",CentralStationFireAlarmPresent},
                                {"CentralStationBurglarAlarmPresent",CentralStationBurglarAlarmPresent},
                                {"CentralAlarmPresent",CentralAlarmPresent},
                                {"Dog",Dog},
                                {"DistanceCoast",DistanceCoast},
                                {"ElectricalUpdatesOrchid",ElectricalUpdates},
                                {"PlumbingFull/PartialUpdateOrchid",PlumbingFullPartial},
                                {"PlumbingUpdateYearOrchid",PlumbingYear},
                                {"HvacUpdatesOrchid",HvacUpdates},
                                {"HvacFull/PartialUpdateOrchid",HvacFullPartial},
                                {"HvacUpdateYearOrchid",HvacYear},
                                {"PlumbingUpdatesOrchid",PlumbingUpdates},
                                {"ElectricalFull/PartialUpdateOrchid",ElectricalFullPartial},
                                {"ElectricalUpdateYearOrchid",ElectricalYear}
                            },
                            IgnoreDuplicates = true
                        }
                    }

            };

            string result = data.MakeJSONHTTPPost(WSurl);

            //var importResults = result.FromJSON<List<ImportResult>>();

            //foreach( var importResult in importResults )
            //{
            //    Console.WriteLine("Import Successful = {0}", importResult.Successful);
            //    Console.WriteLine("");
            //    //Console.WriteLine(importResult.ToJSON());

            //    if( !importResult.Successful )
            //    {
            //        if( importResult.Errors != null )
            //            foreach( var error in importResult.Errors )
            //            {
            //                Console.WriteLine("Error {0}", error.ErrorText);
            //            }
            //    }
            //}

            return result;
        }

        public string ImportOrchidConnect()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

            // effective date 
            DateTime dtEffectiveDate = DateTime.MinValue;
            if (EffectiveDate.Length > 0)
            {

                try
                {
                    dtEffectiveDate = Convert.ToDateTime(EffectiveDate);
                }
                catch
                {
                }
            }

            // Notes
            if (LocationContactName != null)
            {
                if (LocationContactName.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine + System.Environment.NewLine;
                    }
                    Comments = Comments + "Inspection Contact: " + LocationContactName;
                }
            }
            if (LocationContactPhone != null)
            {
                if (LocationContactPhone.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine + System.Environment.NewLine;
                    }
                    Comments = Comments + "Inspection Contact Phone: " + LocationContactPhone;
                }
            }
            if (InsuredEmail != null)
            {
                if (InsuredEmail.Length > 0)
                {
                    if (Comments.Length > 0)
                    {
                        Comments = Comments + System.Environment.NewLine + System.Environment.NewLine;
                    }
                    Comments = Comments + "Inspection Contact Email: " + InsuredEmail;
                }
            }
            
            // Rush
            bool bRush = false;
            if (RushHandling.ToUpper() == "Y")
                bRush = true;

            // Attributes
            decimal decCoverageA = 0;
            if (!Decimal.TryParse(CoverageA, out decCoverageA))
                decCoverageA = 0;

            int iYearBuilt = 0;
            if (!Int32.TryParse(YearBuilt, out iYearBuilt))
                iYearBuilt = 0;

            // ImportInspectionsRequest is in LC360API
            var data = new LC360API.Carrier_V1.ImportInspectionsRequest
            {
                UserName = CustomerUserName,
                Password = CustomerPassword,
                Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            Agent = new Agent
                                {
                                    AgencyName = AgencyAgentName,
                                    AgentCode = AgentCode,
                                    ContactName = Producer,
                                    Fax = "",
                                    Phone = "",
                                    Address = new Address
                                    {
                                        City = AgentCity,
                                        Street1 = AgentAddress1,
                                        Street2 = AgentAddress2,
                                        StateOrProvince = AgentState,
                                        ZipCode = AgentZip
                                    }
                                },
                            Attributes = new Attributes
                            {
                                CoverageAIn = decCoverageA,
                                YearBuilt = iYearBuilt,
                                Occupancy = Occupancy
                            },
                            //Underwriter = new Underwriter
                            //{
                            //    FirstName = UnderwriterFirstName,
                            //    LastName = UnderwriterLastName,
                            //    CorrespondanceEmail = UnderwriterCorrEmail,
                            //    ReportEmail = UnderwriterRptEmail,
                            //    Phone = UnderwriterPhone
                            //},
                            EffectiveDate = dtEffectiveDate,
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            IsRush  = bRush,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = ContactPhoneHome,
                                WorkPhone = ContactPhoneWork,
                                CellPhone = ContactPhoneCell,
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ContactName
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"BurglarAlarmPresent",BurglarlAlarmPresent},
                                {"DistanceCoast",DistanceCoast},
                                {"DryVit",SidingDryVit},
                                {"Eifs",SidingEIFS},
                                {"Floors",Stories},
                                {"Insurance Company", InsuranceCompany},
                                {"Occupancy",Occupancy},
                                {"OtherPet",OtherPets},
                                {"PoolAboveGround",PoolAboveGround},
                                {"PoolFencePresent",PoolFencePresent},
                                {"PoolScreenEnclosurePresent",PoolScreenEnclosurePresent},
                                {"ProtectionClass",ProtectionClass},
                                {"RoofType",RoofType},
                                {"SecuredCommunity",Secured},
                                {"Vinyl",SidingVinyl},
                                {"WiringYear",WiringYear},
                                {"YearBuiltOrchid",YearBuilt},
                                {"Dog",Dog},
                                {"DwellingType",DwellingType},
                                {"FireAlarmPresent",FirelAlarmPresent},
                                {"HeatingAcYear",HeatingYear},
                                {"Masonite",SidingMasonite},
                                {"Other",SidingOther},
                                {"PlumbingYear",PlumbingYear},
                                {"PoolDivingBoardPresent",PoolDivingBoardPresent},
                                {"PoolPresent",PoolPresent},
                                {"PoolSlidePresent",PoolSlidePresent},
                                {"RoofGeometry",RoofGeometry},
                                {"RoofYear",RoofYear},
                                {"Updates",Updates},
                                {"WindstormProtectiveDevices",WindstormProtectiveDevices},
                                {"WoodShake",SidingWood},
                                {"Construction",Construction}

                            },
                            IgnoreDuplicates = true
                        }
                    }

            };

            string result = data.MakeJSONHTTPPost(WSurl);

            //var importResults = result.FromJSON<List<ImportResult>>();

            //foreach( var importResult in importResults )
            //{
            //    Console.WriteLine("Import Successful = {0}", importResult.Successful);
            //    Console.WriteLine("");
            //    //Console.WriteLine(importResult.ToJSON());

            //    if( !importResult.Successful )
            //    {
            //        if( importResult.Errors != null )
            //            foreach( var error in importResult.Errors )
            //            {
            //                Console.WriteLine("Error {0}", error.ErrorText);
            //            }
            //    }
            //}

            return result;
        }
        public string ImportES()
        {
            //string url = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";



            // Attributes
            decimal decCoverageA = 0;
            int iYearBuilt = 0;

            // ImportInspectionsRequest is in LC360API
            var data = new LC360API.Carrier_V1.ImportInspectionsRequest
            {
                UserName = CustomerUserName,
                Password = CustomerPassword,
                Inspections = new List<Inspection>
                    {
                        new Inspection
                        {
                            CustomerKey = CustomerAccount,
                            Notes = Comments,
                            InspectionType = InspectionType,
                            PolicyNumber = PolicyNumber,
                            PolicyHolder = new PolicyHolder
                            {
                                HomePhone = "",
                                WorkPhone = "",
                                CellPhone = "",
                                PolicyHolderName = InsuredName,
                                PolicyHolderContact = ""
                            },
                            Location = new Address
                            {
                                City = LocationCity,
                                Street1 = LocationAddress1,
                                Street2 = LocationAddress2,
                                StateOrProvince = LocationState,
                                ZipCode = LocationZip
                            },
                            Mailing = new Address
                            {
                                City = MailCity,
                                Street1 = MailAddress1,
                                Street2 = MailAddress2,
                                StateOrProvince = MailState,
                                ZipCode = MailZip
                            },
                            ExtraFields = new Dictionary<string,string>
                            {
                                {"Link", Comments},
                            },
                            IgnoreDuplicates = true
                        }
                    }

            };

            string result = data.MakeJSONHTTPPost(WSurl);

            //var importResults = result.FromJSON<List<ImportResult>>();

            //foreach( var importResult in importResults )
            //{
            //    Console.WriteLine("Import Successful = {0}", importResult.Successful);
            //    Console.WriteLine("");
            //    //Console.WriteLine(importResult.ToJSON());

            //    if( !importResult.Successful )
            //    {
            //        if( importResult.Errors != null )
            //            foreach( var error in importResult.Errors )
            //            {
            //                Console.WriteLine("Error {0}", error.ErrorText);
            //            }
            //    }
            //}

            return result;
        }

    }
    public static class Helpers
    {

        //Prod
        //static string s360URL = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

        //UAT
        //static string s360TestURL = "https://suttonuat.losscontrol360.com/API/Carrier/V1/ImportInspections";
        //public static string WSURL { get; set; } = "https://ecommerce3.sibfla.com/API/Carrier/V1/ImportInspections";

        public static T FromJSON<T>(this string json)
        {
            if (!string.IsNullOrEmpty(json))
            {
                using (MemoryStream ms = new MemoryStream(ASCIIEncoding.Default.GetBytes(json)))
                {
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
                    return (T)ser.ReadObject(ms);
                }
            }
            else
            {
                ConstructorInfo cInfo = typeof(T).GetConstructor(new Type[] { });
                return (T)cInfo.Invoke(null);
            }
        }


        /// <summary>
        /// Creates JSON string from an object (Can't do Anonymous types use ToJSON2 to do that)
        /// </summary>
        /// <param name="obj">Object to make into JSON string</param>
        /// <returns>JSON formatted string containing property structure of passed object</returns>
        public static string ToJSON(this object obj)
        {
            string json = string.Empty;
            DataContractJsonSerializer ser = new DataContractJsonSerializer(obj.GetType());
            using (MemoryStream ms = new MemoryStream())
            {
                ser.WriteObject(ms, obj);
                json = Encoding.Default.GetString(ms.ToArray());
            }
            return json;
        }

        public static string MakeJSONHTTPPost(this object objectToPostAsJSON, string url)
        {
            return objectToPostAsJSON.ToJSON().MakeJSONHTTPPost(url);
        }


        public static string MakeJSONHTTPPost(this string jsonToPost, string url)
        {
            Uri address = new Uri(url);

            // Create the web request  
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;

            // Set type to POST  
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Timeout = 1000 * 60 * 60;
            request.UserAgent = "LC360 Sample App";
            request.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
            //request.Accept = "*/*";

            // Create a byte array of the data we want to send  
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(jsonToPost);

            // Set the content length in the request headers  
            request.ContentLength = byteData.Length;

            // Write data  
            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }

            // Get response  
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                // Get the response stream  
                StreamReader reader = new StreamReader(response.GetResponseStream());

                // Request Result Text 
                string result = reader.ReadToEnd();

                return result;
            }
        }
    }
}
