﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace DistrictReport
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_SIBIConnStr;
        static string cfg_SIBUtilConnStr;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static int cfg_TimeZoneOffset;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet oSIBWorksheet = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Worksheet o360MWorksheet = null;
        static Excel.Range oRange = null;
        static DateTime dBegDate;
        static DateTime dEndDate;
        static DateTime dBegDate360;
        static DateTime dEndDate360;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;
        static string sMode;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_SIBIConnStr = colNameVal.Get("SIBIConnStr");
                cfg_SIBUtilConnStr = colNameVal.Get("SIBUtilConnStr");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_TimeZoneOffset = Convert.ToInt32(colNameVal.Get("timezoneoffset"));


                // exit if incorrect number of parameters are supplied
                //
                // Parameters:
                //  Mode
                //  W = weekly
                //  M = monthly
                //  C = custom
                sMode = "";
                if (args.Length < 1)
                {
                    throw new SystemException("Invalid number of arguments supplied");
                }

                if (args[0].ToUpper() == "W")
                {
                    // ** Weekly **
                    // Assumes that it is being run on Monday
                    // Results are for previous Sun-Sat

                    sMode = "W";
                    DateTime dt = new DateTime();
                    dt = DateTime.Today;

                    // Set beg date to previous Sunday - Sat
                    dBegDate = dt.AddDays(-8);
                    dEndDate = dt.AddDays(-2);

                }
                else if (args[0].ToUpper() == "M")
                {

                    ///////////////////////////////////////////
                    // **Monthly**
                    // Runs for previous month

                    sMode = "M";                 
                    DateTime dt = new DateTime();
                    dt = DateTime.Today;
                    dt = dt.AddMonths(-1);

                    // Set beg date to first/last day of previous month
                    dBegDate = FirstDayOfMonth(dt);
                    dEndDate = LastDayOfMonth(dt);
                }
                else if (args[0].ToUpper() == "C")
                {

                    ///////////////////////////////////////////
                    // **Custom Date Range**
                    sMode = "C";
                    dBegDate = Convert.ToDateTime(args[1]);
                    dEndDate = Convert.ToDateTime(args[2]);
                }
                else if (args[0].ToUpper() == "T")
                {

                    ///////////////////////////////////////////
                    // **Run for Current Date and email to Tiana**
                    sMode = "T";
                    dBegDate = DateTime.Today;
                    dEndDate = DateTime.Today;
                }

                else
                {
                    throw new SystemException("Invalid period arguments supplied");
                }

                string sDateDisp = dBegDate.Month + "-" + dBegDate.Day + "_" + dEndDate.Month + "-" + dEndDate.Day;
                string sEmailSubject = "Reviewer stats - " + dBegDate.Month + "/" + dBegDate.Day + " - " + dEndDate.Month + "/" + dEndDate.Day + ", " + dBegDate.Year.ToString();
                string sExcelFileNameNoEx = cfg_outputdir + "DistrictReport" + sDateDisp;
                string sExcelFileName = sExcelFileNameNoEx + ".xlsx";


                ///////////////////////////////////////////////////////////////
                

                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                SqlCommand sqlCmd2 = null;
                SqlDataReader sqlReader2 = null;

                string sDistrict = "";
                int iCompleted = 0;
                decimal dInvoiced = 0;
                decimal dPaid = 0;
                decimal dMargin = 0;
                string sUser = "";
                int iCaseNum = 0;
                DateTime dDateComp;
                string sFieldRep = "";
                int iRow = 0;
                bool bDataFound = false;

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                // Init Excel sheet
                oExcel = new Excel.Application();
                oExcel.Visible = false;
                oWorkbook = oExcel.Workbooks.Add(1);
                oSIBWorksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                oSIBWorksheet.Name = "SIB";
                createHeader(oSIBWorksheet, "SIB");
                iRow = 4;

                // Set up SQL for Districts
                sqlConn1 = new SqlConnection(cfg_SIBUtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "select distinct District, len(district) from ZipCodeSubDistrictZips order by len(district), district";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // Set up SQL for data
                sqlCmd2 = new SqlCommand();
                sqlCmd2.CommandType = CommandType.StoredProcedure;
                sqlCmd2.CommandText = "sp_";
                sqlCmd2.Connection = sqlConn1;

                // Get list of districts from SQL
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    // loop through districts
                    do
                    {
                        // set current district
                        sDistrict = (string)sqlReader.GetSqlString(0);

                        // Get data 
                        sqlCmd2.Parameters.Clear();
                        sqlCmd2.Parameters.AddWithValue("@begdate", dBegDate);
                        sqlCmd2.Parameters.AddWithValue("@enddate", dEndDate);

                        sqlReader2 = sqlCmd2.ExecuteReader();

                        if (sqlReader2.HasRows)
                        {
                            bDataFound = true;

                            sqlReader2.Read();

                            // loop through rows
                            do
                            {

                                //completed
                                if (sqlReader.IsDBNull(0))
                                {
                                    iCompleted = 0;
                                }
                                else
                                {
                                    iCompleted = (int)sqlReader.GetSqlInt32(0);
                                }
                                addData(oSIBWorksheet, iRow, 1, iCompleted.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "#,##0");


                                //invoiced
                                if (sqlReader.IsDBNull(1))
                                {
                                    dInvoiced = 0;
                                }
                                else
                                {
                                    dInvoiced = (decimal)sqlReader.GetSqlMoney(1);
                                }
                                addData(oSIBWorksheet, iRow, 2, dInvoiced.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0.00");


                                //paid
                                if (sqlReader.IsDBNull(2))
                                {
                                    dPaid = 0;
                                }
                                else
                                {
                                    dPaid = (decimal)sqlReader.GetSqlMoney(2);
                                }
                                addData(oSIBWorksheet, iRow, 3, dPaid.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0.00");


                                //margin
                                if (sqlReader.IsDBNull(3))
                                {
                                    dMargin = 0;
                                }
                                else
                                {
                                    dMargin = (decimal)sqlReader.GetSqlDecimal(3);
                                }
                                addData(oSIBWorksheet, iRow, 4, dMargin.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "##0.00%");


                                //user
                                if (sqlReader.IsDBNull(4))
                                {
                                    sUser = "";
                                }
                                else
                                {
                                    sUser = (string)sqlReader.GetSqlString(4);
                                }
                                addData(oSIBWorksheet, iRow, 5, sUser.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "");

                                iRow++;

                            } while (sqlReader2.Read());     // SIB

                        }   // SIB - has rows

                        else
                        {
                            addDataNoFormat(oSIBWorksheet, iRow, 1, "No items found", "A" + iRow.ToString(), "A" + iRow.ToString());             
                        }

                    } while (sqlReader.Read());     // districts

                }   // districts - has rows

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;


                msMsgBody = "Processing completed for SIB" + System.Environment.NewLine + cfg_outputdir + "ReviewerStatsSIB_" + sDateDisp + System.Environment.NewLine + System.Environment.NewLine;

                ///////////////////////////////////////////////////
                // 360 
                ///////////////////////////////////////////////////


                // set time to 12:00am
                // adjust for 360 GMT
                dBegDate = ChangeTime(dBegDate, 0, 0, 0, 0);
                dBegDate360 = dBegDate.AddHours(cfg_TimeZoneOffset);

                // set time to midnight
                dEndDate = ChangeTime(dEndDate, 23, 59, 59, 0);
                dEndDate360 = dEndDate.AddHours(cfg_TimeZoneOffset);

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                //sqlCmd1.CommandText = "sp_DistrictReportInv";
                sqlCmd1.CommandText = "sp_ReviewerStats";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate360);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate360);
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing, oSIBWorksheet, oMissing, oMissing);
                o360Worksheet.Name = "360";
                createHeader(o360Worksheet, "360");

                iRow = 4;

                if (sqlReader.HasRows)
                {

                    bDataFound = true;
                    //oExcel = new Excel.Application();
                    //oExcel.Visible = false;
                    //oWorkbook = oExcel.Workbooks.Add(1);

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //completed
                        if (sqlReader.IsDBNull(0))
                        {
                            iCompleted = 0;
                        }
                        else
                        {
                            iCompleted = (int)sqlReader.GetSqlInt32(0);
                        }
                        addData(o360Worksheet, iRow, 1, iCompleted.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "#,##0");


                        //invoiced
                        if (sqlReader.IsDBNull(1))
                        {
                            dInvoiced = 0;
                        }
                        else
                        {
                            dInvoiced = (decimal)sqlReader.GetSqlDecimal(1);
                        }
                        addData(o360Worksheet, iRow, 2, dInvoiced.ToString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "#,##0.00");


                        //paid
                        if (sqlReader.IsDBNull(2))
                        {
                            dPaid = 0;
                        }
                        else
                        {
                            dPaid = (decimal)sqlReader.GetSqlDecimal(2);
                        }
                        addData(o360Worksheet, iRow, 3, dPaid.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "#,##0.00");


                        //margin
                        if (sqlReader.IsDBNull(3))
                        {
                            dMargin = 0;
                        }
                        else
                        {
                            dMargin = (decimal)sqlReader.GetSqlDecimal(3);
                        }
                        addData(o360Worksheet, iRow, 4, dMargin.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "##0.00%");


                        //user
                        if (sqlReader.IsDBNull(4))
                        {
                            sUser = "";
                        }
                        else
                        {
                            sUser = (string)sqlReader.GetSqlString(4);
                        }
                        addData(o360Worksheet, iRow, 5, sUser.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "");

                        iRow++;

                    } while (sqlReader.Read());     // 360

                }   // has rows

                else
                {
                    addDataNoFormat(o360Worksheet, iRow, 1, "No items found", "A" + iRow.ToString(), "A" + iRow.ToString());
                }


                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;


                ///////////////////////////////////////////////////
                // 360  - multiple completions
                ///////////////////////////////////////////////////

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_MultipleCompletions";
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@begdate", dBegDate360);
                sqlCmd1.Parameters.AddWithValue("@enddate", dEndDate360);
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {

                    bDataFound = true;
                    o360MWorksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing, o360Worksheet, oMissing, oMissing);
                    o360MWorksheet.Name = "360-MultiComp";
                    createHeaderMultiComp(o360MWorksheet);

                    iRow = 4;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //case #
                        iCaseNum = (int)sqlReader.GetSqlInt32(0);
                        addData(o360MWorksheet, iRow, 1, iCaseNum.ToString(), "A" + iRow.ToString(), "A" + iRow.ToString(), "");


                        //date comp
                        dDateComp = (DateTime)sqlReader.GetSqlDateTime(1);
                        addData(o360MWorksheet, iRow, 2, dDateComp.ToShortDateString(), "B" + iRow.ToString(), "B" + iRow.ToString(), "MM/DD/YYYY");

                        //comp by
                        if (sqlReader.IsDBNull(2))
                        {
                            sUser = "";
                        }
                        else
                        {
                            sUser = (string)sqlReader.GetSqlString(2);
                        }
                        addData(o360MWorksheet, iRow, 3, sUser.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(),"");

                        // Field rep
                        if (sqlReader.IsDBNull(3))
                        {
                            sFieldRep = "";
                        }
                        else
                        {
                            sFieldRep = (string)sqlReader.GetSqlString(3);
                        }
                        addData(o360MWorksheet, iRow, 4, sFieldRep.ToString(), "D" + iRow.ToString(), "D" + iRow.ToString(), "");

                        iRow++;

                    } while (sqlReader.Read());     // 360 - multi

                }   // has rows

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;

                //if (bDataFound)
                //{
                    //oWorkbook.SaveAs(cfg_outputdir + "ReviewerStats360_" + sDateDisp, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                    oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                    oWorkbook.Close(true, oMissing, oMissing);
                    oExcel.Quit();
                    msMsgBody += "Processing completed for 360" + System.Environment.NewLine + cfg_outputdir + "ReviewerStatsSIB_" + sDateDisp + System.Environment.NewLine + System.Environment.NewLine;

                    releaseObject(oExcel);
                    releaseObject(oWorkbook);
                    releaseObject(oSIBWorksheet);
                    releaseObject(o360MWorksheet);
                    releaseObject(o360Worksheet);
                    sendExcelFile(sEmailSubject, sExcelFileName);
                    msMsgBody += "File sent " + sExcelFileName;
                //}
                //else
                //{
                //    msMsgBody += "No data found";
                //}

            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(msMsgBody);                
                }
            
            }
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeader(Excel._Worksheet oWorkSheet, string sMode)
        {

            Excel.Range oRange;


            oWorkSheet.get_Range("A1", "E1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "E1");
            if (sMode == "SIB")
            {
                oRange.FormulaR1C1 = "Reviewer Stats - SIB " + dBegDate.ToShortDateString() + " - " + dEndDate.ToShortDateString();
            } 
            else
            {
                oRange.FormulaR1C1 = "Reviewer Stats - 360 " + dBegDate.ToShortDateString() + " - " + dEndDate.ToShortDateString();
            }

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Completed";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Invoiced";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "Paid";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Margin";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "Comp By";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);     
        
        
        }

        static void createHeaderMultiComp(Excel._Worksheet oWorkSheet)
        {

            Excel.Range oRange;
            
            oWorkSheet.get_Range("A1", "D1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "D1");
            oRange.FormulaR1C1 = "360 - Cases previously completed " + dBegDate.ToShortDateString() + " - " + dEndDate.ToShortDateString();

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Case #";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 20;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Date Comp";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 20;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "Comp By";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 20;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Field Rep";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 20;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

        } 
        
   

        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data, 
			string cell1, string cell2,string format)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }



        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "Reviewer Stats";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Reviewer Stats Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "jeff@sibfla.com";
                if (sMode == "C")
                {
                    oMail.MailTo = "jeff@sibfla.com";
                }
                else if (sMode == "T")
                {
                    oMail.MailTo = "Tiana@sibfla.com;";
                }
                else
                {
                    oMail.MailTo = "Tiana@sibfla.com;andrea@sibfla.com;jeff@sibfla.com";
                }
                //oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }

        static DateTime ChangeTime(DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }



    }
}
