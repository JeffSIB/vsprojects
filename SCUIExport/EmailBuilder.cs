﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace SCUIExport
{
    class EmailBuilder : SCUIExport
    {
        private string companyName = ConfigurationManager.AppSettings["CompanyName"];
        private string customerName = ConfigurationManager.AppSettings["CustomerName"];
        private List<LC360Data.Log> errors = new List<LC360Data.Log>();


        public void CreateEmail(List<CaseTracker> caseTracker, List<LC360Data.ExportCaseHolder> cases)
        {
            StringBuilder sb = new StringBuilder();

            List<CaseTracker> successfulExports = new List<CaseTracker>();
            List<CaseTracker> failedExports = new List<CaseTracker>();

            //int errors = 0;

            if(caseTracker != null)
            {
                foreach(var exportedCase in caseTracker)
                {
                    if(exportedCase.Successful)
                    {
                        successfulExports.Add(exportedCase);
                    }
                    else
                    {
                        failedExports.Add(exportedCase);
                    }
                }
            }

            try
            {
                //ERRORS
                if(errors.Count > 0)
                {
                    EmailSubject = "**ERRORS** - " + companyName + " " + customerName + " - Export Errors";
                    sb.AppendLine("<strong>" + companyName + " " + customerName + " - Export Completed with" + "<font color='red'> " + errors + " </font> error(s) found during the export process.</strong><br/><br/>");

                    if(successfulExports.Count() > 0)
                    {
                        sb.AppendLine(successfulExports.Count + " cases have been exported successfully.<br/>");
                    }

                    //HTML TABLE BUILDER
                    sb.AppendLine("<br/>");
                    sb.AppendLine("<table border='0' cellspacing='3' cellpadding='5'>");
                    sb.AppendLine("<tr border='1'>");
                    sb.AppendLine("<td><strong>Policy #</strong></td><td><strong>Policy Holder</strong></td><td><strong>Insured Address</strong></td><td><strong>Phone Number</strong></td><td><strong>Inspection Type</strong></td>");
                    sb.AppendLine("</tr>");

                    StringBuilder sbErrors = new StringBuilder();
                    string previousPolicyNumber = "";

                    foreach(var error in failedExports)
                    {
                        if(error.PolicyNumber != previousPolicyNumber)
                        {
                            if(previousPolicyNumber != "")
                            {
                                sb.Append("</ol>");
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }

                            //POLICY NUMBER
                            sb.AppendFormat("<td valign='top'>{0}</td>", error.PolicyNumber ?? "");

                            //POLICY HOLDER
                            sb.AppendFormat("<td valign='top'>{0}</td>",
                                    String.IsNullOrEmpty(error.PolicyHolder) ? "n/a" : error.PolicyHolder);

                            //ADDRESS
                            string address = "";
                            address += error.Street1 ?? "";
                            address += error.Street2 ?? "";
                            address += "<br/>";
                            address += error.City ?? "";
                            address += String.IsNullOrEmpty(error.City) ? ", " : "";
                            address += error.StateOrProvince ?? "";
                            address += " " + error.ZipCode ?? "";

                            if(String.IsNullOrEmpty(address))
                            {
                                address = "n/a";
                            }

                            sb.AppendFormat("<td valign='top'>{0}</td>", address);

                            //INSURED PHONE
                            sb.AppendFormat("<td valign='top'>{0}</td>",
                               String.IsNullOrEmpty(error.InsuredPhone) ? "n/a" : error.InsuredPhone);

                            //INSPECTION TYPE
                            sb.AppendFormat("<td valign='top'>{0}</td>",
                               String.IsNullOrEmpty(error.InspectionType) ? "n/a" : error.InspectionType);

                            sb.Append("</tr>");
                            sb.Append("<tr>");
                            sb.Append("<td valign='top' colspan='5'>");
                            sb.Append("<ol>");
                        }

                        /********************************************
                        *
                        *  ERRORS LOGGED HERE
                        *
                        *********************************************/
                        string errorStr = "";

                        errorStr = error.Description;


                        sb.AppendFormat("<li>{0}</li>", errorStr);

                        //SET CURRENT TO PREVIOUS FOR CHECK
                        previousPolicyNumber = error.PolicyNumber;
                    }
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.AppendLine("</table>");

                    SendEmail(sb);
                }

                //Success email is sent.
                else
                {
                    EmailSubject = customerName + " - Export Success";

                    if(successfulExports.Count > 0)
                    {
                        sb.AppendLine("<strong> " + " " + customerName + " - Export Successful!</strong><br/>");

                        if(successfulExports.Count > 0)
                        {
                            sb.AppendLine("The following " + successfulExports.Count + " cases have been exported: <br/>");
                            foreach(var successfulCase in successfulExports)
                            {
                                sb.AppendLine("<font color='#404040'>    " + successfulCase.PolicyNumber + " - " + successfulCase.PolicyHolder + " - " + successfulCase.InspectionType + "</font><br/>");
                            }
                        }
                    }
                    if(successfulExports.Count == 0)
                    {
                        EmailSubject = customerName + " No cases were found to export.";
                        sb.AppendLine("No cases were processed.");
                    }

                    SendEmail(sb);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
                Log(new LC360Data.Log() { Decription = "Error trying to send success/failure email.", LogCode = LC360Data.LogCode.Error });
            }
        }

        private string emailSendToLive = ConfigurationManager.AppSettings["EmailSendToLive"];
        private string emailFromLive = ConfigurationManager.AppSettings["EmailFromLive"];
        private string emailSendToDebug = ConfigurationManager.AppSettings["EmailSendToDebug"];
        private string emailFromDebug = ConfigurationManager.AppSettings["EmailFromDebug"];
        private string emailLogs = ConfigurationManager.AppSettings["EmailLogs"];

        private string EmailSubject { get; set; }

        public void SendEmail(StringBuilder message)
        {
            try
            {
#if DEBUG
                string from = emailFromDebug;
                string to = emailLogs;
#else
                string from = emailFromLive;
                string to = emailLogs;
#endif

                using(MailMessage msg = new MailMessage())
                {
                    msg.From = new MailAddress(from);
                    msg.To.Add(new MailAddress(to));
                    msg.Subject = EmailSubject;

                    msg.IsBodyHtml = true;

                    msg.Body = message.ToString();

                    Send(msg);
                }
            }
            catch
            {
                Log(new LC360Data.Log() { Decription = "Error trying to send success/failure email.", LogCode = LC360Data.LogCode.Error });
            }
        }

        public static void Send(MailMessage message)
        {
            try
            {
                SmtpClient client = new SmtpClient();
                client.Send(message);
            }
            catch
            {
                
            }
        }

    }
}
