﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Data.SqlClient;
using System.Data;


namespace RPSExport
{
    public class RPSExport : LC360Data.Exporter
    {
        #region Properties

        /// <summary>
        /// Company name - Set in AppSettings (i.e. Sutton)
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Customer name - Set in AppSettings (i.e. USAA)
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Customers to export from LC360 - Set in AppSettings (i.e. 0000, 1234, LOOKUPID)
        /// It is a comma separated string within the App.config.
        /// </summary>
        public string CustomersToExport { get; set; }

        /// <summary>
        /// Log directory - Set in AppSettings (i.e. C:\Accounts\Sutton\USAA\Export\Logs\)
        /// </summary>
        public string LogsDir { get; set; }

        /// <summary>
        /// Return form data on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. true)
        /// </summary>
        public string ReturnFormData { get; set; }

        /// <summary>
        /// Return PDF on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. true)
        /// </summary>
        public string ReturnPDF { get; set; }

        /// <summary>
        /// Return raw images on export of cases, gets converted to a Boolean - Set in AppSettings (i.e. false)
        /// </summary>
        public string ReturnRawImages { get; set; }

        /// <summary>
        /// Send emails to specified email address on export of cases in DEBUG mode - Set in AppSettings (i.e. test@inpections.com)
        /// </summary>
        public string EmailSendToDebug { get; set; }

        /// <summary>
        /// Specify the From email address on export of cases in DEBUG mode - Set in AppSettings (i.e. test@inpections.com)
        /// </summary>
        public string EmailFromDebug { get; set; }

        /// <summary>
        /// Send emails to specified email address on export of cases in LIVE/RELEASE mode - Set in AppSettings (i.e. inspections@sibfla.com)
        /// </summary>
        public string EmailSendToLive { get; set; }

        /// <summary>
        /// Specify the From email address on export of cases in LIVE/RELEASE mode - Set in AppSettings (i.e. GA_EDS@usaa.com)
        /// </summary>
        public string EmailFromLive { get; set; }

        /// <summary>
        /// Export filter for completed cases by start date
        /// </summary>
        public string CompletedMinDate { get; set; }

        /// <summary>
        /// Dir for exported PDF's
        /// </summary>
        public string PDFDir { get; set; }

        /// <summary>
        /// 360 SQL conn str
        /// </summary>
        public string cfg_360UtilConnStr { get; set; }

        /// <summary>
        /// Number of cases exported
        /// </summary>
        public int ExportCount { get; set; }

        #endregion


        #region IExporter Members

        /// <summary>
        /// Here we set up the filter which tells the control what cases we want back.
        /// Note: We only pass cases that have not been previously exported.
        /// </summary>
        /// <returns></returns>
        public override LC360Data.CasesSearchCriteria GetFilter()
        {
            // Message displayed for logging.
            Log(new LC360Data.Log() { Decription = "Getting Search Criteria", LogCode = LC360Data.LogCode.JustForProgrammer });

            // This would normally be filtered by a company or case type to get back desired cases.  
            // Most likely with a Date filter as well.
            LC360Data.CasesSearchCriteria criteria = new LC360Data.CasesSearchCriteria();

            // Customer selection criteria - split the comma separated string "CustomersToExport" from App.config into a string array of customer lookup IDs
            criteria.CustomerLookupIDs = CustomersToExport.Split(',');

            string date = CompletedMinDate;
            string sEnddate = "2/2/2022";
            DateTime startDate = DateTime.Parse(date);
            DateTime enddate = DateTime.Parse(sEnddate);
            //Setup date filter
            criteria.DateFilters = new LC360Data.DateFilters();
            criteria.DateFilters.CompletedMin = startDate;
            //criteria.DateFilters.CompletedMax = enddate;
            //criteria.CaseTypeCategories = new Guid[] { new Guid("40109C93-185B-4C3B-BC8E-F37EA921DD89"), new Guid("3B97C9D9-AEA1-4642-80B9-D090CA011AF6"), new Guid("116EAA8C-962C-4BB4-B8D5-DF30C66BFBF4") };
            //criteria.CaseTypeCategories = new Guid[] { new Guid("ef10090f-11dd-426b-9db2-9df5a18c5680")};

            

            /// Case Statuses desired
            criteria.CaseStatuses = new int[] { (int)LC360Data.CaseStatuses.Reviewed, (int)LC360Data.CaseStatuses.Complete };

            return criteria;
        }

        /// <summary>
        /// Name of the exporter
        /// </summary>
        public override string Name
        {
            get
            { return (CompanyName + " " + CustomerName + " " + "Exporter"); }
        }

        #endregion

        /// <summary>
        /// Called by controller once the cases have been retrieved according to the Filter set in GetFilter().
        /// </summary>
        /// <param name="cases"></param>
        /// <returns></returns>
        public override LC360Data.ExportResult ExportCases(List<LC360Data.ExportCaseHolder> cases)
        {
            StringBuilder email = new StringBuilder();
            EmailBuilder eb = new EmailBuilder();
            List<CaseTracker> ct = new List<CaseTracker>();
            string emailSubject = "";
            string sIRFileNum = "";
            int iSuccessCount = 0;
            string FromEmailAddress = EmailFromLive;
            string cfg360ConnStr = cfg_360UtilConnStr;
            string sCaseTypeCat = "";
            Guid gu7331 = new Guid("37610334-621c-40f4-a13d-05625c3f7fbd"); //LA

            /// Logging
            Log(new LC360Data.Log() { Decription = "Starting " + CustomerName + " Export", LogCode = LC360Data.LogCode.JustForProgrammer });

            /// Logging
            Log(new LC360Data.Log() { Decription = "Received " + cases.Count + " for export", LogCode = LC360Data.LogCode.GeneralMessage });

            ExportCount = cases.Count;
            
            /// Create List for successfully exported case IDs
            List<Guid> exportedCaseIDs = new List<Guid>();

            try   // 6/2/21
            {

                foreach (LC360Data.ExportCaseHolder holder in cases)
                {

                    ///Keep track of Exports success or failure.
                    Func<String, bool, CaseTracker> TrackCaseInfo = (description, successful) =>
                        new CaseTracker
                        {
                            Description = description,
                            Successful = successful,
                            PolicyNumber = holder.Case.PolicyNumber ?? "",
                            PolicyHolder = holder.Case.InsuredName ?? "",
                            Street1 = holder.Case.LocationAddress ?? "",
                            City = holder.Case.LocationCity ?? "",
                            StateOrProvince = holder.Case.LocationState ?? "",
                            ZipCode = holder.Case.LocationPostalCode ?? "",
                            InspectionType = holder.Case.CaseType ?? ""
                        };

                    Log(new LC360Data.Log() { Decription = "Starting export of Case: " + holder.Case.CaseNumber, LogCode = LC360Data.LogCode.JustForProgrammer });
                    try
                    {

                        // Get IR File number
                        sIRFileNum = GetIRFileNum(holder.Case.CaseID, cfg_360UtilConnStr);

                        string SendToEmail;
                        // 7331 Covington, LA - special handling
                        if (holder.Case.CustomerID == gu7331)
                        {
                            if (sIRFileNum.Length > 0)
                            {
                                emailSubject = holder.Case.InsuredName.Trim() + " [SYS*REF#" + sIRFileNum + "]";
                            }
                            else
                            {
                                emailSubject = holder.Case.PolicyNumber.Trim() + " - completed report";
                            }

                            //  Commercial send to Courtney_Dupuis@RPSins.com
                            //  Personal send to Pam_Schneider@rpsins.com

                            //  If Non-Prod or Additional Info, get ordered as case type
                            if (holder.Case.CaseType.ToUpper().Contains("NON-PROD") || holder.Case.CaseType.ToUpper().Contains("ADDITIONAL"))
                            {
                                sCaseTypeCat = GetOrderedAsCaseTypeCat(holder.Case.CaseID, cfg_360UtilConnStr);
                            }
                            else
                            {
                                sCaseTypeCat = holder.Case.CaseTypeCategory.Trim();
                            }

                            if (sCaseTypeCat.ToUpper().Contains("RESIDENT"))
                            {
                                SendToEmail = "Pam_Schneider@rpsins.com";

                            }
                            else if (sCaseTypeCat.ToUpper().Contains("COMM") || sCaseTypeCat.ToUpper().Contains("PHONE"))
                            {
                                SendToEmail = "Courtney_Dupuis@RPSins.com";

                            }
                            else
                            {
                                emailSubject = "**** RPS NO CASE TYPE CATEGORY - " + holder.Case.CaseNumber;
                                SendToEmail = "jeff@sibfla.com";
                            }
                        }
                        else
                        {
                            // All accounts except 7331
                            SendToEmail = "RPSInspections@rpsins.com";

                            if (sIRFileNum.Length > 0)
                            {
                                emailSubject = "[SYS*REF#" + sIRFileNum + "]";
                            }
                            else
                            {
                                emailSubject = holder.Case.PolicyNumber.Trim() + " - completed report";
                            }
                        }   // if 7331


                        /// Remove characters that would make it fail
                        emailSubject = emailSubject.Replace("\n", " ").Replace("\r", " ");

                        /// Log that email was successfully created
                        Log(new LC360Data.Log() { Decription = "Case  " + holder.Case.CaseNumber+ " sent to " + SendToEmail + "Case type: " + sCaseTypeCat, LogCode = LC360Data.LogCode.GeneralMessage });


                        //Instantiate and create Email message
                        using (MailMessage msg = new MailMessage())
                        {
                            msg.From = new MailAddress(FromEmailAddress);
                            msg.To.Add(new MailAddress(SendToEmail));
                            msg.Subject = emailSubject;

                            /// Gets PDF into a byte array
                            byte[] file = holder.PDFFile;

                            /// Save binary data to memory stream
                            MemoryStream ms = new MemoryStream(file);

                            /// Create attachment from stream.  
                            msg.Attachments.Add(new Attachment(ms, holder.Case.CaseNumber.ToString() + ".pdf"));
                            SendEmailMessage(msg);

                            //Log(new LC360Data.Log() { Decription = "Email for " + CustomerName + " Export of Case " + holder.Case.CaseNumber + " successfully sent to " + SendToEmail, LogCode = LC360Data.LogCode.GeneralMessage });
                            Log(new LC360Data.Log() { Decription = "Case " + holder.Case.CaseNumber + " sent to " + SendToEmail, LogCode = LC360Data.LogCode.GeneralMessage });

                        }


                        Log(new LC360Data.Log() { Decription = holder.Case.CaseNumber + " successfully exported.", LogCode = LC360Data.LogCode.GeneralMessage });

                        ////////////////////////////////////////////////////////////////////////
                        /// Mark as exported
                        exportedCaseIDs.Add(holder.Case.CaseID);
                        ////////////////////////////////////////////////////////////////////////

                        ct.Add(TrackCaseInfo("Success exporting case: " + holder.Case.CaseNumber, true));

                        iSuccessCount++;

                    }
                    catch (Exception ex)
                    {
                        Log(new LC360Data.Log() { Decription = "Error exporting case: " + holder.Case.CaseNumber + ex.Message, LogCode = LC360Data.LogCode.IssueWithPlugin });
                        ct.Add(TrackCaseInfo("Error exporting case: " + holder.Case.CaseNumber, false));
                    }

                    /// End of ForEach holder in cases
                }

            }   // try  6/2/21
            
            catch (Exception ex)
            {
                Log(new LC360Data.Log() { Decription = ex.InnerException.ToString() });                
            }


            ///Create and send success/failure email
            eb.CreateEmail(ct, cases);

            /// Arrange to pass back 'Export Complete' message
            Log(new LC360Data.Log() { Decription = "Export Complete", LogCode = LC360Data.LogCode.Successful });


            /// Set up results to be returned to the calling program
            LC360Data.ExportResult result = new LC360Data.ExportResult();
             

            /// Do only if live - because with test you may need to reexport
            result.ExportedCaseIDs = exportedCaseIDs;

            // return successfully exported cases
            ExportCount = iSuccessCount;

            return result;
        }


        /// <summary>
        /// Send a given message using the current default SMTP Client 
        /// </summary>
        /// <param name="message"></param>
        public static void SendEmailMessage(MailMessage message)
        {
            SmtpClient client = new SmtpClient();
            client.Send(message);
        }


        /// <summary>
        /// Called by controller to get export options for the retrieval of case information
        /// These options are set using AppSettings located in App.config
        /// </summary>
        /// <returns></returns>
        public override LC360Data.ExportOptions GetExportOptions()
        {
            LC360Data.ExportOptions options = new LC360Data.ExportOptions();
            options.ReturnFormData = Convert.ToBoolean(ReturnFormData);
            options.ReturnPDF = Convert.ToBoolean(ReturnPDF);
            options.ReturnRawImages = Convert.ToBoolean(ReturnRawImages);
            return options;
        }

        /// <summary>
        /// Guid ID for this particular application.
        /// </summary>
        public override Guid ApplicationID
        {
            get { return new Guid("6D0331D4-3D28-44D1-98F8-A81491E7F466"); }
        }

        private static string GetIRFileNum(Guid guCaseID, string cfg_360UtilConnStr)
        {

            string sRetVal = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                sqlCmd1.Parameters.AddWithValue("@fieldname", "ImageRight File Number");

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        if (!sqlReader.IsDBNull(0))
                        {
                            sRetVal = sqlReader.GetSqlString(0).ToString();
                        }

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }
                
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();


            }

            return sRetVal;
        }

        private static string GetCaseTypeCat(Guid guCaseID, string cfg_360UtilConnStr)
        {

            string sRetVal = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlCmd1.CommandText = "sp_GetCaseTypeCategory";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        if (!sqlReader.IsDBNull(0))
                        {
                            sRetVal = sqlReader.GetSqlString(0).ToString();
                        }

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }

            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();


            }

            return sRetVal;
        }


        private static string GetOrderedAsCaseTypeCat(Guid guCaseID, string cfg_360UtilConnStr)
        {

            string sRetVal = ""; 

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlCmd1.CommandText = "sp_GetOrderedAsCaseType";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        if (!sqlReader.IsDBNull(0))
                        {
                            sRetVal = sqlReader.GetSqlString(1).ToString();
                        }

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }

            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();


            }

            return sRetVal;
        }


    }

}
