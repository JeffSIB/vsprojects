﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Configuration;
using Word = Microsoft.Office.Interop.Word;
using System.Text.RegularExpressions;

namespace MacDuffRecs
{



    public class Recs
    {
        private readonly string sRecText;
        public string RecText { get { return sRecText; } }

        private readonly Guid guRecID;
        public Guid RecID { get { return guRecID; } }

        private readonly string sRecType;
        public string RecType { get { return sRecType; } }

        public Recs(string RecText, Guid RecID, string RecType)
        {
            this.sRecText = RecText;
            this.guRecID = RecID;
            this.sRecType = RecType;
        }
    }

    public class Photos
    {

        private readonly Guid guRecID;
        public Guid RecID { get { return guRecID; } }

        private readonly Guid guPhotoID;
        public Guid PhotoID { get { return guPhotoID; } }

        public Photos(Guid RecID, Guid PhotoID)
        {
            this.guRecID = RecID;
            this.guPhotoID = PhotoID;
        }
    }
    
    class MacDuffRecs
    {


        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_casefilesroot = ConfigurationManager.AppSettings["CaseFilesRoot"];
        static string cfg_outputdir = ConfigurationManager.AppSettings["OutputDir"];
        static string cfg_templatedir = ConfigurationManager.AppSettings["TemplateDir"];

        static List<Recs> LRecs;
        static List<Photos> LPhotos;

        static bool bGenRecs;
        static bool bSugRecs;

        static string sCaseNum;
        static string sInsured;
        static string sPolicy;
        static string sAdd1;
        static string sAdd2;
        static string sCSZ;
        static string sCaseType;
        static string sInsCo;
        static string sProducer;
        static string sProducerPhone;
        static string sCustNumber;
        static string sCompDate;
        static bool bErr;

        static void Main(string[] args)
        {

            string sCaseID = "";
            Guid guCaseID;
            sCaseNum = "";
            FileInfo fiRec;

            try
            {
                // get file name from command line
                sCaseID = args[0];

                // nothing passed
                if (sCaseID.Length == 0)
                {
                    // send email
                    throw new ApplicationException("No case id passed.");
                }

                //logError("Creating recs for: " + sCaseID);

                // case # or GUID
                if (sCaseID.Length < 9)
                {
                    sCaseID = GetCaseID(sCaseID);
                    try
                    {
                        guCaseID = new Guid(sCaseID);
                    }
                    catch
                    {
                        throw new ApplicationException("Convert Case Number to case ID failed for case num: " + sCaseNum);
                    }
                }
                else
                {
                    guCaseID = new Guid(sCaseID);
                }
            }

            catch (Exception ex)
            {
                sendErrEmail("****MacDuffRecs Error\r\n\r\n" + ex.Message);
                logError(ex.Message);
                return;
            }


            try

            {

                bErr = false;
                LRecs = new List<Recs>();
                LPhotos = new List<Photos>();

                // get case info
                if (!GetCaseInfo(guCaseID))
                {
                    bErr = true;
                    throw new ApplicationException("GetCaseInfo returned error.");
                }

                // Delete existing rec doc (could be removed during reopen)
                fiRec = new FileInfo(cfg_outputdir + sCaseNum + "Recs.docx");
                if (fiRec.Exists)
                {
                    fiRec.Delete();
                }
                
                // get recs
                bGenRecs = false;
                bSugRecs = false;
                int iGetRecs = GetRecs(guCaseID);

                // error
                if (iGetRecs < 0)
                {
                    bErr = true;
                    throw new ApplicationException("GetRecs returned error.");                
                }

                // no recs
                if (iGetRecs == 0)
                { 
                    logError("No recs for case: " + sCaseNum);                
                }
                // recs present - build doc
                else 
                {
                    // get photos
                    for (int i = 0; i < LRecs.Count; i++)
                    {
                        if (!GetPhotos(LRecs[i].RecID))
                        {
                            bErr = true;
                            throw new ApplicationException("GetPhotos returned error.");
                        }

                    }

                    // build doc
                    if (buildDoc(guCaseID))
                    {
                        logError("Rec doc created for: " + sCaseNum);
                    }
                    else
                    {
                        bErr = true;
                        logError("buildDoc returned error processing case - " + sCaseNum);
                        throw new ApplicationException(" buildDoc returned error processing case - " + sCaseNum);
                    }

                }
                Console.WriteLine("Done");

            }

            catch (Exception ex)
            {
                sendErrEmail("****MacDuffRecs Error\r\n\r\n" + ex.Message);
                logError(ex.Message);
                return;
            }
        }

       

        static bool buildDoc(Guid guCaseID)
        {

            bool bRet = false;
            string sPhotoFile = "";

            object oMissing = System.Reflection.Missing.Value;
            object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

            //Start Word and create a new document.
            Word._Application oWord;
            Word._Document oDoc;
            oWord = new Word.Application();
            oWord.Visible = true;
            oDoc = oWord.Documents.Add(cfg_templatedir + "MacDuffRecDocTemplate.dotx", ref oMissing,
                ref oMissing, ref oMissing);
            
            try
            {

                // Styles //
                ///////////

                // Header text
                Word.Style oHeaderTextStyle = oDoc.Styles.Add("HeaderText", Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph);
                oHeaderTextStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oHeaderTextStyle.ParagraphFormat.SpaceAfter = 0;
                oHeaderTextStyle.Font.Name = "Arial";
                oHeaderTextStyle.Font.Size = 10;
                oHeaderTextStyle.Font.Bold = 0;

                // Bold Underline Center
                Word.Style oBUC = oDoc.Styles.Add("BUC", Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph);
                oBUC.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                oBUC.ParagraphFormat.SpaceAfter = 10;
                oBUC.Font.Name = "Arial";
                oBUC.Font.Size = 24;
                oBUC.Font.Bold = 1;
                oBUC.Font.Underline = Word.WdUnderline.wdUnderlineSingle;

                // Rec Header text
                Word.Style oRecHeaderTextStyle = oDoc.Styles.Add("RecHeaderText", Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph);
                oBUC.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oBUC.ParagraphFormat.SpaceAfter = 0;
                oBUC.Font.Name = "Arial";
                oBUC.Font.Size = 10;
                oBUC.Font.Bold = 1;

                // Body
                Word.Style oBodyTextStyle = oDoc.Styles.Add("BodyText", Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph);
                oBodyTextStyle.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oBodyTextStyle.ParagraphFormat.SpaceAfter = 10;
                oBodyTextStyle.Font.Name = "Arial";
                oBodyTextStyle.Font.Size = 10;
                oBodyTextStyle.Font.Bold = 0;


                // Name, address, etc.
                Word.Range oRng = oDoc.Range();
                oRng.set_Style(oHeaderTextStyle);

                Word.Table oTbl = oDoc.Tables.Add(oRng, 6, 3,ref oMissing, ref oMissing);
                oTbl.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleNone;
                oTbl.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleNone;
                oTbl.Columns[1].Width = 75;
                oTbl.Columns[2].Width = 250;
                oTbl.Columns[3].Width = 125;

                //oTbl.Cell(1, 1).Range.Text = " ";
                //oTbl.Cell(1, 2).Range.Text = " ";
                //oTbl.Cell(1, 3).Range.Text = " ";
                
                oTbl.Cell(1, 1).Range.Text = "AGENT: ";
                oTbl.Cell(1, 2).Range.Text = sProducer;
                oTbl.Cell(1, 3).Range.Text = "";
                oTbl.Cell(1, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(1, 3).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(2, 1).Range.Text = "ATTENTION:";
                oTbl.Cell(2, 2).Range.Text = "";
                oTbl.Cell(2, 3).Range.Text = "";
                oTbl.Cell(2, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(2, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(2, 3).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(3, 1).Range.Text = "INSURED:";
                oTbl.Cell(3, 2).Range.Text = sInsured;
                oTbl.Cell(3, 3).Range.Text = "";
                oTbl.Cell(3, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(3, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(3, 3).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(4, 1).Range.Text = "POLICY:";
                oTbl.Cell(4, 2).Range.Text = sPolicy;
                oTbl.Cell(4, 3).Range.Text = "DIARY DATE:";
                oTbl.Cell(4, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(4, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(4, 3).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;

                oTbl.Cell(5, 1).Range.Text = "LOCATION:";
                oTbl.Cell(5, 2).Range.Text = sAdd1 + " " + sCSZ;
                oTbl.Cell(5, 1).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oTbl.Cell(5, 2).Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
                oRng.InsertParagraphAfter();
                oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);

                // Text
                oRng.InsertAfter("Dear insured:" + System.Environment.NewLine);
                oRng.InsertParagraphAfter();
                oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);

                oRng.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                oRng.Font.Size = 12;
                oRng.Font.Bold = 1;
                oRng.Font.Underline = Word.WdUnderline.wdUnderlineSingle;
                oRng.InsertAfter("R E C O M M E N D A T I O N S" + System.Environment.NewLine);
                oRng.InsertAfter("");
                oRng.InsertParagraphAfter();
                oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);

                string sText = "On " + sCompDate + " we made an inspection of your premises at the above captioned location. We make the following recommendation(s):";
                oRng.set_Style(oBodyTextStyle);
                oRng.InsertAfter(sText);
                oRng.InsertParagraphAfter();
                oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);

                oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);

                // Recs

                List<string> sRecTest = new List<string>();

                // Set format for numbered list 
                oWord.ListGalleries[Word.WdListGalleryType.wdNumberGallery].ListTemplates[1].ListLevels[1].NumberFormat = "%1.)";

                object n = 1;
                Word.ListTemplate oLT = oWord.ListGalleries[Word.WdListGalleryType.wdNumberGallery].ListTemplates.get_Item(ref n);

                // Recs
                foreach (Recs sRec in LRecs)
                {
                    oRng.Font.Bold = 0;

                    oRng.ListFormat.ApplyListTemplateWithLevel(oLT, true, oMissing, oMissing, oMissing);
                    oRng.InsertBefore(" ");

                    //// photos
                    //foreach (Photos sPhoto in LPhotos)
                    //{
                    //    if (sPhoto.RecID == sRec.RecID)
                    //    {
                    //        //sPhotoFile = cfg_casefilesroot + guCaseID + @"\" + sPhoto.PhotoID + "-175.jpg";
                    //        sPhotoFile = cfg_casefilesroot + guCaseID + @"\" + sPhoto.PhotoID + "-500.jpg";
                    //        oRng.InlineShapes.AddPicture(sPhotoFile, oMissing, oMissing, oRng);
                    //    }
                    //}

                    //oRng.InsertBefore(sRec.RecText + Convert.ToChar(11) + Convert.ToChar(11));
                    oRng.InsertBefore(sRec.RecText);
                    oRng.InsertParagraphAfter();
                    oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);

                    oRng.ListFormat.RemoveNumbers(Word.WdNumberType.wdNumberParagraph);
                    oRng.InsertParagraphAfter();

                    oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);
                }



                // Footer
                sText = "\v" + "Failure to comply with these recommendations within the next 30 days will result in a direct notice of cancellation. By signing below the insured confirms that all recommendations have been complied with." + "\v" + "\v" + "\v" +
                "Insured: ____________________________________" + "\t" + "Date: _____________________" + "\v" + "\v" + "\v" +
                "Please complete the recommendation(s) listed above within 30 days and, immediately upon completion, return to the address indicated above. Inspections and recommendations are for insurance purposes only. They are necessarily limited in scope and the Company has not undertaken to verify compliance with any local, state or federal health and safety standard." + "\v" + "\v" + "\v";
                oRng.set_Style(oBodyTextStyle);
                oRng.InsertAfter(sText);
                oRng.InsertParagraphAfter();
                oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);


                // photos
                foreach (Photos sPhoto in LPhotos)
                {
                    oRng.InsertBefore(" ");
                    sPhotoFile = cfg_casefilesroot + guCaseID + @"\" + sPhoto.PhotoID + ".jpg";
                    oRng.InlineShapes.AddPicture(sPhotoFile, oMissing, oMissing, oRng);
                    oRng.InsertParagraphAfter();
                    oRng.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);

                }


                oWord.ActiveDocument.SaveAs2(cfg_outputdir + sCaseNum + ".doc",Word.WdSaveFormat.wdFormatDocumentDefault);
                //oWord.ActiveDocument.SaveAs(cfg_outputdir + sCaseNum + "Recs.pdf",17);
                //oDoc.Close();
                //oWord = null;

                bRet = true;
            }
            catch (Exception ex)
            {

                //record exception  
                logError(ex.Message);

            }

            finally 
            {
                if (oDoc != null)
                {
                    oDoc.Close(false);
                    oDoc = null;
                }
                if (oWord != null)
                {
                    oWord.Quit();
                    oWord = null;
                }
            
            }

                return bRet;

        }

        static int GetRecs(Guid guCaseID)
        {

            int iRetVal = -1;

            string sRecText = "";
            string sRecType = "";
            Guid guRecGuid;

            //struRecs SRecs = new struRecs();

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetRecsForCase";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // Get all recs 
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {
                        // rec text
                        if (sqlReader.IsDBNull(0))
                        {
                            sRecText = "";
                        }
                        else
                        {
                            sRecText = sqlReader.GetSqlString(0).ToString();
                        }
                        sRecText = StripTagsCharArray(sRecText);

                        //rec guid
                        guRecGuid = (Guid)sqlReader.GetSqlGuid(7);

                        // rec type
                        if (sqlReader.IsDBNull(6))
                        {
                            sRecType = "General";
                        }
                        else
                        {
                            sRecType = sqlReader.GetSqlString(6).ToString();
                        }
                        
                        if (sRecType.ToUpper().Contains("SUGG"))
                        {
                            bSugRecs = true;
                        }
                        else
                        {
                            bGenRecs = true;
                        }
                        
                        Recs oRecs = new Recs(sRecText, guRecGuid,sRecType);
                        LRecs.Add(oRecs);

                    } while (sqlReader.Read());

                    sqlReader.Close();
                    iRetVal = 1;

                }
                else
                {
                    // no recs
                    iRetVal = 0;
                }
            }
            catch (Exception ex)
            {

                //record exception  
                sendErrEmail("**** MacDuffRecs Error\r\n\r\n" + ex.Message);
                logError(ex.Message);

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();


            }

            return iRetVal;
        }


        static bool GetCaseInfo(Guid guCaseID)
        {

            bool bRetVal = false;

            string sCity = "";
            string sState = "";
            string sZip = "";
            DateTime dCompDate;
            sInsCo = "";


            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetCaseInfo";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        sCaseNum = sqlReader.GetSqlInt32(0).ToString();
                        sInsured = sqlReader.GetSqlString(2).ToString();
                        sPolicy = sqlReader.GetSqlString(3).ToString();
                        sAdd1 = sqlReader.GetSqlString(4).ToString();
                        if (!sqlReader.IsDBNull(5))
                        {
                            sAdd2 = sqlReader.GetSqlString(5).ToString();
                        }
                        sCity = sqlReader.GetSqlString(6).ToString();
                        sState = sqlReader.GetSqlString(7).ToString();
                        sZip = sqlReader.GetSqlString(8).ToString();
                        sCSZ = sCity + ", " + sState + " " + sZip;
                        sCaseType = sqlReader.GetSqlString(9).ToString();
                        sProducer = sqlReader.GetSqlString(10).ToString();
                        dCompDate = (DateTime)sqlReader.GetSqlDateTime(11);
                        sCompDate = dCompDate.ToShortDateString();
                        sProducerPhone = sqlReader.GetSqlString(12).ToString();
                        sCustNumber = sqlReader.GetSqlString(13).ToString().Trim();

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }

                sqlCmd1.CommandText = "sp_GetCaseSpecialField";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@caseID", guCaseID);
                sqlCmd1.Parameters.AddWithValue("@fieldname", "Insurance Company");

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        if (!sqlReader.IsDBNull(0))
                        {
                            sInsCo = sqlReader.GetSqlString(0).ToString();
                        }

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }

                bRetVal = true;
            }
            catch (Exception ex)
            {

                //record exception  
                logError(ex.Message);

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();


            }

            return bRetVal;
        }


        static string GetCaseID(string sCaseNum)
        {

            int iCaseNum = Convert.ToInt32(sCaseNum);
            string sCaseID = "";

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetCaseGUID";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@casenum", iCaseNum);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        sCaseID = sqlReader.GetGuid(0).ToString();

                    } while (sqlReader.Read());

                    sqlReader.Close();
                }
            }
            catch (Exception ex)
            {

                //record exception  
                logError(ex.Message);

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();

            }

            return sCaseID;
        }

        static bool GetPhotos(Guid guRecID)
        {

            bool bRetVal = false;


            Guid guPhotoGuid;

            SqlConnection sqlConn1 = null;
            SqlCommand sqlCmd1 = null;
            SqlDataReader sqlReader = null;

            try
            {

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);
                sqlCmd1 = new SqlCommand();

                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_GetPhotosForRec";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.Parameters.AddWithValue("@CaseFormRecID", guRecID);
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();

                // Get all photos for rec
                sqlReader = sqlCmd1.ExecuteReader();

                if (sqlReader.HasRows)
                {
                    sqlReader.Read();

                    do
                    {

                        //photo guid
                        guPhotoGuid = (Guid)sqlReader.GetSqlGuid(0);

                        Photos oPhoto = new Photos(guRecID, guPhotoGuid);
                        LPhotos.Add(oPhoto);

                    } while (sqlReader.Read());

                    sqlReader.Close();

                }

                bRetVal = true;
            }
            catch (Exception ex)
            {

                //record exception  
                logError(ex.Message);

            }

            finally
            {

                // close objects

                if (sqlReader != null)
                    sqlReader.Close();

                if (sqlConn1 != null)
                    sqlConn1.Close();


            }

            return bRetVal;
        }

        static string StripTagsCharArray(string source)
        {

            string sRet = "";
            //char CrLf = char(13) + char(10);

            source = string.Join(" ", Regex.Split(source, @"(?:\r\n|\n|\r|\t)"));
            
            // replace known strings
            source = source.Replace("&nbsp;", " ");
            source = source.Replace("\\r;", "");
            source = source.Replace("\\n;", "");
            source = source.Replace("&amp;", "&");
            source = source.Replace("&ldquo;", "'");
            source = source.Replace("&rdquo;", "'");
            source = source.Replace("&lsquo;", "'");
            source = source.Replace("&rsquo;", "'");
            source = source.Replace("&ndash;", "-");
            source = source.Replace("&mdash;", "-");
            source = source.Replace("&quot;", "'");

            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }



            sRet = new string(array, 0, arrayIndex);

            // remove /r/n from front of string
            if (sRet.StartsWith("\r\n"))
            {
                sRet = sRet.Remove(0, 3);
            }

            // remove /r/n from front of string
            if (sRet.EndsWith("\r\n"))
            {
                sRet = sRet.Remove(sRet.Length-2, 2);
            }
            
            return sRet;
        }


        static void logError(string sErrText)
        {

            // record error message to log file
            LogUtils.LogUtils oLU;

            oLU = new LogUtils.LogUtils();

            oLU.logFileName = cfg_logfilename;

            oLU.OpenLog();
            oLU.WritetoLog(sErrText);

            oLU.closeLog();

        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "MacDuff Rec Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "jeff@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import MacDuff Rec Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }
    }
}
