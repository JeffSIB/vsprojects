﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace LastLoginAuto
{
    class Program
    {

        static LogUtils.LogUtils oLU;
        static string cfg_smtpserver;
        static string cfg_logfilename;
        static string cfg_360UtilConnStr;
        static string cfg_outputdir;
        static string cfg_emailnotify;
        static string cfg_deliverto;
        static Excel.Application oExcel = null;
        static Excel.Workbook oWorkbook = null;
        static Excel.Worksheet o360Worksheet = null;
        static Excel.Range oRange = null;
        static string sReportDate;
        static object oMissing = System.Reflection.Missing.Value;
        static bool mbErr;
        static string msErrMsg;
        static string msMsgBody;

        static void Main(string[] args)
        {

            mbErr = false;
            msErrMsg = "";
            msMsgBody = "";

            try
            {
                // load configuration values from app.config
                System.Collections.Specialized.NameValueCollection colNameVal;
                colNameVal = System.Configuration.ConfigurationManager.AppSettings;
                cfg_smtpserver = colNameVal.Get("smtpserver");
                cfg_logfilename = colNameVal.Get("logfilename");
                cfg_360UtilConnStr = colNameVal.Get("360UtilConnStr");
                cfg_outputdir = colNameVal.Get("outputdir");
                cfg_emailnotify = colNameVal.Get("emailnotify");
                cfg_deliverto = colNameVal.Get("deliverto");

                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("++++ Begin: " + DateTime.Now.ToString() + " ++++");

                DateTime dt = new DateTime();
                dt = DateTime.Today;
                sReportDate = dt.ToShortDateString();

                string sDateDisp = dt.Month.ToString() + dt.Day.ToString() + dt.Year.ToString();
                string sEmailSubject = "Last login report - " + dt.ToShortDateString();
                string sExcelFileNameNoEx = cfg_outputdir + "LastLogin_" + sDateDisp;
                string sExcelFileName = sExcelFileNameNoEx + ".xlsx";


                ///////////////////////////////////////////////////////////////
                

                // SQL
                SqlConnection sqlConn1 = null;
                SqlCommand sqlCmd1 = null;
                SqlDataReader sqlReader = null;

                int iRet = 0;
                string sFieldRep = "";
                string sFieldRepName = "";
                DateTime dLastLogin;
                string sLockedOut = "";
                bool bLockedOut = false;
                int iNumAssigend = 0;
                int iRow = 0;

                ///////////////////////////////////////////////////
                // 360 
                ///////////////////////////////////////////////////

                // set up SQL connection (360)
                sqlConn1 = new SqlConnection(cfg_360UtilConnStr);

                // Build FieldRepLastLogin table
                sqlCmd1 = new SqlCommand();
                sqlCmd1.CommandType = CommandType.StoredProcedure;
                sqlCmd1.CommandText = "sp_FieldRepLastLogin_Build";
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();
                iRet = sqlCmd1.ExecuteNonQuery();


                // Read from FieldRepLastLogin
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.CommandText = "SELECT [FieldRep],[FieldRepName],[LastLogin],[LockedOut],[NumberAssigned] FROM[Sutton360Utils].[dbo].[FieldRepLastLogin]";
                sqlReader = sqlCmd1.ExecuteReader();
                if (sqlReader.HasRows)
                {

                    oExcel = new Excel.Application();
                    oExcel.Visible = true;
                    oWorkbook = oExcel.Workbooks.Add(1);
                    o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets[1];
                    //o360Worksheet = (Excel.Worksheet)oWorkbook.Sheets.Add(oMissing,oSIBWorksheet,oMissing,oMissing);
                    o360Worksheet.Name = "360";
                    createHeader(o360Worksheet);

                    iRow = 4;

                    sqlReader.Read();

                    // loop through rows
                    do
                    {

                        //inspector number
                        if (sqlReader.IsDBNull(0))
                        {
                            sFieldRep = "";
                        }
                        else
                        {
                            sFieldRep = (string)sqlReader.GetSqlString(0);
                        }

                        //inspector name
                        if (sqlReader.IsDBNull(1))
                        {
                            sFieldRepName = "";
                        }
                        else
                        {
                            sFieldRepName = (string)sqlReader.GetSqlString(1);
                        }


                        //last login
                        dLastLogin = (DateTime)sqlReader.GetSqlDateTime(2);

                        //locked
                        bLockedOut = sqlReader.GetBoolean(3);
                        if (bLockedOut)
                            sLockedOut = "YES";
                        else
                            sLockedOut = "";

                        //assigned
                        if (sqlReader.IsDBNull(4))
                        {
                            iNumAssigend = 0;
                        }
                        else
                        {
                            iNumAssigend = (int)sqlReader.GetSqlInt32(4);
                        }


                        addData(o360Worksheet, iRow, 1, sFieldRep, "A" + iRow.ToString(), "A" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 2, sFieldRepName, "B" + iRow.ToString(), "B" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 3, dLastLogin.ToString(), "C" + iRow.ToString(), "C" + iRow.ToString(), "MM/DD/YYYY", "C");
                        addData(o360Worksheet, iRow, 4, sLockedOut, "D" + iRow.ToString(), "D" + iRow.ToString(), "", "C");
                        addData(o360Worksheet, iRow, 5, iNumAssigend.ToString(), "E" + iRow.ToString(), "E" + iRow.ToString(), "#,##0", "C");
                        iRow++;

                    } while (sqlReader.Read());     // 360

                }   // has rows

                sqlReader.Close();
                sqlConn1.Close();
                sqlConn1 = null;
                sqlCmd1 = null;


 

                //oWorkbook.SaveAs(cfg_outputdir + "ReviewerStats360_" + sDateDisp, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.SaveAs(sExcelFileNameNoEx, Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                oWorkbook.Close(true, oMissing, oMissing);
                oExcel.Quit();
                msMsgBody += "Processing completed for 360" + System.Environment.NewLine + cfg_outputdir + sExcelFileNameNoEx + System.Environment.NewLine + System.Environment.NewLine;
                
                releaseObject(oExcel);
                releaseObject(oWorkbook);
                releaseObject(o360Worksheet);
                sendExcelFile(sEmailSubject, sExcelFileName);
                msMsgBody += "File sent " + sExcelFileName;
            }


            catch (Exception ex)
            {
                //record exception  
                oLU.WritetoLog(ex.Message);
                mbErr = true;
                msErrMsg = ex.Message;
            }

            finally
            {
                oLU.closeLog();
                if (mbErr)
                {
                    sendErrEmail(msErrMsg);
                }
                else
                {
                    sendEmail(msMsgBody);                
                }
            
            }
        }

        static DateTime FirstDayofWeek(DateTime dDate)
        {
            // assumes Sunday is the first day ot the week

            CultureInfo info = Thread.CurrentThread.CurrentCulture;

            DayOfWeek firstday = info.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek today = info.Calendar.GetDayOfWeek(dDate);

            int diff = today - firstday;
            DateTime firstDate = dDate.AddDays(-diff);

            return firstDate;

        }

        static DateTime FirstDayOfMonth(DateTime dDate)
        {

            DateTime dRet = dDate;

            dRet = dRet.AddDays(-(dRet.Day - 1));

            return dRet;

        }

        static DateTime LastDayOfMonth(DateTime dDate)
        {

            DateTime lastDayOfMonth = new DateTime(dDate.Year, dDate.Month, 1).AddMonths(1).AddDays(-1);
            return lastDayOfMonth;

        }


        static void createHeader(Excel._Worksheet oWorkSheet)
        {

            Excel.Range oRange;


            oWorkSheet.get_Range("A1", "E1").Merge(false);
            oRange = oWorkSheet.get_Range("A1", "E1");
            oRange.FormulaR1C1 = "Field Rep Last Login as of " + sReportDate;

            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            oRange.Font.Size = 16;
            oRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Navy);

            oWorkSheet.Cells[3, 1] = "Field Rep";
            oRange = oWorkSheet.get_Range("A3", "A3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 2] = "Name";
            oRange = oWorkSheet.get_Range("B3", "B3");
            oRange.ColumnWidth = 25;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 3] = "Last login";
            oRange = oWorkSheet.get_Range("C3", "C3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 4] = "Locked out";
            oRange = oWorkSheet.get_Range("D3", "D3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);

            oWorkSheet.Cells[3, 5] = "# assigned";
            oRange = oWorkSheet.get_Range("E3", "E3");
            oRange.ColumnWidth = 15;
            oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oRange.Font.Bold = true;
            oRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gainsboro);
       
        }

      
   

        static void addData(Excel._Worksheet oWorkSheet, int row, int col, string data, 
			string cell1, string cell2,string format,string sHorizAlign)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            oRange.NumberFormat = format;
            if (sHorizAlign == "C")
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }
            else
            {
                oRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            }
        }

        static void addDataNoFormat(Excel._Worksheet oWorkSheet, int row, int col, string data,
            string cell1, string cell2)
        {
            oWorkSheet.Cells[row, col] = data;
            oRange = oWorkSheet.get_Range(cell1, cell2);
            oRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }

        static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        static void sendEmail(string sMsgBody)
        {

            string sRet;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();


            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = cfg_emailnotify;
            oMail.MsgSubject = "Last Login";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

            if (sRet.Length > 0)
            {
                oLU.WritetoLog("Error in module SendMail: " + sRet);
                mbErr = true;

            }

        }

        static void sendErrEmail(string sMsgBody)
        {

            string sRet;
            //'SendMail oMail;
            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Last Login Automation **";
            oMail.MsgBody = sMsgBody;
            oMail.SMTPServer = cfg_smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendExcelFile(string sSubject, string sAtt)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];
            string sBodyText = "Sutton Inspection Bureau, Inc." + System.Environment.NewLine + "727-384-5454";

            try
            {

                SendMail.SendMail oMail;
                oMail = new SendMail.SendMail();

                oMail.MailFrom = "noreply@sibfla.com";
                oMail.MailTo = cfg_deliverto;

                //oMail.MailBCC = "jeff@sibfla.com";
                oMail.MsgSubject = sSubject;
                oMail.MsgBody = sBodyText;
                oMail.SMTPServer = smtpserver;
                oMail.SendHTML = false;
                oMail.Attachment = sAtt;
                sRet = oMail.Send();
                oMail = null;
            }
            catch (Exception ex)
            {

                //record exception  
                throw ex;

            }

        }


    }
}
