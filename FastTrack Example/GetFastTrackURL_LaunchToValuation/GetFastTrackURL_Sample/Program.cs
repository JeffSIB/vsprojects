﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.ServiceModel;
using GetFastTrackURL_Sample.RCT_ValuationService;

namespace GetFastTrackURL_Sample
{
    class Program
    {
        static void Main(string[] args)
        {
            // Send the data and process response
            try
            {
                ChannelFactory<ValuationService> channelFactory = new ChannelFactory<ValuationService>("ValuationServiceEndpoint");
                channelFactory.Credentials.UserName.UserName = ConfigurationManager.AppSettings["UserName"];
                channelFactory.Credentials.UserName.Password = ConfigurationManager.AppSettings["Password"];
                ValuationService valuationService = channelFactory.CreateChannel();

                // Establish FastTrackRequest
                FastTrackRequest getFastTrackRequest = new FastTrackRequest();
                getFastTrackRequest.UserLogin = "Sutton_GFT";  // Specify UserLogin of user to be logged into RCT Express
                getFastTrackRequest.ValuationIdentifier = new ValuationIdentifier();
                getFastTrackRequest.ValuationIdentifier.PolicyNumber = "TESTMarchmont"; //Specify the valuation's unique identifier
                //getFastTrackRequest.FirstName = "New";   //optional
                //getFastTrackRequest.LastName = "User";  //optional
                //getFastTrackRequest.AgencyName = "NewTestAgency";   //only required when creating a new user or agency
                getFastTrackRequest.Operation = FastTrackOperationCode.Edit;
                //getFastTrackRequest.Role = "Agent";     //only required when creating a new user             
                getFastTrackRequest.RedirectURL = "http://www.sibfla.com";

                // ExpressLync4 ValutionService web service call to web method GetFastTrackURL:
                FastTrackResult getFastTrackResult = valuationService.GetFastTrackURL(getFastTrackRequest, null, null);

                // Display URL on writeline
                Console.WriteLine("FastTrackURL: {0}", getFastTrackResult.FastTrackURL);

                // Open url in default browser
                System.Diagnostics.Process.Start(getFastTrackResult.FastTrackURL);
            }
            catch (FaultException<ExpressLyncFault> faultException)
            {
                // ExpressLyncFault contains a collection of errors
                foreach (ExpressLyncError error in faultException.Detail.Errors)
                {
                    // Each RctError contains error code and description
                    Console.WriteLine("{0} - {1}", error.ErrorCode, error.ErrorDescription);
                }
            }
            catch (Exception ex)
            {
                // Any other exceptions
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("Press Enter to exit.");
            Console.ReadLine();
        }
    }
}
