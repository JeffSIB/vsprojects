﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using ImportRequest360;
using System.Configuration;
using System.IO;
using LC360API.Carrier_V1;


namespace importAscendant
{

    class Program
    {
        public class ImportRecord
        {

            public string CustomerUserName { get; set; }
            public string CustomerPassword { get; set; }

            public string CustomerAccount { get; set; }
            public string InspectionType { get; set; }
            public string EmailConfirmation { get; set; }
            public string PolicyNumber { get; set; }
            public string EffectiveDate { get; set; }
            public string Underwriter { get; set; }
            public string UnderwriterFirstName { get; set; }
            public string UnderwriterLastName { get; set; }
            public string AgencyAgentName { get; set; }
            public string AgencyAgentContact { get; set; }
            public string AgencyAgentPhone { get; set; }
            public string AgencyAgentCode { get; set; }
            public string AgencyAgentEmail { get; set; }
            public string InsuranceCompany { get; set; }
            public string Producer { get; set; }
            public string RushHandling { get; set; }
            public string InsuredName { get; set; }
            public string InsuredLastName { get; set; }
            public string InsuredFirstName { get; set; }
            public string InsuredEmail { get; set; }
            public string ContactName { get; set; }
            public string ContactPhoneHome { get; set; }
            public string ContactPhoneWork { get; set; }
            public string ContactPhoneCell { get; set; }
            public string MailAddress1 { get; set; }
            public string MailAddress2 { get; set; }
            public string MailCity { get; set; }
            public string MailState { get; set; }
            public string MailZip { get; set; }
            //public string BusinessOperations { get; set; }
            public string LocationNumberOf { get; set; }
            public string LocationAddress1 { get; set; }
            public string LocationAddress2 { get; set; }
            public string LocationCity { get; set; }
            public string LocationState { get; set; }
            public string LocationZip { get; set; }
            public string LocationContactName { get; set; }
            public string LocationContactPhone { get; set; }
            public string Comments { get; set; }
            public string BuildingCost { get; set; }
            public string BusinessTotalRevenue { get; set; }
            public string ContentsCost { get; set; }
            public string CoverageA { get; set; }
            public string ISOClass { get; set; }
            public string YearBuilt { get; set; }
            public string Occupancy { get; set; }
            public string Construction { get; set; }
            public string GenericField1Name { get; set; }
            public string GenericField1Value { get; set; }
            public string GenericField2Name { get; set; }
            public string GenericField2Value { get; set; }
            public string GenericField3Name { get; set; }
            public string GenericField3Value { get; set; }
        }


        /// <summary>
        /// Setup properties from App.Config -> AppSettings
        /// </summary>
        #region Properties

        static string cfg_logfilename = ConfigurationManager.AppSettings["logfilename"];
        static string cfg_360ConnStr = ConfigurationManager.AppSettings["360ConnStr"];
        //static string cfg_360UtilConnStr = ConfigurationManager.AppSettings["360UtilConnStr"];
        static string cfg_SQLMainSIBIConnStr = ConfigurationManager.AppSettings["SIBIConnStr"];
        static string completedMinDate = ConfigurationManager.AppSettings["CompletedMinDate"];
        static string cfg_smtpserver = ConfigurationManager.AppSettings["smtpserver"];
        static string cfg_pdfroot = ConfigurationManager.AppSettings["PDFRoot"];
        static string cfg_sourcedir = ConfigurationManager.AppSettings["SourceDir"];
        static string cfg_archivedir = ConfigurationManager.AppSettings["ArchiveDir"];


        static LogUtils.LogUtils oLU;
        static bool bErr = false;

        //Used for email message body.  
        static StringBuilder sbEmail = new StringBuilder();

        #endregion
        static void Main(string[] args)
        {

            string sExcelFileName = "";
            string sInspType = "";
            string sUWFirst = "";
            string sUWLast = "";
            string sRush = "";
            string sInsName = "";
            string sComments = "";
            bool bErr = false;

            try
            {
                // initialize log file class
                oLU = new LogUtils.LogUtils();

                // set log file name
                oLU.logFileName = cfg_logfilename;

                // open log file
                oLU.OpenLog();
                oLU.WritetoLog("**** Begin ****");

                if (args.Length == 0)
                {
                    throw new ApplicationException("No arguments passed");                
                }

                // get excel file name from command line
                sExcelFileName = args[0];

                // does file exist?
                FileInfo fi = new FileInfo(cfg_sourcedir + sExcelFileName);
                if (!fi.Exists)
                {
                    // send email
                    throw new ApplicationException("File does not exist: " + sExcelFileName);
                }

            }

            catch (Exception ex)
            {
                oLU.closeLog();
                sendErrEmail("Error initializing imptMacNeill\r\n\r\n" + ex.Message);
                return;
            }
            
 
            try
            {

                oLU.WritetoLog("Processing: " + sExcelFileName);
                sbEmail.Append(System.Environment.NewLine + "Processing: " + sExcelFileName + System.Environment.NewLine);
                oLU.closeLog();
                oLU.OpenLog();
                oLU.WritetoLog("Starting ExcelQueryFactory");
                oLU.closeLog();
                oLU.OpenLog();

                var excel = new ExcelQueryFactory(cfg_sourcedir + sExcelFileName);

                oLU.WritetoLog("Mapping data");
                oLU.closeLog();
                oLU.OpenLog();

                //excel.AddMapping<ImportRecord>(x => x.InsuranceCompany, "Insurance Co");
                excel.AddMapping<ImportRecord>(x => x.PolicyNumber, "Policy#");
                excel.AddMapping<ImportRecord>(x => x.InsuredName, "Insured");
                excel.AddMapping<ImportRecord>(x => x.EffectiveDate, "Policy Eff");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentName, "Agent");
                excel.AddMapping<ImportRecord>(x => x.ContactName, "Insured Contact Name");
                excel.AddMapping<ImportRecord>(x => x.ContactPhoneHome, "Insured Contact Phone");
                excel.AddMapping<ImportRecord>(x => x.InsuredEmail, "Insured Email");
                excel.AddMapping<ImportRecord>(x => x.LocationAddress1, "Insured Address");
                excel.AddMapping<ImportRecord>(x => x.LocationCity, "Insured City");
                excel.AddMapping<ImportRecord>(x => x.LocationState, "Insured State");
                excel.AddMapping<ImportRecord>(x => x.LocationZip, "Insured Zipcode");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentName, "Agent");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentPhone, "Agent Phone");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentContact, "Agent Name");
                excel.AddMapping<ImportRecord>(x => x.AgencyAgentEmail, "Agent Email");
                excel.AddMapping<ImportRecord>(x => x.LocationNumberOf, "Number of Locations");


                //excel.AddMapping<ImportRecord>(x => x.Comments, "Comment");

                oLU.WritetoLog("Listing rows");
                oLU.closeLog();
                oLU.OpenLog();

                var rows = (from row in excel.Worksheet<ImportRecord>(0) select row).ToList();

                oLU.WritetoLog("Processing rows");
                oLU.closeLog();
                oLU.OpenLog();

                foreach (var row in rows)
                {

                    ImportRequests oAPI = new ImportRequests();
                    oAPI.CustomerUserName = "APIProd";
                    oAPI.CustomerPassword = "Sutton2012";

                     oAPI.CustomerAccount = "7279"; //Ascendant Commercial Insurance - 7279

                    // Underwriter
                    sUWFirst = "Laylett";
                    sUWLast = "Vazquez ";

                    // Inspection type
                    sInspType = "";

                    if (row.Comments != null)
                    {
                        sComments = row.Comments.Trim();
                        if (sComments.Length > 2)
                        {
                            sInspType = sComments.Substring(0, 3).ToUpper();
                        }
                    }

                    oAPI.InspectionType = "7279-TWC";


                    // Add number of locations to comments
                    row.Comments += "\r\n\r\nNumber of Locations: " + row.LocationNumberOf;

                    sInsName = row.InsuredName.Trim();
                    oAPI.EmailConfirmation = "";
                    oAPI.PolicyNumber = row.PolicyNumber;
                    oAPI.EffectiveDate = row.EffectiveDate;
                    oAPI.Underwriter = "";
                    oAPI.UnderwriterFirstName = sUWFirst;
                    oAPI.UnderwriterLastName = sUWLast;
                    oAPI.UnderwriterCorrEmail = "lvazquez@ascendantgroup.com";
                    oAPI.UnderwriterRptEmail = "lvazquez@ascendantgroup.com";
                    oAPI.UnderwriterPhone = "(877) 834-4990 x1405";
                    oAPI.InsuranceCompany = "";
                    oAPI.Producer = "";
                    oAPI.RushHandling = sRush;
                    oAPI.InsuredName = sInsName;
                    oAPI.ContactName = row.ContactName;
                    oAPI.ContactPhoneHome = row.ContactPhoneHome;
                    oAPI.MailAddress1 = row.LocationAddress1;
                    oAPI.MailAddress2 = row.LocationAddress2;
                    oAPI.MailCity = row.LocationCity;
                    oAPI.MailState = row.LocationState;
                    oAPI.MailZip = row.LocationZip;
                    oAPI.LocationAddress1 = row.LocationAddress1;
                    oAPI.LocationAddress2 = row.LocationAddress2;
                    oAPI.LocationCity = row.LocationCity;
                    oAPI.LocationState = row.LocationState;
                    oAPI.LocationZip = row.LocationZip;
                    oAPI.LocationContactName = row.ContactName;
                    oAPI.LocationContactPhone = row.ContactPhoneHome;
                    oAPI.Comments = row.Comments;
                    //oAPI.CoverageA = row.CoverageA;
                    //oAPI.YearBuilt = row.YearBuilt;

                    // Agency / Agent
                    oAPI.AgencyAgentName = row.AgencyAgentName;
                    oAPI.AgentCode = row.AgencyAgentCode;
                    oAPI.AgencyAgentPhone = row.AgencyAgentPhone;
                    oAPI.AgentFax = "";
                    oAPI.AgencyAgentContact = row.AgencyAgentContact;
                    oAPI.AgencyAgentEmail = row.AgencyAgentEmail;
                    oAPI.AgentAddress1 = "5656 Central Ave";
                    oAPI.AgentAddress2 = "";
                    oAPI.AgentCity = "St. Petersburg";
                    oAPI.AgentState = "FL";
                    oAPI.AgentZip = "33707";

                    oLU.WritetoLog(System.Environment.NewLine + "---- Importing Policy# " + row.PolicyNumber);
                    sbEmail.Append("Importing Policy# " + row.PolicyNumber + System.Environment.NewLine);
                    oLU.closeLog();
                    oLU.OpenLog();


                    string sRet = oAPI.Import();
                    
                    oLU.WritetoLog("oAPI.Import return for for Policy# " + row.PolicyNumber + "\r\n\r\n" + sRet);

                    var importResults = sRet.FromJSON<List<ImportResult>>();

                    foreach (var importResult in importResults)
                    {

                        if (importResult.Successful)
                        {
                            oLU.WritetoLog("Import successful - Case Number: " + importResult.CaseNumber.ToString());
                            sbEmail.Append("Import successful - Case Number: " + importResult.CaseNumber.ToString() + System.Environment.NewLine);
                        }
                        else
                        {
                            bErr = true;
                            oLU.WritetoLog("**** Import failed **** "); if (importResult.Errors != null)
                            sbEmail.Append("**** Import failed ****" + System.Environment.NewLine);

                            foreach (var error in importResult.Errors)
                                {
                                    oLU.WritetoLog("Error: " + error.ErrorText);
                                    sbEmail.Append("Error: " + error.ErrorText + System.Environment.NewLine);
                                }
                        }

                        if ((bool)importResult.Duplicate)
                        {
                            bErr = true;
                            oLU.WritetoLog("Duplicate case");
                            sbEmail.Append("Duplicate case" + System.Environment.NewLine);
                        }
                    }
                }

                excel = null;

                // If import is successful, copy to archive dir and delete source file
                if (!bErr)
                {
                    string sSourceName = cfg_sourcedir + sExcelFileName;
                    string sDestName = cfg_archivedir + sExcelFileName;

                    File.Copy(sSourceName, sDestName);

                    if (File.Exists(sDestName))
                    {
                        File.Delete(sSourceName);
                    }
                    else
                    {
                        throw new ApplicationException("Copy failed for: " + sExcelFileName);
                    }
                }

            }   //try

            catch (Exception ex)
            {
                oLU.WritetoLog("Import Error: \r\n\r\n" + ex.Message);
                sbEmail.Append("Exception Logged" + System.Environment.NewLine + ex.Message + System.Environment.NewLine);
                sendErrEmail("Exception logged" + System.Environment.NewLine + ex.Message);
            }

            finally
            {
                sendLogEmail(sbEmail.ToString());
                oLU.closeLog();
            }
            
        }

        static void sendLogEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "Import MacNeill Processing";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = false;
            sRet = oMail.Send();
            oMail = null;

        }

        static void sendErrEmail(string bodytext)
        {

            string sRet;
            string smtpserver = ConfigurationManager.AppSettings["smtpserver"];

            SendMail.SendMail oMail;
            oMail = new SendMail.SendMail();

            oMail.MailFrom = "noreply@sibfla.com";
            oMail.MailTo = "jeff@sibfla.com";
            oMail.MsgSubject = "** Errors logged by Import MacNeill Processing **";
            oMail.MsgBody = bodytext;
            oMail.SMTPServer = smtpserver;
            oMail.SendHTML = true;
            sRet = oMail.Send();
            oMail = null;

        }
    }
}
