﻿namespace CL_CreateCommVal
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblPolicyNumber;
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.helpProvider2 = new System.Windows.Forms.HelpProvider();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtState = new System.Windows.Forms.TextBox();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.lblzip = new System.Windows.Forms.Label();
            this.lblstate = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblAddValHeader = new System.Windows.Forms.Label();
            this.btAddVal = new System.Windows.Forms.Button();
            this.btGet = new System.Windows.Forms.Button();
            this.btUpdate = new System.Windows.Forms.Button();
            this.txtNewPolicyNumber = new System.Windows.Forms.TextBox();
            this.lblValuaid = new System.Windows.Forms.Label();
            this.txtValuationId = new System.Windows.Forms.TextBox();
            this.lblGetValuationHeader = new System.Windows.Forms.Label();
            this.lblUpdateValuationHeader = new System.Windows.Forms.Label();
            this.lblCurPolNum = new System.Windows.Forms.Label();
            this.txtCurPolNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBusName = new System.Windows.Forms.TextBox();
            lblPolicyNumber = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPolicyNumber
            // 
            lblPolicyNumber.AutoSize = true;
            lblPolicyNumber.Location = new System.Drawing.Point(12, 334);
            lblPolicyNumber.Name = "lblPolicyNumber";
            lblPolicyNumber.Size = new System.Drawing.Size(100, 13);
            lblPolicyNumber.TabIndex = 17;
            lblPolicyNumber.Text = "New Policy Number";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(135, 70);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(205, 20);
            this.txtAddress.TabIndex = 2;
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(135, 95);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(205, 20);
            this.txtCity.TabIndex = 5;
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(135, 119);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(205, 20);
            this.txtState.TabIndex = 6;
            // 
            // txtZip
            // 
            this.txtZip.Location = new System.Drawing.Point(135, 144);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(205, 20);
            this.txtZip.TabIndex = 7;
            // 
            // lblzip
            // 
            this.lblzip.AutoSize = true;
            this.lblzip.Location = new System.Drawing.Point(12, 148);
            this.lblzip.Name = "lblzip";
            this.lblzip.Size = new System.Drawing.Size(22, 13);
            this.lblzip.TabIndex = 8;
            this.lblzip.Text = "Zip";
            // 
            // lblstate
            // 
            this.lblstate.AutoSize = true;
            this.lblstate.Location = new System.Drawing.Point(12, 123);
            this.lblstate.Name = "lblstate";
            this.lblstate.Size = new System.Drawing.Size(32, 13);
            this.lblstate.TabIndex = 9;
            this.lblstate.Text = "State";
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(12, 99);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(24, 13);
            this.lblCity.TabIndex = 10;
            this.lblCity.Text = "City";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(12, 74);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(45, 13);
            this.lblAddress.TabIndex = 11;
            this.lblAddress.Text = "Address";
            // 
            // lblAddValHeader
            // 
            this.lblAddValHeader.AutoSize = true;
            this.lblAddValHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddValHeader.Location = new System.Drawing.Point(12, 9);
            this.lblAddValHeader.Name = "lblAddValHeader";
            this.lblAddValHeader.Size = new System.Drawing.Size(179, 13);
            this.lblAddValHeader.TabIndex = 12;
            this.lblAddValHeader.Text = "AddValuationMinimum Requirements";
            // 
            // btAddVal
            // 
            this.btAddVal.Location = new System.Drawing.Point(402, 95);
            this.btAddVal.Name = "btAddVal";
            this.btAddVal.Size = new System.Drawing.Size(149, 48);
            this.btAddVal.TabIndex = 13;
            this.btAddVal.Text = "Create Valuation with minimum input";
            this.btAddVal.UseVisualStyleBackColor = true;
            this.btAddVal.Click += new System.EventHandler(this.btAddVal_Click);
            // 
            // btGet
            // 
            this.btGet.Location = new System.Drawing.Point(402, 220);
            this.btGet.Name = "btGet";
            this.btGet.Size = new System.Drawing.Size(149, 48);
            this.btGet.TabIndex = 14;
            this.btGet.Text = "Get Valuation";
            this.btGet.UseVisualStyleBackColor = true;
            this.btGet.Click += new System.EventHandler(this.btGet_Click);
            // 
            // btUpdate
            // 
            this.btUpdate.Location = new System.Drawing.Point(402, 316);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(149, 48);
            this.btUpdate.TabIndex = 15;
            this.btUpdate.Text = "Update Policy Number";
            this.btUpdate.UseVisualStyleBackColor = true;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // txtNewPolicyNumber
            // 
            this.txtNewPolicyNumber.Location = new System.Drawing.Point(135, 330);
            this.txtNewPolicyNumber.Name = "txtNewPolicyNumber";
            this.txtNewPolicyNumber.Size = new System.Drawing.Size(205, 20);
            this.txtNewPolicyNumber.TabIndex = 16;
            // 
            // lblValuaid
            // 
            this.lblValuaid.AutoSize = true;
            this.lblValuaid.Location = new System.Drawing.Point(12, 223);
            this.lblValuaid.Name = "lblValuaid";
            this.lblValuaid.Size = new System.Drawing.Size(63, 13);
            this.lblValuaid.TabIndex = 19;
            this.lblValuaid.Text = "Valuation Id";
            // 
            // txtValuationId
            // 
            this.txtValuationId.Location = new System.Drawing.Point(135, 219);
            this.txtValuationId.Name = "txtValuationId";
            this.txtValuationId.Size = new System.Drawing.Size(205, 20);
            this.txtValuationId.TabIndex = 18;
            // 
            // lblGetValuationHeader
            // 
            this.lblGetValuationHeader.AutoSize = true;
            this.lblGetValuationHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGetValuationHeader.Location = new System.Drawing.Point(12, 192);
            this.lblGetValuationHeader.Name = "lblGetValuationHeader";
            this.lblGetValuationHeader.Size = new System.Drawing.Size(94, 13);
            this.lblGetValuationHeader.TabIndex = 20;
            this.lblGetValuationHeader.Text = "Retrieve Valuation";
            // 
            // lblUpdateValuationHeader
            // 
            this.lblUpdateValuationHeader.AutoSize = true;
            this.lblUpdateValuationHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdateValuationHeader.Location = new System.Drawing.Point(12, 301);
            this.lblUpdateValuationHeader.Name = "lblUpdateValuationHeader";
            this.lblUpdateValuationHeader.Size = new System.Drawing.Size(94, 13);
            this.lblUpdateValuationHeader.TabIndex = 21;
            this.lblUpdateValuationHeader.Text = "Retrieve Valuation";
            // 
            // lblCurPolNum
            // 
            this.lblCurPolNum.AutoSize = true;
            this.lblCurPolNum.Location = new System.Drawing.Point(12, 250);
            this.lblCurPolNum.Name = "lblCurPolNum";
            this.lblCurPolNum.Size = new System.Drawing.Size(112, 13);
            this.lblCurPolNum.TabIndex = 23;
            this.lblCurPolNum.Text = "Current Policy Number";
            // 
            // txtCurPolNum
            // 
            this.txtCurPolNum.Location = new System.Drawing.Point(135, 246);
            this.txtCurPolNum.Name = "txtCurPolNum";
            this.txtCurPolNum.Size = new System.Drawing.Size(205, 20);
            this.txtCurPolNum.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Business name";
            // 
            // txtBusName
            // 
            this.txtBusName.Location = new System.Drawing.Point(135, 44);
            this.txtBusName.Name = "txtBusName";
            this.txtBusName.Size = new System.Drawing.Size(205, 20);
            this.txtBusName.TabIndex = 25;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtBusName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblCurPolNum);
            this.Controls.Add(this.txtCurPolNum);
            this.Controls.Add(this.lblUpdateValuationHeader);
            this.Controls.Add(this.lblGetValuationHeader);
            this.Controls.Add(this.lblValuaid);
            this.Controls.Add(this.txtValuationId);
            this.Controls.Add(lblPolicyNumber);
            this.Controls.Add(this.txtNewPolicyNumber);
            this.Controls.Add(this.btUpdate);
            this.Controls.Add(this.btGet);
            this.Controls.Add(this.btAddVal);
            this.Controls.Add(this.lblAddValHeader);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.lblCity);
            this.Controls.Add(this.lblstate);
            this.Controls.Add(this.lblzip);
            this.Controls.Add(this.txtZip);
            this.Controls.Add(this.txtState);
            this.Controls.Add(this.txtCity);
            this.Controls.Add(this.txtAddress);
            this.Name = "frmMain";
            this.Text = "Main ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.HelpProvider helpProvider2;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtState;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.Label lblzip;
        private System.Windows.Forms.Label lblstate;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblAddValHeader;
        private System.Windows.Forms.Button btAddVal;
        private System.Windows.Forms.Button btGet;
        private System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.TextBox txtNewPolicyNumber;
        private System.Windows.Forms.Label lblValuaid;
        private System.Windows.Forms.TextBox txtValuationId;
        private System.Windows.Forms.Label lblGetValuationHeader;
        private System.Windows.Forms.Label lblUpdateValuationHeader;
        private System.Windows.Forms.Label lblCurPolNum;
        private System.Windows.Forms.TextBox txtCurPolNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBusName;
    }
}

