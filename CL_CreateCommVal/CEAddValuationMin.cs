﻿using CL_CreateCommVal.IValuationService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CL_CreateCommVal
{
    class CEAddValuationMin
    {
        private string _contextUserName = ConfigurationManager.AppSettings["ContextUserName"];
        private string _contextUserPassword = ConfigurationManager.AppSettings["ContextUserPassword"];
        private string _contextAlias = ConfigurationManager.AppSettings["ContextAlias"];
        private Guid _contextCompanyId = new Guid(ConfigurationManager.AppSettings["CompanyId"]);
        private string _contextLogonUser = ConfigurationManager.AppSettings["LogonUser"];

        private ChannelFactory<IValuation> _channelFactory = new ChannelFactory<IValuation>("ClearUsernameBinding_IValuation");

        public CEAddValuationMin()
        {
            _channelFactory.Credentials.UserName.UserName = _contextUserName + "@" + _contextAlias;
            _channelFactory.Credentials.UserName.Password = _contextUserPassword;
        }

        public long CEAddValuationMinimum(string businessname,string line1, string city, string state, string zip)
        {
            IValuation ivaluation = _channelFactory.CreateChannel();

            AddValuationMinimumRequest request = new AddValuationMinimumRequest();

            request.CompanyID = _contextCompanyId;
            request.Logon = _contextLogonUser;

            request.Valuation = new ValuationMinimum();
            request.Valuation.Line1 = line1;
            request.Valuation.City = city;
            request.Valuation.RegionCode = state;
            request.Valuation.PostalCode = zip;
            request.Valuation.BusinessName = businessname;

            try
            {
                ResponseNewID response = ivaluation.AddValuationMinimum(request);

                if ((response.Errors.Length > 0))
                {
                    foreach (ErrorDetail error in response.Errors)
                    {
                        MessageBox.Show(error.Description);
                    }
                }
                else
                {
                    return response.NewID;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }

            return 0; //0 = unsuccessful response
        }
       

    }
}
